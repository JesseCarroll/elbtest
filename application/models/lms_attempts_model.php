<?php

class Lms_attempts_model extends CI_Model {

    // table name
    private $tablename = 'lms_attempts';
    private $primarykey = 'id';

    /**
     * Retrieves record data.
     * @return user data
     */
    function get_by_id($id = 0){
        $this->db->where($this->primarykey, $id);
        $query = $this->db->get($this->tablename);
        return $query->row();
    }

    function get_latest_attempt($attempt_data) {
        $limit = 1;
        $this->db->order_by('timestamp','DESC');
        $query = $this->db->get_where($this->tablename, $attempt_data, $limit);
        return $query->row();
    }

    /**
     * Retrieves all attempts for a user based on client and optionally completion.
     * @return attempt data
     */
    function attempts_by_user($user_id, $client_id, $status='completed'){
        $this->db->select('cml_lms_attempts.*');
        $this->db->join('cml_lms_attempts', 'cml_lms_attempts.user_id = cml_lms_users.id');
        $this->db->join('cml_courses', 'cml_courses.course_id = cml_lms_attempts.course_id');
        $this->db->where(array('cml_courses.client_id' => $client_id, 'cml_lms_attempts.status' => $status));

        $query = $this->db->get_where('cml_lms_users', array('cml_lms_users.user_id' => $user_id));

        return $query->result_array();
    }

    /**
     * Retrieves record data of all completions within client id.
     * @return user data, course_id
     */
    function completion_by_client($client_id){

        $this->db->select('cml_courses.course_id, cml_lms_users.user_id, cml_lms_attempts.timestamp');
        $this->db->join('cml_lms_attempts', 'cml_lms_attempts.course_id = cml_courses.course_id');
        $this->db->join('cml_lms_users', 'cml_lms_attempts.user_id = cml_lms_users.id');
        $this->db->where('cml_lms_attempts.status', 'completed');
        $query = $this->db->get_where('cml_courses', array('client_id' => $client_id));

        return $query->result_array();
    }

    /**
     * Adds record with new data.
     * @return new attempt id
     */
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }

    /**
     * Updates record with new data.
     * @return new attempt id
     */
    function update($data,$attempt_id){
        $this->db->where('id', $attempt_id);
        $this->db->update($this->tablename, $data);
    }
}
?>