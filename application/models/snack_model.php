<?php

class Snack_model extends CI_Model {

    // table name
    private $user_table = 'lms_users';
    private $client_table = 'cml_userClient';
    private $token_table = 'cml_userToken';
    private $primarykey = 'id';

    function __construct() {
        parent::__construct();
    }

    function get_user_id($email) {

      $this->db->where('email', $email);
      $query = $this->db->get($this->user_table);
      return $query->row();

    }

    function verify_client($user_id, $code) {

      $this->db->select('client_id');
      $this->db->where('user_id', $user_id);
      $this->db->where('code', $code);
      $query = $this->db->get($this->client_table);
      return $query->row();

    }

    function create_token($user_id) {

      $seed = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.'0123456789');
      $token = '';
      foreach (array_rand($seed, 8) as $k) $token .= $seed[$k];

      return $token;
    }

    function save_token($data) {

      $this->db->insert($this->token_table, $data);

    }

    function email_token($email, $token) {

      $this->load->helper('email');

      if (valid_email($email)) {

        $subject = "Your Snackbar Login Token";
        $message = "Your temporary Snackbar Login token is: " . $token;

        send_email($email, $subject, $message);

      }

    }

    function token_check($user_id, $token) {

      $this->db->where('user_id', $user_id);
      $this->db->where('token', $token);
      $query = $this->db->get($this->token_table);
      return $query->row();

    }

    function remove_token($token_id) {

      $this->db->delete($this->token_table, array($this->primarykey => $token_id));

    }

    /**
     * Retrieves record data.
     * @return user data
     */
    function verify_user($email='', $code=0) {

      $err = '';

      // get user id from email address
      $user = $this->get_user_id($email);

      if($user) {

        // check if user has access to client
        $result = $this->verify_client($user->id, $code);

        if($result->client_id) {

          // user has access to this client, so create a token for them
          $token = $this->create_token($user->id);

          $data = array(
            user_id => $user->id,
            token => $token
          );

          $this->save_token($data);

          // email user their token
          $this->email_token($email, $token);
        }
        else {
          $err = "You do not have access to this client.";
        }
      }
      else {
        $err = "Sorry, that email address is not registered.";
      }

      return $err;
    }

    function verify_token($email='', $code=0, $token=0) {

      $err = '';

      // get user id from email address
      $user = $this->get_user_id($email);

      if($user) {

        // check for token match
        $result = $this->token_check($user->id, $token);

        if($result) {

          // return client id
          $client = $this->verify_client($user->id, $code);

          if($client) {

            $this->remove_token($result->id);

            $success = array(
              'client_id' => $client->client_id,
              'user_id' => $user->id,
              'user_name' => $user->first_name." ".$user->last_name
            );

            print_r(json_encode($success));
          }
        }
        else {
          $err = "The token you have entered is incorrect.";
        }
      }
      else {
        $err = "Sorry, that email address is not registered.";
      }

      return $err;
    }
}
?>