<?php

class Coursedownload_model extends CI_Model {

    // table name
    private $tablename = 'course_downloads';
    private $primarykey = 'download_id';

    function __construct()
    {
        parent::__construct();
    }

    // get number of records in database
    function count_all(){
        return $this->db->count_all($this->tablename);
    }

    // get records with paging
    function get_all($limit = 200, $offset = 0){
        $this->db->order_by($this->primarykey,'desc');
        $query = $this->db->get($this->tablename, $limit, $offset);
        return $query->result();
    }

    // get record by id
    function get_by_id($id = 0){
        $this->db->where($this->primarykey, $id);
        $query = $this->db->get($this->tablename);
        return $query->row();
    }

    // get record by course id
    function get_by_course_id($course_id = 0){
        $this->db->where('course_id', $course_id);
        $this->db->order_by('date_downloaded', 'DESC');
        $query = $this->db->get($this->tablename);
        return $query->result();
    }

    // add new record
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }

    // update record by id
    function update($id = 0, $data){
        $this->db->where($this->primarykey, $id);
        $this->db->update($this->tablename, $data);
    }

    // delete record by id
    function delete($id = 0){
        $this->db->where($this->primarykey, $id);
        $this->db->delete($this->tablename);
    }

}

?>
