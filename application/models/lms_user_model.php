<?php

class Lms_user_model extends CI_Model {

    // table name
    private $tablename = 'lms_users';
    private $primarykey = 'id';


    /**
     * Retrieves record data.
     * @return user data
     */
    function get_by_email($email, $user_id = ''){
        $this->db->where('email', $email);

        if($user_id !== '') {
            $this->db->where('user_id', $user_id);
        }

        $query = $this->db->get($this->tablename);
        return $query->row();
    }

    /**
     * Adds record with new data.
     * @return new user id
     */
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }

    /**
     * Updates the current user with new data.
     * @return a new object with the new data updated, or null on error
     */
    function update($data) {
        $res = $this->db->update($this->tablename, $data,
            array($this->primarykey => $this->id));
        if ($res) {
            // Refresh
            return $this->get_by_id($this->id);
        }
        return null;
    }

}
?>