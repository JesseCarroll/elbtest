<?php

class Version_model extends CI_Model {

    // table name
    private $tablename = 'versions';
    private $primarykey = 'version_id';

    function __construct()
    {
        parent::__construct();
    }

    // get number of records in database
    function count_all(){
        return $this->db->count_all($this->tablename);
    }

    // get records with paging
    function get_all($limit = 200, $offset = 0){
        $this->db->order_by($this->primarykey,'desc');
        $query = $this->db->get($this->tablename, $limit, $offset);
        return $query->result();
    }

    // get record by id
    function get_by_id($id = 0){
        $this->db->where($this->primarykey, $id);
        $query = $this->db->get($this->tablename);
        return $query->row(0, get_class($this));
    }

    // TODO: throw a warning when there is more than one row returned.
    function get_by($column, $value) {
        $this->db->where($column, $value);
        $query = $this->db->get($this->tablename);
        return $query->row(0, get_class($this));
    }

    // add new record
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }

    // update record by id
    function update($id = 0, $data){
        $this->db->where($this->primarykey, $id);
        $this->db->update($this->tablename, $data);
    }

    // delete record by id
    function delete($id = 0){
        $this->db->where($this->primarykey, $id);
        $this->db->delete($this->tablename);
    }

    // get all active records
    function get_all_active(){
        $where = array('version_status' => '1');
        $this->db->where($where);
        $query = $this->db->get($this->tablename);
        return $query->result();
    }

    function get_latest_version() {
        $this->db->select('version_number, version_sub');
        $this->db->order_by('date_created', 'ASC');
        $query = $this->db->get($this->tablename);
        $row = $query->last_row();
        return $row;
    }

	// get version columns data by version id or version number
    function get_version_columns($columns = 'version_number', $version_number = 0, $version_id = 0){
		$this->db->select($columns);
		if(!empty($version_number)) {
			$this->db->where('version_number', $version_number);
		}
		if(!empty($version_id)) {
			$this->db->where($this->primarykey, $version_id);
		}
        $query = $this->db->get($this->tablename);
        $result = $query->row();
        return $result;
    }

    /**
     * Returns the path in the filesystem to the version folder.
     * @param extra_path a path to a file that should be appended to the version folder.
     */
    public function core_folder_path($extra_path = '') {
        $extra_path = ($extra_path) ? "/$extra_path" : '';
        $path = $this->config->item('version_path', 'default') . $this->version_sub . $extra_path;
        return $path;
    }

    /**
     * Returns the url to the version folder
     * @param extra_path a path to a file that should be appended to the version folder.
     */
    public function core_folder_url($extra_path = '') {
        return $this->config->item('version_core_url', 'default') . $this->version_sub . "/$extra_path";
    }

    public function get_subversion($version, $new = false){
        $version_path = $this->config->item('version_path', 'default');
        $version_folders = $this->aws->list_objects($version_path . $version);
        $latest_version = 0;

        if($version_folders) {
            $version_array = array();
            foreach($version_folders as $item) {
                preg_match('/(\d+\.\d+\.?\d*)/', $item['Key'], $matches);
                if(!in_array($matches[0], $version_array)) {
                    $version_array[] = $matches[0];
                }
            }

            list($major,$minor) = explode(".", $version);


            foreach($version_array as $folder) {

                if(substr_count($folder, '.')==2) {
                    list($submajor, $subminor, $subversion) = explode(".", $folder);
                } else {
                    list($submajor, $subminor) = explode(".", $folder);
                }

                if($major == $submajor && $minor == $subminor){
                    if(isset($subversion)){
                        if($subversion > $latest_version){
                            $latest_version = $subversion;
                        }
                    }
                }
            }

            if($new) {
                $latest_version++;
            }

            $version = $major . '.' . $minor . '.' . $latest_version;
        }

        return $version;
    }

    public function setup_version_folder() {

        $package = $_FILES['course_package'];
        $version_dir = $this->version_sub . '/';
        folder_permit($version_dir);
        $tmp_dir = $version_dir . 'tmp/';
        folder_permit($tmp_dir);

        // unzip package into temp folder
        if(is_dir($tmp_dir) && is_writable($tmp_dir)) {

            $zip = new ZipArchive;
            if($zip->open($package['tmp_name']) === TRUE) {
                $zip->extractTo($tmp_dir);
                $zip->close();
            }
            else {
                $this->alert('warning', 'Unable to extract version files.');
                redirect($this->uri->uri_string());
            }
        }
        else {
            $this->alert('warning', 'Unable to create version folder.');
            redirect($this->uri->uri_string());
        }

        // copy temp files to version folder
        if(is_dir($version_dir) && is_writable($version_dir)) {

            $css_dir = $tmp_dir . 'app/css/';
            $js_dir = $tmp_dir . 'public/js/';
            $chameleon_json = $tmp_dir . 'chameleon.json';

            if(is_file($chameleon_json)) {
                copy($chameleon_json, $version_dir . 'chameleon.json');
            }

            if(is_dir($css_dir)) {

                $vendor_css = $tmp_dir . 'public/css/vendor.css';
                chmod($vendor_css, 0777);
                if(is_file($vendor_css)) {
                    copy($vendor_css, $css_dir . '/vendor.css');
                }

                // need to remove _chameleon_variables.less calls from less files
                function remove_chameleon_less($path) {
                    $content = file_get_contents($path);
                    $content = str_replace('@import "../_chameleon_variables.less";', '', $content);
                    file_put_contents($path, $content);
                }

                remove_chameleon_less($css_dir . 'chameleon/chameleon.less');

                copy_files($css_dir, $version_dir . 'css/');
            }

            if(is_dir($js_dir)) {
                copy_files($js_dir, $version_dir . 'js/');
            }

            // remove tmp directory
            delete_folder($tmp_dir);

            // copy to S3
            $version_path = $this->core_folder_path();
            $this->aws->add_directory($version_dir, $version_path);

            // delete version folder
            delete_folder($version_dir);
        }
        else {
            $this->alert('warning', 'Unable to copy version files.');
            redirect($this->uri->uri_string());
        }

        return true;
    }
}

?>
