<?php

class Topic_model extends CI_Model {

	/**
	 * Topic belongs_to a course, so this is the course id it belongs,
	 * and it's UUID.
	 */
	public $course_id;
	public $uuid;

	/**
	 * Data in a Topic
	 */
	public $data; //<< Data, minus the uuid and page array

    function __construct()
    {
        parent::__construct();

        $this->load->model('Course_model');
        $this->data = array();
        $this->pages = array();
    }

 	/**
 	 * Builds a new Topic.
 	 */
  	public function build($course) {
  		$this->course_id = $course->course_id;
  		$this->data = array();
  		$this->ensure_has_uuid();
      return $this;
  	}

  /**
   * Find topic group.
   */
    public function find_topic_group($course) {
      $data = $course->load_json();
      $topic = $data['topic'];
      if (!is_array($topic)) {
        return null;
      }
      $this->course_id = $course->course_id;
      return $this->from_ar($topic);
    }

  	/**
  	 * Finds a topic by uuid from a course.
  	 */
  	public function find($course, $uuid) {
  		$data = $course->load_json();
  		if (!is_array($data['topic'])) {
  			return null;
  		}
  		foreach ($data['topic'] as $topic_ar) {
  			if (!is_array($topic_ar)) {
  				continue;
  			}
  			if ($topic_ar['uuid'] === $uuid) {
  				$this->course_id = $course->course_id;
  				return $this->from_ar($topic_ar);
  			}
  		}
  		return null;
  	}

  	/**
  	 * Loads a model from an array.
  	 */
  	public function from_ar($ar) {
  		if (!is_array($ar)) {
  			return null;
  		}
  		if (array_key_exists('uuid', $ar)) {
  			$this->uuid = $ar['uuid'];
  			unset($ar['uuid']);
  		}
  		unset($ar['page']);
  		$this->data = array_replace($this->data, $ar);
  		$this->ensure_has_uuid();
  		return $this;
  	}

  	/**
  	 * Gets the current page array from the JSON file.
  	 */
  	private function page_ar() {
  		$course = Course_model::get_by_id($this->course_id);
  		$course_data = $course->load_json();
  		// Find the topic
  		foreach ($course_data['topic'] as $topic_ar) {
  			if ($topic_ar['uuid'] === $this->uuid) {
  				return $topic_ar['page'];
  			}
  		}
		return array();
  	}

  	/**
  	 * Gets an array of Topicpage_model objects which represent the pages in this
  	 * Topic.
  	 */
  	public function pages() {
  		$pages = $this->page_ar();
  		$pages_objs = array();
  		foreach ($pages as $page_ar) {
  			$this->load->model('Topicpage_model');
  			$page_obj = new $this->Topicpage_model;
  			$page_obj->from_ar($page_ar);
  			$pages_objs[] = $page_obj;
  		}
  		return $pages_objs;
  	}

  	/**
  	 * Makes an array suitable for saving to the JSON, or using in a form.
  	 */
  	public function to_ar() {
  		$result = $this->data;
  		$result['uuid'] = $this->uuid;
  		return $result;
  	}

  	/**
  	 * Saves this object to the JSON file.
  	 * - if uuid exists, replace it, with the same pages that are currently in the JSON file
  	 * - if not, add it to the end of the topics in this course.
  	 */
  	public function save() {
  		$json_ar = $this->to_ar();
  		$course = Course_model::get_by_id($this->course_id);
  		$coursedata = $course->load_json();
  		for ($topicidx = 0; $topicidx < count($coursedata['topic']); $topicidx++) {
  			$topic = $coursedata['topic'][$topicidx];
  			if (array_key_exists('uuid', $topic) && $topic['uuid'] === $this->uuid) {
  				$json_ar['page'] = $topic['page'];
  				break;
  			}
  		}
  		if (!array_key_exists('page', $json_ar)) {
  			$json_ar['page'] = array();
  		}
  		$coursedata['topic'][$topicidx] = $json_ar;
  		return $course->save_json($coursedata);
  	}

  	/**
  	 * Updates this topic, and saves the changes to the JSON-backed file.
  	 */
  	public function update($attributes) {
  		$this->from_ar($attributes);
  		return $this->save();
  	}

    /**
     * Deletes this topic.
     */
    public function delete() {
      $course = Course_model::get_by_id($this->course_id);
      $coursedata = $course->load_json();
      for ($topicidx = 0; $topicidx < count($coursedata['topic']); $topicidx++) {
        if ($coursedata['topic'][$topicidx]['uuid'] === $this->uuid) {
          unset($coursedata['topic'][$topicidx]);
          $coursedata['topic'] = array_values($coursedata['topic']);
          return $course->save_json($coursedata);
        }
      }
      return false;
    }

  	/**
  	 * Sets the uuid for this model if it doesn't exist.
  	 */
  	public function ensure_has_uuid() {
	    // Make a uuid if there is none
	    if (empty($this->uuid)) {
	      $this->load->helper('uuid');
	      $this->uuid = uuid_v4();
	    }
  	}
}

?>
