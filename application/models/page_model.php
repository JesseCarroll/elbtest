<?php

class Page_model extends CI_Model {

    // table name
    private $tablename = 'leads';
    private $primarykey = 'id';

    function __construct()
    {
        parent::__construct();
    }

    // add new record
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }

}

?>
