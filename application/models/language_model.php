<?php

class Language_model extends CI_Model {

    // table name
    private $tablename = 'languages';
    private $primarykey = 'language_id';

    function __construct()
    {
        parent::__construct();
    }
    
    // get number of records in database
    function count_all(){
        return $this->db->count_all($this->tablename);
    }
    
    // get records with paging
    function get_all($limit = 200, $offset = 0){
        $this->db->order_by($this->primarykey,'desc');
        $query = $this->db->get($this->tablename, $limit, $offset);
        return $query->result();
    }
    
    // get record by id
    function get_by_id($id = 0){
        $this->db->where($this->primarykey, $id);
        $query = $this->db->get($this->tablename);
        return $query->row();
    }
    
    // add new record
    function insert($data){
        $this->db->insert($this->tablename, $data);
        return $this->db->insert_id();
    }
    
    // update record by id
    function update($id = 0, $data){
        $this->db->where($this->primarykey, $id);
        $this->db->update($this->tablename, $data);
    }
    
    // delete record by id
    function delete($id = 0){
        $this->db->where($this->primarykey, $id);
        $this->db->delete($this->tablename);
    }
    
    // get all active records
    function get_all_active(){
        $where = array('language_status' => '1');
        $this->db->where($where);
        $this->db->order_by('language_shortcode', 'asc');
        $query = $this->db->get($this->tablename);
        return $query->result();
    }
	
	// get columns data.
    function get_columns($columns = 'language_shortcode,language_description', $language_shortcode = 0, $language_id = 0){
		$this->db->select($columns);
		if(!empty($language_shortcode)) {
			$this->db->where('language_shortcode', $language_shortcode);
		}
		if(!empty($version_id)) {
			$this->db->where($this->primarykey, $language_id);
		}        		
        $query = $this->db->get($this->tablename);
        $result = $query->row(0, get_class($this));
        return $result;
    }

    // Get a long description from a shortcode.
    function description_from_shortcode($shortcode = 0) {
        if (empty($shortcode)) {
            error_log("missing $shortcode argument for description_from_shortcode");
            return ''; 
        }
        return $this->get_columns('language_description', $shortcode)->language_description;
    }
}

?>
