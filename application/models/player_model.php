<?php

class Player_model extends CI_Model {

	# courseware player user table
	private $user_table = 'users';

	# courseware player usermap table
	private $usermap_table = 'player_usermap';

	# courseware player usertoken table
	private $usertoken_table = 'usertoken';

	function __construct() {
		parent::__construct();
	}

	# save users to courseware user table (player_users)
	function save_user($data) {
		$this->db->insert($this->user_table, $data);
		return $this->db->insert_id();
	}

	# return user record by email address
	function get_user_by_email($email) {
		$this->db->where('email', $email);
		$query = $this->db->get($this->user_table);
		return $query->row();
	}

	# map user to assigned curriculum(s) (player_usermap)
	function usermap($data) {
		$this->db->insert($this->usermap_table, $data);
		return $this->db->insert_id();
	}

	# get curriculum id by user id in the usermap table (player_usermap)
	function get_curriculum($user_id) {
		$this->db->select('cml_player_usermap.curriculum, cml_curriculum.curriculum_status');
		$this->db->join('cml_curriculum', 'cml_player_usermap.curriculum = cml_curriculum.curriculum_id');
		$this->db->where('cml_player_usermap.user_id', $user_id);
		$query = $this->db->get($this->usermap_table);
		# return an array because a user can have access to more than one curriculum
		return $query->result_array();
	}

	# get usertoken by user id
	function get_usertoken($user_id) {
		$this->db->where('user_id', $user_id);
		$query = $this->db->get($this->usertoken_table);
		return $query->row();
	}

	# adds a new user with a generated token and expiration key
	function save_usertoken($data) {
		$this->db->where('user_id', $data['user_id']);
		$query = $this->db->insert($this->usertoken_table, $data);
	}

	# updates and existing user record with a new token
	function update_usertoken($data) {
		$this->db->where('user_id', $data['user_id']);
		$query = $this->db->update($this->usertoken_table, $data);
	}
}

?>