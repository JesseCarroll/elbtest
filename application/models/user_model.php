<?php

class User_model extends CI_Model implements JsonSerializable {
	const HASHED_PASSWORD_PLACEHOLDER = '___HASHED_PASSWORD_PLACEHOLDER___';

	const STATUS_INACTIVE = '0';
	const STATUS_ACTIVE = '1';
	const STATUS_DELETED = '2';

	private static $db;
	private static $session;
	private static $encrypt;

	# table name
	private static $tablename = 'users';
	private static $primarykey = 'user_id';

	function __construct() {
		parent::__construct();
		self::$db = &get_instance()->db;
		self::$session = &get_instance()->session;
		self::$encrypt = &get_instance()->encrypt;

		# These constants may be changed without breaking existing hashes.
		if (!defined("PBKDF2_HASH_ALGORITHM")) {
			define("PBKDF2_HASH_ALGORITHM", "sha256");
		}
		if (!defined("PBKDF2_ITERATIONS")) {
			define("PBKDF2_ITERATIONS", 1000);
		}
		if (!defined("PBKDF2_SALT_BYTE_SIZE")) {
			define("PBKDF2_SALT_BYTE_SIZE", 24);
		}
		if (!defined("PBKDF2_HASH_BYTE_SIZE")) {
			define("PBKDF2_HASH_BYTE_SIZE", 24);
		}
		if (!defined("HASH_SECTIONS")) {
			define("HASH_SECTIONS", 4);
		}
		if (!defined("HASH_ALGORITHM_INDEX")) {
			define("HASH_ALGORITHM_INDEX", 0);
		}
		if (!defined("HASH_ITERATION_INDEX")) {
			define("HASH_ITERATION_INDEX", 1);
		}
		if (!defined("HASH_SALT_INDEX")) {
			define("HASH_SALT_INDEX", 2);
		}
		if (!defined("HASH_PBKDF2_INDEX")) {
			define("HASH_PBKDF2_INDEX", 3);
		}
	}

	public function jsonSerialize() {
		$ret = new stdClass;
		foreach (array(
			'user_id',
			'client_id',
			'first_name',
			'last_name',
			'email',
			'super_admin',
			'user_status',
			'login_status',
			'date_created',
			'date_updated',
		) as $serializable_property_name) {
			if (!property_exists($this, $serializable_property_name)) {
				continue;
			}
			$ret->$serializable_property_name = $this->$serializable_property_name;
		}
		return $ret;
	}

	public static function create_hash($password) {
		# format: algorithm:iterations:salt:hash
		$salt = base64_encode(mcrypt_create_iv(PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
		return PBKDF2_HASH_ALGORITHM . ":" . PBKDF2_ITERATIONS . ":" .  $salt . ":" . base64_encode(self::pbkdf2(
			PBKDF2_HASH_ALGORITHM,
			$password,
			$salt,
			PBKDF2_ITERATIONS,
			PBKDF2_HASH_BYTE_SIZE,
			true
		));
	}

	private static function validate_password($password, $correct_hash) {
		$params = explode(":", $correct_hash);
		if (count($params) < HASH_SECTIONS)
			return false;
		$pbkdf2 = base64_decode($params[HASH_PBKDF2_INDEX]);
		return self::slow_equals(
			$pbkdf2,
			self::pbkdf2(
				$params[HASH_ALGORITHM_INDEX],
				$password,
				$params[HASH_SALT_INDEX],
				(int)$params[HASH_ITERATION_INDEX],
				strlen($pbkdf2),
				true
			)
		);
	}

	# Compares two strings $a and $b in length-constant time.
	private static function slow_equals($a, $b) {
		$diff = strlen($a) ^ strlen($b);
		for ($i = 0; $i < strlen($a) && $i < strlen($b); $i++) {
			$diff |= ord($a[$i]) ^ ord($b[$i]);
		}
		return $diff === 0;
	}

	/*
	 * PBKDF2 key derivation function as defined by RSA's PKCS #5: https:#www.ietf.org/rfc/rfc2898.txt
	 * $algorithm - The hash algorithm to use. Recommended: SHA256
	 * $password - The password.
	 * $salt - A salt that is unique to the password.
	 * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
	 * $key_length - The length of the derived key in bytes.
	 * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
	 * Returns: A $key_length-byte key derived from the password and salt.
	 *
	 * Test vectors can be found here: https:#www.ietf.org/rfc/rfc6070.txt
	 *
	 * This implementation of PBKDF2 was originally created by https:#defuse.ca
	 * With improvements by http:#www.variations-of-shadow.com
	 */
	private static function pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false) {

		$algorithm = strtolower($algorithm);
		if (!in_array($algorithm, hash_algos(), true)) {
			trigger_error('PBKDF2 ERROR: Invalid hash algorithm.', E_USER_ERROR);
		}
		if ($count <= 0 || $key_length <= 0) {
			trigger_error('PBKDF2 ERROR: Invalid parameters.', E_USER_ERROR);
		}

		if (function_exists("hash_pbkdf2")) {
			# The output length is in NIBBLES (4-bits) if $raw_output is false!
			if (!$raw_output) {
				$key_length = $key_length * 2;
			}
			return hash_pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output);
		}

		$hash_length = strlen(hash($algorithm, "", true));
		$block_count = ceil($key_length / $hash_length);

		$output = "";
		for ($i = 1; $i <= $block_count; $i++) {
			# $i encoded as 4 bytes, big endian.
			$last = $salt . pack("N", $i);
			# first iteration
			$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
			# perform the other $count - 1 iterations
			for ($j = 1; $j < $count; $j++) {
				$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
			}
			$output .= $xorsum;
		}

		if ($raw_output) {
			return substr($output, 0, $key_length);
		} else {
			return bin2hex(substr($output, 0, $key_length));
		}
	}

	public static function login($email = '', $password = '') {
		self::$db->where(array(
			'email' => $email,
			'user_status' => self::STATUS_ACTIVE,
		));
		$query = self::$db->get(self::$tablename);

		if ($query->num_rows() > 0) {
			$row = $query->row_array();

			if (empty($row['password'])) {
				if (self::validate_password($password, $row['hash'])) {
					return $row;
				}
			} else {
				$db_password = self::$encrypt->decode($row['password']);
				if ($password == $db_password) {
					return $row;
				}
			}
		}
		return array();
	}

	# get number of users in database
	public static function count_all() {
		return self::$db->count_all(self::$tablename);
	}

	# get records with paging
	public static function get_all($limit = 200, $offset = 0) {
		self::$db->order_by(self::$primarykey, 'desc');
		$query = self::$db->get(self::$tablename);
		return $query->result();
	}

	# get a user by ID
	public static function get_by_id($id) {
		self::$db->where(self::$primarykey, $id);
		$query = self::$db->get(self::$tablename);
		return $query->row(0, __CLASS__);
	}

	# get a user by email address
	public static function get_by_email($email) {
		self::$db->where('email', $email);
		$query = self::$db->get(self::$tablename);
		if (0 == $query->num_rows()) {
			throw new Exception("No user with address '$email' found");
		}
		return $query->row(0, __CLASS__);
	}

	# get all users having the specified client ID
	public static function get_by_client($client_id) {
		self::$db->where('client_id', $client_id);
		$query = self::$db->get(self::$tablename);
		$ret = array();
		foreach ($query->result(__CLASS__) as $user) {
			$ret[] = $user;
		}
		return $ret;
	}

	# get all users assigned to the specified group
	public static function get_by_group($group_id) {
		self::$db->select('users.*');
		self::$db->join('user_group', self::$tablename . '.user_id = user_group.user_id');
		self::$db->where('group_id', $group_id);
		$query = self::$db->get(self::$tablename);
		$ret = array();
		foreach ($query->result(__CLASS__) as $user) {
			$ret[] = $user;
		}
		return $ret;
	}

	# checks if the provided email address would violate the uniqueness
	# constraint on the email field in the user table if we attempt to insert
	# the given value; if a user ID is provided, we exclude the record
	# corresponding to that ID from consideration
	public static function is_unique_user_email($email, $id = null) {
		self::$db->where('email', $email);
		if (null !== $id) {
			self::$db->where_not_in('user_id', $id);
		}
		return (0 === self::$db->get(self::$tablename)->num_rows());
	}

	# add/update user
	public static function add_update($data, $user_id = null) {
		# convert password to a hash
		if (isset($data['password'])) {
			if (0 !== strcmp(self::HASHED_PASSWORD_PLACEHOLDER, $data['password'])) {
				$data['hash'] = self::create_hash($data['password']);
			}
			$data['password'] = '';
		}

		# break group associations out because we don't want to try to save
		# them as part of the user record
		if (isset($data['groups'])) {
			$groups = (false === $data['groups']) ? array() : $data['groups'];
			unset($data['groups']);
		}

		# break course and client roles out because we don't want to try to
		# save them as part of the user record
		foreach (array('course', 'client') as $course_or_client) {
			if (!isset($data["{$course_or_client}_roles"])) {
				continue;
			}
			${"{$course_or_client}_roles"} = $data["{$course_or_client}_roles"];
			unset($data["{$course_or_client}_roles"]);
		}

		# check if the user provided by the $user_id argument exists, telling
		# us whether we're creating a new user or updating a user
		$is_existing_user = false;
		if (null !== $user_id) {
			self::$db->where(self::$primarykey, $user_id);
			$query = self::$db->get(self::$tablename);
			if (0 == self::$db->get(self::$tablename)->num_rows()) {
				throw new Exception("Could not find user with ID '$user_id'");
			}
			$is_existing_user = true;
		}

		# start a transaction so we can atomically update the user and update
		# all related tables -- either all changes succeed, or none of them do
		self::$db->trans_start();

		# add or update the user record
		if ($is_existing_user) {
			self::$db->where('user_id', $user_id);
			self::$db->update(self::$tablename, $data);
		} else {
			self::$db->insert(self::$tablename, $data);
			$user_id = self::$db->insert_id();
		}
		
		# TODO: update manager assignment
		# 
		
		# super admins don't need to be part of groups because they can do everything
		if (!empty($data['super_admin'])) {
			$sql = sprintf('DELETE FROM %1$s WHERE user_id = ?', self::$db->dbprefix('user_group'));
			self::$db->query($sql, array($user_id));
		}

		# update group associations
		if (isset($groups) && is_array($groups)) {
			# delete existing associations
			$sql = sprintf('DELETE FROM %1$s WHERE user_id = ?', self::$db->dbprefix('user_group'));
			self::$db->query($sql, array($user_id));

			# insert new associations
			$sql = sprintf('INSERT INTO %1$s (user_id, group_id) VALUES (?, ?)', self::$db->dbprefix('user_group'));
			foreach ($groups as $group_id) {
				if (empty($group_id)) {
					continue;
				}
				self::$db->query($sql, array($user_id, $group_id));
			}
		}

		# update role assignments for courses and clients
		foreach (array('course', 'client') as $course_or_client) {
			# super admins don't need any associations recorded since they can
			# do everything
			if (!empty($data['super_admin'])) {
				$sql = sprintf('DELETE FROM %1$s WHERE user_id = ?', self::$db->dbprefix("user_$course_or_client"));
				self::$db->query($sql, array($user_id));
				continue;
			}

			# sanity check -- if we did not receive any associations, the call
			# may not wish to update role associations or there might be
			# something wrong with the request and we should abort instead of
			# proceeding with clearing out and repopulating role associations
			if (!isset(${"{$course_or_client}_roles"}) || !is_array(${"{$course_or_client}_roles"})) {
				continue;
			}

			# delete existing associations
			$sql = sprintf('DELETE FROM %1$s WHERE user_id = ?', self::$db->dbprefix("user_$course_or_client"));
			self::$db->query($sql, array($user_id));

			# insert new associations
			$sql = sprintf('INSERT INTO %1$s (user_id, %2$s_id, role_id)
				VALUES (?, ?, ?)',
				self::$db->dbprefix("user_$course_or_client"),
				$course_or_client,
				self::$db->dbprefix('roles')
			);
			foreach (${"{$course_or_client}_roles"} as $course_or_client_id => $role_id) {
				if (empty($role_id)) {
					continue;
				}
				self::$db->query($sql, array($user_id, $course_or_client_id, $role_id));
			}
		}

		# complete the transaction
		self::$db->trans_complete();

		return $user_id;
	}

	public static function get_logged_in_user() {
		$user_id = self::$session->userdata('user_id');
		if (empty($user_id)) {
			return false;
		}
		return self::get_by_id($user_id);
	}

	# delete record by id
	public static function delete($id = 0) {
		self::$db->trans_start();
		self::$db->where(self::$primarykey, $id);
		self::$db->delete(self::$tablename);
		foreach (array('course', 'client') as $type) {
			self::$db->where('user_id', $id);
			self::$db->delete(self::$db->dbprefix("user_$type"));
		}
		self::$db->trans_complete();
	}

	# get record by where
	public static function get_where($where = array(), $limit = 1, $offset = 0) {
		$query = self::$db->get_where(self::$tablename, $where, $limit, $offset);
		//return $query->result();
		return $query->row(0, __CLASS__);
	}

	# return a list of groups this user belongs to
	public function get_groups() {
		self::$db->where('user_id', $this->user_id);
		$query = self::$db->get('user_group');
		$ret = array();
		foreach ($query->result() as $result) {
			$ret[] = $result->group_id;
		}
		return $ret;
	}

	# return an array associating course IDs with role IDs;
	# if the user has no role relationship with a course,
	# that course is not mentioned in the results
	public function get_course_roles() {
		if (!property_exists($this, 'course_roles')) {
			$this->refresh_course_client_roles();
		}
		return $this->course_roles;
	}

	# return an array associating client IDs with role IDs;
	# if the user has no role relationship with a client,
	# that client is not mentioned in the results
	public function get_client_roles() {
		if (!property_exists($this, 'client_roles')) {
			$this->refresh_course_client_roles();
		}
		return $this->client_roles;
	}

	# worker method for get_client_roles() and get_course_roles()
	private function refresh_course_client_roles() {
		$this->course_roles = array();
		$this->client_roles = array();

		foreach (array('course', 'client') as $course_or_client) {
			self::$db->where('user_id', $this->user_id);
			$query = self::$db->get("user_{$course_or_client}");
			foreach ($query->result() as $result) {
				$this->{"{$course_or_client}_roles"}[$result->{"{$course_or_client}_id"}] = $result->role_id;
			}
		}
	}

	# return a list of course models for courses with which this user has any
	# role relationship
	public function get_allowed_courses() {
		if (!property_exists($this, 'allowed_courses')) {
			$this->refresh_course_client_lists();
		}
		return $this->allowed_courses;
	}

	# return a list of client models for clients with which this user has any
	# role relationship
	public function get_allowed_clients() {
		if (!property_exists($this, 'allowed_clients')) {
			$this->refresh_course_client_lists();
		}
		return $this->allowed_clients;
	}


	public function refresh_course_client_lists() {
		$this->allowed_courses = array();
		$this->allowed_clients = array();

		$this->load->model('Client_model');
		$this->load->model('Course_model');
		foreach (Course_model::get_all_active() as $course) {
			if ('translate' == $course->state) {
				if (!($this->can('translate', $course->client_id, $course->course_id) || $this->can('list_courses', $course->client_id, $course->course_id))
				) {
					continue;
				}
			}
			else if (!$this->can('list_courses', $course->client_id, $course->course_id)) {
				continue;
			}
			if (!isset($this->allowed_clients[$course->client_id])) {
				$this->allowed_clients[$course->client_id] = Client_model::get_by_id($course->client_id);
			}
			if (!isset($this->allowed_courses[$course->course_id])) {
				$this->allowed_courses[$course->course_id] = $course;
			}
		}
		foreach (Client_model::get_all() as $client) {
			if ($this->can('list_client', $client->client_id)) {
				$this->allowed_clients[$client->client_id] = $client;
			}
		}
		$this->allowed_clients = array_values($this->allowed_clients);
		$this->allowed_courses = array_values($this->allowed_courses);
	}

	public function display_name() {
		return $this->first_name . ' ' . $this->last_name;
	}

	/**
	 * Permissions super-function.
	 * Returns TRUE if the user has permission to perform the specified action.
	 * Returns FALSE otherwise
	 */
	function can($permission_name, $client_id = null, $course_id = null) {
		$allowed = array();

		# super admins can do everything
		if ($this->super_admin) {
			return true;
		}

		# a user who is not a super admin assumes roles with respect to a
		# certain course/client, so if no course/client was provided, this
		# user is not granted any permissions
		if (empty($client_id) && empty($course_id)) {
			return false;
		}

		# determine what role this user has in relationship to this
		# client/course; we use a method from the role model that helps us
		# find the "strongest" role that the user has from any of its course,
		# client and group connections, but it is important to note that the
		# strength of each role is subjective and is defined by the role model
		$role = null;
		$this->load->model('Role_model');
		if (!empty($client_id)) {
			# roles acquired by direct relationship with the client
			self::$db->join('user_client', 'user_client.role_id = roles.role_id');
			self::$db->where('user_client.user_id', $this->user_id);
			self::$db->where('user_client.client_id', $client_id);
			$query = self::$db->get('roles');
			foreach ($query->result() as $row) {
				$role = Role_model::max_role($role, $row->role);
			}

			# roles acquired by being a member of a group that has a relationship with the client
			self::$db->join('group_client', 'group_client.role_id = roles.role_id');
			self::$db->join('user_group', 'group_client.group_id = user_group.group_id');
			self::$db->where('user_id', $this->user_id);
			self::$db->where('client_id', $client_id);
			$query = self::$db->get('roles');
			foreach ($query->result() as $row) {
				$role = Role_model::max_role($role, $row->role);
			}
		}
		if (!empty($course_id)) {
			# roles acquired by direct relationship with the course
			self::$db->join('user_course', 'user_course.role_id = roles.role_id');
			self::$db->where('user_course.user_id', $this->user_id);
			self::$db->where('user_course.course_id', $course_id);
			$query = self::$db->get('roles');
			foreach ($query->result() as $row) {
				$role = Role_model::max_role($role, $row->role);
			}

			# roles acquired by being a member of a group that has a relationship with the course
			self::$db->join('group_course', 'group_course.role_id = roles.role_id');
			self::$db->join('user_group', 'group_course.group_id = user_group.group_id');
			self::$db->where('user_id', $this->user_id);
			self::$db->where('course_id', $course_id);
			$query = self::$db->get('roles');
			foreach ($query->result() as $row) {
				$role = Role_model::max_role($role, $row->role);
			}
		}
		if (null === $role) {
			return false;
		}

		# determine allowed actions based on the user's strongest role
		switch ($role) {
		case 'CA': # admin
			$allowed = array('list_clients', 'list_courses', 'add_course', 'add_curriculum', 'customize_theme', 'delete_course', 'download_course', 'edit_all_courses', 'stage', 'translate', 'edit_course');
			break;
		case 'ED': # editor
			$allowed = array('list_courses', 'translate', 'edit_all_courses', 'edit_course');
			break;
		case 'TR': # translator
			//$allowed = array('list_courses', 'edit_all_courses', 'translate');
			$allowed = array('translate');
			break;
		default:
			$allowed = array();
		}

		if (in_array($permission_name, $allowed)){
			return true;
		}

		# Default deny.
		return false;
	}
}
