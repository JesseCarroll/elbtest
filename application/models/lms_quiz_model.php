<?php

class Lms_quiz_model extends CI_Model {

    // table name
    private $tablename = 'lms_quiz';
    private $primarykey = 'id';

    /**
     * Adds record with new data.
     * @return new user id
     */
    function insert($data){
        $this->db->insert($this->tablename, $data);
    }

}
?>