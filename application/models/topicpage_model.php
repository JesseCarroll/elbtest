<?php

/**
 * JSON-backed topicpage model.
 * This is JSON-data independent, it depends on the controller to structure it correctly.
 *
 *  arrays are arrays of associative arrays for now.
 */
class Topicpage_model extends CI_model {
  /**
   * These two items uniquely locate the topicpage within a course data file and topic.
   */
  public $course_id;
  public $uuid;
  /**
   * A third, topic_uuid is needed when the page is new or is being moved from one ID to another
   */
  public $topic_uuid;

  /**
   * Page data
   */
  public $data; //< Data, minus the uuid.

  /**
   * Generic constructor.  Doesn't tie this page to any particular course, or topic.
   * Both of those are needed before it can be update()d or save()d
   */
  function __construct() {
    parent::__construct();
    $this->data = array();
  }

  public function build($course, $topic_uuid) {
    $this->load->model('Topic_model');
    $topic = $this->Topic_model->find($course, $topic_uuid);
    if (empty($topic)) {
      return null;
    }
    $this->course_id = $course->course_id;
    $this->topic_uuid = $topic->uuid;
    $this->ensure_has_uuid();
    return $this;
  }


  /**
   * Finds a page by uuid from a course.
   */
  public function find($course, $uuid) {
    $data = $course->load_json();
    if (!is_array($data['topic'])) {
      return null;
    }
    foreach ($data['topic'] as $topic_ar) {
      if (!is_array($topic_ar)) {
        continue;
      }
      foreach ($topic_ar['page'] as $page_ar) {
        if (!is_array($page_ar)) {
          continue;
        }
        if ($page_ar['uuid'] === $uuid) {
          $this->course_id = $course->course_id;
          $this->topic_uuid = $topic_ar['uuid'];
          return $this->from_ar($page_ar);
        }
      }
    }
    return null;
  }

  /**
   * Loads or updates a model given an array.
   */
  public function from_ar($ar) {
    if (!is_array($ar)) {
      return null;
    }
    if (array_key_exists('uuid', $ar)) {
      $this->uuid = $ar['uuid'];
      unset($ar['uuid']);
    }
    $this->data = array_replace($this->data, $ar);
    $this->ensure_has_uuid();
    return $this;
  }

  /**
   * Distills the data stored on this object to the array to be
   * stored in the JSON.
   */
  public function to_ar() {
    $result = $this->data;
    $result['uuid'] = $this->uuid;
    return $result;
  }


  private static function find_page_indices($coursedata, $page_uuid, $topic_uuid = null) {
    $this_topic = false;
    for ($topicidx = 0; $topicidx < count($coursedata['topic']); $topicidx++) {
      $topic = $coursedata['topic'][$topicidx];
      $this_topic = array_key_exists('uuid', $topic)
                  && $topic['uuid'] === $topic_uuid;
      for ($pageidx = 0; $pageidx < count($topic['page']); $pageidx++) {
        if ($coursedata['topic'][$topicidx]['page'][$pageidx]['uuid'] === $page_uuid) {
          return array($topicidx, $pageidx);
        }
      }
      if ($this_topic) {
        return array($topicidx, count($topic['page']));
      }
    }
    return array(null, null);
  }

  /**
   * Saves this object to the JSON file.
   * - if the uuid exists, replaces it.
   * - if not, will add a page to the end of the topic specified by $topic_uuid
   */
  public function save() {
    $json_ar = $this->to_ar();
    $this->load->model('Course_model');
    $course = Course_model::get_by_id($this->course_id);
    $coursedata = $course->load_json();
    list($topicidx, $pageidx) = $this->find_page_indices($coursedata, $this->uuid, $this->topic_uuid);
    if (!is_null($topicidx) && !is_null($pageidx)) {
      $coursedata['topic'][$topicidx]['page'][$pageidx] = $json_ar;
      return $course->save_json($coursedata);
    }
    return false;
  }

  /**
   * Updates this page, including persisting the changes to the JSON-backed file.
   */
  public function update($attributes) {
    $this->from_ar($attributes);
    return $this->save($attributes);
  }

  /**
   * Delete this page.
   */
  public function delete() {
    $this->load->model('Course_model');
    $course = Course_model::get_by_id($this->course_id);
    $coursedata = $course->load_json();
    list($topicidx, $pageidx) = $this->find_page_indices($coursedata, $this->uuid);
    if (!is_null($topicidx) && !is_null($pageidx)) {
      unset($coursedata['topic'][$topicidx]['page'][$pageidx]);
      $coursedata['topic'][$topicidx]['page'] = array_values($coursedata['topic'][$topicidx]['page']);
      return $course->save_json($coursedata);
    }
    return false;
  }

  public static function get_page_ord($course, $page_uuid) {
    $coursedata = $course->load_course_json();
    return Topicpage_model::find_page_indices($coursedata, $page_uuid);
  }

  private function ensure_has_uuid() {
    // Make a uuid if there is none
    if (empty($this->uuid)) {
      $this->load->helper('uuid');
      $this->uuid = uuid_v4();
    }
  }
}
