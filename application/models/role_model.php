<?php

class Role_model extends CI_Model {
	const STATUS_INACTIVE = '0';
	const STATUS_ACTIVE = '1';
	const STATUS_DELETED = '2';

	private static $tablename = 'roles';
	private static $primarykey = 'role_id';
	private static $db;

	function __construct() {
		parent::__construct();
		self::$db = &get_instance()->db;
	}

	# get number of records in database
	public static function count_all() {
		return self::$db->count_all(self::$tablename);
	}

	# get records with paging
	public static function get_all($limit = 200, $offset = 0) {
		self::$db->order_by(self::$primarykey,'desc');
		$query = self::$db->get(self::$tablename, $limit, $offset);
		return $query->result(__CLASS__);
	}

	# get record by id
	public static function get_by_id($id = 0) {
		self::$db->where(self::$primarykey, $id);
		$query = self::$db->get(self::$tablename);
		return $query->row(0, __CLASS__);
	}

	# add new record
	public static function insert($data) {
		self::$db->insert(self::$tablename, $data);
		return self::$db->insert_id();
	}

	# update record by id
	public static function update($id = 0, $data) {
		self::$db->where(self::$primarykey, $id);
		self::$db->update(self::$tablename, $data);
	}

	# delete record by id
	public static function delete($id = 0) {
		self::$db->where(self::$primarykey, $id);
		self::$db->delete(self::$tablename);
	}

	# get all active records
	public static function get_all_active() {
		$where = array('role_status' => '1');
		self::$db->where($where);
		self::$db->order_by('role_title', 'asc');
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# checks if the provided role code would violate the uniqueness constraint
	# on the role code in the roles table if we attempt to insert the given
	# value; if a role ID is provided, we exclude the record corresponding to
	# that ID from consideration
	public static function is_unique_role_code($code, $id = null) {
		self::$db->where('role', $code);
		if (null !== $id) {
			self::$db->where_not_in('role_id', $id);
		}
		return (0 === self::$db->get(self::$tablename)->num_rows());
	}
	
	# compare the two provided role codes and returns the one that trumps the
	# other role; although roles are stored in a database table, the
	# subjective strength of each role is only defined here
	public static function max_role($role1, $role2) {
		$role_precedence = array(
			'CA',
			'ED',
			'TR',
			'LR',
		);
		$pos1 = array_search($role1, $role_precedence, true);
		$pos2 = array_search($role2, $role_precedence, true);
		if (false === $pos1 && false !== $pos2) {
			return $role2;
		}
		if (false !== $pos1 && false === $pos2) {
			return $role1;
		}
		if (false === $pos1 && false === $pos2) {
			throw new Exception('Neither role code was recognized');
		}
		if ($pos1 >= $pos2) {
			return $role1;
		}
		else {
			return $role2;
		}
	}
}
