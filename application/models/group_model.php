<?php

class Group_model extends CI_Model {
	private static $tablename = 'groups';
	private static $primarykey = 'group_id';
	private static $db;

	function __construct() {
		parent::__construct();
		self::$db = &get_instance()->db;
	}

	# get number of records in database
	public static function count_all() {
		return self::$db->count_all(self::$tablename);
	}

	# get records with paging
	public static function get_all($limit = 200, $offset = 0) {
		self::$db->order_by(self::$primarykey,'desc');
		$query = self::$db->get(self::$tablename, $limit, $offset);
		return $query->result(__CLASS__);
	}

	# get record by id
	public static function get_by_id($id = 0) {
		self::$db->where(self::$primarykey, $id);
		$query = self::$db->get(self::$tablename);
		return $query->row(0, __CLASS__);
	}

	# add new record
	public static function insert($data) {
		self::$db->insert(self::$tablename, $data);
		return self::$db->insert_id();
	}

	# update record by id
	public static function update($id = 0, $data) {
		self::$db->where(self::$primarykey, $id);
		self::$db->update(self::$tablename, $data);
	}

	# add/update group
	public static function add_update($data, $group_id = null) {
		# break course and client roles out because we don't want to try to
		# save them as part of the group record
		foreach (array('course', 'client') as $course_or_client) {
			if (!isset($data["{$course_or_client}_roles"])) {
				continue;
			}
			${"{$course_or_client}_roles"} = $data["{$course_or_client}_roles"];
			unset($data["{$course_or_client}_roles"]);
		}

		# check if the group provided by the $group_id argument exists,
		# telling us whether we're creating a new group or updating a group
		$is_existing_group = false;
		if (null !== $group_id) {
			self::$db->where(self::$primarykey, $group_id);
			$query = self::$db->get(self::$tablename);
			if (0 == self::$db->get(self::$tablename)->num_rows()) {
				throw new Exception("Could not find group with ID '$group_id'");
			}
			$is_existing_group = true;
		}

		# start a transaction so we can atomically update the group and update
		# all related tables -- either all changes succeed, or none of them do
		self::$db->trans_start();

		# add or update the group record
		if ($is_existing_group) {
			self::$db->where('group_id', $group_id);
			self::$db->update(self::$tablename, $data);
		} else {
			self::$db->insert(self::$tablename, $data);
			$group_id = self::$db->insert_id();
		}
		
		# update role assignments for courses and clients
		foreach (array('course', 'client') as $course_or_client) {

			# sanity check -- if we did not receive any associations, the call
			# may not wish to update role associations or there might be
			# something wrong with the request and we should abort instead of
			# proceeding with clearing out and repopulating role associations
			if (!isset(${"{$course_or_client}_roles"}) || !is_array(${"{$course_or_client}_roles"})) {
				continue;
			}

			# delete existing associations
			$sql = sprintf('DELETE FROM %1$s WHERE group_id = ?', self::$db->dbprefix("group_$course_or_client"));
			self::$db->query($sql, array($group_id));

			# insert new associations
			$sql = sprintf('INSERT INTO %1$s (group_id, %2$s_id, role_id)
				VALUES (?, ?, ?)',
				self::$db->dbprefix("group_$course_or_client"),
				$course_or_client,
				self::$db->dbprefix('roles')
			);
			foreach (${"{$course_or_client}_roles"} as $course_or_client_id => $role_id) {
				if (empty($role_id)) {
					continue;
				}
				self::$db->query($sql, array($group_id, $course_or_client_id, $role_id));
			}
		}

		# complete the transaction
		self::$db->trans_complete();

		return $group_id;
	}

	# delete record by id
	public static function delete($id = 0) {
		self::$db->where(self::$primarykey, $id);
		self::$db->delete(self::$tablename);
	}

	# get all active records
	public static function get_all_active() {
		self::$db->order_by('name', 'asc');
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# get all users associated with this group
	public function get_users() {
		$this->load->model('User_model');
		return User_model::get_by_group($this->group_id);
	}

	public function get_course_roles() {
		if (!property_exists($this, 'course_roles')) {
			$this->refresh_course_client_roles();
		}
		return $this->course_roles;
	}

	public function get_client_roles() {
		if (!property_exists($this, 'client_roles')) {
			$this->refresh_course_client_roles();
		}
		return $this->client_roles;
	}

	private function refresh_course_client_roles() {
		$this->course_roles = array();
		$this->client_roles = array();

		foreach (array('course', 'client') as $course_or_client) {
			self::$db->where('group_id', $this->group_id);
			$query = self::$db->get("group_{$course_or_client}");
			foreach ($query->result() as $result) {
				$this->{"{$course_or_client}_roles"}[$result->{"{$course_or_client}_id"}] = $result->role_id;
			}
		}
	}

	# checks if the provided group name would be unique; if a group ID is
	# provided, we exclude the record corresponding to that ID from
	# consideration
	public static function is_unique_group_name($value, $id = null) {
		self::$db->where('name', $value);
		if (null !== $id) {
			self::$db->where_not_in('group_id', $id);
		}
		return (0 === self::$db->get(self::$tablename)->num_rows());
	}
}
