<?php

class Course_model extends CI_Model implements JsonSerializable {
	private static $db;
	private static $aws;

	// table name
	private static $tablename = 'courses';
	private static $primarykey = 'course_id';

	public $course_status = 0;

	function __construct() {
		parent::__construct();
		self::$db = &get_instance()->db;
		self::$aws = &get_instance()->aws;
	}

	public function jsonSerialize() {
		$ret = array();
		foreach (get_object_vars($this) as $k => $v) {
			switch ($k) {
			case 'course_enabled_languages':
				$ret[$k] = json_decode($v);
				break;
			default:
				$ret[$k] = $v;
			}
		}
		return $ret;
	}

	# get all active courses
	public static function get_all_active() {
		self::$db->select('courses.*, curriculum.curriculum_name, users.first_name, users.last_name');
		self::$db->join('curriculum', 'curriculum.curriculum_id = courses.course_curriculum', 'left');
		self::$db->join('users', 'users.user_id = courses.updated_by', 'left');
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# get all courses for the specified client
	public static function get_by_client($client_id) {
		self::$db->select('courses.*, curriculum.curriculum_name');
		self::$db->from('courses');

		/*
		Formerly, if the user was assigned the translator role, we limited the
		query to only return courses in the 'translate' state. However,
		following the user permissions overhaul in CHAM-2353, there is no such
		thing as "being a translator" -- users no longer have one single
		overarching role, but rather have a role with respect to individ
		courses and/or clients. Consequently, we can no longer limit the query
		in the way. If this turns out to be problematic, here are some ideas:

		1) Introduce a new user attribute that is solely responsible for
		filtering visible courses by state.

		2) Put controls on the course list that allow users to filter the
		course list by attributes of their choosing, or at least by course
		state.
		*/

		#if($this->session->userdata['role'] == 'TR') {
		#	self::$db->where('state', 'translate');
		#}

		self::$db->join('curriculum', 'curriculum.curriculum_id = courses.course_curriculum', 'left');

		self::$db->where('courses.client_id', $client_id);
		self::$db->order_by('courses.course_id','desc');

		$query = self::$db->get();
		return $query->result(__CLASS__);
	}

	# get course by ID
	public static function get_by_id($id) {
		self::$db->where(self::$primarykey, $id);
		$query = self::$db->get(self::$tablename);
		return $query->row(0, __CLASS__);
	}

	# get courses by curriculum
	public static function get_by_curriculum($curriculum) {
		self::$db->select('cml_courses.*, cml_player_order.course_order');
		self::$db->join('cml_player_order','cml_courses.course_id = cml_player_order.course_id');
		self::$db->where('course_curriculum', $curriculum);
		# make sure we are sorting the returned courses by any custom ordering that has been entered
		self::$db->order_by('cml_player_order.course_order', 'desc');
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# add a new course
	public static function insert($data) {
		self::$db->insert(self::$tablename, $data);
		$model = self::get_by_id(self::$db->insert_id());
		return $model;
	}

	# create a new course, and returns the new course object.
	public static function create($initial_data) {
		return self::insert($initial_data);
	}

	/**
	 * Updates the current object with the new data.
	 * @return a new object with the new data updated, or null on error
	 */
	function update($data) {
		$data->course_theme = null;
		$res = self::$db->update(self::$tablename, $data, array(self::$primarykey => $this->course_id));
		if ($res) {
			// Refresh
			return $this->get_by_id($this->course_id);
		}
		return null;
	}

	/**
	 * delete : deletes course record, and associated folder(s)
	 * @param id {integer} the course id, used to delete record in database
	 * @param admin {boolean} if user is super admin course will be deleted f'real
	 * @return true for no error. TODO
	 */
	function delete($id = 0, $admin) {
		return; # (not used anyway, yet)
		/*
		if ($admin) {
			// completely and irreversably delete course
			self::$db->where(self::$primarykey, $id);
			self::$db->delete(self::$tablename);

			// remove files/directories
			$path = $this->course_folder_path();
			self::$aws->delete_content($path);
		}
		else {
			$record = array('course_status' => '2');
			$this->update($record);
		}
		*/
	}


	/**
	 * load_json : function for the loading of various json files
	 * load_chameleon_json : function to load chameleon json
	 * load_course_json : function to load course json
	 * load_translation_json : function to load translation json
	 * @param path {string} location of the json file
	 * @param shortcode {string} language shortcode
	 * @param dev {string} designates whether dev or normal json should be returned ('-dev')
	 */
	private function load_json($path) {
		if (self::$aws->is_file($path)) {
			$json_file_content = self::$aws->get_content($path);
			$json_file_data = json_decode($json_file_content, TRUE);
			return ($json_file_data);
		}
		return false;
	}
	public function load_chameleon_json($version = '') {
		$chameleon_json = $this->config->item('version_path', 'default') . $this->version($version)->version_sub . '/chameleon.json';
		return $this->load_json($chameleon_json);
	}
	public function load_course_json($stage='pre') {
		$course_json = $this->json_file_path($stage);
		return $this->load_json($course_json);
	}
	public function load_translation_json($shortcode, $dev = '') {
		$translation_json = $this->course_folder_path('pre/course/data/' . $shortcode . $dev . '.js');

		if (!self::$aws->is_file($translation_json)) {
			self::$aws->copy_content($this->config->item('master_json', 'default'), $translation_json);
		}

		return $this->load_json($translation_json);
	}


	/**
	 * save_json : function for the saving of data in various json files
	 * save_course_json : function to save course data
	 * save_translation_json : function to save translation data
	 * @param path {string} location of the json file
	 * @param data {object} content to save
	 * @param dev {string} designates whether data should be saved to dev or normal json ('-dev')
	 * @param shortcode {string} language shortcode
	 * @param skip_id {boolean} when true, data will be checked for missing uuids on course topics and pages
	 */
	private function save_json($path, $data, $skip_id) {
		$data = ($skip_id) ? $data : $this->insert_missing_uuids($data);
		$options = 0;

		if (ENVIRONMENT == 'development') {
			$options = JSON_PRETTY_PRINT;
		}

		$content = json_encode($data, $options);
		self::$aws->add_content($content, $path);
	}
	public function save_course_json($data, $skip_id = FALSE) {
		$course_json = $this->json_file_path();
		$this->save_json($course_json, $data, $skip_id);
	}
	public function save_stage_json($data, $skip_id = FALSE) {
		$course_json = $this->json_file_path('stage');
		$this->save_json($course_json, $data, $skip_id);
	}
	public function save_translation_json($data, $shortcode, $dev, $skip_id = TRUE) {
		$translation_json = $this->course_folder_path('pre/course/data/' . $shortcode . $dev . '.js');
		$this->save_json($translation_json, $data, $skip_id);
	}


	/**
	 * Adds UUIDs to the data array where missing and appropriate.
	 * @param path {object} content to update
	 */
	private static function insert_missing_uuids($data) {
		get_instance()->load->helper('uuid');
		foreach ($data['topic'] as $t_key => $t_ar) {
			if (is_array($t_ar)) {
				if (!array_key_exists('uuid', $t_ar)) {
					$data['topic'][$t_key]['uuid'] = uuid_v4();
				}
				foreach ($t_ar['page'] as $p_key => $p_ar) {
					if (!array_key_exists('uuid', $p_ar)) {
						$data['topic'][$t_key]['page'][$p_key]['uuid'] = uuid_v4();
					}
				}
			}
			else if (is_object($t_ar)) {
				if (!isset($t_ar->uuid)) {
					$data['topic'][$t_key]->uuid = uuid_v4();
				}
				foreach ($t_ar->page as $p_key => $p_ar) {
					if (!isset($p_ar->uuid)) {
						$data['topic'][$t_key]->page[$p_key]->uuid = uuid_v4();
					}
				}
			}
		}
		return $data;
	}


	/**
	 * syncAssets - makes sure course assets contain all files for selected version
	 * @param assets {array} list of required assets in version json
	 */
	public function syncAssets($assets) {
		$master_asset_folder = $this->config->item('assets_path', 'default');
		$course_asset_folder = $this->course_folder_path('pre/course/assets/');


		// loop through asset groups and compile list of filenames
		$file_array = array();
		foreach($assets as $key => $value) {
			foreach($value as $item) {
				$file_array[] = $item['filename'];
			}
		}

		// loop through filenames and check to see if the course contains them - add if necessary
		foreach($file_array as $item) {
			$course_file = $course_asset_folder . $item;
			$exists = self::$aws->is_file($course_file);
			if(!$exists){
				$master_file = $master_asset_folder . $item;
				if(self::$aws->is_file($master_file)){
					self::$aws->copy_content($master_file, $course_file);
				}
			}
		}
	}


	/**
	 * Updates course.html with correct paths:
	 */
	public function update_course_html($stage='pre/') {

		$error = false;
		$master_path = $this->config->item('master_path', 'default');

		if(strpos($this->version()->version_number, '3.') > -1) {
			$index_html = $this->course_folder_path($stage . 'index.html');
			$index_template = $master_path . 'modern/pre/index.html';
		} else {
			$index_html = $this->course_folder_path($stage . 'course.html');
			$index_template = $master_path . 'pre/course.html';
		}
		$course_root = $this->config->item('cloud_service_url', 'default');

		// placeholders
		$chameleon_root_key = $this->config->item('chameleon_root_placeholder', 'default'); // [CHAMELEONURL]
		$course_folder_key = $this->config->item('client_version_url_placeholder', 'default'); // [CLIENTVERSIONURL]
		$course_version_key = $this->config->item('course_key_placeholder', 'default'); // [COURSEKEY]
		$course_stage_key = $this->config->item('course_stage_placeholder', 'default'); // [COURSESTAGE]
		$timestamp_key = $this->config->item('timestamp', 'default'); // [TIMESTAMP]

		$course_id = $this->config->item('course_id_placeholder', 'default'); // [COURSEID]
		$cdn_url = $this->config->item('cdn_url_placeholder', 'default'); // [CDNURL]

		$content = self::$aws->get_content($index_template);

		$content = str_replace($course_version_key, $this->course_version, $content);
		$content = str_replace($chameleon_root_key, $course_root, $content);
		$content = str_replace($course_folder_key, $this->cloudfront_folder_url(), $content);
		$content = str_replace($course_stage_key, $stage, $content);
		$content = str_replace($timestamp_key, time(), $content);

		$content = str_replace($course_id, $this->course_id, $content);
		$content = str_replace($cdn_url, $this->config->item('aws_path', 'default'), $content);

		// by default, 'debug' should be set to FALSE in prod
		if(strpos($stage, 'prod') > -1) {
			$pattern = '/(["\']?debug["\']?:\s?["\']?)true(["\']?)/';
			$replace = '$1false$2';
			$content = preg_replace($pattern, $replace, $content);
		}

		self::$aws->add_content($content, $index_html, 'text/html');
	}


	/**
	 * reset less variables file
	 */
	public function reset_less_file($version) {
		$version_path = $this->config->item('version_path', 'default');
		$source_path = $version_path . $version . '/css/';
		self::$aws->copy_content($source_path . '_chameleon_variables.less', $this->course_folder_path('_chameleon_variables.less'));
	}

	/**
	 * setup_course_folder :
	 */
	public function setup_course_folder($source_path = '', $destination_client = '', $version = '') {

		$error = false;
		$this->course_folder =  uniqid($this->course_id . '-');
		$this->client_id = ($destination_client) ? $destination_client : $this->client_id;
		$master_path = $this->config->item('master_path', 'default') . 'modern/';
		$source_path = ($source_path) ? $source_path . '/' : $master_path;
		$destination_path = $this->course_folder_path();

		// build new course
		self::$aws->copy_content($source_path . '_variables.less', $this->course_folder_path('_variables.less'));
		self::$aws->copy_content($source_path . 'pre', $this->course_folder_path('pre'));

		// update course files
		$this->update_course_html();

		$update_result = self::$db->update(self::$tablename,
			array('course_folder' => $this->course_folder),
			array(self::$primarykey => $this->course_id)
		);
	}


	/**
	 * Returns the path in the filesystem to the client folder.
	 * @param extra_path a path to a file that should be appended to the client folder.
	 */
	public function client_folder_path($extra_path = '') {
		$path = $this->config->item('client_path', 'default') . $this->client_id . "/$extra_path";
		return $path;
	}

	/**
	 * Returns the path in the filesystem to the course folder.
	 * @param extra_path a path to a file that should be appended to the course folder.
	 */
	public function course_folder_path($extra_path = '') {
		$extra_path = ($extra_path) ? "/$extra_path" : '';
		return $this->config->item('client_path', 'default') . $this->client_id . '/' . $this->config->item('course_path', 'default') . $this->course_folder . $extra_path;
	}


	/**
	 * Returns the url to the course folder
	 * @param extra_path a path to a file that should be appended to the course folder.
	 */
	public function course_folder_url($extra_path = '') {
		return $this->config->item('client_url', 'default') . $this->client_id . '/courses/' . $this->course_folder . "/$extra_path";
	}

	public function cloudfront_folder_url($extra_path = '') {
		return $this->config->item('cloudfront_path', 'default') . $this->config->item('client_path', 'default') . $this->client_id . '/courses/' . $this->course_folder . "/$extra_path";
	}

	public function json_file_path($stage='pre') {
		$json_file_path = $this->course_folder_path($stage . '/course/data/main.js');
		return $json_file_path;
	}


	/**
	 * Gets the associated version with this course.
	 */
	public function version($version = '') {
		$version = ($version) ? $version : $this->course_version;
		return $this->Version_model->get_by('version_number', $version);
	}


	/**
	 * Reads the _chameleon_variables.less file from the course directory, and parses it into a reasonable data structure for modification.
	 */
	public function get_theme_variables() {
		$chameleon_variables_file = $this->course_folder_path('_chameleon_variables.less');
		$version_variables_file = $this->version()->core_folder_path('css/_chameleon_variables.less');
		$master_variables_file = $this->config->item('master_theme_path', 'default');
		if(!self::$aws->is_file($chameleon_variables_file)) {
			if(self::$aws->is_file($version_variables_file)) {
				self::$aws->copy_content($version_variables_file, $chameleon_variables_file);
			} else {
				self::$aws->copy_content($master_variables_file, $chameleon_variables_file);
			}
		}
		$variables = array();
		$groupname = null;
		$group = null;

		// pull out necessary lines
		preg_match_all('/(\/{2}==[^\/@]+|@[^\/;]+;)/', self::$aws->get_content($chameleon_variables_file), $matches);

		foreach ($matches[0] as $line) {

			if (preg_match('/^\/\/== (.+)$/', $line, $matches)) {
				// Beginning of new "Group"
				if ($group !== null) {
					$variables[$groupname] = $group;
				}
				$group = array();
				$groupname = trim($matches[1]);
			} else if (strpos($line, "//") === 0) {
				// Comment, skip.
				continue;
			}
			if (preg_match('/^(@.+):\s+(.+);/', $line, $matches)) {
				$varname = $matches[1];
				$varval = $matches[2];
				$group[$varname] = $varval;
			}
		}
		if ($group !== null) {
			$variables[$groupname] = $group;
		}

		return $variables;
	}


	/**
	 * set_theme_variables : updates the custom theme with the following structure:
	 * array("Group Name" => array("@key-name" => "value"))
	 * Groups that aren't existing in the set variables will not be modified.
	 */
	public function set_theme_variables($new_vars) {
		$set_variables = $this->get_theme_variables();

		foreach ($new_vars as $groupname => $values) {
			$set_variables[$groupname] = $values;
		}

		//$backup = self::$aws->get_content($this->course_folder_path('_chameleon_variables.less'));
		//self::$aws->add_content($backup, $this->course_folder_path('_chameleon_variables.less.bak'));

		$content = "//#### THIS FILE AUTOGENERATED BY chameleoneditor ####\r\n";
		foreach ($set_variables as $groupname => $values) {
			$content .= "\r\n\r\n//== " . $groupname . "\r\n";
			$content .= "// \r\n";
			foreach ($values as $k => $v) {
				$content .= "${k}: $v;\r\n";
			}
		}
		self::$aws->add_content($content, $this->course_folder_path('_chameleon_variables.less'));
	}


	/**
	 * Returns an array of the languages that this course can be translated into.
	 */
	function translation_languages() {
		if (empty($this->course_enabled_languages)) {
			return array();
		}
		$enabled_language_ar = json_decode($this->course_enabled_languages);
		if (!is_array($enabled_language_ar)) {
			return array();
		}
		return array_diff($enabled_language_ar, array('en-US'));
	}

	/**
	 * Returns true if the user passed in can translate this course.
	 * Otherwise returns a string stating why you can't translate this course otherwise
	 */
	function can_translate($user) {
		if ($this->course_status != 1) {
			return "Course is not Approved yet";
		}
		if (count($this->translation_languages()) == 0) {
			return "No translation languages are enabled";
		}
		if (!$user->can('translate', $this->client_id, $this->course_id)) {
			return "You don't have permission";
		}
		return TRUE;
	}


	/**
	 * publish_course : copies course files to stage folder and updates course.html and main.js (for prod only)
	 * @param {string} env : the env the course is entering ('stage' or 'prod')
	 */
	public function publish_course($env, $stage_status_function = 'time') {
		$stage_status_function('message: Migrating course');
		$stage_status_function('progress_mode: indeterminate');

		// copy files to appropriate folder
		$source_dir = ($env == 'stage') ? 'pre/' : 'stage/';
		$source_path = $this->course_folder_path($source_dir);
		$destination_path = $this->course_folder_path($env . '/');
		self::$aws->copy_content($source_path, $destination_path);

		// after the course files are copied, update references in course.html
		$this->update_course_html($env . '/');
	}


	/**
	 * cloud_package : creates online package with limited files
	 * @param {array} settings (see package_course function)
	 */
	public function cloud_package($settings) {

		if(strpos($this->version()->version_number, '3.') > -1) {
			$target_files = ['index.html'];
		} else {
			$target_files = ['course.html', 'index.html','respond.proxy.gif','respond.proxy.js'];
		}

		if($settings['api'] !== 'none' && $settings['api'] !== 'chameleon') {
			$this->api_files($settings);
		}

		foreach($target_files as $item) {
			$content = self::$aws->get_content($settings['dir'] . $item);

			// before we zip the index.html, we need to make sure that debug is set to FALSE
			if($item == 'index.html') {
				$pattern = '/(["\']?debug["\']?:\s?["\']?)true(["\']?)/';
				$replace = '$1false$2';
				$content = preg_replace($pattern, $replace, $content);
			}

			$this->zip->add_data($settings['course_root'] . $item, $content);
		}

		$filename = $settings['filename'] . '-' . $settings['phase'];
		$package = $this->zip->get_zip();
		self::$aws->add_content($package, $settings['zip_root'] . $filename . '.zip');

		// clear zip data when finished
		$this->zip->clear_data();

	}


	/**
	 * api_files : adds api related files into course packages
	 * @param {array} settings (see package_course function)
	 */
	public function api_files($settings) {

		$api_path = $this->config->item('api_path', 'default');

		$manifest_title_key = $this->config->item('manifest_placeholder', 'default'); // [IMSMANIFESTCOURSETITLE]
		$manifest_id_key = $this->config->item('manifest_placeholder_id', 'default'); // [IMSMANIFESTCOURSEID]

		$target_files = ['imsmanifest.xml','tc-config.js','tincan.xml','aicc-config.crs','aicc-config.des'];

		if(strpos($this->version()->version_number, '3.') > -1) {
			$lms_path = $api_path . 'master/templates/' . $settings['api'] . '/';
		} else {
			$lms_path = $api_path . 'templates/' . $settings['api'] . '/';
			self::$aws->zip_folder($api_path, 'scormdriver/');
		}

		$getFiles = self::$aws->list_objects($lms_path);

		foreach($getFiles as $item) {

			$filename = str_replace($lms_path, '', $item['Key']);

			if(substr($item['Key'], -1) == '/') {
				if($item['Key'] !== $lms_path) {
					self::$aws->zip_folder($lms_path, $filename);
				}
			}
			else if(in_array($filename, $target_files)) {
				$content = self::$aws->get_content($item['Key']);
				$content = str_replace($manifest_title_key, $this->course_title, $content);
				$content = str_replace($manifest_id_key, $this->course_id, $content);
				$this->zip->add_data($filename, $content);
			}
			else {
				$content = self::$aws->get_content($item['Key']);
				$this->zip->add_data($filename, $content);
			}
		}
	}


	/**
	 * package_course : the main function call for packaging a course
	 * @param {string} phase : the phase the course is entering ('stage' or 'prod')
	 * @desc {array} settings : [ api, course_root, dir, phase, zip_root]
	 *  @desc {string} api : [ aicc, chameleon, none, scorm, scorm_2, scorm_3, scorm_4, tincan ]
	 *  @desc {string} course_root : course files should be in 'scormcontent/' folder if version is less than 3.x
	 *  @desc {string} dir : directory path for course files
	 *  @desc {string} phase : the phase the course is entering ('stage' or 'prod')
	 *  @desc {string} zip_root : directory path where zip files should be stored
	 */
	public function package_course($phase, $stage_status_function = 'time') {
		$stage_status_function('message: Packaging course');
		$stage_status_function('progress_mode: indeterminate');

		$this->load->library('zip');

		// clear zip for fresh start
		$this->zip->clear_data();

		// get API setting
		$course_data = $this->load_course_json();
		$api = $course_data['config']['api'];

		$settings = [
			'api' => $api,
			'course_root' => (strpos($this->version()->version_number, '3.') > -1 || ($api == 'none' || $api == 'chameleon')) ? '' : 'scormcontent/',
			'dir' => $this->course_folder_path($phase . '/'),
			'filename' => package_name($this->course_title),
			'phase' => $phase,
			'zip_root' => $this->course_folder_path('cloud/downloads/')
		];

		if($phase == 'stage') {
			$data['date_staged'] = date("Y-m-d H:i:s");
		} else {
			$data['date_published'] = date("Y-m-d H:i:s");
		}

		$data['local_download'] = $settings['filename'];
		$this->update($data);

		// start packaging
		$this->cloud_package($settings);
	}

	/**
	 * compile_course : refreshes course.html and parses less css files
	 */
	public function compile_course() {

		// append timestamps to course.html to solve cacheing issues
		$this->update_course_html();

	}

	/**
	 * Compares the list of files and directories in the course's media
	 * directory with strings that are present in the course data.  Any files
	 * and directories present in the media directory that are not referenced in
	 * the course data are deleted from S3.  Legacy subdirectories are protected
	 * for backwards compatibility.
	 *
	 * History:
	 * Whereas it used to be that we would have subdirectories within the
	 * course's media directory to hold different types of media, in CHAM-1815
	 * we began placing media files directly into the course's media directory.
	 * Since media files are only expected to be placed directly within the
	 * media directory going forward, we're only concerned with garbage
	 * collecting files that are immediate descendants of the course's media
	 * directory.
	 */
	 public function media_garbage_collection($data, $translation = null) {
 		$allowed_subdirectory_ar = array('audio', 'custom', 'images', 'resources', 'video', 'translation');

 		$gc_root = $this->course_folder_path('pre/course/media');
 		if (null !== $translation) {
 			$gc_root = $this->course_folder_path("pre/course/media/translation/$translation");
 		}

 		# obtain a list of all strings within the course data
 		$course_data_string_ar = $this->get_course_strings($data);

 		# obtain a list of all items within the directory being garbage collected
 		$media_file_ar = $this->get_filenames_in_directory("$gc_root/");

 		# diff these two lists in order to arrive at a list of orphaned items
 		$orphaned_item_ar = array();
 		foreach ($media_file_ar as $filename => $fileinfo) {
 			# when we're not garbage collecting a translation media subdirectory, some
 			# subdirectories must be protected from removal
 			if ('dir' == $fileinfo['type'] && null === $translation && in_array($filename, $allowed_subdirectory_ar)) {
 				continue;
 			}

 			# if this file or directory is not mentioned in the course data, add it
 			# to the list of orphaned media items to be deleted
 			if (!in_array($filename, $course_data_string_ar)) {
 				$fileinfo['path'] = "$gc_root/$filename";
 				$orphaned_item_ar[] = $fileinfo;
 			}
 		}

 		# delete all orphaned media items
 		foreach ($orphaned_item_ar as $fileinfo) {
 			if ('dir' == $fileinfo['type']) {
 				self::$aws->delete_by_prefix($fileinfo['path']);
 			} else {
 				self::$aws->delete_by_key($fileinfo['key']);
 			}
 		}
 	}

	/**
	 * Retrieves a list of all files that are immediate descendants of the
	 * course's media directory.
	 */
	private function get_filenames_in_directory($target) {
		if ('/' != $target[strlen($target)-1]) {
			echo "Appending '/' to target '$target'\n";
			$target .= '/';
		}
		$ret_ar = array();
		foreach (self::$aws->ls($target) as $filename => $fileinfo) {
			$ret_ar[$filename] = $fileinfo;
		}
		return $ret_ar;
	}

	/**
	 * Given course data that has been decoded from a JSON string, this returns
	 * a flat array of all strings within the data.  This will be a superset of
	 * all media filenames referenced in the course data.
	 */
	private function get_course_strings($object_or_array) {
		$ret_ar = array();

		# if this node is not an object or an array, we cannot operate on it
		if (in_array(gettype($object_or_array), array('object', 'array'))) {

			# iterate through each item in this object/Array
			foreach ($object_or_array as $k => $v) {
				$datatype = gettype($v);

				# recurse into objects and arrays, but add strings to the return array
				if (in_array($datatype, array('object', 'array'))) {
					$ret_ar = array_merge($ret_ar, $this->get_course_strings($v));
				}
				elseif ('string' == $datatype && !empty($v)) {
					$ret_ar[] = $v;
				}
			}
		}

		return $ret_ar;
	}

	/**
	 * Using recursion, creates a ZIP archive containing all files and directories
	 * beneath the provided file system location.
	 *
	 * @param  string $zip              Path to output file
	 * @param  string $src_dirpath      Path to source directory
	 * @param  string $override_dirpath Path to a source directory having the same
	 * directory structure as $src_dirpath.  Any files that exist in this
	 * structure will override a file existing at the same location in
	 * $src_dirpath. Optional.
	 * @param  string $dst_dirpath      Do not use. Reserved for recursion.
	 */
	private function package_media_zip($zip, $src_dirpath, $override_dirpath = null, $dst_dirpath = '') {
		if (is_string($zip)) {
			$zip_filepath = $zip;
			$zip = new ZipArchive;
			$zip->open($zip_filepath, ZipArchive::CREATE);
		}
		$dh = opendir($src_dirpath);
		while ($name = readdir($dh)) {
			if ('.' == $name || '..' == $name) {
				continue;
			}
			if (is_dir("$src_dirpath/$name")) {
				$new_dst_dirpath = empty($dst_dirpath) ? $name : "$dst_dirpath/$name";
				self::package_media_zip($zip, "$src_dirpath/$name", "$override_dirpath/$name", $new_dst_dirpath);
			}
			else {
				$dst_filepath = empty($dst_dirpath) ? $name : "$dst_dirpath/$name";
				if (null !== $override_dirpath && is_file("$override_dirpath/$name")) {
					echo "Add '$dst_filepath' to zip from source '$override_dirpath/$name'\n";
					$zip->addFile("$override_dirpath/$name", $dst_filepath);
				} else {
					echo "Add '$dst_filepath' to zip from source '$src_dirpath/$name'\n";
					$zip->addFile("$src_dirpath/$name", $dst_filepath);
				}
			}
		}
		closedir($dh);
		if (!empty($zip_filepath)) {
			$zip->close();
		}
	}

	/**
	 * Lists all files in a specific S3 location. Only lists directories if
	 * their name matches a pattern that an Adobe Edge animation's directory
	 * would adhere to.
	 * 
	 * @param  string $s3_path	Path to location to search for media assets.
	 * @return [type]
	 */
	function enumerate_media_assets($s3_path) {
		$asset_ar = array();
		$ret = self::$aws->ls($s3_path);
		foreach ($ret as $name => $info) {
			# skip any directories that do not appear to be Adobe Edge animation
			# directories in order to avoid including directories that house legacy
			# media assets
			if ('dir' == $info['type'] && !preg_match('#^[0-9a-zA-Z]+-[0-9a-zA-Z]+$#', $name)) {
				continue;
			}

			$asset_ar[$name] = $info;
		}
		return $asset_ar;
	}

	/**
	 * Create media packages for the original English language version of this
	 * course as well as for all other enabled translation languages. Since this
	 * process can take some time, we periodically invoke a status callback.
	 *
	 * @param  function $stage_status_function A function that will be called to
	 * indicate a status update.
	 */
	public function package_media($stage_status_function) {
		# only create packages if this course is marked as available offline
		if (!$this->offline) {
			return;
		}

		# initializations
		$stage_status_function('progress_pct: 0');
		$stage_status_function('progress_mode: determinate');
		$media_root = $this->course_folder_path('pre/course/media');
		$package_dst_root = $this->course_folder_path('cloud/translations');

		# get translation languages enabled for this course
		$translation_shortcode_ar = $this->translation_languages();
		$translation_count = count($translation_shortcode_ar);

		# enumerate media assets that are direct descendants of the media directory
		$stage_status_function('message: Finding English-language media assets');
		$english_asset_ar = $this->enumerate_media_assets("$media_root/");
		$stage_status_function('progress_pct: 2');

		# enumerate assets within the translation media subdirectories for the
		# languages we're packaging
		$translation_asset_ar = array();
		foreach ($translation_shortcode_ar as $shortcode) {
			$stage_status_function("message: Finding media assets for language '$shortcode'");
			$translation_asset_ar[$shortcode] = $this->enumerate_media_assets("$media_root/translation/$shortcode/");
		}
		$stage_status_function('progress_pct: 4');

		# create a temporary directory for storing downloaded media assets
		$tmp_dirpath = sys_get_temp_dir() . '/' . uniqid('package_media_');
		$stage_status_function($tmp_dirpath);
		if (!mkdir($tmp_dirpath)) {
			throw new Exception("Unable to create temp dir '$tmp_dirpath'");
		}

		# download English-language media assets from S3
		$stage_status_function('message: Processing English-language media assets');
		$tmp_english_dirpath = "$tmp_dirpath/en-US";
		if (!mkdir($tmp_english_dirpath)) {
			throw new Exception('Unable to create temp dir for English assets');
		}
		$english_asset_count = count($english_asset_ar);
		$i = 0;
		foreach ($english_asset_ar as $name => $info) {
			$dst_path = "$tmp_english_dirpath/$name";
			if ('dir' == $info['type']) {
				$src_dirpath = "$media_root/$name";
				self::$aws->download_directory($src_dirpath, $dst_path);
				$stage_status_function("Download DIR '$src_dirpath' to '$dst_path'");
			} else {
				$stage_status_function("Download FILE '$name' to '$dst_path'");
				if (!file_put_contents($dst_path, self::$aws->get_content($info['key']))) {
					$stage_status_function("Unable to download key '{$info['key']}'");
				}
			}
			$stage_status_function(sprintf('progress_pct: %d', 4 + floor(46 * ($i+1) / $english_asset_count)));
			$i++;
		}
		$stage_status_function('progress_pct: 50');

		# download translation media assets
		$i = 0;
		$total_translation_asset_count = 0;
		foreach ($translation_asset_ar as $asset_list) {
			$total_translation_asset_count += count($asset_list);
		}
		$stage_status_function("There are '$total_translation_asset_count' translated assets to download");
		foreach ($translation_asset_ar as $shortcode => $asset_list) {
			$stage_status_function("message: Processing '$shortcode' translation assets");
			$tmp_translation_dirpath = "$tmp_dirpath/$shortcode";
			if (!mkdir($tmp_translation_dirpath)) {
				throw new Exception("Unable to create temp dir for '$shortcode' assets");
			}
			foreach ($asset_list as $name => $info) {
				$dst_path = "$tmp_translation_dirpath/$name";
				if ('dir' == $info['type']) {
					$src_dirpath = "$media_root/translation/$shortcode/$name";
					self::$aws->download_directory($src_dirpath, $dst_path);
					$stage_status_function("Download DIR '$src_dirpath' to '$dst_path'");
				} else {
					$stage_status_function("Download FILE '$name' to '$dst_path'");
					self::$aws->get_content($info['key']);
					if (!file_put_contents($dst_path, self::$aws->get_content($info['key']))) {
						$stage_status_function("Unable to download key '{$info['key']}'");
					}
				}
				$stage_status_function(sprintf('progress_pct: %d', 50 + floor(35 * ($i+1) / $total_translation_asset_count)));
				$i++;
			}
		}
		$stage_status_function('progress_pct: 85');

		# create and upload English-language media package
		$stage_status_function('message: Creating English-language media package');
		$media_package_filepath = "$tmp_dirpath/en-US.zip";
		self::package_media_zip($media_package_filepath, $tmp_english_dirpath);
		self::$aws->add_file($media_package_filepath, "$package_dst_root/en-US.zip", 'application/zip');
		unlink($media_package_filepath);
		$stage_status_function('progress_pct: 90');

		# create and upload translation media packages
		$i = 0;
		foreach ($translation_asset_ar as $shortcode => $asset_list) {
			$stage_status_function("message: Creating '$shortcode' package");
			$media_package_filepath = "$tmp_dirpath/$shortcode.zip";
			self::package_media_zip($media_package_filepath, $tmp_english_dirpath, "$tmp_dirpath/$shortcode");
			self::$aws->add_file($media_package_filepath, "$package_dst_root/$shortcode.zip", 'application/zip');
			unlink($media_package_filepath);
			$stage_status_function(sprintf('progress_pct: %d', 90 + floor(10 * ($i+1) / $translation_count)));
			$i++;
		}
		$stage_status_function('progress_pct: 100');
		$stage_status_function('message: Done!');

		# clean up
		delete_files($tmp_dirpath, true);
		rmdir($tmp_dirpath);
	}
}
