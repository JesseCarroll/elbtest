<?php

class Curriculum_model extends CI_Model {

	# table name
	private $tablename = 'curriculum';

	# primary key
	private $primarykey = 'curriculum_id';

	function __construct() {
		parent::__construct();
	}

	# delete record by id
	function delete($id = 0){
		$this->db->where($this->primarykey, $id);
		$this->db->delete($this->tablename);
	}

	# get all records
	function get_all($client_id){
		$this->db->where('client_id', $client_id);
		$query = $this->db->get($this->tablename);
		return $query->result();
	}

	# get record by id
	function get_by_id($id = 0){
		$this->db->where($this->primarykey, $id);
		$query = $this->db->get($this->tablename);
		return $query->row();
	}

	# add new record
	function insert($data){
		$this->db->insert($this->tablename, $data);
		return $this->db->insert_id();
	}

	# update record by id
	function update($id = 0, $data){
		$this->db->where($this->primarykey, $id);
		$this->db->update($this->tablename, $data);
	}
}

?>