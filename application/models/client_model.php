<?php

class Client_model extends CI_Model {
	private static $db;
	private static $aws;

	// table name
	private static $tablename = 'clients';
	private static $primarykey = 'client_id';

	function __construct() {
		parent::__construct();
		self::$db = &get_instance()->db;
		self::$aws = &get_instance()->aws;
	}

	# count the number of clients
	public static function count_all() {
		return self::$db->count_all(self::$tablename);
	}

	# get a list of all clients
	public static function get_all($sort_field = 'client_name', $sort_order = 'asc') {
		self::$db->order_by($sort_field, $sort_order);
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# get a client by ID
	public static function get_by_id($id = 0) {
		self::$db->where(self::$primarykey, $id);
		$query = self::$db->get(self::$tablename);
		return $query->row(0, __CLASS__);
	}

	# return courses specific to this client
	public function get_courses() {
		return Course_model::get_by_client($this->client_id);
	}

	public function get_player_order() {
		self::$db->select('player_order.id, courses.course_id, courses.course_title, courses.course_folder, curriculum.curriculum_name, player_order.course_order');
		self::$db->from('courses');
		self::$db->where('courses.offline', 1);
		self::$db->where('courses.client_id', $this->client_id);

		self::$db->join('player_order', 'player_order.course_id = courses.course_id', 'left');
		self::$db->join('curriculum', 'curriculum.curriculum_id = courses.course_curriculum', 'left');

		$query = self::$db->get();
		return $query->result_array();
	}

	public function update_player_order($data) {
		foreach ($data as $item) {
			$data_array = array(
				'course_id' => $item->course_id,
				'course_order' => $item->course_order
			);
			if ($item->id) {
				self::$db->where('player_order.id', $item->id);
				self::$db->update("player_order", $data_array);
			} else {
				self::$db->insert("player_order", $data_array);
			}
		}
	}

	public function delete_player_order($data) {
		self::$db->where('player_order.course_id', $data->course_id);
		self::$db->delete("player_order");
	}

	public function get_player_config() {
		self::$db->select('*');
		self::$db->from('player_config');
		self::$db->where('client_id', $this->client_id);
		$query = self::$db->get();
		if ($query->result()) {
			return $query->result()[0];
		}
	}

	public function update_player_config($data) {
		self::$db->where('player_config.client_id', $this->client_id);
		$data->client_id = $this->client_id;
		if(isset($data->id)) {
			self::$db->update("player_config", $data);
		} else {
			self::$db->insert("player_config", $data);
		}
	}

	// return users specific to this client
	public function get_users() {
		return User_model::get_by_client($this->client_id);
	}

	// return curriculum specific to this client
	public function get_curriculum($active = false) {
		self::$db->select('curriculum_id, curriculum_name, curriculum_status, date_created');
		self::$db->from('curriculum');
		self::$db->where('curriculum.client_id', $this->client_id);
		if($active) {
			self::$db->where('curriculum.curriculum_status', '1');
		}
		$query = self::$db->get();
		return $query->result_array();
	}

	// return courses in a specified curriculum
	public function get_curriculum_courses($id) {
		self::$db->from('courses');
		$array = array('client_id' => $this->client_id, 'course_curriculum' => $id);
		self::$db->where($array);
		return self::$db->count_all_results();
	}

	// add new curriculum record
	public static function create_curriculum($data) {
		self::$db->insert('curriculum', $data);
		return self::$db->insert_id();
	}

	// update curriculum record
	public static function update_curriculum($id, $data) {
		self::$db->where('curriculum_id', $id);
		self::$db->update('curriculum', $data);
	}

	// return events specific to this client
	public function get_events() {
		self::$db->from('lms_events');
		self::$db->where('lms_events.client_id', $this->client_id);
		$query = self::$db->get();
		return $query->result_array();
	}

	# add a new client
	public static function insert($client_name) {
		# add a record to the clients table
		$record = array(
			'client_name' => $client_name,
			'client_status' => '1',
			'date_updated' => date('Y-m-d H:i:s'),
			'date_created' => date('Y-m-d H:i:s'),
			'default_theme' => $initial_theme_id,
		);
		$query = self::$db->insert(self::$tablename, $record);
		if (false === $query) {
			throw new Exception('Unable to insert new record');
		}
		$model = self::get_by_id(self::$db->insert_id());

		# create an empty default theme for this client
		$replacement_ar = array(
			'#\s+#' => '_',
			'#\W+#' => '',
			'#_+#'  => '_',
		);
		$initial_theme_id = preg_replace(array_keys($replacement_ar), array_values($replacement_ar), strtolower($client_name));
		self::$aws->add_content('{}', $model->client_folder_path("themes/$initial_theme_id.json"), 'text/json');

		return $model;
	}

	# update a client
	public function update($data) {
		self::$db->where(self::$primarykey, $this->client_id);
		self::$db->update(self::$tablename, $data);
	}

	# delete a client
	public function delete() {
		if (!$this->client_id) {
			return;
		}

		self::$db->where(self::$primarykey, $this->client_id);
		self::$db->delete(self::$tablename);
	}

	# return a list of models for all active clients
	public static function get_all_active() {
		$where = array('client_status' => '1');
		self::$db->where($where);
		self::$db->order_by('client_name', 'asc');
		$query = self::$db->get(self::$tablename);
		return $query->result(__CLASS__);
	}

	# go through courses, and unlock inactives for editing
	# also set value to null curriculums (Question for developer: Why do this here?)
	public static function unlock_inactives($client_id) {
		$num_unlocked = 0;
		$courses = Course_model::get_by_client($client_id);
		for ($i = 0; $i < count($courses); $i++) {
			if ($courses[$i]->being_edited == 1) {
				$current_time = time();
				$expire_time = strtotime($courses[$i]->date_updated) + (60 * 60);

				if ($expire_time < $current_time) {
					$courses[$i]->update(array('being_edited' => '0'));
					$num_unlocked++;
				}
			}

			if (empty($courses[$i]->curriculum_name)) {
				$courses[$i]->curriculum_name = 'None';
			}
		}
		return $num_unlocked;
	}

	/**
	 * Returns the path in the filesystem to the client folder.
	 * @param extra_path a path to a file that should be appended to the client folder.
	 */
	public function client_folder_path($extra_path = '') {
		$path = $this->config->item('client_path', 'default') . $this->client_id . "/$extra_path";
		return $path;
	}

	/**
	 * Returns the url to the client folder
	 * @param extra_path a path to a file that should be appended to the client folder.
	 */
	public function client_folder_url($extra_path = '') {
		return $this->config->item('client_url', 'default') . $this->client_id . "/$extra_path";
	}
}
