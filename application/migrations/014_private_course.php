<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_private_course extends CI_Migration {
	public function up() {
		$this->dbforge->add_column('courses', array(
			'private' => array(
				'type'    => 'TINYINT(1)',
				'null'    => false,
				'default' => '0'
			),
		));
	}

	public function down() {
	  $this->dbforge->drop_column('courses', 'private');
	}
}
