<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_login_status extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_users` ADD COLUMN `login_status` INT(1) NULL  AFTER `user_status` ;
EOSQL
);
	}

	public function down() {
	  $this->dbforge->drop_column('users', 'login_status');
	}
}