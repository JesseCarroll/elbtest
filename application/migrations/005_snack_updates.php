<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_snack_updates extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_lms_users` CHANGE COLUMN `token` `config` TEXT DEFAULT NULL;
EOSQL
);

		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` ADD COLUMN `learning_snack` INT(11) NULL  AFTER `local_download` ;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_userClient` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `code` varchar(25) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_userToken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `creation_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);
	}
	
	public function down() {
		$this->dbforge->drop_table('userClient');
		$this->dbforge->drop_table('userToken');
	  $this->dbforge->drop_column('cml_courses', 'learning_snack');

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_lms_users` CHANGE COLUMN `config` `token` varchar(100) DEFAULT NULL;
EOSQL
);
	}
}