<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_remove_groups_client_id extends CI_Migration {
	public function up() {
		# get rid of unneeded client ID field in group table
		$this->dbforge->drop_column('groups', 'client_id');

		# create missing table associating users with groups
		$this->dbforge->add_field(array(
			"user_id" => array(
				'type' => 'INT',
				'constraint' => 11,
			),
			"group_id" => array(
				'type' => 'INT',
				'constraint' => 11,
			),
		));
		$this->dbforge->add_key('user_id', true);
		$this->dbforge->add_key('group_id', true);
		$this->dbforge->create_table('user_group');
	}

	public function down() {
		# re-add group table client ID field
		$this->dbforge->add_column('groups', array(
			'client_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
		));

		# delete user_group table
		$this->dbforge->drop_table('user_group');
	}
}
