<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Migration_course_themes extends CI_Migration {

	public function up() {
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` ADD COLUMN `course_theme` ENUM('course','client','curriculum') NULL DEFAULT 'client'  AFTER `course_status` , ADD COLUMN `course_curriculum` VARCHAR(45) NULL DEFAULT NULL  AFTER `course_theme`;
EOSQL
);
	}

	public function down() {
		$this->dbforge->drop_column('cml_courses', 'course_theme');
		$this->dbforge->drop_column('cml_courses', 'course_curriculum');
	}
}