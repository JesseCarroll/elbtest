<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_migration_updates extends CI_Migration {
	public function up() {
		$this->dbforge->add_column('courses', array(
			'locked' => array(
				'type'    => 'TINYINT(1)',
				'null'    => false,
				'default' => '0'
			),
		));
		$this->dbforge->add_column('courses', array(
			'state' => array(
				'type'    => "ENUM('author', 'translate')",
				'null'    => true
			),
		));
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` ADD COLUMN `date_staged` timestamp NULL DEFAULT NULL AFTER `date_updated`;
EOSQL
);

	}

	public function down() {
	  $this->dbforge->drop_column('courses', 'locked');
	  $this->dbforge->drop_column('courses', 'state');
	  $this->dbforge->drop_column('courses', 'date_staged');
	}
}
