<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_variable_theming extends CI_Migration {
	public function up() {
		$this->dbforge->add_column('clients', array(
			'default_theme' => array(
				'type'    => 'VARCHAR(255)',
				'null'    => true,
			),
		));
	}

	public function down() {
	  $this->dbforge->drop_column('clients', 'default_theme');
	}
}
