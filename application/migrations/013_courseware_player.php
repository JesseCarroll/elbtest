<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_courseware_player extends CI_Migration {
	public function up() {
    $this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_player_users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `email` VARCHAR(255) NOT NULL ,
  `client_id` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);

    $this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_player_usermap` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `user_id` INT(11) NOT NULL ,
  `curriculum` INT(11) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_player_order` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `course_order` INT(11) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);

    $this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_player_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `client_id` INT(11) NOT NULL ,
  `title` VARCHAR(255) NULL ,
  `logo` VARCHAR(255) NULL ,
  `color` VARCHAR(255) NULL ,
  `expiration` INT(11) NULL ,
  `endpoint` VARCHAR(255) NULL ,
  `username` VARCHAR(255) NULL ,
  `password` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `learning_snack` `offline` INT(11) NOT NULL DEFAULT '0', ADD COLUMN `description` VARCHAR(255) NULL AFTER `offline`, ADD COLUMN `poster` VARCHAR(255) NULL AFTER `description`;
EOSQL
);

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_clients` ADD COLUMN `invite_code` VARCHAR(30) NULL  AFTER `default_theme` ;
EOSQL
);

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_usertoken` CHANGE COLUMN `creation_date` `creation_date` TIMESTAMP NOT NULL , ADD COLUMN `expiration_key` VARCHAR(100) NULL AFTER `token`;
EOSQL
);
	}

	public function down() {
    $this->dbforge->drop_table('player_users');
    $this->dbforge->drop_table('player_usermap');
		$this->dbforge->drop_table('player_order');
    $this->dbforge->drop_table('player_config');
    $this->dbforge->drop_column('clients', 'invite_code');
    $this->dbforge->drop_column('usertoken', 'expiration_key');
    $this->dbforge->drop_column('courses', 'description');
    $this->dbforge->drop_column('courses', 'poster');
    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `offline` `learning_snack` INT(11) NOT NULL DEFAULT '0';
EOSQL
);

	}
}