<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_login_session extends CI_Migration {
	public function up() {
	  $this->dbforge->drop_column('users', 'login_status');
		$this->dbforge->add_column('users', array(
			'login_status' => array(
				'type'    => 'TEXT',
				'null'    => true,
			),
		), 'user_status');
	}

	public function down() {
	  $this->dbforge->drop_column('users', 'login_status');
		$this->dbforge->add_column('users', array(
			'login_status' => array(
				'type'    => 'INT(1)',
				'null'    => true,
			),
		), 'user_status');
	}
}
