<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_feedback_field extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_leads` ADD COLUMN `feedback` VARCHAR(10000) NULL  AFTER `job_title` ;
EOSQL
);
	}

	public function down() {
	  $this->dbforge->drop_column('leads', 'feedback');
	}
}