<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_user_permission_upgrade extends CI_Migration {
	public function up() {
		# rename user_roles to just roles -- without this, the naming of this
		# table is fundamentally confusing because one might take it to mean that
		# the table associates roles with users
		$this->dbforge->rename_table('user_roles', 'roles');
		$this->dbforge->modify_column('roles', array(
			'user_role_id' => array(
				'name' => 'role_id',
				'type' => 'INT',
			),
		));
		log_message('info', 'Renamed "user_roles" to "roles"');

		# create groups table
		$this->dbforge->add_field(array(
			'group_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			),
			'name' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'null' => true,
			),
			'client_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
		));
		$this->dbforge->add_key('group_id', true);
		$this->dbforge->create_table('groups');
		log_message('info', 'Created groups table');

		# create group/client, group/course, user/client and user/course lookup tables
		foreach (array('group', 'user') as $user_or_group) {
			foreach (array('course', 'client') as $course_or_client) {
				$this->dbforge->add_field(array(
					"{$user_or_group}_id" => array(
						'type' => 'INT',
						'constraint' => 11,
					),
					"{$course_or_client}_id" => array(
						'type' => 'INT',
						'constraint' => 11,
					),
					'role_id' => array(
						'type' => 'INT',
						'constraint' => 11,
					),
				));
				$this->dbforge->add_key("{$user_or_group}_id", true);
				$this->dbforge->add_key("{$course_or_client}_id", true);
				$this->dbforge->create_table("{$user_or_group}_{$course_or_client}");
				log_message('info', "Created {$user_or_group}/{$course_or_client} lookup table");
			}
		}

		# create table relating one user to another in in order to indicate which
		# user manages another user
		$this->dbforge->add_field(array(
			'user_id'=> array(
				'type' => 'INT',
				'constraint' => 11,
			),
			'manager_id' => array(
				'type' => 'INT',
				'constraint' => 11,
			),
		));
		$this->dbforge->add_key('user_id', true);
		$this->dbforge->add_key('manager_id', true);
		$this->dbforge->create_table('user_manager');
		log_message('info', "Created user/manager lookup table");

		# create variables to hold prefiexed table names for use in raw queries
		$user_table_name = $this->db->dbprefix('users');
		$role_table_name = $this->db->dbprefix('roles');
		$group_table_name = $this->db->dbprefix('groups');
		$user_course_table_name = $this->db->dbprefix('user_course');
		$user_client_table_name = $this->db->dbprefix('user_client');

		# create an array associating role name with role ID to help later with lookups
		$query = $this->db->query("SELECT role, role_id FROM $role_table_name");
		$role_id_lookup_ar = array();
		foreach ($query->result() as $row) {
			$role_id_lookup_ar[$row->role] = $row = $row->role_id;
		}

		# convert allowed_clients/allowed_courses to rows in the new lookup tables
		$src_query = $this->db->query("SELECT allowed_clients, allowed_courses, user_id, role FROM $user_table_name");
		foreach ($src_query->result() as $user) try {
			# assuming that no records are needed in user_courses/user_clients for users that
			# have the SA role because the SA role allows them to do anything in the editor
			if ($user->role == 'SA') {
				continue;
			}

			# look up user's role ID using users.role column value
			if (!isset($role_id_lookup_ar[$user->role])) {
				throw new Exception("Unrecognized role '{$user->role}'");
			}
			$role_id = $role_id_lookup_ar[$user->role];

			foreach (array('course', 'client') as $type) {
				if (empty($user->{"allowed_{$type}s"})) continue;
				$dst_id_ar = preg_split('#\s*,\s*#', $user->{"allowed_{$type}s"});
				foreach ($dst_id_ar as $dst_id) {
					log_message('info', "Insert $type row associating user {$user->user_id} with $type $dst_id");
					$table_name = ${"user_{$type}_table_name"};
					$sql = "INSERT INTO $table_name (user_id, {$type}_id, role_id) VALUES (?, ?, ?)";
					if (!$this->db->query($sql, array($user->user_id, $dst_id, $role_id))) {
						throw new Exception("Unable to execute SQL: $sql");
					}
				}
				//$dst_query = $this->db->query("INSERT INTO ")
			}
		}
		catch (Exception $e) {
			log_message('error', "Error processing user '{$user->user_id}: {$e->getMessage()}'");
			continue;
		}

		# convert users.role into a users.super_admin boolean column
		$this->dbforge->add_column('users', array(
			'super_admin' => array(
				'type' => 'BOOLEAN',
				'default' => 0,
			),
		));
		$sql = "UPDATE $user_table_name SET super_admin = (STRCMP('SA', role) = 0)";
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to set users.super_admin');
		}
		$this->dbforge->drop_column('users', 'role');
		log_message('info', "Converted users.role to users.super_admin");

		# drop users.allowed_courses & users.allowed_clients
		$this->dbforge->drop_column('users', 'allowed_clients');
		$this->dbforge->drop_column('users', 'allowed_courses');
		log_message('info', "Dropped users.allowed_clients & users.allowed_courses");

		# create a "legacy" user table view that emulates the structure of the
		# users table prior to this set of changes
		$sql = sprintf('CREATE VIEW %5$s AS
			SELECT *,
			IF (super_admin, "SA", (
				SELECT role FROM %4$s
				WHERE role_id IN (
					SELECT %2$s.role_id FROM %2$s WHERE %2$s.user_id = %1$s.user_id
					UNION
					SELECT %3$s.role_id FROM %3$s WHERE %3$s.user_id = %1$s.user_id
				) LIMIT 1)
			) as role,
			(
				SELECT GROUP_CONCAT(%2$s.course_id SEPARATOR ",")
				FROM %2$s WHERE %2$s.user_id = %1$s.user_id
			) AS allowed_courses,
			(
				SELECT GROUP_CONCAT(%3$s.client_id SEPARATOR ",")
				FROM %3$s WHERE %3$s.user_id = %1$s.user_id
			) AS allowed_clients
			FROM %1$s',
			$this->db->dbprefix('users'),
			$this->db->dbprefix('user_course'),
			$this->db->dbprefix('user_client'),
			$this->db->dbprefix('roles'),
			$this->db->dbprefix('users_legacy')
		);
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to create legacy user table view');
		}
		log_message('info', "Created legacy user table view");
	}

	public function down() {
		# add allowed_clients, allowed_courses, role back into users table
		$sql = sprintf('ALTER TABLE %1$s
			ADD COLUMN `role` varchar(5) DEFAULT "NONE" AFTER `reset`,
			ADD COLUMN `allowed_clients` varchar(45) DEFAULT NULL AFTER `role`,
			ADD COLUMN `allowed_courses` varchar(45) DEFAULT NULL AFTER `allowed_clients`',
			$this->db->dbprefix('users')
		);
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to add user table columns');
		}
		log_message('info', "Readded users.role, users.allowed_clients, users.allowed_courses");

		# populate readded user table columns using the legacy user table view
		$sql = sprintf('UPDATE %1$s
			LEFT JOIN %2$s ON %1$s.user_id = %2$s.user_id
			SET	%1$s.role = %2$s.role,
					%1$s.allowed_courses = %2$s.allowed_courses,
					%1$s.allowed_clients = %2$s.allowed_clients',
			$this->db->dbprefix('users'),
			$this->db->dbprefix('users_legacy')
		);
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to repopulate user table columns');
		}
		log_message('info', "Repopulated users.role, users.allowed_clients, users.allowed_courses");

		# drop view providing legacy users table structure
		$sql = sprintf('DROP VIEW IF EXISTS %1$s', $this->db->dbprefix('users_legacy'));
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to drop legacy user table view');
		}
		log_message('info', "Dropped legacy view");

		# drop new users.super_admin column
		$sql = sprintf('ALTER TABLE %1$s DROP COLUMN super_admin', $this->db->dbprefix('users'));
		if (!$this->db->query($sql)) {
			throw new Exception('Unable to drop users.super_admin column');
		}
		log_message('info', "Dropped users.super_admin");

		# remove new lookup tables
		foreach (array('user_manager', 'user_course', 'user_client', 'group_course', 'group_client', 'groups') as $table_name) {
			$this->dbforge->drop_table($table_name);
			log_message('info', "Dropped table '$table_name'");
		}

		# revert user role table naming
		$this->dbforge->rename_table('roles', 'user_roles');
		$this->dbforge->modify_column('user_roles', array(
			'role_id' => array(
				'name' => 'user_role_id',
				'type' => 'INT',
			),
		));
		log_message('info', 'Renamed "roles" to "user_roles"');
	}
}
