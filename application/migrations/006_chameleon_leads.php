<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_chameleon_leads extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_leads` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `first_name` VARCHAR(45) NOT NULL ,
  `last_name` VARCHAR(45) NOT NULL ,
  `company` VARCHAR(45) NULL ,
  `email` VARCHAR(100) NOT NULL ,
  `job_title` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) );
EOSQL
);
	}

	public function down() {
		$this->dbforge->drop_table('cml_leads');
	}
}