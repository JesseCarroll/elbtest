<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_initial_tables extends CI_Migration {

	public function up() {

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_course_downloads` (
  `download_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `downloaded_by` int(11) NOT NULL,
  `date_downloaded` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `filename` varchar(100) NOT NULL,
  PRIMARY KEY (`download_id`),
  KEY `download_course_id_idx` (`course_id`),
  CONSTRAINT `download_course_id` FOREIGN KEY (`course_id`) REFERENCES `cml_courses` (`course_id`) )
DEFAULT CHARACTER SET = utf8;
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_courses` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `course_title` varchar(255) NOT NULL,
  `course_folder` varchar(45) DEFAULT NULL,
  `course_enabled_languages` text DEFAULT NULL,
  `course_version` varchar(10) NOT NULL,
  `course_status` enum('0','1','2') NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `date_created` timestamp DEFAULT NULL,
  `updated_by` int(11) NOT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_published` timestamp NULL DEFAULT NULL,
  `being_edited` enum('0','1') DEFAULT '0',
  PRIMARY KEY (`course_id`),
  KEY `course_client_id_idx` (`client_id`),
  CONSTRAINT `course_client_id` FOREIGN KEY (`client_id`) REFERENCES `cml_clients` (`client_id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_languages` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_shortcode` varchar(5) DEFAULT NULL,
  `language_description` varchar(45) DEFAULT NULL,
  `language_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `language_id_UNIQUE` (`language_id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `json_data` text,
  `resume_data` varchar(45) DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  `time_spent` int(11) DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_awards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  `course_id` int(11) DEFAULT '0',
  `event_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_badges` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `type` enum('points','multiplier','percent') NOT NULL,
  `value` int(11) DEFAULT '0',
  `category` enum('client','course') NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_quiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attempt_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `percent_complete` int(11) NOT NULL,
  `time_spent` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_lms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `token` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `user_id_UNIQUE` (`user_id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(5) NOT NULL DEFAULT 'ROLE',
  `role_title` varchar(45) NOT NULL DEFAULT 'Role Title',
  `role_status` enum('0','1','2') NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_role_id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);

		$this->db->query(<<<'EOSQL'
CREATE TABLE `cml_versions` (
  `version_id` int(11) NOT NULL AUTO_INCREMENT,
  `version_number` varchar(10) NOT NULL DEFAULT '1.0',
  `version_sub` varchar(10) NOT NULL,
  `version_status` enum('0','1','2') NOT NULL DEFAULT '0',
  `date_created` timestamp NULL DEFAULT NULL,
  `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`) )
COLLATE='utf8_general_ci'
ENGINE=InnoDB;
EOSQL
);
	}

	public function down() {
		$this->dbforge->drop_table('course_downloads');
		$this->dbforge->drop_table('courses');
		$this->dbforge->drop_table('languages');
		$this->dbforge->drop_table('lms_attempts');
		$this->dbforge->drop_table('lms_awards');
		$this->dbforge->drop_table('lms_badges');
		$this->dbforge->drop_table('lms_events');
		$this->dbforge->drop_table('lms_quiz');
		$this->dbforge->drop_table('lms_users');
		$this->dbforge->drop_table('user_roles');
		$this->dbforge->drop_table('versions');
	}
}