<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_curriculum_table extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_curriculum` (
  `curriculum_id` INT(11) NOT NULL AUTO_INCREMENT ,
  `curriculum_name` VARCHAR(150) NOT NULL ,
  `client_id` INT(11) NOT NULL ,
  `date_created` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `curriculum_status` ENUM('0','1') NULL DEFAULT '1' ,
  PRIMARY KEY (`curriculum_id`) ,
  UNIQUE INDEX `curriculum_id_UNIQUE` (`curriculum_id` ASC) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);
	}

	public function down() {
		$this->dbforge->drop_table('curriculum');
	}
}