<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_curriculum_count extends CI_Migration {
	public function up() {
		$this->dbforge->add_column('curriculum', array(
			'course_count' => array(
				'type'    => 'INT(11)',
				'null'    => true,
			),
		));
	}

	public function down() {
	  $this->dbforge->drop_column('curriculum', 'course_count');
	}
}
