<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_course_stage_table extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
CREATE  TABLE `cml_course_stage` (
  `id` INT(11) NOT NULL AUTO_INCREMENT ,
  `course_id` INT(11) NOT NULL ,
  `user_id` INT(11) NOT NULL ,
  `stage_id` INT(11) NOT NULL DEFAULT '0',
  `description` VARCHAR(150) NULL DEFAULT NULL ,
  `date_updated` TIMESTAMP NOT NULL ,
  PRIMARY KEY (`id`) )
DEFAULT CHARACTER SET = utf8;
EOSQL
);

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `course_status` `course_status` INT(11) NOT NULL DEFAULT '1' , ADD COLUMN `cloud_download` VARCHAR(100) NULL  AFTER `date_published` , ADD COLUMN `local_download` VARCHAR(100) NULL  AFTER `cloud_download` ;
EOSQL
);
	}

	public function down() {
		$this->dbforge->drop_table('course_stage');
    $this->dbforge->drop_column('cml_courses', 'cloud_download');
    $this->dbforge->drop_column('cml_courses', 'local_download');

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `course_status` `course_status` enum('0','1','2') NOT NULL DEFAULT '0';
EOSQL
);
	}
}