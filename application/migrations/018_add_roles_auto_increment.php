<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_add_roles_auto_increment extends CI_Migration {
	public function up() {
		$sql = sprintf('ALTER TABLE %1$s MODIFY COLUMN role_id INT auto_increment', $this->db->dbprefix('roles'));
		$query = $this->db->query($sql);
	}

	public function down() {
		$sql = sprintf('ALTER TABLE %1$s MODIFY COLUMN role_id INT', $this->db->dbprefix('roles'));
		$query = $this->db->query($sql);
	}
}
