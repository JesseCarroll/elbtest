<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_date_updated extends CI_Migration {
	public function up() {

    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `date_updated` `date_updated` timestamp NULL DEFAULT NULL;
EOSQL
);

	}

	public function down() {
    $this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` CHANGE COLUMN `date_updated` `date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;
EOSQL
);
	}
}