<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_course_notes extends CI_Migration {
	public function up() {
		$this->db->query(<<<'EOSQL'
ALTER TABLE `cml_courses` ADD COLUMN `course_notes` VARCHAR(10000) NULL  AFTER `course_curriculum` ;
EOSQL
);
	}

	public function down() {
	  $this->dbforge->drop_column('courses', 'course_notes');
	}
}