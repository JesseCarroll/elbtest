<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Aws\S3\S3Client;

class CI_Aws {
	private $s3;
	private $s3_bucket;
	private $config;

	public function __construct() {
		$proxy = array();
		if (ENVIRONMENT == 'development' && defined('PROXY_URL')) {
			$proxy['proxy'] = PROXY_URL;
			$proxy['verify'] = false;
		}

		$CI = get_instance();
		$this->config = $CI->config;

		# Try to get the name of the S3 bucket from configuration first before
		# relying on the S3_BUCKET constant; this makes it possible to override the
		# name of the S3 bucket in config files, the advantage of which being that
		# config files are not versioned in the development environment, so
		# customizing it there will not be noticed by the version control system.
		$this->s3_bucket = $this->config->item('s3_bucket', 'default');
		if (false === $this->s3_bucket) {
			$this->s3_bucket = S3_BUCKET;
		}

		$this->s3 = S3Client::factory(array(
			'region' => S3_REGION,
			'version' => S3_VERSION,
			'credentials' => array(
				'key'	 => AWS_KEY,
				'secret' => AWS_SECRET
			),
			'http' => $proxy
		));
	}

	/*
	 * Creates object on S3 Server
	 */
	function add_file($sourceFile, $destination, $type='') {
		$result = $this->s3->putObject(array(
			'Bucket' => $this->s3_bucket,
			'Key' => $destination,
			'SourceFile' => $sourceFile,
			'ContentType' => $type,
			'ACL' => S3_ACCESS_CONTROL
		));
	}

	/*
	 * Create directory
	 */
	function create_directory($bucket, $directory) {
		$result = $this->s3->putObject(array(
			'Bucket' => $bucket,
			'Key' => $directory."/",
			'Body' => "",
			'ACL' => S3_ACCESS_CONTROL
		));
	}

	function add_content($sourceData, $destination, $type='') {
		$result = $this->s3->putObject(array(
			'Bucket' => $this->s3_bucket,
			'Key' => $destination,
			'Body' => $sourceData,
			'ACL' => S3_ACCESS_CONTROL,
			'ContentType' => $type
		));
	}

	/*
	 * Creates "directory" on S3 Server
	 */
	function add_directory($source_path, $destination_path) {
		$result = $this->s3->uploadDirectory($source_path, $this->s3_bucket . '/' . $destination_path, '', array('ACL' => S3_ACCESS_CONTROL));
	}

	function download_directory($source_path, $destination_path) {
		$this->s3->downloadBucket(
			$destination_path,
			$this->s3_bucket,
			$source_path,
			array('ACL' => S3_ACCESS_CONTROL)
		);
	}

	function list_objects($source_path) {
		//$result = $this->s3->listObjects(array(
		$result = $this->s3->getIterator('ListObjects', array(
			'Bucket' => $this->s3_bucket,
			'Prefix' => $source_path
		));

		$results_arr = [];
		foreach($result as $object) {
			array_push($results_arr, $object);
		}

		//return $result['Contents'];
		return $results_arr;
	}

	/**
	 * Deletes all keys beginning with the specified prefix.  This can be used to
	 * delete entire non-empty directory structures, so this is a method to be
	 * used with great care.  The ONLY safeguard in place is an empty check on the
	 * prefix to make sure we don't try to delete every key in the entire bucket.
	 */
	function delete_by_prefix($prefix) {
		if (empty($prefix)) {
			throw new Exception('No prefix provided');
		}
		$arg = array(
			'Bucket' => $this->s3_bucket,
			'Prefix' => $prefix,
		);
		$result = $this->s3->listObjects($arg);
		if (isset($result['Contents'])) foreach ($result['Contents'] as $file_result) {
			$this->delete_by_key($file_result['Key']);
		}
	}

	function ls($source_path) {
		$arg = array(
			'Bucket' => $this->s3_bucket,
			'Prefix' => $source_path,
			'Delimiter' => '/',
		);
		$result = $this->s3->listObjects($arg);

		# post-process the response to create an associative array with filenames
		# as keys pointing to associative arrays of file information
		$ret_ar = array();

		# file results
		if (isset($result['Contents'])) foreach ($result['Contents'] as $file_result) {
			$path_part_ar = explode('/', $file_result['Key']);
			$last_path_part = array_pop($path_part_ar);
			if (empty($last_path_part)) {
				continue;
			}
			$last_modified_string = (string) $file_result['LastModified'];
			$ts = strtotime($last_modified_string);
			if (null === $ts) {
				throw new Exception("Unable to interpret string '$last_modified_string' as a time");
			}
			$ret_ar[$last_path_part] = array(
				'type'          => 'file',
				'last_modified' => $ts,
				'size'          => $file_result['Size'],
				'key'           => $file_result['Key'],
			);
		}

		# directory results
		if (isset($result['CommonPrefixes'])) foreach ($result['CommonPrefixes'] as $dir_result) {
			$result_path = str_replace(array($source_path, '/'), '', $dir_result['Prefix']);
			if (empty($result_path)) {
				continue;
			}
			$ret_ar[$result_path] = array(
				'type' => 'dir'
			);
		}

		return $ret_ar;
	}

	function zip_folder($source_path, $folder, $prefix='') {
		$CI = get_instance();
		$keys = $this->list_objects($source_path . $folder);

		foreach ($keys as $object) {
			if (substr($object['Key'], -1) !== '/') {
				$path = str_replace($source_path, '', $object['Key']);
				$content = $this->get_content($object['Key']);
				$CI->zip->add_data($prefix . $path, $content);
			}
		}
	}

	/*
	 * Copies object on S3 Server
	 */
	function copy_content($source_path, $destination_path) {
		$keys = $this->list_objects($source_path);
		if (null === $keys) {
			throw new Exception('Could not enumerate source path objects');
		}

		if (count($keys > 1)) {
			foreach ($keys as $object) {

				$path = ($source_path !== $object['Key']) ? str_replace($source_path, '', $object['Key']) : '';

				$this->s3->copyObject(
					array(
						'Bucket' => $this->s3_bucket,
						'CopySource' => urlencode($this->s3_bucket . '/' . $source_path . $path),
						'Key' => $destination_path . $path,
						'ACL' => S3_ACCESS_CONTROL
					)
				);
			}
		} else {
			$this->s3->copyObject(
				array(
					'Bucket' => $this->s3_bucket,
					'CopySource' => urlencode($this->s3_bucket . '/' . $source_path),
					'Key' => $destination_path,
					'ACL' => S3_ACCESS_CONTROL
				)
			);
		}
	}

	/**
	 * A special version of the "copy_content" method that can copy folders in the 'chameleon-hls' bucket.
	 */
	public function copy_content_hls($src_folder, $dst_folder) {
		$original_s3_bucket_value = $this->s3_bucket;
		$this->s3_bucket = 'chameleon-hls';
		try {
			$this->copy_content($src_folder, $dst_folder);
		}
		catch (Exception $e) {
			throw $e;
		}
		finally {
			$this->s3_bucket = $original_s3_bucket_value;
		}
	}

	/*
	 * Deletes object on S3 Server
	 */
	function delete_content($file) {
		// List the objects and get keys
		$keys = $this->s3->listObjects(
			array(
				'Bucket' => $this->s3_bucket,
				'Prefix' => $file
			)
		)->getPath('Contents/*/Key');

		// Delete the objects.
		$result = $this->s3->deleteObjects(array(
			'Bucket'  => $this->s3_bucket,
			'Objects' => array_map(function ($key) {
				return array('Key' => $key);
			}, $keys),
		));
	}

	/*
	 * Deletes a file on S3 identified by the specified key.
	 */
	function delete_by_key($key) {
		$result = $this->s3->deleteObject(array(
			'Bucket'  => $this->s3_bucket,
			'Key' => $key,
		));
	}

	/*
	 * Retrieve object content from S3 Server
	 */
	function get_content($file) {
		$result = $this->s3->getObject(array(
			'Bucket' => $this->s3_bucket,
			'Key' => $file
		));

		if ($result) {
			return (string) $result['Body'];
		}
	}

	function is_file($path) {
		return $this->s3->doesObjectExist($this->s3_bucket, $path);
	}

	function get_url($path) {
		return $this->s3->getObjectUrl($this->s3_bucket, $path);
	}

	/**
	 * Create signed authorization information that will allow a user's browser to
	 * upload a file directly to S3. Although it is possible to create more
	 * permissive authotization information, this method is strictly for
	 * authorizing the end user to upload file data to a specific S3 object key.
	 *
	 * $destination_file_key is expected to be a bucket-relative object key.
	 * $max_upload_size is expected to be the maximum number of bytes allowed to
	 * be uploaded.
	 *
	 * The array returned contains key/value pairs that should be used by the end
	 * user's browser, along with the data for the file that they are uploading,
	 * to make a POST request to the S3 bucket URL.
	 *
	 * This authorization information is calculated in accordance with
	 * documentation at
	 * http://docs.aws.amazon.com/AmazonS3/latest/dev/UsingHTTPPOST.html
	 */
	function directpost_form_fields($destination_file_key, $max_upload_size) {
		# Amazon wants your times to be in UTC, so here set the default timezone to
		# UTC, saving the original default so we can leave it set how we left it
		# when we're done
		$orig_tz = date_default_timezone_get();
		date_default_timezone_set('UTC');

		# create the signing key
		$signing_date_string = date('Ymd');
		$date_key = hash_hmac('sha256', $signing_date_string, 'AWS4' . AWS_SECRET, true);
		$date_region_key = hash_hmac('sha256', S3_REGION, $date_key, true);
		$date_region_service_key = hash_hmac('sha256', 's3', $date_region_key, true);
		$signing_key = hash_hmac('sha256', 'aws4_request', $date_region_service_key, true);

		# these variables appear both in the POST policy and in the form fields, so
		# we define them once here and package it up in both destinations below
		$policy_variable_ar = array(
			'bucket'                => $this->s3_bucket,
			'acl'                   => S3_ACCESS_CONTROL,
			'key'                   => $destination_file_key,
			'success_action_status' => '200',
			'x-amz-algorithm'       => 'AWS4-HMAC-SHA256',
			'x-amz-credential'      => sprintf('%s/%s/%s/s3/aws4_request', AWS_KEY, $signing_date_string, S3_REGION),
			'x-amz-date'            => "{$signing_date_string}T000000Z",
		);

		# create a POST policy
		$policy = new stdclass;
		$policy->expiration = date('Y-m-d\TH:i:s\Z', strtotime('now +1 day'));
		$policy->conditions = array();
		foreach ($policy_variable_ar as $k => $v) {
			$policy->conditions[] = array('eq', "\$$k", $v);
		}
		$policy->conditions[] = array('content-length-range', 0, $max_upload_size);
		$base64_encoded_policy = base64_encode(json_encode($policy));

		# create POST form fields
		$result = new stdclass;
		foreach ($policy_variable_ar as $k => $v) {
			$result->$k = $v;
		}
		$result->policy = $base64_encoded_policy;
		$result->{'x-amz-signature'} = hash_hmac('sha256', $base64_encoded_policy, $signing_key);

		# restore the original timezone setting
		date_default_timezone_set($orig_tz);

		return $result;
	}
}
