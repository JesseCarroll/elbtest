<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Media {

	private $config;
	private $aws;
	private $input;

	public function __construct() {

		$CI = get_instance();
		$CI->load->library('upload');
		$CI->config->load('default', true);
		$CI->config->load('upload', true);
		$CI->load->library('aws');
		log_message('debug', "Media Class Initialized");

		$this->config = $CI->config;
		$this->aws = $CI->aws;
		$this->input = $CI->input;
	}

	// --------------------------------------------------------------------

	function ingest($base_path) {
		if (empty($_FILES)) {
			throw new Exception('No uploaded files were found');
		}
		$file = array_shift($_FILES);
		if ($file['error'] != 0) {
		 	throw new Exception('There was an error related to the uploaded file');
		}

		# Where beneath the base path should this file go?
		#
		# If we received an 'id' request parameter, the file will always go into the
		# 'media' directory immediately beneath the base path, and it is the
		# caller's responsibility to ensure that the filename will not collide with
		# any other filename in the course.
		#
		# Otherwise, the file will be placed in various locations depending on the
		# classification we received in the 'type' request parameter.
		$classification = $this->input->post('type');
		$translation_language = $this->input->post('translationLanguage');
		$is_translation = (false !== $translation_language && 'false' !== $translation_language);
		$is_unique = ('false' != $this->input->post('isUnique'));
		$dst_filename = $this->input->post('name');
		$optimize = $this->input->post('optimize');
		if (empty($dst_filename)) {
			$dst_filename = $file['name'];
		}
		if ($is_translation) {
			$dst_dirpath = $base_path . "media/translation/$translation_language/";
		}
		else {
			if ($is_unique) {
				$dst_dirpath = $base_path . 'media/';
			}
			else {
				switch ($classification) {
				case 'client':
				case 'theme':
					$dst_dirpath = $base_path;
					break;
				case 'assets':
					$dst_dirpath = $base_path . 'assets/';
					break;
				case 'caption':
					$dst_dirpath = $base_path . 'media/video/';
					break;
				case 'adobe-edge':
					$dst_dirpath = $base_path . 'media/custom/';
					break;
				default:
					$dst_dirpath = $base_path . 'media/' . $classification . '/';
				}
			}
		}

		# force MIME type for weird extensions
		if (false !== strpos($dst_filename, '.vtt')) {
			$file['type'] = 'text/vtt';
		} else if (false !== strpos($dst_filename, '.mkv')) {
			$file['type'] = 'video/x-matroska';
		} else if (false !== strpos($dst_filename, '.flv')) {
			$file['type'] = 'video/x-flv';
		}

		# special processing for various media types
		$result = array();
		switch (true) {
		case 'adobe-edge' == $classification:

			# OVERVIEW: We received a compressed ZIP archive.  We need to create a
			# directory beneath the destination directory on S3 (which is given to us
			# in $dst_dirpath) that is named the same as the file being uploaded
			# (minus the .zip extension, if present).  Then we need to decompress the
			# contents of the archive into that directory.

			try {
				# create a temporary directory to hold the decompressed archive contents
				$temp_dirpath = tempnam(sys_get_temp_dir(), 'adobe_edge.');
				if (!unlink($temp_dirpath) || !mkdir($temp_dirpath)) {
					throw new Exception("Unable to convert temp file '$temp_dirpath' into directory");
				}

				# extract the archive to the temp directory
				$zip = new ZipArchive;
				$ret = $zip->open($file['tmp_name']);
				if (true !== $ret) {
					throw new Exception("Unable to open '{$file['tmp_name']}' as a ZIP archive ('$ret')");
				}
				$zip->extractTo($temp_dirpath);

				# locate all html files within the top level of the extracted data
				$dh = opendir($temp_dirpath);
				$inverted_html_filename_ar = array();
				while (false !== ($entryname = readdir($dh))) {
					if ('.html' == substr($entryname, -1 * strlen('.html'))) {
						$inverted_html_filename_ar[$entryname] = true;
					}
				}
				if (empty($inverted_html_filename_ar)) {
					throw new Exception('Unable to locate any .html files within archive');
				}
				$html_filename_ar = array_keys($inverted_html_filename_ar);
				closedir($dh);

				# If one of the .html files is named 'index.html', great!  But if not,
				# we need to rename one of the other .html files to 'index.html'
				# because front-end display code expects that it wil be present.  For
				# now, we'll make this really simple and just use the first .html file
				# in our listd, but this logic could be updated if necessary.
				if (!isset($inverted_html_filename_ar['index.html'])) {
					$src_filepath = "$temp_dirpath/{$html_filename_ar[0]}";
					$dst_filepath = "$temp_dirpath/index.html";
					if (!rename($src_filepath, $dst_filepath)) {
						throw new Exception("Unable to move '$src_filepath' to '$dst_filepath'");
					}
				}

				# load index.html into a DOMDocument so we can make modifications
				$d = new DOMDocument;
				libxml_use_internal_errors(true);
				if (false === $d->loadHTMLFile("$temp_dirpath/index.html")) {
					throw new Exception('Unable to load index.html into DOMDocument for manipulation');
				}
				libxml_clear_errors();
				$x = new DOMXPath($d);

				# modify body element
				$r = $x->query('/html/body');
				if (0 == $r->length) {
					throw new Exception('Could not locate body element in index.html');
				}
				$body_element = $r->item(0);
				$body_style_attr = $body_element->getAttribute('style');
				if (!empty($body_style_attr) && false !== stripos($body_style_attr, 'background-color:')) {
					$body_style_attr = preg_replace('!background-color:\s*[#a-zA-Z0-9]+;?!i', '', $body_style_attr);
					$body_element->setAttribute('style', $body_style_attr);
				}

				# modify canvas element ---- NO LONGER NEEDED. Adobe Animate CC now appends the canvas element dynamically
				# and allows for a designer to control its styles (make it transparent) - thus negating this function
				#
				# $r = $x->query('/html/body/canvas');
				# if (0 == $r->length) {
				#	throw new Exception('Could not locate canvas element in index.html');
				# }
				# $canvas_element = $r->item(0);
				# $canvas_element->removeAttribute('style');

				# isolate first script element in head
				# $r = $x->query('/html/head');
				# if (0 == $r->length) {
				#	throw new Exception('Unable to find head element');
				# }
				# $head_element = $r->item(0);

				# insert script element
				# $script_element = $d->createElement('script');
				# $script_element->setAttribute('src', 'https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.5.5/iframeResizer.contentWindow.min.js');
				# $head_element->appendChild($script_element);
//				$script_element->insertBefore($script_element);

				# write modified index.html to disk
				if (false === $d->saveHTMLFile("$temp_dirpath/index.html")) {
					throw new Exception('Could not write new index.html');
				}

				# call a method in the AWS library to synchronize the contents of the
				# temp directory with the target S3 location
				$dest_dirpath = $dst_dirpath . basename($dst_filename, '.zip');
				$this->aws->add_directory($temp_dirpath, $dest_dirpath);
			}
			catch (Exception $e) {
				throw $e;
			}
			finally {
				if (!empty($temp_dirpath)) {
					delete_files($temp_dirpath, true);
					unlink($temp_dirpath);
				}
			}

			break;

		case false !== strpos($file['type'], 'image'):
			$this->aws->add_file($file['tmp_name'], $dst_dirpath . $dst_filename);
			if(isset($optimize) && !empty($optimize) && $optimize === 'true') {
				$result = $this->optimize_images($dst_dirpath, $dst_filename);
			} else {
				$result['file_name'] = $dst_filename;
			}
			break;

		case false !== strpos($file['type'], 'video'):
			$this->aws->add_file($file['tmp_name'], $dst_dirpath . $dst_filename, $file['type']);
			$result = $this->optimize_videos($dst_dirpath, $dst_filename, $this->input->post('hls'), $this->input->post('audioTrack'), $translation_language);
			break;
		case false !== strpos($file['type'], 'audio'):
			$this->aws->add_file($file['tmp_name'], $dst_dirpath . $dst_filename, $file['type']);
			$result = $this->optimize_audio($dst_dirpath, $dst_filename);
			break;

		case 'resources':
			$this->aws->add_file($file['tmp_name'], $dst_dirpath . $dst_filename, $file['type']);
			switch (true) {
			case false !== strpos($file['type'], 'jpeg'):
				$result['type'] = 'jpg';
				break;
			case false !== strpos($file['type'], 'audio') || false !== strpos($file['type'], 'video'):
				$result['type'] = 'media';
				break;
			case false !== strpos($file['type'], 'pdf') || false !== strpos($file['type'], 'x-download'):
				$result['type'] = 'pdf';
				break;
			case false !== strpos($file['type'], 'excel') || false !== strpos($file['type'], 'spreadsheet'):
				$result['type'] = 'xls';
				break;
			case false !== strpos($file['type'], 'word'):
				$result['type'] = 'doc';
				break;
			case false !== strpos($file['type'], 'powerpoint'):
				$result['type'] = 'ppt';
				break;
			case 'text/plain' == $file['type']:
				$result['type'] = 'txt';
				break;
			default:
				$result['type'] = 'default';
			}
			$result['size'] = round($file['size'] / 1000);
			break;

		default:
			$result = $this->aws->add_file($file['tmp_name'], $dst_dirpath . $dst_filename, $file['type']);
		}

		return $result;
	}

	private function optimize_images($path, $filename) {
		$s3_path = $this->config->item('aws_path', 'default') . $path;

		$kraken = new Kraken(KRAKEN_API, KRAKEN_KEY);

		# Try to get the name of the S3 bucket from configuration first before
		# relying on the S3_BUCKET constant; this makes it possible to override the
		# name of the S3 bucket in config files, the advantage of which being that
		# config files are not versioned in the development environment, so
		# customizing it there will not be noticed by the version control system.
		$s3_bucket = $this->config->item('s3_bucket');
		if (false === $s3_bucket) {
			$s3_bucket = S3_BUCKET;
		}

		$params = array(
			'file'  => $s3_path . $filename,
			'json'  => true,
			'wait'  => true,
			'lossy' => true,
			's3_store' => array(
				'key'    => AWS_KEY,
				'secret' => AWS_SECRET,
				'bucket' => $s3_bucket,
				'path'   => $path . $filename,
				'acl'    => 'public_read'
			)
		);

		$data = $kraken->upload($params);
		return $data;
	}

	private function optimize_audio($path, $filename) {
		$input_url = $this->config->item('aws_path', 'default') . $path . $filename;

		// Switch extension to MP4
		$name = pathinfo($filename);
		$output_url = $this->config->item('aws_path', 'default') . $path . $name['filename'] . '.mp3';

		try {
			$zencoder = new Services_Zencoder(ZENCODER);
			$encoder_option_ar = array(
				'input' => $input_url,
				'outputs' => array(
					array(
						'label'             => 'web',
						'url'               => $output_url,
						'credentials'       => 'chameleon_editor',
						'public'            => true,
						'audio_bitrate'     => 96,
						'audio_codec'		=> "mp3"
					)
				)
			);
			$encoding_job = $zencoder->jobs->create($encoder_option_ar);
			return $encoding_job->id;
		} catch (Services_Zencoder_Exception $e) {
			// If we are here, an error occured
			echo "Fail :(\n\n";
			echo "Errors:\n";
			foreach ($e->getErrors() as $error) echo $error."\n";
			echo "Full exception dump:\n\n";
			print_r($e);
		}
	}

	private function optimize_videos($path, $filename, $hls, $audioTrack, $translation) {
		$input_url = $this->config->item('aws_path', 'default') . $path . $filename;

		// Switch extension to MP4
		$name = pathinfo($filename);
		$output_url = $this->config->item('aws_path', 'default') . $path . $name['filename'] . '.mp4';

		try {
		  $zencoder = new Services_Zencoder(ZENCODER);
		  if ($translation=="false"  ||  $translation == false){
		  	$hls_url = "https://s3.amazonaws.com/chameleon-hls/" . $name['filename'];
		  } else {
		  	$hls_url = "https://s3.amazonaws.com/chameleon-hls/" . $name['filename'] . "_" . $translation;
		  }
		  
		  
		  //Is HLS Encoding enabled in the course, otherwise we'll do regular encoding
		  if ($hls=="true") {
		  	$encoder_option_ar = array(
				'input' => $input_url,
				'outputs' => array(
					array('label' => 'web', 'url' => $output_url, 'credentials' => 'chameleon_editor', 'public' => true, 'height' => 720, 'width' => 1280, 'max_frame_rate' => 30, 'crf' => 23, 'h264_profile' => 'main', 'h264_level' => 3.1, 'audio_quality' => 3, 'audio_sample_rate' => 48000),
					array('audio_bitrate' => 64, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'filename' => 'file-64k.m3u8', 'format' => 'aac', 'public' => 1, 'type' => 'segmented'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 360, 'decoder_buffer_size' => 840, 'filename' => 'file-240k.m3u8', 'max_frame_rate' => 15, 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 184, 'width' => 400, 'format' => 'ts'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 578, 'decoder_buffer_size' => 1344, 'filename' => 'file-440k.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 384, 'width' => 400, 'format' => 'ts'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 960, 'decoder_buffer_size' => 2240, 'filename' => 'file-640k.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 584, 'width' => 400, 'format' => 'ts'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 1500, 'decoder_buffer_size' => 4000, 'filename' => 'file-1040k.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 1000, 'width' => 640, 'format' => 'ts'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 2310, 'decoder_buffer_size' => 5390, 'filename' => 'file-1540k.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 1484, 'width' => 960, 'format' => 'ts'),
					array('audio_bitrate' => 56, 'audio_sample_rate' => 22050, 'base_url' => $hls_url, 'decoder_bitrate_cap' => 3060, 'decoder_buffer_size' => 7140, 'filename' => 'file-2040k.m3u8', 'public' => 1, 'type' => 'segmented', 'video_bitrate' => 1984, 'width' => 1024, 'format' => 'ts'),
					array('base_url' => $hls_url, 'filename' => 'playlist.m3u8', 'public' => 1, 'type' => 'playlist', 'streams' => array( array('bandwidth' => 2040, 'path' => 'file-2040k.m3u8'), array('bandwidth' => 1540, 'path' => 'file-1540k.m3u8'), array('bandwidth' => 1040, 'path' => 'file-1040k.m3u8'), array('bandwidth' => 640, 'path' => 'file-640k.m3u8'), array('bandwidth' => 440, 'path' => 'file-440k.m3u8'), array('bandwidth' => 240, 'path' => 'file-240k.m3u8'), array('bandwidth' => 64, 'path' => 'file-64k.m3u8')))
				)
			);
		  } else {
		  	if ($audioTrack=="true"){ 
				$encoder_option_ar = array(
					'input' => $input_url,
					'outputs' => array(
						array('label' => 'web', 'url' => $output_url, 'credentials' => 'chameleon_editor', 'public' => true, 'height' => 720, 'width' => 1280, 'max_frame_rate' => 30, 'crf' => 23, 'h264_profile' => 'main', 'h264_level' => 3.1, 'audio_quality' => 3, 'audio_sample_rate' => 48000
						)
					)
				);
			} else {
				$encoder_option_ar = array(
					'input' => $input_url,
					'outputs' => array(
						array('label' => 'web', 'url' => $output_url, 'credentials' => 'chameleon_editor', 'public' => true, 'height' => 720, 'width' => 1280, 'max_frame_rate' => 30, 'crf' => 23, 'h264_profile' => 'main', 'h264_level' => 3.1, 'skip_audio' => true
						)
					)
				);
			}
		  }
		  $encoding_job = $zencoder->jobs->create($encoder_option_ar);
		  return $encoding_job->id;
		} catch (Services_Zencoder_Exception $e) {
		  // If we are here, an error occured
		  echo "Fail :(\n\n";
		  echo "Errors:\n";
		  foreach ($e->getErrors() as $error) echo $error."\n";
		  echo "Full exception dump:\n\n";
		  print_r($e);
		}
	}
}

// END Media Class

/* End of file Media.php */
/* Location: ./application/libraries/Media.php */
