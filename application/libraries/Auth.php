<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use \Firebase\JWT\JWT;

class CI_Auth {

	private $key = JWT_KEY;

	public function __construct() {}

	function generateToken($email, $expire) {

		$claims = [
			'nbf'     => time(),                // Not before
			'iat'     => time(),                // Issued at
			'exp'     => time() + 3600,         // Expires at
			'iss'     => 'BI Worldwide',        // Issuer
			'aud'     => $email,                // Audience
			'sub'     => 'Courseware Player',   // Subject
			'admin'   => false,                 // Custom claim
			'exp_key'	=> $expire								// Expiration Key (used to force expire a user)
		];

		$jwt = JWT::encode($claims, $this->key);

		return $jwt;
	}

	function decodeToken($jwt) {

		$decoded = JWT::decode($jwt, $this->key, array('HS256'));

		return $decoded;
	}
}