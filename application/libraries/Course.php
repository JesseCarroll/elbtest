<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Course {

	private $db;

	public function __construct() {
		$CI = get_instance();
		$this->db = $CI->db;
		log_message('debug', "Course Class Initialized");
	}

	/*
	 * Get information about the specified course.
	 *
	 * If a course field is supplied, return just the value of that field within 
	 * the specified course.  If not, return the entire course record as an 
	 * array.
	 */
	public function get_course_field($course_id, $course_field = null) {
		# select the row from the DB for the requested course ID
		$this->db->where('course_id', $course_id);
		$query = $this->db->get('courses');
		if (0 == $query->num_rows()) {
			$error_message = "Could not find course '$course_id'";
			if (null !== $course_field) {
				$error_message .= "; unable to determine '$course_field'";
			}
			throw new Exception($error_message);
		}
		$row = $query->row(0);

		# if a specific field was requested, return just that field;
		# otherwise, return the entire row
		if (null !== $course_field) {
			if (!is_string($course_field)) {
				throw new Exception('Requested field was not a string!');
			}
			if (!property_exists($row, $course_field)) {
				throw new Exception("Requested field '$course_field' does not exist in course record");
			}
			$result = $row->$course_field;
		}
		else {
			$result = $row;
		}

		# cache the result and return
		return $result;
	}
}

// END Course Class

/* End of file Course.php */
/* Location: ./application/libraries/Course.php */
