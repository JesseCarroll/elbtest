<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once FCPATH . 'vendor/tcpdf/tcpdf.php';

class CoursePdf extends TCPDF {
	private $course;
	private $client;
	private $aws_base_url;
	private $tmp_dirpath;
	private $img_dirpath;
	private $ci;
	private $client_logo_filename;
	private $course_json;
	private $chameleon_json;

	const PDF_ORIENTATION = 'P';
	const PDF_PAPER_SIZE = 'A4';
	const PDF_UNITS = 'mm';
	const PDF_PAGE_WIDTH = 210;
	const PDF_PAGE_HEIGHT = 297;
	const PDF_MARGIN_LEFT = 10;
	const PDF_MARGIN_RIGHT = 10;
	const PDF_MARGIN_BOTTOM = 10;
	const PDF_MARGIN_HEADER = 35;
	const PDF_MARGIN_FOOTER = 30;

	const PDF_CREATOR = 'Chameleon Courseware Editor';
	const PDF_AUTHOR = 'BI Worldwide';

	const COVER_PAGE_BIW_BRANDING_Y = 224;
	const COVER_PAGE_BIW_LOGO_HEIGHT = 33;

	const MENU_LEFT_OFFSET = 50;
	const MENU_INTRATOPIC_SPACING = 10;

	const RIGHT_RAIL_VERTICAL_SPACING = 3;
	const RIGHT_RAIL_WIDTH = 44;
	const RIGHT_RAIL_GUTTER_WIDTH = 6;
	const RIGHT_RAIL_MAX_IMAGE_HEIGHT = 50;

	const CONTENT_SECTION_DIVIDER_THICKNESS = .01;
	const CONTENT_SECTION_DIVIDER_SPACING = 5;

	const HEADER_TEMPLATE_TYPE_BOX_WIDTH = 50;

	const FOOTER_MAX_CLIENT_LOGO_WIDTH = 30;

	const FONT_FACE = 'dejavusans';

	public function __construct($course, $client) {
		$this->course = $course;
		$this->client = $client;

		# we need some time because we'll be fetching images
		set_time_limit(300);

		# get a reference to the code igniter instance
		$this->ci =& get_instance();

		# set up a temp directory for assets
		$tmp_dirpath = tempnam(sys_get_temp_dir(), 'tcpdf_assets.');
		if (false === $tmp_dirpath || !unlink($tmp_dirpath) || !mkdir($tmp_dirpath)) {
			throw new Exception('Unable to create temp directory');
		}
		$this->tmp_dirpath = $tmp_dirpath;
		$img_dirpath = "$tmp_dirpath/images";
		if (!mkdir($img_dirpath)) {
			throw new Exception('Unable to create images directory');
		}
		$this->img_dirpath = $img_dirpath;

		# clean up the temp directory when the process ends (NOTE: we don't do
		# this with the destructor because instances may make copies of
		# themselves and we don't want to clean up the temp directory until
		# they are ALL destroyed)
		register_shutdown_function(function($tmp_dirpath) {
			$cmd = sprintf('rm -r %s', escapeshellarg($tmp_dirpath));
			$out = array(); $ret = '';
			exec($cmd, $out, $ret);
		}, $this->tmp_dirpath);

		# variables
		$tcpdf_dirpath = FCPATH . 'vendor/tcpdf';
		$this->aws_base_url = $this->ci->config->item('aws_path', 'default');

		# download the course JSON
		$this->course_json = $course->load_course_json();
		if (false === $this->course_json) {
			throw new Exception('Unable to load course data');
		}
		#var_dump($this->course_json);exit;

		# download Chameleon JSON for the version that this course uses
		$this->chameleon_json = $course->load_chameleon_json($this->course_json['config']['version']);
		if (false === $this->chameleon_json) {
			throw new Exception('Unable to load version data');
		}

		parent::__construct(self::PDF_ORIENTATION, self::PDF_UNITS, self::PDF_PAPER_SIZE, true, 'UTF-8', false);
	}

	public function render() {
		# set up document properties
		$this->SetTitle($this->course->course_title);
		$this->SetSubject($this->course->description);
		$this->SetCreator(self::PDF_CREATOR);
		$this->SetAuthor(self::PDF_AUTHOR);
		$this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		$this->SetMargins(self::PDF_MARGIN_LEFT, self::PDF_MARGIN_HEADER, self::PDF_MARGIN_RIGHT);
		$this->SetImageScale(PDF_IMAGE_SCALE_RATIO);
		$this->SetFontSubsetting(false); # setting to false due to anecdotal evidence that it impacts PDF performance

		$this->progress('Resampling images');

		# download client logo and resample; we're resampling without resizing
		# because it seems to fix a buggy behavior in TCPDF where an image
		# with a transparent background isn't being rendered transparently
		$logo_filename = '';
		$logo_width = 0;
		try {
			if (empty($this->client->client_logo)) {
				$logo_url = $this->aws_base_url . 'core/images/logo.png';
			} else {
				$logo_url = $this->aws_base_url . $this->client->client_folder_path('images/' . $this->client->client_logo);
			}
			$logo_filepath = "{$this->img_dirpath}/logo.png";
			$this->download_asset($logo_url, $logo_filepath);
			$ret = getimagesize($logo_filepath);
			if (false === $ret) {
				throw new Exception('Unable to get image size');
			}
			list($logo_width, $logo_height) = $ret;
			$this->client_logo_filename = basename($logo_filepath);
			self::resample_image($logo_filepath);
		}
		catch (Exception $e) {
		}

		# prepare footer, doing as much as we can right now once so we don't have to do it every time we render a footer
		$this->PrepareFooter();

		# render all document sections; these should be built such that each
		# is independent from the others, where one does not rely on side
		# effects from another, so each should explicitly set page attributes
		# as they need to be set
		$this->render_cover_page(2);
		$this->render_home_page(2);
		$this->render_menu(2);
		$this->render_topics(90);
		$this->render_resources(2);
		$this->render_glossary(2);

		# output the PDF -- activating 'direct_render' makes the PDF data go
		# directly to stdout and shunts log messages to a log file in the
		# system temp directory, which can make for easier development/testing
		if (!empty($_GET['direct_render'])) {
			echo $this->Output('course.pdf', 'I');
			exit;
		}
		$pdf_data = $this->Output('course.pdf', 'S');
		$this->progress('Done!');
		$this->progress(100);
		return $pdf_data;
	}

	# render the cover page
	private function render_cover_page($progress_amount) {
		$this->progress('Rendering cover page');

		# this is guaranteed to be one page
		$this->SetAutoPageBreak(false);

		# start a page
		$this->SetPrintHeader(false);
		$this->AddPage();

		# bounding boxes for logo and course info, obtained from CHAM-3268 mock-up
		$logo_bounding_box = array();
		$logo_bounding_box['w'] = (181 - 35) / 215 * self::PDF_PAGE_WIDTH;
		$logo_bounding_box['h'] = (141 - 69) / 279 * self::PDF_PAGE_HEIGHT;
		$logo_bounding_box['x'] = self::PDF_PAGE_WIDTH / 2 - $logo_bounding_box['w'] / 2;
		$logo_bounding_box['y'] = 69 / 279 * self::PDF_PAGE_HEIGHT;
		$courseinfo_bounding_box = array(
			'x' => 43 / 215 * self::PDF_PAGE_WIDTH,
			'y' => 148 / 279 * self::PDF_PAGE_HEIGHT,
			'w' => (172 - 43) / 215 * self::PDF_PAGE_WIDTH,
			'h' => (199 - 148) / 279 * self::PDF_PAGE_HEIGHT,
		);
		//$this->render_bounding_box($logo_bounding_box);
		//$this->render_bounding_box($courseinfo_bounding_box);

		# client logo
		$ret = getimagesize("{$this->img_dirpath}/{$this->client_logo_filename}");
		if (false !== $ret) {

			# constrain logo dimensions to bounding box
			list($width_px, $height_px) = $ret;
			$height = $this->pixelsToUnits($height_px);
			$logo_w = min($logo_bounding_box['w'], $this->pixelsToUnits($width_px));
			$logo_h = $logo_w * $height_px / $width_px;
			if ($logo_h > $logo_bounding_box['h']) {
				$logo_h = $logo_bounding_box['h'];
				$logo_w = $logo_h * $width_px / $height_px;
			}

			# center logo vertically within bounding box
			$logo_y = $logo_bounding_box['y'] + ($logo_bounding_box['h'] - $logo_h) / 2;

			# render the logo
			$this->Image(
				"{$this->img_dirpath}/{$this->client_logo_filename}",
				0, # x
				$logo_y, # y
				$logo_w, # width
				$logo_h, # height
				'', # image type
				'', # link
				'M', # vertical align
				true, # resize
				300, # dpi used for resize
				'C', # center horizontally
				false,
				false,
				0,
				'CM'
			);
		}

		# course title
		$this->SetFont(self::FONT_FACE, 'B', 20, '', true);
		$this->SetTextColor(109);
		$this->SetXY($courseinfo_bounding_box['x'], $courseinfo_bounding_box['y']);
		$this->MultiCell($courseinfo_bounding_box['w'], 0, $this->course->course_title, 0, 'C');

		# other course info
		$this->SetXY($courseinfo_bounding_box['x'], $this->GetY() + 3); # 3 mm spacing beneath course title
		$this->SetFont(self::FONT_FACE, '', 14, '', true);
		$this->MultiCell($courseinfo_bounding_box['w'], 0, sprintf('Last updated %s', date('F j, Y', strtotime($this->course->date_updated))), 0, 'C');
		if (!empty($this->course->curriculum_name)) {
			$this->SetX($courseinfo_bounding_box['x']);
			$this->MultiCell($courseinfo_bounding_box['w'], 0, "Curriculum: {$this->course->curriculum_name}", 0, 'C');
		}
		$this->SetX($courseinfo_bounding_box['x']);
		if (!empty($this->course_json['config']['theme_id'])) {
			$this->MultiCell($courseinfo_bounding_box['w'], 0, "Theme: {$this->course_json['config']['theme_id']}", 0, 'C');
		}

		# BIW branding
		$text = 'A Chameleon training module outline from';
		$this->SetFont(self::FONT_FACE, '', 12, '', true);
		$text_height = $this->GetStringHeight(self::PDF_PAGE_WIDTH, $text);
		$spacing = 3;
		$this->SetTextColor(180);
		$this->SetAbsXY(0, self::COVER_PAGE_BIW_BRANDING_Y);
		$this->Cell(self::PDF_PAGE_WIDTH, 0, $text, '', 0, 'C');
		$this->Image(FCPATH . 'img/BILogo_DarkText.png',
			0, # x
			$this->GetY() + $this->GetLastH() + $spacing, # y
			0, # width
			self::COVER_PAGE_BIW_LOGO_HEIGHT,
			'', # image type
			'', # link
			'T', # vertical align
			true, # resize
			300, # dpi used for resize
			'C' # center horizontally
		);

		$this->SetPrintFooter(false);
		$this->progress($progress_amount);
	}

	private function render_menu($progress_amount) {
		$this->progress('Rendering menu');

		# set up page
		$this->SetAutoPageBreak(true, self::PDF_MARGIN_FOOTER);
		$this->SetHeaderText('Menu');
		$this->SetHeaderCircleColor(66);
		$this->SetHeaderCircleTextColor(255);
		$this->SetHeaderCircleText(null);
		$this->SetTemplate(null);
		$this->AddPage();

		# list all topics
		$topic_number = 1;
		$this->SetCellMargins(self::MENU_LEFT_OFFSET, 5, 0, 0);
		$this->SetTextColor(66);
		$text_width = self::PDF_PAGE_WIDTH - self::PDF_MARGIN_LEFT - self::PDF_MARGIN_RIGHT;
		foreach ($this->course_json['topic'] as $topic) {
			$this->SetFont(self::FONT_FACE, 'B', 9, '', true);
			$this->MultiCell($text_width, 0, "T{$topic_number}  {$topic['title']}", 0, 'L');

			$page_number = 1;
			foreach ($topic['page'] as $page) {
				$this->SetFont(self::FONT_FACE, '', 9, '', true);
				$this->MultiCell($text_width, 0, "P{$page_number}  {$page['title']}", 0, 'L');
				$page_number++;
			}

			$topic_number++;
			$this->SetY($this->GetY() + self::MENU_INTRATOPIC_SPACING);
		}
		$this->SetCellMargins(0, 0, 0, 0);

		$this->progress($progress_amount);
	}

	private function render_home_page($progress_amount) {
		$this->progress('Rendering home page');

		# find the template ID and named used for the home page
		$template_id = $this->course_json['home']['template'];

		# set up page
		$this->SetAutoPageBreak(true, self::PDF_MARGIN_FOOTER);
		$this->SetFooterText($this->course->course_title);
		$this->SetHeaderCircleColor(66);
		$this->SetHeaderCircleTextColor(255);
		$this->SetHeaderText('Home');
		$this->SetTemplate($template_id);
		$this->SetFooterText($this->course_json['course']);
		$this->SetPrintHeader(true);
		$this->AddPage();
		$this->SetPrintFooter(true);

		# calculate the width of the page allocated to text content
		$text_width = self::PDF_PAGE_WIDTH - self::PDF_MARGIN_LEFT - self::PDF_MARGIN_RIGHT - self::RIGHT_RAIL_WIDTH - self::RIGHT_RAIL_GUTTER_WIDTH;

		# set cursor to starting point
		$this->SetY(self::PDF_MARGIN_HEADER);

		# home title
		if (isset($this->course_json['home']['title'])) {
			$this->SetFont(self::FONT_FACE, 'B', 14, '', true);
			$this->SetTextColor(66);
			$this->MultiCell($text_width, 0, $this->course_json['home']['title'], 0, 'L');
		}

		# background media
		if (isset($this->course_json['home']['media'])) {
			$this->SetRightRailY($this->GetY());
			$this->render_media_box($this->course_json['home']['media']);
		}

		# home summary
		if (isset($this->course_json['home']['summary'])) {
			$this->SetY($this->GetY() + 5);
			$this->SetFont(self::FONT_FACE, '', 9, '', true);
			$this->WriteHTMLCell($text_width, 0, '', '', $this->course_json['home']['summary'], 0, 1);
		}

		# call to action
		if (isset($this->course_json['home']['callToAction'])) {
			$this->SetY($this->GetY() + 5);
			$this->SetFont(self::FONT_FACE, 'italic', 9, '', true);
			$this->SetTextColor(180);
			$this->MultiCell($text_width, 0, 'Call To Action', 0, 'L');

			$this->SetFont(self::FONT_FACE, 'B', 9, '', true);
			$this->SetTextColor(66);
			$this->MultiCell($text_width, 0, $this->course_json['home']['callToAction'], 0, 'L');
		}

		if ('home' == $template_id) {
			# start with a divider to set the topic list apart from the home page info
			$this->render_content_section_divider();

			# list all topics
			$topic_number = 1;
			foreach ($this->course_json['topic'] as $topic) {
				# topic title
				$this->render_html_with_label("Topic $topic_number", $topic['title']);

				# topic media
				if (!empty($topic['media'])) {
					$this->SetRightRailY($this->GetY());
					$this->render_media_box($topic['media']);
				}

				# topic description
				$this->render_html_with_label('Topic Description', $topic['description']);

				# end topic display with a divider
				$this->render_content_section_divider();

				$topic_number++;
			}
		}

		$this->progress($progress_amount);
	}

	private function render_topics($progress_amount) {
		$this->SetAutoPageBreak(true, self::PDF_MARGIN_FOOTER);
		$this->SetHeaderCircleColor(array(228, 242, 255));
		$this->SetHeaderCircleTextColor(array(79, 143, 246));
		$this->SetTemplate(null);
		$this->SetPrintHeader(true);

		# calculate the width of the page allocated to text content
		$text_width = self::PDF_PAGE_WIDTH - self::PDF_MARGIN_LEFT - self::PDF_MARGIN_RIGHT - self::RIGHT_RAIL_WIDTH - self::RIGHT_RAIL_GUTTER_WIDTH;

		# count total pages so we know how much progress each page represents
		$page_count = 0;
		foreach ($this->course_json['topic'] as $topic) {
			$page_count += count($topic['page']);
		}
		$page_progress_amount = $progress_amount / $page_count;

		$topic_number = 1;
		foreach ($this->course_json['topic'] as $topic) {
			$page_number = 1;
			foreach ($topic['page'] as $page) {
				$topic_page_designation = "T{$topic_number}P{$page_number}";
				$this->progress("Rendering $topic_page_designation");
				$is_quiz = (false !== strpos($page['template'], 'quiz') || in_array($page['template'], array('706', '707', 'peril')));
				$this->SetHeaderText(null);
				$this->SetTemplate($page['template']);
				$this->SetHeaderCircleText($topic_page_designation);
				$this->SetHeaderShowTemplateThumbnail(true);
				$this->AddPage();
				$this->SetHeaderShowTemplateThumbnail(false);
				$this->SetPrintFooter(true);

				# get information about the template selected for this page; this
				# comes from chameleon.json and just as we use this information for
				# deciding which tabs to display in the editor when editing a page, it
				# is useful for deciding which sections to render here
				$template_info = $this->GetTemplateInfo();

				#################################
				# PAGE OVERVIEW
				#################################

				# set cursor to starting point
				$this->SetY(self::PDF_MARGIN_HEADER);

				# page title (with topic title above in smaller lettering)
				$this->render_page_title($page['title'], $topic['title']);

				# course roles
				$this->render_html_with_label("Roles assigned to $topic_page_designation", empty($page['roles']) ? '(none)' : implode(', ', $page['roles']));

				# objectives (except on quiz pages -- CHAM-3268 says objectives are on the question level for quizzes)
				if (!empty($this->course_json['config']['objectives']) && false === strpos($page['template'], 'quiz')) {
					if (empty($page['objectives'])) {
						$text = '(none)';
					} else {
						$objective_title_ar = array();
						foreach (array_unique($page['objectives']) as $objective_id) {
							try {
								$objective = $this->get_objective($objective_id);
							}
							catch (Exception $e) {
								continue;
							}
							$objective_text = null;
							if (isset($objective['name'])) {
								$objective_text = $objective['name'];
							} elseif(isset($objective['title'])) {
								$objective_text = $objective['title'];
							}
							if (!empty($objective_text)) {
								$objective_title_ar[] = $objective_text;
							}
						}
						$text = implode(', ', $objective_title_ar);
					}
					$this->render_html_with_label("Objectives assigned to $topic_page_designation", $text);
				}

				# help prompt
				$this->render_html_with_label('Help Prompt', empty($page['prompt']) ? '(none)' : $page['prompt']);

				#################################
				# PAGE TEXT AND MEDIA
				#################################
				if (!empty($page['text']) || !empty($page['media']) || !empty($page['background'])) {
					$this->SetHeaderText('Text and Media');
					$this->AddPage();
					$this->SetHeaderText('Text and Media continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					# show media
					$this->SetRightRailY($this->GetY());
					if (!empty($template_info['background']['enable']) && isset($page['background']) && is_array($page['background'])) {
						$this->render_background_media_box($page['background']);
					}
					if (isset($page['media']) && is_array($page['media'])) {
						$this->render_media_box($page['media']);
					}

					# page text
					$this->SetCellMargins(0, 5, 0, 0);
					$this->SetFont(self::FONT_FACE, '', 9, '', true);
					$this->SetTextColor(66);
					$this->WriteHTMLCell($text_width, 0, '', '', $page['text'], 0, 1);
					$this->SetCellMargins(0, 0, 0, 0);
				}
				$this->EndContentSection();

				#################################
				# PAGE INTERACTION
				#################################
				if (!empty($template_info['interactions']['enable']) && !empty($page['interaction']) && is_array($page['interaction'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Interaction');
					$this->AddPage();
					$this->SetHeaderText('Interaction continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					for ($i = 0, $forcepage = false; $i < count($page['interaction']); $i++) {
						$interaction = $page['interaction'][$i];

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$this->AddPage();
						} else {
							$current_page = $this->PageNo();
							$this->startTransaction();
						}

						#  media
						if (!empty($interaction['media'])) {
							$this->SetRightRailY($this->GetY());
							$right_rail_page_break = $this->render_media_box($interaction['media']);
						}

						# label
						if (!empty($interaction['label'])) {
							$this->SetX(self::PDF_MARGIN_LEFT);
							$this->SetCellMargins(0, 5, 0, 0);
							$this->SetTextColor(66);
							$this->SetFont(self::FONT_FACE, 'B', 10, '', true);
							$this->MultiCell($text_width, 0, $interaction['label'], 0, 'L');
							$this->SetCellMargins(0, 0, 0, 0);
						}

						# text
						if (!empty($interaction['text'])) {
							$this->SetX(self::PDF_MARGIN_LEFT);
							$this->SetCellMargins(0, 5, 0, 0);
							$this->SetFont(self::FONT_FACE, '', 9, '', true);
							$this->SetTextColor(66);
							$this->WriteHTMLCell($text_width, 0, '', '', $interaction['text'], 0, 1);
							$this->SetCellMargins(0, 0, 0, 0);
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$forcepage = false;
							$this->commitTransaction();
						} else {
							if ($this->PageNo() != $current_page || !empty($right_rail_page_break) || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
								$this->rollbackTransaction(true);
								$i--;
								$forcepage = true;
								continue;
							}
						}

						# display a horizontal rule
						$this->render_content_section_divider();
					}
				}

				#################################
				# PAGE EXTRAS
				#################################
				if (!empty($template_info['extras']['enable']) && !empty($page['extra']) && is_array($page['extra'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Extras');
					$this->AddPage();

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					for ($i = 0, $forcepage = false; $i < count($page['extra']); $i++) {
						$extra = $page['extra'][$i];
						if (!isset($extra['type']) || empty($extra['title'])) {
							continue;
						}

						# we only know about 'tip-response' type extras right now;
						# this may be expanded to handle other types if they exist in the future
						if ('tip-response' != $extra['type']) {
							continue;
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$this->SetHeaderText('Extras continued');
							$this->AddPage();
						} else {
							$current_page = $this->PageNo();
							$this->startTransaction();
						}

						$this->render_html_with_label("Question/Answer Tip following paragraph {$extra['position']}", $extra['title']);

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$forcepage = false;
							$this->commitTransaction();
						} else {
							if ($this->PageNo() != $current_page || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
								$this->rollbackTransaction(true);
								$i--;
								$forcepage = true;
								continue;
							}
						}

						# display a horizontal rule
						$this->render_content_section_divider();
					}
				}

				#################################
				# QUIZ QUESTIONS
				#################################
				if ($is_quiz && !empty($page['question']) && is_array($page['question'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Questions');
					$this->AddPage();
					$this->SetHeaderText('Questions continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					for ($i = 0; $i < count($page['question']); $i++) {
						$question = $page['question'][$i];
						$question_number = $i+1;

						for ($pass = 1, $original_page_number = $this->PageNo(), $this->startTransaction(); $pass < 3; $pass++) {
							# question media
							if (!empty($question['media'])) {
								$this->SetRightRailY($this->GetY());
								$right_rail_page_break = $this->render_media_box($question['media']);
							}

							# question prompt
							$this->render_html_with_label("Question $question_number", empty($question['prompt']) ? '(no prompt)' : $question['prompt']);

							# question type
							$question_type_text = '';
							if (isset($this->chameleon_json['question_list'][$question['template']])) {
								if (is_string($this->chameleon_json['question_list'][$question['template']])) {
									$question_type_text = $this->chameleon_json['question_list'][$question['template']];
								}
								elseif (is_array($this->chameleon_json['question_list'][$question['template']])) {
									$question_type_text = $this->chameleon_json['question_list'][$question['template']]['name'];
								}
							}
							$this->render_html_with_label('Question Type', $question_type_text);

							# question objectives (if objectives are enabled)
							if (!empty($this->course_json['config']['objectives']) && !empty($question['objective'])) foreach ($question['objective'] as $objective_id) try {
								$objective = $this->get_objective($objective_id);

								if (isset($objective['name'])) {
									$this->render_html_with_label('Objective Title', $objective['name']);
								}

								if (isset($objective['description'])) {
									$this->render_html_with_label('Objective Description', $objective['description']);
								}
							}
							catch (Exception $e) {
								$this->progress($e->getMessage(), 'warning');
							}

							# correct feedback
							$this->render_html_with_label('Correct Feedback', $question['correct']);

							# final incorrect feedback
							$this->render_html_with_label('Final Incorrect Feedback', $question['incorrect']);

							# if we crossed a page boundary let's back up and put this section on a new page
							if (1 == $pass) {
								if ($this->PageNo() != $original_page_number || !empty($right_rail_page_break) || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
									$this->rollbackTransaction(true);
									$this->AddPage();
								} else {
									$this->commitTransaction();
									break;
								}
							}
						} 

						# end content section to bring the content level down
						# below the right rail content, in case the media for
						# the question is super long
						$this->EndContentSection();

						# distractors
						$this->SetLeftMargin(self::PDF_MARGIN_LEFT + 5);
						for ($j = 0; $j < count($question['distractor']); $j++) {
							$distractor = $question['distractor'][$j];
							$distractor_number = $j+1;

							# keep sections from being divided between pages if possible
							if ($forcepage) {
								$this->AddPage();
							} else {
								$current_page = $this->PageNo();
								$this->startTransaction();
							}

							# media
							if (!empty($distractor['media'])) {
								$this->SetRightRailY($this->GetY());
								$right_rail_page_break = $this->render_media_box($distractor['media']);
							}
							
							# content
							if (!empty($distractor['text'])) {
								$text = "{$distractor['text']}<br>" . ($distractor['value'] ? '[Correct Answer]' : '[Incorrect Answer]');
								$this->render_html_with_label("Question $question_number - Answer $distractor_number", $text);
							}
							if (!empty($distractor['feedback'])) {
								$this->render_html_with_label("Question $question_number - Answer $distractor_number Initial Incorrect Feedback", $distractor['feedback']);
							}

							# keep sections from being divided between pages if possible
							if ($forcepage) {
								$forcepage = false;
								$this->commitTransaction();
							} else {
								if ($this->PageNo() != $current_page || !empty($right_rail_page_break) || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
									$this->rollbackTransaction(true);
									$j--;
									$forcepage = true;
									continue;
								}
							}

							# end content section to bring the content level down
							# below the right rail content, in case the media for
							# the distractor is super long
							$this->EndContentSection();
						}
						$this->SetLeftMargin(self::PDF_MARGIN_LEFT);

						# display a horizontal rule
						$this->render_content_section_divider();
					}
				}

				#################################
				# CATEGORIES 
				#################################
				$category_ar = array();
				if (!empty($template_info['categories']['enable']) && !empty($page['categories'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Categories');
					$this->AddPage();
					$this->SetHeaderText('Categories continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					# render each category
					for ($i = 0, $forcepage = false; $i < count($page['categories']); $i++) {
						$category = $page['categories'][$i];
						$category_ar[$category['key']] = $category;
						if (empty($category['title'])) {
							continue;
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$this->AddPage();
						} else {
							$current_page = $this->PageNo();
							$this->startTransaction();
						}

						# media
						if (!empty($category['media'])) {
							$this->SetRightRailY($this->GetY());
							$right_rail_page_break = $this->render_media_box($category['media']);
						}

						# print heading for this category
						$this->render_html_with_label(sprintf('Category %d', $i+1), $category['title']);

						# text
						if (!empty($category['text'])) {
							$this->SetCellMargins(0, 5, 0, 0);
							$this->SetTextColor(66);
							$this->SetFont(self::FONT_FACE, '', 10, '', true);
							$this->WriteHTMLCell($text_width, 0, '', '', $category['text'], 0, 1);
							$this->SetCellMargins(0, 0, 0, 0);
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$forcepage = false;
							$this->commitTransaction();
						} else {
							if ($this->PageNo() != $current_page || !empty($right_rail_page_break) || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
								$this->rollbackTransaction(true);
								$i--;
								$forcepage = true;
								continue;
							}
						}

						# end item display with a divider
						$this->render_content_section_divider();
					}
				}

				#################################
				# ITEMS 
				#################################
				if (!empty($template_info['items']['enable']) && !empty($page['items'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Items');
					$this->AddPage();
					$this->SetHeaderText('Items continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					# render each category
					for ($i = 0, $forcepage = false; $i < count($page['items']); $i++) {
						$item = $page['items'][$i];
						if (empty($item['title'])) {
							continue;
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$this->AddPage();
						} else {
							$current_page = $this->PageNo();
							$right_rail_page_break = false;
							$this->startTransaction();
						}

						# print heading for this item
						$this->render_html_with_label(sprintf('Item %d', $i+1), $item['title']);

						# media
						if ('402' == $page['template']) {
							$this->SetRightRailY($this->GetY());
							$right_rail_page_break = $this->render_right_rail_image($page['media'][0], array(
								'hotspot' => array($item['x_position'], $item['y_position'])
							));

						} else {
							if (!empty($item['media'])) {
								$this->SetRightRailY($this->GetY());
								$right_rail_page_break = $this->render_media_box($item['media']);
							}
						}

						# text
						if (!empty($item['text'])) {
							$this->SetCellMargins(0, 5, 0, 0);
							$this->SetTextColor(66);
							$this->SetFont(self::FONT_FACE, '', 10, '', true);
							$this->WriteHTMLCell($text_width, 0, '', '', $item['text'], 0, 1);
							$this->SetCellMargins(0, 0, 0, 0);
						}

						# categories
						if (!empty($item['category'])) foreach ($item['category'] as $category_key) {
							$temp_ar = array();
							if (isset($category_ar[$category_key])) {
								$temp_ar[] = $category_ar[$category_key]['title'];
							}
							$this->render_html_with_label('Categories', implode(', ', $temp_ar));
						}

						# feedback
						if (!empty($item['correct'])) {
							$this->render_html_with_label('Feedback - Correct', $item['correct']);
						}
						if (!empty($item['incorrect'])) {
							$this->render_html_with_label('Feedback - Incorrect', $item['incorrect']);
						}
						if (!empty($item['final'])) {
							$this->render_html_with_label('Feedback - Final', $item['final']);
						}

						# keep sections from being divided between pages if possible
						if ($forcepage) {
							$forcepage = false;
							$this->commitTransaction();
						} else {
							if ($this->PageNo() != $current_page || !empty($right_rail_page_break) || $this->GetY() > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
								$this->rollbackTransaction(true);
								$i--;
								$forcepage = true;
								continue;
							}
						}

						# end item display with a divider
						$this->render_content_section_divider();
					}
				}

				#################################
				# SUMMARY
				#################################
				if (!empty($template_info['summary']['enable']) && !empty($page['summary']) && is_array($page['summary'])) {
					$this->SetHeaderShowTemplateThumbnail(false);
					$this->SetHeaderText('Summary');
					$this->AddPage();
					$this->SetHeaderText('Summary continued');

					# page title (with topic title above in smaller lettering)
					$this->render_page_title($page['title'], $topic['title']);

					# initial summary
					if (isset($page['summary']['initial'])) {
						# pass
						if (isset($page['summary']['initial']['pass']) && !empty($page['summary']['initial']['pass']['text'])) {
							$this->render_html_with_label('Initial Summary - Pass', $page['summary']['initial']['pass']['text']);
						}

						# fail
						if (isset($page['summary']['initial']['fail']) && !empty($page['summary']['initial']['fail']['text'])) {
							$this->render_html_with_label('Initial Summary - Fail', $page['summary']['initial']['fail']['text']);
						}

						# timeout
						if (isset($page['summary']['initial']['timeout']) && !empty($page['summary']['initial']['timeout']['text'])) {
							$this->render_html_with_label('Initial Summary - Timeout', $page['summary']['initial']['timeout']['text']);
						}
					}

					# final summary
					if (isset($page['summary']['final'])) {
						# pass
						if (isset($page['summary']['final']['pass']) && !empty($page['summary']['final']['pass']['text'])) {
							$this->render_html_with_label('Final Summary - Pass', $page['summary']['final']['pass']['text']);
						}

						# fail
						if (isset($page['summary']['final']['fail']) && !empty($page['summary']['final']['timeout']['text'])) {
							$this->render_html_with_label('Final Summary - Fail', $page['summary']['final']['fail']['text']);
						}

						# timeout
						if (isset($page['summary']['final']['timeout']) && !empty($page['summary']['final']['timeout']['text'])) {
							$this->render_html_with_label('Final Summary - Timeout', $page['summary']['final']['timeout']['text']);
						}
					}

					# unscored
					if (isset($page['summary']['unscored']) && !empty($page['summary']['unscored']['text'])) {
						$this->render_html_with_label('Unscored Summary', $page['summary']['unscored']['text']);
					}
				}

				$page_number++;
				$this->progress($page_progress_amount);
			}

			$topic_number++;
		}
	}

	private function render_resources($progress_amount) {
		$this->progress('Rendering resources');
		if (empty($this->course_json['resources'])) {
			$this->progress($progress_amount);
			return;
		}

		# set up page
		$this->SetAutoPageBreak(true, self::PDF_MARGIN_FOOTER);
		$this->SetHeaderText('Resources');
		$this->SetHeaderCircleColor(66);
		$this->SetHeaderCircleTextColor(255);
		$this->SetHeaderCircleText(null);
		$this->SetTemplate(null);
		$this->AddPage();
		$this->SetHeaderText('Resources continued');

		# list all resources
		$line_height = 10;
		$line_spacing = 3;
		$offset_left = 12;
		$icon_text_spacing = 3;
		$known_resource_type_ar = array('generic', 'doc', 'jpg', 'pdf', 'play', 'ppt', 'txt', 'xls');
		$resource_imagetype_filepath_ar = array();
		foreach ($this->course_json['resources'] as $resource) try {
			$type = !empty($resource['type']) ? $resource['type'] : 'generic';
			if (!in_array($type, $known_resource_type_ar)) {
				$type = 'generic';
			}
			if (!isset($resource_imagetype_filepath_ar[$type])) try {
				$filepath = "{$this->img_dirpath}/resource-type-$type.png";
				$this->download_asset($this->aws_base_url . "core/icons/type-$type.png", $filepath);
				$resource_imagetype_filepath_ar[$type] = $filepath;
			}
			catch (Exception $e) {
				throw new Exception("Work on that mapping -- not sure what to do with type $type: {$e->getMessage()}");
			}

			$ret = getimagesize($filepath);
			if (false === $ret) {
				throw new Exception("Unable to get size of '$filepath'");
			}
			list($width_px, $height_px) = $ret;
			$width_mm = $width_px * $line_height / $height_px;
			$this->Image(
				$resource_imagetype_filepath_ar[$type],
				self::PDF_MARGIN_LEFT + $offset_left, # x
				$this->GetY(), # y
				$width_mm, # width
				$line_height # height
			);
			$this->SetX(self::PDF_MARGIN_LEFT + $offset_left + $width_mm + $icon_text_spacing);
			$this->SetTextColor(66);
			$this->SetFont(self::FONT_FACE, 'B', 10, '', true);
			$this->Cell(0, $line_height, !empty($resource['label']) ? $resource['label'] : 'Unlabeled Resource');

			$this->SetY($this->GetY() + $line_height + $line_spacing);
		}
		catch (Exception $e) {
			$this->progress($e->getMessage(), 'warning');
		}

		$this->progress($progress_amount);
	}

	private function render_glossary($progress_amount) {
		$this->progress('Rendering glossary');
		if (empty($this->course_json['glossary'])) {
			$this->progress($progress_amount);
			return;
		}

		# set up page
		$this->SetAutoPageBreak(true, self::PDF_MARGIN_FOOTER);
		$this->SetHeaderText('Glossary');
		$this->SetHeaderCircleColor(66);
		$this->SetHeaderCircleTextColor(255);
		$this->SetHeaderCircleText(null);
		$this->SetTemplate(null);
		$this->AddPage();
		$this->SetHeaderText('Glossary continued');

		$width = (self::PDF_PAGE_WIDTH - self::PDF_MARGIN_LEFT - self::PDF_MARGIN_RIGHT) * .7;
		foreach ($this->course_json['glossary'] as $item) {
			$this->SetTextColor(66);
			$this->SetFont(self::FONT_FACE, 'B', 10, '', true);
			$this->SetCellMargins(self::PDF_MARGIN_LEFT, 5, 0, 0);
			$this->Cell($width, 0, $item['term'], '', 1);

			$this->SetCellMargins(self::PDF_MARGIN_LEFT, 0, 0, 0);
			$this->SetFont(self::FONT_FACE, '', 9, '', true);
			$this->WriteHTMLCell($width, 0, '', '', $item['definition'], 0, 1);
		}
		$this->SetCellMargins(0, 0, 0, 0);
	}

	# render a horizontal rule with padding at the top; updates the cursor
	# position for the main text area and that of the right rail to sit below
	# the rule
	private function render_content_section_divider() {
		$divider_thickness = .01;

		# end the content section to avoid drawing the divider through right rail content
		$this->EndContentSection();

		# avoid adding the divider if doing so would trigger a page break,
		# because it looks dumb to have a divider right at the top of the page
		if ($this->GetY() + self::CONTENT_SECTION_DIVIDER_THICKNESS + self::CONTENT_SECTION_DIVIDER_SPACING*2 > self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER) {
			return;
		}

		# spacing
		$this->SetY($this->GetY() + self::CONTENT_SECTION_DIVIDER_SPACING);

		# draw the divider
		$this->SetLineStyle(array('width' => .01, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(180)));
		$this->Cell(self::PDF_PAGE_WIDTH - self::PDF_MARGIN_LEFT - self::PDF_MARGIN_RIGHT, 0, '', 'T');

		# reset the cursor X position
		$this->SetY($this->GetY());
	}

	# render one of the most common configurations of text where a small,
	# italic gray label sits above a piece of content that may contain HTML
	private function render_html_with_label($label, $html) {
		$margins = $this->GetMargins();
		$width = self::PDF_PAGE_WIDTH - $margins['left'] - $margins['right'] - self::RIGHT_RAIL_WIDTH - self::RIGHT_RAIL_GUTTER_WIDTH;

		$this->SetCellMargins(0, 5, 0, 0);
		$this->SetTextColor(180);
		$this->SetFont(self::FONT_FACE, 'italic', 8, '', true);
		$this->MultiCell($width, 0, $label, 0, 'L');
		$this->SetCellMargins(0, 0, 0, 0);

		$this->SetTextColor(66);
		$this->SetFont(self::FONT_FACE, '', 10, '', true);
		$this->WriteHTMLCell($width, 0, '', '', $html, 0, 1);
	}

	# render a common configuration of text where a small, italic gray label
	# sits above a large, bold piece of text, typically at the top of the
	# page; advances the position of the right rail if it is currently sitting
	# at or above the level of the page title
	private function render_page_title($page_title, $topic_title = null) {
		$margins = $this->GetMargins();
		$width = self::PDF_PAGE_WIDTH - $margins['left'] - $margins['right'] - self::RIGHT_RAIL_WIDTH - self::RIGHT_RAIL_GUTTER_WIDTH;

		# print the topic title if it was provided
		if (null !== $topic_title) {
			$this->SetFont(self::FONT_FACE, 'italic', 9, '', true);
			$this->SetTextColor(180);
			$this->MultiCell($width, 0, $topic_title, 0, 'L');
		}

		# print the page title
		$this->SetFont(self::FONT_FACE, 'B', 14, '', true);
		$this->SetTextColor(66);
		$this->WriteHTMLCell($width, 0, '', '', $page_title, 0, 1);
	}

	# returns the X position of the leftmost edge of the right rail
	private function GetRightRailX() {
		return self::PDF_PAGE_WIDTH - self::RIGHT_RAIL_WIDTH - self::PDF_MARGIN_RIGHT;
	}

	# Modify internal state to advise other methods that we are now positioned
	# within the right rail.
	#
	# This implicitly brings the level of the right rail to the current
	# content level because we should enter and render right items at the
	# level of the content with which they are associated.
	#
	# The current position and page number is saved so we can restore it when
	# we exit the right rail.
	private function EnterRightRail() {
		if (!isset($this->right_rail_level)) {
			$this->right_rail_level = 0;
		}
		$this->right_rail_level++;
		if (1 == $this->right_rail_level) {
			# save current cursor position so we can restore it when exiting
			# the right rail
			$this->content_area_x = $this->GetX();
			$this->content_area_y = $this->GetY();
			$this->content_area_page = $this->PageNo();

			# set the cursor position to the current right rail position
			$this->SetXY($this->GetRightRailX(), $this->GetRightRailY());
		}
	}

	# Modify internal state to advise other methods that we are no longer
	# positioned within the right rail.
	#
	# We also restore the page number and cursor position to what they were at
	# prior to entering the right rail.
	private function ExitRightRail() {
		if (!isset($this->right_rail_level)) {
			$this->right_rail_level = 0;
		}
		$this->right_rail_level--;
		if ($this->right_rail_level <= 0) {
			$this->right_rail_level = 0;

			# save the page that the right rail is on, as we may actually jump
			# back to a previous page when exiting the right rail
			$this->right_rail_page = $this->PageNo();

			# restore the page and cursor position prior to entering the right rail
			$this->SetPage($this->content_area_page);
			$this->SetXY($this->content_area_x, $this->content_area_y);
		}
	}

	# Are we in the right rail currently?
	private function InRightRail() {
		return (isset($this->right_rail_level) && $this->right_rail_level > 0);
	}

	# set the level of the right rail
	private function SetRightRailY($n) {
		$this->right_rail_y = $n;
	}

	# get the level of the right rail
	private function GetRightRailY() {
		if (empty($this->right_rail_y)) {
			return 0;
		}
		return $this->right_rail_y;
	}

	# override TCPDF's SetY() so that if it is called while we're in the right
	# rail, it also updates the right rail level that we track internally;
	# obeys the $resetx parameter, but instead of setting X to the left side
	# of the page, sets X to the left side of the rail
	public function SetY($y, $resetx=true, $rtloff=false) {
		if ($this->InRightRail()) {
			if ($resetx) {
				$this->SetX($this->GetRightRailX());
			}
			$this->SetRightRailY($y);
		}
		return parent::SetY($y, $resetx, $rtloff);
	}

	# call this when a logical section of content in the main content area of
	# the page is finished; if the right rail is ahead of the content in the
	# main area of the page, this sets the page position and cursor position
	# so that it is level with the end of the right rail content; this allows
	# us to avoid situations where we are rendering main area content next to
	# unrelated right rail content
	private function EndContentSection() {
		if ($this->right_rail_page > $this->PageNo()) {
			$this->progress("Right rail is on page {$this->right_rail_page} but main content is still on page " .  $this->PageNo(), 'info');
			$this->progress("Setting page to {$this->right_rail_page} and Y to " . $this->GetRightRailY(), 'info');
			$this->SetPage($this->right_rail_page);
			$this->SetY($this->GetRightRailY());
		}
		else if ($this->GetY() < $this->GetRightRailY()) {
			$this->SetY($this->GetRightRailY());
		}
	}

	private function render_background_media_box($background) {
		# sanity checks
		if (!is_array($background) || !isset($background['type']) || !in_array($background['type'], array('image', 'video', 'color'))) {
			return;
		}

		$this->EnterRightRail();

		# position cursor at the appropriate starting place for new right rail content
		$this->SetY($this->GetY() + self::RIGHT_RAIL_VERTICAL_SPACING);

		# label this box as background media
		$this->SetX($this->GetRightRailX());
		$this->SetTextColor(180);
		$this->SetFont(self::FONT_FACE, 'italic', 8, '', true);
		$this->MultiCell(0, 0, "Background Media", '', 'L');

		# type label
		$this->SetX($this->GetRightRailX());
		$this->SetTextColor(66);
		$this->SetFont(self::FONT_FACE, '', 8, '', true);
		$this->MultiCell(0, 0, ucfirst($background['type']), '', 'L');

		# a little spacing
		$this->SetY($this->GetY() + 1);

		switch ($background['type']) {
		case 'color':
			$padding = 5;
			$this->SetFont(self::FONT_FACE, '', 11, '', true);
			if (empty($background['src'])) {
				$color_text = 'None selected';
				$dec_color = array('R' => 255, 'G' => 255, 'B' => 255);
			} else {
				$color_text = $background['src'];
				$dec_color = TCPDF_COLORS::convertHTMLColorToDec($background['src'], $this->spot_colors);
			}
			$box_height = $padding + $this->GetStringHeight(self::RIGHT_RAIL_WIDTH, "$color_text\n$color_text") + $padding;

			$this->Rect($this->GetRightRailX(), $this->GetY(), self::RIGHT_RAIL_WIDTH, $box_height, 'F', '', $dec_color);
			$this->SetY($this->GetY() + $padding);

			$this->SetX($this->GetRightRailX());
			$this->SetTextColor(100);
			$this->MultiCell(self::RIGHT_RAIL_WIDTH, 0, $color_text, '', 'C', false, 2);

			$this->SetX($this->GetRightRailX());
			$this->SetTextColor(255);
			$this->MultiCell(self::RIGHT_RAIL_WIDTH, 0, $color_text, '', 'C', false, 2);

			$this->SetY($this->GetY() + $padding);
			break;
		case 'image':
		case 'video':
			# image resides in different locations for backgrounds
			$media = array(
				'type' => 'image',
				'image' => array()
			);
			if ('image' == $background['type'] && !empty($background['src'])) {
				$media['image'][] = $background['src'];
			}
			elseif('video' == $background['type'] && !empty($background['src']) && !empty($background['src']['image'])) {
				$media['image'][] = $background['src']['image'];
			}
			if (isset($background['id'])) {
				$media['id'] = $background['id'];
			}

			$this->render_right_rail_image($media);
			break;
		default:
			$this->MultiCell(0, 0, "Unrecognized media type '{$media['type']}'", 'LTRB', 'L');
		}

		# restore the original cursor position
		$this->ExitRightRail();
	}

	# render all media items in the provided array in the right rail
	private function render_media_box($media_ar) {
		$page_no = $this->PageNo();
		$this->EnterRightRail();

		$ignored_media_types = array('custom', 'embed');
		foreach ($media_ar as $media) try {
			if (!isset($media['type']) || in_array($media['type'], $ignored_media_types)) {
				continue;
			}
			# standard spacing above media item to set it apart from previous
			# media item or header content
			$this->SetY($this->GetY() + self::RIGHT_RAIL_VERTICAL_SPACING);

			# label this box as a media asset
			$this->SetX($this->GetRightRailX());
			$this->SetTextColor(180);
			$this->SetFont(self::FONT_FACE, 'italic', 8, '', true);
			$this->MultiCell(0, 0, "Media", '', 'L');

			# show media type
			$this->SetX($this->GetRightRailX());
			$this->SetTextColor(66);
			$text = isset($this->chameleon_json['media_list'][$media['type']]) ?
				$this->chameleon_json['media_list'][$media['type']] :
				ucfirst($media['type']);
			$this->MultiCell(0, 0, $text, '', 'L');

			# a little spacing above the image
			$this->SetY($this->GetY() + 1);

			switch ($media['type']) {
			case 'gallery':
			case 'video':
			case 'image':
			case 'audio':
			case 'slideshow':
				# render the first image
				$this->render_right_rail_image($media);

				break;
			case 'pullquote':
				$this->SetCellPadding(1);
				$this->SetDrawColor(0, 0, 0);
				$this->WriteHTMLCell(0, 0, $this->GetRightRailX(), '', $media['caption'], '', 1);
				$this->SetCellPadding(0);
				break;
			case 'textBlock':
				$this->SetCellPadding(1);
				$this->SetDrawColor(0, 0, 0);
				$this->WriteHTMLCell(0, 0, $this->GetRightRailX(), '', $media['textBlock'], '', 1);
				$this->SetCellPadding(0);
				break;
			case 'fpo':
				$this->SetX($this->GetRightRailX());
				$this->MultiCell(self::RIGHT_RAIL_WIDTH, 0, "\nFor\nPosition\nOnly\n\n", 'ltrb', 'C', false, 2);
				break;
			case 'zoom':
			case 'before-after':
				# render both images with a divider separating them
				$divider_thickness = .4;
				$this->render_right_rail_image($media, array('index' => 0));
				$this->SetLineStyle(array('width' => $divider_thickness, 'cap' => 'butt', 'join' => 'miter', 'dash' => 4, 'color' => array(0)));
				$this->SetX($this->GetRightRailX());
				$this->Cell(self::RIGHT_RAIL_WIDTH, 0, '', 'T', 0, 'L');
				$this->SetY($this->GetY() + $divider_thickness);
				$this->render_right_rail_image($media, array('index' => 1));

				break;
			default:
				#$this->MultiCell(0, 0, "Unrecognized media type '{$media['type']}'", 'LTRB', 'L');
				$this->progress("Unrecognized media type '{$media['type']}'", 'warning');
			}

			# display the caption if present
			if ('pullquote' != $media['type'] && !empty($media['caption']) && strlen(trim($media['caption'])) > 0) {
				$this->SetY($this->GetY() + 1);
				$this->SetX($this->GetRightRailX());
				$this->SetFont(self::FONT_FACE, 'italic', 8, '', true);
				$this->SetTextColor(180);
				$this->MultiCell(0, 0, $media['caption'], '', 'L', false, 2);
			}

		}
		catch (Exception $e) {}

		$break_detected = ($this->PageNo() != $page_no);
		$this->ExitRightRail();
		return $break_detected;
	}

	# takes a single media item and renders its first image in the right rail;
	# if it does not have any images, render a placeholder image; caps the
	# height of the image in accordance with how RIGHT_RAIL_MAX_IMAGE_HEIGHT
	# is set; returns the height that the image spans
	private function render_right_rail_image($media, $options = array()) {
		$page_no = $this->PageNo();
		$this->EnterRightRail();
		$image_idx = isset($options['index']) ? $options['index'] : 0;

		# download the image
		list($filepath, $image_width_pixels, $image_height_pixels) = empty($media['image'][$image_idx]) ?
			$this->get_placeholder_image() :
			$this->download_course_image($media['image'][$image_idx], empty($media['id']));

		# make image as wide as right rail and scale height to match
		$image_width_mm = self::RIGHT_RAIL_WIDTH;
		$image_height_mm = $image_width_mm * $image_height_pixels / $image_width_pixels;

		# if height exceeds max right rail image height, cap the height and scale the width
		if ($image_height_mm > self::RIGHT_RAIL_MAX_IMAGE_HEIGHT) {
			$image_width_mm *= self::RIGHT_RAIL_MAX_IMAGE_HEIGHT / $image_height_mm;
			$image_height_mm = self::RIGHT_RAIL_MAX_IMAGE_HEIGHT;
		}

		# display the image
		$image_x = $this->GetRightRailX();
		$image_y = $this->GetY();
		$ret = $this->Image(
			$filepath,
			$this->GetRightRailX(), # x
			$this->GetY(), # y
			$image_width_mm, # width
			$image_height_mm # height
		);

		# draw hotspot
		if (!empty($options['hotspot'])) {
			list($x_pct, $y_pct) = $options['hotspot'];
			$this->SetAlpha(0.5);
			$this->SetDrawColor(255, 0, 0);
			$this->Circle(
				$image_x + $image_width_mm * $x_pct / 100,
				$image_y + $image_height_mm * $y_pct / 100,
				3, 0, 360, 'DF', array(), 0);
			$this->SetAlpha(1.0);
		}

		$this->SetY($this->GetY() + $image_height_mm);

		$break_detected = ($this->PageNo() != $page_no);
		$this->ExitRightRail();
		return $break_detected;
	}

	# render a bounding box, mostly useful for debugging purposes
	private function render_bounding_box($bounding_box_ar) {
		$this->Rect(
			$bounding_box_ar['x'],
			$bounding_box_ar['y'],
			$bounding_box_ar['w'],
			$bounding_box_ar['h'],
			'S'
		);
	}

	# override TCPDF's Header() method so we can control header rendering
	public function Header() {
		# circle in upper left
		$radius = 9;
		$circle_x = 20;
		$circle_y = 20;
		$fontsize = 18;
		$this->Circle($circle_x, $circle_y, $radius, 0, 360, 'F', array(), $this->GetHeaderCircleColor());

		# text inside of the circle; start with an approximate ideal font size
		# and if it is too big to fit in the circle with a little room to
		# spare (5% of the radius on either side), reduce font size until it fits
		$this->SetTextColorArray($this->GetHeaderCircleTextColor());
		$this->SetFont(self::FONT_FACE, '', 18, '', true);
		$textWidth = $this->GetStringWidth($this->GetHeaderCircleText());
		for (; $textWidth > $radius * 2 * .90; $fontsize--) {
			$this->SetFont(self::FONT_FACE, '', $fontsize, '', true);
			$textWidth = $this->GetStringWidth($this->GetHeaderCircleText());
		}
		$textHeight = $this->GetStringHeight($textWidth, $this->GetHeaderCircleText());
		$this->SetXY($circle_x - $radius, $circle_y - $textHeight/2);
		$this->Cell(2 * $radius, 0, $this->GetHeaderCircleText(), 0, 0, 'C');

		# text next to circle
		$this->SetXY($circle_x + $radius + 3, $circle_y - $textHeight/2);
		$this->SetTextColor(array(255, 0, 0));
		$this->SetFont(self::FONT_FACE, '', 18, '', true);
		$this->Cell(2 * $radius, 0, $this->GetHeaderText());

		# template type box in upper right
		$template_info = $this->GetTemplateInfo();
		if (!empty($template_info)) {
			$padding = 5;
			$line_spacing = 2;
			$useable_width = self::HEADER_TEMPLATE_TYPE_BOX_WIDTH - $padding * 2;

			# calculate the height needed for the box
			$this->SetFont(self::FONT_FACE, 'italic', 9, '', true);
			$text1 = 'Template Type';
			$text1height = $this->GetStringHeight($useable_width, $text1);
			$this->SetFont(self::FONT_FACE, 'B', 10, '', true);
			$template_type_text_height = $this->GetStringHeight($useable_width, $template_info['name']);
			$box_height = $padding + $text1height + $line_spacing + $template_type_text_height + $padding;
			if (!empty($template_info['image_filepath']) && $this->GetHeaderShowTemplateThumbnail()) {
				$image_width_mm = $useable_width;
				$image_height_mm = $image_width_mm * $template_info['image_height'] / $template_info['image_width'];
				$box_height += $line_spacing + $image_height_mm + .1;
			}

			# draw the box
			$this->SetDrawColor(235);
			$this->SetFillColor(247);
			$rect_x = self::PDF_PAGE_WIDTH - self::HEADER_TEMPLATE_TYPE_BOX_WIDTH - self::PDF_MARGIN_RIGHT;
			$rect_y = $circle_y - $radius;
			$this->RoundedRect($rect_x, $rect_y, self::HEADER_TEMPLATE_TYPE_BOX_WIDTH, $box_height, 5.0, '1111', '');

			# draw the contents
			$this->SetTextColor(155);
			$this->SetFont(self::FONT_FACE, 'italic', 9, '', true);
			$this->SetCellPaddings($padding, $padding, $padding, 0);
			$this->SetXY($rect_x, $rect_y);
			$this->MultiCell(self::HEADER_TEMPLATE_TYPE_BOX_WIDTH, $text1height, $text1, '', 'C', false, 1, '', '', true, 0, false, true);
			$this->SetCellPaddings(0, 0, 0, 0);

			$this->SetCellPaddings($padding, $line_spacing, $padding, 0);
			$this->SetX($rect_x);
			$this->SetFont(self::FONT_FACE, 'B', 10, '', true);
			$this->SetDrawColor(50);
			$this->MultiCell(self::HEADER_TEMPLATE_TYPE_BOX_WIDTH, $template_type_text_height, $template_info['name'], '', 'C', false, 1, '', '', true, 0, false, true);
			$this->SetCellPaddings(0, 0, 0, 0);

			if (!empty($template_info['image_filepath']) && $this->GetHeaderShowTemplateThumbnail()) {
				$this->Image(
					$template_info['image_filepath'],
					$rect_x + $padding, # x
					$this->GetY() + $line_spacing, # y
					$image_width_mm, # width
					$image_height_mm # height
				);
			}

			# after we're done drawing the header, TCPDF will set the cursor's
			# position to the top of the page, but in doing so it obeys page
			# margins; since the template information box can hang down lower
			# than the rest of the header, we set the top margin according to
			# the height of the template information box when we are in the
			# right rail to avoid right rail content colliding with the
			# template information box
			$this->SetTopMargin($this->InRightRail() ? ($rect_y + $box_height + $padding) : self::PDF_MARGIN_HEADER);
		}
	}

	# override TCPDF's Footer() method so we can control footer rendering
	public function Footer() {
		# client logo
		$this->Image(
			$this->footer_image_filepath,
			self::PDF_MARGIN_LEFT, # x
			$this->client_logo_footer_y, # y
			$this->client_logo_footer_width, # width
			$this->client_logo_footer_height # height
		);

		# print current footer text
		$this->SetFont(self::FONT_FACE, 'italic', 9, '', true);
		$this->SetTextColor(190);
		$footer_text_height = $this->GetStringHeight(self::PDF_PAGE_WIDTH, $this->GetFooterText());
		$this->SetXY(self::PDF_MARGIN_LEFT + $this->client_logo_footer_width, self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER/2 - $footer_text_height/2);
		$this->SetCellMargins(3, 0, 0, 0);
		$this->MultiCell(0, self::PDF_MARGIN_FOOTER/2, $this->GetFooterText(), '', 'L', false, 2, '', '', true, 0, false, true, 0, 'T', false);
		$this->SetCellMargins(0, 0, 0, 0);

		# print page number
		$this->SetFont(self::FONT_FACE, '', 12, '', true);
		$this->SetTextColor(109);
		$pagenumber_text_height = $this->GetStringHeight(self::PDF_PAGE_WIDTH, $this->GetAliasNumPage());
		$this->SetXY(self::PDF_PAGE_WIDTH - self::PDF_MARGIN_RIGHT*4 - $this->GetStringWidth('99999'), self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER/2 - $footer_text_height/2 - ($pagenumber_text_height - $footer_text_height));
		$this->SetXY($this->original_rMargin, $this->GetY());
		$this->MultiCell(0, self::PDF_MARGIN_FOOTER, '{{rsc:1.4}' . $this->GetAliasNumPage(), '', 'R', false, 2, '', '', true, 0, false, false, 0, 'T', false);
	}

	private function PrepareFooter() {
		# get the size of the client logo
		$client_logo_filepath = "{$this->img_dirpath}/{$this->client_logo_filename}";
		$ret = getimagesize($client_logo_filepath);
		if (false === $ret) {
			throw new Exception('Unable to get client logo dimensions');
		}
		list($width_px, $height_px, $image_type) = $ret;

		# convert to mm
		$this->client_logo_footer_height = $this->pixelsToUnits($height_px);
		$this->client_logo_footer_width = $this->pixelsToUnits($width_px);

		# if the logo is too wide, reduce it to fit the max width
		if ($this->client_logo_footer_width > self::FOOTER_MAX_CLIENT_LOGO_WIDTH) {
			$this->client_logo_footer_width = self::FOOTER_MAX_CLIENT_LOGO_WIDTH;
			$this->client_logo_footer_height = $this->client_logo_footer_width * $height_px / $width_px;
		}

		# if the logo takes up more than 70% of the height of the footer, reduce it to fit that span
		$footer_logo_max_height_pct = .7;
		$footer_logo_max_height = self::PDF_MARGIN_FOOTER * $footer_logo_max_height_pct;
		if ($this->client_logo_footer_height > $footer_logo_max_height) {
			$this->client_logo_footer_height = $footer_logo_max_height;
			$this->client_logo_footer_width = $this->client_logo_footer_height * $width_px / $height_px;
		}
		$this->client_logo_footer_y = self::PDF_PAGE_HEIGHT - self::PDF_MARGIN_FOOTER / 2 - $this->client_logo_footer_height / 2;

		# it is important that the footer image is not too large because
		# resampling a large image over and over again is very CPU intensive
		# and can slow down document rendering a lot
		try {
			# calculate the ideal size of the client logo that we could use in
			# the footer; the calculation here simply doing the opposite of
			# TCPDF's pixelsToUnits()
			$ideal_width_px = round($this->imgscale * $this->k * $this->client_logo_footer_width, 0);
			$ideal_height_px = round($this->imgscale * $this->k * $this->client_logo_footer_height, 0);

			# resample
			$this->footer_image_filepath = "{$this->img_dirpath}/logo_footer_resized.png";
			self::resample_image($client_logo_filepath, $this->footer_image_filepath, $ideal_width_px, $ideal_height_px);
		}
		catch (Exception $e) {
			# fall back to using the original client logo if resizing failed
			$this->footer_image_filepath = $client_logo_filepath;
			//var_dump($e->getMessage());exit;
		}
	}

	private static function resample_image($src_filepath, $dst_filepath = null, $dst_width_px = 0, $dst_height_px = 0) {
		# get image information
		$ret = @getimagesize($src_filepath);
		if (false === $ret) {
			throw new Exception('Unable to get image dimensions');
		}
		list($width_px, $height_px, $image_type) = $ret;

		# load the image
		switch ($image_type) {
		case IMAGETYPE_GIF:
			$src = imagecreatefromgif($src_filepath);
			break;
		case IMAGETYPE_JPEG:
			$src = imagecreatefromjpeg($src_filepath);
			break;
		case IMAGETYPE_PNG:
			$src = imagecreatefrompng($src_filepath);
			break;
		default:
			throw new Exception("Could not resize image -- unknown image type '$image_type'");
		}
		if (false === $src) {
			throw new Exception('Could not load image');
		}

		# default to source dimensions if new dimensions are not provided
		if (0 == $dst_width_px) {
			$dst_width_px = $width_px;
		}
		if (0 == $dst_height_px) {
			$dst_height_px = $height_px;
		}

		# create a destination canvas
		$dst = imagecreatetruecolor($dst_width_px, $dst_height_px);
		if (false === $dst) {
			throw new Exception('Could not create destination canvas');
		}
		if (!imagealphablending($dst, false)) {
			throw new Exception('Unable to set alpha blending on destination');
		}
		if (!imagesavealpha($dst, true)) {
			throw new Exception('Unable to set save alpha on destination');
		}

		# resample
		if (!imagecopyresampled($dst, $src, 0, 0, 0, 0, $dst_width_px, $dst_height_px, $width_px, $height_px)) {
			throw new Exception('Unable to resample image');
		}

		# write to disk
		if (null === $dst_filepath) {
			$dst_filepath = $src_filepath;
		}
		if (!imagepng($dst, $dst_filepath, 9)) {
			throw new Exception('Unable to save resized image');
		}
	}

	# set/get the text that appears to the right of the logo in the footer
	private function SetFooterText($s) {
		$this->footer_text = $s;
	}
	private function GetFooterText() {
		return isset($this->footer_text) ? $this->footer_text : '';
	}

	# set/get the color of the circle drawn in the upper left of the header
	private function SetHeaderCircleColor($v) {
		if (is_scalar($v)) {
			$v = array($v, $v, $v);
		}
		if (!is_array($v)) {
			throw new Exception('Not a scalar nor an array');
		}
		if (3 != count($v)) {
			throw new Exception('Wrong number of elements in array');
		}
		$this->header_circle_color = $v;
	}
	private function GetHeaderCircleColor() {
		if (!isset($this->header_circle_color)) {
			return array();
		}
		return $this->header_circle_color;
	}

	# set/get the color of the text drawn inside of the header circle
	private function SetHeaderCircleTextColor($v) {
		if (is_scalar($v)) {
			$v = array($v, $v, $v);
		}
		if (!is_array($v)) {
			throw new Exception('Not a scalar nor an array');
		}
		if (3 != count($v)) {
			throw new Exception('Wrong number of elements in array');
		}
		$this->header_circle_text_color = $v;
	}
	private function GetHeaderCircleTextColor() {
		if (!isset($this->header_circle_text_color)) {
			return array(255, 255, 255);
		}
		return $this->header_circle_text_color;
	}

	# set/get the text drawn to the right of the header circle
	private function SetHeaderText($s) {
		$this->header_text = $s;
	}
	private function GetHeaderText() {
		if (!isset($this->header_text)) {
			return '';
		}
		return $this->header_text;
	}

	# set the template that is displayed in the right portion of the header,
	# or clear it by passing in null
	private function SetTemplate($id) {
		if (null === $id) {
			$this->template_info = null;
		}

		# satisfy from cache if possible
		if (!isset($this->template_type_cache)) {
			$this->template_type_cache = array();
		}
		if (isset($this->template_type_cache[$id])) {
			$this->template_info = $this->template_type_cache[$id];
			return;
		}

		# init template info array
		$this->template_info = array();

		# home templates
		if (false !== strpos($id, 'home')) {
			# template name
			$this->template_info['name'] = $this->chameleon_json['home'][$id]['name'];
		}

		# other templates
		else foreach ($this->chameleon_json['templates'] as $template) {
			if ($id != $template['key']) {
				continue;
			}

			# template name
			$this->template_info = $template;

			# template image
			$this->template_info['image_filepath'] = "{$this->img_dirpath}/template-image-$id.png";
			$this->download_asset($this->aws_base_url . "core/templates/$id.png", $this->template_info['image_filepath']);
			$ret = getimagesize($this->template_info['image_filepath']);
			if (false === $ret) {
				throw new Exception("Unable to get template image dimensions for template '$id'");
			}
			list($this->template_info['image_width'], $this->template_info['image_height']) = $ret;
			break;
		}

		# store in cache
		$this->template_type_cache[$id] = $this->template_info;
	}

	# get information about the currently selected template
	private function GetTemplateInfo() {
		if (empty($this->template_info)) {
			return null;
		}
		return $this->template_info;
	}

	# set/get whether or not we show the template thumbnail when rendering the
	# header; if set to false and a template is set, we only show the template
	# name
	private function SetHeaderShowTemplateThumbnail($b) {
		$this->show_template_thumbnail = (bool) $b;
	}
	private function GetHeaderShowTemplateThumbnail() {
		if (!isset($this->show_template_thumbnail)) {
			return false;
		}
		return $this->show_template_thumbnail;
	}

	# set/get the text that is rendered inside of the circle in the header
	private function SetHeaderCircleText($s) {
		$this->header_circle_text = $s;
	}
	private function GetHeaderCircleText() {
		if (empty($this->header_circle_text)) {
			return 'n/a';
		}
		return $this->header_circle_text;
	}

	# download a course image, identified by filename; since legacy media
	# files are stored in a different location than where we usually store
	# media files, if the image belongs to a legacy media object it is
	# necessary use $is_legacy to disclose this so the image can be located
	private function download_course_image($image, $is_legacy = false) {

		# deal with the differing structure of gallery media
		if (is_array($image)) {
			if (isset($image['src'])) {
				$image = $image['src'];
			} else {
				return $this->get_placeholder_image();
			}
		}

		# calculate the URL for retrieving this asset
		$course_relative_path = 'pre/course/media';
		if ($is_legacy) {
			$course_relative_path .= '/images';
		}
		$course_relative_path .= '/' . urlencode($image);
		$url = $this->aws_base_url . $this->course->course_folder_path($course_relative_path);

		# where we'll store the asset locally
		$dst_filepath = "{$this->img_dirpath}/$image";

		# download the image
		$this->download_asset($url, $dst_filepath);

		# get the dimensions of the requested image
		$ret = getimagesize($dst_filepath);
		if (false === $ret) {
			throw new Exception("Unable to get image size from '$dst_filepath'");
		}
		list($width, $height) = $ret;

		# return image path and dimensions
		return array($dst_filepath, $width, $height);
	}

	# download content from a url and save to to the specified filepath
	private function download_asset($url, $dst_filepath) {
		# extension for storing the file's etag
		$etag_extension = 'etag';

		# begin a basic stream context
		$context_ar = array(
			'http' => array(),
			);

		# add context option to communicate by proxy if so configured
		if (ENVIRONMENT == 'development' && defined('PROXY_URL')) {
			$context_ar['http']['proxy'] = PROXY_URL;
		}

		# if we've stored an ETag for this asset, tell the server what it is
		# so it can return a 304 Not Modified status if the asset has not
		# changed
		if (is_file($dst_filepath) && is_file("$dst_filepath.$etag_extension")) {
			$etag_value = file_get_contents("$dst_filepath.$etag_extension");
			if (false !== $etag_value) {
				$context_ar['http']['header'] = "If-None-Match: \"$etag_value\"";
			}
		}

		# request the asset
		$data = @file_get_contents($url, false, stream_context_create($context_ar));
		if (false === $data) {
			#throw new Exception("Unable to fetch '$url'");
			copy(FCPATH . 'img/placeholder.jpg', $dst_filepath);
			return;
		}

		# process response headers
		foreach ($http_response_header as $header_string) {
			if (preg_match('#^HTTP/[0-9\.]+\s+([0-9]+)#', $header_string, $matches)) {
				$http_status_code = $matches[1];
		}
			if (preg_match('#^ETag:\s*"([^"]+)"#', $header_string, $matches)) {
				$etag_value = $matches[1];
			}
		}
		if (!isset($http_status_code)) {
			throw new Exception('No HTTP status code found: ' . print_r($http_response_header, true));
		}

		# take different actions depending on what the HTTP status code is
		switch ($http_status_code) {
		case '200':
			# write the response data to the destination filepath
			if (false === file_put_contents($dst_filepath, $data)) {
				throw new Exception("Unable to write data to '$dst_filepath'");
		}

			# if we got an ETag, store that too so we don't have to redownload this
			if (isset($etag_value)) {
				file_put_contents("$dst_filepath.$etag_extension", $etag_value);
			}
			break;
		case '304':
			# no need to do anything because the server said the asset is the same as what we've got
			break;
		default:
			throw new Exception("Unhandled HTTP status code '$http_status_code': " . print_r($http_response_header, true));
		}
	}

	# get a placeholder image for the situations where you would expect an
	# image to be there, but it just isn't; conforms to the same return value
	# format as download_course_image() so that their output can be used
	# interchangeably
	private function get_placeholder_image() {
		try {
			$filepath = FCPATH . 'img/placeholder.jpg';
			if (!is_file($filepath)) {
				throw new Exception("File '$filepath' not found");
			}
			$ret = getimagesize($filepath);
			if (false === $ret) {
				throw new Exception("Could not get dimensions of '$filepath'");
			}
			list($image_width_pixels, $image_height_pixels) = $ret;
			return array($filepath, $image_width_pixels, $image_height_pixels);
		}
		catch (Exception $e) {
			throw new Exception("Error getting placeholder image: {$e->getMessage()}");
		}
	}

	# the data in the course json isn't great for mapping objective ids to
	# their titles and descriptions, so this method generates one that is
	private function build_objective_map() {
		if (isset($this->objective_map)) {
			return;
		}
		$this->objective_map = array();
		foreach ($this->course_json['objectives'] as $objective) {
			$key = $objective['key'];
			unset($objective['key']);
			$this->objective_map[$key] = $objective;
		}
	}

	# retrieve the objective matching the provided ID
	private function get_objective($objective_id) {
		$this->build_objective_map();
		if (!isset($this->objective_map[$objective_id])) {
			throw new Exception("Objective with ID '$objective_id' does not exist");
		}
		return $this->objective_map[$objective_id];
	}

	# prints a labeled string to standard out, intended to be consumed either
	# by an XHR handler or by a developer looking at the JS console
	private function progress($arg, $label = 'message') {

		# assemble message
		if (is_string($arg)) {
			$line = "$label: $arg";
		}
		elseif (is_numeric($arg)) {
			if (!isset($this->progress_pct)) {
				$this->progress_pct = 0;
			}
			$this->progress_pct += $arg;
			$line = 'progress_pct: ' . floor($this->progress_pct);
		}
		$line .= "\n";

		# output message
		if (!empty($_GET['direct_render'])) {
			$log_filepath = sprintf('%s/%s-%s.log',
				sys_get_temp_dir(),
				str_replace('/', '_', __FILE__),
				date('Ymd')
			);
			file_put_contents($log_filepath, date('H:i:s') . " - $line", FILE_APPEND);
		}
		else {
			echo $line;
			flush();
		}
	}
}
/* End of file CoursePdf.php */
