<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config = array(
	'client/add_course' => array(
		array(
			'field' => 'course_title',
			'label' => 'Course Title',
			'rules' => 'required|trim|prep_for_form'
		),
		array(
			'field' => 'module_title',
			'label' => 'Module Title',
			'rules' => 'xss_clean|trim|prep_for_form'
		)
	)
);