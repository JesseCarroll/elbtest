<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Amazon AWS Settings
|--------------------------------------------------------------------------
|
| Global defaults for AWS settings and related third-party services
|
*/

define('AWS_PROFILE', 'chameleon_editor');
define('S3_REGION', 'us-east-1');
define('S3_VERSION', '2006-03-01');
define('S3_ACCESS_CONTROL', 'public-read');

define('KRAKEN_API', 'd8096c14a774e3fbcda3d2e1e4fc4b4e');
define('KRAKEN_KEY', '9ddaa6304f625ec5be29fa2f5a2b7f0645bb7b0c');

define('ZENCODER', 'c4ad7b60d332e43fb914e8d85d181ceb');

define('AWS_KEY', 'AKIAJGG3HEB4GORQTQHQ');
define('AWS_SECRET', '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');

define('JWT_KEY', 'nRnhXC754qNOH69xSKx7187j3q5f241h');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/* End of file constants.php */
/* Location: ./application/config/constants.php */