<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['kraken_on'] = true;
$config['zencoder_on'] = true;

//$config['config_name'] = 'config_value';
$config['course_theme_folder'] = 'pre/course/';
$config['course_media_folder'] = $config['course_theme_folder'] . 'media/';
$config['course_assets_folder'] = $config['course_theme_folder'] . 'assets/';

$config['assets']['upload_path'] = $config['course_theme_folder'] . 'assets/';
$config['assets']['allowed_types'] = 'png|gif|jpg';
$config['assets']['max_size'] = '2000';
$config['assets']['max_width']  = '1920';
$config['assets']['max_height']  = '1080';
$config['assets']['max_filename']  = 45;
$config['assets']['overwrite']  = FALSE;

$config['audio']['upload_path'] = $config['course_media_folder'] . 'audio/';
$config['audio']['allowed_types'] = 'mp3|wav';
$config['audio']['max_size'] = '5000';
$config['audio']['max_width']  = '0';
$config['audio']['max_height']  = '0';
$config['audio']['max_filename']  = 45;
$config['audio']['overwrite']  = FALSE;

$config['caption']['upload_path'] = $config['course_media_folder'] . 'video/';
$config['caption']['allowed_types'] = 'vtt';
$config['caption']['max_size'] = '2000';
$config['caption']['max_width']  = '0';
$config['caption']['max_height']  = '0';
$config['caption']['max_filename']  = 45;
$config['caption']['overwrite']  = FALSE;

$config['client']['upload_path'] = '';
$config['client']['allowed_types'] = 'png|gif|jpg';
$config['client']['max_size'] = '5000';
$config['client']['max_width']  = '0';
$config['client']['max_height']  = '0';
$config['client']['max_filename']  = 45;
$config['client']['overwrite']  = FALSE;

$config['custom']['upload_path'] = $config['course_media_folder'] . 'custom/';
$config['custom']['allowed_types'] = '*';
$config['custom']['max_size'] = '30000';
$config['custom']['max_width']  = '0';
$config['custom']['max_height']  = '0';
$config['custom']['max_filename']  = 45;
$config['custom']['overwrite']  = FALSE;

$config['image']['upload_path'] = $config['course_media_folder'] . 'images/';
$config['image']['allowed_types'] = 'png|gif|jpg';
$config['image']['max_size'] = '5000';
$config['image']['max_width']  = '0';
$config['image']['max_height']  = '0';
$config['image']['max_filename']  = 45;
$config['image']['overwrite']  = FALSE;

$config['logo']['upload_path'] = $config['course_assets_folder'];
$config['logo']['allowed_types'] = 'png';
$config['logo']['max_size'] = '5000';
$config['logo']['max_width']  = '0';
$config['logo']['max_height']  = '0';
$config['logo']['max_filename']  = 45;
$config['logo']['overwrite']  = TRUE;

$config['main']['upload_path'] = $config['course_theme_folder'] . 'data/';
$config['main']['allowed_types'] = 'js';
$config['main']['max_size'] = '5000';
$config['main']['max_width']  = '0';
$config['main']['max_height']  = '0';
$config['main']['max_filename']  = 45;
$config['main']['overwrite']  = FALSE;

$config['resources']['upload_path'] = $config['course_media_folder'] . 'resources/';
$config['resources']['allowed_types'] = '*';
$config['resources']['max_size'] = '15000';
$config['resources']['max_width']  = '0';
$config['resources']['max_height']  = '0';
$config['resources']['max_filename']  = 0;
$config['resources']['overwrite']  = FALSE;

$config['theme']['upload_path'] = $config['course_theme_folder'];
$config['theme']['allowed_types'] = 'css';
$config['theme']['max_size'] = '5000';
$config['theme']['max_width']  = '0';
$config['theme']['max_height']  = '0';
$config['theme']['max_filename']  = 45;
$config['theme']['overwrite']  = FALSE;

$config['video']['upload_path'] = $config['course_media_folder'] . 'video/';
$config['video']['allowed_types'] = 'mp4';
$config['video']['max_size'] = '30000';
$config['video']['max_width']  = '0';
$config['video']['max_height']  = '0';
$config['video']['max_filename']  = 45;
$config['video']['overwrite']  = FALSE;

/* End of file upload.php */
/* Location: ./application/config/upload.php */