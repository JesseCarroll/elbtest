<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

//$route['default_controller'] = "welcome";
//$route['404_override'] = '';

$route['default_controller'] = 'page';

$route['login'] = 'page/login';
$route['page/(:any)'] = 'page/index/$1';

$route['load/(:any)/(:any)'] = 'load/index/$1/$2';
$route['download/(:any)/(:any)'] = 'files/download_file/$2';
$route['download/(:any)'] = 'files/index/$1';

// analytics API
$route['api/analytics/(:num)'] = 'analytics/index/$1';

// theme API
$route['api/clients/(\d+)/settings/default_theme']        = 'theme/handle_request/default_theme/$1'; # get/put the default theme
$route['api/clients/(\d+)/themes/([^/]+)']                = 'theme/handle_request/json/$1/$2';       # get/put theme JSON
$route['api/clients/(\d+)/themes']                        = 'theme/handle_request/list/$1';          # list available theme IDs
$route['api/courses/(\d+)/themes/([^/]+)/css/([0-9\.]+)'] = 'theme/handle_request/css/$1/$2/$3';     # get CSS for the specified course/theme/chameleon version
$route['api/courses/(\d+)/phase/([^/]+)']                 = 'theme/handle_request/courseinfo/$1/$2'; # return course information for the specified phase of the specified course

// course repository
$route['api/courses/(\d+)/repository/s3_directpost/(.*)'] = 'api/s3_directpost/$1/15728640/cloud/repository/$2'; # (15 MB max upload size)
$route['api/courses/(\d+)/repository(?:/([^/]+))?'] = 'api/courserepository/$1/$2';

// Learning Snacks
$route['snack/(:any)'] = 'snack/$1';

// LMS API
$route['lms/(:any)/(:any)'] = 'lms/$1/$2';
$route['lms/(:any)'] = 'lms/$1';

// Version routes
$route['version/get_subversion/(:any)'] = 'version/get_subversion/$1';

// Maintenance routes
$route['maintenance'] = 'maintenance';
$route['maintenance/(:any)'] = 'maintenance/$1';

// Pagination routes
$route['(:any)/index'] = '$1/index/';
$route['(:any)/index/(:any)'] = '$1/index/';

$route['(:any)/(:num)/(:any)/(:any)/get_data'] = '$1/get_data';
$route['(:any)/(:num)/(:any)/(:any)/upload'] = '$1/upload';
$route['(:any)/(:num)/(:any)/get_data'] = '$1/get_data';
$route['(:any)/(:any)/get_data'] = '$1/get_data';

// Courses have many translations
$route['course/(:num)/translations/(:any)/update_translation'] = 'course/update_translation/$2';

$route['course/(:any)/get_translation/(:any)'] = 'course/get_translation/$2';
// course download
$route['course/(:any)/download/(:any)'] = 'course/download/$2/$3';
// specific course functions
$route['course/(:num)/(:any)'] = 'course/$2/$1';
// course dashboard
$route['course/(:num)'] = 'course/dashboard';
// non-specific course functions
$route['course/(:any)'] = 'course/$1';

// Basic RESTFUL routes
$route['([a-z]+)'] = '$1/index';
$route['(:any)/new'] = '$1/info';
$route['(:any)/settings'] = '$1/settings';
$route['(:any)/(:any)/edit'] = '$1/info/$2';
$route['(:any)/(:any)/settings'] = '$1/settings/$2';
$route['(:any)/(:any)/delete'] = '$1/delete/$2';
$route['(:any)/(:any)/profile'] = '$1/profile/$2';

// specific client functions
$route['client/(:num)/theme/(:any)'] = 'client/theme/$1/$2';
$route['client/(:num)/(:any)'] = 'client/$2/$1';
$route['client/(:num)'] = 'client/index/$1';
$route['client/(:any)'] = 'client/$1';

// specific user functions
$route['user/(:num)/profile/(:any)'] = 'user/$2/$1';

$route['ajax/(:any)'] = 'ajax/$1';

$route['(:any)/(:any)'] = '$1/$2';

$route['(:any)'] = 'page/index/$1';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
