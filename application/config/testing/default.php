<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//$config['config_name'] = 'config_value';
$config['site_name'] = 'Chameleon Testing';
$config['contactus_name'] = 'Chameleon';
$config['contactus_email'] = 'no-reply@chameleon.biworldwide.com';

$config['cloud_service_path'] = $_SERVER['DOCUMENT_ROOT'] . '/';
$config['cloud_service_url'] = 'https://int.chameleon-elearning.com/';

$config['core_cloud_service_path'] = $config['cloud_service_path'] . 'core/';
$config['core_cloud_service_url'] = $config['cloud_service_url'] . 'core/';

$config['course_core_path'] = $config['core_cloud_service_path'] . 'courses/';
$config['course_core_url'] = $config['core_cloud_service_url'] . 'courses/';

$config['version_core_path'] = $config['core_cloud_service_path'] . 'versions/';
$config['version_core_url'] = $config['core_cloud_service_url'] . 'versions/';

$config['client_path'] = $config['cloud_service_path'] . 'clients/';
$config['client_url'] = $config['cloud_service_url'] . 'clients/';

$config['course_template_folder'] = $config['core_cloud_service_path'] . 'master/';

$config['chameleon_root_placeholder'] = '[CHAMELEONURL]';
$config['course_key_placeholder'] = '[COURSEKEY]';
$config['course_stage_placeholder'] = '[COURSESTAGE]';
$config['client_version_url_placeholder'] = '[CLIENTVERSIONURL]';
$config['timestamp'] = '[TIMESTAMP]';
$config['version_load_url_placeholder'] = '[VERSIONLOADURL]';
$config['manifest_placeholder'] = '[IMSMANIFESTCOURSETITLE]';
$config['manifest_placeholder_id'] = '[IMSMANIFESTCOURSEID]';

$config['cdn_url_placeholder'] = '[CDNURL]';
$config['course_id_placeholder'] = '[COURSEID]';

// AWS Settings
$config['cdn_url'] = 'https://s3.amazonaws.com/';
$config['aws_root'] = 's3://' . S3_BUCKET . '/';
$config['aws_path'] = $config['cdn_url'] . S3_BUCKET . '/';

$config['cloudfront_path'] = $config['aws_path'];
//$config['cloudfront_path'] = '//d2uxstvvd7fkpb.cloudfront.net' . '/';

$config['master_theme'] = '_chameleon_variables.less';

$config['client_path'] = 'clients/';
$config['course_path'] = 'courses/';

$config['core_path'] = 'core/';
$config['api_path'] = $config['core_path'] . 'apis/';
$config['assets_path'] = $config['core_path'] . 'assets/';
$config['html_path'] = $config['core_path'] . 'html/';
$config['master_path'] = $config['core_path'] . 'master/';
$config['third_party_path'] = $config['core_path'] . 'third_party/';
$config['version_path'] = $config['core_path'] . 'versions/';

$config['master_theme_path'] = $config['master_path'] . $config['master_theme'];
$config['master_json'] = $config['master_path'] . 'pre/course/data/main.js';

$config['adobe_image_editor_api_key'] = 'b9382093bf8c4035b9cea261cdee9882';

/* End of file default.php */
/* Location: ./application/config/production/default.php */
