<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Ported from Ruby's decamelize and camelize, as from
// http://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case
if (!function_exists('decamelize')){
    function decamelize($word) {
      return preg_replace_callback(
        '/(^|[a-z])([A-Z])/', 
        function($m) {
            return strtolower(strlen($m[1]) ? $m[1] . '_' . $m[2] : $m[2]);
        },
        $word 
      ); 
    }
}

if (!function_exists('camelize')){
    function camelize($word) { 
      return preg_replace('/(^|_)([a-z])/', function($m) { return strtoupper($m[2]); }, $word); 
    }    
}

// END MY String helper