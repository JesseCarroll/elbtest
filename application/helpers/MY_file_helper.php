<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// copy_file() is not in the File Helper, so it defines a new function
if(!function_exists('copy_files')){
    function copy_files($source, $dest)
    {
        $dir = opendir($source);
        @mkdir($dest);
        while(false !== ( $file = readdir($dir)) ) {
            if (( $file != '.' ) && ( $file != '..' ) && ( $file != '.DS_Store' )) {
                if ( is_dir($source . '/' . $file) ) {
                    copy_files($source . '/' . $file, $dest . '/' . $file);
                }
                else {
                    copy($source . '/' . $file, $dest . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
}

if(!function_exists('delete_folder')){
    function delete_folder($dirPath){
        foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS| FilesystemIterator::UNIX_PATHS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
        }
        rmdir($dirPath);
    }
}

if(!function_exists('folder_permit')){
    function folder_permit($path) {
        if (!is_dir($path)) {
            mkdir($path, 0777, TRUE);
        }
        else {
            if(!is_writable($path)) {
                chmod($path, 0777);
            }
        }
    }
}


/*
 * Package Name : defines the name assigned to downloadable packages
 * @param {string} name - course title
 * @param {string} phase - current phase of course (stage or production)
 */
if(!function_exists('package_name')){
    function package_name($name) {
        $max_length = 50;
        $package_name = strtolower($name);
        $package_name = substr(preg_replace('/[^a-z0-9-]+/', '', $package_name), 0, $max_length);
        return $package_name;
    }
}

// END MY File Helper

/* End of file MY_file_helper.php */
/* Location: ./application/helpers/MY_file_helper.php */