<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SessionStatus_class {
	const EX_GRACEFULLY_ABORT_HOOK = 1;

	# a reference to the instance of the CI class
	private $ci;

	public function __construct() {
		$this->ci =& get_instance();
	}

	/*
	 * run() is the main entry point of the hook, but it only executes when the 
	 * GET parameter 'session_status_hook' has been passed in and is set to a 
	 * non-empty value.  run() is invoked in two phases of the CI setup process, 
	 * mainly so that it is possible to voluntarily update or preserve the 
	 * expiration time of CI's session as needed. 
	 *
	 * The 'action' GET parameter is expected to contain the name of a method 
	 * within this class to execute. The selected method is then executed and the 
	 * result is collected and returned to the caller in JSON format.  If an 
	 * error occurs, the error message is also returned in JSON format.  In 
	 * either case, execution always halts after the hook code is finished 
	 * executing.
	 *
	 * In the event that handler code wishes to end handling for the current hook 
	 * phase without returning any JSON and without terminating the PHP process, 
	 * the handler code may throw an exception having the code 
	 * EX_GRACEFULLY_ABORT_HOOK.  Action methods can take advantage of this in 
	 * order to segment their code into portions that run during each phase.
	 */
	public function run_pre_controller() {
		$this->run('pre_controller');
	}
	public function run_post_controller_constructor() {
		$this->run('post_controller_constructor');
	}
	private function run($phase) {
		# hook has not been activated, so allow normal request handling to proceed
		if (empty($_GET['chameleoneditor_session_hook'])) {
			return;
		}

		# initialize return array
		$ret_ar = array();
		try {
			if (empty($_GET['action'])) {
				throw new Exception('GET does not contain "action" so there is nothing to do');
			}
			$method_name = $_GET['action'];
			if (!method_exists($this, $method_name)) {
				throw new Exception("No handler method called '$method_name' was found");
			}
			$ret_ar['result'] = $this->$method_name($phase);
		}
		catch (Exception $e) {
			# exit hook without returning results JSON or terminating PHP
			if (self::EX_GRACEFULLY_ABORT_HOOK == $e->getCode()) {
				return;
			}
			$ret_ar['error'] = $e->getMessage();
		}

		# return results
		echo json_encode($ret_ar);
		exit;
	}


	/*************************************************************************
	 * ACTION METHODS
	 *
	 * These methods are entry points that can be invoked by specifying one an 
	 * "action" parameter as described above in the run() method.
	 *************************************************************************/

	# Does the user still have a valid session?  How many seconds are left in it?
	private function check_validity($phase) {

		# If we're in the "pre_controller" phase, we have the opportunity to set 
		# config values prior to the controller (and session) objects being set up.  
		# We set the sess_time_to_update value ridiculously high in order to 
		# prevent the CI_Session constructor from updating the expiration time of 
		# the session, because all we want to do is check the validity of the 
		# session, NOT cause an changes to it.
		if ('pre_controller' == $phase) {
			$GLOBALS['CFG']->set_item('sess_time_to_update', 10000000);
			throw new Exception('Allowing execution to proceed after eliminating CI session update', self::EX_GRACEFULLY_ABORT_HOOK);
		}


		# Otherwise, we're in the "post_controller_constructor" phase, and we now 
		# have a session object that we can use to determine whether or not an 
		# active session exists and how much time is left in it.
		$return_ar = array(
			'is_valid' => true,
		);

		# verify that there is time left in the session, and if there is, state 
		# how many seconds are left
		$session_ar = $this->ci->session->all_userdata();
		if (empty($session_ar['user_id'])) {
			throw new Exception('Not logged in');
		}
		$last_activity = $session_ar['last_activity'];
		$return_ar['last_activity_timestamp'] = $last_activity;
		$sess_expiration = $this->ci->config->item('sess_expiration');
		if (false === $sess_expiration) {
			throw new Exception('Unable to obtain session expiration time');
		}
		$time_until_expiration = max(0, $last_activity + $sess_expiration - time());
		if (0 == $time_until_expiration) {
			throw new Exception('No time left in session');
		}
		$return_ar['time_until_expiration'] = $time_until_expiration;

		# During the user's login event, we generated a login session ID, stored it 
		# in the user's DB record, and sent the ID to their browser to be stored in 
		# a cookie.  Does the cookie's login session ID still match the login 
		# session ID stored in their DB record?  If not, the database record was 
		# probably updated with a new login ID due to a separate login being done 
		# on top of the existing session, invalidating the previous login session.
		$login_session_cookie_name = $this->ci->config->item('cookie_prefix') . 'login_session_id';
		if (empty($_COOKIE[$login_session_cookie_name])) {
			throw new Exception("No login session ID provided");
		}
		$cookie_login_session_id = $_COOKIE[$login_session_cookie_name];

		$this->ci->db->select('login_status');
		$this->ci->db->where('user_id', $session_ar['user_id']);
		$query = $this->ci->db->get('users');
		$result_ar = $query->result();
		if (empty($result_ar)) {
			throw new Exception("No user with ID '$user_id' found");
		}
		list($db_login_session_id) = explode(':', $result_ar[0]->login_status);
		if ($db_login_session_id != $cookie_login_session_id) {
			throw new Exception("Login session ID stored in DB '$db_login_session_id' does not match login session ID from cookie '$cookie_login_session_id'");
		}

		return $return_ar;
	}

	# Forces CI to update the session, resetting the session expiration time.
	private function update_active($phase) {

		# If we're in the "pre_controller" phase ibefore session is instantiated, 
		# shorten the time to update so that we're guaranteed to update the session 
		# expiration time when the controller is instantiated
		if ('pre_controller' == $phase) {
			$GLOBALS['CFG']->set_item('sess_time_to_update', 1);
			throw new Exception('Allowing execution to proceed after forcing CI session update', self::EX_GRACEFULLY_ABORT_HOOK);
		}

		###########################################################################
		# Between the "pre_controller" and "post_controller_constructor" phase, the 
		# session object was instantiated, which should have taken care of updating 
		# the CI session's expiration time.
		###########################################################################

		# Otherwise, we're in the "post_controller_constructor" phase, and a 
		# database class instance exists that lets us update the user's database 
		# record with the current timestamp so that we have a non-volatile record 
		# of when they were last active.
		$session_ar = $this->ci->session->all_userdata();
		if (empty($session_ar['user_id'])) {
			throw new Exception('Not logged in');
		}
		list($login_session_id) = explode(':', $session_ar['login_status']);
		$this->ci->config->set_item('sess_time_to_update', 1);
		$this->ci->db->where('user_id', $session_ar['user_id']);
		$this->ci->db->like('login_status', "$login_session_id:", 'after');
		$this->ci->db->update('users', array(
			'login_status' => "$login_session_id:" . time(),
		));

		return true;
	}

	// Allows clientside javascript to learn the names of the cookies that should 
	// be cleared out when the session has expired.
	private function get_ci_login_cookie_names($phase) {
		if ('post_controller_constructor' != $phase) {
			throw new Exception('We only want to act during the "post_controller_construction" phase', self::EX_GRACEFULLY_ABORT_HOOK);
		}

		return array(
			$this->ci->config->item('cookie_prefix') . $this->ci->config->item('sess_cookie_name'),
			$this->ci->config->item('cookie_prefix') . 'login_session_id',
		);
	}

	// Destroys login cookies.
	private function logout($phase) {
		if ('post_controller_constructor' != $phase) {
			throw new Exception('We only want to act during the "post_controller_construction" phase', self::EX_GRACEFULLY_ABORT_HOOK);
		}

		foreach ($this->get_ci_login_cookie_names($phase) as $cookie_name) {
			setcookie($cookie_name, '', time() - 3600, $this->ci->config->item('cookie_path'), $this->ci->config->item('cookie_domain'), $this->ci->config->item('cookie_secure'));
		}
	}
}

/* End of file SessionStatus.php */
/* Location: ./application/hooks/SessionStatus.php */
