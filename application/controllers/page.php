<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page extends CI_Controller {

	private $data = array();

	public function __construct() {
		parent::__construct();

		$this->load->model('Page_model');

	}

	public function index($page='index') {

		$this->data['has_registered'] = $this->session->userdata('has_registered');

		if($this->input->post('fname')) {

			$user_data = array(
				'first_name' => $this->input->post('fname', TRUE),
				'last_name' => $this->input->post('lname', TRUE),
				'company' => $this->input->post('company', TRUE),
				'email' => $this->input->post('email', TRUE),
				'job_title' => $this->input->post('jtitle', TRUE),
				'feedback' => $this->input->post('feedback', TRUE),
			);

			$result = false;
			$msg = '';

			$email_to = "learning@biworldwide.com";
			$email_subject = "Chameleon Homepage: Form Submission";

			$first_name = $this->input->post('fname');
			$last_name = $this->input->post('lname');
			$email_from = $this->input->post('email');
			$company = $this->input->post('company');
			$jtitle = $this->input->post('jtitle');
			$feedback = $this->input->post('feedback');

			$email_message = "Form details below.\r\n\r\n";

			$email_message .= "First Name: ". $first_name ."\r\n";
			$email_message .= "Last Name: ". $last_name ."\r\n";
			$email_message .= "Email: ". $email_from ."\r\n";
			$email_message .= "Company: ". $company ."\r\n";
			$email_message .= "Phone: ". $jtitle ."\r\n";
			$email_message .= "Feedback: ". $feedback ."\r\n";

			$this->load->library('email');
			$this->email->from($email_from);
			$this->email->to($email_to);

			$this->email->subject($email_subject);
			$this->email->message($email_message);

			if ( ! $this->email->send()) {
				//echo $this->email->print_debugger();
				//exit;
				$msg = 'Email has not been sent successfully!';
			} else {
				$result = true;
				$msg = 'Email has been sent successfully!';
			}

			if($result) {
				$resultarray = array('result' => $result, 'msg' => $msg);
				$resultarray = json_encode($resultarray);

				echo $resultarray;

				$result = $this->Page_model->insert($user_data);

				if($result) {
					$this->session->set_userdata('has_registered', 1);
					redirect($this->uri->uri_string);
				}
			}

		}

		// load view
		$data = $this->data;
		$this->load->view('site/header', $data);
		$this->load->view('site/' . $page, $data);
		if($page == 'results' || $page == 'demos') {
			$this->load->view('site/modal', $data);
		}
		$this->load->view('site/footer', $data);

	}

	public function login() {
		$logged_in_user = User_model::get_logged_in_user();
		if (false !== $logged_in_user) {
			$allowed_clients = $logged_in_user->get_allowed_clients();
			if (count($allowed_clients) == 1) {
				redirect('/client/' . $allowed_clients[0]);
			}
			else {
				redirect('/client/');
			}
		}

		// load view
		$data = $this->data;
		$this->load->view('site/login_header', $data);
		$this->load->view('site/login', $data);
		$this->load->view('site/login_footer', $data);
	}

}

/* End of file page.php */
/* Location: ./application/controllers/page.php */