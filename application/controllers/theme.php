<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Theme extends CI_Controller {
	const DEFAULT_CACHE_MINUTES = 1;
	public function __construct() {
		parent::__construct();
	}

	/*
	 * Main entrypoint of the themes API.
	 *
	 * At the time of development, we're on a version of CodeIgniter that does
	 * not natively support routing requests to different methods depending on
	 * the HTTP request method.  That's fine, we'll just do it ourselves and
	 * simplify the routing entries at the same time!
	 *
	 * All API request paths should be routed to this handler method.  The first
	 * argument is expected to be the name of the API resource being accessed.
	 * The rest of the arguments are passed to the method invoked.  The name of
	 * this method incorporates both the API resource being accessed and the HTTP
	 * verb used to request it, like so: [REQUESTED API RESOURCE]_[HTTP REQUEST
	 * METHOD].
	 */
	public function handle_request() {

		# send headers that instruct browser security models to allow this resource
		# to be called from other domains
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

		# we need to have at least one argument so we know which method to call
		$num_args = func_num_args();
		if ($num_args < 1) {
			throw new Exception('Too few arguments');
		}
		$arg_ar = func_get_args();
		$method_name = array_shift($arg_ar);

		# the name of the method we'll call incorporates both the requested action
		# and the HTTP verb used to make the request
		$http_action = strtolower($_SERVER['REQUEST_METHOD']);
		$method_name_with_action = "{$method_name}_{$http_action}";
		if (!method_exists($this, $method_name_with_action)) {
			throw new Exception("Method '$method_name_with_action' is not defined");
		}

		# install a custom error handler to turn all errors into exceptions
		set_error_handler(function($errno, $errstr, $errfile, $errline) {
			throw new Exception("$errfile, line $errline ($errno): $errstr");
		});

		# set header for JSON response
		header('Content-Type: text/json');

		# Invoke the method.
		#
		# Any uncaught exception thrown from within the method will be caught here,
		# and will cause the service to indicate an error condition, setting the
		# 'error' property of the returned object to the exception's error message.
		#
		# Additionally, if the exception included a code that is within the range
		# of valid HTTP response codes, we will also set the HTTP response code,
		# which allows API methods to indicate things like Not Found (404) and
		# Unauthorized (401).
		try {
			$result = call_user_func_array(array($this, $method_name_with_action), $arg_ar);
		}
		catch (Exception $e) {
			$result = array(
				'error' => $e->getMessage(),
			);
			$code = $e->getCode();
			if ($code < 100 || $code >= 600) {
				$code = 500;
			}
			http_response_code($code);
			echo json_encode($result);
			exit;
		}

		# For now, we always render the result in JSON format, but it is
		# conceivable that we could respond to an HTTP Content-Type request header
		# to allow the output to be rendered in XML or other formats if the need
		# arises.
		#
		# NOTE: Since JSON *must* be encoded in UTF-8, we're making an assumption
		# here that all of these methods return their text using UTF-8 encoding.
		# If character encoding issues come up, this may be a good place to start
		# looking.
		#
		# NOTE: Since the AngularJS $resource service needs to receive either an
		# array or an object, we'll package the result into an object if it is
		# neither a string nor an object.
		if (!is_array($result) && !is_object($result)) {
			$result = array(
				'result' => $result,
			);
		}
		echo json_encode($result);
		exit;
	}

	/*
	 * Enumerate a list of all themes that are available for the given client ID.
	 *
	 * Since this method supports the editor and up-to-the-second accuracy is
	 * more important than being tolerant of high request volume, caching is
	 * disabled when enumerating theme files on S3.
	 */
	private function list_get($client_id) {
		$ret_ar = array();
		$client_theme_dirpath = $this->get_theme_dirpath($client_id);
		$theme_filename_ar = $this->aws_ls("$client_theme_dirpath/", false);
		foreach ($theme_filename_ar as $filename => $file_info) {
			if (false === stripos($filename, '.json')) {
				continue;
			}
			$theme_name = str_ireplace('.json', '', $filename);
			if (empty($theme_name)) {
				continue;
			}
			$ret_ar[] = $theme_name;
		}
		return $ret_ar;
	}

	/*
	 * Get a theme JSON object, given a client ID and theme ID.
	 *
	 * If merge=1 is present in the query string (or, if invoking
	 * programmatically, by passing a true value in the third argument), we will
	 * grab the "variable" property of the object defined in core/theme.json,
	 * apply override values from the "variable" property in the theme JSON file,
	 * and return the result of this as the "variable" property of the returned
	 * JSON object.  Note that this merge process does not allow any new values
	 * to be defined in the theme JSON file; only values defined in
	 * core/theme.json's "variable" property may be overridden.
	 */
	private function json_get($client_id, $theme_id, $do_merge = false) {
		$do_merge = $do_merge || !empty($_GET['merge']);
		$aws_filepath = $this->get_theme_filepath($client_id, $theme_id);
		return $this->get_theme_content($aws_filepath, $do_merge);
	}

	/*
	 * Put a theme JSON object -- both PUT and POST work.
	 *
	 * We verify that the received string is valid JSON before writing it to AWS.
	 */
	private function json_put($client_id, $theme_id) {
		return $this->json_post($client_id, $theme_id);
	}
	private function json_post($client_id, $theme_id) {
		$json_filepath = $this->get_theme_filepath($client_id, $theme_id);
		$content = file_get_contents('php://input');
		$content_ar = json_decode($content);
		if (null === $content_ar) {
			throw new Exception("Content received is not valid JSON");
		}
		$this->aws->add_content($content, $json_filepath, 'application/json');
		return true;
	}

	/*
	 * Get the default theme for the specified client.
	 */
	private function default_theme_get($client_id) {
		$this->db->where('client_id', $client_id);
		$query = $this->db->get('clients');
		if ($query->num_rows < 1) {
			throw new Exception("Could not locate client '$client_id'");
		}
		$row = $query->first_row();
		if (null === $row->default_theme) {
			throw new Exception('No default theme has been set');
		}
		return $this->normalize_theme_id($row->default_theme);
	}

	/*
	 * Set the default theme for the specified client -- both PUT and POST work.
	 *
	 * Before updating the client record in the database, we verify that the
	 * theme ID provided really is a valid theme for this client.
	 */
	private function default_theme_put($client_id) {
		return $this->default_theme_post($client_id);
	}
	private function default_theme_post($client_id) {
		$theme_id = file_get_contents('php://input');
		$json_filepath = $this->get_theme_filepath($client_id, $theme_id);
		if (false === $this->aws->is_file($json_filepath)) {
			throw new Exception('Theme does not exist', 404);
		}
		$this->db->set('default_theme', $this->normalize_theme_id($theme_id));
		$this->db->where('client_id', $client_id);
		$this->db->update('clients');
		return true;
	}

	/*
	 * Get an S3 URL reference to a CSS file for a specific combination of course
	 * ID, theme ID and chameleon framework version.  This URL may then be used
	 * from within a course to provide styles for that course.
	 *
	 * The CSS file is created by pulling together theme information and style
	 * information from the chameleon version that the course was created to use.
	 * Since this involves a lot of fetching and putting information to/from S3,
	 * rebuilding the CSS is not a lightweight operation.  That is why we employ
	 * modification time checks to see whether or not the CSS file needs to be
	 * rebuilt.  However, even with this in place, the mere act of checking
	 * modification times on S3 files can be expensive if traffic is bursty, so
	 * we also do some caching on S3 file list operations in order to limit the
	 * potential impact of sudden bursts of traffic (at the modest expense of a
	 * small lag in CSS source file changes being reflected).
	 */
	private function css_get($course_id, $requested_theme_id, $chameleon_version) {
		# we got a course ID, but we actually need the ID of the client that the
		# course belongs to so that we can access that client's files on S3
		$client_id = $this->course->get_course_field($course_id, 'client_id');

		# where should we get/put the compiled CSS?
		$theme_dirpath         = $this->get_theme_dirpath($client_id);
		$compiled_css_filename = "$requested_theme_id-$chameleon_version.css";
		$compiled_css_filepath = "$theme_dirpath/compiled/$compiled_css_filename";

		# look for the requested theme -- if we cannot find it, try using the default theme instead
		$requested_theme_filepath = $this->get_theme_filepath($client_id, $requested_theme_id);
		try {
			$requested_theme_modification_ts = $this->get_file_modification_ts($requested_theme_filepath);
		}
		catch (Exception $e) {
			$default_theme_id = $this->default_theme_get($client_id);
			$requested_theme_filepath = $this->get_theme_filepath($client_id, $default_theme_id);
			$requested_theme_modification_ts = $this->get_file_modification_ts($requested_theme_filepath);
		}

		# where is the chameleon.less for the version being requested? does it exist?
		$chameleon_less_filepath = "core/versions/$chameleon_version/css/chameleon/chameleon.less";
		$chameleon_less_ts = $this->get_file_modification_ts($chameleon_less_filepath);

		# exceptions thrown in this block mean we build or rebuild the compiled
		# CSS; it is therefore critical that we need to have ALREADY checked for
		# all conditions that would prevent us from successfully doing the build
		try {
			# get file information about any existing compiled CSS; the ls will
			# throw an exception if no CSS is already present
			$css_modification_ts = $this->get_file_modification_ts($compiled_css_filepath);

			# recompile if the master theme was modified after the existing CSS
			$master_theme_modification_ts = $this->get_file_modification_ts('core/theme.json');
			if ($master_theme_modification_ts > $css_modification_ts) {
				throw new Exception('Master theme is newer than existing CSS');
			}

			# recompile if requested theme was modified after the existing CSS
			if ($requested_theme_modification_ts > $css_modification_ts) {
				throw new Exception('Requested theme was modified after existing CSS');
			}

			# if the existing CSS is older than the chameleon.less file associated
			# with the requested version, we'll need to rebuild the CSS
			if ($css_modification_ts < $chameleon_less_ts) {
				throw new Exception("Existing CSS is older than chameleon.less for requested version '$chameleon_version'");
			}
		}
		catch (Exception $e) {
			$this->build_css($requested_theme_filepath, $chameleon_less_filepath, $compiled_css_filepath);
		}

		# returning the S3 path where the CSS for the requested theme and version
		# number may be accessed
		return $this->config_item('aws_path') . $compiled_css_filepath;
	}

	/*
	 * Accepts a course ID and course phase (pre, stage, etc.).
	 *
	 * Returns information about the course:
	 *   -the client ID for the client that owns the course
	 *   -the highest MAJOR.MINOR.PATCH version number for the MAJOR.MINOR
	 *    version that the course is assigned to
	 *   -the theme ID selected for this course
	 */
	private function courseinfo_get($course_id, $phase) {
		# satisfy the request from cache if possible to limit the load on the DB in
		# high traffic scenarios
		$cache_id = 'chameleon_api_courseinfo_get';
		$cache_key = implode('|', array($this->config_item('aws_path'), $course_id, $phase));
		$result = $this->cache_get($cache_id, $cache_key);
		if (false !== $result) {
			return $result;
		}

		# could not serve from cache if we got this far, so we need to calculate
		$result = array();

		# get information from the DB for the requested course
		$course = $this->course->get_course_field($course_id);

		# pack the client ID into the result
		$result['client_id'] = $course->client_id;

		# retrieve the chameleon version from the database. we will next check if
		# there is a version in the courses json data (which is preferred).  This
		# version number is only to be used as a fallback if none exist in the main.js
		$chameleon_version = $course->course_version;

		# open the main.js for the specified phase and read out the theme ID, but
		# use the default theme ID for the client if we don't have one
		try {
			$main_js_path = "clients/{$course->client_id}/courses/{$course->course_folder}/$phase/course/data/main.js";
			$json_content = $this->aws->get_content($main_js_path);
			$course_data_object = json_decode($json_content);
			if (null === $course_data_object) {
				throw new Exception('Unable to decode JSON');
			}
			if (!property_exists($course_data_object, 'config')) {
				throw new Exception("Decoded JS object does not have 'config' property");
			}
			if (property_exists($course_data_object->config, 'theme_id')) {
				$result['theme_id'] = $this->normalize_theme_id($course_data_object->config->theme_id);
			}
			else {
				$result['theme_id'] = $this->default_theme_get($course->client_id);
			}
			if (property_exists($course_data_object->config, 'version')) {
				$chameleon_version = $course_data_object->config->version;
			}
		}
		catch (Exception $e) {
			throw new Exception("Unable to read theme ID: {$e->getMessage()}");
		}


		# Chameleon versions are composed like so: MAJOR.MINOR.PATCH -- however,
		# the version that is stored in the course record may not include the PATCH
		# portion.  In such a case, we want to send back the highest PATCH version
		# available for the MAJOR.MINOR version that the course is assigned to.
		$chameleon_version_ar = explode('.', $chameleon_version);
		if (2 == count($chameleon_version_ar)) {
			$max_patch_version = 0;
			foreach ($this->aws_ls('core/versions/') as $filename => $file_info) {
				# results should be arrays, directories only, with names beginning with
				# the requested chameleon version
				if (
					!is_array($file_info) ||
					$file_info['type'] != 'dir' ||
					0 !== strpos($filename, "$chameleon_version.")
				) {
					continue;
				}

				# if the patch version of this version is higher than any we've already
				# seen, make a note of it
				$patch_version = substr($filename, strlen("$chameleon_version."));
				if ($patch_version > $max_patch_version) {
					$max_patch_version = $patch_version;
				}
			}
			if ($max_patch_version > 0) {
				$chameleon_version .= ".$max_patch_version";
			}
		}

		# pack the chamaleon version into the result
		$result['chameleon_version'] = $chameleon_version;

		# cache the result and return it
		$this->cache_put($cache_id, $cache_key, $result);
		return $result;
	}

	###### UTILITY METHODS ######

	/*
	 * Get a theme JSON object, given an S3 filepath to the theme.
	 *
	 * Please see comments on json_get for notes on merging functionality.
	 */
	private function get_theme_content($aws_filepath, $do_merge = false) {

		# fetch the data for the requested theme
		$ret_ar = array();
		try {
			$ret_ar = json_decode($this->aws->get_content($aws_filepath), true);
		}
		catch (Aws\S3\Exception\S3Exception $e) {
			if (404 == $e->getResponse()->getStatusCode()) {
				throw new Exception("Could not find file '$aws_filepath'", 404);
			}
			throw $e;
		}

		# Perform a merge with the master theme JSON file, if requested.  This
		# merge occurs only on the 'variable' property of the objects.
		#
		# If the requested theme does not include a 'variable' property, default to
		# using an empty array for it, which essentially will give us defaults from
		# core/theme.json following the merge.
		#
		# If the requested theme includes a 'variable' property, but it is not an
		# array, then the structure of the requested theme is incorrect and we'll
		# complain about it.
		if ($do_merge) {
			if (!isset($ret_ar['variable'])) {
				$ret_ar['variable'] = array();
			}
			elseif (!is_array($ret_ar['variable'])) {
				$type = gettype($ret_ar['variable']);
				throw new Exception("Requested theme '$theme_id' has a 'variable' property, but it is a '$type' instead of an array");
			}
			$master_theme_ar = $this->file_get_json('core/theme.json');
			if (!isset($master_theme_ar['variable'])) {
				throw new Exception("Could not perform merge because 'variable' property does not exist in 'core/theme.json'");
			}
			$ret_ar['variable'] = $this->apply_theme_overrides($master_theme_ar['variable'], $ret_ar['variable']);
		}

		# This is the price of choosing to deal with JSON by decoding into
		# associative arrays; an empty array will encode to an array instead of
		# an object, so we check for that here and handle it specially.
		if (empty($ret_ar)) {
			return new stdClass;
		}

		return $ret_ar;
	}

	/*
	 * Takes two arrays.  Recursively iterates through the first array, and if
	 * the second array is similarly structured, the leafs of the second array
	 * can override the value of the leafs in the first array.  However, the
	 * second array cannot create any structures or leafs that are not present in
	 * the first array.
	 */
	private function apply_theme_overrides($master_ar, $override_ar) {
		foreach ($master_ar as $k => $v) {
			if (!isset($override_ar[$k])) {
				continue;
			}
			if (is_scalar($v) && is_scalar($override_ar[$k])) {
				$master_ar[$k] = $override_ar[$k];
				continue;
			}
			if (is_array($v) && is_array($override_ar[$k])) {
				$master_ar[$k] = $this->apply_theme_overrides($v, $override_ar[$k]);
			}
		}
		return $master_ar;
	}

	/*
	 * Given AWS paths to a theme file and a chameleon.less file, builds a CSS
	 * file using a LESS library and uploads it to the target AWS path.  Returns
	 * nothing, throws an exception for any fatal errors.
	 */
	private function build_css($aws_theme_filepath, $aws_chameleon_less_filepath, $target_filepath) {
		require_once APPPATH . 'third_party/Less.php';

		# starting with the master theme.json file, apply overrides from the
		# requested theme file, then use the merged result to create a temporary
		# LESS file locally
		$merged_theme = $this->get_theme_content($aws_theme_filepath, true);
		$less_line_ar = array();
		foreach ($merged_theme['variable'] as $k => $v) {
			$less_line_ar[] = "@$k: $v;";
		}
		$local_theme_less_filepath = tempnam(sys_get_temp_dir(), 'variable_theming.theme_less.');
		if (!file_put_contents($local_theme_less_filepath, implode("\n", $less_line_ar))) {
			throw new Exception("Unable to write to '$local_theme_less_filepath'");
		}

		# create a temporary directory locally that will be used to mirror LESS
		# files from the chameleon version on AWS that we'll use to build the CSS
		$local_chameleon_less_dirpath = tempnam(sys_get_temp_dir(), 'variable_theming.chameleon_less.');
		if (!unlink($local_chameleon_less_dirpath) || !mkdir($local_chameleon_less_dirpath)) {
			throw new Exception('Unable to transform temp file into temp dir for local chameleon.less files');
		}
		$local_chameleon_less_filepath = "$local_chameleon_less_dirpath/" . basename($aws_chameleon_less_filepath);

		# we will need ALL of the files in chameleon.less' directory in order to
		# successfully parse chameleon.less, so mirror files into the temp
		# directory that we just created
		$aws_chameleon_less_dirpath = dirname($aws_chameleon_less_filepath);
		$ls_result = $this->aws_ls("$aws_chameleon_less_dirpath/", false);
		foreach ($ls_result as $filename => $file_info) {
			$local_chameleon_less_filepath = "$local_chameleon_less_dirpath/$filename";
			$aws_chameleon_less_filepath   = "$aws_chameleon_less_dirpath/$filename";
			if (!file_put_contents($local_chameleon_less_filepath, $this->aws->get_content($aws_chameleon_less_filepath))) {
				throw new Exception("Unable to copy .less file '$filename' for use locally");
			}
		}

		# now we can compile the LESS files into CSS and save the result to AWS
		$parser = new Less_Parser();
		$parser->parseFile($local_theme_less_filepath);
		$parser->parseFile($local_chameleon_less_filepath);
		$this->aws->add_content($parser->getCss(), $target_filepath, 'text/css');

		# clean up temp files
		array_map('unlink', glob("$local_chameleon_less_dirpath/*"));
		rmdir($local_chameleon_less_dirpath);
		unlink($local_theme_less_filepath);
	}

	/*
	 * Fetch a file from AWS, decode it as JSON, and return the result.
	 *
	 * Throws an exception on invalid JSON.
	 */
	private function file_get_json($path) {
		try {
			$json_content = $this->aws->get_content($path);
			$decoded_object = json_decode($json_content, true);
			if (null === $decoded_object) {
				throw new Exception('Content could not be decoded as JSON');
			}
			return $decoded_object;
		}
		catch (Exception $e) {
			throw new Exception("Unable to fetch '$path' as JSON: {$e->getMessage()}");
		}
	}

	/*
	 * Get the modification timestamp of a file on AWS.
	 *
	 * Throws an exception if the file could not be found or if the modification
	 * time could not be interpreted as a time value.
	 */
	private function get_file_modification_ts($path) {
		try {
			$filename = basename($path);
			$ls_result = $this->aws_ls($path, false);
			if (!isset($ls_result[$filename])) {
				throw new Exception("Unable to find file '$path'");
			}
			if (!isset($ls_result[$filename]['last_modified'])) {
				ob_start();
				var_dump($ls_result);
				throw new Exception('Ls data was not of the expected format: ' . ob_get_clean());
			}
			return $ls_result[$filename]['last_modified'];
		}
		catch (Exception $e) {
			throw new Exception("Could not get file modification time: {$e->getMessage()}");
		}
	}

	/*
	 * The cache_* suite of methods provides a general framework for using the
	 * /tmp directory to store and retrieve data from cache, as well as for
	 * cleaning up old cache data.
	 */
	private function cache_get($cache_id, $cache_key) {
		# this is where we will cache the data
		$cache_dirpath = $this->cache_get_dirpath($cache_id);
		if (!is_dir($cache_dirpath) && !mkdir($cache_dirpath)) {
			throw new Exception("Unable to create cache directory '$cache_dirpath'");
		}

		# really simple method of setting the cache lifetime -- much room for
		# making this configable
		$cache_expire_minutes = self::DEFAULT_CACHE_MINUTES;

		# nothing is going to clean up old cache files for us, so we should garbage
		# collect them; we don't want to do it all of the time, though, so let's
		# garbage collect on approximately 5% of requests
		if (rand(0, 99) < 5) {
			$cmd = sprintf('find %s -maxdepth 1 -type f -mmin +%d -exec rm {} \;',
				$cache_dirpath,
				$cache_expire_minutes
			);
			$out = array(); $ret = '';
			exec($cmd, $out, $ret);
			if ($ret > 0) {
				# do anything if garbage collection failed?
			}
		}

		# return the result from cache if it is present and fresh enough
		$cache_filepath = $this->cache_get_filepath($cache_id, md5($cache_key));
		if (!is_file($cache_filepath) || time() > filemtime($cache_filepath) + $cache_expire_minutes * 60) {
			return false;
		}
		return unserialize(file_get_contents($cache_filepath));
	}

	private function cache_put($cache_id, $cache_key, $cache_value) {
		file_put_contents($this->cache_get_filepath($cache_id, md5($cache_key)), serialize($cache_value));
	}

	private function cache_get_dirpath($cache_id) {
		return sys_get_temp_dir() . "/$cache_id";
	}

	private function cache_get_filepath($cache_id, $cache_key) {
		return $this->cache_get_dirpath($cache_id) . '/' . md5($cache_key);
	}

	/*
	 * This method wraps the 'ls' method in the AWS interface library with
	 * caching, but only if the $do_cache argument is enabled.  Caching should be
	 * enabled whenever this operation may be run in response to an
	 * external-facing request that could potentially see high volume.
	 */
	private function aws_ls($path, $do_cache = true) {
		# Fulfill request from cache if possible.
		#
		# We're choosing the cache key so that it incorporates both environment and
		# path, which prevents cross-environment cache collisions in case one host
		# serves requests for multiple environments.
		if ($do_cache) {
			$cache_id = 'chameleon_api_aws_ls';
			$cache_key = $this->config_item('aws_path') . $path;
			$result = $this->cache_get($cache_id, $cache_key);
			if (false !== $result) {
				return $result;
			}
		}

		# request object data from S3
		$ret_ar = $this->aws->ls($path);

		# write the result to cache and return
		if ($do_cache) {
			$this->cache_put($cache_id, $cache_key, $ret_ar);
		}
		return $ret_ar;
	}

	/*
	 * Get the theme directory for the provided client.
	 */
	private function get_theme_dirpath($client_id) {
		return $this->config_item('client_path') . "$client_id/themes";
	}

	/*
	 * Get the path where the specified theme should be for the specified client.
	 */
	private function get_theme_filepath($client_id, $theme_id) {
		$theme_id = $this->normalize_theme_id($theme_id);
		return $this->get_theme_dirpath($client_id) . "/$theme_id.json";
	}

	/*
	 * Transforms the given theme ID to eliminate distinctions that we do not
	 * wish to allow in theme IDs.  The rules for theme IDs are:
	 *   -case does not matter -- transform all uppercase to lowercase
	 */
	private function normalize_theme_id($theme_id) {
		return strtolower($theme_id);
	}

	/*
	 * A wrapper for grabbing items from the config that throws an exception if
	 * the requested config key is not present.
	 */
	private function config_item($k) {
		$v = $this->config->item($k, 'default');
		if (false === $v) {
			throw new Exception("Key '$k' does not exist in default config");
		}
		return $v;
	}
}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */
