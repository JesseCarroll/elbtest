<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course extends MY_Controller {

	private $model;

	public function __construct() {
		parent::__construct();

		# load dependencies
		$this->load->model('Coursedownload_model');
		$this->load->helper('download');
		$this->load->library('zip');

		# if we're handling a request for a specific course, get a model for that course
		if ('course' == $this->uri->segment(1) && $this->uri->segment(2) && is_numeric($this->uri->segment(2))) {
			$this->model = Course_model::get_by_id($this->uri->segment(2,1));
		}
	}

	function dashboard() {
		# kick the user out if no permission to edit this course
		if ('translate' == $this->model->state) {
			if (!$this->current_user->can('translate', $this->model->client_id, $this->model->course_id)) {
				redirect("/client/{$this->model->client_id}");
			}
		}
		elseif (!$this->current_user->can('edit_course', $this->model->client_id, $this->model->course_id)) {
			redirect("/client/{$this->model->client_id}");
		}


		# prepare data to feed to the view
		$data = $this->commondata;
		if (!empty($this->model)) {
			$data['course'] = $this->model;
		}
		$this->load->view('client_header', $data);
		$this->load->view('course/course_dashboard', $data);
		$this->load->view('client_footer', $data);
	}

	function translations() {
		$data = $this->commondata;
		$this->load->view('client_header', $data);
		$this->load->view('translation_pages', $data);
		$this->load->view('client_footer', $data);
	}

	function delete() {
		//$this->model->delete($this->model->course_id, $this->current_user->role == 'SA');

		/*** TODO : INCORPORATE ALERT MESSAGES TO ANGULAR NOTICES IN CONTROLLER ***/
	}

	function preview() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		$location = ($this->uri->segment(4)) ? $this->uri->segment(4) : 'pre';
		$location_url = 'clients/' . $this->model->client_id . '/courses/' . $this->model->course_folder . '/' . $location;

		// compile css
		$this->model->compile_course();

		$language = $this->input->get('language');
		$page_uuid = ($this->uri->segment(5)) ? $this->uri->segment(5) : '';

		$hash_arguments = array();
		if (!empty($page_uuid)) {
			$this->load->model('Topicpage_model');
			list($topic_order, $page_order) = $this->Topicpage_model->get_page_ord($this->model, $page_uuid);
			$hash_arguments[] = 'topic=' . ($topic_order + 1);
			$hash_arguments[] = 'page=' . ($page_order + 1);
		}

		if (!empty($language)) {
			$hash_arguments[] = 'language=' . $language;
		}

		if (!empty($hash_arguments)) {
			if($language) {
				print_r($location_url . '/index.html#' . implode(',', $hash_arguments));
			}
			else {
				print_r($location_url . '/index.html#' . implode(',', $hash_arguments));
			}
		} else {
			print_r($location_url . '/index.html');
		}
	}

	function upload() {
		try {
			$result_ar = $this->media->ingest($this->model->course_folder_path('pre/course/'));
		}
		catch (Exception $e) {
			http_response_code(500);
			$result_ar['error_message'] = $e->getMessage();
		}
		header('Content-Type: text/json');
		echo json_encode($result_ar);
	}

	function direct_edit() {

		$file = $this->input->post('filename');
		$file_data = $this->input->post('file_data');
		$type = $this->input->post('type');
		$update = $this->input->post('update');
		$path = $this->model->course_folder_path('pre/course/') . $file;

		if ($update) {
			$this->data['filename'] = $file;
			$this->data['file_data'] = $file_data;
			$this->aws->add_content($file_data, $path, $type);
		}
		else {
			echo $this->aws->get_content($path);
		}
	}

	function reset_version() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = (array) $request->data;
		$version = $data[0];
		$this->model->reset_less_file($version);
	}

	function sync_assets() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = (array) $request->data;
		$version = $data[0];
		$chameleon_data = $this->model->load_chameleon_json($version);
		if (array_key_exists('assets', $chameleon_data)) {
			$this->model->syncAssets($chameleon_data['assets']);
		}
		echo json_encode($chameleon_data);
	}

	function update() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = (array) $request->data;
		if ($request->stage) {
			$this->model->save_stage_json($data);
		} else {
			$this->model->save_course_json($data);
		}

		# delete assets on S3 that are not referenced in the course JSON
		$this->model->media_garbage_collection($data);
	}

	function update_translation($shortcode) {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$data = (array) $request->data;
		$prod = (isset($request->prod)) ? '' : '-dev';
		$this->model->save_translation_json($data, $shortcode, $prod);

		# delete assets on S3 that are not referenced in the course JSON
		$this->model->media_garbage_collection($data, $shortcode);
	}

	function update_db() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		if (isset($request->data->course_enabled_languages)) {
			$request->data->course_enabled_languages = json_encode($request->data->course_enabled_languages);
		}
		//$request->data->date_updated = date('Y-m-d H:i:s');
		$request->data->updated_by = $this->current_user->user_id;
		$course_data = $this->model->update($request->data);
		print_r(json_encode($course_data));
	}

	function publish() {
		$data['date_published'] = date('Y-m-d H:i:s');
		$this->model->update($data);
		$this->model->publish_course();
	}

	function stage() {
		# keep running even if the other end hangs up
		ignore_user_abort(true);

		# shut down output buffering to make it possible for us to flush updates to
		# the output stream as execution happens
		while (ob_get_level() > 0) {
			ob_end_clean();
		}

		# set a header to tell FastCGI to not buffer the response
		header('X-Accel-Buffering: no');

		# get request data
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		# this function can be called to send incremental status updates back to
		# the client to update the user on long-running processes
		$stage_status_function = function($message) {
			echo "$message\n";
			flush();
		};

		# do course publishing and packaging if we're moving to certain stages
		# both possible stages will require publishing and packaging
		$this->model->publish_course($request->env, $stage_status_function);
		$this->model->package_course($request->env, $stage_status_function);

		# if the environment is 'prod' and it is marked as being available
		# offline, we will create media packages containing all of the media
		# required to run the course in the offline player
		if ('prod' == $request->env && $request->offline) {
			$this->model->package_media($stage_status_function);
		}
	}

	function get_translation($shortcode) {
		$translation_data = $this->model->load_translation_json($shortcode, '-dev');
		echo ")]}',\n" . json_encode($translation_data);
	}

	function get_data() {
		$client_model = Client_model::get_by_id($this->model->client_id);
		$course_data = [
			'active_directories' => array(
				'pre' => $this->aws->is_file($this->model->course_folder_path('pre/index.html')),
				'stage' => $this->aws->is_file($this->model->course_folder_path('stage/index.html')),
				'prod' => $this->aws->is_file($this->model->course_folder_path('prod/index.html'))
			),
			'allowed_clients' => $this->current_user->get_allowed_clients(),
			'chameleon' => $this->model->load_chameleon_json(),
			'client' => $client_model,
			'course' => $this->model,
			'curriculum' => $client_model->get_curriculum(true),
			'languages' => $this->Language_model->get_all_active(),
			'data' => $this->model->load_course_json(),
			'stage' => $this->model->load_course_json('stage'),
			'versions' => $this->Version_model->get_all_active(),
			'user_id' => $this->current_user->user_id
		];

		if ($this->uri->segment(4) && $this->uri->segment(3) == 'translations') {
			$shortcode = $this->uri->segment(4);
			$course_data['shortcode'] = $shortcode;
			$course_data['translation'] = $this->model->load_translation_json($shortcode, '-dev');
			$course_data['translation_prod'] = $this->model->load_translation_json($shortcode);
		}

		/*
		 * adding ")]}',\n" for known JSON vulnerability
		 * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
		 */
		echo ")]}',\n" . json_encode($course_data);
	}
}

/* End of file course.php */
/* Location: ./application/controllers/course.php */
