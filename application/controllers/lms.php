<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lms extends CI_Controller {

    // STATUS VALUES: completed, incomplete, not attempted, unknown

    private $data = array();

    public function __construct() {
        parent::__construct();

        $this->config->load('default', TRUE);

        // load model
        $this->load->model('Version_model');
        $this->load->model('Lms_user_model');
        $this->load->model('Lms_attempts_model');
        $this->load->model('Lms_quiz_model');
    }

    public function user() {
        $data = $this->input->post();
        $user_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'user_id' => $data['user_id'],
            'email' => $data['email']
        );
        $attempt_data = array(
            'course_id' => $data['course_id']
        );

        if(!empty($data['email'])){
            $user = $this->Lms_user_model->get_by_email($data['email'], $data['user_id']);

            if($user) {
                $this->data['user_id'] = $attempt_data['user_id'] = $user->id;
                $this->data['attempt'] = $this->Lms_attempts_model->get_latest_attempt($attempt_data);
                echo json_encode($this->data);
            }
            else{
                $this->data['user_id'] = $this->Lms_user_model->insert($user_data);
                $this->data['attempt'] = '';
                echo json_encode($this->data);
            }
        }
        exit;
    }

    public function newAttempt() {
        $data = $this->input->post();
        echo json_encode($this->Lms_attempts_model->insert($data));
        exit;
    }

    public function setData() {
        $data = $this->input->post();
        $attempt_id = $data['id'];

        //$attempt = $this->Lms_attempts_model->get_by_id($attempt_id);
        $this->Lms_attempts_model->update($data,$attempt_id);
        exit;
    }

    public function newQuiz() {
        $data = $this->input->post();
        $this->Lms_quiz_model->insert($data);
    }

    public function getAttemptsData() {
        $data = $this->input->post();
        $user_id = $data['user_id'];
        $client_id = $data['client_id'];
        $status = $data['status'];

        $result = $this->Lms_attempts_model->attempts_by_user($user_id, $client_id, $status);
        echo json_encode($result);
    }

    public function completionTotals() {
        $data = $this->input->post();
        $client_id = $data['client_id'];

        $result = $this->Lms_attempts_model->completion_by_client($client_id);
        echo json_encode($result);
    }

    public function load_version_files() {

        $version = $this->uri->segment(3);
        $file = $this->uri->segment(4);
        $dir = explode('.', $file);
        $dir = $dir[1];

        $version = $this->Version_model->get_by('version_number', $version);
        $get_file = $version->core_folder_path($dir . '/' . $file);

        if($dir == 'css') {
            header('Content-type: text/css');
        } else {
            header('Content-type: application/javascript');
        }

        $contents = $this->aws->get_content($get_file);

        echo $contents;
    }
}

/* End of file lms.php */
/* Location: ./application/controllers/lms.php */