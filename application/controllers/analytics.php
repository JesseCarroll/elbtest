<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Analytics extends MY_Controller {

	private $analytics;

	public function __construct() {
		parent::__construct();

		// Create and configure a new client object.
		$client = new Google_Client();
		$client->setApplicationName("Hello Analytics Reporting");
		$client->setAuthConfig($_SERVER['DOCUMENT_ROOT'] . '/chameleon-903db20fec67.json');
		$client->setScopes(['https://www.googleapis.com/auth/analytics.readonly']);
		$this->VIEW_ID = '98010277';
		$this->analytics = new Google_Service_AnalyticsReporting($client);
	}

	function index($id) {
		if(!isset($id) || empty($id)) return;

		$client = $id;
		$course = $_GET['course'];

		// in order to test the analytics page in development or integration, we
		// need to force the use of production data - in this case GSK (22)
		if($_SERVER['HTTP_HOST'] == 'localhost.chameleondev.com' || $_SERVER['HTTP_HOST'] == 'int.chameleon-elearning.com') {
			$client = '22';
			$course = '1271';
		}

		// return results for the specified date range - defaults to one week
		$startDate = date('Y-m-d', strtotime($_GET['startDate']));
		$endDate = date('Y-m-d', strtotime($_GET['endDate']));

		// if course has been sent in the request - filter the analytics data for the
		// specific course ID only - otherwise we will filter analytics data for the
		// whole client
		if($_GET['course']) {
			$dimFilter = array(
				'dimensionName' => 'ga:dimension1',
				'operator' => 'EXACT',
				'expression' => $course
			);
		} else {
			$dimFilter = array(
				'dimensionName' => 'ga:dimension2',
				'operator' => 'EXACT',
				'expression' => $client
			);
		}

		/*
		 * ANALYTICS SUMMARY DATA (Sessions, Users, Average Time In Course)
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:dimension2' => 'client_id'
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions',
				'ga:users' => 'users',
				'ga:avgTimeOnPage' => 'averageTimeOnPage',
			),
			'dimFilter' => $dimFilter
		);
		$summary = $this->getReport($this->analytics, $params);

		/*
		 * SESSIONS BY COUNTRY
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:country' => 'country'
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions'
			),
			'dimFilter' => $dimFilter,
			'orderField' => 'ga:sessions',
			'orderType'  => 'DESCENDING',
			//'pageSize'   => 500
		);
		$countries = $this->getReport($this->analytics, $params);

		/*
		 * SESSIONS BY BROWSER
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:browser' => 'browser',
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions'
			),
			'dimFilter' => $dimFilter,
			'orderField' => 'ga:sessions',
			'orderType'  => 'DESCENDING',
			//'pageSize'   => 500
		);
		$browsers = $this->getReport($this->analytics, $params);

		/*
		 * SESSIONS BY OS
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:operatingSystem' => 'os',
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions'
			),
			'dimFilter' => $dimFilter,
			'orderField' => 'ga:sessions',
			'orderType'  => 'DESCENDING'
		);
		$os = $this->getReport($this->analytics, $params);

		/*
		 * SESSIONS BY DEVICE
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:deviceCategory' => 'device',
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions'
			),
			'dimFilter' => $dimFilter,
			'orderField' => 'ga:sessions',
			'orderType'  => 'DESCENDING'
		);
		$device = $this->getReport($this->analytics, $params);

		/*
		 * SESSIONS BY DATE
		 */
		$params = array(
			'view_id' => $this->VIEW_ID,
			'startDate'  => $startDate,
			'endDate'    => $endDate,
			'dimensions' => array(
				'ga:date' => 'date'
			),
			'metrics'    => array(
				'ga:sessions' => 'sessions',
				'ga:users' => 'users'
			),
			'dimFilter' => $dimFilter
		);
		$date = $this->getReport($this->analytics, $params);

		// return analytics data
		$data = (object)[];
		$data->summary = $summary[0];
		$data->countries = array_slice($countries, 0, 5);
		$data->browsers = array_slice($browsers, 0, 7);
		$data->os = $os;
		$data->device = $device;
		$data->date = $date;
		print_r(json_encode($data));

	}

	function getReport($analytics, $param) {
		/* set view_id - required */
		if (isset($param['view_id'] ))
			$view_id = $param['view_id'];
		else {
			echo 'Error: Google Analytics View ID must be set';
			return;
		}

		/* set date -required */
		$dateRange = new Google_Service_AnalyticsReporting_DateRange();
		if (isset($param['startDate']) && isset($param['endDate'])) {
			$dateRange->setStartDate($param['startDate']);
			$dateRange->setEndDate($param['endDate']);
		}
		else {
			echo 'Error: Google Analytics date range must be set';
			return;
		}
 
		/* Set dimensions - required */
		if (!isset($param['dimensions']) || empty ($param['dimensions'])) {
			echo 'Error: No dimensions have been set';
			return;
		}
		else {
			$dimensions = array();
			$dimension_names = array();
			foreach ($param['dimensions'] as $k => $v) {
				$dimension = new Google_Service_AnalyticsReporting_Dimension();
				$dimension->setName($k);
				$dimensions[] = $dimension;
				$dimension_names[] = $v;
			}
		}
 
		/* set metrics - required */
		if (!isset($param['metrics']) || empty ($param['metrics'])) {
			echo 'Error: No metrics have been set';
			return;
		}
		else {
			$metrics = array();
			$metric_names = array();
			foreach ($param['metrics'] as $k => $v) {
				$metric = new Google_Service_AnalyticsReporting_Metric();
				$metric->setExpression($k);
				$metric->setAlias($v);
				$metrics[] = $metric;
				$metric_names[] = $v;
			}
		}

		/* set dimension filter - optional */
		if (isset($param['dimFilter']) && isset($param['dimFilter']['dimensionName']) && isset($param['dimFilter']['operator']) && isset($param['dimFilter']['expression'])) {
			$filter = new Google_Service_AnalyticsReporting_DimensionFilter();
			$filter->setDimensionName($param['dimFilter']['dimensionName']);
			$filter->setOperator($param['dimFilter']['operator']);
			$filter->setExpressions($param['dimFilter']['expression']);
			if (isset($param['dimFilter']['exclude'] ))
				$filter->setNot($param['dimFilter']['exclude']);
			$filter_clause = new Google_Service_AnalyticsReporting_DimensionFilterClause();
			$filter_clause->setFilters($filter);
		}
 
		/* set order by - optional */
		if (isset($param['orderField'])) {
			$orderby = new Google_Service_AnalyticsReporting_OrderBy();
			$orderby->setFieldName($param['orderField']);
			if (isset( $param['orderType']))
				$orderby->setSortOrder($param['orderType']);
		 }

		/* Create the ReportRequest object */
		$request = new Google_Service_AnalyticsReporting_ReportRequest();
		$request->setViewId($view_id);
		$request->setDateRanges($dateRange);
		$request->setDimensions($dimensions);
		$request->setMetrics($metrics);
		if (isset($orderby))
			$request->setOrderBys($orderby);
		if (isset($filter_clause))
			$request->setDimensionFilterClauses($filter_clause);
		if (isset($param['pageSize']))
			$request->setPageSize($param['pageSize']);

		/* set the request body */
		$body = new Google_Service_AnalyticsReporting_GetReportsRequest();

		/* iterate to call the API and store the response data, 500 a time */
		$pageToken;
		$report = array();
		do {
			/* fire the request */
			$body->setReportRequests(array($request));
			$response = $analytics->reports->batchGet($body);

			/* get rows from the response */
			$rows = $this->getRows($response, $report, $dimension_names);

			/* merge rows into the report */
			$report = array_merge($report, $rows);

			/* set next page token to get the next 500 rows */
			$pageToken = $response[0]->getNextPageToken();
			$request->setPageToken($pageToken);

		} while ( $pageToken != null );
		return $report;
	}

	function getRows($response, $report, $dimension_names) {
		/* get dimension headers and metric headers from the API response */
		for ($i = 0; $i < count($response); $i++) {
			$header = $response[$i]->getColumnHeader();
			$dimensionHeaders = $header->getDimensions();
			$metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
			$rows = $response[$i]->getData()->getRows();

			/* iterate row by row */
			$report_rows = array();
			for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
				$row = $rows[$rowIndex];
				$dimensions = $row->getDimensions();
				$metrics = $row->getMetrics();
				$result_row = array();

				/* get dimension data */
				for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
					$result_row[$dimension_names[$i]] = $dimensions[$i];
				}

				/* get metric data */
				for ($i = 0; $i < count($metricHeaders); $i++) {
					$result_row[$metricHeaders[$i]->getName()] = $metrics[0]->getValues()[$i];
				}

				/* add the row to the report */
				$report_rows[] = $result_row;
			}
		}
		return $report_rows;
	}

	function printResults($reports) {

		for ( $reportIndex = 0; $reportIndex < count( $reports ); $reportIndex++ ) {
			$report = $reports[ $reportIndex ];
			$header = $report->getColumnHeader();
			$dimensionHeaders = $header->getDimensions();
			$metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
			$rows = $report->getData()->getRows();

			for ( $rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
				$row = $rows[ $rowIndex ];
				$dimensions = $row->getDimensions();
				$metrics = $row->getMetrics();
				for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
					//print($dimensionHeaders[$i] . ": " . $dimensions[$i] . "\n");
				}

				for ($j = 0; $j < count($metrics); $j++) {
					$values = $metrics[$j]->getValues();
					for ($k = 0; $k < count($values); $k++) {
						$entry = $metricHeaders[$k];
						$this->data[$entry->getName()] = $values[$k];
					}
				}
			}
		}

		print_r(json_encode($this->data));
	}

}

/* End of file analytics.php */
/* Location: ./application/controllers/analytics.php */