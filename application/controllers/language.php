<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Language extends MY_Controller {

    private $data = array();

    public function __construct() {
        parent::__construct();

        $this->data['section_name'] = 'language';
        $this->data['section_url'] = $this->data['section_name'] . '/';

		// load data
        $this->data = array_merge($this->data, $this->commondata);

        // load model
        $this->load->model('Language_model');
    }

    function index() {

        $per_page = 10;
        $uri_segment = 3;
        $offset = $this->uri->segment($uri_segment, 0);

        $total_rows = $this->Language_model->count_all();
        $this->data['records'] = $this->Language_model->get_all($per_page, $offset);

        // generate pagination
        $this->load->library('pagination');
        $pagination_config = $this->bootstrap_pagination_config;
        $pagination_config['base_url'] = site_url($this->data['section_url'] . 'index/');
        $pagination_config['total_rows'] = $total_rows;
        $pagination_config['per_page'] = $per_page;
        $pagination_config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($pagination_config);
        $this->data['pagination'] = $this->pagination->create_links();

        // load view
        $data = $this->data;
        $this->load->view('client_header', $data);
        $this->load->view('language_list', $data);
        $this->load->view('client_footer', $data);
    }

    function info() {

        $action = $this->input->post('action');

        if ($action) {

			// set validation properties
            $this->form_validation->set_rules('description', 'Description', 'trim|required');
			$this->form_validation->set_rules('shortcode', 'Shortcode', 'trim|required|max_length[10]');
            $this->form_validation->set_rules('status', 'Status', 'trim|required');
            $this->form_validation->set_error_delimiters('<p>', '</p>');
            $id = $this->input->post('id');

            // run validation
            if ($this->form_validation->run() == FALSE) {
                $this->data['id'] = $id;
                $this->data['description'] = $this->input->post('description');
				$this->data['shortcode'] = $this->input->post('shortcode');
				$this->data['status'] = $this->input->post('status');
            } else {

				$versiontemplates = $this->input->post('versiontemplates');
				if(!is_array($versiontemplates)) {
					$versiontemplates = array();
				}

                $record = array('language_description' => $this->input->post('description'),
					'language_shortcode' => $this->input->post('shortcode'),
                    'language_status' => $this->input->post('status'));

				if (empty($id)) {
                    $id = $this->Language_model->insert($record);

                    $this->session->set_flashdata('alert_type', 'success');
                    $this->session->set_flashdata('alert_msg', 'Record has been inserted successfully!');
                } else {
                    $this->Language_model->update($id, $record);

                    $this->session->set_flashdata('alert_type', 'success');
                    $this->session->set_flashdata('alert_msg', 'Record has been updated successfully!');
                }

                // redirect to list page
                redirect($this->data['section_url'], 'location', 303);
            }
        } else {

            $id = $this->uri->segment(2, 0);

            if (!empty($id) && is_numeric($id)) {
                // prefill form values
                $record = $this->Language_model->get_by_id($id);
                $this->data['id'] = $record->language_id;
                $this->data['description'] = $record->language_description;
				$this->data['shortcode'] = $record->language_shortcode;
                $this->data['status'] = $record->language_status;
            } else {
                $this->data['id'] = '';
                $this->data['description'] = '';
                $this->data['shortcode'] = '';
                $this->data['status'] = 1;
            }
        }

        // load view
        $data = $this->data;
        $this->load->view('client_header', $data);
        $this->load->view('language_info', $data);
        $this->load->view('client_footer', $data);
    }

    function delete() {

        $id = $this->uri->segment(2);

        if (!empty($id) and is_numeric($id)) {

            if($this->current_user->super_admin){
                // Actually delete stuff.
                $this->Language_model->delete($id);
            } else {
                $record = array('language_status' => '2');

                //update data
    			$this->Language_model->update($id, $record);
            }

            // set alert message
            $this->session->set_flashdata('alert_type', 'success');
            $this->session->set_flashdata('alert_msg', 'Record has been deleted successfully!');
        }

        // redirect to list page
        redirect($this->data['section_url'], 'location', 303);
    }

}

/* End of file language.php */
/* Location: ./application/controllers/language.php */
