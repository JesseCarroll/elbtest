<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends MY_Controller {
	public function __construct() {
		parent::__construct();

		# load model
		$this->load->model('Role_model');
	}

	function index() {
		$data = $this->commondata;

		$per_page = 10;
		$uri_segment = 3;
		$offset = $this->uri->segment($uri_segment, 0);

		$total_rows = Role_model::count_all();
		$data['records'] = Role_model::get_all($per_page, $offset);

		# generate pagination
		$this->load->library('pagination');
		$pagination_config = $this->bootstrap_pagination_config;
		$pagination_config['total_rows'] = $total_rows;
		$pagination_config['per_page'] = $per_page;
		$pagination_config['uri_segment'] = $uri_segment;
		$this->pagination->initialize($pagination_config);
		$data['pagination'] = $this->pagination->create_links();

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('role_list', $data);
		$this->load->view('client_footer', $data);
	}

	public function check_unique_role_code($code) {
		$id = $this->input->post('id') ? $this->input->post('id') : null;
		$result = Role_model::is_unique_role_code($code, $id);
		if (true === $result) {
			return true;
		}

		$this->form_validation->set_message(__FUNCTION__, 'Role code must be unique');
		return false;
	}

	function info() {

		$action = $this->input->post('action');
		$data = $this->commondata;
		$data['id'] = '';
		$data['title'] = '';
		$data['code'] = '';
		$data['status'] = Role_model::STATUS_ACTIVE;

		if ($action) {

			$id = $this->input->post('id');
			if (!empty($id)) {
				$record = Role_model::get_by_id($id);
			}

			# set validation properties
			$this->form_validation->set_rules('title', 'Title', 'trim|required|max_length[45]');
			$this->form_validation->set_rules('code', 'Code', 'trim|required|alpha_numeric|max_length[5]||callback_check_unique_role_code');
			$this->form_validation->set_rules('status', 'Status', 'trim|required');
			$this->form_validation->set_error_delimiters('<p>', '</p>');

			# run validation
			if ($this->form_validation->run() == FALSE) {
				$data['id'] = $id;
				$data['title'] = $this->input->post('title');
				$data['code'] = $this->input->post('code');
				$data['status'] = $this->input->post('status');
			} else {
				$record = array('role_title' => $this->input->post('title'),
					'role' => $this->input->post('code'),
					'role_status' => $this->input->post('status'));

				if (empty($id)) {

					# insert data
					$id = Role_model::insert($record);

					# set alert message
					$this->_set_Flash('Record has been inserted successfully!');

				} else {

					#update data
					Role_model::update($id, $record);

					# set alert message
					$this->_set_flash('Record has been updated successfully!');
				}

				# redirect to list page
				redirect('roles', 'refresh');
			}
		} else {

			$id = $this->uri->segment(2, 0);

			if (!empty($id) && is_numeric($id)) {

				# prefill form values
				$record = Role_model::get_by_id($id);
				$data['id'] = $record->role_id;
				$data['title'] = $record->role_title;
				$data['code'] = $record->role;
				$data['status'] = $record->role_status;

			}
		}

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('role_info', $data);
		$this->load->view('client_footer', $data);
	}

	function delete() {
		$id = $this->uri->segment(2);

		if (!empty($id) && is_numeric($id)) {

			# super admins can actually delete roles; other users can only
			# mark roles as deleted
			 
			# TODO: There probably needs to be more work done here to clean up
			# references to a deleted role in the super admin case
			if ($this->current_user->super_admin) {
				Role_model::delete($id);
			} else {
				$record = array('role_status' => Role_model::STATUS_DELETED);
				Role_model::update($id, $record);
			}
			$this->_set_flash('Record has been deleted successfully!');
		}

		# redirect to list page
		redirect('roles', 'refresh');
	}

}

/* End of file roles.php */
/* Location: ./application/controllers/roles.php */
