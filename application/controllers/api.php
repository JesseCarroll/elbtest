<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends MY_Controller {

	private $data = array();

	public function __construct() {
		parent::__construct();

		// load data
		$this->data = array_merge($this->data, $this->commondata);

		/*
		 * adding ")]}',\n" for known JSON vulnerability
		 * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
		 */
	}

	function index() {
		$this->load->view('client_header', $this->data);
		$this->load->view('client_footer', $this->data);
	}

	function assets($id='', $filename='') {
		$method = $_SERVER['REQUEST_METHOD'];
		$asset_root = $this->config->item('assets_path', 'default');

		if($id) {
			$course = Course_model::get_by_id($id);
			$asset_root = $course->course_folder_path('pre/course/assets/');

			switch($method) {
				case 'GET':
					$data = $this->aws->ls("$asset_root");
					break;
				case 'DELETE':
					if(!isset($filename)) return;
					$data = $this->aws->delete_by_key("$asset_root$filename");
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = $this->aws->ls("$asset_root");
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function clients($id='') {
		$method = $_SERVER['REQUEST_METHOD'];
		$this->load->model('Client_model');

		if($id) {
			switch($method) {
				case 'GET':
					$data = Client_model::get_by_id($id);
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = $this->Client_model->update($id, $request);
					break;
				case 'DELETE':
					$data = $this->Client_model->delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = $this->current_user->get_allowed_clients();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = Client_model::insert($request);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function courses($id="") {
		$method = $_SERVER['REQUEST_METHOD'];
		$this->load->model('Course_model');

		if($id) {
			switch($method) {
				case 'GET':
					$course = Course_model::get_by_id($id);

					$data['record'] = $course;
					$data['chameleon'] = $course->load_chameleon_json();
					$data['data'] = $course->load_course_json();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = $this->Course_model->update($id, $request);
					break;
				case 'DELETE':
					$data = $this->Course_model->delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = $this->current_user->get_allowed_courses();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = Course_model::insert($request);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function data($id="", $state ="pre") {

		$method = $_SERVER['REQUEST_METHOD'];

		switch($method) {
			case 'GET':
				$course = Course_model::get_by_id($id);
				$data = $course->load_course_json($state);
				break;
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function userroles($id="") {
		//$this->load->model('Userrole_model');
		$method = $_SERVER['REQUEST_METHOD'];

		if($id) {
			switch($method) {
				case 'GET':
					$data = Role_model::get_by_id($id);
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = Role_model::update($id, $request);
					break;
				case 'DELETE':
					$data = User_model::delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = Role_model::get_all();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = Role_model::insert($request);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function user($id="") {
		$method = $_SERVER['REQUEST_METHOD'];

		if($id) {
			switch($method) {
				case 'GET':
					$data = User_model::get_by_id($id);

					// if the can action is set, we are checking the permission of a user
					// so we will return that instead of the user data
					if(isset($_GET['action']) && $_GET['action'] == 'can') {
						$client_id = isset($_GET['client_id']) ? $_GET['client_id'] : null;
						$course_id = isset($_GET['course_id']) ? $_GET['course_id'] : null;
						$permission = $data->can($_GET['task'], $client_id, $course_id);
						$data = [];
						$data['success'] = $permission;
					}
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = User_model::add_update($request, $id);
					break;
				case 'DELETE':
					$data = User_model::delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = User_model::get_all();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = User_model::add_update($request);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function users($id="") {
		$method = $_SERVER['REQUEST_METHOD'];

		if($id) {
			switch($method) {
				case 'GET':
					$data = Client_model::get_users($id);
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function versions($id="") {
		$method = $_SERVER['REQUEST_METHOD'];

		if($id) {
			switch($method) {
				case 'GET':
					if($id == 'latest') {
						$data = $this->Version_model->get_latest_version();
					} else {
						$data = $this->Version_model->get_by_id($id);
					}
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = $this->Version_model->update($id, $request);
					break;
				case 'DELETE':
					$data = $this->Version_model->delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = $this->Version_model->get_all();
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = $this->Version_model->insert($request);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}

	function update_db() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		$request->data->date_updated = date('Y-m-d H:i:s');
		$this->data['client'] = Client_model::get_by_id($request->data->client_id);
		$this->data['client']->update($request->data);
	}

	function upload() {
		try {
			$result_ar = $this->media->ingest($this->input->post('path'));
			$result_ar['code'] = 200;
			$result_ar['message'] = 'Success';
		}
		catch (Exception $e) {
			http_response_code(500);
			$result_ar['code'] = 500;
			$result_ar['message'] = $e->getMessage();
		}
		header('Content-Type: text/json');
		echo json_encode($result_ar);
	}

	/**
	 * Render the course referenced by the given ID in a PDF document as described in CHAM-3268.
	 */
	function course_pdf($course_id) {
		try {
			# disable output buffering so we can send incremental progress updates
			while(ob_get_level() > 0) {
				ob_end_clean();
			}

			# get course/client models
			$course = Course_model::get_by_id($course_id);
			if (empty($course)) {
				throw new Exception('Course not found');
			}
			$client = Client_model::get_by_id($course->client_id);
			if (!$this->current_user->can('export_pdf', $client->client_id, $course->course_id)) {
				throw new Exception('No permission to export');
			}

			# get course curriculum model
			if (!empty($course->course_curriculum)) {
				$this->load->model('Curriculum_model');
				$curriculum = $this->Curriculum_model->get_by_id($course->course_curriculum);
				$course->curriculum_name = $curriculum->curriculum_name;
			}

			# render the PDF
			require_once FCPATH . 'application/libraries/CoursePdf.php';
			$pdf = new CoursePdf($course, $client);
			$data = $pdf->render();

			# save the PDF to S3
			$dst_root = $course->course_folder_path('cloud/downloads');
			$filename = preg_replace('#[^a-zA-Z]#', '', $course->course_title) . '.pdf';
			$this->aws->add_content($data, "$dst_root/$filename", 'application/pdf');
			$download_url = $this->config->item('aws_path', 'default') . "$dst_root/$filename";
			echo "download_url: $download_url\n";
			flush();
		}
		catch (Exception $e) {
			echo "progress_pct: 0\n";
			echo "error: 1\n";
			echo "message: {$e->getMessage()}\n";
			flush();
		}
		exit;
	}

	/*
	 * This API method publishes actions that support the RESTful querying and
	 * manipulation of a course repository.
	 *
	 * GET: render a JSON-encoded list of all assets in the course repository,
	 * sorted in natural order
	 *
	 * DELETE: delete a specific asset in the repository, specified by filename
	 */
	function courserepository($course_id, $filename) {
		// the CI framework apparently does not URL decode values that are routed
		// to controller methods, so we need to do that for any arguments that may
		// contain URL-encoded character
		if (is_string($filename)) {
			$filename = urldecode($filename);
		}
		try {
			$http_action = strtolower($_SERVER['REQUEST_METHOD']);
			$course = Course_model::get_by_id($course_id);
			$repository_root = $course->course_folder_path('cloud/repository');

			switch ($http_action) {
			case 'get':
				$result = $this->aws->ls("$repository_root/", false);
				$asset_filename_ar = array_keys($result);
				//ob_start();
				//print_r($asset_filename_ar);
				natcasesort($asset_filename_ar);
				//print_r($asset_filename_ar);
				//file_put_contents('/tmp/sort.txt', ob_get_clean());
				echo json_encode(array_values($asset_filename_ar));
				break;
			case 'delete':
				if (empty($filename)) {
					throw new Exception('No filename was provided');
				}
				$key = urldecode("$repository_root/$filename");
				$result = $this->aws->delete_by_key($key);
				//ob_start();
				//var_dump($result);
				//file_put_contents('/tmp/delete.txt', "$key: " . ob_get_clean());
				break;
			}
		}
		catch (Exception $e) {
			$code = $e->getCode() > 0 ? $e->getCode : 500;
			http_response_code($code);
			echo $e->getMessage();
		}
	}

	/*
	 * This API method publishes a service for obtaining signed authorization data
	 * that will allow a user's browser to upload a file directly to S3.
	 * Currently this is  restricted to only allowing direct upload of a file to a
	 * location within a specific course, although this restriction could be
	 * loosened in the future if necessary.
	 *
	 * The first argument is the ID of the course we want the browser to be able
	 * to upload to.
	 *
	 * The second argument is the maximum size of the uploaded file in bytes.
	 *
	 * All remaining arguments are components of the course-relative destination
	 * path to the file we want to authorize for upload.
	 *
	 * See the AWS S3 library for details on how the authorization information is
	 * created and packaged.
	 */
	function s3_directpost() {
		$args = func_get_args();
		$course_id = array_shift($args);
		$max_upload_size = array_shift($args);
		$course_relative_filepath = urldecode(implode('/', $args));

		$course = Course_model::get_by_id($course_id);
		$destination_file_key = $course->course_folder_path($course_relative_filepath);

		echo json_encode($this->aws->directpost_form_fields($destination_file_key, $max_upload_size));
	}

	/*
	 * This API method publishes actions that support media operations.
	 *
	 * get_adobe_image_editor_api_key:
	 * Provides a means for the Angular front-end to discover the API key needed
	 * for instantiating the Adobe Aviary image editor.
	 *
	 * download_to_course:
	 * Accepts a URL, downloads it, and then uploads the result to a media
	 * folder in the specified course.
	 *
	 * initialize_translation:
	 * Initializes the translation for a specific media object within a specific
	 * course.  This is needed because media objects can be marked as translated,
	 * but this is the highest level of granularity that our data structures
	 * afford.  In other words, a specific media asset within a media object
	 * cannot be translated in isolation.  If a media object contains image assets
	 * A.jpg, B.jpg and C.jpg, then all of these assets must be copied to the
	 * media folder for the language we're translating to.
	 */
	function media() {
		try {
			# determine which action to perform with a query string parameter
			if (!isset($_GET['action'])) {
				throw new Exception('No action specified');
			}
			switch ($_GET['action']) {
			case 'copy_theme_assets':
				# keep running even if the other end hangs up
				ignore_user_abort(true);

				# shut down output buffering to make it possible for us to flush updates to
				# the output stream as execution happens
				while (ob_get_level() > 0) {
					ob_end_clean();
				}

				# get media root for the specified course
				if (!isset($_GET['src_client_id'])) {
					throw new Exception('No source client ID specified');
				}
				if (!isset($_GET['dst_client_id'])) {
					throw new Exception('No destination client ID specified');
				}
				$src_client = Client_model::get_by_id($_GET['src_client_id']);
				$src_client_path = $src_client->client_folder_path('images');
				$dst_client = Client_model::get_by_id($_GET['dst_client_id']);
				$dst_client_path = $dst_client->client_folder_path('images');

				print_r($src_client_path);
				print_r($dst_client_path);

				# ingest POST data
				$postdata = file_get_contents('php://input');
				$input_ar = json_decode($postdata, true);
				if (null === $input_ar) {
					throw new Exception('Unable to interpret input as JSON string');
				}
				if (!is_array($input_ar)) {
					throw new Exception('POST did not decode as an array');
				}

				# copy each media asset as specified by the POST data
				$total = count($input_ar);
				$i = 1;
				foreach ($input_ar as $src_filename) {
					$src_filepath = "$src_client_path/$src_filename";
					$dst_filepath = "$dst_client_path/$src_filename";
					$this->aws->copy_content($src_filepath, $dst_filepath);
					echo "copied '$src_filepath' to '$dst_filepath'\n";
					printf("progress_pct: %d\n", $i / $total * 100);
					flush();
					$i++;
				}
				break;
			case 'copy_media_assets':
				# keep running even if the other end hangs up
				ignore_user_abort(true);

				# shut down output buffering to make it possible for us to flush updates to
				# the output stream as execution happens
				while (ob_get_level() > 0) {
					ob_end_clean();
				}

				# get media root for the specified course
				if (!isset($_GET['course_id'])) {
					throw new Exception('No course ID specified');
				}
				$course = Course_model::get_by_id($_GET['course_id']);
				$media_root = $course->course_folder_path('pre/course/media');

				# ingest POST data
				$postdata = file_get_contents('php://input');
				$input_ar = json_decode($postdata, true);
				if (null === $input_ar) {
					throw new Exception('Unable to interpret input as JSON string');
				}
				if (!is_array($input_ar)) {
					throw new Exception('POST did not decode as an array');
				}

				# copy each media asset as specified by the POST data
				$total = count($input_ar);
				$i = 1;
				foreach ($input_ar as $src_filename => $info) {
					$src_filepath = "$media_root/$src_filename";
					$dst_filepath = "$media_root/{$info['dst_filename']}";
					$this->aws->copy_content($src_filepath, $dst_filepath);
					echo "copied '$src_filepath' to '$dst_filepath'\n";
					if (!empty($info['hls'])) {
						$src_folder = preg_replace('#^(.*)\.[^\.]*$#', '$1', $src_filename);
						$dst_folder = preg_replace('#^(.*)\.[^\.]*$#', '$1', $info['dst_filename']);
						$this->aws->copy_content_hls($src_folder, $dst_folder);
					}
					printf("progress_pct: %d\n", $i / $total * 100);
					flush();
					$i++;
				}
				break;
			case 'enumerate_media_assets':
				if (!isset($_GET['course_id'])) {
					throw new Exception('No course ID provided');
				}
				$course = Course_model::get_by_id($_GET['course_id']);
				$media_root = $course->course_folder_path('pre/course/media');
				$asset_ar = $course->enumerate_media_assets("$media_root/");
				echo json_encode($asset_ar);
				break;
			case 'initialize_translation':
				# ingest input
				$postdata = file_get_contents('php://input');
				$input_ar = json_decode($postdata, true);

				if (null === $input_ar) {
					throw new Exception("Unable to interpret input as JSON string");
				}
				foreach (array('course_id', 'language_shortcode', 'media') as $required_key) {
					if (!isset($input_ar[$required_key])) {
						throw new Exception("Missing required key '$required_key' in input");
					}
					${$required_key} = $input_ar[$required_key];
				}

				# does the media object have an ID?
				# This is important so that we can correctly identify the source path of
				# the assets we're going to copy.
				$media_has_id = isset($media['id']);

				# instantiate a course object and use it to determine the destination
				# path for translated media assets
				$course = Course_model::get_by_id($course_id);
				$media_root = $course->course_folder_path('pre/course/media');
				$translation_media_root = "$media_root/translation/$language_shortcode";

				# This mapping provides two vital pieces of information. The keys tell
				# us which properties within the provided media object may hold the
				# filenames of media assets that need to be copied into the
				# translation's media folder. The values indicate the source folder
				# where we should expect to find English-language media assets for
				# legacy media objects (media objects that pre-date the introduction of
				# media IDs).
				$legacy_media_location_map = array(
					'image'  => 'images',
					'folder' => 'custom',
					'audio'  => 'audio',
					'video'  => 'video',
				);

				# look in all areas of the provided media object that may hold filenames
				# of media assets and copy them into the translation's media folder
				$warning_ar = array();
				foreach ($legacy_media_location_map as $media_object_key => $legacy_src_dirname) {
					if (!isset($media[$media_object_key])) continue;
					if (!is_array($media[$media_object_key])) {
						$media[$media_object_key] = array($media[$media_object_key]);
					}
					foreach ($media[$media_object_key] as $filename) {

						// we have just introduced a new media type that has a data structure
						// unline any other existing media data structure. so great! in this
						// case, the image object is an array of data that includes a title
						// and caption as well as the image filename. in this case, lets force
						// it to use the right key: 'src'
						$filename = (is_array($filename)) ? trim($filename['src']) : trim($filename);

						if (empty($filename)) {
							continue;
						}
						$src_filepath = "$media_root/";
						if (!$media_has_id) {
							$src_filepath .= "$legacy_src_dirname/";
						}
						$src_filepath .= $filename;

						$dst_filepath = "$translation_media_root/$filename";
						try {
							$this->aws->copy_content($src_filepath, $dst_filepath);
						}
						catch (Exception $e) {
							$warning_ar[] = "Failed to copy '$filename': {$e->getMessage()}";
						}
					}
				}

				$response = new StdClass;
				$response->warnings = $warning_ar;
				echo json_encode($response);
				break;

			case 'get_adobe_image_editor_api_key':
				echo $this->config->item('adobe_image_editor_api_key', 'default');
				break;

			case 'download_to_course':

				# arguments for the action come from within the POST payload in the
				# form a JSON object -- may wish to move this up and out of the switch
				# if adding more actions and it makes sense for them to be
				# parameterized in the same way
				$postdata = file_get_contents('php://input');
				if (false === $postdata) {
					throw new Exception('Unable to read from stdin');
				}
				$params = json_decode($postdata);
				if (null === $params) {
					throw new Exception('Unable to decode POST payload as JSON');
				}

				# check for required parameters
				foreach (array('course', 'url', 'target_filepath') as $required_param_name) {
					if (empty($params->$required_param_name)) {
						throw new Exception("Required parameter '$required_param_name' is missing or empty");
					}
				}

				# get information about the target course
				$course_id = $params->course;
				if (!preg_match('#^\d+$#', $course_id)) {
					throw new Exception("Course id must be an integer; but we received '$course_id'");
				}

				# setup options for BIW proxy
				$context = null;
				if (ENVIRONMENT == 'development' && defined('PROXY_URL')) {
					$opts = array(
						'http' => array(
							'proxy' => PROXY_URL
						)
					);
					$context = stream_context_create($opts);
				}

				# download the asset from the requested URL
				$data = file_get_contents($params->url, false, $context);
				if (false === $data) {
					throw new Exception("Unable to download '$params->url'");
				}
				$content_type = null;
				foreach ($http_response_header as $header_string) {
					if (0 !== stripos($header_string, 'Content-Type:')) {
						continue;
					}
					$content_type = trim(substr($header_string, strlen('Content-Type:')));
				}
				if (null === $content_type) {
					throw new Exception('Could not determine the content type of the edited image');
				}
				#echo "OK, I downloaded data from '{$params->url}' and the content type is '$content_type'\n";

				# write the asset to the specified filename in the course's AWS media
				# assets location
				$client_path = $this->config->item('client_path', 'default');
				if (false === $client_path) {
					throw new Exception('Could not get client path');
				}
				$client_id = $this->course->get_course_field($course_id, 'client_id');
				#echo "Now I need to upload the data to '{$params->target_filepath}'\n";
				$this->aws->add_content($data, $params->target_filepath, $content_type);

				#echo 'Done!';
				break;
			default:
				throw new Exception("Invalid action '{$_GET['action']}'", 404);
			}
		}
		catch (Exception $e) {
			$code = $e->getCode() > 0 ? $e->getCode : 500;
			http_response_code($code);
			echo $e->getMessage();
		}
		exit;
	}
}

/* End of file api.php */
/* Location: ./application/controllers/api.php */
