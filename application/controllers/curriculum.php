<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Curriculum extends MY_Controller {

	private $data = array();

	public function __construct() {
		parent::__construct();

		// load model
		$this->load->model('Curriculum_model');

		// load data
		$this->data = array_merge($this->data, $this->commondata);

		/*
		 * adding ")]}',\n" for known JSON vulnerability
		 * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
		 */
	}

	function index($id="") {
		$method = $_SERVER['REQUEST_METHOD'];

		if($id) {
			switch($method) {
				case 'GET':
					$data = $this->Curriculum_model->get_by_id($id);
					break;
				case 'POST':
					$postdata = file_get_contents("php://input");
					$request = json_decode($postdata);
					$data = $this->Curriculum_model->update($id, $request);
					break;
				case 'DELETE':
					$data = $this->Curriculum_model->delete($id);
					break;
			}
		} else {
			switch($method) {
				case 'GET':
					$data = $this->Curriculum_model->get_all($this->data['client']->client_id);
					break;
			}
		}

		if($data) { echo ")]}',\n" . json_encode($data); }
	}
}

/* End of file curriculum.php */
/* Location: ./application/controllers/curriculum.php */
