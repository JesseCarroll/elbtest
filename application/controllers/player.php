<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Player extends CI_Controller {

	private $data = array();

	public function __construct() {
		parent::__construct();

		# load models
		$this->load->model('Player_model');
		$this->load->model('User_model');

		# load libraries
		$this->load->library('Auth');

		#load config
		$this->load->config('default', TRUE);

		# set access headers
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Headers: origin, x-requested-with, content-type, x-authorization');
		header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
	}

	function index() {}

	/*
	 * save a single user email, or a comma separated list of emails for the courseware
	 * player, and map them to the assigned curricululum. Email addresses are saved in
	 * the player_user table with the client ID. Then the record ID for that email address is
	 * mapped to the curriculum ID in the player_usermap table.
	 */
	function save_users() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$duplicates = array();

		# curriculum and users are required
		if(!isset($request->users) || !isset($request->curriculum)) {
			throw new Exception('At least one user and a curriculum is required.');
		}

		# break up the user list and save each one with the client id
		$users = explode(",", $request->users);
		foreach($users as $user) {
			$email = trim($user);

			# if the email address exists already, do not store it again
			if(count($this->Player_model->get_user_by_email($email))) {
				$duplicates[] = $email;
				continue;
			}

			# save the email address and client ID in the user table
			$user_data = array(
				'email' => $email,
				'client_id' => $request->client_id
			);
			$user_id = $this->Player_model->save_user($user_data);
			if(!isset($user_id)) {
				throw new Exception('Unable to save ' . $email . ' to the database.');
			}

			# once the email is saved in the user table, use the record ID
			# to map the user to the selected curriculum
			if($user_id) {
				$usermap_data = array(
					'user_id' => $user_id,
					'curriculum' => $request->curriculum
				);
				$usermap_id = $this->Player_model->usermap($usermap_data);
				if(!isset($usermap_id)) {
					throw new Exception('Unable to link ' . $email . 'to the specified curriculum.');
				}
			}
		}

		# since duplicates were stored in a separate array, go through those now
		# so we can alert which emails were not added
		if(count($duplicates)) {
			$duplicates = implode(", ", $duplicates);
			echo $duplicates;
		}
	}


	/*
	 * passes in the user email address and invite code to check if the user
	 * is allowed to access the courseware player. returns a success object
	 * that is true or false.
	 */
	function player_user_check() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		# by default, success is false
		$inviteCheck['success'] = false;

		# email address and invite code are required
		if(!isset($request->email)) {
			throw new Exception('A valid email address is required.');
		}
		else {
			# check to see if the email address exists in the player_users table
			$result = $this->Player_model->get_user_by_email($request->email);

			# if user exists, we're good
			if(!empty($result)) {
				# generate a new login token and save the token to the usertoken table
				$token = $this->create_token($result->user_id, $result->client_id);

				# email token to the user
				$this->email_token($result->email, $token);

				# success!
				$inviteCheck['success'] = true;
			}
		}

		# return response
		echo json_encode($inviteCheck);
	}


	/*
	 * sends an email to a successfully authenticated user with a temporary validation code
	 */
	function email_token($email, $token) {

		$this->load->helper('email');

		if(valid_email($email)) {
			$subject = "Courseware Player Login Code";
			$message = "Here is your temporary login code for the Chameleon Courseware Player: " . $token;
			send_email($email, $subject, $message);
		}
	}

	/*
	 * create a login token and save it to the user in the usertoken table
	 */
	function create_token($user_id, $client_id) {
		# token will be numeric
		$seed = str_split('9081726354');
		$token = '';
		foreach (array_rand($seed, 5) as $k) $token .= $seed[$k];

		# epiration key is not just numeric
		$expire = md5(microtime().rand());

		# now lets check to see if this user already has a usertoken record
		$data = array(
			"user_id" => $user_id,
			"token" => $token,
			"expiration_key" => $expire
		);

		$user_token = $this->Player_model->get_usertoken($user_id);

		if(!empty($user_token) && !empty($user_token->expiration_key)) {
			# token record already exists, we we will just update the user token
			$data['expiration_key'] = $user_token->expiration_key;
			$this->Player_model->update_usertoken($data);
		}
		else {
			# token record does not exist, so we will create one using the
			# generated user token and expiration key
			$this->Player_model->save_usertoken($data);
		}

		return $token;
	}


	/*
	 * validate the user by making sure they have the correct login token
	 * that was emailed to them. returns a token object that is either false
	 * or returns the authorization header token (JWT).
	 */
	function final_login_check() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		# by default, the token object is false
		$inviteCheck['token'] = false;

		if(!isset($request->email)) {
			throw new Exception('Email address is required.');
		}
		if(!isset($request->access_code)) {
			throw new Exception('An Access Code is required.');
		}

		if($request->email && $request->access_code) {
			$email = $request->email;
			$access_code = $request->access_code;

			# check the usertoken table to verify the user
			$user = $this->Player_model->get_user_by_email($email);
			if(isset($user)) {
				# use the user_id to find the access token in the usertoken table
				$usertoken = $this->Player_model->get_usertoken($user->user_id);
				if(!isset($usertoken)) {
					throw new Exception('Access Code not found. Please try logging in again using your invite code.');
				}
				else {
					if($access_code === $usertoken->token) {
						# if validation is successfull, generate the authentication token that
						# will be saved in local storage and used in all subsequent server calls
						# as an authorization header
						$inviteCheck['token'] = $this->auth->generateToken($email, $usertoken->expiration_key);
					}
					else {
						throw new Exception('You have entered an invalid Access Code. Please try again.');
					}
				}
			}

			echo json_encode($inviteCheck);
		}
	}


	/*
	 * check if a user is logged in by passing in their stored token. tokens are
	 * verified by validating them with the secret key. if the token is valid, the
	 * contents of the token are returned, one item of which is a expiration key which
	 * is matched against the user to ensure the user should still have access to the
	 * courseware player (this enables us to force-expire a token if necessary)
	 */
	function is_logged_in() {
		$token_data = $this->auth_header_check();
		$result = false;

		if($token_data) {
			# pull out the expiration key from the token payload and match it to the database
			$email = $token_data->aud;
			$expiration_key = $token_data->exp_key;

			$user = $this->Player_model->get_user_by_email($email);
			$usertoken = $this->Player_model->get_usertoken($user->user_id);

			if($expiration_key == $usertoken->expiration_key) {
				# login is still valid, so return the valid login token
				$result = $token_data;
			}
		}
		echo json_encode($result);
	}

	function auth_header_check() {
		# pull request headers
		$headers = apache_request_headers();
		# check if the authorization header exists
		if(isset($headers['X-Authorization'])) {
			$request = $headers['X-Authorization'];
			if($request) {
				$token = $this->auth->decodeToken($request);
				return $token;
			}
		}
	}

	/*
	 * use this in the application to return the accessible courses for the user
	 * requires a valid authentication token, which is used to retrieve the user id
	 */
	function get_courses() {
		$token_data = $this->auth_header_check();

		if($token_data) {
			$email = $token_data->aud;
			$user = User_model::get_where(['email' => $email]);

			// previously we used curriculum to map users to courses for the courseware
			// player, but now that we have updated user role management, we are now
			// simply pulling in all courses users have a set role to - whether that is
			// explicitly in a course/client or as a part of a group setting.
			$course_array = [];
			$courses = $user->get_allowed_courses();

			# because the poster image is saved as only a file name, we need to set the full
			# image path here so the player can correctly display the poster images
			foreach($courses as $course) {
				if(isset($course->poster)) {
					$course->poster = $this->config->item('aws_path', 'default') . 'clients/' . $course->client_id . '/courses/' . $course->course_folder . '/prod/course/media/' . $course->poster;
				}

				if($course->offline == 1 && $course->date_published !== null) {
					array_push($course_array, $course);
				}
			}

			print_r(json_encode($course_array));
		}
	}
}

/* End of file player.php */
/* Location: ./application/controllers/player.php */
