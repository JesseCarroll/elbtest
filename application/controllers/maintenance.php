	<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	private $data = array();

    public function __construct() {
        parent::__construct();

		$this->config->load('migration', TRUE);

		$this->data = array_merge($this->data, $this->commondata);
    }

    function index() {

    	if ($this->current_user != null && $this->current_user->can('maintenance')) {
    		$this->data['db_version'] = $this->db->get('migrations')->row()->version;
				$this->data['configured_db_version'] = $this->config->item('migration_version', 'migration');

				$this->load->view('client_header', $this->data);
    		$this->load->view('maintenance_panel', $this->data);
    		$this->load->view('client_footer', $this->data);
    	} else {
    		$this->load->view('client_header', $this->data);
    		$this->load->view('maintenance_underway', $this->commondata);
    		$this->load->view('client_footer', $this->data);
    	}
	}

	function act() {
		if (empty($this->current_user) || !$this->current_user->can('maintenance')) {
			redirect();
		}

		$action = $this->input->get_post('action');
		if (empty($action)) {
			redirect('maintenance');
		}

		switch ($action) {
			case 'db_upgrade':
				$this->load->library('migration');
				if ($this->migration->current()) {
					$this->_set_flash('Upgraded database to version ' . $this->config->item('migration_version', 'migration'));
				} else {
					$this->_set_flash('Database upgrade error: ' . $this->migration->error_string(), 'danger');
				}
				break;
		}
		redirect('maintenance');
	}

	function db_upgrade($to_version = '') {
		if ($this->input->is_cli_request()) {
			$this->load->library('migration');
			$this->config->load('migration');
			if (empty($to_version)) {
				$to_version = $this->config->item('migration_version', 'migration');
			}
			print "Upgrading to version: " . $to_version . "\r\n";
			if (!$this->migration->version($to_version)) {
					show_error($this->migration->error_string());
			} else {
				 print "DB Upgraded Successfully\r\n";
			}
		} else {
			exit("db_upgrade msist be executed via command line");
		}
	}
}
