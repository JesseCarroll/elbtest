<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Load extends MY_Controller {

    protected $commondata = array();
    private $data = array();

    public function __construct() {
        parent::__construct();

		// load data
        $this->data = array_merge($this->data, $this->commondata);
        $this->_require_login();
    }

    function index() {
		$type = $this->uri->segment(2, 0);
        $key = $this->uri->segment(3, 0);
		$keydata = explode('-', $key);
		$found = false;
		if(isset($keydata[0]) && !empty($keydata[0])) {
			if(isset($keydata[1]) && !empty($keydata[1])) {

				// load model
				$this->load->model('Course_model');
				$courserecord = Course_model::get_by_id($keydata[1]);
				$master_course_version = $courserecord->version()->version_sub;

				$version_core_path = $this->config->item('version_core_path_ssl', 'default');
				$course_version_path = $version_core_path . $master_course_version . '/';


				switch($type) {
					case 'core':
						if (file_exists($version_core_path . 'core.js')) {
							$found = true;
							$file_content = file_get_contents($version_core_path . 'core.js');
							//replace course version url with placeholder
							$course_version_url_placeholder = $this->config->item('course_version_url_placeholder', 'default');
							$client_version_url_placeholder = $this->config->item('client_version_url_placeholder', 'default');
							$version_core_url = $this->config->item('version_core_url', 'default');
							$course_version_url = $version_core_url . $master_course_version . '/';
							$course_package_url = $this->config->item('course_package_url', 'default');
							$course_package_url = $course_package_url . $courserecord->course_folder . '/';

							$file_content = str_replace($course_version_url_placeholder, $course_version_url, $file_content);
							$file_content = str_replace($client_version_url_placeholder, $course_package_url, $file_content);
							header("Content-type: text/javascript");
							//readfile($version_core_path . 'core.js');
							echo $file_content;
						}
						break;

					case 'css':
						if (file_exists($course_version_path . 'css/vendor.css')) {
							$found = true;
							header("Content-type: text/css");
							readfile($course_version_path . 'css/vendor.css');
						}

						if ($found && file_exists($course_version_path . 'css/app.css')) {
							readfile($course_version_path . 'css/app.css');
						}
						break;

					case 'vendor':
						if (file_exists($course_version_path . 'js/vendor.js')) {
							$found = true;
							header("Content-type: text/javascript");
							readfile($course_version_path . 'js/vendor.js');
						}
						break;

					case 'template':
						if (file_exists($course_version_path . 'js/templates.js')) {
							$found = true;
							header("Content-type: text/javascript");
							readfile($course_version_path . 'js/templates.js');
						}
						break;

					case 'app':
						if (file_exists($course_version_path . 'js/app.js')) {
							$found = true;
							header("Content-type: text/javascript");
							readfile($course_version_path . 'js/app.js');
						}
						break;
				}

			}
		}

		if(!$found) {
			header("HTTP/1.0 404 Not Found");
		} else {
			exit;
		}
    }

}

/* End of file load.php */
/* Location: ./application/controllers/load.php */