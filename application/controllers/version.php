<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Version extends MY_Controller {
    private $data = array();

    public function __construct() {

        parent::__construct();

        $this->data['header']['section'] = 'Versions';
        $this->data['header']['icon'] = 'fa-code-fork';
        $this->data['section_name'] = 'version';
        $this->data['section_url'] = $this->data['section_name'] . '/';


		// load data
        $this->data = array_merge($this->data, $this->commondata);

        $this->load->helper('directory');
        $this->load->config('default', true);
        $this->load->helper('file');

        $this->data['id'] = '';
        if($this->uri->segment(2, 1) && is_numeric($this->uri->segment(2, 1))) {
            $this->data['id'] = $this->uri->segment(2, 1);
        }
    }

    function index() {

        $per_page = 10;
        $uri_segment = 4;
        $offset = $this->uri->segment($uri_segment, 0);

        $total_rows = $this->Version_model->count_all();
        $this->data['records'] = $this->Version_model->get_all($per_page, $offset);

        // generate pagination
        $this->load->library('pagination');
        $pagination_config = $this->bootstrap_pagination_config;
        $pagination_config['base_url'] = site_url($this->data['section_url'] . 'index/');
        $pagination_config['total_rows'] = $total_rows;
        $pagination_config['per_page'] = $per_page;
        $pagination_config['uri_segment'] = $uri_segment;
        $this->pagination->initialize($pagination_config);
        $this->data['pagination'] = $this->pagination->create_links();

        // load view
        $data = $this->data;
        $this->load->view('client_header', $data);
        $this->load->view('admin/version_list', $data);
        $this->load->view('client_footer', $data);
    }



    function info() {

        $action = $this->input->post('action');
        $id = $this->data['id'];

        if ($action) {

			// set validation properties
            $this->form_validation->set_rules('vnumber', 'Version Number', 'xss_clean|trim|required|max_length[10]|is_unique[versions.version_number.version_id.'.$id.']');
            $this->form_validation->set_rules('status', 'Status', 'xss_clean|required');

            // run validation
            if ($this->form_validation->run() == FALSE) {

                $this->data['id'] = $id;
				$this->data['vnumber'] = $this->input->post('vnumber', TRUE);
				$this->data['status'] = $this->input->post('status', TRUE);
            }
            else {

                $new_version = false;
                if($_FILES['course_package']['error'] == 0 && !empty($_FILES['course_package']['name'])) {
                    $new_version = true;
                }

                $record = array(
                    'version_number' => $this->input->post('vnumber', TRUE),
                    'version_sub' => $this->Version_model->get_subversion($this->input->post('vnumber'), $new_version),
                    'version_status' => $this->input->post('status', TRUE)
                );

                if (empty($id)) {
                    $record['date_created'] = date('Y-m-d H:i:s');
                    $id = $this->Version_model->insert($record);
                    $msg = 'Version has been successfully added!';
                } else {
                    $record['date_updated'] = date('Y-m-d H:i:s');
                    $this->Version_model->update($id, $record);
                    $msg = 'Version has been successfully updated!';
                }

                $this->data['version'] = $this->Version_model->get_by_id($id);
                if($new_version) {
                    $this->data['version']->setup_version_folder();
                }

                // redirect to list page
                redirect('version', 'refresh');
            }
        } else {

            if (!empty($id) && is_numeric($id)) {

                $this->data['version'] = $this->Version_model->get_by_id($id);

                // prefill form values
                $this->data['id'] = $this->data['version']->version_id;
				$this->data['vnumber'] = $this->data['version']->version_number;
                $this->data['subvnumber'] = $this->data['version']->version_sub;
                $this->data['status'] = $this->data['version']->version_status;

            } else {
                $this->data['id'] = '';
				$this->data['vnumber'] = '';
                $this->data['subvnumber'] = '';
				$this->data['status'] = 1;
            }
        }

        $this->data['header']['sub_section'] = ($this->data['id']) ? 'Update Version' : 'Add Version';
        $this->data['header']['buttons'] = array(
            anchor('version/' . $this->data['id'], 'Back', 'class="btn btn-default btn-sm"')
        );

        // load view
        $data = $this->data;
        $this->load->view('client_header', $data);
        $this->load->view('admin/version_info', $data);
        $this->load->view('client_footer', $data);
    }

    function delete() {

        $id = $this->uri->segment(2, 0);

        if (!empty($id)) {

			$record = array('version_status' => '2');

            //update data
			$this->Version_model->update($id, $record);

            // set alert message
            $this->_set_flash('Record has been deleted successfully!');
        }

        // redirect to list page
        redirect($this->data['section_url'] . 'index/', 'refresh');
    }

}

/* End of file version.php */
/* Location: ./application/controllers/version.php */