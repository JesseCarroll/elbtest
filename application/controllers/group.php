<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group extends MY_Controller {
	public function __construct() {
		parent::__construct();

		# load model
		$this->load->model('Group_model');
	}

	function index() {
		$data = $this->commondata;

		$per_page = 10;
		$uri_segment = 3;
		$offset = $this->uri->segment($uri_segment, 0);

		$total_rows = Group_model::count_all();
		$data['records'] = Group_model::get_all($per_page, $offset);

		# generate pagination
		$this->load->library('pagination');
		$pagination_config = $this->bootstrap_pagination_config;
		$pagination_config['total_rows'] = $total_rows;
		$pagination_config['per_page'] = $per_page;
		$pagination_config['uri_segment'] = $uri_segment;
		$this->pagination->initialize($pagination_config);
		$data['pagination'] = $this->pagination->create_links();

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('admin/group_list', $data);
		$this->load->view('client_footer', $data);
	}

	public function check_unique_group_name($value) {
		$id = $this->input->post('id') ? $this->input->post('id') : null;
		$result = Group_model::is_unique_group_name($value, $id);
		if (true === $result) {
			return true;
		}

		$this->form_validation->set_message(__FUNCTION__, 'Group name must be unique');
		return false;
	}

	function info() {
		$data = $this->commondata;

		# redirect out of here if the user requesting this page is not allowed to manage groups
		if (!$this->current_user->can('manage_groups')) {
			redirect();
		}

		try {
			# add/update a group if the form on this page was submitted
			if ($this->input->post('action')) {
				$id = $this->input->post('id');

				# collect form data from post
				$form_data = array(
					'name' => $this->input->post('name'),
					'client_roles' => $this->input->post('client_roles'),
					'course_roles' => $this->input->post('course_roles'),
				);

				# set validation properties
				$this->form_validation->set_rules('name', 'Name', 'trim|required|max_length[45]|callback_check_unique_group_name');
				$this->form_validation->set_error_delimiters('<p>', '</p>');

				# run validation, bailing out if anything failed
				if ($this->form_validation->run() == false) {
					$data = $form_data;
					$data['id'] = $id;
					throw new Exception('Validation did not pass');
				}

				# collect information for updating/adding group
				$record = $form_data;

				if (empty($id)) {
					# newgroup 
					Group_model::add_update($record);

					# set alert message
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_msg', 'Record has been inserted successfully!');
				} else {
					# updating existing group 
					Group_model::add_update($record, $id);

					# set alert message
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_msg', 'Record has been updated successfully!');
				}

				# redirect to list page
				redirect('group', 'location', 303);
				exit;
			}

			$id = $this->uri->segment(2, 0);
			if ($id === 'new') {
				$id = '';
			}
			if (!empty($id)) {
				# prefill form values
				$model = Group_model::get_by_id($id);
				$data['id'] = $model->group_id;
				$data['name'] = $model->name;
				$data['client_roles'] = $model->get_client_roles();
				$data['course_roles'] = $model->get_course_roles();
				$data['members'] = $model->get_users();
			} else {
				$data['id'] = '';
				$data['name'] = '';
				$data['client_roles'] = array();
				$data['course_roles'] = array();
				$data['members'] = array();
			}
		}
		catch (Exception $e) {}

		$data['all_clients'] = Client_model::get_all();

		$this->load->model('Role_model');
		$data['all_active_roles'] = Role_model::get_all_active();

		$this->load->model('Group_model');
		$data['all_groups'] = Group_model::get_all();

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('admin/group_info', $data);
		$this->load->view('client_footer', $data);
	}

	function delete() {
		$id = $this->uri->segment(2);

		if ($this->current_user->super_admin && !empty($id) && is_numeric($id)) {
			Group_model::delete($id);
			$this->_set_flash('Record has been deleted successfully!');
		}

		# redirect to list page
		redirect('group', 'refresh');
	}

}

/* End of file group.php */
/* Location: ./application/controllers/group.php */
