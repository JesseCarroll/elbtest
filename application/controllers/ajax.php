<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Ajax extends CI_Controller {
	const EX_CLOBBER_SESSION_PROMPT = 1;

	private $data = array();

	public function __construct() {
		parent::__construct();
		$this->load->helper('cookie');
	}

	function login() {
		$result_ar = array(
			'result'           => true,
			'msg'              => '',
			'login_session_id' => uniqid(),
		);

		try {
			$email          = $this->input->post('email');
			$password       = $this->input->post('password');
			$input_error_ar = array();

			# error messages for empty email and/or password
			if (empty($email)) {
				$input_error_ar[] = 'Please enter your email address.';
			}
			if (empty($password)) {
				$input_error_ar[] = 'Please enter your password.';
			}
			if (!empty($input_error_ar)) {
				throw new Exception(implode('<br>', $input_error_ar));
			}

			# test the email/password combo
			$user_record = User_model::login($email, $password);
			if (empty($user_record)) {
				throw new Exception('The email address or password you entered is incorrect.');
			}

			# check for a record of a recent login session in the user record; if one
			# exists that may not have expired yet, we need to ask the user whether
			# or not it's OK to wipe out the previous login session
			$can_we_clobber_previous_session = $this->input->post('clobber_previous_session');
			if (
				!$can_we_clobber_previous_session &&
				preg_match('#^([^:]+):(.*)$#', $user_record['login_status'], $matches) &&
				time() < $matches[2] + $this->config->item('sess_expiration')
			) {
				throw new Exception($matches[2], self::EX_CLOBBER_SESSION_PROMPT);
			}

			# generate a new value for the login_status field in the user record
			$login_status = sprintf('%s:%s', $result_ar['login_session_id'], time());
			$user_record['login_status'] = $login_status;
			User_model::add_update(array(
				'login_status' => $login_status,
			), $user_record['user_id']);

			# place the user data into the login session
			$this->session->set_userdata($user_record);

			# issue a cookie to the user that will store the login session ID that we 
			# just generated for the login session we're setting up; their browser 
			# must render this cookie when checking the validity of the login 
			# session; if what they provide during this check does not match what is 
			# recorded in their user record in the database, this means that another 
			# login event has clobbered and invalidated their current session
			set_cookie('login_session_id', $result_ar['login_session_id'], 0);

			# if the password needs to be upgraded to a hashed password, or if the
			# password is flagged as needing to be changed, redirect to the profile
			# area with an appropriate message
			$password_change_reason = null;
			if (empty($user_record['hash'])) {
				$password_change_reason = 'In an effort to incease the security of your account, please update your password below.';
			}
			elseif ($user_record['reset'] == 1) {
				$password_change_reason = 'Please enter your new password below.';
			}
			if (null !== $password_change_reason) {
				$this->session->set_flashdata('alert_type', 'warning');
				$this->session->set_flashdata('alert_msg', $password_change_reason);
				$result_ar['msg'] = site_url("user/{$user_record['user_id']}/profile");
			}
		}
		catch (Exception $e) {
			$result_ar = array(
				'result' => false,
				'msg'    => $e->getMessage(),
			);
			if (self::EX_CLOBBER_SESSION_PROMPT == $e->getCode()) {
				$result_ar['previous_session'] = true;
			}
		}

		# print result JSON
		echo json_encode($result_ar);
	}

	function logout() {
		$user_id = $this->session->userdata('user_id');
		User_model::add_update(array('login_status' => ''), $user_id);

		$this->session->sess_destroy();
		$sess_cookie_name = $this->config->item('sess_cookie_name');
		delete_cookie($sess_cookie_name);
		redirect();
	}
}

/* End of file ajax.php */
/* Location: ./application/controllers/ajax.php */
