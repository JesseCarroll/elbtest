<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class User extends MY_Controller {

	private $data = array();

	public function __construct() {
		parent::__construct();

		$this->data['section'] = 'User';
		$this->data['section_icon'] = 'fa-user';
		$this->data['section_name'] = 'user';
		$this->data['section_url'] = $this->data['section_name'] . '/';

		# load data
		$this->data = array_merge($this->data, $this->commondata);

	}

	function can() {
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		echo $this->current_user->can($request->task);
	}

	function test() {
		// for Super Admins ONLY: allows user to emulate being logged in as a different user for testing
		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		# add test user id to session
		if(isset($request->id)) {
			$this->session->set_userdata('test_user', $request->id);
		} else {
			$this->session->unset_userdata('test_user');
		}
	}

	function index() {

		if(!$this->current_user->can('manage_users')){ redirect(); }

		# load view
		$data = $this->data;
		$this->load->view('client_header', $data);
		$this->load->view('admin/user_list', $data);
		$this->load->view('client_footer', $data);
	}

	public function check_unique_email($email) {
		$id = $this->input->post('id') ? $this->input->post('id') : null;
		$result = User_model::is_unique_user_email($email, $id);
		if (true === $result) {
			return true;
		}

		$this->form_validation->set_message(__FUNCTION__, 'Email must be unique');
		return false;
	}

	function info() {
		$data = $this->commondata;

		# redirect out of here if the user requesting this page is not allowed to manage users
		if (!$this->current_user->can('manage_users')) {
			redirect();
		}

		try {
			# add/update a user if the form on this page was submitted
			if ($this->input->post('action')) {
				$id = $this->input->post('id');

				# collect form data from post
				$form_data = array(
					'client_id' => $this->input->post('client_id', true),
					'first_name' => $this->input->post('first_name', true),
					'last_name' => $this->input->post('last_name', true),
					'email' => $this->input->post('email', true),
					'password' => $this->input->post('password', true),
					'user_status' => $this->input->post('user_status', true),
					'super_admin' => $this->input->post('super_admin', true),
					'client_roles' => $this->input->post('client_roles', true),
					'course_roles' => $this->input->post('course_roles', true),
					'groups' => $this->input->post('groups', true),
				);

				# set validation properties
				$this->form_validation->set_rules('client_id', 'Client', 'xss_clean|trim|required');
				$this->form_validation->set_rules('first_name', 'First Name', 'xss_clean|trim|required|max_length[45]');
				$this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean|trim|required|max_length[45]');
				$this->form_validation->set_rules('email', 'Email', 'xss_clean|trim|required|valid_email|callback_check_unique_email');
				$this->form_validation->set_rules('password', 'Password', 'xss_clean|trim|max_length[45]');
				$this->form_validation->set_rules('user_status', 'Status', 'xss_clean|trim|required');
				$this->form_validation->set_rules('super_admin', 'Super Admin?', 'xss_clean|trim');
				$this->form_validation->set_error_delimiters('<p>', '</p>');

				# run validation, bailing out if anything failed
				if ($this->form_validation->run() == false) {
					$data = $form_data;
					$data['id'] = $id;
					throw new Exception('Validation did not pass');
				}

				# collect information for updating/adding user
				$record = $form_data;

				if (empty($id)) {
					# new user
					User_model::add_update($record);

					# set alert message
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_msg', 'Record has been inserted successfully!');
				} else {
					# updating existing user
					User_model::add_update($record, $id);

					# set alert message
					$this->session->set_flashdata('alert_type', 'success');
					$this->session->set_flashdata('alert_msg', 'Record has been updated successfully!');
				}

				# redirect to list page
				redirect('user', 'location', 303);
				exit;
			}

			$id = $this->uri->segment(2, 0);
			if ($id === 'new') {
				$id = '';
			}
			if (!empty($id)) {
				# prefill form values
				$model = User_model::get_by_id($id);
				$data['id'] = $model->user_id;
				$data['client_id'] = $model->client_id;
				$data['super_admin'] = $model->super_admin;
				$data['first_name'] = $model->first_name;
				$data['last_name'] = $model->last_name;
				$data['email'] = $model->email;
				$data['password'] = User_model::HASHED_PASSWORD_PLACEHOLDER;
				$data['user_status'] = $model->user_status;
				$data['client_roles'] = $model->get_client_roles();
				$data['course_roles'] = $model->get_course_roles();
				$data['groups'] = $model->get_groups();
			} else {
				$data['id'] = '';
				$data['client_id'] = '';
				$data['role'] = '';
				$data['first_name'] = '';
				$data['last_name'] = '';
				$data['email'] = '';
				$data['password'] = '';
				$data['user_status'] = 1;
				$data['client_roles'] = array();
				$data['course_roles'] = array();
				$data['groups'] = array();
			}
		}
		catch (Exception $e) {}

		$data['all_clients'] = Client_model::get_all();

		$this->load->model('Role_model');
		$data['all_active_roles'] = Role_model::get_all_active();

		$this->load->model('Group_model');
		$data['all_groups'] = Group_model::get_all();

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('user_info', $data);
		$this->load->view('client_footer', $data);
	}

	function profile() {
		$data = $this->commondata;
		$data['id'] = $this->current_user->user_id;

		# updating password
		if ($this->input->post('update_password')) try {

			# set validation properties
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			$this->form_validation->set_rules('newpassword', 'New Password', 'trim|required');
			$this->form_validation->set_rules('confirmnewpassword', 'Confirm New Password', 'trim|required|matches[newpassword]');

			# run validation
			if (!$this->form_validation->run()) {
				throw new Exception('Validation failed');
			}

			# try logging in with the provided password
			$results = User_model::login($this->current_user->email, $this->input->post('password'));
			if (empty($results)) {
				$this->session->set_flashdata('alert_type', 'danger');
				$this->session->set_flashdata('alert_msg', 'The password you entered is incorrect.');
				redirect($this->uri->uri_string);
				exit;
			}

			# calculate new password hash
			$newpass = User_model::create_hash($this->input->post('newpassword'));
			$record = array(
				'hash' => $newpass,
				'reset' => null
			);
			User_model::add_update($record, $this->current_user->user_id);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_msg', 'You have successfully updated your password.');
			redirect($this->uri->uri_string);
		}
		catch (Exception $e) {
			$data['current_user']->first_name = $this->input->post('first_name');
			$data['current_user']->last_name = $this->input->post('last_name');
			$data['current_user']->email = $this->input->post('email');
		}

		# updating account information
		if ($this->input->post('update_account')) try {

			# set validation properties
			$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|callback_check_unique_email');

			# run validation
			if ($this->form_validation->run() == false) {
				throw new Exception('Validation failed');
			}

			$record = array(
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'email' => $this->input->post('email')
			);

			User_model::add_update($record, $this->current_user->user_id);

			$this->session->set_flashdata('alert_type', 'success');
			$this->session->set_flashdata('alert_msg', 'You have successfully updated your account information.');
			redirect($this->uri->uri_string);
		}
		catch (Exception $e) {
			$data['current_user']->first_name = $this->input->post('first_name');
			$data['current_user']->last_name = $this->input->post('last_name');
			$data['current_user']->email = $this->input->post('email');
		}

		# load view
		$this->load->view('client_header', $data);
		$this->load->view('user/user_profile', $data);
		$this->load->view('client_footer', $data);
	}

}

/* End of file user.php */
/* Location: ./application/controllers/user.php */
