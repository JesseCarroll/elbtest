<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Client extends MY_Controller {

	private $model;

	public function __construct() {
		parent::__construct();

		# Retrieve client data using either the default client, or currently selected client.
		if (($this->uri->segment(2) && is_numeric($this->uri->segment(2))) && $this->uri->segment(1) == 'client' ) {
			$active_client = $this->uri->segment(2);
		} else {
			$active_client = (isset($this->session->userdata['active_client'])) ? $this->session->userdata['active_client'] : $this->current_user->client_id;
		}
		$this->session->set_userdata('active_client', $active_client);

		# unlock inactives for editing
		$num_unlocked = Client_model::unlock_inactives($active_client);
		if ($num_unlocked > 0) {
			redirect($this->uri->uri_string, 'refresh');
		}

		# get a model for the current client
		$this->model = Client_model::get_by_id($active_client);
	}

	public function index($id = '') {
		# bounce the request back to the client list if the user does not have
		# permission to list this client's courses
		if (!empty($id) && empty($this->current_user->get_allowed_courses($id))) {
			redirect('/client');
		}

		# prepare data to feed to the view
		$data = $this->commondata;
		$data['client'] = Client_model::get_by_id($id);

		# output the page
		$this->load->view('client_header', $data);
		if ($id) {
			$this->load->view('client/client_dashboard', $data);
		} else {
			$this->load->view('client/client_list', $data);
		}
		$this->load->view('client_footer', $data);
	}

	function add_client() {

		// form field validation
		$this->form_validation->set_rules('client_name', 'Name', 'trim|required|is_unique[clients.client_name.client_id.'.$this->model->client_id.']');
		$this->form_validation->set_message('is_unique', 'Client already exists.');

		if ($this->form_validation->run() == FALSE) {
			/* TODO: Handle this with view and error feedback */
			//$this->data['client_name'] = $this->input->post('client_name', TRUE);
			redirect('/client', 'location', 303);
			return;
		}

		$client_name = $this->input->post('client_name', TRUE);

		$this->model = Client_model::insert($client_name);

		redirect('client/' . $this->model->id, 'location', 303);
	}

	function add_course() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$course_id = isset($request->data->course_id) ? $request->data->course_id : '';
		$client_id = isset($request->data->client_id) ? $request->data->client_id : $this->model->client_id;
		$course_title = $request->data->course_title;
		$module_title = isset($request->data->module) ? $request->data->module : '';
		$version = isset($request->data->version) ? $request->data->version : '';

		$version_number = ($version == 'modern') ? $this->Version_model->get_latest_version()->version_number : $version;

		// if a current_record exists, a specific course is being copied
		$current_record = ($course_id) ? Course_model::get_by_id($course_id) : '';
		$source_path = ($current_record) ? $current_record->course_folder_path() : '';

		// if a client_id exists, course is being copied to a different client
		$destination_client = ($client_id) ? $client_id : $this->model->client_id;

		$course_version = ($current_record) ? $current_record->course_version : $version_number;
		$course_enabled_languages = ($current_record) ? $current_record->course_enabled_languages : '';
		$course_curriculum = ($current_record) ? $current_record->course_curriculum : '';
		$created_by = ($current_record) ? $current_record->created_by : $this->current_user->user_id;
		$date_created = ($current_record) ? $current_record->date_created : date('Y-m-d H:i:s');

		// create database record
		$record = array(
			'client_id' => $destination_client,
			'course_version' => $course_version,
			'course_title' => $course_title . ': ' . $module_title,
			'course_enabled_languages' => $course_enabled_languages,
			'state' => 'author',
//			'course_theme' => '',
			'course_curriculum' => $course_curriculum,
			'updated_by' => $this->current_user->user_id,
			'date_created' => $date_created,
			'created_by' => $created_by,
			'locked' => 0
		);
		$course = Course_model::create($record);

		// create course folder
		$course->setup_course_folder($source_path, $destination_client, $version);

		// update course data
		$course_data = $course->load_course_json();
		$course_data['course'] = $course_title;
		$course_data['module'] = $module_title;
		$course->save_course_json($course_data);

		echo('client/' . $destination_client);
	}

	function add_curriculum() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);
		$request = $request->data;

		if($request->curriculum_id) {
			// updating existing database record
			$curriculum = $this->model->update_curriculum($request->curriculum_id, $request);
		}
		else {
			// create new database record
			$request->date_created = date('Y-m-d H:i:s');
			$request->client_id = $this->model->client_id;
			$curriculum = $this->model->create_curriculum($request);
		}

		if ($curriculum) {
			// define curriculum folder and create if necessary
			$curriculum_path = $this->model->client_folder_path('modules/' . $curriculum . '/');
			$master_path = $this->config->item('master_path', 'default');
			$variables_file = '_chameleon_variables.less';
			$destination_path = $curriculum_path . $variables_file;

			// only copy the variables file if it doesn't exist
			if (!is_file($this->data['aws_path'] . $destination_path)) {

				$client_theme = $this->model->client_folder_path($variables_file);
				$master_theme = $master_path . $variables_file;

				$curriculum_theme = (is_file($this->data['aws_path'] . $client_theme)) ? $client_theme : $master_theme;

				if ($curriculum_theme) {
					$this->aws->copy_content($curriculum_theme, $destination_path);
				}
			}
		}

		redirect('client/' . $this->model->client_id . '/', 303);
	}

	function upload(){
		if ($_FILES) {
			$this->media->ingest($this->model->client_folder_path('images/'));
		}
	}

	function update_db() {

		$postdata = file_get_contents("php://input");
		$request = json_decode($postdata);

		$request->data->date_updated = date('Y-m-d H:i:s');
		$this->model->update($request->data);

	}

	function delete() {

		if ($this->current_user->super_admin){
			// Actually delete stuff.
			$this->model->delete();
		} else {

			$record = array('client_status' => '2');

			//update data
			$this->model->update($record);
		}

		// set alert message
		$this->session->set_flashdata('alert_type', 'success');
		$this->session->set_flashdata('alert_msg', 'Record has been deleted successfully!');
	}

	function theme($client_id, $theme_id) {
		$data = $this->commondata;

		$this->load->view('client_header', $data);
		$this->load->view('theme/theme_dashboard', $data);
		$this->load->view('client_footer', $data);
	}

	function player_order($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];

		switch($method) {
			case 'GET':
				$data = $this->model->get_player_order();
				$data = json_encode($data);
				echo $data;
				break;
			case 'POST':
				$postdata = file_get_contents("php://input");
				$request = json_decode($postdata);
				$data = $this->model->update_player_order($request);
				break;
			case 'PUT':
				$postdata = file_get_contents("php://input");
				$request = json_decode($postdata);
				$data = $this->model->delete_player_order($request);
		}

	}

	function player_config($client_id) {
		$method = $_SERVER['REQUEST_METHOD'];

		switch($method) {
			case strtoupper('get'):
				$data = $this->model->get_player_config();
				if($data) {
					$data = json_encode($data);
					echo $data;
				}
				break;
			case strtoupper('post'):
				$postdata = file_get_contents("php://input");
				$request = json_decode($postdata);
				$data = $this->model->update_player_config($request);
				break;
		}

	}

	function get_data() {

		# get curriculum for client
		$curriculum = $this->model->get_curriculum();
		for ($i = 0; $i < count($curriculum); $i++) {
			$count = $this->model->get_curriculum_courses($curriculum[$i]['curriculum_id']);
			$curriculum[$i]['course_count'] = ($count) ? $count : '0';
		}

		$client_data = [
			'client' => $this->model,
			'curriculum_list' => $curriculum,
			'event_list' => $this->model->get_events(),
			'master_user_list' => User_model::get_all(),
			'user_list' => User_model::get_by_client($this->model->client_id),
			'user' => $this->current_user,
			'allowed_clients' => $this->current_user->get_allowed_clients(),
			'allowed_courses' => $this->current_user->get_allowed_courses(),
		];

		/*
		 * adding ")]}',\n" for known JSON vulnerability
		 * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
		 */
		echo ")]}',\n" . json_encode($client_data);
	}

}

/* End of file client.php */
/* Location: ./application/controllers/client.php */
