<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: origin, x-requested-with, content-type');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Snack extends CI_Controller {

    private $data = array();

    public function __construct() {
        parent::__construct();

        $this->config->load('default', TRUE);

        // load model
        $this->load->model('Snack_model');
    }

    //use submitted email address and registraction code to verify user/client and return validation token
    public function login() {

      $postdata = file_get_contents("php://input");
      $request = json_decode($postdata);

      $email = $request->email;
      $code = $request->code;

      $result = $this->Snack_model->verify_user($email, $code);

      if(!empty($result)) {
        print_r($result);
      }
    }

    public function verify_token() {

      $postdata = file_get_contents("php://input");
      $request = json_decode($postdata);

      $email = $request->email;
      $code = $request->code;
      $token = $request->token;

      $result = $this->Snack_model->verify_token($email, $code, $token);

      if(!empty($result)) {
        print_r($result);
      }
    }

    function data($client_id) {

      $client = Client_model::get_by_id($client_id);

      // get courses labelled as learning snack only
      $courses = $client->get_learning_snacks();

        /*
         * adding ")]}',\n" for known JSON vulnerability
         * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
         */
        echo ")]}',\n" . json_encode($courses);
    }

}

/* End of file snack.php */
/* Location: ./application/controllers/snack.php */