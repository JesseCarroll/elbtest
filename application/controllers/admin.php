<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends MY_Controller {

    private $data = array();

    public function __construct() {

        parent::__construct();

        // load data
        $this->data = array_merge($this->data, $this->commondata);

    }

    function index() {

        $data = $this->data;
        $this->load->view('client_header', $data);
        $this->load->view('admin/admin_dashboard', $data);
        $this->load->view('client_footer', $data);
    }

    function get_data() {
        /*
         * adding ")]}',\n" for known JSON vulnerability
         * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
         */
        echo ")]}',\n" . json_encode($this->data);
    }

}

/* End of file admin.php */
/* Location: ./application/controllers/admin.php */