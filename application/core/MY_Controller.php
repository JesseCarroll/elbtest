<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once $_SERVER['DOCUMENT_ROOT'] . '/vendor/autoload.php';

class MY_Controller extends CI_Controller {

	protected $current_user;
	protected $commondata = array();

	protected $bootstrap_pagination_config = array();

	function __construct() {
		parent::__construct();

		// loading configs here to ensure array index naming
		$this->config->load('default', TRUE);
		$this->config->load('migration', TRUE);
		$this->commondata['aws_root'] = $this->config->item('aws_root', 'default');
		$this->commondata['aws_path'] = $this->config->item('aws_path', 'default');
		$this->commondata['cloudfront_path'] = $this->config->item('cloudfront_path', 'default');

		// check for command-line access
		if ($this->input->is_cli_request()) { return; }

		// do not continue if user is not logged in
		if (!empty($this->uri->uri_string)){
			$this->_require_login();
		}

		// site defaults
		if ($this->session->userdata('test_user')) {
			$this->commondata['current_user'] = $this->current_user = User_model::get_by_id($this->session->userdata['test_user']);
		} else {
			$this->commondata['current_user'] = $this->current_user = User_model::get_by_id($this->session->userdata['user_id']);
		}
		$this->commondata['site_name'] = $this->config->item('site_name', 'default');

		// Super Admins can and should perform database maintenance
		if (!empty($this->current_user->super_admin)) {
			$this->_check_migration();
		}

		// Gravitar Image
		$this->current_user->gravitar_hash = md5( strtolower( trim( $this->current_user->email ) ) );

		// load alert data
		$this->commondata['alert_type'] = $this->session->flashdata('alert_type');
		$this->commondata['alert_msg'] = $this->session->flashdata('alert_msg');
		$this->commondata['alert'] = (empty($this->commondata['alert_type'])) ? false : true;

		$bootstrap_pagination_tag_open = "<li>";
		$bootstrap_pagination_tag_close = "</li>";

		$this->bootstrap_pagination_config = array(
			'full_tag_open'   => '<ul class="pagination">',
			'full_tag_close'  => '</ul>',
			'num_tag_open'    => $bootstrap_pagination_tag_open,
			'first_tag_open'  => $bootstrap_pagination_tag_open,
			'last_tag_open'   => $bootstrap_pagination_tag_open,
			'next_tag_open'   => $bootstrap_pagination_tag_open,
			'prev_tag_open'   => $bootstrap_pagination_tag_open,
			'num_tag_close'   => $bootstrap_pagination_tag_close,
			'first_tag_close' => $bootstrap_pagination_tag_close,
			'last_tag_close'  => $bootstrap_pagination_tag_close,
			'next_tag_close'  => $bootstrap_pagination_tag_close,
			'prev_tag_close'  => $bootstrap_pagination_tag_close,
			'cur_tag_open'    => '<li class="active"><a href="#">',
			'cur_tag_close'   => '</a></li>',
			'num_links'       => 4,
			'first_link'      => FALSE,
			'last_link'       => FALSE
			);

			date_default_timezone_set('America/Chicago');
		}

		function _require_login() {
			if ($this->session->userdata && $this->session->userdata['user_id'] == null) {
				redirect();
			}
		}

		function _set_flash($msg, $type = 'success') {
			// set alert message
			$this->session->set_flashdata('alert_type', $type);
			$this->session->set_flashdata('alert_msg', $msg);
		}

	function _check_migration() {
		$db_version = $this->db->get('migrations')->row()->version;
		$expected_version = $this->config->item('migration_version', 'migration');
		if ($db_version != $expected_version) {
			if (!in_array($this->uri->segment(1, 'dummy'), array('maintenance', 'login', 'logout'))) {
				redirect('maintenance');
			}
		}
	}

	function get_data() {
		/*
		 * adding ")]}',\n" for known JSON vulnerability
		 * http://haacked.com/archive/2008/11/20/anatomy-of-a-subtle-json-vulnerability.aspx
		 */
		echo ")]}',\n" . json_encode($this->commondata);
	}
}