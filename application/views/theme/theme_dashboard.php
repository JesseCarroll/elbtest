<div flex layout="column" ng-controller="editTheme" class="theme">
	<md-content flex layout="row">

		<md-content flex ng-switch="theme_section" class="md-padding">

			<!-- UI PAGE -->
			<md-content ng-switch-when="ui" class="md-margin global-ui">
				<div flex="50" class="header">
					<h2>Global UI</h2>
					<p>Customize the UI (user interface) of your course using a Dark or Light setting. The UI setting affects the navigation bar and secondary pages like the Loading Page, Menu, Resources, Glossary, and Language Selection.</p>
				</div>
				<div class="theme-toggle md-whiteframe-z1" layout="row" layout-align="center start" layout-padding>
					<h2>Dark UI <span class="sub break">(Default)</span></h2>
					<md-switch aria-label="UI Theme" ng-model="theme_data.uiTheme" ng-true-value="'light'" ng-false-value="'dark'"></md-switch>
					<h2>Light UI</h2>
				</div>
				<div layout="row">
					<div flex>
						<div class="preview-dark ui-slideshow" ng-class="{'active': theme_data.uiTheme == 'dark'}">
							<img src="{{aws_path}}core/images/themes/dark_loading.jpg">
							<img src="{{aws_path}}core/images/themes/dark_menu.jpg">
							<img src="{{aws_path}}core/images/themes/dark_resources.jpg">
							<img src="{{aws_path}}core/images/themes/dark_topichome.jpg">
						</div>
					</div>
					<div flex>
						<div class="preview-light ui-slideshow" ng-class="{'active': theme_data.uiTheme == 'light'}">
							<img src="{{aws_path}}core/images/themes/light_loading.jpg">
							<img src="{{aws_path}}core/images/themes/light_menu.jpg">
							<img src="{{aws_path}}core/images/themes/light_resources.jpg">
							<img src="{{aws_path}}core/images/themes/light_topichome.jpg">
						</div>
					</div>
				</div>
				<div layout="column" layout-align="start center" ng-controller="navigationCtrl as nav">
					<p>Once you’ve selected your UI, you can select colors <a class="link" ng-click="nav.editThemeSection('color')">here</a>.</p>
				</div>
			</md-content>





			<!-- COLOR PAGE -->
			<md-content ng-switch-when="color" class="md-margin color">
				<div flex="50" class="header">
					<h2>Brand Color</h2>
					<p>Select colors that will display throughout common course elements.</p>
				</div>
				<div layout="row" style="margin-bottom: 76px;">
					<div flex>
						<h2>Primary Brand Color</h2>
						<p>The color that plays a vital role in defining a brand. It may be used widely on marketing materials, but is used selectively in the course to balance the color and the content.</p>
						<p>Used in items such as:<br />Progress Indicator (percent complete), key elements of interactions.</p>
					</div>
					<div flex="40" flex-offset="10" layout="column">
						<theme-color-picker
							picker-label="Primary Brand Color"
							picker-hex="theme_data.variable.brandPrimary"
							picker-control="hue">
						</theme-color-picker>
					</div>
				</div>
				<div layout="row">
					<div flex="50">
						<h2>Secondary Brand Color</h2>
						<p>A color that is meant to contrast, compliment or accent the Primary Brand Color. Some clients do not have a defined Secondary Color, or prefer to not use them in courses.</p>
						<p>In some brand guidelines, a Secondary Color can be used as a neutral color. The editor automatically creates lighter and darker tones based on the Secondary Color. You can adjust them by individually unlocking the tones.</p>
						<p>Used in items such as:<br />Progress Indicator (percent to be completed), Start Course buttons, Video Play, edge color of Navigation Bar, underline beneath page titles.</p>
					</div>
					<div flex="40" flex-offset="10" layout="column">
						<theme-color-picker
							picker-label="Secondary Brand Color"
							picker-hex="theme_data.variable.brandSecondary"
							picker-control="hue">
						</theme-color-picker>
					</div>
				</div>
				<div layout="row">
					<div flex="50"></div>
					<div flex="40" flex-offset="10" layout="column">
						<theme-color-picker
							picker-hex="theme_data.variable.brandSecondaryLight"
							picker-label="Lighter Tone"
							secondary-hex="theme_data.variable.brandSecondary"
							adjustment="20">
						</theme-color-picker>
						<theme-color-picker
							picker-hex="theme_data.variable.brandSecondaryDark"
							picker-label="Darker Tone"
							secondary-hex="theme_data.variable.brandSecondary"
							adjustment="-20">
						</theme-color-picker>
					</div>
				</div>
			</md-content>





			<!-- TYPE PAGE -->
			<md-content ng-switch-when="type" class="type">
				<md-content layout-margin class="md-padding">
					<h2>Type</h2>
					<p>Select a preloaded typeface for your Headings and Body Text, or upload a custom font.</p>
				</md-content>

				<md-content layout="row" class="md-padding section">
					<md-content flex class="md-padding md-whiteframe-z1 gray-box" style="margin-right: 31px;">
						<h3>Current Heading Typeface<span class="sub break">bold style applies automatically</span></h3>
						<div class="font-preview header" style="font-family: {{theme_data.variable.headingTypeface}};">{{fonts[theme_data.variable.headingTypeface]}}</div>
						<select ng-model="theme_data.variable.headingTypeface" ng-options="item.key as item.name for item in ordered_fonts | orderBy:'name'"></select>
					</md-content>
					<md-content flex class="md-padding md-whiteframe-z1 gray-box">
						<h3>Current Body Text Typeface<span class="sub break">normal style applies automatically</span></h3>
						<div class="font-preview body" style="font-family: {{theme_data.variable.bodyTypeface}};">{{fonts[theme_data.variable.bodyTypeface]}}</div>
						<select ng-model="theme_data.variable.bodyTypeface" ng-options="item.key as item.name for item in ordered_fonts | orderBy:'name'"></select>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Load Custom Fonts <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_custom_fonts', $event)" aria-label="Show Custom Fonts" ng-model="toggle.show_custom_fonts"></md-switch>
					</div>
					<md-content ng-show="toggle.show_custom_fonts">
						<p flex="50" class="intro">Chameleon supports some third party services for licensed webfonts. Each vendor has a unique process for loading the font into Chameleon before they can be applied. You can use multiple vendors or use a single vendor for both fonts.</p>
						<div class="group googleFonts">
							<div layout="row" class="google" layout-align="start center">
								<img height="41" alt="Google" src="//www.gstatic.com/images/branding/googlelogo/1x/googlelogo_color_116x41dp.png"><span style="margin: 0 20px 5px 0">Fonts</span><span class="sticker free">Free</span>
							</div>
							<p flex="50">A free and popular service by Google. You just need to select a font from the <a href="https://www.google.com/fonts" target="_blank">Google Font Library Website</a> and type in its name below.</p>
							<md-autocomplete
					            md-input-name="googleFont"
					            md-no-cache="false"
					            md-selected-item-change="onSelection(item)"
					            md-search-text="searchText"
					            md-items="item in querySearch(searchText)"
					            md-item-text="item.family"
					            md-min-length="0"
					            placeholder="Enter Google Font Name">
						          <md-item-template>
						          	<span md-highlight-text="searchText" md-highlight-flags="^i">{{item.family}}</span>
						          </md-item-template>
						    </md-autocomplete>
							<md-button ng-click="addFont()" class="gray">Add Font</md-button>
						</div>
						<div class="group adobe">
							<div layout="row" class="typekit" layout-align="start center">
								<img height="59" src="https://s3.amazonaws.com/chameleonprod/core/images/typekit-logo.png">
								<span class="sticker free">Free</span>
								<span class="sticker pay">$</span>
							</div>
							<p flex="50">This is a third party subscription service provided by Adobe Systems. It is a complex and detailed process, and requires you to have an Adobe paid account or a Creative Cloud account. View <a href="https://helpx.adobe.com/typekit/using/add-fonts-website.html" target="_blank">Adobe Typekit Support</a>. <strong>Note:</strong> Please make sure to add <strong style="font-style:italic;">s3.amazonaws.com, *.chameleon-elearning.com</strong> to domains field to allow our cloud service access to load fonts.</p>
							<input name="typekit" type="text" ng-model="theme_data.customFont.typekit" placeholder="Enter Adobe Typekit Key Code"></input>
							<md-button ng-click="addTypekit()" class="gray">Add Typekit</md-button>
						</div>
						<div class="group">
							<div layout="row">
								<h3 style="margin: 0">Other Custom Fonts</h3>
							</div>
							<md-content flex="60" class="customcustom md-whiteframe-1dp" ng-repeat="custom in theme_data.customFont.custom" layout="column">
								<div layout="row" layout-align="start center" class="md-padding">
									<input flex name="customJS" type="text" ng-model="custom.src" placeholder="CSS/JS URL"></input>
									<md-button md-no-ink ng-click="deleteCustomFont($index)">x</md-button>
								</div>
								<div ng-show="custom.type" flex class="md-padding custom-row" layout="row">
									
								</div>
								<div ng-repeat="font in custom.fonts" layout="row" class="md-padding custom-row">
									<input name="customName" type="text" ng-model="font.name" placeholder="Font Name"></input>
									<input flex name="customCSS" type="text" ng-model="font.css" placeholder="CSS Stack"></input>
									<md-button md-no-ink ng-click="addCustomFontFamily(custom.fonts)">+</md-button>
									<md-button md-no-ink ng-click="deleteCustomFontFamily(custom.fonts, $index)">-</md-button>
								</div>
							</md-content>
							<md-button ng-click="addCustomFont()" class="gray">Add Other Custom Font</md-button>
							<p flex="50">If you have a custom font that is not supported by any of the above, the license and licensed font can be evaluated by our support team. </p>
							<md-button class="gray" ng-href="mailto:chameleonsupport@biworldwide.com" style="display:inline-block;">Email Support Team</md-button>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Font Sizing <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_font_sizing', $event)" aria-label="Show Font Sizing" ng-model="toggle.show_font_sizing"></md-switch>
					</div>
					<md-content ng-show="toggle.show_font_sizing">
						<p flex="50" class="intro">A master control that affects all of the text size of the theme. To make a global adjustment to all text, use this slider. <strong>View the results in the examples below.</strong></h4>
						<h4 class="center">Global Text Adjustment<span class="sub break">default size is tested and a web standard for multiple devices</span></h4>
						<md-slider-container layout="row" flex="60" flex-offset="20">
							<span class="small">A</span>
							<md-slider flex min=".8" max="1.2" step=".1" ng-model="theme_data.masterFontSizeValue" ng-change="updateAllSizes()" aria-label="Master Font Size" id="font-slider"></md-slider>
							<span class="large">A</span>
						</md-slider-container>

						<theme-font-size theme-data="theme_data.variable" text-element="'body'" text-name="type_elements['body']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<div class="spacer"></div>
						<theme-font-size theme-data="theme_data.variable" text-element="'h1'" text-name="type_elements['h1']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'h2'" text-name="type_elements['h2']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'h3'" text-name="type_elements['h3']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'h4'" text-name="type_elements['h4']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'h5'" text-name="type_elements['h5']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'h6'" text-name="type_elements['h6']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<div class="spacer"></div>
						<theme-font-size theme-data="theme_data.variable" text-element="'label'" text-name="type_elements['label']" master-size="theme_data.masterFontSizeValue"></theme-font-size>
						<theme-font-size theme-data="theme_data.variable" text-element="'caption'" text-name="type_elements['caption']" master-size="theme_data.masterFontSizeValue"></theme-font-size>

					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Type Weights, Spacing and Styling <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_font_styling', $event)" aria-label="Show Font Styling" ng-model="toggle.show_font_styling"></md-switch>
					</div>
					<md-content ng-show="toggle.show_font_styling">
						<md-content class="md-padding sample-box type-styles" layout="column" layout-align="start center">
							<select ng-model="typeElement" class="type_elements">
								<option ng-repeat="(key, value) in type_elements" value="{{key}}" ng-selected="key == typeElement">{{value}}</option>
							</select>
							<p class="sub">Select the typographic element to be adjusted</p>
							<div layout="row" flex>
								<div flex="70">
									<p class="item" style="margin-bottom: 6px;">Text Example</p>
									<div class="text-example" ng-style="getElementCss(typeElement)">
										Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus.
									</div>
								</div>

								<div flex="30" class="font-controls" layout="column">
									<label>Font Weight</label>
									<select ng-model="theme_data.variable[typeElement + '_fontWeight']">
										<option ng-repeat="item in font_weight" value="{{item}}" ng-selected="item == theme_data.variable[typeElement + '_fontWeight']">{{item}}</option>
									</select>
									<label>Text Transform</label>
									<select ng-model="theme_data.variable[typeElement + '_textTransform']">
										<option ng-repeat="item in text_transform" value="{{item}}" ng-selected="item == theme_data.variable[typeElement + '_textTransform']">{{item}}</option>
									</select>
									<label>Line Height</label>
									<theme-font-styles theme-data="theme_data.variable" text-element="typeElement" text-item="'lineHeight'"></theme-font-styles>

									<label>Letter Spacing</label>
									<theme-font-styles theme-data="theme_data.variable" text-element="typeElement" text-item="'letterSpacing'"></theme-font-styles>

								</div>
							</div>
							<div layout="column" layout-align="start center" ng-controller="navigationCtrl as nav">
								<p>Additional styling is available on specific typographic elements. View those elements <a class="link" ng-click="nav.editThemeSection('elements')">here</a></p>
							</div>
						</md-content>
					</md-content>
				</md-content>
			</md-content>





			<!-- ASSETS PAGE -->
			<md-content ng-switch-when="assets" class="assets">
				<md-content layout-margin class="md-padding">
					<div flex="50" class="header">
						<h1>Brand Assets</h1>
						<p>Items that may appear multiple times within multiple courses, that aren’t related directly to page content.</p>
					</div>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Logos</h3>
					</div>
					<md-content>
						<p flex="50" class="intro">For optimal aesthetics use logos with transparent backgrounds (PNG-24). File dimensions should be near 1280 x 720 (or with a similar aspect ratio).</p>
						<div class="logo-step" layout="column">
							<h4>Step 1 - Upload Logo</h4>
							<div layout="row">
								<div flex class="section image preview-content fileinput fileupload" layout="column" layout-align="start center" ng-repeat="slot_name in logo_slots">
									<p>{{slot_name}}</p>
									<div class="preview-content">
										<img class="logo-preview" ng-src="{{logo_previews[$index]}}">
										<md-content class="media-buttons">
											<md-button ng-if="theme_data.assets.logos[$index]" ng-click="removeLogoImage($index)" class="material-icons delete">delete</md-button>
										</md-content>
									</div>
									<button class="md-button gray" trigger-upload="uploadFile($file, 'theme_data.assets.logos[' + $index + ']')">Upload</button>
								</div>
							</div>
						</div>
						<div class="logo-step">
							<h4>Step 2 - Select Logo Location</h4>
							<div layout="row">
								<div flex class="section select" ng-repeat="(logo_id, logo_label) in logo_variables">
									<p>{{logo_label}}</p>
									<md-radio-group ng-model="theme_data.variable[logo_id]">
										<md-radio-button ng-disabled="undefined == theme_data.assets.logos[$index]" ng-repeat="item in logo_slots" value="{{getClientImagePath(theme_data.assets.logos[$index])}}" class="md-primary">{{item}}<span ng-if="$index == 0"> (default)</span></md-radio-button>
										<md-radio-button ng-checked="'' == theme_data.variable[logo_id]" value="" class="md-primary">Empty/None</md-radio-button>
									</md-radio-group>
								</div>
								<div flex class="section select"></div>
							</div>
						</div>
					</md-content>
				</md-content>

				<!--<md-content class="section">
					<div layout="row">
						<h3 flex>Icons <span>(advanced)</span><span class="beta"></span></h3>
						<md-switch ng-model="show_icons"></md-switch
					</div>
					<md-content ng-show="show_icons">
						<p flex="50" class="intro"></p>
					</md-content>
				</md-content>-->

				<md-content class="section">
					<div layout="row">
						<h3 flex>Avatars <span class="sub">(advanced)</span></h3>
						<md-switch aria-label="Show Avatars" ng-model="toggle.show_avatars"></md-switch>
					</div>
					<md-content ng-show="toggle.show_avatars">
						<p flex="50" class="intro">Select a set of avatars (icons that represent the players) if your courses will have either of the games Peril or Escape.</p>
						<md-content flex class="md-padding md-whiteframe-z1 gray-box">
							<select ng-model="theme_data.variable.avatarSet">
								<option ng-repeat="(key, value) in avatar_options" value="{{key}}"ng-selected="key == theme_data.variable.avatarSet">{{value}}</option>
							</select>
							<div layout="row" class="slideshow-container">
								<div flex="5" class="navigation left" layout="row" layout-align="center center"></div>
								<div flex class="avatar-slideshow" ng-controller="avatarSlideshow">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-01.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-03.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-02.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-04.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-05.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-06.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-07.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-08.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-09.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-10.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-11.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-12.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-13.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-14.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-15.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-16.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-17.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-18.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-19.png'}}">
									<img ng-src="{{aws_path+'core/images/avatars/'}}{{theme_data.variable.avatarSet+'/avatar-20.png'}}">
								</div>
								<div flex="5" class="navigation right" layout="row" layout-align="center center"></div>
							</div>
						</md-content>
					</md-content>
				</md-content>
			</md-content>





			<!-- ELEMENTS PAGE -->
			<div ng-switch-when="elements" class="elements">
				<md-content class="md-padding section">
					<div flex="50" class="header">
						<h1>Elements</h1>
						<p>More experienced Chameleon editors can fine tune some of the following items. Some adjustments may affect stability and legibility, so please review your course thoroughly for any conflicts.</p>
					</div>
				</md-content>
				<md-content class="section">
					<div layout="row">
						<h3 flex>Link Colors <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_link_colors', $event)" ng-model="toggle.show_link_colors" aria-label="Show Link Colors"></md-switch>
					</div>
					<md-content ng-show="toggle.show_link_colors">
						<div class="group" layout="row">
							<div flex="50">
								<h4 class="light">Hyperlink Color</h4>
								<p flex="90"><i>Used in items such as:</i> Outbound links</p>
							</div>
							<div flex="50" layout="row" class="md-padding">
								<theme-color-picker
									picker-label="Hyperlink Color"
									picker-hex="theme_data.variable.hyperlinkColor"
									picker-control="hue">
								</theme-color-picker>
								<div class="sample-box">
									<p class="item">Hyperlink Example</p>
									<p class="example">The quick brown fox jumps over <a style="color: {{'hyperlinkColor' | toHex:theme_data.variable}};">thelazydog.com</a> while chased by the even quicker yellow cheetah.</p>
								</div>
							</div>
						</div>
						<div class="group" layout="row">
							<div flex="50">
								<h4 class="light">Glossary Term Color</h4>
								<p flex="90"><i>Used in items such as:</i> Words within body text that are in the course glossary.</p>
							</div>
							<div flex="50" layout="row" class="md-padding">
								<theme-color-picker
									picker-label="Glossary Term Color"
									picker-hex="theme_data.variable.glossaryTermColor"
									picker-control="hue">
								</theme-color-picker>
								<div class="sample-box">
									<p class="item">Glossary Term Example</p>
									<p class="example">The quick brown for jumps over the lazy <a style="color: {{'glossaryTermColor' | toHex:theme_data.variable}};">kinkajou</a> while chased by the even quicker yellow cheetah.</p>
								</div>
							</div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Button Color <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_button_color', $event)" ng-model="toggle.show_button_color" aria-label="Show Button Color"></md-switch>
					</div>
					<md-content ng-show="toggle.show_button_color">
						<div class="group" layout="row">
							<div flex="50">
								<h4 class="light">Default Buttons</h4>
								<p flex="90">When <strong>there is not</strong> a preferred or suggestion selection for the course’s end user.<br><br>
								These button styles are part of the framework and cannot be adjusted.</p>
							</div>
							<div flex="50" layout="column" class="md-padding">
								<div layout="row">
									<div class="button light">BACK</div>
									<div class="button light">NEXT</div>
								</div>
								<div layout="row">
									<div class="button dark">BACK</div>
									<div class="button dark">NEXT</div>
								</div>
							</div>
						</div>
						<div class="group" layout="row">
							<div flex="50">
								<h4 class="light">Action Button Color</h4>
								<p flex="90">When <strong>there is</strong> a preferred selection or actionable item for the course’s end user.<br><br>
								<i>Used in items such as:</i> Submit buttons in pages, submit buttons in quiz pages, pop-up modals, closing tip buttons, attestation pages.</p>
							</div>
							<div flex="50" layout="row" class="md-padding">
								<theme-color-picker
									picker-label="Action Button Color"
									picker-hex="theme_data.variable.actionButtonColor"
									picker-control="hue">
								</theme-color-picker>
								<div style="padding-left:15px;">
									<p class="item">Action Button Example</p>
									<div class="button action" style="background-color: {{theme_data.variable.actionButtonColor}}; color: {{theme_data.variable.actionButtonTextColor}}">SUBMIT</div>
				          <md-checkbox ng-model="theme_data.variable.actionButtonTextColor" ng-true-value="'#4A4A4A'" ng-false-value="'#FFF'" aria-label="Action Button Dark Text">Dark Text Color</md-checkbox>
								</div>
							</div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Text Color <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_text_color', $event)" ng-model="toggle.show_text_color" aria-label="Show Text Color"></md-switch>
					</div>
					<md-content ng-show="toggle.show_text_color">
						<div class="group">
							<div layout="row">
								<div flex="50">
									<p flex="90"><i>Used in items such as:</i> General content text, intro text, quiz intro text, quiz questions, content text in interactions, labels and most every place text is used.</p>
								</div>
								<div flex="50" layout="row" class="md-padding">
									<theme-color-picker
										picker-label="Text Color"
										picker-hex="theme_data.variable.textColor"
										picker-control="hue">
									</theme-color-picker>
									<div class="sample-box">
										<p class="item">Text Color Example</p>
										<p class="example" style="color: {{'textColor' | toHex:theme_data.variable}};">The quick brown for jumps over the lazy dog. The quick brown fox jumps over the lazy dog. The quick brown for jumps over the lazy dog. The quick brown fox jumps over the lazy dog.</p>
									</div>
								</div>
							</div>
							<div layout="row" layout-align="center center" ng-controller="navigationCtrl as nav"><i>Sizing for text can be adjusted <a class="link" ng-click="nav.editThemeSection('type')">here</a>. Spacing and additional styling can be adjusted <a class="link" ng-click="nav.editThemeSection('type')">here</a>.</i></div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section neutrals">
					<div layout="row">
						<h3 flex>Neutral Colors <span class="sub">(advanced)</span></h3>
						<md-switch ng-click="showConfirm('show_neutral_colors', $event)" ng-model="toggle.show_neutral_colors" aria-label="Show Neutral Colors"></md-switch>
					</div>
					<md-content ng-show="toggle.show_neutral_colors">
						<div class="group" layout="row">
							<div flex="50">
								<p flex="90">These colors have been selected to have a large amount of contrast so text, buttons, tabs, accordions and interaction containers are clearly defined and legible. They are intentionally muted or subtle not to draw attention away from the course’s content.<br>
								<i>It’s strongly recommended these colors are adjusted only slightly, and retain a comparable quality...</i></p>
							</div>
							<div flex="50" layout="column" class="md-padding">
								<div layout="row" class="theme-wrap">
									<theme-color-picker
										class="theme-margin"
										picker-label="80% Black"
										picker-hex="theme_data.variable.neutral80"
										picker-control="hue">
									</theme-color-picker>
									<theme-color-picker
										picker-label="65% Black"
										picker-hex="theme_data.variable.neutral65"
										picker-control="hue">
									</theme-color-picker>
								</div>
								<div layout="row" class="theme-wrap">
									<theme-color-picker
										class="theme-margin"
										picker-label="55% Black"
										picker-hex="theme_data.variable.neutral55"
										picker-control="hue">
									</theme-color-picker>
									<theme-color-picker
										picker-label="40% Black"
										picker-hex="theme_data.variable.neutral40"
										picker-control="hue">
									</theme-color-picker>
								</div>
								<div layout="row" class="theme-wrap">
									<theme-color-picker
										class="theme-margin"
										picker-label="15% Black"
										picker-hex="theme_data.variable.neutral15"
										picker-control="hue">
									</theme-color-picker>
									<theme-color-picker
										picker-label="5% Black"
										picker-hex="theme_data.variable.neutral5"
										picker-control="hue">
									</theme-color-picker>
								</div>
							</div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Transparency</h3>
						<md-switch ng-click="showConfirm('show_transparency', $event)" ng-model="toggle.show_transparency" aria-label="Show Transparency"></md-switch>
					</div>
					<md-content ng-show="toggle.show_transparency">
						<div class="group" layout="row">
							<div flex="50">
								<p flex="90">Affects coursewide transparency options, such as overlays, modals, opened menu items and the control bar.</p>
							</div>
							<div flex="50" layout="column" class="md-padding">
								<md-switch ng-model="theme_data.variable.transparency" ng-true-value="'true'" ng-false-value="'false'">Transparency <span ng-show="theme_data.variable.transparency=='true'"><strong>enabled</strong></span><span ng-show="theme_data.variable.transparency=='false'"><strong>disabled</strong><span></md-switch>
							</div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Gradients</h3>
						<md-switch ng-click="showConfirm('show_gradients', $event)" ng-model="toggle.show_gradients" aria-label="Show Gradients"></md-switch>
					</div>
					<md-content ng-show="toggle.show_gradients">
						<div class="group" layout="row">
							<div flex="50">
								<p flex="90">Affects coursewide gradient options, such in the control bar.</p>
							</div>
							<div flex="50" layout="column" class="md-padding">
								<md-switch ng-model="theme_data.variable.gradients" ng-true-value="'true'" ng-false-value="'false'">Gradients <span ng-show="theme_data.variable.gradients=='true'"><strong>enabled</strong></span><span ng-show="theme_data.variable.gradients=='false'"><strong>disabled</strong><span></md-switch>
							</div>
						</div>
					</md-content>
				</md-content>

				<md-content class="section">
					<div layout="row">
						<h3 flex>Page Topic and Page Title <span class="sub">(advanced)</span><span class="beta"></span></h3>
						<md-switch ng-click="showConfirm('show_underline_color', $event)" ng-model="toggle.show_underline_color" aria-label="Show Underline Color"></md-switch>
					</div>
					<md-content ng-show="toggle.show_underline_color">
						<div class="group" layout="row">
							<div flex="50">
								<p flex="90">Affects coursewide coloring and styling of content pages' topic and page title elements.<br><br>Topic Titles default to all caps unless they are overridden. When overridden, Topic Titles will appear how they are entered.<br><br>Page titles always appear how they are entered.</p>
								<p class="item page-topic-example">Example</p>
								<div flex="70" class="sample-box clean" style="background-color:#eee;">
									<span class="topic-title" style="margin-top:0; color: {{'pageTitleTopicColor' | toHex:theme_data.variable}}; text-transform:{{theme_data.variable.topicTitleAutoCaps=='false' || theme_data.variable.topicTitleAutoCaps==undefined ? 'none' : 'uppercase'}}; visibility:{{theme_data.variable.hideTopicTitle=='true' ? 'hidden' : 'visible'}};">Topic Title</span>
									<h2 class="underline" style="color: {{'pageTitleColor' | toHex:theme_data.variable}}; border-color: {{'pageTitleUnderlineColor' | toHex:theme_data.variable}}; visibility:{{theme_data.variable.hidePageTitle=='true' ? 'hidden' : 'visible'}}; border-bottom-style:{{theme_data.variable.hidePageTitleUnderline=='true' ? 'hidden' : 'solid'}}; padding-bottom:{{theme_data.variable.hidePageTitleUnderline=='true' ? '11px' : '5px'}};">Page Title</h2>
								</div>
							</div>
							<div flex="50" layout="column" class="md-padding">
								<div layout="row" class="theme-wrap">
									<div class="theme-margin">
										<theme-color-picker
											picker-label="Topic"
											picker-hex="theme_data.variable.pageTitleTopicColor"
											picker-control="hue">
										</theme-color-picker>
										<md-checkbox ng-model="theme_data.variable.hideTopicTitle" ng-true-value="'true'" ng-false-value="'false'" aria-label="Hide Topic" class="theme-check-margin">Hide Topic</md-checkbox>
									</div>
									<div class="theme-margin">
										<theme-color-picker
											picker-label="Page Title"
											picker-hex="theme_data.variable.pageTitleColor"
											picker-control="hue">
										</theme-color-picker>
										<md-checkbox ng-model="theme_data.variable.hidePageTitle" ng-true-value="'true'" ng-false-value="'false'" aria-label="Hide Topic" class="theme-check-margin">Hide Page Title</md-checkbox>
									</div>
								</div>
								<div layout="row" class="theme-wrap">
									<div>
										<theme-color-picker
											picker-label="Underline"
											picker-hex="theme_data.variable.pageTitleUnderlineColor"
											picker-control="hue">
										</theme-color-picker>
										<md-checkbox ng-model="theme_data.variable.hidePageTitleUnderline" ng-true-value="'true'" ng-false-value="'false'" aria-label="Hide Topic" class="theme-check-margin">Hide Underline</md-checkbox>
									</div>
								</div>
								<div>
									<div class="theme-auto-cap">Topic Title Auto Capitalization</div>
									<md-switch ng-model="theme_data.variable.topicTitleAutoCaps" ng-true-value="'false'" ng-false-value="'true'" class="theme-switch-margin"><span ng-show="theme_data.variable.topicTitleAutoCaps=='false'"><strong>Off</strong></span><span ng-show="theme_data.variable.topicTitleAutoCaps=='true' || theme_data.variable.topicTitleAutoCaps==undefined"><strong>On (default)</strong></span>
								</div>
							</div>
						</div>
					</md-content>
				</md-content>
			</div>





			<div ng-switch-default class="elements">
				<md-content class="section">
					<h1>Loading Theme...</h1>
				</md-content>
			</div>
		</md-content>
	</md-content>
</div>
