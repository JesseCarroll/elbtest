	  <?php if($this->session->userdata('test_user')) { ?>
	  <md-content class="test-user-modal" ng-controller="userCtrl as admin">
		You are currently logged in as <?php echo $current_user->first_name . ' ' . $current_user->last_name; ?>. <md-button class="md-link" ng-click="admin.test_user()">Return</md-button>
	  </md-content>
	  <?php } ?>
	</div><!-- /#page-wrapper -->

	</div><!-- /#wrapper -->

	<script>
	var HW_config = {
	  selector: ".changelog", // CSS selector where to inject the badge
	  account: "Rxdwqy" // your account ID
	};
	</script>
	<script async src="//cdn.headwayapp.co/widget.js"></script>
	<script src="//ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/LoadGo/2.1/loadgo.min.js"></script>
	<script src="//dme0ih8comzn4.cloudfront.net/imaging/v3/editor.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
	<script type="text/javascript" src="//www.gstatic.com/charts/loader.js"></script>

	<script>

	var angularScripts = [
	  'js/angular/app.js?v=3.21',
	  'js/angular/directives.js?v=3.21',
	  'js/angular/directives/editor_elements.js?v=3.21',
	  'js/angular/filters.js',
	  'js/angular/services.js?v=3.21',
	  'js/angular/controllers.js?v=3.21',
	  'js/angular/sortable.js',
	  'js/angular/codemirror.js',
	  'js/angular/ui-codemirror.js',
	  'js/angular/angular-minicolors.js',
	  'js/angular/angular-ui-router.min.js'
	];

	var loadScripts = [
	  'js/bootstrap.js',
	  'js/jasny-bootstrap.js',
	  // wysiwyg text editor
	  'js/ckeditor/ckeditor.js',
	  // video media
	  'js/video.js',
	  // colorpicker
	  'js/jquery.minicolors.min.js',
	  // QR codes for preview
	  'js/jquery.qrcode.min.js',
	  // slideshow
	  'js/owl.carousel.min.js',
	  // before after media
		'js/juxtapose.min.js',
		// color format conversion utilities
		'js/tinycolor.js'
	];

	$script.ready('jquery', function() {

	  // jQuery UI
	  if (!window.jQuery.ui) {
		$script.get(['//code.jquery.com/ui/1.12.1/jquery-ui.js'], function() {
		$script.done('jquery-ui'); });
	  } else {
		// jQuery UI already loaded
		$script.done('jquery-ui');
	  }

	  // don't go chasing waterfalls
	  $script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-messages.js', function() {
			$script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js', function() {
		  	$script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-route.js', function() {
					$script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-resource.js', function() {
			  		$script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-animate.min.js', function() {
							$script.get('//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-aria.min.js', function() {
								$script.get('//unpkg.com/angular-ui-router@0.3.2/release/angular-ui-router.min.js', function() {
				  				$script.get('//ajax.googleapis.com/ajax/libs/angular_material/1.1.1/angular-material.min.js', function() {
										$script(angularScripts, function() {
					  					angular.bootstrap(document, ['chameleonEditor']);
					  					$script.done('angular');
					  				});
				  				});
								});
			  			});
						});
		  		});
				});
			});
	  });

	  $script(loadScripts, function() {
		$script('js/chameleon.js');
		$script.done('document');
	  });
	});

	</script>

	<script>
	(function(w,d,s,g,js,fs){
	  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
	  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
	  js.src='https://apis.google.com/js/platform.js';
	  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
	}(window,document,'script'));
	</script>

	</body>
</html>
