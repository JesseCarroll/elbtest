<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--link rel="icon" type="image/png" href="test/favicon-96x96.png" sizes="96x96"-->
    <!--link rel="icon" type="image/png" href="test/favicon-32x32.png" sizes="32x32"-->
    <!--link rel="icon" type="image/png" href="test/favicon-16x16.png" sizes="16x16"-->

    <title>
		<?php
      if (isset($page_title) && !empty($page_title)) {
        echo $page_title;
      } else {
        echo $site_name;
      }
    ?>
  	</title>
  <script>
    <?php readfile("js/script.min.js"); ?>;
    var base_url = "<?php echo base_url(); ?>";
    var aws_path = "<?php echo $aws_path; ?>";
    var cloudfront_path = "<?php echo $cloudfront_path; ?>";
    var current_url = "<?php echo current_url(); ?>";
    $script.path(base_url);
    window.preload = [];

  </script>

  <!-- loading jquery and angular here so ng-cloak successfully hides view while content is loading -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.js"></script>

	<!-- load file upload directive early to avoid issues related to how it modifies window.XMLHttpRequest for tracking upload progress -->
  <script src="<?php echo base_url('js/angular/ng-file-upload.min.js'); ?>"></script>

  <script src="<?php echo base_url('js/offline.min.js'); ?>"></script>
  <script>$script.done('jquery');</script>

	<!-- Add 3rd party CSS here -->
	<link rel="stylesheet" href="<?php echo base_url('css/jquery-ui.theme.min.css'); ?>" />
  <link href="<?php echo base_url('css/codemirror.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/video-js.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/editor.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/offline-theme-chrome.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/offline-language-english.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/jquery.minicolors.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/owl.carousel.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/owl.theme.default.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('css/jquery.minicolors.css'); ?>" rel="stylesheet">
	<link href="<?php echo base_url('css/juxtapose.css'); ?>" rel="stylesheet">
  <link href='//fonts.googleapis.com/css?family=PT+Sans|Arimo|Roboto:400,500' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/angular_material/1.1.1/angular-material.min.css">
  <link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.css'); ?>" />
  <link rel="stylesheet" href="<?php echo base_url('css/jasny-bootstrap.min.css'); ?>" />

  <!-- Add custom CSS here -->
  <link rel="stylesheet" href="<?php echo base_url('css/app.css'); ?>?v=3.21" />
  <link href="<?php echo base_url('css/course.css'); ?>?v=3.21" rel="stylesheet">
  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

  <script>
    $script.ready('jquery', function() {
      var run = function(){
        if (Offline.state === 'up')
          Offline.check();
      }
      setInterval(run, 5000);
    });
  </script>

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <?php
    if (ENVIRONMENT == 'production') {
        $trackurl = str_replace('/', '', str_replace('www.', '', str_replace('http://', '', base_url())));
    } ?>
</head>
