<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
    <div flex layout="row" layout-align="start center" layout-padding layout-margin>
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/admin"); ?>">Admin</a>&nbsp;>&nbsp;
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/language"); ?>">Languages</a>&nbsp;>&nbsp;
		<?php echo (empty($id))? 'Add' : 'Edit'; ?> Language
    </div>
    <div layout="row" layout-padding class="controls">
		<md-button type="submit" form="info" class="md-raised md-accent md-hue-3">Save</md-button>
		<md-button style="font-size: 14px;" class="md-primary md-raised" ng-href="<?php echo base_url("language"); ?>">Cancel</md-button>
    </div>
</md-toolbar>

<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true">
    <md-content layout="column" flex class="md-padding md-whiteframe-z1">

	    <form role="form" id="info" method="post" action="<?php echo base_url() . $section_url; ?>new">
	        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
	        <input type="hidden" id="action" name="action" value="info">

			<?php $has_error = (strlen(form_error('shortcode')) > 0)? 'has-error' : ''; ?>
			<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="shortcode">Shortcode</label>
			<div class="row">
			<div class="col-md-4">
			  <input type="text" class="form-control" id="shortcode" name="shortcode" maxlength="45" value="<?php echo set_value('shortcode', $shortcode); ?>">
			  <span class="help-block">A BCP47 language tag. <a href="http://rishida.net/utils/subtags/">Lookup tool</a></span>
			</div>
			</div>
			<?php echo form_error('shortcode', '<span class="help-block">', '</span>'); ?>
			</div>

			<?php $has_error = (strlen(form_error('description')) > 0)? 'has-error' : ''; ?>
			<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="description">Description</label>
			<div class="row">
			<div class="col-md-4">
			  <input type="text" class="form-control" id="description" name="description" maxlength="45" value="<?php echo set_value('description', $description); ?>">
			</div>
			</div>
			<?php echo form_error('description', '<span class="help-block">', '</span>'); ?>
			</div>

			<?php $has_error = (strlen(form_error('status')) > 0)? 'has-error' : ''; ?>
			<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="status">Status</label>
			<div class="row">
			<div class="col-md-4">
			  <div class="radio">
			  <label>
				<input type="radio" name="status" id="status1" value="1" <?php echo set_radio('status', '1', ($status == 1)); ?>>
				Active
			  </label>
			  </div>
			  <div class="radio">
			  <label>
				<input type="radio" name="status" id="status0" value="0" <?php echo set_radio('status', '0', ($status == 0)); ?>>
				Inactive
			  </label>
			  </div>
			  <div class="radio">
			  <label>
				<input type="radio" name="status" id="status2" value="2" <?php echo set_radio('status', '2', ($status == 2)); ?>>
				Deleted
			  </label>
			  </div>
			</div>
			</div>
			<?php echo form_error('status', '<span class="help-block">', '</span>'); ?>
			</div>

	    </form>
	</md-content>
</md-content>

<script type="text/javascript">
	$('form#info #shortcode').focus();
</script>