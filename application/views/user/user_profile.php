<div flex layout="column" id="main">
	<?php if(!empty($alert)) { ?>
	<div class="alert alert-dismissable alert-<?php echo $alert_type; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right:2px;">&times;</button>
		<?php echo $alert_msg; ?>
	</div>
	<?php } ?>

	<md-content flex class="md-padding content-main" ng-init="hideClientMenu = true">

		<md-content layout="row" layout-fill layout-margin class="md-padding md-whiteframe-z1">

		<?php if(validation_errors()) { ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="alert alert-dismissable alert-danger ?>"><?php echo validation_errors(); ?></div>
				</div>
			</div>
		<?php } ?>

		<div flex>
			<form role="update_account" method="post" action="<?php echo current_url(); ?>">
				<input type="hidden" name="update_account" value="1">
				<input type="hidden" name="id" value="<?php echo $id; ?>">

				<div class="form-group">
					<label for="first_name">First Name</label>
					<input type="text" name="first_name" class="form-control" id="first_name" placeholder="Enter your first name" value="<?php echo $current_user->first_name; ?>">
				</div>

				<div class="form-group">
					<label for="last_name">Last Name</label>
					<input type="text" name="last_name" class="form-control" id="last_name" placeholder="Enter your last name" value="<?php echo $current_user->last_name; ?>">
				</div>

				<div class="form-group">
					<label for="email">Email Address</label>
					<input type="text" name="email" class="form-control" id="email" placeholder="Enter a valid email address" value="<?php echo $current_user->email; ?>">
				</div>

				<div class="form-group">
					<label>Avatar</label>
					<div flex layout="row">
						<div><a href="//en.gravatar.com/emails/" target="new"><img aria-label="Avatar" class="md-avatar" class="md-avatar" src="//www.gravatar.com/avatar/<?php echo $current_user->gravitar_hash ?>?d=mm"></a></div>
						<div flex="5"></div>
						<div>
							<p>Your avatar is displayed at the top of the editor when you are logged in. The image used as your avatar comes from <a href="//en.gravatar.com/" target="new">Gravatar</a>, a service that associates your e-mail address with an avatar that may be used by many websites.</p>
							<p><a href="//en.gravatar.com/emails/" target="new">Manage my avatar</a></p>
						</div>
					</div>
				</div>

				<button type="submit" class="btn btn-primary">Update Account</button>
			</form>
		</div>
		<div flex="5"></div>
		<div flex>

			<form role="update_password" method="post" action="<?php echo current_url(); ?>">
				<input type="hidden" name="update_password" value="1">

				<div class="form-group">
					<label for="password">Current Password</label>
					<input type="password" name="password" class="form-control" id="password" placeholder="Enter current password">
				</div>

				<div class="form-group">
					<label for="newpassword">New Password</label>
					<input type="password" name="newpassword" class="form-control" id="newpassword" placeholder="Enter new password">
				</div>

				<div class="form-group">
					<label for="confirmnewpassword">Confirm New Password</label>
					<input type="password" name="confirmnewpassword" class="form-control" id="confirmnewpassword" placeholder="Confirm password">
				</div>

				<button type="submit" class="btn btn-primary">Update Password</button>
			</form>

		</div>
	</md-content>
</md-content>
