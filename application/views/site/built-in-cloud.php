<div class="main-content">
	<div class="page-container internal-page internal-science fixed-internal">
		<img src="../media/images/science-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li class="active"><a href="<?php echo base_url(); ?>page/built-in-cloud">built in cloud</a></li>
					<li><a href="<?php echo base_url(); ?>page/scorm-data">SCORM data</a></li>
					<li><a href="<?php echo base_url(); ?>page/technical-specs">technical specs</a></li>
				</ul>
				<h1 class="section-title">Technology</h1>
				<h3>Under the hood. Science, technology and research.</h3>
				<h4>eLearning Built in the Cloud</h4>
				<P>We build your eLearning in the cloud. This approach enables you to preview, update and approve your courses through your browser anywhere you have access to the Internet. It also allows your translators to add translations to your course from anywhere in the world. Then we deliver a final course package to you for your LMS.</p>
				<p>Security concerns? It's on Amazon Web Services. We've passed multiple security scans. We encrypt all your content. It's rock solid.</p>
				<p>But there's a more important reason Chameleon eLearning is in the cloud. Soon, every major browser in the world will automatically update itself on your device whenever the manufacturer decides to do it. You won't have the control. And with any browser update, your eLearning might not work anymore. Doesn’t matter who built it or where it resides. The risk is there for all of us.</P>
				<p>Here's where Chameleon in the cloud comes into play. Your course content is on your LMS, but we keep our core logic in the cloud. So if a browser update, an operating system update or even an IT security patch suddenly breaks your eLearning – we fix it once and every Chameleon course in the world is instantly fixed.</p>
				<p>Chameleon custom eLearning. It's secure. It's in the cloud. With today's technology – you gotta have it.</p>
			</div>
		</div>
	</div>
</div>