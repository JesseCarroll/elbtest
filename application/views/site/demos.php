<div class="main-content">
	<div class="page-container internal-page demos fixed-internal">
		<img src="../media/images/demos-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<h1 class="section-title">Demos</h1>
				<h3>Go ahead, kick the tires.</h3>
				<P>You would never buy a car before taking it for a test drive. We understand. In that spirit, we've pulled together a few demos, videos and screens that highlight the unique features of Chameleon eLearning. We feel confident that once you see how Chameleon works, you'll want us to put it to work for you.</p>
				<p>Click below to take them for a spin.</p>
			</div>
		</div>
		<div class="row samples">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<div class="row">

					<!-- <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="<?php echo base_url(); ?>demos/demo-1" target="_blank" onClick="ga('send', 'event', 'Demos', 'TEST');">
						<?php } else { ?>
							<a class="internal-sample" data-toggle="modal" data-target="#myModal" data-resource="course1">
						<?php } ?>
						<img src="../media/images/demos##.png" /><p>Welcome to Chameleon</p></a>
					</div> -->

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="https://s3.amazonaws.com/chameleonprod/clients/32/courses/697-56d5dc0338a12/prod/course.html" target="_blank" onClick="ga('send', 'event', 'Demos', '7 Reasons');">
						<?php } else { ?>
							<a class="internal-sample" data-toggle="modal" data-target="#myModal" data-resource="course3">
						<?php } ?>
						<img src="../media/images/demos01.png" /><p>7 Reasons to Recognize Your Employees</p></a>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="https://s3.amazonaws.com/chameleonprod/clients/32/courses/698-56d5e8eaedc93/prod/course.html" target="_blank" onClick="ga('send', 'event', 'Demos', 'Medical Demo');">
						<?php } else { ?>
							<a class="internal-sample" data-toggle="modal" data-target="#myModal" data-resource="course4">
						<?php } ?>
						<img src="../media/images/demos02.png" /><p>Medical Demo</p></a>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="https://s3.amazonaws.com/chameleonprod/clients/32/courses/699-56d5f465bf62f/prod/course.html" target="_blank" onClick="ga('send', 'event', 'Demos', 'Building a Culture');">
						<?php } else { ?>
							<a class="internal-sample" data-toggle="modal" data-target="#myModal" data-resource="course2">
						<?php } ?>
						<img src="../media/images/demos03.png" /><p>Building a Culture of Recognition</p></a>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>