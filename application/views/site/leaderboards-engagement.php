<div class="main-content">
	<div class="page-container internal-page internal-passion fixed-internal">
		<img src="../media/images/passion-banner03.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/user-experience">user experience</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/leaderboards-engagement">leaderboard / engage</a></li>
					<li><a href="<?php echo base_url(); ?>page/course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Passion</h1>
				<h3>One course. All devices. A new experience.</h3>
				<h4>Leaderboards or Engagement?</h4>
				<P>Don't just throw a simple game mechanic like leaderboards at your learning. That clang you just heard? That was your leaderboard bouncing off your learners and landing uselessly on the floor. Instead, design game mechanics for engagement. And make it flexible to your changing needs. We did. The Chameleon leaderboards we create in your custom eLearning use game mechanics that engage learners on four levels. Performance, speed, thoroughness and re-use.</p>
				<p>We track everything your learners touch on every page and in every question – with or without an LMS. That means our leaderboards can be configured to drive speed and performance; or thoroughness and re-use. Or any other engagement combination. We engage whatever learning behavior you need to drive business results.</p>
				<!--<img src="media/images/passion-gfx02-update.png" />-->
				<div class="callOut">
					<h4>We track everything your learners touch on every page and in every question – with or without an LMS.</h4>
				</div>
			</div>
		</div>
	</div>
</div>