<!-- Navigation -->
	<footer>
		<div class="inner-container">
			<div class="row nav">
				<ul class='main-nav'>
					<li><a href="<?php echo base_url(); ?>" class="activated"><i class="fa fa-home"></i></a></li>
					<li><a href="<?php echo base_url(); ?>page/vision">vision</a></li>
					<li><a href="<?php echo base_url(); ?>page/adaptive-design">passion</a></li>
					<li><a href="<?php echo base_url(); ?>page/built-in-cloud">technology</a></li>
					<li><a href="<?php echo base_url(); ?>page/why-global">global</a></li>
					<li><a href="<?php echo base_url(); ?>page/results">results</a></li>
					<li><a href="<?php echo base_url(); ?>page/demos">demos</a></li>
					<li class="right-nav"><a href="<?php echo base_url(); ?>login">Log In</a> | <a href="http://www.biworldwide.com" target="_blank">BI WORLDWIDE</a></li>
				</ul>
				<a class="nav-menu"><i class="fa fa-bars"></i></a>
				<a class="nav-home" href="http://www.biworldwide.com" target="_blank">BI WORLDWIDE</a>
			</div>
		</div>
	</footer>
	<script type="text/javascript" src="<?php echo base_url(); ?>js/site/theme.js"></script>
</body>
</html>