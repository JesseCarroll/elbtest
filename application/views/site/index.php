<div class="main-content">
<!-- Intro -->
	<div id="intro" class="page-container page p0 fixed clearfix">
		<div class="row header">
			<div class="inner-container">
				<div class="logo">
					<a href="http://www.biworldwide.com/en/sales-effectiveness/elearning/" target="_blank"><img src="media/images/bi-logo.png" /></a>
				</div>
				<div class="login">
					<a href="<?php echo base_url(); ?>login" class="btn">Log In</a>
				</div>
			</div>
		</div>
		<div class="intro-wrap">
			<img src="media/images/lan-A-iPad01.png" />
			<div class="row body-copy">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1>Chameleon</h1>
					<h3>Passionately Designed eLearning.</h3>
				</div>
			</div>
			<div class="page-prompt">
				<a href="#vision">
					<i class="fa fa-angle-double-down"></i>
					<p>scroll down</p>
				</a>
			</div>
		</div>
	</div>
<!-- Vision -->
	<div id="vision" class="page-container page p1 fixed clearfix">
		<img src="media/images/lan-B-bkgd01.jpg" class="mobile-img" />
		<div class="inner-container">
			<div class="row body-copy">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-7 col-md-offset-7 col-sm-offset-7">
					<h1 class="section-title">Vision</h1>
					<h3>There's no substitute for smart.</h3>
					<p>It's time to bring relevant learning to today's learners exactly how they need it.</p>
					<p>Make it interesting, make it easy, make it stick.</p><br/>
					<a href="<?php echo base_url(); ?>page/vision" class="btn">Read About Our Vision</a>
				</div>
			</div>
			<div class="page-prompt">
				<a href="#passion">
					<i class="fa fa-angle-down"></i>
				</a>
			</div>
		</div>
	</div>
<!-- Passion -->
	<div id="passion" class="page-container page p2 fixed clearfix">
		<div class="inner-container">
			<div class="row body-copy">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="section-title">Passion</h1>
					<h3>Learning isn't a game. Or is it?</h3>
					<p>To really change things, we had to look at eLearning in new ways.</p>
					<a href="<?php echo base_url(); ?>page/adaptive-design" class="btn">Read About Our Passion</a>
				</div>
			</div>
			<img src="media/images/lan-C-iPad01.png" />
		</div>
		<div class="page-prompt">
			<a href="#science">
				<i class="fa fa-angle-down"></i>
			</a>
		</div>
	</div>
<!-- Technology (formally Science) -->
	<div id="science" class="page-container page p3 fixed clearfix">
		<div class="inner-container">
			<div class="row body-copy">
				<div class="fixed-inner">
					<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
						<h1 class="section-title">Technology</h1>
						<h3>We did our homework.</h3>
						<p>There's a vast amount of science, technology and research that went into the development of Chameleon.</p>
						<a href="<?php echo base_url(); ?>page/built-in-cloud" class="btn">Read About Our Technology</a>
					</div>
				</div>
			</div>
		</div>
		<img src="media/images/lan-D-bkgd01.jpg" class="mobile-img" />
		<div class="page-prompt">
			<a href="#global">
				<i class="fa fa-angle-down"></i>
			</a>
		</div>
	</div>
<!-- Global -->
	<div id="global" class="page-container page p6 fixed clearfix">
		<img src="media/images/lan-G-bkgd01.png" class="mobile-img" />
		<div class="inner-container">
			<div class="row body-copy">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-lg-offset-7 col-md-offset-7 col-sm-offset-7">
					<h1 class="section-title">Global</h1>
					<h3>Learning that speaks to everyone.</h3>
					<p>The market is driving learning global. Your eLearning needs to keep up.</p><br/>
					<a href="<?php echo base_url(); ?>page/why-global" class="btn">Read About Our Global Perspective</a>
				</div>
			</div>
			<div class="page-prompt">
				<a href="#results">
					<i class="fa fa-angle-down"></i>
				</a>
			</div>
		</div>
	</div>
<!-- Results -->
	<div id="results" class="page-container page p4 fixed clearfix">
		<img src="media/images/lan-E-bkgd01.jpg" class="mobile-img" />
		<div class="row body-copy">
			<div class="fixed-inner">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="section-title">Results</h1>
					<h3>The only grade that counts is yours... and theirs.</h3>
					<p>There are business results and learner results. Both are connected. Both are critical.</p>
					<a href="<?php echo base_url(); ?>page/results" class="btn">Read About Our Results</a>
				</div>
			</div>
		</div>
		<div class="page-prompt">
			<a href="#demos">
				<i class="fa fa-angle-down"></i>
			</a>
		</div>
	</div>
<!-- Demos -->
	<div id="demos" class="page-container page p5 fixed clearfix">
		<div class="inner-container">
			<div class="row body-copy">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<h1 class="section-title">Demos</h1>
					<h3>Go ahead, kick the tires.</h3>
					<p>Here are a few demos that highlight some of the unique tools and features of Chameleon eLearning.</p>
					<a href="<?php echo base_url(); ?>page/demos" class="btn">Read About Our Demos</a>
				</div>
			</div>
			<img src="media/images/lan-F-devices01.png" />
		</div>
	</div>
</div>