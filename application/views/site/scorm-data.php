<div class="main-content">
	<div class="page-container internal-page internal-science fixed-internal">
		<img src="../media/images/science-banner02.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/built-in-cloud">built in cloud</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/scorm-data">SCORM data</a></li>
					<li><a href="<?php echo base_url(); ?>page/technical-specs">technical specs</a></li>
				</ul>
				<h1 class="section-title">Technology</h1>
				<h3>Under the hood. Science, technology and research.</h3>
				<h4>SCORM Data is for Noobs</h4>
				<P>OK. It's not that we're against SCORM data. We pass it to LMSs all the time. It's just that SCORM is limited. You can do better. We do. Chameleon can be configured to track every page of content, every swipe, every media element and every type of interactivity.</p>
				<p>You want to know if Jose clicked on the third tab of a tabbed interaction? Yep. Jose touched 'em all. Did Mary finish watching the video embedded behind the last accordion? She bailed with 22 seconds left. You'd like to know if everyone saw the complete timeline for your new drug? Pretty much. On average they scrolled 89% of the way down the page.</p>
				<p>Here's the kicker. We can also gate any page or any topic in your eLearning course. We can make the learner touch everything, swipe everywhere and complete every video before they can move on. HR compliance and medical regulatory in the Life Sciences will stand up and cheer.</p>
				<p>Yeah, we know that SCORM looks pretty skinny next to this kind of data. Let us help you take it up a notch.</p>
				<a href="<?php echo base_url(); ?>demos/scorm.pdf" target="_blank" onClick="ga('send', 'event', 'SCORMData', 'Scorm');"><img src="../media/images/results-gfx01.png" /><p class="label-title">Would you like to know more?</p></a>
				<div class="callOut">
					<h4>We track everything your learners touch on every page and in every question – with or without an LMS.</h4>
				</div>
			</div>
		</div>
	</div>
</div>