<div class="main-content">
	<div class="page-container internal-page results fixed-internal">
		<img src="../media/images/results-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<h1 class="section-title">Results</h1>
				<h3>Learner results and business results.</h3>
				<P>Your learning results need to feed business results. To prove it, you want results tracking – and down to the individual. We do too. So with or without an LMS, Chameleon tracks detailed learner data from every page. You get learning results that drive business results.</p>
				<p>But don't just take our word for it. Click below to see a few case studies.</p>
			</div>
		</div>
		<div class="row samples">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<div class="row">

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="<?php echo base_url(); ?>demos/resources/resource-1.pdf" target="_blank" onClick="ga('send', 'event', 'Results', 'iPad Launch');">
						<?php } else { ?>
							<a class="internal-sample" data-toggle="modal" data-target="#myModal">
						<?php } ?>
						<img src="../media/images/results-gfx01.png" /><p>Case Study:<br/>iPad Launch</p></a>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="<?php echo base_url(); ?>demos/resources/resource-2.pdf" target="_blank" onClick="ga('send', 'event', 'Results', 'Flash Conversion');">
						<?php } else { ?>
						<a class="internal-sample" data-toggle="modal" data-target="#myModal">
						<?php } ?>
						<img src="../media/images/results-gfx01.png" /><p>Case Study:<br/>Flash Conversion</p></a>
					</div>

					<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
						<?php if(!empty($has_registered)) { ?>
							<a href="<?php echo base_url(); ?>demos/resources/resource-3.pdf" target="_blank" onClick="ga('send', 'event', 'Results', 'Medical Devices');">
						<?php } else { ?>
						<a class="internal-sample" data-toggle="modal" data-target="#myModal">
						<?php } ?>
						<img src="../media/images/results-gfx01.png" /><p>Case Study:<br/>Medical Devices</p></a>
					</div>

				</div>
				<div class="callOut">
					<h4>"To me, the [chameleon] eLearning module truly represents state-of-the-art eLearning achievement, with strong roots in instructional design and focus on learning-by-doing while respecting the learner’s time. Additionally, the graphical design and the navigation through the chapters are really appealing and enjoyable across different platforms, such as legacy systems and mobile platforms."</h4>
					<p>– Philippe Potty, Medtronic Training Manager, Europe &amp; Canada</p>
				</div>
			</div>
		</div>
	</div>
</div>