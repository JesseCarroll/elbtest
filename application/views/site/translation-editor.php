<div class="main-content">
	<div class="page-container internal-page internal-passion fixed-internal">
		<img src="../media/images/passion-banner05.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/user-experience">user experience</a></li>
					<li><a href="<?php echo base_url(); ?>page/leaderboards-engagement">leaderboard / engage</a></li>
					<li><a href="<?php echo base_url(); ?>page/course-editor">course editor</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Passion</h1>
				<h3>One course. All devices. A new experience.</h3>
				<h4>Translation Editor</h4>
				<P>Translating eLearning the old way is painful.</P>
				<p>With Chameleon we make it simple and error-free. When it's done, all languages are delivered in a single course package where learners can change languages on-the-fly.</p>
				<p>Here's how it works.</p>
				<p>First the English version is created and approved using our Course Editor. Then translators are given secure access to our Translation Editor. Screen-by-screen, for each block of text content, translators see the English content on the left and a text box on the right. They simply type or copy the translated text in each empty text box and Chameleon takes care of everything else. The same easy screen-by-screen process is used for all the media, too.</p>
				<p>To shortcut the normally error-prone QA process, the translator can preview the translation exactly how the learner will see the course at any time. Oh, yeah!</p>
				<p>If it's that easy, who actually does the translations?</p>
				<p>Glad you asked.</p>
				<p>We can do it. You can do it. Your favorite translation partner can do it. It's in the cloud, so anywhere in the world your translator sits – they can translate your content. Simple and error-free. With all your languages in a single course version.</p>
				<img src="../media/images/passion-gfx03.png" />
			</div>
		</div>
	</div>
</div>