<div class="main-content">
	<div class="page-container internal-page internal-global fixed-internal">
		<img src="../media/images/global-banner02.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/why-global">why global</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/global-challenges">global challenges</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/multiple-languages">multiple languages</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Global</h1>
				<h3>Any language. Any device. One course.</h3>
				<h4>Global Challenges</h4>
				<p>At BIW we've seen a 350% increase in the need for global eLearning. The global footprint of business means the need for eLearning is growing by leaps and bounds. But the real need is to deliver eLearning in multiple languages, into multiple geographies and onto multiple device types.</p>
				<p>The downside? Most eLearning is demanding to translate, expensive to maintain and full of technical problems as you deliver to different devices. Not so with Chameleon eLearning. Chameleon delivers just what global eLearning needs. It is straightforward to translate, easy to maintain and technically correct.</p>
				<p>eLearning in any language and on any device – all in a single course version.</p>
			</div>
		</div>
	</div>
</div>