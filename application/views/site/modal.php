<!-- Modal -->
<div class="modal fade custom-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<h4>Get the Resource</h4>
				<form id="contactform" method="post" action="<?php echo current_url(); ?>">
					<input type="hidden" name="resource">
					<label>First Name:</label>
					<input required type="text" name="fname" placeholder="First Name">
					<label>Last Name:</label>
					<input required type="text" name="lname" value="" placeholder="Last Name">
					<label>Company Name:</label>
					<input type="text" name="company" value="" placeholder="Company Name">
					<label>Email Address:</label>
					<input required type="text" name="email" value="" placeholder="Email Address">
					<label>Phone:</label>
					<input type="text" name="jtitle" value="" placeholder="Phone Number">
					<textarea name="feedback" style="width: 100%" rows="4" placeholder="Can we help you with anything specific?"></textarea>
					<div class="modal-footer">
						<h4>Submitting your information allows us to reach out to you in the future.</h4>
						<input class="modal-submit" type="submit" name="submit" value="Submit" class="btn btn-primary submit-form">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<script>

/*$("form[id='contactform']").submit(function(e) {
 e.preventDefault(); // Prevent the form from submitting via the browser.
 $.ajax({
     url: $(this).attr('action'),
     type: $(this).attr('method'),
     data: $(this).serialize(),
     success: function(responseHTML) {
     	//console.log(responseHTML);
		$('#myModal').modal('hide');
     }
 });
});*/

</script>