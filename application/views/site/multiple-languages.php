<div class="main-content">
	<div class="page-container internal-page internal-global fixed-internal">
		<img src="../media/images/global-banner03.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/why-global">why global</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-challenges">global challenges</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-adaptive-design">any device eLearning</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/multiple-languages">multiple languages</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Global</h1>
				<h3>Any language. Any device. One course.</h3>
				<h4>Multiple Languages</h4>
				<p>Delivering a course in multiple languages solves a lot of problems. But it can be expensive and difficult to maintain without Chameleon eLearning.</p>
				<p>The traditional translation process is a pain. Content is exported then sent to a translator. After the translation it gets re-imported, packaged, uploaded and tested. Then the whole process is repeated multiple times. Hopefully you can catch all the errors. Hopefully.</p>
				<p>On top of that, many eLearning tools create a seperate course package for each language. 20 languages? That's 20 seperate courses. Chameleon eLearning solves both problems.</p>
				<p>First, our Translation Editor lets you complete your own translations in real-time. Previewing, QA and approval of your content is also real-time. No export / import needed. No assembly required.</p>
				<p>Second, one finished Chameleon course contains ALL the languages. That's one course to upload, review, QA and one course to maintain. No matter how many languages you need.</p>
				<p>You save time and money while increasing your eLearning reach and effectiveness.</p>
			</div>
		</div>
	</div>
</div>