<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if IE 10 ]><html class="ie ie10" lang="en"> <![endif]-->
<!--[if (gte IE 11)|!(IE)]> <!--><html class="not-ie" lang="en"> <!--<![endif]-->
<head>
	<title>Chameleon</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta charset="utf-8" />
	<meta name="viewport" content="initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,width=device-width,user-scalable=no,minimal-ui" />

	<meta property='og:title' content='Chameleon Homepage' />
	<meta property='og:image' content='<?php echo base_url(); ?>/media/images' />
	<meta property='og:description' content='Images from the Chameleon Homepage' />
	<meta property='og:url' content='<?php echo base_url(); ?>' />

	<link href='//fonts.googleapis.com/css?family=PT+Sans+Narrow:400,700' rel='stylesheet' type='text/css'>
	<link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>css/site/bootstrap-theme.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link href="<?php echo base_url(); ?>css/site/theme.css" rel="stylesheet" type="text/css">
	<script type="text/javascript" src="<?php echo base_url(); ?>js/site/vendor.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	  ga('create', 'UA-74685005-1', 'auto');
	  ga('send', 'pageview');
	</script>

</head>
<body>