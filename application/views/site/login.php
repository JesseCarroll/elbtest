<form id="login" method="post" action="<?php echo base_url(); ?>ajax/login">
	<img class="header" src="../img/chameleon-editor-text.png" />
	<div class="form-content">
		<img class="bi-logo" src="../img/BILogo_DarkText.png" />
		<div>
			<label>email</label>
			<input type="email" id="inputEmail" name="email" maxlength="255" autofocus="autofocus">
		</div>
		<div>
			<label>password</label>
			<input type="password" id="inputPassword" name="password" maxlength="20" autocomplete="off">
		</div>
		<button type="submit" class="button buttonBlue">Log In
			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
		</button>
		<div id="loginAlert" class="error" style="display: none;"></div>
	</div>
	<div class="clobber-previous-session-prompt">
		<p>A recently active login session already exists for this user.
		You may still be logged in from another location. <span class="warning">If you continue,
		you will lose all unsaved work in your existing session.</span></p>
		<p>Do you want to proceed?</p>
		<input type="button" value="Proceed" id="proceed" class="button buttonBlue"/>
		<input type="button" value="Cancel" id="cancel" class="button buttonBlue"/>
	</div>
	<div class="help-content">
		<p>Login to the editor requires a training parnership with BI Worldwide's Learning and Engagement group. Please contact info@biworldwide.com.</p>
		<p>Use Google Chrome for the most stable editing experience.</p>
		<p>An edit session will close after 60 minutes of inactivity.</p>
		<p>Save often to avoid lost data.</p>
		<button type="button" class="button buttonBlue back">Back to Login
			<div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
		</button>
	</div>
	<i class="help fa fa-question-circle" aria-hidden="true"></i>
</form>
