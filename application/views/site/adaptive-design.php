<div class="main-content">
	<div class="page-container internal-page internal-passion fixed-internal">
		<img src="../media/images/passion-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li class="active"><a href="<?php echo base_url(); ?>page/adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/user-experience">user experience</a></li>
					<li><a href="<?php echo base_url(); ?>page/leaderboards-engagement">leaderboard / engage</a></li>
					<li><a href="<?php echo base_url(); ?>page/course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Passion</h1>
				<h3>One course. All devices. A new experience.</h3>
				<h4>Any Device eLearning</h4>
				<P>From your old IE8 PC browser, to the latest tablets and smartphones. Learning goes where your people go. End of story.</p>
				<p>We make learning more interesting. The user experience engages with games, leaderboards and tons of interactivity. We make the content changeable, by you. If you can fill out an online form, you can update your own courses and add translations. You know what happens? People like it. They learn better, with up-to-date content, so they perform better. A lot better.</p>
				<img src="../media/images/passion-gfx01.png" />
				<p>Here's our challenge. When we build custom eLearning, how do we make all that work for any device? Simple responsive design says shrink it, stretch it or stack it. That doesn’t cut it when our clients include 4 of the top 10 brands in the world. Chameleon includes sound responsive design then extends to adaptive design. We pour your content into a unique layout that’s designed to play beautifully on each device. No more stretching. The content on your smartphone is designed for the smartphone. And the content on your tablet is designed for your tablet. One course. All devices. A new experience.</p>
			</div>
		</div>
	</div>
</div>