<div class="main-content">
	<div class="page-container internal-page internal-science fixed-internal">
		<img src="../media/images/science-banner03.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/built-in-cloud">built in cloud</a></li>
					<li><a href="<?php echo base_url(); ?>page/scorm-data">SCORM data</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/technical-specs">technical specs</a></li>
				</ul>
				<h1 class="section-title">Technology</h1>
				<h3>Under the hood. Science, technology and research.</h3>
				<h4>Technical Specs</h4>
				<P>Our technical development group kicked out marketing and claimed this page as their own. They want us to net it out for you quickly. Here goes.</p>
				<p>Chameleon delivers eLearning to PC, Mac, Linux (via Firefox), iPads, Android tablets, MS Surface Pro tablets, iPhones, Android phones and the Windows phones. We support the current version minus 1 for browsers and operating systems. Except for Internet Explorer which we support all the way back to IE8.</p>
				<p>All from a single course version.</p>
				<p>See the <a href="<?php echo base_url(); ?>page/adaptive-design"> Passion / Any Device eLearning</a> page for details.</p>
				<p>Disclaimer. We don't do wedge phones, Firefox v3, IE7, wacky off-brand Windows phones/tablets, or any of the eight BlackBerry Playbooks ever sold.</p>
				<p>That's it.</p>
			</div>
		</div>
	</div>
</div>