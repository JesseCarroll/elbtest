<div class="main-content">
	<div class="page-container internal-page internal-passion fixed-internal">
		<img src="../media/images/passion-banner02.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/adaptive-design">any device eLearning</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/user-experience">user experience</a></li>
					<li><a href="<?php echo base_url(); ?>page/leaderboards-engagement">leaderboard / engage</a></li>
					<li><a href="<?php echo base_url(); ?>page/course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Passion</h1>
				<h3>One course. All devices. A new experience.</h3>
				<h4>User Experience (UX)</h4>
				<P>Bersin by Deloitte released research that eLearning is moving past graphic design and into the world of UX. We thought so too. Four years ago. Since then we’ve been revamping the way eLearning is experienced by learners. It started with realizations as simple as "scrolling is no longer the kiss of death." Scrolling is now a necessity. In 2013 there were 232 unique screen resolutions used to get to web sites. There's no way to meet that need with fixed screen eLearning. When you reduce your footprint to accommodate only 80% of your traffic, you still need to serve 15 different screen resolutions.</p>
				<p>Today's learners unlock their smartphones 9 times an hour and jump online 27 times a day. In 2016, millennials are the largest generation in the US workforce. If your eLearning looks like an old Windows UX course ported to a mobile device, you'll lose 'em.</p>
				<p>The eLearning we build for you with Chameleon is designed for mobile, using mobile UX – but still made available on devices like PCs. Your eLearning is fresh, easy to use, "swipe-able" and as new as your millennials' preferences.</p>
				<div class="callOut">
					<h4>"I wish everyone here was using Chameleon eLearning. It's so clean and easy to use."</h4>
					<p>– Lead LMS Technologist, Global Auto Manufacturer</p>
				</div>
			</div>
		</div>
	</div>
</div>