<div class="main-content">
	<div class="page-container internal-page internal-passion fixed-internal">
		<img src="../media/images/passion-banner04.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li><a href="<?php echo base_url(); ?>page/why-global">why global</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-challenges">global challenges</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/multiple-languages">multiple languages</a></li>
					<li class="active"><a href="<?php echo base_url(); ?>page/global-course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title internal-global-copy">Global</h1>
				<h3>Any language. Any device. One course.</h3>
				<h4>Course Editor</h4>
				<P>Job skills for today's learners have a half-life of 2.5 – 5 years. That means a year after you approved your last eLearning script, up to 20% of the content needs to change. So you need to easily update your content. Yeah, we said, <strong>You</strong>. Once we build your eLearning course using Chameleon, <strong>You</strong> can make your own updates. If you can buy something on Amazon, you can update your course. Really.</p>
				<p>You can change the text. You can update an image. You can choose a different layout. You can switch an interaction. You can upload a video. You can add or delete a page. You can reorder your content. You can preview your results. Or we're always standing by to help with the updates if you need us.</p>
				<p>When all your changes are ready, we create a new SCORM or AICC package for your LMS.</p>
				<p>Now 100% of your content is up-to-date.</p>
				<img src="../media/images/passion-gfx03.png" />
			</div>
		</div>
	</div>
</div>