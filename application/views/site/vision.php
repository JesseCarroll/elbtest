<div class="main-content">
	<div class="page-container internal-page vision fixed-internal">
		<img src="../media/images/vision-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<h1 class="section-title">Vision</h1>
				<h3>eLearning how they need it, how they want it, how they use it.</h3>
				<P>It's time to make eLearning for today’s learner. Make it interesting, make it easy, make it stick. Chameleon from BI WORLDWIDE delivers to any device, anywhere, in any language. Have doubts? We get it. You need to learn more. Read on.</p>
				<ul class="vision-icons">
					<li><a href="<?php echo base_url(); ?>demos/sell.pdf" target="_blank" onClick="ga('send', 'event', 'Vision', 'Fact Sheet');"><img src="../media/images/vision-icon-sellsheet01.png" /><p>Fact Sheet</p></a></li>
					<li><a href="<?php echo base_url(); ?>demos/brochure.pdf" target="_blank" onClick="ga('send', 'event', 'Vision', 'Brochure');"><img src="../media/images/vision-icon-brochure01.png" /><p>Brochure</p></a></li>
					<li><a href="<?php echo base_url(); ?>demos/infographic.pdf" target="_blank" onClick="ga('send', 'event', 'Vision', 'Infographic');"><img src="../media/images/vision-icon-infographic01.png" /><p>Infographic</p></a></li>
					<li><a href="<?php echo base_url(); ?>demos/fiveTrends.pdf" target="_blank" onClick="ga('send', 'event', 'Vision', 'Five Trends');"><img src="../media/images/vision-icon-trends01.png" /><p>Five eLearning Trends</p></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>