<div class="main-content">
	<div class="page-container internal-page internal-global fixed-internal">
		<img src="../media/images/global-banner01.jpg" />
		<div class="row body-copy">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
				<ul class="sub-nav">
					<li class="active"><a href="<?php echo base_url(); ?>page/why-global">why global</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-challenges">global challenges</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-adaptive-design">any device eLearning</a></li>
					<li><a href="<?php echo base_url(); ?>page/multiple-languages">multiple languages</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-course-editor">course editor</a></li>
					<li><a href="<?php echo base_url(); ?>page/global-translation-editor">translation editor</a></li>
				</ul>
				<h1 class="section-title">Global</h1>
				<h3>Any language. Any device. One course.</h3>
				<h4>Why Global?</h4>
				<p>We are smack in the middle of a 20-year surge in global business. New markets are growing 75% faster than in developed countries. Another 10 years and nearly half of the world's consumption will come from these emerging markets.</p>
				<p>Good learning is a 'gotta have' for global companies to grow – and to manage the growth. Because the scale is so large and the geographies so diverse, traditional learning is way too expensive. Instead, eLearning has the potential to be a scalable, cost-effective solution.</p>
				<p>Our clients have observed these drivers of global eLearning:</p>
				<ol>
					<li>Expansion requires good training to be effective.</li>
					<li>Compliance needs to account for learners all over the world.</li>
					<li>Role-based training is now global as roles are standardized internationally.</li>
					<li>Channels are global; even if the corporate footprint is not.</li>
				</ol>
				<p>Sound familiar?</p>
				<p>That's why we've developed Chameleon to be the premier global eLearning development solution.</p>
				<a href="<?php echo base_url(); ?>demos/global-eLearning.pdf" target="_blank" onClick="ga('send', 'event', 'Global', '7 Musts');"><img src="../media/images/results-gfx01.png" /><p class="label-title">Would you like to know more?</p></a>
				<div class="callOut">
					<h4>"... You've solved the global eLearning issues we've been wrestling with..."</h4>
					<p>– Director of Global Training, Global Medical Device Manufacturer</p>
				</div>
			</div>
		</div>
	</div>
</div>