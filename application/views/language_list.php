<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
    <div flex layout="row" layout-align="start center" layout-padding layout-margin>Languages</div>
    <div layout="row" layout-padding class="controls">
        <md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo base_url("$section_name/new"); ?>" style="font-size: 14px; text-decoration: none;">
            Add New
        </md-button>
    </div>
</md-toolbar>

<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true">
    <md-content layout="column" flex class="md-padding md-whiteframe-z1">

        <table class="table table-hover table-bordered">
            <thead>
                <tr>
    			<th>Shortcode</th>
                <th>Description</th>
                <th style="width:70px;">Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($records as $record) {
                ?>
                <tr>
    				<td><?php echo $record->language_shortcode; ?></td>
                    <td><?php echo $record->language_description; ?></td>
                    <td>
                        <div class="btn-group pull-right">
                            <button class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">
                            <?php echo ($record->language_status == 1)? 'Active' : (($record->language_status == 2)? 'Deleted' : 'Inactive'); ?>
                            <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo base_url() . $section_url; ?><?php echo $record->language_id; ?>/edit">Manage Language</a></li>
    							<?php if($record->language_status != 2) { ?>
                                <li><a href="javascript:deleterecord('<?php echo $record->language_id; ?>')">Delete Language</a></li>
    							<?php } ?>
                            </ul>
                        </div>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
        </table>

        <div class="pull-right"><?php echo $pagination; ?></div>

        <form id="record" method="post" action="">
            <input type="hidden" id="id" name="id" value="0">
        </form>
    </md-content>
</md-content>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Delete language</h4>
	  </div>
	  <div class="modal-body">

		<p>Selected language will be permanently deleted and cannot be recovered. Are you sure?</p>

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary" id="dialog-confirm">Save changes</button>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        $( "#dialog-confirm" ).click(function() {
			 $('form#record').submit();
		});
    });

    function deleterecord(id)
    {
    	var url = '<?php echo base_url(); ?>' + 'language/' + id + '/delete';
        $('form#record').attr('action',url);
        $('#myModal').modal('show');
    }
</script>