<div class="row">
	<div class="col-lg-12">
		<?php	if($alert) { ?>
			<div class="alert alert-dismissable alert-<?php echo $alert_type; ?>">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<?php echo $alert_msg; ?>
			</div>
		<?php	}	?>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<h1>Maintenance Panel</h1>

		<ul>
			<li>
				Database Version: <?php echo $db_version; ?>
				<?php if ($db_version != $configured_db_version) { ?>
					<a href="<?php echo base_url(array('maintenance', 'act?action=db_upgrade')); ?>">Upgrade to version <?php echo $configured_db_version; ?></a>
				<?php } ?>
			</li>
		</ul>
	</div>
</div>