<script type="text/ng-template" id="clientPlayer.html">

<md-content flex layout="row" layout-fill class="md-whiteframe-z2 courseware-player" ng-controller="PlayerCtrl">
  <md-sidenav class="md-sidenav-left" md-component-id="player" md-is-locked-open="true" md-whiteframe="4">
  	<ul>
				<li><button ng-class="{'active' : page == 'config'}" ng-click="page = 'config'" class="md-button md-default-theme">App Configuration</button></li>
				<li><button ng-class="{'active' : page == 'order'}" ng-click="page = 'order'" class="md-button md-default-theme">Course Ordering</button></li>
				<li><button ng-class="{'active' : page == 'users'}" ng-click="page = 'users'" class="md-button md-default-theme">User Upload</button></li>
  	</ul>
  </md-sidenav>
  <md-content flex class="md-padding md-margin" ng-switch="page">
  	<div ng-switch-when="config">
			<div layout="row">
				<md-button class="md-raised md-primary" ng-click="saveConfig($event, player)">Save</md-button>
			</div>
	  	<add-text ng-model="player.title" label="Application Title" class="app-title"></add-text>
	  	<label>Application Logo</label>
			<div class="preview-content" flex="50">
				<div layout="column" class="media-buttons md-whiteframe-z1">
					<i class="fa fa-image" trigger-upload="uploadFile($file, 'player.logo')"></i>
					<i class="fa fa-trash-o" ng-show="player.logo" ng-click="removeImage(0)"></i>
				</div>
				<img ng-if="player.logo" style="width: 100%;" ng-src="{{getMediaUrl()}}"></img>
			</div>

	  	<add-colorpicker ng-model="player.color" label="Accent Color"></add-colorpicker>
			<md-input-container>
			  <label>Course Expiration</label>
			  <input type="number" min="0" ng-model="player.expiration" aria-label="Course Expiration">
			</md-input-container>
			<add-text ng-model="client.invite_code" label="Invite Code"></add-text>
			<add-text ng-model="client.endpoint" label="Endpoint"></add-text>
			<add-text ng-model="client.username" label="Username"></add-text>
			<add-text ng-model="client.password" label="Password"></add-text>
		</div>
		<md-content ng-switch-when="order" layout="column">
			<div layout="row">
				<md-button class="md-raised md-primary" ng-click="saveOrder()">Save</md-button>
			</div>
			<p>Use this page to modify the order courses will appear in the Courseware Player.</p>
			<table class="material course-order">
				<thead>
					<tr>
						<th>Course Name</th>
						<th>Curriculum</th>
						<th>Order</th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="item in player_order | orderBy: ['!course_order', 'course_order']">
						<td>{{item.course_title}}</td>
						<td>{{item.curriculum_name}}</td>
						<td><input type="text" ng-model="item.course_order" aria-label="Course Order"></td>
					</tr>
				</tbody>
			</table>
		</md-content>
  	<md-content ng-switch-when="users" style="overflow:visible" class="user-upload">
  		<label>First select a curriculum that contains the courses you want your users to see.</label>
			<md-input-container ng-if="curriculum_list.length">
				<!--<label>Select Curriculum</label>-->
				<add-select ng-model="users.curriculum" data-label="Select Curriculum" select-list="curriculum_list"></add-select>
			</md-input-container>
			<label>Then enter or paste a comma separated list of the email addresses you want assigned to your selected curriculum.</label>
			<textarea ng-model="users.users" rows="10" style="width: 100%"></textarea>
			<div layout="row">
				<md-button class="md-raised md-primary" ng-click="importUsers($event, users)">Save</md-button>
			</div>
  	</md-content>
  </md-content>
</md-content>

</script>
