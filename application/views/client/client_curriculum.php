<script type="text/ng-template" id="clientCurriculum.html">
<div ng-controller="curriculumCtrl as list">
	<md-content layout="row" layout-align="start center">
    <?php if($current_user->can('add_curriculum', $client->client_id)) { ?>
		  <md-button class="md-raised md-primary" ng-click="list.curriculumDetail($event)">Add New Curriculum</md-button>
    <?php } ?>
		<div flex layout="row" layout-align="end center"><input type="text" ng-model="q" placeholder="Search Curriculum"></div>
	</md-content>

  <table class="table table-bordered table-hover">
    <thead>
      <tr style="cursor: pointer;">
        <th ng-click="this.predicate = 'curriculum_name'; list.reverse = !list.reverse">Name</th>
        <th ng-click="this.predicate = 'curriculum_count'; list.reverse = !list.reverse" class="col-md-1 center">Children</th>
        <th ng-click="this.predicate = 'curriculum_id'; list.reverse = !list.reverse" class="col-md-2 center">Date Created</th>
  			<th ng-click="this.predicate = 'curriculum_status'; list.reverse = !list.reverse" class="col-md-2 center">Status</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="item in curriculum_list | orderBy:list.predicate:list.reverse | filter:q" style="cursor: pointer;" ng-click="list.curriculumDetail($event, item)">
      	<td>{{item.curriculum_name}}</td>
      	<td class="center">{{item.course_count}}</td>
        <td class="center">{{item.date_created | asDate | date:'mediumDate'}}</td>
        <td class="center">{{curriculum_status[item.curriculum_status]}}</td>
      </tr>
		</tbody>
	</table>
</div>




	<script>
	    $('#addCurriculum').on('show.bs.modal', function (event) {
	        var button = $(event.relatedTarget) // Button that triggered the modal
	        var data = button.data('curriculum') // Extract info from data-* attributes

	        if(data !== '') {
	            var modal = $(this);
	            modal.find('.curriculum_id').val(data.curriculum_id);
	            modal.find('.curriculum_name').val(data.curriculum_name);
	            modal.find('.curriculum_status').css('display', 'block');
	            //modal.find('.curriculum_status input[value=' + data.curriculum_status + ']').selected();
	        }
	    })
	</script>

</script>
