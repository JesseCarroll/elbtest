<md-content flex layout="row" layout-wrap id="client-list" class="md-padding content-main md-hue-1" ng-controller="clientListCtrl">
  <div ng-if="record.client_name" ng-repeat="record in allowed_clients | orderBy:'client_name'" class="client-card">
    <md-card ng-click="goToClient(record.client_id)">
      <div flex layout="row" ng-style="{'background-color': record.client_primary_color}">
        <div flex ng-if="record.client_logo" class="logo" style="background-image:url('<?php echo $aws_path; ?>clients/{{record.client_id}}/images/{{record.client_logo}}');"></div>
      </div>
    </md-card>
    <p>{{record.client_name}}</p>
  </div>
  <?php if($current_user->can('add_client')) { ?>
    <div class="client-card add">
      <md-content style="height: 128px; margin:8px;" layout layout-align="center center">
        <div>
          <md-button class="md-fab md-primary" aria-label="Add Client" data-toggle="modal" data-target="#addClient"><i class="fa fa-plus"></i></md-button>
        </div>
      </md-content>
      <p>&nbsp</p>
    </div>
  <?php } ?>
</md-content>

<div class="modal fade" id="addClient">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Add Client</h4>
      </div>
      <div class="modal-body">
          <form role="form" id="add_client" method="post" action="<?php echo current_url() . '/add_client'; ?>" enctype="multipart/form-data">
              <input type="hidden" class="action" name="action" value="add_client">

          <?php $has_error = (strlen(form_error('client_name')) > 0)? 'has-error' : ''; ?>
          <div class="form-group <?php echo $has_error; ?>">
            <label class="control-label" for="client_name">Client Name</label>
              <input type="text" class="form-control client_name" name="client_name" maxlength="45" value="<?php echo (isset($client_name)) ? $client_name : ''; ?>">
          </div>
          <?php echo form_error('client_name', '<p class="text-danger">', '</p>'); ?>

        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" form="add_client" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script>

  var error = '<?php echo strlen(validation_errors()) > 0; ?>';

  $script.ready("base", function(){

    if(error) {
      $('#addClient').modal('show');
    };

  });

</script>