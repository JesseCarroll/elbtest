<script type="text/ng-template" id="clientSettings.html">

<form id="clientTheme" enctype="multipart/form-data">
	<md-content layout="row" layout-wrap layout-sm="column" layout-margin>

		<p flex="100"><strong>Last Updated:</strong> {{client.date_updated | date : 'MMMM d, yyyy @ h:mm a' }}</p>

		<div flex="20">
			<label>Client Logo</label>
			<div class="preview-content">
				<img ng-if="client.client_logo" style="width: 100%;" ng-src="{{aws_path}}clients/{{client.client_id}}/images/{{client.client_logo}}">
				<md-content ng-if="!disableFields" layout="column" class="media-buttons md-whiteframe-z1">
					<button class="md-button material-icons add" trigger-upload="uploadFile($file, 'client.client_logo')">image</button>
					<md-button ng-show="client.client_logo" ng-click="removeObjectItem(client, 'client_logo')" class="material-icons delete">delete</md-button>
				</md-content>
			</div>
		</div>

		<div flex="50" layout="column">
			<md-input-container flex>
				<label>Client Name</label>
				<input ng-model="client.client_name">
			</md-input-container>

			<button type="button" ng-click="saveTheme()" class="btn btn-primary">Save</button>
		</div>
	</md-content>
</form>
</script>
