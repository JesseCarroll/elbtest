<?php
	include 'client_courses.php';
	include 'client_users.php';
	include 'client_curriculum.php';
	include 'client_events.php';
	include 'client_badges.php';
	include 'client_settings.php';
	include 'client_theme.php';
	include 'client_player.php';
?>

<md-content flex class="content-main md-hue-1">
	<?php if(!empty($alert)) { ?>
		<div class="alert alert-dismissable alert-<?php echo $alert_type; ?>">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<?php echo $alert_msg; ?>
		</div>
	<?php } ?>

	<div ng-controller="clientCtrl" layout-fill>
		<ng-switch on="editor.client.section">
			<md-content flex ng-switch-when="courses" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div flex ng-include="'clientCourses.html'"></div>
			</md-content>

			<md-content ng-switch-when="users" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div ng-include="'clientUsers.html'"></div>
			</md-content>

			<md-content ng-switch-when="curriculum" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div ng-include="'clientCurriculum.html'"></div>
			</md-content>

			<md-content ng-switch-when="events" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div ng-include="'clientEvents.html'"></div>
			</md-content>

			<md-content ng-switch-when="badges" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div ng-include="'clientBadges.html'"></div>
			</md-content>

			<md-content ng-switch-when="theme" layout="column" layout-fill ng-controller="clientTheme">
				<div layout="row" layout-fill class="theme" ng-include="'clientTheme.html'"></div>
			</md-content>

			<md-content ng-switch-when="settings" layout="column" layout-fill layout-margin class="md-padding md-whiteframe-z1">
				<div ng-include="'clientSettings.html'"></div>
			</md-content>

			<md-content ng-switch-when="player" layout="column" layout-fill>
				<div flex ng-include="'clientPlayer.html'"></div>
			</md-content>

			<md-content ng-switch-when="analytics" class="analytics" layout="column" layout-fill ng-controller="analyticsCtrl">
				<div class="header md-whiteframe-z2" layout="row">
					<h2>Usage Report</h2>
					<md-input-container flex style="margin-left: 100px;">
						<label>Course:</label>
						<md-select ng-model="courseAnalytics">
							<md-option ng-value="0"><em>All Courses</em></md-option>
							<md-option ng-if="item.client_id === client.client_id" ng-repeat="item in allowed_courses" ng-value="item.course_id">
								{{item.course_title}}
							</md-option>
						</md-select>
					</md-input-container>
					<md-input-container>
						<label>Start Date:</label>
						<md-datepicker ng-model="startDate" md-placeholder="Start Date"></md-datepicker>
					</md-input-container>
					<md-input-container>
						<label>End Date:</label>
						<md-datepicker ng-model="endDate" md-placeholder="End Date"></md-datepicker>
					</md-input-container>
				</div>

				<div ng-if="!loaded">
					<md-progress-linear md-mode="indeterminate"></md-progress-linear>
				</div>

				<div ng-show="loaded" class="main-data">
					<div class="md-padding stat-group" layout="row">
						<div flex>
							<h3>Course Usage</h3>
							<div layout="row">
								<div class="stat" layout="column">
									{{summary.users}}
									<label>Users</label>
								</div>
								<div class="stat" layout="column">
									{{summary.sessions}}
									<label>Sessions</label>
								</div>
							</div>
						</div>
						<div flex>
							<h3>Average Time</h3>
							<div layout="row">
								<div class="stat" layout="column">
									{{summary.averageTimeOnPage | analyticsTimeFormat}}
									<label>In Course</label>
								</div>
							</div>
						</div>
					</div>
				
					<div class="dashboard-group">
						<md-card>
							<md-card-content>
								<h3>Course Activity</h3>
								<canvas id="courseActivity" height="75"></canvas>
							</md-card-content>
						</md-card>
					</div>

					<div class="dashboard-group" layout="row">
						<md-card flex>
							<md-card-content>
								<div layout="row header">
									<div layout="column" flex>
										<h3>Top Countries</h3>
										<small>by sessions</small>
									</div>
									<md-button ng-click="countriesDisplay = 'map'" ng-class="{'md-raised': countriesDisplay == 'map'}">Map</md-button>
									<md-button ng-click="countriesDisplay = 'chart'" ng-class="{'md-raised': countriesDisplay == 'chart'}">Chart</md-button>
								</div>

								<div ng-show="countriesDisplay == 'map'" id="regions_div" style="height: 300px;"></div>
								<div ng-show="countriesDisplay == 'chart'" class="chartContainer">
									<canvas id="topCountries" height="128"></canvas>
								</div>
							</md-card-content>
						</md-card>
						<md-card flex>
							<md-card-content>
								<div layout="row" class="header">
									<div layout="column" flex>
										<h3>Technology</h3>
										<small>by sessions</small>
									</div>
									<md-button ng-click="tableDisplay = 'browser'" ng-class="{'md-raised': tableDisplay == 'browser'}">Browsers</md-button>
									<md-button ng-click="tableDisplay = 'devices'" ng-class="{'md-raised': tableDisplay == 'devices'}">Devices</md-button>
									<md-button ng-click="tableDisplay = 'os'" ng-class="{'md-raised': tableDisplay == 'os'}">Operating Systems</md-button>
								</div>
								<div ng-show="tableDisplay == 'browser'" layout="column" class="table">
									<div layout="row" class="table-row header">
										<div flex="70">Browser</div>
										<div flex="30">Sessions</div>
									</div>
									<div layout="row" class="table-row" ng-repeat="item in browsers" ng-class-odd="'odd'" ng-if="percent(item.sessions) !== 0">
										<div flex="70">{{item.browser}}</div>
										<div flex="30">{{item.sessions}} ({{percent(item.sessions)}}%)</div>
									</div>
								</div>
								<div ng-show="tableDisplay == 'devices'" layout="column" class="table">
									<div layout="row" class="table-row header">
										<div flex="70">Device</div>
										<div flex="30">Sessions</div>
									</div>
									<div layout="row" class="table-row" ng-repeat="item in device" ng-class-odd="'odd'" ng-if="percent(item.sessions) !== 0">
										<div flex="70">{{item.device}}</div>
										<div flex="30">{{item.sessions}} ({{percent(item.sessions)}}%)</div>
									</div>
								</div>
								<div ng-show="tableDisplay == 'os'" layout="column" class="table">
									<div layout="row" class="table-row header">
										<div flex="70">Operating System</div>
										<div flex="30">Sessions</div>
									</div>
									<div layout="row" class="table-row" ng-repeat="item in os" ng-class-odd="'odd'" ng-if="percent(item.sessions) !== 0">
										<div flex="70">{{item.os}}</div>
										<div flex="30">{{item.sessions}} ({{percent(item.sessions)}}%)</div>
									</div>
								</div>
							</md-card-content>
						</md-card>
					</div>
				</div>
			</md-content>
		</ng-switch>
	</div>
</md-content>