<script type="text/ng-template" id="clientCourses.html">
<div layout="column" ng-controller="courseListCtrl as list">
	<md-input-container flex>
		<label>Search Courses:</label>
		<md-icon class="material-icons" aria-label="search">search</md-icon>
		<input ng-model="q" aria-label="search">
	</md-input-container>

  <!--md-list flex>
	<md-list-item class="md-3-line" ng-repeat="course in allowed_courses | orderBy:list.predicate:list.reverse | filter:q" ng-click="null">
		<div class="md-list-item-text" layout="column">
			<div>
			<md-icon ng-if="course.private == 1" class="pull-left material-icons" style="font-size: 12px; line-height: 18px;">
				lock
				<md-tooltip md-direction="left">Course is Private</md-tooltip>
			</md-icon>
				<h3>{{course.course_title}}</h3>
			</div>
			<p>Updated: {{course.date_updated | asDate | date:'mediumDate'}}</p>
			<p ng-if="course.curriculum_name !== 'None'">Curriculum: {{course.curriculum_name}}</p>
		</div>
		<md-button class="md-icon-button material-icons" ng-click="showPreview($event, course)" title="Preview">search</md-button>
	  <?php if($current_user->can('add_client')) { ?>
		  <md-button class="md-icon-button material-icons" ng-click="list.newCourse($event, course)" title="Copy">content_copy</md-button>
	  <?php } ?>
		<md-divider></md-divider>
	</md-list-item>
  </md-list-->


	<table class="table table-bordered table-hover">
		<thead>
			<tr style="cursor: pointer;">
				<th ng-click="list.predicate = 'course_title'; list.reverse = !list.reverse">Title</th>
				<th ng-click="list.predicate = 'curriculum_name'; list.reverse = !list.reverse" class="col-md-2 center">Curriculum</th>
				<th ng-click="list.predicate = 'date_updated'; list.reverse = !list.reverse" class="col-md-2 center">Date Updated</th>
				<th ng-click="list.predicate = 'state'; list.reverse = !list.reverse" class="col-md-2 center">Course State</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<tr ng-if="course.client_id == client.client_id" ng-repeat="course in allowed_courses | orderBy:list.predicate:list.reverse | filter:q" style="cursor: pointer;" ng-class="{'danger': course.being_edited == '1'}" ng-click="goToCourse(course)">
				<td>
					<md-icon ng-if="course.private == 1" class="pull-left material-icons" style="font-size: 12px; line-height: 18px;">
						lock
						<md-tooltip md-direction="left">Course is Private</md-tooltip>
					</md-icon>
					<p class="pull-left" style="margin:0;">{{course.course_title}}<span ng-if="course.being_edited == '1'" class="alert-danger"> - being edited by {{course.first_name}} {{course.last_name[0]}}</span></p>
					<i class="fa fa-question-circle pull-right course-note-button" ng-class="{'notes-active': course.course_notes}" ng-click="openNotes(course);$event.stopPropagation()"></i>
					<div ng-if="course.course_version[0] < 3" class="well bg-primary well-sm pull-right version" title="Course Version">{{course.course_version}}</div>
				</td>
				<td class="center">{{course.curriculum_name}}</td>
				<td class="center">{{course.date_updated | asDate | date:'mediumDate'}}</td>
				<td ng-if="course.locked==0" class="center">{{state(course)}}</td>
				<td ng-if="course.locked!=0" class="center">Locked</td>
				<td class="center" style="padding: 0;">
					<div ng-hide="loading[course.course_id]">
						<md-button class="md-icon-button material-icons" ng-click="$event.stopPropagation();showPreview($event, course)" title="Preview">search</md-button>
						<?php if($current_user->can('add_course', $client->client_id)) { ?>
							<md-button class="md-icon-button material-icons" ng-click="$event.stopPropagation();newCourse($event, course)" title="Copy">content_copy</md-button>
						<?php } ?>
						<md-button class="md-icon-button material-icons" ng-click="$event.stopPropagation();exportCourse($event, course)" title="Export">picture_as_pdf</md-button>
					</div>
					<md-progress-linear style="margin-top: 13px;" ng-show="loading[course.course_id]" md-mode="indeterminate"></md-progress-linear>
				</td>
			</tr>
		</tbody>
	</table>
</div>

<div class="modal fade" id="add-course">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Add Course</h4>
			</div>
			<div class="modal-body">
				<form role="form" id="add_course" method="post" action="<?php echo current_url() . '/add_course'; ?>" enctype="multipart/form-data">
					<input type="hidden" class="course_id" name="course_id" value="">
					<input type="hidden" class="action" name="action" value="add_course">

					<?php $has_error = (strlen(form_error('course_title')) > 0)? 'has-error' : ''; ?>
					<div class="form-group <?php echo $has_error; ?>">
						<label class="control-label" for="course_title">Course Title</label>
						<input type="text" class="form-control course_title" name="course_title" maxlength="45" value="">
					</div>
					<?php echo form_error('course_title', '<span class="help-block">', '</span>'); ?>

					<?php $has_error = (strlen(form_error('module_title')) > 0)? 'has-error' : ''; ?>
					<div class="form-group module_field <?php echo $has_error; ?>">
						<label class="control-label" for="module_title">Module Title</label>
						<input type="text" class="form-control module_title" name="module_title" maxlength="45" value="">
					</div>
					<?php echo form_error('module_title', '<span class="help-block">', '</span>'); ?>

					<div class="client_id">
						<label class="control-label" for="client_id">Copy to Client</label>
						<select class="form-control" name="client_id">
							<option value=''>Select a Client</option>
							<option ng-repeat="client in allowed_clients" value="{{client.client_id}}">{{client.client_name}}</option>
						</select>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" form="add_course" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>

<script>
$('#add-course').on('show.bs.modal', function (event) {
	var button = $(event.relatedTarget) // Button that triggered the modal
	var data = button.data('course') // Extract info from data-* attributes
	var modal = $(this);

	if(data) {
		var title = data.course_title.split(':');

		modal.find('.course_title').val(title[0].trim());
		modal.find('.module_title').val(title[1].trim() + ' - (copy)');
		modal.find('.course_id').val(data.course_id);
		modal.find('.client_id').css('display', 'block');
		modal.find('.import').css('display', 'none');
	}
	else {
		modal.find('.course_title').val('');
		modal.find('.module_title').val('');
		modal.find('.course_id').val('');
		modal.find('.client_id').css('display', 'none');
		modal.find('.import').css('display', 'block');
	}
});
</script>