<script type="text/ng-template" id="clientTheme.html">
	<md-content flex layout="column" class="md-whiteframe-z2 md-padding md-margin">

	<div layout="row" style="flex-shrink:0;">
		<md-button class="md-raised md-primary" ng-click="addTheme($event)" style="height: 40px; width: 120px;">Add Theme</md-button>
		<md-input-container>
			<label>Search Themes:</label>
			<md-icon class="material-icons" aria-label="search">search</md-icon>
			<input ng-model="q" aria-label="search">
		</md-input-container>
	</div>

	<table class="table table-bordered table-hover" ng-if="theme.list">
		<thead>
			<tr style="cursor: pointer;">
				<th ng-click="list.predicate = 'theme_title'; list.reverse = !list.reverse">Title</th>
				<th style="width: 80px; text-align: center;">Copy</th>
				<th style="width: 200px; text-align: center;">Default</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="item in theme.list | orderBy:list.predicate:list.reverse | filter:q" style="cursor: pointer;">
				<td ng-click="editTheme(item)" style="padding: 14px;text-transform:uppercase;">{{item}}</td>
				<td class="center" style="padding: 0;">
					<md-button class="md-icon-button material-icons" ng-click="addTheme($event, item)" title="Copy">content_copy</md-button>
				</td>
				<td class="center" style="padding: 0;">
					<p class="default" ng-if="item == theme.default_theme" style="padding: 14px; margin: 0;" ng-cloak><i>Default Theme</i></p>
					<md-button class="md-accent md-raised md-hue-1" ng-if="item !== theme.default_theme" ng-click="makeDefault(item)">Make Default</md-button>
				</td>
			</tr>
		</tbody>
	</table>

	</md-content>
</script>
