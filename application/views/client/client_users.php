<script type="text/ng-template" id="clientUsers.html">

<div ng-init="this.predicate = 'first_name'" class="table-responsive">
	<table class="table table-hover table-bordered">
		<thead>
			<tr style="cursor: pointer;">
				<th ng-click="this.predicate = 'first_name'; this.reverse = !reverse">Name</th>
				<th ng-click="this.predicate = 'email'; this.reverse = !reverse">Email</th>
				<th ng-click="this.predicate = 'role_title'; this.reverse = !reverse">Role</th>
			</tr>
		</thead>
		<tbody>
			<tr ng-repeat="user in user_list | orderBy:this.predicate:this.reverse">
				<td>{{user.first_name}} {{user.last_name}}</td>
				<td>{{user.email}}</td>
				<td>{{user.role_title}}</td>
			</tr>
		</tbody>
	</table>
</div>

</script>
