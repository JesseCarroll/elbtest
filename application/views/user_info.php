<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
	<div flex layout="row" layout-align="start center" layout-padding layout-margin>
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/admin"); ?>">Admin</a>&nbsp;>&nbsp;
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/user"); ?>">Users</a>&nbsp;>&nbsp;
		<?php echo (empty($id))? 'Add' : 'Edit'; ?> User
	</div>
	<div layout="row" layout-padding class="controls">
		<md-button type="submit" form="info" class="md-raised md-accent md-hue-3">Save</md-button>
		<md-button style="font-size: 14px;" class="md-primary md-raised" ng-href="<?php echo base_url("user"); ?>">Cancel</md-button>
	</div>
</md-toolbar>

<md-content flex class="md-padding content-main" ng-init="hideClientMenu = true">
	<md-content class="md-padding md-whiteframe-z1">

	<form role="form" id="info" method="post" action="<?php echo current_url(); ?>">
		<input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
		<input type="hidden" id="action" name="action" value="info">

		<?php $has_error = (strlen(form_error('client_id')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="client_id">Client</label>
			<div class="row">
				<div class="col-md-4">
					<select class="form-control" id="client_id" name="client_id">
						<?php
						foreach ($all_clients as $item) {
							$selected = ($item->client_id == $client_id)? 'selected="selected"' : '';
							echo "<option value=\"{$item->client_id}\" $selected>{$item->client_name}</option>\n";
						}
						?>
					</select>
				</div>
			</div>
			<?php echo form_error('client_id', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('first_name')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="first_name">First Name</label>
			<div class="row">
				<div class="col-md-4">
					<input type="text" class="form-control" id="first_name" name="first_name" maxlength="50" value="<?php echo set_value('first_name', $first_name); ?>">
				</div>
			</div>
			<?php echo form_error('first_name', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('last_name')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="last_name">Last Name</label>
			<div class="row">
				<div class="col-md-4">
					<input type="text" class="form-control" id="last_name" name="last_name" maxlength="50" value="<?php echo set_value('last_name', $last_name); ?>">
				</div>
			</div>
			<?php echo form_error('last_name', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('email')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="email">Email</label>
			<div class="row">
				<div class="col-md-6">
					<input type="text" class="form-control" id="email" name="email" maxlength="100" value="<?php echo set_value('email', $email); ?>">
				</div>
			</div>
			<?php echo form_error('email', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('password')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="password">Password</label>
			<div class="row">
				<div class="col-md-4">
					<input type="password" class="form-control" id="password" name="password" maxlength="20" value="<?php echo set_value('password', $password);?>" onfocus="if ('<?php echo User_model::HASHED_PASSWORD_PLACEHOLDER; ?>' == this.value) this.value = '';" onblur="if ('' == this.value) this.value = '<?php echo User_model::HASHED_PASSWORD_PLACEHOLDER; ?>'">
				</div>

				<?php /* if(!empty($id)) { ?>
				<div class="col-md-3">
				  <a class="btn btn-sm btn-danger" id="sendpassword" href="javascript:sendpassword(<?php echo $id; ?>)">Reset Password</a>
				</div>
				<?php } */ ?>
			</div>
			<?php echo form_error('password', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('user_status')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label" for="user_status">Status</label>
			<div class="row">
				<div class="col-md-4">
					<div class="radio">
						<label>
							<input type="radio" name="user_status" id="status1" value="1" <?php echo set_radio('user_status', '1', ($user_status == User_model::STATUS_ACTIVE)); ?>>
							Active
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="user_status" id="status0" value="0" <?php echo set_radio('user_status', '0', ($user_status == User_model::STATUS_INACTIVE)); ?>>
							Inactive
						</label>
					</div>
					<div class="radio">
						<label>
							<input type="radio" name="user_status" id="status2" value="2" <?php echo set_radio('user_status', '2', ($user_status == User_model::STATUS_DELETED)); ?>>
							Deleted
						</label>
					</div>
				</div>
			</div>
			<?php echo form_error('user_status', '<span class="help-block">', '</span>'); ?>
		</div>

		<?php $has_error = (strlen(form_error('super_admin')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<div class="row">
				<div class="col-md-4">
					<div class="checkbox">
						<label>
							<?php
							$checked = '';
							if (!empty($super_admin)) {
								$checked = 'checked = "1"';
							}
							?>
							<script type="text/javascript">
							function updatePermissionControlVisibility() {
								var super_admin_checkbox = $('input[name="super_admin"]');
								if (super_admin_checkbox.is(':checked')) {
									$('div#permissions').hide();
								} else {
									$('div#permissions').show();
								}
							}
							$(document).ready(function() {
								updatePermissionControlVisibility();
								$('input[name="super_admin"]').change(function() {
									updatePermissionControlVisibility();
								});
							});
							</script>
							<input type="checkbox" name="super_admin" value="1" <?php echo $checked; ?>> Super Admin?
						</label>
					</div>
				</div>
			</div>
			<?php echo form_error('role', '<span class="help-block">', '</span>'); ?>
		</div>

<style>
table#client_course_roles {
	width: 100%;
	white-space: nowrap;
	overflow-x: hidden;
}
table#client_course_roles tr.spacer {
	height: 2rem;
}
table#client_course_roles td.client {
	border-bottom: 1px #444 solid;
}
table#client_course_roles td.course {
	padding-left: 5rem;
	overflow-x: hidden;
	border-bottom: 1px #ccc solid;
}

ul.group li {
	list-style-type: none;
}
</style>

<?php
function roles_dropdown($all_active_roles, $name, $selected_value = null) {
	$ret = "<select name='$name'><option value=''></option>";
	foreach ($all_active_roles as $role) {
		$selected = ($role->role_id == $selected_value) ? 'selected' : '';
		$ret .= "<option value='{$role->role_id}' $selected>{$role->role_title}</option>";
	}
	$ret .= "</select>";
	return $ret;
}
?>
		<?php if($current_user->super_admin) { ?>
			<div class="form-group" id="permissions">
				<div class="row">
					<div class="col-xs-6">
						<h2>Groups</h2>
						<ul class="group">
							<?php
							foreach ($all_groups as $group) {
								$checked = (false !== array_search($group->group_id, $groups)) ? 'checked' : '';
								echo "<li><input type='checkbox' name='groups[]' value='{$group->group_id}' $checked>";
								$url = base_url("group/{$group->group_id}/edit");
								echo "&nbsp;<a href='$url'>{$group->name}</a></li>";
							}
							?>
						</ul>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-6">
						<h2>Client &amp; Course Roles</h2>
						<table id="client_course_roles">
							<thead>
								<th>Course/Client</th>
								<th>Role</th>
								<!--th>Effective Role</th-->
							</thead>
							<tbody>
								<?php
								$current = '';
								foreach ($all_clients as $client) {
									$selected_role = isset($client_roles[$client->client_id]) ? $client_roles[$client->client_id] : null;
									$markup = roles_dropdown($all_active_roles, "client_roles[{$client->client_id}]", $selected_role);
									echo "<tr><td class='client'>{$client->client_name}</td><td>$markup</td><td></td></tr>";
									foreach ($client->get_courses() as $course) {
										$selected_role = isset($course_roles[$course->course_id]) ? $course_roles[$course->course_id] : null;
										$markup = roles_dropdown($all_active_roles, "course_roles[{$course->course_id}]", $selected_role);
										$effective_role = '???';
										echo "<tr><td class='course'>{$course->course_title}</td><td>$markup</td><!--td>$effective_role</td--></tr>";
									}
									echo "<tr class='spacer'></tr>";
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		<?php } ?>

		<button type="submit" class="btn btn-primary btn-sm">Save</button>
		<a class="btn btn-default btn-sm" href="<?php echo base_url('user'); ?>">Cancel</a>

	  </form>
	</md-content>
</md-content>


<script type="text/javascript">
  $('form#info #first_name').focus();

  // check for existing checked clients
  $("input[name='allowed_clients[]']").each(function() {
	var client_id = $(this).val();

	if(this.checked) {
		$("input[name='allowed_courses[]']").each(function() {
			if($(this).attr('data-client') == client_id) {
				$(this).attr('disabled', true);
				$(this).parent('label').css('color', 'rgba(0,0,0,.5)');
			}
		});
	  }
  });

  // toggle courses when a client is checked
  $("input[name='allowed_clients[]']").on('click', function() {
	var client_id = $(this).val();

	if(this.checked) {
		$("input[name='allowed_courses[]']").each(function() {
			if($(this).attr('data-client') == client_id) {
				$(this).attr('disabled', true);
				$(this).parent('label').css('color', 'rgba(0,0,0,.5)');
			}
		});
	  } else {
		$("input[name='allowed_courses[]']").each(function() {
			if($(this).attr('data-client') == client_id) {
				$(this).removeAttr('disabled');
				$(this).parent('label').css('color', 'rgba(0,0,0,1)');
			}
		  });
	  }
  });

</script>
