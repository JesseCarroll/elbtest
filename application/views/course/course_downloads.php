<div class="row col-md-8">
	<table class="table table-bordered">
	    <thead>
	        <tr>
				<th>Filename</th>
	            <th>Download Date</th>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach ($records as $record) { ?>
	        	<tr>
	    			<td><a href="<?php echo $download_path . '/' . $record->filename; ?>"><?php echo $record->filename; ?></a></td>
	    			<td><?php echo $record->date_downloaded; ?></td>
	    		</tr>
	    	<?php } ?>
	    </tbody>
	</table>
</div>

<script>
	$script.ready("jquery", function(){
		$('.file').on('click', function(){
			$.ajax({
				url: '<?php echo current_url(); ?>',
				type: 'post',
				data: {'file': $(this).html()}
			});
		});
	});
</script>