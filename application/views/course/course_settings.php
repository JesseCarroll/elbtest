<script type="text/ng-template" id="courseSettings.html">

<md-content layout="row" layout-xs="column" layout-margin ng-controller="courseSettings as settings">
	<md-content ng-if="course.state == 'author'" flex layout="column" class="md-whiteframe-z1">
		<md-toolbar>
			<div class="md-toolbar-tools">
				<h3>Enabled Pages</h3>
			</div>
		</md-toolbar>
		<div layout="row" layout-align="start center" class="settings-item" ng-repeat-start="item in options_list">
			<label flex>{{item.name}} <md-icon class="material-icons" ng-if="helper[item.value]" ng-click="openHelp($this, item.value)">help</md-icon></label>
		  <md-switch ng-model="data.config[item.value]" aria-label="enable {{item.name}}" ng-change="addMenuItem(item.value, $event)"></md-switch>
		</div>
		<md-divider ng-repeat-end></md-divider>
	</md-content>

	<md-content ng-if="course.state == 'author'" flex layout="column" class="md-whiteframe-z1">
		<md-toolbar>
			<div class="md-toolbar-tools">
				<h3>Course Options</h3>
			</div>
		</md-toolbar>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Available Offline <md-icon ng-if="helper.availableOffline" class="material-icons" ng-click="openHelp($this, 'availableOffline')">help</md-icon></label>
		  <md-switch ng-model="data.config.availableOffline" aria-label="available offline"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Click State <md-icon ng-if="helper.clickstate" class="material-icons" ng-click="openHelp($this, 'clickstate')">help</md-icon></label>
		  <md-switch ng-model="data.config.clickState" aria-label="toggle click state"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Enable Gating <md-icon ng-if="helper.gating" class="material-icons" ng-click="openHelp($this, 'gating')">help</md-icon></label>
		  <md-switch ng-model="data.config.gate" aria-label="enable course gating"></md-switch>
		</div>
		<div ng-if="data.config.gate" layout="row" layout-align="start center" class="settings-item">
			<span flex>Gate Type <md-icon ng-if="helper.gateType" class="material-icons" ng-click="openHelp($this, 'gatetype')">help</md-icon></span>
			<md-switch ng-model="data.config.gateType" aria-label="toggle gate type" ng-true-value="'timer'" ng-false-value="'click'">{{data.config.gateType}}</md-switch>
		</div>
		<div ng-if="data.config.gateType == 'timer' && data.config.gate" layout="row" layout-align="start center" class="settings-item">
      <md-input-container>
        <label>Page Timer (in seconds)</label>
        <input type="number" ng-model="data.config.timer" min="0">
        <md-icon ng-if="helper.gatingbytimer" class="material-icons" ng-click="openHelp($this, 'gatingbytimer')">help</md-icon>
      </md-input-container>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Hide Statbar <md-icon ng-if="helper.hideStatbar" class="material-icons" ng-click="openHelp($this, 'hideStatbar')">help</md-icon></label>
			<md-switch ng-model="data.home.statbar" aria-label="hide statbar" ng-true-value="false" ng-false-value="true"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Inline Glossary <md-icon ng-if="helper.inlineGlossary" class="material-icons" ng-click="openHelp($this, 'inlineGlossary')">help</md-icon></label>
		  <md-switch ng-model="data.config.disableGlossary" aria-label="toggle inline glossary" ng-true-value="false" ng-false-value="true"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>iPad Audio Playback <md-icon ng-if="helper.ipadAudio" class="material-icons" ng-click="openHelp($this, 'ipadAudio')">help</md-icon></label>
		  <md-switch ng-model="data.config.ipadAudio" aria-label="enable ipad audio playback"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Non-Linear <md-icon ng-if="helper.nonLinear" class="material-icons" ng-click="openHelp($this, 'nonLinear')">help</md-icon></label>
		  <md-switch ng-model="data.config.nonLinear" aria-label="enable non-linear"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Page Narration <md-icon ng-if="helper.narration" class="material-icons" ng-click="openHelp($this, 'narration')">help</md-icon></label>
		  <md-switch ng-model="data.config.narration" aria-label="enable course gating"></md-switch>
		</div>
		<md-divider></md-divider>
		<div ng-if="user_id == course.created_by" layout="row" layout-align="start center" class="settings-item">
			<label flex>Private <md-icon ng-if="helper.private" class="material-icons" ng-click="openHelp($this, 'private')">help</md-icon></label>
		  <md-switch ng-model="course.private" ng-true-value="'1'" ng-false-value="'0'" aria-label="make course private"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Resize Course Window <md-icon ng-if="helper.resize" class="material-icons" ng-click="openHelp($this, 'resize')">help</md-icon></label>
		  <md-switch ng-model="data.config.resize" aria-label="resize course window"></md-switch>
		</div>
		<md-divider></md-divider>
		<div layout="row" layout-align="start center" class="settings-item">
			<label flex>Encode Video for HLS<md-icon ng-if="helper.hls" class="material-icons" ng-click="openHelp($this, 'hls')">help</md-icon></label>
		  <md-switch ng-model="data.config.hls" aria-label="encode video for hls"></md-switch>
		</div>
	</md-content>

	<md-content ng-if="course.state == 'author'" flex layout="column" class="md-whiteframe-z1">
		<md-toolbar>
			<div class="md-toolbar-tools">
				<h3>Course Settings</h3>
			</div>
		</md-toolbar>
		<div layout="column" class="md-padding">
			<add-select ng-model="data.config.api" data-label="Select LMS" select-list="api_list"></add-select>
			<add-select ng-if="curriculum_list.length" ng-model="course.course_curriculum" data-label="Select Curriculum" select-list="curriculum_select"></add-select>
			<add-select ng-if="theme_list.length > 0" ng-model="data.config.theme_id" data-label="Select Theme" select-list="theme_list"></add-select>
		</div>
		<?php if($current_user->super_admin) { ?>
			<md-content layout="column" class="md-padding section-settings">
				<label>Version: </label>
				{{course_version}}
				<add-text ng-model="data.config.bugherd" data-label="Bugherd Key"></add-text>
			</md-content>
		<?php } ?>
	</md-content>

	<?php if($current_user->super_admin) { ?>
		<md-content flex layout="column" class="md-whiteframe-z1">
			<md-toolbar>
				<div class="md-toolbar-tools">
					<h3 flex>Enabled Languages</h3>
					<md-icon ng-if="helper.languages" class="material-icons" ng-click="openHelp($this, 'languages')">help</md-icon>
				</div>
			</md-toolbar>
			<div ng-repeat="language in languages | orderBy:'language_description'">
				<div ng-if="language.language_shortcode !== 'en-US'" layout="row" layout-align="start center" class="settings-item">
					<label flex>{{language.language_description}}</label>
					<md-checkbox ng-checked="course.course_enabled_languages.indexOf(language.language_shortcode) > -1" ng-click="toggleLanguages(language.language_shortcode)" aria-label="enable {{language.language_description}}" /></md-checkbox>
				</div>
			</div>
		</md-content>
	<?php } ?>
</md-content>

</script>
