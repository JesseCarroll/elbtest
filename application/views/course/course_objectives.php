<script type="text/ng-template" id="courseObjectives.html">

<section flex layout="row">
	<md-content flex layout-margin class="md-padding role-content">
	<div layout="row" style="margin-bottom: 50px;">
		<div flex="50" class="header">
			<h2>Objectives</h2>
			<p>Create objectives and then assign them to pages and quiz questions throughout the course.</p>
		</div>
		<div flex="30" style="margin: auto;">
			<md-checkbox flex ng-model="data.config.displayObjectives" aria-label="Display Objective Menu">Display Objective Menu in course?</md-checkbox>
		</div>
	</div>

		<div layout="row" style="margin-bottom: 50px;">
			<md-content flex>
				<h4>Create Objectives</h4>
				<p>Enter title and description. The learner will be able to see all objectives’ titles and descriptions.</p>
			</md-content>
			<div flex class="add-role-box teal md-padding" layout="column">
				<p>Create Objectives</p>
				<input ng-model="newObject.name" type="text" placeholder="Title (250 char. max)" maxlength="250">
				<textarea ng-model="newObject.description" rows="5" placeholder="Description"></textarea>
				<div layout="row" layout-align="space-between center" style="height: 50px;">
					<md-checkbox flex ng-model="newObject.scored" aria-label="Score this Objective">Score this Objective</md-checkbox>
					<md-input-container flex ng-if="newObject.scored" style="margin: 0">
						<input type="number" ng-model="newObject.masteryScore" placeholder="Score (%)" min="0" max="100">
					</md-input-container>
				</div>
				<div layout="row" layout-align="center center">
					<md-button md-theme="grey" class="md-primary md-raised md-hue-2" aria-label="Clear Fields" ng-click="resetFields()">Reset</md-button>
					<md-button md-theme="grey" class="md-primary md-raised md-hue-2" aria-label="Add Objective" ng-click="addNewObjective()">Enter</md-button>
				</div>
			</div>
		</div>

		<md-content flex="50">
			<h4>Link Objectives to Course Pages</h4>
			<p>Associate objectives to course pages and quiz questions. Multiple objectives can be applied to a single page or quiz question.</p>
		</md-content>

		<md-content class="course-pages" flex>
			<div ng-repeat="topic in data.topic" layout="column">
				<label>Topic {{$index+1}} : {{topic.title}}</label>
				<div ng-repeat="page in topic.page" layout="row">
					<div flex layout="row" layout-align="start center">{{$index+1}} : {{page.title}}</div>
					<div flex class="droppable" data-topic="{{$parent.$index}}" data-page="{{$index}}" ng-init="objectiveExists($parent.$index, $index)">
				    <md-chips ng-model="page.objectives" md-autocomplete-snap
              md-transform-chip="transformChip($chip)"
              md-require-match="false">
				      <md-autocomplete
			          md-selected-item="selectedItem"
			          md-search-text="searchText"
			          md-items="item in querySearch(searchText, data.objectives, 'name')"
			          md-item-text="item.name"
			          placeholder="Drag objective or type objective name here">
				        <span md-highlight-text="searchText">{{item.name}}</span>
				      </md-autocomplete>
				      <md-chip-template>
				        <span>{{getChipTitle($chip, data.objectives)}}</span>
				      </md-chip-template>
				    </md-chips>
					</div>
				</div>
			</div>
		</md-content>
	</md-content>
</section>
<md-sidenav class="md-sidenav-right md-padding role-list" md-compontent-id="roles" md-is-locked-open="true" md-whiteframe="2">
	<div ng-if="!data.objectives || data.objectives.length == 0" class="teal no-roles">
		<p>No objectives entered yet.</p>
		<md-button class="md-raised new-tag" aria-label="Add New Tag">Add New Tag</md-button>
	</div>
	<div ng-if="data.objectives.length > 0" class="roles">
		<label>Objective Tags</label>
		<div layout="row" ng-repeat="objective in data.objectives" class=" draggable role-chip teal" data-key="{{$index}}" ng-click="editObjective(objective)">
			<p flex>{{objective.name}}</p>
			<md-button aria-label="Delete" class="material-icons" ng-click="deleteObjective($event, $index, objective)">close</md-button>
		</div>
		<md-button class="md-raised new-tag" aria-label="Add New Tag" ng-click="createNewChip('.role-content')">Add New Tag</md-button>
	</div>
</md-sidenav>
</script>