<?php
	include 'course_settings.php';
	include 'course_roles.php';
	include 'course_objectives.php';
?>
<div flex id="main" ng-controller="courseCtrl" ng-switch="editor.course.section" ng-show="data && course.locked == '0'">

	<md-content ng-switch-when="detail" layout="column" layout-margin layout-padding>
		<div layout="row">
			<div flex>
				<md-content layout="column" class="md-padding">
					<md-input-container>
						<label>Course Title</label>
						<input type="text" ng-model="data.course" aria-label="Course Title" />
					</md-input-container>
					<md-input-container>
						<label>Module Title</label>
						<input type="text" ng-model="data.module" aria-label="Module Title" />
					</md-input-container>
					<md-input-container>
				      <label>Course Description</label>
				      <textarea ng-model="course.description" rows="3" md-select-on-focus></textarea>
				    </md-input-container>
				</md-content>
			</div>
			<md-content flex layout="column" class="md-padding">
				<add-media-controls
					expand-all="true"
					disable-decorations="true"
					existing-media-item-list="data.coursePoster"
					template-settings="{media: {limit: 1, items: [{label: 'Poster', type: 'image'}]}}"
					misc-options = '{hideCaption: true}'
					flex = 50
					internal-flex = 100
				></add-media-controls>
			</md-content>
		</div>

		<div flex><md-subheader class="md-primary">Course Repository <em>(20 files max. Files must not exceed 15MB in size.)</em></md-subheader></div>
		<div layout="row" id="repository" layout-wrap ng-controller="courseRepositoryCtrl">
			<md-content ng-repeat="item in courseRepositoryAssets" layout-align="center center">
				<div layout="column" layout-fill layout-align="end center" layout-margin="5">
					<div class="icon {{item.type}}"></div>
					<div>{{item.filename}}</div>
				</div>
				<div layout="column" class="media-buttons md-whiteframe-z1">
					<md-button class="material-icons add"      trigger-upload="uploadFile($file, $index)">add</md-button>
					<md-button class="material-icons delete"   ng-click="remove($index)">delete</md-button>
					<md-button class="material-icons download" ng-click="download($index)">file_download</md-button>
				</div>
				<div class="upload-progress" ng-show="uploadStatus[$index].state">
					<md-progress-circular ng-show="uploadStatus[$index].state == 'uploading'" md-mode="determinate" value="{{uploadStatus[$index].percent}}"></md-progress-circular>
					<div ng-show="uploadStatus[$index].text">{{uploadStatus[$index].text}}</div>
				</div>
			</md-content>
			<md-content ng-show="courseRepositoryAssets.length < 20">
				<div layout="column" layout-fill layout-align="end center" layout-margin="5" ng-show="uploadStatus.state">
					<div class="icon"></div>
					<div>{{uploadStatus.filename}}</div>
				</div>
				<div layout="column" class="media-buttons md-whiteframe-z1" ng-show="!uploadStatus.state">
					<md-button class="material-icons add" trigger-upload="uploadFile($file, courseRepositoryAssets.length)">add</md-button>
				</div>
				<div class="upload-progress" ng-show="uploadStatus[courseRepositoryAssets.length].state">
					<md-progress-circular ng-show="uploadStatus[courseRepositoryAssets.length].state == 'uploading'" md-mode="determinate" value="{{uploadStatus[courseRepositoryAssets.length].percent}}"></md-progress-circular>
					<div ng-show="uploadStatus[courseRepositoryAssets.length].text">{{uploadStatus[courseRepositoryAssets.length].text}}</div>
				</div>
			</md-content>
		</div>
	</md-content>

	<md-content ng-switch-when="home" layout="column" class="md-padding">
		<add-select ng-model="data.home.template" data-label="Home Layout" select-list="home_template"></add-select>
		<md-input-container>
			<label>Hero Title</label>
			<input type="text" ng-model="data.home.title" aria-label="Hero Title" />
		</md-input-container>

		<div class="md-block">
			<add-textarea ng-model="data.home.summary" data-label="Hero Description"></add-textarea>
		</div>

		<add-media-controls
			ng-if="template_settings.media.enable"
			existing-media-item-list="data.home.media"
			template-settings="template_settings"
		></add-media-controls>

		<add-text ng-model="data.home.callToAction" data-label="Call to Action"></add-text>

	</md-content>

	<md-content ng-switch-when="topics" class="md-whiteframe-z1" layout="row">
		<md-content flex ng-switch="data_type" class="topic-edit md-padding">
			<topic-edit ng-switch-when="topic"></topic-edit>
			<page-edit ng-switch-when="page"></page-edit>
		</md-content>
	</md-content>

	<md-content ng-switch-when="glossary" layout="column" layout-margin layout-padding>
		<md-content ng-if="glossary" layout="column" class="md-whiteframe-z2">
			<md-subheader class="md-primary">Glossary</md-subheader>
			<md-content layout="column" class="md-padding">
				<md-input-container>
					<label>Glossary Term</label>
					<input type="text" ng-model="glossary.term" aria-label="Module Title" />
				</md-input-container>
				<textarea ck-editor ng-model="glossary.definition" columns="1" aria-label="Glossary Definition" class="editor"></textarea>
			</md-content>
		</md-content>
	</md-content>

	<md-content ng-switch-when="resources" layout="column" layout-margin layout-padding>
		<md-content ng-if="resources" layout="column" class="md-whiteframe-z2">
			<md-subheader class="md-primary">Resource</md-subheader>
			<md-content layout="column" class="md-padding">
				<md-input-container>
					<label>Resource Title</label>
					<input type="text" ng-model="resources.label" aria-label="Module Title" />
				</md-input-container>
		    <add-textarea ng-model="resources.description" data-label="Resource Description" disabled="{{disableFields}}"></add-textarea>
		    <md-switch ng-model="resources.external" aria-label="Resource Type" ng-if="!hideControls" ng-change="removeResource()">Resource Type: {{resource_type}}</md-switch>
		    <add-text ng-if="resources.external" ng-model="resources.src" data-label="External URL" disabled="{{disableFields}}"></add-text>
		    <div flex ng-if="!resources.external" layout="row" layout-align="start center" class="fileinput fileupload resource">
		      <md-button ng-if="!disableFields" class="md-icon-button">
		        <md-icon class="material-icons" trigger-upload="uploadFile($file, 'resources.src',  {'classification': 'resources'})">add</md-icon>
		      </md-button>
		    	<add-text flex ng-if="resources.src" ng-model="resources.src" data-label="File Name" data-disabled="true"></add-text>
		    	<add-text flex="10" ng-if="resources.type" ng-model="resources.type" data-label="Resource Type" data-disabled="true"></add-text>
		    	<add-text flex="10" ng-if="resources.size" ng-model="resources.size" data-label="Resource Size (kb)" data-disabled="true"></add-text>
		    	<span ng-hide="disableFields" ng-click="removeResource()"><i ng-if="resources.src" class="fa fa-trash-o button"></i></span>
		    </div>
			</md-content>
		</md-content>
	</md-content>

	<md-content id="common-terms" ng-switch-when="terms" class="md-whiteframe-z1">
		<common-terms ng-model="data.common_terms" filter="true"></common-terms>
	</md-content>

	<md-content flex layout="column" ng-switch-when="pretest" class="pretest md-whiteframe-z2" ng-controller="preTestCtrl">
		<md-tabs md-dynamic-height md-selected="tabSelect">
			<md-tab id="tab1">
				<md-tab-label>Main</md-tab-label>
				<md-tab-body>
					<md-content layout="column" class="md-padding">
						<add-text ng-model="preTest.title" data-label="Page Title"></add-text>
						<add-textarea ng-model="preTest.text" data-label="Intro"></add-textarea>
					</md-content>
				</md-tab-body>
			</md-tab>
			<md-tab id="tab2">
				<md-tab-label>Media</md-tab-label>
				<md-tab-body>
					<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()">
						<add-media-controls
							ng-if="preTest.media"
							existing-media-item-list="preTest.media"
							expand-all="true"
							disable-decorations="true"
							template-settings="{media: {limit: 1, items: [{label: 'Popup Image', type: ['image']}]}}"
						></add-media-controls>
					</md-content>
				</md-tab-body>
			</md-tab>
			<md-tab id="tab3" md-on-select="pretestActive = true" md-on-deselect="pretestActive = false">
				<md-tab-label>Summary</md-tab-label>
				<md-tab-body>
					<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()" >
						<summary-object ng-if="pretestActive" ng-model="preTest" template-settings="template_settings"></summary-object>
					</md-content>
				</md-tab-body>
			</md-tab>
			<md-tab id="tab4">
				<md-tab-label>Questions</md-tab-label>
				<md-tab-body>
					<md-content flex class="md-whiteframe-z1">
						<md-content flex class="md-padding">
							<md-content ng-if="!preTest.type">
								<md-button class="md-raised md-accent" ng-click="preTest.type = 'new'">Build New Questions</md-button>
								<md-button class="md-raised md-accent" ng-click="preTest.type = 'mirror'">Use Existing Questions</md-button>
								<md-button class="md-raised md-accent" ng-click="preTest.type = 'copy'">Copy/Edit Existing Questions</md-button>
							</md-content>
							<ng-switch flex on="preTest.type">
								<div ng-switch-when="new">
									<md-button class="md-icon-button material-icons" ng-click="resetQuestions()">arrow_back</md-button>
									<div ui-sortable="sortableModuleOptions" ng-model="preTest.question">
										<div ng-repeat="question in preTest.question" ng-init="parent = preTest.question; parentTemplate = 'pretest'" ng-include="'<?php echo base_url(); ?>angular/add_question.php'"></div>
									</div>
									<button type="button" class="btn btn-link" ng-click="addElement(preTest.question)">Add Question</button>
								</div>
								<md-content ng-switch-when="copy">
									<div ng-if="!quiz_selected">
										<md-content layout="row" layout-align="start center">
											<md-button class="md-icon-button material-icons" ng-click="resetQuestions()">arrow_back</md-button>
											<span>Please select the questions you would like to copy and edit:</span>
										</md-content>
										<md-content ng-repeat="item in quiz_list" class="md-whiteframe-z1 md-padding md-margin button" ng-click="copyQuiz($index)">
											<label>{{item.title}}</label>
											<p ng-repeat="question in item.question">{{$index+1}}: {{question.prompt}}</p>
										</md-content>
									</div>
									<md-content ng-if="quiz_selected">
										<md-button class="md-icon-button material-icons" ng-click="resetCopy()">arrow_back</md-button>
										<div ui-sortable="sortableModuleOptions" ng-model="preTest.question">
											<div ng-repeat="question in preTest.question" ng-init="parent = preTest.question; parentTemplate = 'pretest'" ng-include="'<?php echo base_url(); ?>angular/add_question.php'"></div>
										</div>
										<button type="button" class="btn btn-link" ng-click="addElement(preTest.question)">Add Question</button>
									</md-content>
								</md-content>
								<md-content ng-switch-when="mirror">
									<md-content layout="row" layout-align="start center">
										<md-button class="md-icon-button material-icons" ng-click="deleteMirrorKey(); resetQuestions()">arrow_back</md-button>
										<span>Please select which questions you would like to use:</span>
									</md-content>
									<md-content ng-repeat="item in quiz_list" class="md-whiteframe-z1 md-padding md-margin button" ng-click="mirrorQuiz($index)" ng-class="{'blue':item.mirror}">
										<label>{{item.title}}</label>
										<p ng-repeat="question in item.question">{{$index+1}}: {{question.prompt}}</p>
									</md-content>
								</md-content>
							</ng-switch>
						</md-content>
					</md-content>
				</md-tab-body>
			</md-tab>
		</md-tabs>
	</md-content>

	<md-content ng-switch-when="certificate" ng-init="_temp.certificate = certificate_data" class="md-padding md-whiteframe-z1" layout="column">
		<md-input-container>
			<label>Certificate Title</label>
			<input ng-model="data.certificate.title">
		</md-input-container>
		<my-text-area flex ng-model="data.certificate.text" my-label="'Certificate Text'"></my-text-area>
		<add-media-controls
			expand-all="true"
			disable-decorations="true"
			existing-media-item-list="data.certificate.media"
			template-settings="{media: {limit: 1, items: [{label: 'Certificate Image', type: 'image'}]}}"
			misc-options = '{hideCaption: true}'
		></add-media-controls>
	</md-content>

	<md-content layout-fill ng-switch-when="objectives" class="theme" style="overflow: hidden;" ng-controller="objectiveCtrl">
		<div layout="row" layout-fill ng-include="'courseObjectives.html'"></div>
	</md-content>

	<md-content ng-switch-when="cms" class="md-padding md-whiteframe-z1" ng-controller="courseCMS">
		<md-input-container flex>
			<label>CMS Code Title</label>
			<input ng-model="cms.title">
		</md-input-container>

		<add-textarea flex ng-model="cms.text" my-label="'CMS Code Text'"></add-textarea>

		<md-input-container flex>
			<label>Font Color (Hex Code)</label>
			<input ng-model="cms.font_color">
		</md-input-container>

		<md-input-container flex>
			<label>Font Size</label>
			<input ng-model="cms.font_size">
		</md-input-container>

		<md-input-container flex>
			<label>Box Color (Hex Code or 'transparent')</label>
			<input ng-model="cms.box_color">
		</md-input-container>

		<md-input-container flex>
			<label>Box Border Color (Hex Code or 'transparent')</label>
			<input ng-model="cms.box_border_color">
		</md-input-container>
	</md-content>

	<md-content ng-switch-when="assets" class="md-padding md-whiteframe-z1" ng-controller="assetCtrl">
		<md-whiteframe layout="column" class="md-whiteframe-z1" flex="96" ng-repeat="(key, value) in asset_array">
			<md-toolbar>
				<div class="md-toolbar-tools">
					<h2>{{key}}</h2>
				</div>
			</md-toolbar>
			<md-content layout="row" layout-margin layout-wrap>
				<md-content class="md-whiteframe-z1" layout="column" flex="20" layout-fill ng-repeat="item in value">
					<div flex class="md-padding" layout="column" layout-align="start center">
						<img ng-if="item.path.indexOf('.mp3')==-1" ng-src="{{item.path}}" style="background-color: #363636; max-width: 100%;">
						<div ng-if="item.path.indexOf('.mp3')>-1" style="font-family:FontAwesome; font-size:40px;">&#xf028;</div>
						<p>{{item.title}}</p>
						{{item.filename | removeTimestamp}}
					</div>
					<div class="md-actions fileinput fileupload" layout="row" layout-align="center center">
						<button class="md-button" trigger-upload="uploadFile($file, 'item.filename', {'classification': 'assets', 'filename': item.filename, 'callback': changeAsset(item)})">Change</button>
						<md-button ng-click="restoreAsset(item)">Reset</md-button>
					</div>
				</md-content>
			</md-content>
		</md-whiteframe>
	</md-content>

	<md-content layout-fill ng-switch-when="roles" class="theme">
		<div layout-fill ng-include="'courseRoles.html'"></div>
	</md-content>

	<md-content ng-switch-when="settings" class="md-whiteframe-z1">
		<div ng-include="'courseSettings.html'"></div>
	</md-content>

	<div ng-switch-when="translate" ng-controller="courseTranslateIndex">
		<div layout="row" layout-wrap class="md-padding">
			<md-card ng-repeat="language in active_languages | orderBy: 'language_description'" class="language-box" ng-click="goToTranslation(language.language_shortcode)">
				<md-card-header layout="row" layout-align="start start">
					<h3 flex>{{language.language_description}}</h3>
					<md-button ng-show="!loading[language.language_shortcode]" class="md-primary md-icon-button material-icons" ng-click="previewTranslation($event, language.language_shortcode)">search</md-button>
					<md-progress-circular ng-show="loading[language.language_shortcode]" class="md-hue-2" md-diameter="15px"></md-progress-circular>
				</md-card-header>
				<md-card-footer ng-show="language.progress">
					{{language.progress}}%
					<md-progress-linear md-mode="determinate" value="{{language.progress}}"></md-progress-linear>
				</md-card-footer>
			</md-card>
		</div>
	</div>
</div>
