<script type="text/ng-template" id="courseRoles.html">

<section layout-fill layout="row" flex ng-controller="roleCtrl">
	<md-content flex layout-margin class="md-padding role-content">
		<div flex="50" class="header">
			<h2>Roles or Regions</h2>
			<p>Specify what course pages certain groups of learners see. Create groups by making tags, and then applying the tags to all or some pages.</p>
		</div>

		<div layout="row" style="margin-bottom: 50px;">
			<md-content flex>
				<h4>Create Tags</h4>
				<p>Enter employee roles, titles, regions or other groups you would like to assign particular course pages to.</p>
				<p><i>Examples:</i><br>Contractors, Trainees, Production Department, North America, Europe, Western Sales Team, Eastern Sales Team</p>
			</md-content>
			<div flex class="add-role-box teal md-padding" layout="column">
				<p>Create Tags</p>
				<input ng-model="newObject.name" type="text">
				<md-checkbox ng-model="applyToAll">Apply Tag To All Pages</md-checkbox>
				<md-button class="gray" aria-label="Add New Tag" ng-click="addNewRole()">Enter</md-button>
			</div>
		</div>

		<md-content flex="50">
			<h4>Apply Tags to Course Pages</h4>
			<p>All course content is viewable by all groups until pages are tagged. Apply tags to the appropriate pages to make them available for particular groups.</p>
		</md-content>

		<md-content class="course-pages" flex>
			<div ng-repeat="topic in data.topic" layout="column">
				<label>Topic {{$index+1}} : {{topic.title}}</label>
				<div ng-repeat="page in topic.page" layout="row">
					<div flex layout="row" layout-align="start center">{{$index+1}} : {{page.title}}</div>
					<div flex class="droppable" data-topic="{{$parent.$index}}" data-page="{{$index}}" ng-init="roleExists($parent.$index, $index)">
				    <md-chips ng-model="page.roles" md-autocomplete-snap
              md-transform-chip="transformChip($chip)"
              md-require-match="false">
				      <md-autocomplete
			          md-selected-item="selectedItem"
			          md-search-text="searchText"
			          md-items="item in querySearch(searchText, data.roles, 'name')"
			          md-item-text="item.name"
			          placeholder="Drag tags or type tag name here">
				        <span md-highlight-text="searchText">{{item.name}}</span>
				      </md-autocomplete>
				      <md-chip-template>
				        <span>{{getChipTitle($chip, data.roles)}}</span>
				      </md-chip-template>
				    </md-chips>
					</div>
				</div>
			</div>
		</md-content>
	</md-content>
	<md-sidenav class="md-sidenav-right md-padding role-list" md-compontent-id="roles" md-is-locked-open="true" md-whiteframe="2">
		<div ng-if="!data.roles || data.roles.length == 0" class="teal no-roles">
			<p>No roles or regions entered yet.</p>
			<md-button class="md-raised new-tag" aria-label="Add New Tag">Add New Tag</md-button>
		</div>
		<div ng-if="data.roles.length > 0" class="roles">
			<label>Assigned Tags</label>
			<div layout="row" ng-repeat="role in data.roles" class=" draggable role-chip teal" data-key="{{$index}}">
				<p flex>{{role.name}}</p>
				<md-button aria-label="Delete" class="material-icons" ng-click="deleteRole($event, $index, role)">close</md-button>
			</div>
			<md-button class="md-raised new-tag" aria-label="Add New Tag" ng-click="createNewChip('.role-content')">Add New Tag</md-button>
		</div>
	</md-sidenav>
</section>

</script>