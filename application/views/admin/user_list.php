<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true" ng-controller="userCtrl as admin">
	<md-content layout="column" flex class="md-whiteframe-z1">
		<md-toolbar layout="row" class="md-hue-1" layout-padding layout-align="start center">
			<span flex layout-margin>Users</span>
			<md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo current_url() . '/new'; ?>">Add New</md-button>
		</md-toolbar>
		<md-subheader class="md-primary">
			<div layout="row" layout-align="start center">
				<md-input-container flex class="md-margin">
					<label>Search Users</label>
					<md-icon class="material-icons" aria-label="search">search</md-icon>
					<input ng-model="q" aria-label="search">
				</md-input-container>
				<md-radio-group ng-model="admin.status_toggle" layout="row">
					<md-radio-button value="all">All Users</md-radio-button>
					<md-radio-button value="1">Active Users</md-radio-button>
					<md-radio-button value="0">Inactive Users</md-radio-button>
				</md-radio-group>
			</div>
		</md-subheader>
		<md-content class="md-padding">
			<table flex class="table table-hover table-bordered">
				<thead>
					<tr style="cursor: pointer;">
						<th ng-click="this.predicate = 'first_name'; reverse = !reverse">Name</th>
						<th ng-click="this.predicate = 'email'; reverse = !reverse">Email</th>
						<th ng-click="this.predicate = 'super_admin'; reverse = !reverse">Super Admin</th>
						<th ng-click="this.predicate = 'user_status'; reverse = !reverse">Status</th>
						<?php if($current_user->can('test_user')) {?>
							<th>Actions</th>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr ng-if="user.user_status == admin.status_toggle || admin.status_toggle == 'all'" ng-repeat="user in user_list | orderBy:this.predicate:reverse | filter:q" ng-click="admin.goTo(user.user_id)" style="cursor: pointer;">
						<td>{{user.first_name}} {{user.last_name}}</td>
						<td>{{user.email}}</td>
						<td>
							<div ng-if="1 == user.super_admin">♛</div>
						</td>
						<td>{{user.user_status|statusName}}</td>
						<?php if($current_user->can('test_user')) {?>
							<td><md-icon class="material_icons" ng-click="admin.test_user(user.user_id); $event.stopPropagation();">supervisor_account</md-icon></td>
						<?php } ?>
					</tr>
				</tbody>
			</table>
		</md-content>
	</md-content>
	<form id="record" method="post" action="<?php echo base_url('user/0/delete'); ?>">
		<input type="hidden" id="id" name="id" value="0">
	</form>

</md-content>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Delete user</h4>
			</div>
			<div class="modal-body">

				<p>Selected user will be permanently deleted and cannot be recovered. Are you sure?</p>

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="dialog-confirm">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$script.ready('document', function() {
		$( "#dialog-confirm" ).click(function() {
			 $('form#record').submit();
		});
	});

	function deleterecord(id) {
		$('form#record #id').val(id);
		$('#myModal').modal('show');
	}
</script>