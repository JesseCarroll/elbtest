<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
	<div flex layout="row" layout-align="start center" layout-padding layout-margin>Groups</div>
	<div layout="row" layout-padding class="controls">
		<md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo current_url() . '/new'; ?>" style="font-size: 14px; text-decoration: none;">
			Add New
		</md-button>
	</div>
</md-toolbar>

<?php if(!empty($alert)) { ?>
	<div class="alert alert-dismissable alert-<?php echo $alert_type; ?>">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="right:0;">&times;</button>
		<?php echo $alert_msg; ?>
	</div>
<?php } ?>

<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true">
	<md-content layout="column" flex class="md-padding md-whiteframe-z1">

		<?php
		if (count($records) > 0):
		?>
		<table class="table table-hover table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Members</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($records as $group):
				?>
				<tr>
					<td><?php echo $group->name; ?></td>
					<td>
						<?php
						$members = $group->get_users();
						$count = count($members);
						printf('%d %s', $count, ($count == 1) ? 'user' : 'users');
						?>
					</td>
					<td>
						<div class="btn-group pull-right">
							<button class="btn btn-xs btn-info dropdown-toggle" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<ul class="dropdown-menu">
								<li><a href="<?php echo base_url("group/$group->group_id/edit"); ?>">Manage Group</a></li>
								<li><a href="javascript:deleterecord('<?php echo $group->group_id; ?>')">Delete Group</a></li>
							</ul>
						</div>
					</td>
				</tr>
				<?php
				endforeach;
				?>
			</tbody>
		</table>
		<?php
		else:
		?>
		<p>No groups yet!</p>
		<?php
		endif;
		?>

		<div class="pull-right"><?php echo $pagination; ?></div>

		<form id="record" method="post" action="<?php echo current_url(); ?>delete">
			<input type="hidden" id="id" name="id" value="0">
		</form>
	</md-content>
</md-content>

<!-- Modal -->
<div class="modal fade group-list" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">Delete group</h4>
			</div>
			<div class="modal-body">
				<p>Selected group will be permanently deleted and cannot be recovered. Are you sure?</p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary" id="dialog-confirm">Save changes</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
	$(function() {
		$( "#dialog-confirm" ).click(function() {
			$('form#record').submit();
		});
	});

	function deleterecord(id) {
		$('form#record').attr('action', "<?php echo current_url(); ?>/" + id + '/delete');
		$('#myModal').modal('show');
	}
</script>
