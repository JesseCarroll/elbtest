<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true" ng-controller="languageCtrl as admin">
  <md-content layout="column" flex class="md-whiteframe-z1">
    <md-toolbar layout="row" class="md-hue-1" layout-padding layout-align="start center">
      <span flex layout-margin>Languages</span>
      <div layout="row" layout-padding class="controls">
        <md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo current_url() . '/new'; ?>" style="font-size: 14px; text-decoration: none;">
          Add New
        </md-button>
      </div>
    </md-toolbar>
    <md-subheader class="md-primary">
      <div layout="row" layout-align="start center">
        <md-input-container flex class="md-margin">
          <label>Search Languages</label>
          <md-icon class="material-icons" aria-label="search">search</md-icon>
          <input ng-model="q" aria-label="search">
        </md-input-container>
      </div>
    </md-subheader>
    <md-content class="md-padding">
      <table flex class="table table-hover table-bordered">
        <thead>
          <tr style="cursor: pointer;">
            <th ng-click="this.predicate = 'language_shortcode'; reverse = !reverse">Shortcode</th>
            <th ng-click="this.predicate = 'language_description'; reverse = !reverse">Description</th>
            <th ng-click="this.predicate = 'language_status'; reverse = !reverse">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="item in languages | orderBy:this.predicate:reverse | filter:q" ng-click="admin.goTo(base_url + 'language/' + item.language_id + '/edit')" style="cursor: pointer;">
            <td>{{item.language_shortcode}}</td>
            <td>{{item.language_description}}</td>
            <td>{{item.language_status|statusName}}</td>
          </tr>
        </tbody>
      </table>
    </md-content>
  </md-content>
</md-content>






<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">Delete language</h4>
	  </div>
	  <div class="modal-body">

		<p>Selected language will be permanently deleted and cannot be recovered. Are you sure?</p>

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		<button type="button" class="btn btn-primary" id="dialog-confirm">Save changes</button>
	  </div>
	</div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    $(function() {
        $( "#dialog-confirm" ).click(function() {
			 $('form#record').submit();
		});
    });

    function deleterecord(id)
    {
    	var url = '<?php echo base_url(); ?>' + 'language/' + id + '/delete';
        $('form#record').attr('action',url);
        $('#myModal').modal('show');
    }
</script>