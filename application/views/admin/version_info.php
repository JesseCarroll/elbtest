<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
    <div flex layout="row" layout-align="start center" layout-padding layout-margin>
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/admin"); ?>">Admin</a>&nbsp;>&nbsp;
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/version"); ?>">Versions</a>&nbsp;>&nbsp;
		<?php echo (empty($id))? 'Add' : 'Edit'; ?> Version
    </div>
    <div layout="row" layout-padding class="controls">
		<md-button type="submit" form="info" class="md-raised md-accent md-hue-3">Save</md-button>
		<md-button style="font-size: 14px;" class="md-primary md-raised" ng-href="<?php echo base_url("version"); ?>">Cancel</md-button>
    </div>
</md-toolbar>

<md-content flex class="md-padding content-main" ng-init="hideClientMenu = true">
    <md-content class="md-padding md-whiteframe-z1">

    <form class="col-md-4" role="form" id="info" method="post" action="<?php echo current_url(); ?>" enctype="multipart/form-data">
        <input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
        <input type="hidden" id="action" name="action" value="info">

		<?php $has_error = (strlen(form_error('vnumber')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label required" for="vnumber">Version Number</label>
			<?php echo form_error('vnumber', '<span class="help-block">', '</span>'); ?>
			<?php if(isset($subvnumber)) { ?>
				<div class="well bg-promary well-sm pull-right version">Sub Version: <?php echo $subvnumber; ?></div>
			<?php } ?>
		  	<input type="text" class="form-control" id="vnumber" name="vnumber" maxlength="45" value="<?php echo set_value('vnumber', $vnumber); ?>">
		</div>

		<?php $has_error = (strlen(form_error('vnumber')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
			<label class="control-label required" for="course_package">Upload Course</label>
			<?php echo form_error('course_package', '<span class="help-block">', '</span>'); ?>
			<div class="fileinput fileinput-new input-group" style="margin-bottom: 0;" data-provides="fileinput">
			  <div class="form-control" data-trigger="fileinput">
			  	<i class="glyphicon glyphicon-file fileinput-exists"></i>
			  	<span class="fileinput-filename"></span>
			  </div>
			  <span class="input-group-addon btn btn-default btn-file">
			  	<span class="fileinput-new" data-trigger="fileinput">Select file</span>
			  	<span class="fileinput-exists" data-trigger="fileinput">Change</span>
			  	<input type="file" name="course_package">
			  </span>
			  <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
			</div>
		    <small>Upload a zipped course file here. Must include /public and /app directories.</small>
		</div>

		<?php $has_error = (strlen(form_error('status')) > 0)? 'has-error' : ''; ?>
		<div class="form-group <?php echo $has_error; ?>">
		<label class="control-label" for="status">Status</label>
		<div class="row">
		<div class="col-md-4">
		  <div class="radio">
		  <label>
			<input type="radio" name="status" id="status1" value="1" <?php echo set_radio('status', '1', ($status == 1)); ?>>
			Active
		  </label>
		  </div>
		  <div class="radio">
		  <label>
			<input type="radio" name="status" id="status0" value="0" <?php echo set_radio('status', '0', ($status == 0)); ?>>
			Inactive
		  </label>
		  </div>
		  <div class="radio">
		  <label>
			<input type="radio" name="status" id="status2" value="2" <?php echo set_radio('status', '2', ($status == 2)); ?>>
			Deleted
		  </label>
		  </div>
		</div>
		</div>
		<?php echo form_error('status', '<span class="help-block">', '</span>'); ?>
		</div>

		<button type="submit" class="btn btn-primary btn-sm">Save</button>
		<a class="btn btn-default btn-sm" href="<?php echo base_url('version'); ?>">Cancel</a>

      </form>
	</md-content>
</md-content>