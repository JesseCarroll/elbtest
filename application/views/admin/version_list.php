<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true" ng-controller="versionCtrl as admin">
  <md-content layout="column" flex class="md-whiteframe-z1">
    <md-toolbar layout="row" class="md-hue-1" layout-padding layout-align="start center">
      <span flex layout-margin>Versions</span>
      <div layout="row" layout-padding class="controls">
        <md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo current_url() . '/new'; ?>" style="font-size: 14px; text-decoration: none;">
          Add New
        </md-button>
      </div>
    </md-toolbar>
    <md-subheader class="md-primary">
      <div layout="row" layout-align="start center">
        <md-input-container flex class="md-margin">
          <label>Search Versions</label>
          <md-icon class="material-icons" aria-label="search">search</md-icon>
          <input ng-model="q" aria-label="search">
        </md-input-container>
      </div>
    </md-subheader>
    <md-content class="md-padding">
      <table flex class="table table-hover table-bordered" ng-init="this.predicate = 'version_number'; this.reverse = true">
        <thead>
          <tr style="cursor: pointer;">
            <th ng-click="this.predicate = 'version_number'; reverse = !reverse">Number</th>
            <th ng-click="this.predicate = 'version_status'; reverse = !reverse">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="item in versions | orderBy:this.predicate:reverse | filter:q" ng-click="admin.goTo(item.version_id)" style="cursor: pointer;">
            <td>{{item.version_number}}</td>
            <td>{{item.version_status|statusName}}</td>
          </tr>
        </tbody>
      </table>
    </md-content>
  </md-content>
</md-content>