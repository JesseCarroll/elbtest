<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
	<div flex layout="row" layout-align="start center" layout-padding layout-margin>
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/admin"); ?>">Admin</a>&nbsp;>&nbsp;
		<a style="color: #FFF; text-decoration: none;" href="<?php echo base_url("/group"); ?>">Groups</a>&nbsp;>&nbsp;
		<?php echo (empty($id))? 'Add' : 'Edit'; ?> Group
	</div>
	<div layout="row" layout-padding class="controls">
		<md-button type="submit" form="info" class="md-raised md-accent md-hue-3">Save</md-button>
		<md-button style="font-size: 14px;" class="md-primary md-raised" ng-href="<?php echo base_url("group"); ?>">Cancel</md-button>
	</div>
</md-toolbar>

<style>
table#client_course_roles {
	width: 100%;
	white-space: nowrap;
	overflow-x: hidden;
}
table#client_course_roles tr.spacer {
	height: 2rem;
}
table#client_course_roles td.client {
	border-bottom: 1px #444 solid;
}
table#client_course_roles td.course {
	padding-left: 5rem;
	overflow-x: hidden;
	border-bottom: 1px #ccc solid;
}
</style>

<?php
function roles_dropdown($all_active_roles, $name, $selected_value = null) {
	$ret = "<select name='$name'><option value=''></option>";
	foreach ($all_active_roles as $role) {
		$selected = ($role->role_id == $selected_value) ? 'selected' : '';
		$ret .= "<option value='{$role->role_id}' $selected>{$role->role_title}</option>";
	}
	$ret .= "</select>";
	return $ret;
}
?>

<md-content flex class="md-padding content-main" ng-init="hideClientMenu = true">
	<md-content class="md-padding md-whiteframe-z1">
		<form role="form" id="info" method="post" action="<?php echo current_url(); ?>">
			<input type="hidden" id="id" name="id" value="<?php echo $id; ?>">
			<input type="hidden" id="action" name="action" value="info">

			<?php $has_error = (strlen(form_error('name')) > 0)? 'has-error' : ''; ?>
			<div class="form-group <?php echo $has_error; ?>">
				<label class="control-label" for="name">Name</label>
				<div class="row">
					<div class="col-md-4">
						<input type="text" class="form-control" id="name" name="name" maxlength="45" value="<?php echo set_value('name', $name); ?>">
					</div>
				</div>
				<?php echo form_error('name', '<span class="help-block">', '</span>'); ?>
			</div>

			<div class="row">
				<div class="col-xs-6">
					<h2>Client &amp; Course Roles</h2>
					<table id="client_course_roles">
						<thead>
							<th>Course/Client</th>
							<th>Role</th>
						</thead>
						<tbody>
							<?php
							$current = '';
							foreach ($all_clients as $client) {
								$selected_role = isset($client_roles[$client->client_id]) ? $client_roles[$client->client_id] : null;
								$markup = roles_dropdown($all_active_roles, "client_roles[{$client->client_id}]", $selected_role);
								echo "<tr><td class='client'>{$client->client_name}</td><td>$markup</td><td></td></tr>";
								foreach ($client->get_courses() as $course) {
									$selected_role = isset($course_roles[$course->course_id]) ? $course_roles[$course->course_id] : null;
									$markup = roles_dropdown($all_active_roles, "course_roles[{$course->course_id}]", $selected_role);
									echo "<tr><td class='course'>{$course->course_title}</td><td>$markup</td></tr>";
								}
								echo "<tr class='spacer'></tr>";
							}
							?>
						</tbody>
					</table>
				</div>
			</div>

			<h2>Group Members</h2>
			<?php
			if (!empty($members) > 0):
			?>
			<ul>
				<?php
				foreach ($members as $user) {
					$url = base_url("user/{$user->user_id}/edit");
					echo "<li><a href='$url'>{$user->first_name} {$user->last_name} ({$user->email})</a></li>";
				}
				?>
			</ul>
			<?php
			else:
			?>
			<p>No members yet!</p>
			<?php
			endif;
			?>
		</form>
	</md-content>
</md-content>

<script type="text/javascript">
	$('form#info #name').focus();
</script>