<md-toolbar class="md-hue-1 header" layout="row" layout-align="center center">
  <div flex layout="row" layout-align="start center" layout-padding layout-margin>
    Administration Area
  </div>
</md-toolbar>

<div flex layout="column" id="main" ng-init="hideClientMenu = true">
    <?php if(!empty($alert)) { ?>
    <div class="alert alert-dismissable alert-<?php echo $alert_type; ?>">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <?php echo $alert_msg; ?>
    </div>
    <?php } ?>

    <md-content layout="column" flex class="md-padding content-main">
        <md-content layout="column" flex class="md-padding">
            <div layout="row" layout-align="center center">
                <md-card>
                    <md-button class="md-raised md-accent" ng-href="{{base_url}}language">Languages</md-button>
                </md-card>

                <md-card>
                    <md-button class="md-raised md-accent" ng-href="{{base_url}}roles">User Roles</md-button>
                </md-card>

                <md-card>
                    <md-button class="md-raised md-accent" ng-href="{{base_url}}user">Users</md-button>
                </md-card>

                <md-card>
                    <md-button class="md-raised md-accent" ng-href="{{base_url}}version">Versions</md-button>
                </md-card>
            </div>
        </md-content>
    </md-content>

<div>



</div>