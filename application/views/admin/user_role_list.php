<md-content layout="column" flex class="md-padding content-main" ng-init="hideClientMenu = true" ng-controller="roleCtrl as admin">
  <md-content layout="column" flex class="md-whiteframe-z1">
    <md-toolbar layout="row" class="md-hue-1" layout-padding layout-align="start center">
      <span flex layout-margin>User Roles</span>
      <div layout="row" layout-padding class="controls">
        <md-button class="md-raised md-accent md-hue-3" ng-href="<?php echo current_url() . '/new'; ?>" style="font-size: 14px; text-decoration: none;">
          Add New
        </md-button>
      </div>
    </md-toolbar>
    <md-subheader class="md-primary">
      <div layout="row" layout-align="start center">
        <md-input-container flex class="md-margin">
          <label>Search User Roles</label>
          <md-icon class="material-icons" aria-label="search">search</md-icon>
          <input ng-model="q" aria-label="search">
        </md-input-container>
      </div>
    </md-subheader>
    <md-content class="md-padding">
      <table flex class="table table-hover table-bordered">
        <thead>
          <tr style="cursor: pointer;">
            <th ng-click="this.predicate = 'role_title'; reverse = !reverse">Title</th>
            <th ng-click="this.predicate = 'role'; reverse = !reverse">Code</th>
            <th ng-click="this.predicate = 'role_status'; reverse = !reverse">Status</th>
          </tr>
        </thead>
        <tbody>
          <tr ng-repeat="item in roles | orderBy:this.predicate:reverse | filter:q" ng-click="admin.goTo(base_url + 'role/' + item.role_id + '/edit')" style="cursor: pointer;">
            <td>{{item.role_title}}</td>
            <td>{{item.role}}</td>
            <td>{{item.role_status|statusName}}</td>
          </tr>
        </tbody>
      </table>
    </md-content>
  </md-content>
</md-content>
