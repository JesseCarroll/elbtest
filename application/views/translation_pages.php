<md-content flex layout="column" ng-controller="courseTranslate">
	<md-content flex ng-class="{'rtl':rtl}" class="md-padding">

		<md-content ng-switch="item.type" ng-repeat="item in page_data.keys" layout="column">

			<md-content class="md-whiteframe-1dp md-margin translation-group">
				<md-subheader class="md-primary md-no-sticky">{{item.key | capitalize | title}}</md-subheader>

				<!-- BACKGROUNDS -->
				<md-content layout="row" class="translation-row" ng-switch-when="background">
					<div flex="50">
						<background-object
							ng-model							= "page_data.data[item.key]"
							hide-non-translatable =	"true"
							editable							= "false"
						></background-object>
					</div>
					<div flex="50">
						<background-object
							ng-model							= "page_data.translation[item.key]"
							hide-non-translatable = "true"
							translation						= "shortcode"
							editable							= "true"
						></background-object>
					</div>
				</md-content>

				<!-- CATEGORIES -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="categories">
					<md-content ng-if="value.title" layout="column" ng-repeat="(key, value) in page_data.data['categories']" class="md-whiteframe-1dp md-margin">
						<md-subheader class="subheading md-no-sticky">Title</md-subheader>
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{value.title}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation.categories[key].title" class="form-control">
							</div>
						</md-content>
						<div ng-if="value.text">
							<md-subheader class="subheading md-no-sticky">Text</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<div ng-bind-html="value.text"></div>
								</div>
								<div flex="50" class="md-padding">
									<textarea ck-editor ng-model="page_data.translation.categories[key].text" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
								</div>
							</md-content>
						</div>
						<div ng-if="value.media && value.media.length > 0">
							<md-subheader class="subheading md-no-sticky">Media</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<add-media-controls
										existing-media-item-list = "value.media"
										disable-add              = "true"
										disable-decorations      = "true"
										expand-all               = "true"
										hide-non-translatable    = "true"
										editable                 = "false"
										internal-flex            = "100"
									></add-media-controls>
								</div>
								<div flex="50" class="md-padding">
									<add-media-controls
										existing-media-item-list = "page_data.translation.categories[key].media"
										disable-add              = "true"
										disable-decorations      = "true"
										expand-all               = "true"
										hide-non-translatable    = "true"
										editable                 = "true"
										internal-flex            = "100"
										translation              = "shortcode"
									></add-media-controls>
								</div>
							</md-content>
						</div>
					</md-content>
				</md-content>

				<!-- CMS CODE -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="cmsCode">
					<md-content layout="column" class="md-whiteframe-1dp md-margin">
						<md-subheader class="subheading md-no-sticky">Title</md-subheader>
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{page_data.data.title}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation.title" class="form-control">
							</div>
						</md-content>
						<div ng-if="page_data.data.text">
							<md-subheader class="subheading md-no-sticky">Text</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<div ng-bind-html="page_data.data.text"></div>
								</div>
								<div flex="50" class="md-padding">
									<textarea ck-editor ng-model="page_data.translation.text" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
								</div>
							</md-content>
						</div>
					</md-content>
				</md-content>

				<!-- COMMON TERMS -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="common terms">
					<common-terms ng-model="page_data.data" common_term_trans="page_data.translation" shortcode="shortcode"></common-terms>
				</md-content>
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="common_terms">
					<common-terms ng-model="page_data.data" common_term_trans="page_data.translation" shortcode="shortcode" page-terms="page_data.data['common_terms']"></common-terms>
				</md-content>

				<!-- DEFAULT: simple text input -->
				<md-content layout="row" class="translation-row" ng-switch-default>
					<div flex="50" layout="row" layout-align="start center" class="md-padding">
						<span>{{page_data.data[item.key]}}</span>
					</div>
					<div flex="50" class="md-padding">
						<input flex type="text" ng-model="page_data.translation[item.key]" class="form-control">
					</div>
				</md-content>

				<!-- GLOSSARY -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="glossary">
					<md-content ng-if="value.term" layout="column" ng-repeat="(key, value) in page_data.data" class="md-whiteframe-1dp md-margin">
						<md-subheader class="subheading md-no-sticky">Term</md-subheader>
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{value.term}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation[key].term" class="form-control">
							</div>
						</md-content>
						<div ng-if="value.definition">
							<md-subheader class="subheading md-no-sticky">Definition</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<div ng-bind-html="value.definition"></div>
								</div>
								<div flex="50" class="md-padding">
									<textarea ck-editor ng-model="page_data.translation[key].definition" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
								</div>
							</md-content>
						</div>
					</md-content>
				</md-content>

				<!-- INLINE AUDIO -->
				<md-content layout="row" class="translation-row" ng-switch-when="audio">
					<div flex="50" class="md-padding" layout="row" layout-align="start center">
						<audio controls style="width: 100%;">
							<source ng-src="{{audioPath + page_data.data[item.key]}}" preload="auto" />
						</audio>
					</div>
					<div flex="50" class="md-padding" layout="row" layout-align="start center">
						<audio controls ng-if="page_data.translation[item.key]" style="width: 100%;">
							<source ng-src="{{audioPath + page_data.translation[item.key]}}" type="audio/mp3" />
						</audio>
						<button ng-hide="page_data.translation[item.key]" class="md-button" trigger-upload="uploadFile($file, 'page_data.translation[\'audio\']', {classification: 'audio'})">Upload</button>
						<md-button ng-show="page_data.translation[item.key]" ng-click="page_data.translation[item.key] = ''">Delete</md-button>
					</div>
				</md-content>

				<!-- INTERACTIONS -->
				<md-content layout="row" class="translation-row" ng-switch-when="interaction">
					<div flex="50" class="md-padding">
						<div ng-repeat="interaction in page_data.data[item.key]" ng-init="parent = page_data.data[item.key]; hideControls = true; disableFields = true;" ng-include="base_url + 'angular/add_translation_interaction.php'"></div>
					</div>
					<div flex="50" class="md-padding">
						<div ng-repeat="interaction in page_data.translation[item.key]" ng-init="hideControls = true; noTranslate = true; mediaControlsTranslationShortcode = shortcode;" ng-include="base_url + 'angular/add_translation_interaction.php'"></div>
					</div>
				</md-content>

				<!-- ITEMS -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="items">
					<md-content layout="column" ng-repeat="(key, value) in page_data.data['items']" class="md-whiteframe-1dp md-margin">
						<md-subheader class="subheading md-no-sticky">Title</md-subheader>
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{value.title}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation.items[key].title" class="form-control">
							</div>
						</md-content>
						<div ng-if="value.text">
							<md-subheader class="subheading md-no-sticky">Text</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<div ng-bind-html="value.text"></div>
								</div>
								<div flex="50" class="md-padding">
									<textarea ck-editor ng-model="page_data.translation.items[key].text" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
								</div>
							</md-content>
						</div>
						<div ng-if="value.media && value.media.length > 0">
							<md-subheader class="subheading md-no-sticky">Media</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<add-media-controls
										existing-media-item-list = "value.media"
										disable-add              = "true"
										disable-decorations      = "true"
										expand-all               = "true"
										hide-non-translatable    = "true"
										editable                 = "false"
										internal-flex            = "100"
									></add-media-controls>
								</div>
								<div flex="50" class="md-padding">
									<add-media-controls
										existing-media-item-list = "page_data.translation.items[key].media"
										disable-add              = "true"
										disable-decorations      = "true"
										expand-all               = "true"
										hide-non-translatable    = "true"
										editable                 = "true"
										internal-flex            = "100"
										translation              = "shortcode"
									></add-media-controls>
								</div>
							</md-content>
						</div>
						<div ng-if="value.correct">
							<md-subheader class="subheading md-no-sticky">Correct</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<span>{{value.correct}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<input flex type="text" ng-model="page_data.translation.items[key].correct" class="form-control">
								</div>
							</md-content>
						</div>
						<div ng-if="value.incorrect">
							<md-subheader class="subheading md-no-sticky">Incorrect</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<span>{{value.incorrect}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<input flex type="text" ng-model="page_data.translation.items[key].incorrect" class="form-control">
								</div>
							</md-content>
						</div>
						<div ng-if="value.final">
							<md-subheader class="subheading md-no-sticky">Final Incorrect</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<span>{{value.final}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<input flex type="text" ng-model="page_data.translation.items[key].final" class="form-control">
								</div>
							</md-content>
						</div>
					</md-content>
				</md-content>

				<!-- MEDIA -->
				<md-content layout="row" class="translation-row" ng-switch-when="media">
					<div flex="50" layout="column" class="md-padding">
						<add-media-controls
							existing-media-item-list = "page_data.data[item.key]"
							disable-add              = "true"
							disable-decorations      = "true"
							expand-all               = "true"
							hide-non-translatable    = "true"
							editable                 = "false"
							internal-flex            = "100"
						></add-media-controls>
					</div>
					<div flex="50" layout="column" class="md-padding">
						<add-media-controls
							existing-media-item-list = "page_data.translation[item.key]"
							disable-add              = "true"
							disable-decorations      = "true"
							expand-all               = "true"
							hide-non-translatable    = "true"
							editable                 = "true"
							internal-flex            = "100"
							translation              = "shortcode"
						></add-media-controls>
					</div>
				</md-content>

				<!-- QUESTIONS -->
				<md-content layout="row" class="translation-row" ng-switch-when="question">
					<div flex="50" class="md-padding">
						<div ng-repeat="question in page_data.data[item.key]" ng-init="parent = page_data.data[item.key]; noTranslate = true; disableFields = true; translation = false;" ng-include="base_url + 'angular/add_translation_question.php'"></div>
					</div>
					<div flex="50" class="md-padding">
						<div ng-repeat="question in page_data.translation[item.key]" ng-init="parent = page_data.translation[item.key]; noTranslate = true; mediaControlsTranslationShortcode = shortcode;" ng-include="base_url + 'angular/add_translation_question.php'"></div>
					</div>
				</md-content>

				<!-- QUIZ SUMMARY -->
				<md-content layout="column" ng-switch-when="quizSummary">
					<md-content layout="column" ng-repeat="(key, object) in page_data.data['summary']" class="md-whiteframe-1dp md-margin">
						<md-subheader class="subheading md-no-sticky">{{key | capitalize}}</md-subheader>
						<md-content layout="column" class="md-whiteframe-1dp md-margin" ng-if="object.title">
							<md-subheader class="subheading md-no-sticky">Title</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<span>{{object.title}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<input flex type="text" ng-model="page_data.translation.summary[key].title" class="form-control">
								</div>
							</md-content>
							<div ng-if="object.text">
								<md-subheader class="subheading md-no-sticky">Text</md-subheader>
								<md-content layout="row" class="translation-row">
									<div flex="50" class="md-padding">
										<div ng-bind-html="object.text"></div>
									</div>
									<div flex="50" class="md-padding">
										<textarea ck-editor ng-model="page_data.translation.summary[key].text" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
									</div>
								</md-content>
							</div>
							<div ng-if="object.media && object.media.length > 0">
								<md-subheader class="subheading md-no-sticky">Media</md-subheader>
								<md-content layout="row" class="translation-row">
									<div flex="50" class="md-padding">
										<add-media-controls
											existing-media-item-list = "object.media"
											disable-add              = "true"
											disable-decorations      = "true"
											expand-all               = "true"
											hide-non-translatable    = "true"
											editable                 = "false"
											internal-flex            = "100"
										></add-media-controls>
									</div>
									<div flex="50" class="md-padding">
										<add-media-controls
											existing-media-item-list = "page_data.translation.summary[key].media"
											disable-add              = "true"
											disable-decorations      = "true"
											expand-all               = "true"
											hide-non-translatable    = "true"
											editable                 = "true"
											internal-flex            = "100"
											translation              = "shortcode"
										></add-media-controls>
									</div>
								</md-content>
							</div>
						</md-content>
						<md-content layout="column" ng-repeat="(subKey, value) in object" class="md-whiteframe-1dp md-margin" ng-if="!object.title">
							<div ng-if="value.title">
								<md-subheader class="subheading md-no-sticky">Title</md-subheader>
								<md-content layout="row" class="translation-row">
									<div flex="50" layout="row" layout-align="start center" class="md-padding">
										<span>{{value.title}}</span>
									</div>
									<div flex="50" layout="row" layout-align="start center" class="md-padding">
										<input flex type="text" ng-model="page_data.translation.summary[key][subKey].title" class="form-control">
									</div>
								</md-content>
							</div>
							<div ng-if="value.text">
								<md-subheader class="subheading md-no-sticky">Text</md-subheader>
								<md-content layout="row" class="translation-row">
									<div flex="50" class="md-padding">
										<div ng-bind-html="value.text"></div>
									</div>
									<div flex="50" class="md-padding">
										<textarea ck-editor ng-model="page_data.translation.summary[key][subKey].text" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
									</div>
								</md-content>
							</div>
							<div ng-if="value.media && value.media.length > 0">
								<md-subheader class="subheading md-no-sticky">Media</md-subheader>
								<md-content layout="row" class="translation-row">
									<div flex="50" class="md-padding">
										<add-media-controls
											existing-media-item-list = "value.media"
											disable-add              = "true"
											disable-decorations      = "true"
											expand-all               = "true"
											hide-non-translatable    = "true"
											editable                 = "false"
											internal-flex            = "100"
										></add-media-controls>
									</div>
									<div flex="50" class="md-padding">
										<add-media-controls
											existing-media-item-list = "page_data.translation.summary[key][subKey].media"
											disable-add              = "true"
											disable-decorations      = "true"
											expand-all               = "true"
											hide-non-translatable    = "true"
											editable                 = "true"
											internal-flex            = "100"
											translation              = "shortcode"
										></add-media-controls>
									</div>
								</md-content>
							</div>
						</md-content>
					</md-content>
				</md-content>

				<!-- RESOURCES -->
				<md-content layout="column" class="md-whiteframe-1dp" ng-switch-when="resources">
					<md-content layout="column" ng-repeat="item in page_data.data" class="md-whiteframe-1dp md-margin">
						<div ng-if="item.label">
							<md-subheader class="subheading md-no-sticky">Label</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<span>{{item.label}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding">
									<input flex type="text" ng-model="page_data.translation[$index].label" class="form-control">
								</div>
							</md-content>
						</div>
						<div ng-if="item.description && item.description !== ''">
							<md-subheader class="subheading md-no-sticky">Description</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding">
									<div ng-bind-html="item.description"></div>
								</div>
								<div flex="50" class="md-padding">
									<textarea ck-editor ng-model="page_data.translation[$index].description" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
								</div>
							</md-content>
						</div>
						<div ng-if="item.external && item.src">
							<md-subheader class="subheading md-no-sticky">External URL</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding" layout="row" layout-align="start center">
									<span>{{item.src}}</span>
								</div>
								<div flex="50" class="md-padding">
									<input flex type="text" ng-model="page_data.translation[$index].src" class="form-control">
								</div>
							</md-content>
						</div>
						<div ng-if="!item.external && item.src">
							<md-subheader class="subheading md-no-sticky">File Upload</md-subheader>
							<md-content layout="row" class="translation-row">
								<div flex="50" class="md-padding" layout="row" layout-align="start center">
									<span>{{item.src}}</span>
								</div>
								<div flex="50" layout="row" layout-align="start center" class="md-padding resource">
									<md-button ng-if="!disableFields" class="md-icon-button">
										<md-icon class="material-icons" trigger-upload="uploadFile($file, 'page_data.translation[{{$index}}]', {classification: 'resources'})">add</md-icon>
									</md-button>
									<add-text flex ng-if="page_data.translation[$index].src" ng-model="page_data.translation[$index].src" data-label="File Name" data-disabled="true"></add-text>
									<add-text flex="10" ng-if="page_data.translation[$index].type" ng-model="page_data.translation[$index].type" data-label="Type" data-disabled="true"></add-text>
									<add-text flex="10" ng-if="page_data.translation[$index].size" ng-model="page_data.translation[$index].size" data-label="Size (kb)" data-disabled="true"></add-text>
									<span ng-click="removeResource(page_data.translation[$index])"><i ng-if="page_data.translation[$index].src" class="fa fa-trash-o button" style="width: auto"></i></span>
								</div>
							</md-content>
						</div>
					</md-content>
				</md-content>

				<!-- ROLES -->
				<md-content ng-if="page_data.title == 'Roles'" layout="column" class="md-whiteframe-1dp" ng-switch-when="roles">
					<md-content layout="column" ng-repeat="item in page_data.data" class="md-whiteframe-1dp md-margin">
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{item.name}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation[$index].name" class="form-control">
							</div>
						</md-content>
					</md-content>
				</md-content>

				<!-- TEXT AREA -->
				<md-content layout="row" class="translation-row" ng-switch-when="textarea">
					<div flex="50" class="md-padding">
						<div ng-bind-html="page_data.data[item.key]"></div>
					</div>
					<div flex="50" class="md-padding">
						<textarea ck-editor ng-model="page_data.translation[item.key]" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>
					</div>
				</md-content>

			</md-content>

			<md-content class="translation-row" ng-switch-when="dropzones" layout="row">
				<md-content flex="50" class="md-padding">
					<div ng-repeat="dropzone in page_data.data['dropzones']" ng-init="parent = page_data.data['dropzones']; disableFields = true" ng-include="base_url + 'angular/add_translation_dropzone.php'"></div>
				</md-content>
				<md-content flex="50" class="md-padding">
					<div ng-repeat="dropzone in page_data.translation['dropzones']" ng-init="parent = page_data.data['dropzones'];" ng-include="base_url + 'angular/add_translation_dropzone.php'"></div>
				</md-content>
			</md-content>

			<md-content ng-show="page_data.data[item.key]" class="translation-row" ng-switch-when="image" layout="row">
				<md-content flex="50" class="md-padding">
					<div class="preview-content">
						<img style="width: 100%;" ng-src="{{imagePath + page_data.data[item.key]}}">
					</div>
				</md-content>
				<md-content flex="50" class="md-padding" ng-init="page_data.translation[item.key] = (page_data.translation[item.key]) ? page_data.translation[item.key] : page_data.data[item.key]">
					<div class="preview-content">
						<img style="width: 100%;" ng-src="{{imagePath + page_data.translation[item.key]}}">
						<md-content layout="column" class="media-buttons md-whiteframe-z1 fileinput fileupload" data-provides="fileinput">
							<input  class="upload" type="file" file-model="myFile">
							<span data-trigger="fileinput"><i class="fa fa-image" data-model="page_data.translation[item.key]"></i></span>
							<span ng-click="page_data.translation[item.key] = ''"><i ng-if="page_data.translation[item.key]" class="fa fa-trash-o"></i></span>
						</md-content>
					</div>
				</md-content>
			</md-content>

			<md-content ng-show="page_data.data[item.key].length > 0" class="translation-row" ng-switch-when="extra" layout="row">
				<md-content flex="50" class="md-padding">
					<div ng-repeat="extra in page_data.data[item.key]" ng-init="hideControls = true; disableFields = true;" ng-include="base_url + 'angular/add_extra.php'"></div>
				</md-content>
				<md-content flex="50" class="md-padding">
					<div ng-repeat="extra in page_data.translation[item.key]" ng-init="hideControls = true; noTranslate = true" ng-include="base_url + 'angular/add_extra.php'"></div>
				</md-content>
			</md-content>

			<md-content ng-show="page_data.data.image" class="translation-row" ng-switch-when="certificate" layout="row">
				<md-content flex="50" class="md-padding">
					<div class="preview-content">
						<img style="width: 100%;" ng-src="{{imagePath + page_data.data.image}}">
					</div>
				</md-content>

				<md-content flex="50" class="md-padding" ng-init="page_data.translation.image = (page_data.translation.image) ? page_data.translation.image : page_data.data.image">
					<div class="preview-content">
						<img style="width: 100%;" ng-src="{{imagePath + page_data.translation.image}}">
						<md-content layout="column" class="media-buttons md-whiteframe-z1 fileinput fileupload" data-provides="fileinput">
							<input class="upload" type="file" file-model="myFile">
							<span data-trigger="fileinput"><i class="fa fa-image" data-model="page_data.translation.image"></i></span>
							<span ng-click="page_data.translation.image=''"><i ng-if="page_data.translation.image" class="fa fa-trash-o"></i></span>
						</md-content>
					</div>
				</md-content>
			</md-content>

		</md-content>
		<div id="busy-wrapper" style="display:none;">
			<div id="ajax-busy">Please wait...</div>
		</div>
	</md-content>

	<md-content ng-if="page_data" layout="row" class="md-margin">
    <md-button ng-hide="firstPage()" ng-click="prevPage()" class="md-raised md-accent">Previous</md-button>
    <div flex></div>
    <md-button ng-hide="lastPage()" ng-click="nextPage()" class="md-raised md-primary btn-next-page">Next</md-button>
	</md-content>
</md-content>