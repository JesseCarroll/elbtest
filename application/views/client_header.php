<!DOCTYPE html>
<html lang="en">

<?php include_once('client_head.php'); ?>

<body layout="column" ng-controller="chameleonCtrl" ng-cloak>
	<md-toolbar id="header" layout="row" layout-align="start center" class="md-whiteframe-z2 md-hue-1">
		<div class="home">
			<a hide-xs hide-gt-xs show-gt-md ng-href="{{base_url}}client"><img ng-src="{{aws_path}}core/images/logo.png" /></a>
			<md-button aria-label="toggle-menu" hide-gt-md ng-click="toggle('left')"><i class="fa fa-bars"></i></md-button>
		</div>
		<md-menu class="client" ng-if="editor.client">
		 	<div layout="row" layout-align="start center" layout-fill>
				<a ng-href="{{base_url}}client/{{client.client_id}}">{{client.client_name}}</a>
				<md-icon class="material-icons" ng-click="openMenu($mdOpenMenu, $event)">arrow_drop_down</md-icon>
			</div>
			<md-menu-content width="4">
				<md-menu-item ng-repeat="i in allowed_clients">
					<md-button ng-href="{{base_url}}client/{{i.client_id}}">{{i.client_name}}</md-button>
				</md-menu-item>
			</md-menu-content>
		</md-menu>
		<md-menu class="course" ng-if="course.course_title">
			<div layout="row" layout-align="start center" layout-fill>
				{{course.course_title}}
				<md-icon class="material-icons" ng-click="openMenu($mdOpenMenu, $event)">arrow_drop_down</md-icon>
			</div>
			<md-menu-content width="6">
				<md-menu-item ng-repeat="i in allowed_courses" ng-if="i.client_id == client.client_id">
					<md-button ng-click="courseChangeConfirm($event, i)">{{i.course_title}}</md-button>
				</md-menu-item>
			</md-menu-content>
		</md-menu>
		<md-menu class="course" ng-if="theme_id">
			<div layout="row" layout-align="start center" layout-fill>
				Themes: {{theme_id | capitalize}}
				<md-icon class="material-icons" ng-click="openMenu($mdOpenMenu, $event)">arrow_drop_down</md-icon>
			</div>
			<md-menu-content width="6">
				<md-menu-item ng-repeat="i in theme_list">
					<md-button ng-href="{{base_url}}client/{{client.client_id}}/theme/{{i}}">{{i | capitalize}}</md-button>
				</md-menu-item>
			</md-menu-content>
		</md-menu>
		<div class="header-text" ng-if="course.state == 'translate' && language_description">{{language_description}} Translation</div>
		<div flex></div>
		<div layout="row" class="controls" ng-if="pageControls" ng-cloak>
			<div ng-repeat="control in pageControls" ng-if="control.permission.success">
				<md-button ng-if="control.type == 'single'" class="md-raised md-primary md-hue-2" ng-click="control.action()">{{control.name}}</md-button>
				<md-menu ng-if="control.type == 'multi'">
					<div layout="row" layout-align="center center">
						<md-button ng-click="openMenu($mdOpenMenu, $event)" class="md-raised md-primary md-hue-2">
							<span>
								{{control.name}}
								<md-icon class="material-icons">arrow_drop_down</md-icon>
							</span>
						</md-button>
					</div>
					<md-menu-content width="2">
						<md-menu-item ng-repeat="subcontrol in control.options">
							<md-button ng-if="subcontrol.href" class="md-button" ng-href="{{subcontrol.href}}">{{subcontrol.name}}</md-button>
							<md-button ng-if="subcontrol.action && !subcontrol.href" class="md-button" ng-click="subcontrol.action(subcontrol.argument)">{{subcontrol.name}}</md-button>
						</md-menu-item>
				</md-menu>
			</div>
		</div>

		<div class="changelog"></div>
		<?php if ($current_user->can('admin')) { ?>
		<md-menu>
			<div layout="row" layout-align="center center">
				<md-button style="margin-right:0" class="md-icon-button material-icons" ng-click="openMenu($mdOpenMenu, $event)">build</md-button>
				<md-icon style="cursor: pointer" ng-click="openMenu($mdOpenMenu, $event)" class="material-icons">arrow_drop_down</md-icon>
			</div>
			<md-menu-content width="4">
				<md-menu-item>
					<md-button class="md-button" ng-href="{{base_url}}language">Languages</md-button>
				</md-menu-item>
				<md-menu-item>
					<md-button class="md-button" ng-href="{{base_url}}roles">Roles</md-button>
				</md-menu-item>
				<md-menu-item>
					<md-button class="md-button" ng-href="{{base_url}}group">Groups</md-button>
				</md-menu-item>
				<md-menu-item>
					<md-button class="md-button" ng-href="{{base_url}}user">Users</md-button>
				</md-menu-item>
				<md-menu-item>
					<md-button class="md-button" ng-href="{{base_url}}version">Versions</md-button>
				</md-menu-item>
			</md-menu-content>
		</md-menu>
		<?php } ?>

		<md-menu>
			<md-list style="padding:0">
				<md-list-item>
					<img style="margin:0" aria-label="Profile/Logout" class="md-avatar" ng-click="openMenu($mdOpenMenu, $event)" class="md-avatar" src="//www.gravatar.com/avatar/<?php echo $current_user->gravitar_hash ?>?d=mm">
					<md-icon style="cursor: pointer" ng-click="openMenu($mdOpenMenu, $event)" class="material-icons">arrow_drop_down</md-icon>
				</md-list-item>
			</md-list>
			<md-menu-content width="4">
				<md-menu-item>
					<md-button ng-href="{{base_url}}user/profile">Profile</md-button>
				</md-menu-item>
				<md-menu-item>
					<a class="md-button" ng-href="{{base_url}}ajax/logout">Logout</a>
				</md-menu-item>
			</md-menu-content>
		</md-menu>
	</md-toolbar>
	<div flex layout="row">
		<md-sidenav ng-if="showSidenav" id="sidenav" md-theme="grey" class="md-sidenav-left md-whiteframe-z1" md-component-id="left" md-is-locked-open="$mdMedia('gt-md')">
			<ul id="menu" flex ng-controller="navigationCtrl as nav">
				<li class="details" ng-if="!editor.course">
					<ul>
						<li><md-button ng-class="{'active' : nav.selectedSection('courses', true)}" ng-click="nav.selectPage('courses', true)" class="md-primary md-raised md-hue-2">Courses</md-button></li>
						<li><md-button ng-class="{'active' : nav.selectedSection('curriculum', true)}" ng-click="nav.selectPage('curriculum', true)" class="md-primary md-raised md-hue-2">Curriculum</md-button></li>
						<li><md-button ng-class="{'active' : nav.selectedSection('player', true)}" ng-click="nav.selectPage('player', true)" class="md-primary md-raised md-hue-2">Player</md-button></li>
						<li>
							<md-button ng-class="{'active' : nav.selectedSection('theme', true)}" ng-click="nav.selectPage('theme', true)" class="md-primary md-raised md-hue-2">Theme</md-button>
							<div ng-if="theme_loaded">
								<div class="theme-list list">
									<div class="theme-group group">
										<div layout="row" layout-align="start center" class="theme-item item" ng-class="{'active' : theme_section == 'ui'}">
											<span flex class="list-title" title="Global UI" ng-click="nav.editThemeSection('ui')">Global UI</span>
										</div>
										<div layout="row" layout-align="start center" class="theme-item item" ng-class="{'active' : theme_section == 'color'}">
											<span flex class="list-title" title="Brand Color" ng-click="nav.editThemeSection('color')">Brand Color</span>
										</div>
										<div layout="row" layout-align="start center" class="theme-item item" ng-class="{'active' : theme_section == 'type'}">
											<span flex class="list-title" title="Type" ng-click="nav.editThemeSection('type')">Type</span>
										</div>
										<div layout="row" layout-align="start center" class="theme-item item" ng-class="{'active' : theme_section == 'assets'}">
											<span flex class="list-title" title="Brand Assets" ng-click="nav.editThemeSection('assets')">Brand Assets</span>
										</div>
										<div layout="row" layout-align="start center" class="theme-item item" ng-class="{'active' : theme_section == 'elements'}">
											<span flex class="list-title" title="Elements" ng-click="nav.editThemeSection('elements')">Elements</span>
										</div>
									</div>
								</div>
							</div>
						</li>
						<li><md-button ng-class="{'active' : nav.selectedSection('settings', true)}" ng-click="nav.selectPage('settings', true)" class="md-primary md-raised md-hue-2">Settings</md-button></li>
						<li><md-button ng-class="{'active' : nav.selectedSection('analytics', true)}" ng-click="nav.selectPage('analytics', true)" class="md-primary md-raised md-hue-2">Analytics</md-button></li>
					</ul>
				</li>
				<li class="details" ng-show="editor.course">
					<ul>
						<div ng-if="course.state == 'author' && course.locked == 0">
							<li><md-button ng-class="{'active' : nav.selectedSection('detail')}" ng-click="nav.selectPage('detail')" class="md-primary md-raised md-hue-2">Course Details</md-button></li>
							<li><md-button ng-class="{'active' : nav.selectedSection('home')}" ng-click="nav.selectPage('home')" class="md-primary md-raised md-hue-2">Homepage</md-button></li>
							<li>
								<md-button md-no-ink ng-class="{'active' : nav.selectedSection('topics')}" ng-click="nav.selectPage('topics')" class="md-primary md-raised md-hue-2">Topics</md-button>
								<topic-navigation ng-if="nav.selectedSection('topics')" topic="data.topic"></topic-navigation>
							</li>
							<li ng-if="data.glossary">
								<md-button layout="row" md-no-ink ng-class="{'active' : nav.selectedSection('glossary')}" ng-click="nav.selectPage('glossary')" class="md-primary md-raised md-hue-2">
									Glossary
									<div ng-if="nav.selectedSection('glossary')" class="inline-search" flex>
										<input ng-class="{'active': showSearch}" ng-model="nav.glossary_filter" aria-label="search">
										<md-icon class="material-icons" aria-label="search" ng-click="nav.toggleSearch()">search</md-icon>
									</div>
								</md-button>
								<div ng-if="nav.selectedSection('glossary')">
									<div ui-sortable="tn.glossarySortingOptions" ng-model="data.glossary" class="glossary-list list">
										<div ng-repeat="glossary in data.glossary | filter:nav.glossary_filter" class="glossary-group group">
											<div layout="row" layout-align="start center" class="glossary-item item" ng-class="{'active': glossaryActive($index)}">
												<div class="handle" aria-label="Drag to Sort">
													<md-tooltip md-direction="top" md-delay="1000" md-autohide="true">Drag to Sort</md-tooltip>
													<md-icon class="material-icon">more_vert</md-icon>
												</div>
												<span flex class="list-title" title="{{glossary.term}}" ng-click="nav.editSection('glossary', $index)">{{glossary.term || 'click to add glossary term'}}</span>
												<remove-buttons ng-model="data.glossary" index="$index"></remove-buttons>
											</div>
										</div>
									</div>
									<div class="glossary-item item add"><md-button ng-click="addItem(data.glossary, 'glossary')">Add New Term</button></div>
								</div>
							</li>
							<li ng-if="data.resources">
								<md-button layout="row" md-no-ink ng-class="{'active' : nav.selectedSection('resources')}" ng-click="nav.selectPage('resources')" class="md-primary md-raised md-hue-2">
									Resources
									<div ng-if="nav.selectedSection('resources')" class="inline-search" flex>
										<input ng-class="{'active': showSearch}" ng-model="nav.resource_filter" aria-label="search">
										<md-icon class="material-icons" aria-label="search" ng-click="nav.toggleSearch()">search</md-icon>
									</div>
								</md-button>
								<div ng-if="nav.selectedSection('resources')">
									<div ui-sortable="tn.glossarySortingOptions" ng-model="data.resources" class="resource-list list">
										<div ng-repeat="resources in data.resources | filter:nav.resource_filter" class="resource-group group">
											<div layout="row" layout-align="start center" class="resource-item item" ng-class="{'active': glossaryActive($index)}">
												<div class="handle" aria-label="Drag to Sort">
													<md-tooltip md-direction="top" md-delay="1000">Drag to Sort</md-tooltip>
													<md-icon class="material-icon">more_vert</md-icon>
												</div>
												<span flex class="list-title" title="{{resources.label}}" ng-click="nav.editSection('resources', $index)">{{resources.label || 'click to add resource item'}}</span>
												<img ng-src="{{aws_path}}core/icons/{{resources.type || 'generic'}}.svg" style="height: 65%">
												<remove-buttons ng-model="data.resources" index="$index"></remove-buttons>
											</div>
										</div>
									</div>
									<div class="resource-item item add"><md-button ng-click="addItem(data.resources, 'resources')">Add New Resource</button></div>
								</div>
							</li>
							<li><md-button ng-class="{'active' : nav.selectedSection('terms')}" ng-click="nav.selectPage('terms')" class="md-primary md-raised md-hue-2">Common Terms</md-button></li>
							<li ng-if="data.preTest"><md-button layout="row" ng-class="{'active' : nav.selectedSection('pretest')}" ng-click="nav.selectPage('pretest')" class="md-primary md-raised md-hue-2">Pre-Test</md-button></li>
							<li ng-if="data.certificate"><md-button ng-class="{'active' : nav.selectedSection('certificate')}" ng-click="nav.selectPage('certificate')" class="md-primary md-raised md-hue-2">Certificate</md-button></li>
							<li ng-if="data.objectives"><md-button ng-class="{'active' : nav.selectedSection('objectives')}" ng-click="nav.selectPage('objectives')" class="md-primary md-raised md-hue-2">Objectives</md-button></li>
							<li ng-if="data.cmsCode"><md-button ng-class="{'active' : nav.selectedSection('cms')}" ng-click="nav.selectPage('cms')" class="md-primary md-raised md-hue-2">CMS Code</md-button></li>
							<li><md-button ng-class="{'active' : nav.selectedSection('assets')}" ng-click="nav.selectPage('assets')" class="md-primary md-raised md-hue-2">Assets</md-button></li>
							<li><md-button ng-class="{'active' : nav.selectedSection('roles')}" ng-click="nav.selectPage('roles')" class="md-primary md-raised md-hue-2">Assign Roles/Regions</md-button></li>
							<li><md-button ng-class="{'active' : nav.selectedSection('settings')}" ng-click="nav.selectPage('settings')" class="md-primary md-raised md-hue-2">Settings</md-button></li>
						</div>
						<div ng-if="course.state == 'translate' && course.locked == 0">
							<li ng-repeat="item in pages">
								<md-button ng-class="{'active' : activeTransPage($index)}" ng-click="translatePage($index)" class="md-primary md-raised md-hue-2">
									<md-tooltip md-delay="1000" md-direction="right" md-autohide="true">{{item.title}}</md-tooltip>
									{{item.title}}
								</md-button>
							</li>
						</div>
					</ul>
				</li>
			</ul>

			<?php if (!empty($course) && $current_user->can('stage', $course->client_id, $course->course_id)) { ?>
			<div ng-if="course" ng-controller="migrationCtrl">
				<ul>
					<li ng-class="{'active': active == 'migration'}">
						<md-button class="md-primary md-raised md-hue-3 course-controls" layout="row" ng-click="toggleMigration($event, 'migration')">
							<span flex>Course Migration</span>
							<md-icon class="material-icons">add_circle</md-icon>
						</md-button>
						<div class="migration-content migration">
							<md-content class="md-padding" layout="column" layout-align="center center">
								<md-button class="md-primary md-raised md-hue-2" ng-click="migrateCourse($event, 'stage')">STAGE</md-button>
								<p>Creates or updates a stable review link</p>
								<md-button class="md-primary md-raised md-hue-2" ng-click="migrateCourse($event, 'prod')" ng-disabled="disablePublish">PUBLISH</md-button>
								<p>Creates or updates a course package</p>
							</md-content>
						</div>
					</li>
					<li ng-class="{'active': active == 'state'}">
						<md-button class="md-primary md-raised md-hue-3 course-controls" layout="row" ng-click="toggleMigration($event, 'state')">
							<span flex>Course State: {{state}}</span>
							<md-icon class="material-icons">add_circle</md-icon>
						</md-button>
						<div class="migration-content state">
							<md-content class="md-padding" layout="column" layout-align="center center">
								<md-button class="md-primary md-raised md-hue-2" ng-click="toggleLock($event)">{{lock_button_text}}</md-button>
								<div layout="row">
									<md-button class="course_state" ng-class="{'active': course.state == 'author'}" ng-click="changeState($event, 'author')" ng-disabled="state == 'locked'"><md-icon class="material-icons">mode_edit</md-icon>Author</md-button>
									<md-button ng-if="course.course_enabled_languages.length>1" class="course_state right" ng-class="{'active': course.state == 'translate'}" ng-click="changeState($event, 'translate')" ng-disabled="state == 'locked'"><md-icon class="material-icons">translate</md-icon>Translate</md-button>
								</div>
								<p>{{state_message[state]}}</p>
							</md-content>
						</div>
					</li>
				</ul>
			</div>
			<?php } ?>
		</md-sidenav>

		<div flex layout="column">
