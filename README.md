# Chameleon Editor

## Setup

1. Change the values for the database in the correct environment under `application/config/database.php`
1. Change your paths and URLs for the install in `application/config/<ENVIRONMENT>/default.php` and `.../config.php`
1. Change `.htaccess` in the root directory to redirect to the correct location for your base URL.
1. ***MAGIC?  TODO FIGURE THIS OUT***
1. Visit the server maintenance page at `<base_url>/manage/maintenance.php` and login to setup the database.

