$(window, document, undefined).ready(function() {

	$('.help, .back').on('click', function() {
		$('.help').toggleClass('fa-question-circle');
		$('.help').toggleClass('fa-arrow-circle-left');
		$('.form-content, .help-content').toggle();
	});

	$('input').blur(function() {
		var $this = $(this);
		if ($this.val())
			$this.addClass('used');
		else
			$this.removeClass('used');
	});

	var $ripples = $('.ripples');

	$ripples.on('click.Ripples', function(e) {

		var $this = $(this);
		var $offset = $this.parent().offset();
		var $circle = $this.find('.ripplesCircle');

		var x = e.pageX - $offset.left;
		var y = e.pageY - $offset.top;

		$circle.css({
			top: y + 'px',
			left: x + 'px'
		});

		$this.addClass('is-active');

	});

	$ripples.on('animationend webkitAnimationEnd mozAnimationEnd oanimationend MSAnimationEnd', function(e) {
		$(this).removeClass('is-active');
	});

});

!function ($) {
	$(function() {
		//post-submit callback
		function postlogincallback(response) {
			try {
				if (response.previous_session) {
					var login_form = $('form#login');
					login_form.find('div.form-content, .help').hide();
					login_form.find('div.clobber-previous-session-prompt').show();
					login_form.find('input#cancel').click(function() {
						login_form.find('div.clobber-previous-session-prompt').hide();
						login_form.find('div.form-content, .help').show();
					});
					login_form.find('input#proceed').click(function() {
						login_form.append('<input type="hidden" name="clobber_previous_session" value="1" />');
						login_form.submit();
					});
					return;
				}

				if (!response.result) {
					throw response.msg;
				}

				window.location = response.msg;
			}
			catch (msg) {
				$('#loginAlert').html(msg);
				$('#loginAlert').show();
				$('form#loginForm #inputPassword').val('');
				$('form#loginForm #inputEmail').focus();
			}
		}

		// bind form using ajaxForm
		var $loginform = $('#login')
		if ($loginform.length > 0) {
			$loginform.ajaxForm({
				dataType: 'json',
				success:  postlogincallback
			});
		}
	});
}(window.jQuery)
