$('md-content.profile').click(function(e) {
	var target = e.target;

	if($(this).hasClass('expanded')) {
		if($(target).is('button.md-fab') || $(target).parents().is('button.md-fab')) {
			$(this).toggleClass('expanded').animate({'right': '-250px'}, 500);
		}
	} else {
		$(this).toggleClass('expanded').animate({'right': '0px'}, 500);
	};
});

$(document).click(function(e) {
	var target = e.target;

	if(!$(target).is('md-content.profile') && !$(target).parents().is('md-content.profile') && $('md-content.profile').hasClass('expanded')) {
		$('md-content.profile').toggleClass('expanded').animate({'right': '-250px'}, 500);
	}
});