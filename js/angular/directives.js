var chameleonDir = angular.module("chameleonDir", []);

/*
 * Combines information about a list of existing media items and allowed media
 * items in order to present controls for viewing and editing existing media
 * items, as well as controls for adding new ones.
 *
 * Params:
 * existing-media-item-list: The list of media items that these media controls
 *                           should manage.
 *        template-settings: (optional) Settings that govern how media may be
 *                           placed into the media item list.
 *              disable-add: (optional) Explicitly prevents us from display
 *                           controls for adding more media items, even if more
 *                           would otherwise be allowed by the template
 *                           settings.
 *      disable-decorations: (optional) Do not include controls for removing
 *                           media items, changing media item type, or toggling
 *                           the expanded state of a media slot.
 *                 editable: (optional) When set to a falsy value, fields
 *                           within media items will be presented in a
 *                           disabled, read-only state.
 *               expand-all: (optional) All media slots will initially be shown
 *                           in their expanded state.
 *    hide-non-translatable: (optional) Not all controls that are part of media
 *                           items contain translatable data. For example, a
 *                           "chart" media item may be one of several preset
 *                           types of charts, and it is not appropriate to
 *                           allow this sort of control to be changed during
 *                           translation. This option hides such controls.
 *              translation: (optional) When provided, this should be set to the
 *                           shortcode of a non-English language.  It indicates
 *                           that this media slot is meant to show media on the
 *                           right hand side of the translation editor.  This
 *                           impacts the way the media slot looks and behaves.
 *             misc-options: (optional) When set to an object, this value is
 *                           passed through to each media slot's scope and can
 *                           be used to influence the behavior of media slots.
 */
chameleonDir.directive('addMediaControls', ['mediaService', '$timeout', function(mediaService, $timeout) {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			existingMediaItemList: '=',
			templateSettings:      '=?',
			disableAdd:            '=?',
			disableDecorations:    '=?',
			editable:              '=?',
			expandAll:             '=?',
			hideNonTranslatable:   '=?',
			internalFlex:          '=?',
			miscOptions:           '=?',
			translation:           '=?',
		},
		link: function(scope, element, attrs) {
			if (angular.isUndefined(scope.editable)) {
				scope.editable = true;
			}
			if (angular.isUndefined(scope.disableAdd)) {
				scope.disableAdd = false;
			}
			if (angular.isUndefined(scope.disableDecorations)) {
				scope.disableDecorations = false;
			}
			if (angular.isUndefined(scope.expandAll)) {
				scope.expandAll = false;
			}
			if (angular.isUndefined(scope.hideNonTranslatable)) {
				scope.hideNonTranslatable = false;
			}
			if (angular.isUndefined(scope.translation)) {
				scope.translation = false;
			}
			if (angular.isUndefined(scope.miscOptions) || 'object' != typeof scope.miscOptions) {
				scope.miscOptions = {};
			}

			// sanity checks
			scope.initError = false;
			try {
				if (angular.isUndefined(scope.existingMediaItemList)) {
					throw 'Existing media item list is undefined!';
				}
			}
			catch (e) {
				console.log(e);
				scope.initError = e;
				return;
			}

			// which slots should be shown in expanded mode?
			scope.expandedSlotList = scope.expandAll ?
				Array(scope.existingMediaItemList.length).fill(true) :
				Array(scope.existingMediaItemList.length).fill(false);

			// initialize a media item with the specified media type ID
			scope.initMediaListObject = function(index, mediaTypeId) {
				if (index == scope.existingMediaItemList.length) {
					scope.expandedSlotList[index] = true;
				}
				mediaService.initMediaListObject(scope.existingMediaItemList, index, mediaTypeId);
			};

			// remove a media item with the specified index, returning true; if
			// "test" is true, instead of removing the media item, only test to see
			// if it is permissible to remove this item, returning true or false
			scope.removeMediaItem = function(index, test) {
				if (test) {
					return scope.canRemoveMediaItem(index);
				}

				scope.existingMediaItemList.splice(index, 1);
				return true;
			};

			/* canRemoveMediaItem()
			 *
			 * If this slot is "predefined" then it can only be removed if it is the
			 * last media item in list.
			 *
			 * "Predefined" means that the template settings give this slot a special
			 * label (see getSlotLabel()).
			 *
			 * Why? When a predefined slot is full and the slot after it is also
			 * full, removing the media item in the predefined slot will cause the
			 * media information from the slot after it to drop down into the
			 * predefined slot. Since the user probably didn't ever intend for the
			 * media information from that next slot to be in the predefined slot, we
			 * prevent this situation by simply not allowing predefined slots to be
			 * removed unless they are the final media item in the list.
			 */
			scope.canRemoveMediaItem = function(index) {
				index = parseInt(index);
				var isLastIndex = ((index + 1) == scope.existingMediaItemList.length);
				var isThisIndexPredefined = (
					angular.isDefined(scope.templateSettings) &&
					angular.isDefined(scope.templateSettings.media) &&
					angular.isDefined(scope.templateSettings.media.items) &&
					index < scope.templateSettings.media.items.length
				);
				return (isLastIndex || !isThisIndexPredefined);
			};

			/*
			 * The template settings may specify allowable media types for specific
			 * media slots; this is not a requirement, but it allows the template
			 * creator greater control over what kinds of media can be inserted into
			 * a specific slot.
			 *
			 * The template settings may also specifc a list of allowable media types
			 * that apply to all media slots that are not governed by a slot-specific
			 * setting.
			 *
			 * If neither of these restrictions are placed into the template
			 * settings, this method will return all media types available.
			 *
			 * In any case, the return value of this method is an object that is
			 * structured as a one-dimensional associative array:
			 * {
			 *   'mediaID1': 'media label 1',
			 *   'mediaID2': 'media label 2',
			 *   ...
			 * }
			 */
			scope.getAllowedMediaTypes = function(index) {
				try {
					var allowedMediaTypeIdList = [];

					// sanity check -- do we have any media slots defined for this template?
					if (
							angular.isUndefined(scope.templateSettings) ||
							angular.isUndefined(scope.templateSettings.media)
					) {
						throw 'No media settings found';
					}
					var mediaSettings = scope.templateSettings.media;

					// slot-specific media type restriction
					var index = parseInt(index);
					if (
						angular.isDefined(mediaSettings.items) &&
						angular.isDefined(mediaSettings.items[index]) &&
						angular.isDefined(mediaSettings.items[index].type)
					) {
						allowedMediaTypeIdList = mediaSettings.items[index].type;
					}

					// all-slot media type restriction
					else if (angular.isDefined(mediaSettings.type)) {
						allowedMediaTypeIdList = scope.templateSettings.media.type;
					}

					// turn the type ID list into an object containing [type ID] -> [type label] pairs and return
					if (allowedMediaTypeIdList.length == 0) {
						throw 'No media type restrictions present for this slot';
					}
					var allowedMediaTypes = {};
					for (k in allowedMediaTypeIdList) {
						var typeId = allowedMediaTypeIdList[k];
						if (angular.isUndefined(typeId)) {
							continue;
						}
						allowedMediaTypes[typeId] = mediaService.getMediaTypeLabel(typeId);
					}
					return allowedMediaTypes;
				}
				catch (e) {}

				return mediaService.getAllMediaTypes();
			};

			/* getSlotLabel()
			 *
			 * In template settings, we have the option of specifying a label for a
			 * media slot.  The label is meant to give the user a hint about how
			 * media placed into the slot will be used in the template.
			 *
			 * For example, calling a slot "Hero Media" suggests that the media item
			 * placed in this slot will be used in a "front and center" manner in the
			 * template.
			 */
			scope.getSlotLabel = function(index) {
				try {
					var index = parseInt(index);

					if (
						angular.isUndefined(scope.templateSettings) ||
						angular.isUndefined(scope.templateSettings.media)
					) {
						throw 'No media settings present for template';
					}
					var mediaSettings = scope.templateSettings.media;

					if (angular.isUndefined(mediaSettings.items)) {
						throw 'No predefined slots present in template';
					}
					if (angular.isUndefined(mediaSettings.items[index])) {
						throw 'No predefined slot information for slot ' + index;
					}
					if (angular.isUndefined(mediaSettings.items[index].label)) {
						throw 'No label specified for predefined slot ' + index;
					}

					return mediaSettings.items[index].label;
				}
				catch (e) {
					return 'Media';
				}
			};

      /*
			 * Return true if controls for adding a new media slot should be shown.
			 */
			scope.showNewMediaSlot = function() {
				return (
					scope.editable &&
					!scope.disableAdd &&
					(
						angular.isUndefined(scope.templateSettings) ||
						angular.isUndefined(scope.templateSettings.media) ||
						angular.isUndefined(scope.templateSettings.media.limit) ||
						scope.templateSettings.media.limit < 1 ||
						scope.templateSettings.media.limit > scope.existingMediaItemList.length
					)
				);
			};

			/*
			 * Governs whether or not a given media slot should be shown.  We always
			 * show a media slot that has a media item in it, but we may not show
			 * existing media slots that are empty, depending on options that were
			 * passed in to this directive.
			 */
			scope.showExistingMediaSlot = function(index) {
				return (
					angular.isDefined(scope.existingMediaItemList[index]) &&
					(
						angular.isDefined(scope.existingMediaItemList[index].type) ||
						scope.hideNonTranslatable == false
					)
				);
			};

			/*
			 * AngularJS tends to re-use markup created by directives whenever
			 * possible.  One consequence of this is that initialization code may not
			 * always be run when we expect it to be run.  This forces some of the
			 * markup in this directive's template to be destroyed and re-created
			 * when certain inputs of this directive change.
			 */
			scope.mediaSlotCloak = false;
			scope.recreateMediaSlots = function(newValue, oldValue) {
				if(newValue === oldValue) return;

				scope.mediaSlotCloak = true;
				$timeout(function() {
					scope.mediaSlotCloak = false;
				});
			}
			scope.$watch('templateSettings', scope.recreateMediaSlots);
			scope.$watch('existingMediaItemList', scope.recreateMediaSlots);
		},
		template: `<div>
			<span ng-if="initError">An error occurred initializing media controls: {{initError}}</span>

			<div ng-if="!initError && !mediaSlotCloak">
				<!-- show media items that already exist -->
				<div flex ng-repeat="media in existingMediaItemList track by media.id">
					<media-slot
						media                 = "media"
						expanded              = "expandedSlotList[$index]"
						disable-decorations   = "disableDecorations"
						editable              = "editable"
						hide-non-translatable = "hideNonTranslatable"
						translation           = "translation"

						ng-show               = "showExistingMediaSlot($index)"

						label                 = "{{getSlotLabel($index)}}"
						slot-index            = "{{$index}}"
						init-function         = "initMediaListObject(__index, __mediaTypeId)"
						remove-function       = "removeMediaItem(__index, __test)"
						media-type-json       = "{{getAllowedMediaTypes($index) | json}}"
						internal-flex         = "internalFlex"
						misc-options          = "miscOptions"
					></media-slot>
				</div>

				<!-- show a media type selector for adding new media items, if more items are permitted -->
				<media-slot
					ng-if                 = "showNewMediaSlot()"

					label                   = "{{getSlotLabel(existingMediaItemList.length)}}"
					slot-index              = "{{existingMediaItemList.length}}"
					init-function           = "initMediaListObject(__index, __mediaTypeId)"
					remove-function         = "removeMediaItem(__index, __test)"
					media-type-json         = "{{getAllowedMediaTypes(existingMediaItemList.length) | json}}"
					internal-flex           = "internalFlex"
				></media-slot>
			</div>
		</div>`,
	};
}]);

/*
 * Presents a media slot for a specific item in a media list.
 *
 * This slot may be empty, in which case we present controls for selecting a media
 * type to go into the slot.
 *
 * Otherwise, we display information about the media that is currently in the slot
 * along with controls for manipulating the media.
 *
 * Params:
 *                 media: (optional) The media item to show in this slot.
 *              expanded: (optional) Initialize this slot in its expanded
 *                        state?
 *                 label: A string that will be used to label this slot
 *                        slot-index: The media list index that this slot will
 *                        affect; needed when calling the init and remove
 *                        functions.
 *         init-function: A function that may be called to initialize the media
 *                        list index associated with this slot.
 *       remove-function: A function that may be called to remove the media
 *                        item in this slot's position in the media list.
 *                        NOTE: Since angular maintains a slot instance for
 *                        each media item in existence, REMOVING a media item
 *                        from the list will DESTROY the corresponding media
 *                        slot instance.
 *       media-type-json: A JSON string defining a list of media types
 *                        permitted in this slot.
 *   disable-decorations: (optional) Do not include controls for removing
 *                        media items, changing media item type, or toggling
 *                        the expanded state of a media slot.
 *              editable: (optional) When set to a falsy value, fields
 *                        within media items will be presented in a disabled,
 *                        read-only state.
 * hide-non-translatable: (optional) Not all controls that are part of media
 *                        items contain translatable data. For example, a
 *                        "chart" media item may be one of several preset
 *                        types of charts, and it is not appropriate to
 *                        allow this sort of control to be changed during
 *                        translation. This option hides such controls.
 *         internal-flex: (optional) If provided, allows the caller to specify a
 *                        desired value for the "flex" attribute on the div
 *                        containing this media slot's controls.  The name
 *                        'internal-flex' was chosen instead of just 'flex'
 *                        because if we call it 'flex' we are unable to
 *                        separately set the flex value on the div containing
 *                        this media slot and on the div inside of the slot that
 *                        contains the media controls.
 *           translation: (optional) When set, indicates that this media control
 *                        instance is for manipulating media assets for a
 *                        translation.  This will be passed down to the media
 *                        slot so that when assets are uploaded, we can place
 *                        them into the media/translation/[language shortcode]
 *                        subdirectory.
 *          misc-options: (optional) This parameter is meant to be set to an
 *                        object that contains options for influencing the
 *                        presentation and behavior of the media slot.
 */
chameleonDir.directive('mediaSlot',
['$rootScope', 'mediaService', '$timeout', '$http', '$parse', 'Upload', 'Media', '$mdDialog',
function($rootScope, mediaService, $timeout, $http, $parse, Upload, Media, $mdDialog) {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			media:               '=?',
			expanded:            '=?',
			label:               '@',
			slotIndex:           '@',
			initFunction:        '&',
			removeFunction:      '&',
			mediaTypeJson:       '@',
			disableDecorations:  '=?',
			editable:            '=?',
			hideNonTranslatable: '=?',
			internalFlex:        '=?',
			miscOptions:         '=?',
			translation:         '=?'
		},
		link: function(scope, element, attrs) {
			// set defaults on optional inputs; note that we are intentionally
			// leaving "media" undefined if it was not passed in, as this means that
			// there is no media present in the slot
			if (angular.isUndefined(scope.expanded)) {
				scope.expanded = false;
			}
			if (angular.isUndefined(scope.disableDecorations)) {
				scope.disableDecorations = false;
			}
			if (angular.isUndefined(scope.editable)) {
				scope.editable = true;
			}
			if (angular.isUndefined(scope.hideNonTranslatable)) {
				scope.hideNonTranslatable = false;
			}
			if (angular.isUndefined(scope.miscOptions) || 'object' != typeof scope.miscOptions) {
				scope.miscOptions = {};
			}

			// image optimization is on by default
			if(angular.isDefined(scope.media) && angular.isUndefined(scope.media.optimize)) {
				scope.media.optimize = true;
			}

			// we need to initialize the "translated" property of media objects,
			// because if this property does not exist in the main english version of
			// the course data, the translation editor will not correctly load the
			// "translated" property of the corresponding media object in translations
			if (angular.isDefined(scope.media) && angular.isUndefined(scope.media.translated)) {
				scope.media.translated = false;
			}

			// generate a unique identifier for this media slot instance -- this value
			// can be incorporated into media control templates to make it easy for
			// business logic to find specific elements within this media slot's DOM
			// fragment
			scope.mediaSlotId = mediaService.generateMediaId();

			// if we received an "internalFlex" parameter, that will override any
			// default flex value that may be set for the div that contains the
			// controls within this media slot; if not, different media types have
			// different default flex values associted with them (see this directive's
			// template), and that default will be used
			if (angular.isDefined(scope.internalFlex)) {
				scope.flexOverride = scope.internalFlex;
			}
			scope.$watch('media.type', function(oldValue, newValue) {
				if (angular.isUndefined(scope.media)) {
					return;
				}
				if (scope.flexOverride) {
					scope.internalFlex = scope.flexOverride;
				} else {
					switch(scope.media.type) {
						case 'adobe-edge':
						case 'audio':
						case 'before-after':
						case 'chart':
						case 'gallery':
							scope.internalFlex = '100';
							break;
						case 'image':
							scope.internalFlex = '65';
							break;
						case 'video':
							scope.miscOptions.hls = ($rootScope.data.config.hls) ? true : false;
							scope.media.audioTrack = true;
							scope.internalFlex = '100';
							break;
						case 'before-after':
							scope.internalFlex = '70';
							break;
						case 'pullquote':
						case 'slideshow':
						case 'textBlock':
						default:
							scope.internalFlex = '';
					}
				}
			});

			// although "disableFields" is a vague variable name, we're setting this
			// in the media slot's scope so that it will be available and usable for
			// the media includes without having to re-engineer the way fields are
			// disabled
			scope.disableFields = !scope.editable;

			// many templates look for a variable called "noTranslate"; rather than
			// change all of them to look for a better-named variable, we'll just
			// create "noTranslate" in the scope here
			scope.noTranslate = scope.hideNonTranslatable;

			// bring variables from the root scope into the local scope so we can
			// reference them from the template
			scope.base_url = $rootScope.base_url;
			scope.coreRoot = $rootScope.coreRoot;
			scope.embedRoot = aws_path+'core/';
			scope.placeholder_image = $rootScope.placeholder_image;

			// define the order in which the embed asset URLs show up
			scope.embedAssetTypeOrder = ['youtube', 'vimeo', 'sketchfab'];

			// define patterns used to identify URLs for determining what kind of
			// embedded asset a URL is for
			scope.embedAssetTypes = {
				youtube: {
					pattern: /^https?:\/\/(?:www\.)?youtube\.com\/watch\?.*v=([^&]*).*$/,
				},
				vimeo: {
					pattern: /^https?:\/\/(?:www\.)?vimeo\.com\/(\d+)$/,
				},
				sketchfab: {
					pattern: /^https?:\/\/(?:www\.)?sketchfab\.com\/models\/(.*)$/,
				}
			};

			// when media.embed.url is initialized or changed, light up the logo for
			// the type of embed that the URL leads to, if any
			scope.$watch('media.embed.url', function(newValue, oldValue) {
				for (k in scope.embedAssetTypes) {
					scope.embedAssetTypes[k].disabled = ('undefined' !== typeof newValue && '' != newValue) &&
						null === scope.embedAssetTypes[k].pattern.exec(newValue);
				}
			});

			/*
			 * This function can be used to nuke the markup inside of the media slot
			 * and re-initialize it.
			 */
			scope.mediaSlotCloak = false;
			scope.reInitialize = function() {
				scope.mediaSlotCloak = true;
				$timeout(function() {
					scope.mediaSlotCloak = false;
				});
			};

			/**
			 * This group of watches is meant to keep an eye on anything in the media
			 * slot scope that requires us to intialize or re-initialize components
			 * within the media slot.  Sometimes this is tricky because if an update
			 * happens to trigger multiple watch conditions, we can end up with  an
			 * initialization function being called twice in rapid succession, which
			 * can lead to unexpected behavior, so care must be taken in deciding what
			 * changes require initialization.
			 */
			scope.$watchGroup(['media.video', 'media.audio', 'media.subtitle'], function(newValue, oldValue) {
				// do nothing on initilization
				if (newValue === oldValue) {
					return;
				} else {
					scope.initializeAudioVideo();
				}
			});
			scope.$watch('media.image', function(newValue, oldValue) {
				// do nothing on initialization
				if (newValue === oldValue) {
					return;
				}
				else if (-1 != ['video', 'audio'].indexOf(scope.media.type)) {
					// keep poster image updated
					scope.initializeAudioVideo();
				}
			}, true);
			scope.$watchGroup(['expanded', 'media.translated'], function(newValue, oldValue) {
				// do nothing on initialization
				if (newValue === oldValue) {
					return;
				}

				// we're only interested in performing actions when a slot is expanded
				// and has been assigned a media type
				if (!scope.expanded || angular.isUndefined(scope.media) || angular.isUndefined(scope.media.type)) {
					return;
				}

				// different initialization code for different media types
				switch (scope.media.type) {
				case 'before-after':
					scope.initializeBeforeAfter();
					break;
				case 'adobe-edge':
					scope.initializeAdobeEdge();
					break;
				case 'audio':
				case 'video':
					scope.initializeAudioVideo();
					break;
				}
			});

			/*
			 * Wrap a call to mediaService.getMediaTypeLabel() so we can call it
			 * from within the directive's template.
			 */
			scope.getMediaTypeLabel = function(typeId) {
				return mediaService.getMediaTypeLabel(typeId);
			};

			/**
			 * Media assets are stored only as filenames.  getMediaUrl() provides
			 * templates and other methods within scope a way of obtaining a fully
			 * qualified URL for a particular media asset.
			 *
			 * Implementation note: Placing a cache-busting timestamp on the end of
			 * the url forces us to cache URLs.  Unless we do this, calls to get a URL
			 * for the same asset are almost never idempotent, which convinces
			 * AngularJS that the URL is constantly changing, which leads to runaway
			 * digest cycles.  In order to clear the timestamps, call clearUrlCache()
			 * to force these URLs to be recalculated.
			 */
			scope.urlCache = {};
			scope.getMediaUrl = function(type, filename) {

				var courseRoot = aws_path + 'clients/' + $rootScope.course.client_id + '/courses/' + $rootScope.course.course_folder;
				var mediaRoot = courseRoot + '/pre/course/media/';

				if (angular.isUndefined(filename) || "" == filename) {
					if ('video' == type) {
						return $rootScope.placeholder_video;
					}

					return '';
				}

				// determine cache key
				var cacheKey = `${filename}|${type}`;
				if (angular.isDefined(scope.media.id)) {
					cacheKey += `|${scope.media.id}`;
				}
				if (angular.isDefined(scope.translation) && scope.media.translated) {
					cacheKey += `|${scope.translation}`;
				}

				// create cache entry if missing
				if (angular.isUndefined(scope.urlCache[cacheKey])) {
					var url = '';
					if (angular.isDefined(scope.translation) && scope.media.translated) {
						url = mediaRoot + `translation/${scope.translation}/`;
					}
					else {
						url = mediaRoot;
						if (angular.isUndefined(scope.media.id)) {
							switch (type) {
								case 'image':
									url += 'images/';
									break;
								case 'audio':
									url += 'audio/';
									break;
								case 'video':
									url += 'video/';
									break;
								case 'adobe-edge':
									url += 'custom/';
									break;
								default:
									throw `Unrecognized type ${type}`;
							}
						}
					}
					url += filename;
					if ('adobe-edge' == type) {
						url += '/index.html';
					}
					url += '?' + new Date().getTime();
					scope.urlCache[cacheKey] = url;
				}

				// satisfy request from cache
				return scope.urlCache[cacheKey];
			};
			scope.clearUrlCache = function() {
				scope.urlCache = {};
			};

			/*
			 * Initialize Adobe Edge media slots whenever they are expanded.  This is
			 * to deal with the case where a slot containing an animation is
			 * instantiated in a collapsed state.  We do not load the animation
			 * until/unless the slot is expanded.
			 */
			scope.initializeAdobeEdge = function() {
				if (
					angular.isUndefined(scope.media) ||
					angular.isUndefined(scope.media.type) ||
					'adobe-edge' != scope.media.type ||
					!scope.expanded
				) {
					return;
				}

				/*
				 * Since Adobe Edge animations can be fairly large in terms of screen
				 * real estate, we must scale down the animation in order to render a
				 * preview inside of a media slot.  This becomes complicated by the fact
				 * that an animation must be loaded into an iframe, so if we we simply
				 * set the iframe's src attribute, then we run into browser security
				 * model roadblocks.  This method downloads index.html with an AJAX call,
				 * eliminates resource references that rely on the iframe's src attribute
				 * being set, calculates the zoom factor required to make the animation's
				 * canvas fit perfectly within the size of the iframe, applies that zoom
				 * factor to the document's body element, and writes the result into the
				 * iframe's document.
				 */
				var loadAdobeEdgePreview = function(mediaFolder) {
					if (undefined === mediaFolder) {
						return;
					}

					var iframe = element.find('iframe').first()[0];
					if (angular.isUndefined(iframe)) {
						return;
					}
					var index_url = scope.getMediaUrl('adobe-edge', mediaFolder);

					$http({
						method : 'GET',
						url: index_url
					}).success(function(data, status, headers, config) {
						var animationBaseUrl = index_url.replace(/^(.*\/)index\.html.*/, '$1');
						var animationHtml = data;
						animationHtml = animationHtml.replace('<head>', '<head><base href="' + animationBaseUrl + '" />');

						var doc = iframe.contentDocument;

						// nothing to do if no document is loaded into the iframe
						if (null === doc) {
							return;
						}

						doc.open();
						doc.writeln(animationHtml);
						doc.close();
						iframe.onload = function() {
							var result;

							result = doc.getElementsByTagName('canvas');
							var canvas_element = result[0];
							var canvas_width = canvas_element.getAttribute('width');
							var canvas_height = canvas_element.getAttribute('height');

							var width_zoom  = 100.0;
							var height_zoom = 100.0;
							if (canvas_width > $(iframe).width()) {
								width_zoom = $(iframe).width() / canvas_width * 100;
							}
							if (canvas_height > $(iframe).height()) {
								height_zoom = $(iframe).height() / canvas_height * 100;
							}
							doc.body.style.zoom = Math.min(width_zoom, height_zoom) + '%';
						};
					});
				};

				$timeout(function() {
					try {
						if (!scope.expanded) {
							throw new Exception('Slot is not expanded');
						}
						if (angular.isUndefined(scope.media.folder)) {
							throw new Exception('No media folder defined');
						}
						loadAdobeEdgePreview(scope.media.folder);
					}
					catch (e) {}
				});
			}

			// initialize before-after preview
			scope.initializeBeforeAfter = function() {
				if (
					angular.isUndefined(scope.media) ||
					angular.isUndefined(scope.media.type) ||
					'before-after' != scope.media.type ||
					!scope.expanded
				) {
					return;
				}

				$timeout(function() {
					// determine expression to use to find this before-after element
					var beforeAfterPreviewElementId = `before-after-preview-${scope.mediaSlotId}`;
					var beforeAfterPreviewElementExpression = `#${beforeAfterPreviewElementId}`;

					// find the preview content element
					var previewContentElement = element.find('.preview-content');
					if (previewContentElement.length < 1) {
						throw 'Could not find preview content element!';
					}

					// remove any existing before-after element
					var existingBeforeAfterElement = previewContentElement.find(beforeAfterPreviewElementExpression);
					if (existingBeforeAfterElement.length > 0) {
						existingBeforeAfterElement.remove();
					}

					// place a new before-after element into the DOM
					previewContentElement.prepend(`<div style="width: 100%; top: 0;" id="${beforeAfterPreviewElementId}"></div>`);

					// create a new JXSlider instance
					scope.slider = new juxtapose.JXSlider(beforeAfterPreviewElementExpression, [{
					    src: scope.getMediaUrl('image', scope.media.image[0]),
					    label: 'Before'
					}, {
					    src: scope.getMediaUrl('image', scope.media.image[1]),
					    label: 'After'
					}], {
					    animate: true,
					    showLabels: true,
					    showCredits: false,
					    startingPosition: '50%'
					});
				});
			};

			// load/reload audio/video loaded into the videojs player
			scope.initializeAudioVideo = function() {
				// sanity check -- media type must be audio or video and the
				// corresponding attribute in the media object must be defined
				if (
					angular.isUndefined(scope.media) ||
					angular.isUndefined(scope.media.type) ||
					-1 == ['video', 'audio'].indexOf(scope.media.type) ||
					!scope.expanded
				) {
					return;
				}

				// initialize the asset holding property of the media object if it is
				// not already set
				if (angular.isUndefined(scope.media[scope.media.type])) {
					scope.media[scope.media.type] = '';
				}

				// determine the src to feed to the player
				var videojsSrc = scope.getMediaUrl(scope.media.type, scope.media[scope.media.type]);

				// If there is already an instance of the video player, we'll destroy
				// it and create a new one.  After much trial, error and
				// experimentation, this appears to be the most reliable way of
				// changing the configuration of the player.
				var previewContentElement = element.find('.preview-content');
				if (previewContentElement.length < 1) {
					throw 'Could not find preview content element!';
				}
				if (angular.isDefined(scope.videojsPlayer)) {
					scope.videojsPlayer.dispose();
				}
				var videoElement = document.createElement('video');
				videoElement.setAttribute('class', 'video-js vjs-default-skin editor-vjs-player');
				videoElement.setAttribute('controls', '');
				videoElement.setAttribute('preload', 'auto');
				videoElement.setAttribute('width', '100%');
				videoElement.setAttribute('height', '480');
				videoElement.setAttribute('crossorigin', 'anonymous');
				if ('video' == scope.media.type && angular.isDefined(scope.media.subtitle) && "" != scope.media.subtitle) {
					var trackElement = document.createElement('track');
					trackElement.setAttribute('default', '');
					trackElement.setAttribute('kind', 'captions');
					trackElement.setAttribute('label', 'Subtitles');
					trackElement.setAttribute('src', scope.getMediaUrl('video', scope.media.subtitle));
					$(videoElement).append(trackElement);
				}
				previewContentElement.append(videoElement);
				scope.videojsPlayer = videojs(videoElement, {"controlBar":{"volumeControl": false}}, function() {
					if (videojsSrc) {
						this.src(videojsSrc);
					}
					if (videojsSrc && 'video' == scope.media.type &&
						(angular.isUndefined(scope.media.image) || scope.media.image.length < 1 || '' == scope.media.image[0])
					) {
						// we have a video asset loaded into the player but no poster
						// image -- don't use the placeholder image as the poster in this
						// case because we would rather show the first frame of the video
					}
					else {
						this.poster(scope.getMediaUrl('image', scope.media.image[0]));
					}
					this.load();
				});
			};

			scope.initializeGallery = function() {
				// because the gallery image property contains and array of objects
				// rather than a simple array of filenames, we need to make sure data
				// is correct when a media type has been changed to Gallery but already
				// contains image data.
				angular.forEach(scope.media.image, function(value, idx) {
					if(typeof value == 'string') {
						var imgObj = {};
						imgObj.src = value;
						scope.media.image[idx] = imgObj;
					}
				});
			}

			/*
			 * Call the method that was handed to this scope for creating a media
			 * item.
			 */
			scope.setMediaType = function(mediaTypeId) {
				if (angular.isUndefined(mediaTypeId)) {
					scope.expanded = true;
				}
				scope.initFunction({
					'__index': scope.slotIndex,
					'__mediaTypeId': mediaTypeId,
				});
			};

			/*
			 * This method wraps the removeFunction that was brought into scope when
			 * this directive was initialized.  Unfortunately it appears that it is
			 * not possible to pass a function directly through to another directive,
			 * so we're wrapping it just to allow this to happen.
			 */
			scope.removeMediaItem = function(index) {
				return scope.removeFunction({
					'__index': index,
				});
			};

			scope.removeObjectItem = function(object, index) {
				delete object[index];
			};

			scope.removeImage = function(index) {
				scope.media.image[index] = '';
			};

			scope.removeSlide = function(index) {
				scope.media.image.splice(index, 1);
			};

			scope.addSlide = function() {
				scope.media.image.push('');
			};

			scope.toggleExpanded = function() {
				if (scope.disableDecorations) {
					return;
				}
				scope.expanded = !scope.expanded;
			};

			/**
			 * This upload function complies with the signature expected of an
			 * ngFileUpload function for handling a file selection (ngf-change) event.
			 * It is meant to work in conjunction with the "trigger-upload" directive
			 * and allows templates to cleanly attach upload triggers and behaviors to
			 * DOM elements.
			 *
			 * Arguments:
			 * $file: An object representing the file that was selected for upload.
			 *
			 * filenameDstExpression: A string expression indicating the scope
			 *   variable that should receive the uploaded filename upon successful
			 *   completion of the upload.
			 *
			 * options: (optional) This object may contain any of these additional
			 *   parameters for influencing upload behavior:
			 *   -> assetPosition: An ordinal indicating the position of the media
			 *     item we're uploading within a collection of items.  This is
			 *     included so we can individually track upload progress on a
			 *     collection of items.  Consider the scenario where you're uploading
			 *     an image to position 3 of 5 images.  The filenameDstExpression will
			 *     indicate that media.image[2] should receive the filename when the
			 *     upload completes successfully.  But since we're remaining agnostic
			 *     about the content of the expression in filenameDstExpression, we
			 *     have no way of knowing that the file is being uploaded to position
			 *     2 of the image array.  assetPosition, therefore, allows the caller
			 *     to tell us what index to use for tracking upload status, and the
			 *     caller can then use this index in expressions for displaying upload
			 *     progress.  If no assetPosition is supplied, 0 is used.
			 *   -> classification: The serverside code handling uploads accepts a
			 *     "type" parameter, which simply serves as an indication of how the
			 *     serverside code should handle the code in terms of post-processing.
			 */
			scope.uploadFile = function($file, filenameDstExpression, options) {
				if (!$file) {
					return;
				}
				options = options || {};

				// ingest filenameDstExpression and turn it into a setter function if needed
				var filenameGetter = $parse(filenameDstExpression);
				var filenameSetter = filenameGetter.assign;

				// ingest assetPosition option
				var assetPosition = angular.isDefined(options.assetPosition) ? options.assetPosition : 0;

				// this function is called before, during and at the end of the upload
				// process in order to update a scope variable where we advertise the
				// progress of the upload for any observers interested in acting on this
				// information
				var setPercent = function(pct) {
					scope.uploadStatus = scope.uploadStatus || [];
					if (0 === pct) {
						scope.uploadStatus[assetPosition] = {
							state: '',
							text: '',
							percent: 0
						};
					}
					else if (pct >= 99) {
						scope.uploadStatus[assetPosition] = {
							state: 'finalizing',
							text: 'Finalizing upload...',
							percent: pct
						};
					}
					else {
						scope.uploadStatus[assetPosition] = {
							state: 'uploading',
							text: `Uploading... (${pct}% complete)`,
							percent: pct
						};
					}
				};

				// picking a random SIMS loading message for Zencoder status, because queue/waiting is weird to an end user
				// who doesn't understand our upload/transcode process.
				var simsStatus = ["Adding Hidden Agendas","Asserting Packed Exemplars","Compounding Inert Tessellations","Deunionizing Bulldozers","Searching for Llamas","Reticulating Splines","Speculating Stock Market Indices","Unable to Reveal Current Activity"];
				var zencoderStatus = simsStatus[Math.floor(Math.random()*simsStatus.length)];

				// this function is called during video upload to check zencoder transcoding
				// progress. once resolved, we know that the video has been returned to our servers
				var zencoderProgress = function(job){
					$http({
						method : 'GET',
						url: 'https://app.zencoder.com/api/v2/jobs/'+job+'/progress.json?api_key=c4ad7b60d332e43fb914e8d85d181ceb'
					}).success(function(data, status, headers, config) {
						if (data.state == "waiting"){

							scope.uploadStatus[assetPosition].text = zencoderStatus;
							$timeout(function(){zencoderProgress(job)},1000);
						} else if (data.state == "processing"){
							scope.uploadStatus[assetPosition].text = "Transcoding File";
							$timeout(function(){zencoderProgress(job)},1000);
						} else {
							// invoke the setter expression for communicating the filename
							// back to the location elected by the caller
							filenameSetter(scope, filename);
							scope.clearUrlCache();
							scope.reInitialize();
							setPercent(0);
						}
					});
				}

				try {
					// target filename:
					//
					// If we're showing this media slot in a translation, be sure to keep
					// the same filename.
					//
					// Otherwise:
					//
					// If this media slot has an ID, generate a new name for the file
					// based on the media slot's ID.
					//
					// Otherwise, use the filename provided by the user's browser.
					if (scope.translation) {
						var filename = filenameGetter(scope);
					}
					if (angular.isUndefined(filename)) {
						if (scope.media.id) {
							var mediaId = scope.media.id;
							var extension = $file.name.replace(/^.*\.([^\.]*)$/, '$1');
							var filename = mediaService.generateMediaFilename(mediaId, extension);
						}
						else {
							var filename = $file.name;
						}
					}

					// initialize progress variables in the scope
					setPercent(0);

					// prepare the payload data
					var uploadData = {
						file: $file,
						name: filename,
						isUnique: angular.isDefined(scope.media.id)
					};
					if (angular.isDefined(options.classification)) {
						uploadData.type = options.classification;
					}
					if (angular.isDefined(scope.translation)) {
						uploadData.translationLanguage = scope.translation;
					}
					if (angular.isDefined(options.hls)) {
						uploadData.hls = options.hls;
						scope.media.hls = options.hls;
					}
					if (angular.isDefined(options.audioTrack)) {
						uploadData.audioTrack = options.audioTrack;
					}
					if(angular.isDefined(scope.media.optimize)) {
						uploadData.optimize = (scope.media.optimize) ? true : false;
					}

					// this function will be called if the upload succeeds
					var successFunction = function (resp) {
						// remove the filename's extension if requested
						if (options.removeExtension) {
							filename = filename.replace(/^(.*)\.zip$/i, '$1');
						}

						//Track progress of video encoding if video slot and replace extension on completion
						if (options.classification == "video") {
							filename = filename.slice(0, -4) + ".mp4";
							zencoderProgress(resp.data);
						} else if (options.classification == "audio") {
							filename = filename.slice(0, -4) + ".mp3";
							zencoderProgress(resp.data);
						} else {
							// invoke the setter expression for communicating the filename
							// back to the location elected by the caller
							filenameSetter(scope, filename);
							scope.clearUrlCache();
							scope.reInitialize();
							setPercent(0);
						}
					};

					// upload errored out
					var errorFunction = function(resp) {
						var errorMessage = '';
						try {
							errorMessage = resp.data.error_message;
						}
						catch (e) {
							errorMessage = 'Something went wrong when uploading the file.';
						}
						var popupBox = $mdDialog.show(
							$mdDialog.alert({
								title: 'Uploading Error',
								textContent: errorMessage,
								ok: 'Continue'
							})
						).finally(function() {});
						setPercent(0);
					};

					// this is called to update the progress indicator
					var progressFunction = function (evt) {
						setPercent(parseInt(100.0 * evt.loaded / evt.total));
					};

					// initiate the upload
					Upload.upload({
						url: current_url + '/upload',
						data: uploadData
					}).then(successFunction, errorFunction, progressFunction);
				}
				catch (e) {
					console.log(e);
				}
			};

			/**
			 * Copy assets for this media slot to the translation media subdirectoy
			 * for the specified language.
			 */
			scope.initializeTranslation = function(languageShortcode) {
				scope.translationInitInProgress = true;
				var request = Media.initialize_translation({
					'language_shortcode': languageShortcode,
					'media': scope.media,
					'course_id': $rootScope.editor.course.id
				});
				request.$promise.then(function() {
					// success
					scope.media.translated = true;
					scope.clearUrlCache();
				}, function(request) {
					// error
					console.log(request);
				}).finally(function() {
					// finally
					scope.translationInitInProgress = false;
				});
			};
		},
		template: `<md-whiteframe class="md-whiteframe-z1" layout="column">
			<!--pre>{{media | prettyJSON}}</pre-->
			<!-- mini summary bar that can toggle display of media details -->
			<div ng-hide="undefined === media" layout="row" layout-padding layout-align="start center" class="sortable module_header">
				<span flex class="md-primary" ng-click="toggleExpanded()"><strong>{{label}}:</strong> {{getMediaTypeLabel(media.type)}}</span>
				<i   class        = "material-icons toggle-arrow"
					ng-class        = "{'active': expanded}"
					ng-click        = "toggleExpanded()"
					ng-if           = "!disableDecorations"
				>arrow_drop_up</i>
				<md-button
					md-no-ink
					ng-if           = "editable && !disableDecorations"
					ng-show         = "media.type"
					class           = "md-mini"
					ng-click        = "setMediaType(undefined)"
					aria-label      = "Change Type"
				>Change Type</md-button>
				<remove-buttons
					ng-if           = "editable && !disableDecorations"
					index           = "slotIndex"
					enabled         = "removeFunction({__index: slotIndex, __test: true})"
					remove-function = "removeMediaItem(__index)"
				></remove-buttons>
			</div>

			<!-- display media type selector when no type has been chosen for this position -->
			<div ng-if="editable && !hideNonTranslatable">
				<label flex ng-show="undefined === media"><strong>Add {{label}}:</strong></label>
				<media-type-selector
					ng-show         = "undefined === media || (undefined === media.type && expanded)"
					media-type-json = "{{mediaTypeJson}}"
					on-select       = "setMediaType(__mediaTypeId)"
				></media-type-selector>
			</div>

			<!-- show details about media in this position in the list -->
			<md-content layout="row" layout-padding ng-show="undefined !== media && expanded && undefined !== media.type" class="md-padding" ng-class="media.type=='fpo' ? 'fpo':''" flex style="overflow: hidden;">

				<div ng-show="translation && !media.translated" class="translate-this">
					<md-button ng-show="!translationInitInProgress" class="md-raised md-accent" ng-click="initializeTranslation(translation)">Replace Media Item</md-button>
					<md-progress-circular ng-show="translationInitInProgress" md-mode="indeterminate"></md-progress-circular>
				</div>

				<div ng-if="media.type && !mediaSlotCloak" ng-switch on="media.type" flex ng-class="{'translate-this-blur': translation && !media.translated}">
					<div ng-switch-when="adobe-edge" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_adobe_edge.html'" onload="initializeAdobeEdge()"></div>
					</div>
					<div ng-switch-when="audio" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_audio.html'" onload="initializeAudioVideo()"></div>
					</div>
					<div ng-switch-when="before-after" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_before_after.html'" onload="initializeBeforeAfter()"></div>
					</div>
					<div ng-switch-when="image" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_image.html'" layout="column"></div>
					</div>
					<div ng-switch-when="slideshow" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_slideshow.html'"></div>
					</div>
					<div ng-switch-when="gallery" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_gallery.html'" onload="initializeGallery()"></div>
					</div>
					<div ng-switch-when="video" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_video.html'" onload="initializeAudioVideo()"></div>
					</div>
					<div ng-switch-when="zoom" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_zoom.html'"></div>
					</div>

					<div ng-switch-when="pullquote" flex="{{internalFlex}}">
						<md-input-container flex style="width:100%;">
							<label>Pull Quote (recommended 75 characters)</label>
							<input ng-model="media.caption" ng-disabled="!editable">
						</md-input-container>
					</div>

					<add-chart ng-switch-when="chart" flex="{{internalFlex}}"
						disable-fields = "!editable"
						no-translate   = "hideNonTranslatable"
					></add-chart>

					<div ng-switch-when="textBlock" flex="{{internalFlex}}">
						<div ng-if="!editable" ng-bind-html="media.textBlock"></div>
						<my-text-area ng-if="editable" ng-model="media.textBlock" rtl="rtl" language="shortcode"></my-text-area>
					</div>

					<div ng-switch-when="embed" flex="{{internalFlex}}">
						<div ng-include="base_url + 'js/angular/templates/media/add_embed.html'"></div>
					</div>
			</md-content>
		</md-whiteframe>`,
	};
}]);

/*
 * Presents a set of controls for selecting one of a list of media types.
 *
 * Params:
 * media-type-json: A JSON string defining an object containing the types to
 *                  display for selection.
 *       on-select: An expression to invoke when an item is selected.
 */
chameleonDir.directive('mediaTypeSelector', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			mediaTypeJson: '@',
			onSelect:      '&',
		},
		controller: function($scope) {
			$scope.$watch('mediaTypeJson', function(newValue, oldValue) {
			$scope.mediaTypeList = angular.fromJson($scope.mediaTypeJson);
			});
		},
		template: `<div>
			<div class="add-media-list">
				<md-grid-list md-cols="6" md-cols-gt-lg="6" md-cols-lg="3" md-cols-md="6" md-cols-sm="3" md-row-height="4:1" md-gutter="4px">
					<md-grid-tile ng-repeat="(typeId, typeLabel) in mediaTypeList" class="gray" md-rowspan="1" md-colspan="1" ng-click="onSelect({__mediaTypeId: typeId})">{{typeLabel}}</md-grid-tile>
				</md-grid-list>
			</div>
		</div>`,
	};
});

/*
 * This directive enables the use of the Adobe Creative SDK image editor on a
 * fragment of the DOM.  The fragment to which it is applied must:
 *   a) Contain at least one image.
 *   b) Contain at least one element designated as a trigger.
 *
 * This is the (extremely generalized) expected structure:
 * <some-container-like-a-div class="blah blah2" adobe-image-editor>
 *   <img ... [adobe-image-editor-target] />
 *   <button adobe-image-editor-trigger>Edit the image</button>
 * </some-container-like-a-div>
 *
 * If the container has more than one img element, the first img element found
 * will be the one that the image editor will operate upon.  However, if you
 * wish to explicitly designate one of the images as the one to edit, it may be
 * tagged with the 'adobe-image-editor-target' attribute.
 *
 * At least one element must be tagged with the 'adobe-image-editor-trigger'
 * attribute.  This directive will add a click handler to the tagged element
 * that will launch the editor.  If more than one element is thusly tagged, all
 * of the tagged elements will launch the editor when clicked.
 */
chameleonDir.directive('adobeImageEditor', ['aviaryEditor', '$timeout', function(aviaryEditor, $timeout) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			// NOTE:
			// $timeout is used here because it makes us execute this code AFTER
			// ng-if directives are evaluated; without this, if any of the critical
			// elements involved with the image editor are tagged with ng-if, or if
			// they are descendants of elements tagged with ng-if, we would fail to
			// locate these elements because they have actually been removed from the
			// DOM entirely at the moment when the link function runs
			$timeout(function() {
				// find the image element
				var image_element = null;
				var results;
				results = element.find('img[adobe-image-editor-target]');
				if (results.length > 0) {
					image_element = results.first();
				}
				else {
					results = element.find('img');
					if (results.length > 0) {
						image_element = results.first();
					}
				}
				if (null === image_element) {
					throw 'No image element found; element tagged with adobe-image-editor must contain at least one image element';
				}

				// find the trigger element(s)
				var trigger_elements = element.find('*[adobe-image-editor-trigger]');
				if (trigger_elements.length < 1) {
					throw 'Could not find any trigger elements';
				}

				// initialize our instance of the image editor, then wire up all trigger
				// elements to launch it
				aviaryEditor.initialize().then(function() {
					trigger_elements.each(function() {
						$(this).click(function() {
							aviaryEditor.saveCallback = function() {
								scope.clearUrlCache();
								scope.initializeBeforeAfter();
							};
							aviaryEditor.editImage(image_element);
						});
					});
				});
			});
		}
	};
}]);

/**
 * Decorate an element with "trigger-upload" to make clicking on the element
 * trigger a file upload using the ngFileUpload directive
 * (https://github.com/danialfarid/ng-file-upload).
 *
 * The value supplied for the attribute should be a call to a function available
 * within the calling template.  A click on the decorated element will display a
 * file selection box (due to the addition of 'ngf-select'), and once a file is
 * selected the supplied function will be invoked for performing the actual
 * upload (due to the addition of 'ngf-change').  Consequently, this function's
 * argument list should comply with the signature supplied in the ngFileUpload
 * documentation:
 *
 * fname ($files, $file, $newFiles, $duplicateFiles, $invalidFiles, $event)
 *
 * It is expected that this function will use ngFileUpload's "Upload" service to
 * facilitate the upload itself.
 */
chameleonDir.directive('triggerUpload', ['$compile', function($compile) {
	return {
		restrict: 'A',
		priority: 1,
		link: function(scope, element, attrs) {
			var replacement = element.clone();
			replacement.attr('ngf-select', '');
			replacement.attr('ngf-change', replacement.attr('trigger-upload'));
			replacement.removeAttr('trigger-upload');
			replacement.removeAttr('ng-transclude');
			replacement = $compile(replacement)(scope);
			element.replaceWith(replacement);
		}
	};
}]);

chameleonDir.directive('ckEditor', ['$timeout', function($timeout) {
	return {
		require: '?ngModel',
		link: function(scope, elm, attr, ngModel) {
			if (!ngModel) return;

			var editorInstance = null;

			function updateModel() {
				$timeout(function() {
					scope.$apply(function() {
						ngModel.$setViewValue(editorInstance.getData());
					});
				});
			}

				var options = {
				extraPlugins: 'tip',
					on: {
						instanceReady: function(evt) {
							editorInstance.setData(ngModel.$viewValue);
					},
					change: function(evt) {
						updateModel();
					},
					key: function(evt) {
						updateModel();
					},
					dataReady: function(evt) {
						updateModel();
					},
					}
				};

				if (scope.rtl || scope.$root.rtl) {
					options.language = scope.language;
					options.contentsLangDirection = 'rtl';
				}

			// Dear Reader:
			// I apologize profusely for this kludge.
			//
			// There are occasions when CKEditor will initial without the model being
			// successfully bound to the editor, resulting in a CKEditor instance that
			// is blank and uneditable, appearing to the end user to be disabled.  See
			// CHAM-1997.  From a programmatic perspective, this happens because some
			// of the CKEditor events that should happen on initialization
			// (instanceReady, most notably) fail to fire.
			//
			// This appears to be the product of a race condition that only happens
			// when trying to instantiate CKEditor under particular conditions.
			// *Something* feels like it is not fully initialized when we get to this
			// point, and by delaying the CKEditor instantiation by a very short
			// amount of time, we may be allowing whatever this thing is to complete
			// initializing.  For my own workstation, 75ms is too little and 100+ms is
			// almost always enough.
			//
			// Please do not model your own coding habit after this.
			$timeout(function() {
				editorInstance = CKEDITOR.replace(elm[0], options);
				ngModel.$render = function(value) {
					editorInstance.setData(ngModel.$viewValue);
				};
			}, 500);
		}
	};
}]);

chameleonDir.directive('fileModel', ['$parse', 'fileUpload', function ($parse, fileUpload) {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.bind('change', function() {

				if(attrs.preview) {
					// update preview immediately from cache
					var preview = $parse(attrs.preview);
					var previewSetter = preview.assign;

					function readURL(input) {
						if (input.files && input.files[0]) {
							var reader = new FileReader();
							reader.onload = function (e) {
								scope.$apply(function(){
									previewSetter(scope, e.target.result);
								});
							}
							reader.readAsDataURL(input.files[0]);
						}
					}
					readURL(element[0]);
				}

				var object = $(element[0]);

				// get media type
				var media_type = object.data('type') || scope.mediaType || 'images';

				// get model object
				var input = $parse(object.data('input'));
				var inputSetter = input.assign;

				scope.$evalAsync(function(){
					var model = $parse(attrs.fileModel);
					var modelSetter = model.assign;
					modelSetter(scope, element[0].files[0]);

					if(angular.isObject(scope.myFile)) {

						// convert spaces to underscores
						var value = scope.myFile.name.replace(/\s/g, '_');
						var name = object.data('name') || value;


						fileUpload.uploadFileToUrl(scope.myFile, media_type, name).then(function(data){

							switch (media_type) {
								case 'adobe-edge':
									value = scope.myFile.name.replace('.zip', '');
									break;

								case 'caption':
									value = (scope.myFile.name.indexOf('.vtt') > -1) ? scope.myFile.name : '';
									break;

								case 'assets':
									value = object.data('name') + '?' + new Date().getTime();
									break;

								case 'resources':
									var resourceType = $parse(object.data('filetype'));
									var resourceTypeSetter = resourceType.assign;
									resourceTypeSetter(scope, data.type);

									var size = $parse(object.data('size'));
									var sizeSetter = size.assign;
									sizeSetter(scope, data.size);
									break;
							}

							inputSetter(scope, value);
						});
					}
				});

			});
		}
	};
}]);

chameleonDir.directive('mySingleUpload', ['actions', '$mdDialog', function(actions, $mdDialog) {
	return {
		restrict: 'E',
		scope: {
			ngModel: '=',
			myLabel: '=',
			myLimit: '=',
			mediaType: '=',
			myDisabled: '=',
			myPreview: '='
		},
		template: '<div layout="column">' +
						'<label ng-hide="myLabel==\'\'" class="control-label">{{myLabel}}</label>' +
						'<div ng-hide="myDisabled" ng-class="{\'form-group\':myLabel}">' +
							'<div ng-class="ngModel ? \'fileinput-exists\' : \'fileinput-new\'" class="fileinput fileupload input-group" data-provides="fileinput">' +
								'<div class="form-control" trigger-upload="uploadFile($file)">' +
									'<i data-type="mediaType" style="margin-right: 10px; position:absolute; top:10px;" class="fa fa-file fileinput-exists"></i>' +
									'<span data-type="mediaType" class="fileinput-filename" ng-bind="ngModel" style="padding:0 16px 0 18px;"></span>' +
								'</div>' +
								'<span class="input-group-addon btn btn-default btn-file">' +
									'<span class="fileinput-new" data-trigger="fileinput" trigger-upload="uploadFile($file)">Browse</span>' +
									'<span class="fileinput-exists" trigger-upload="uploadFile($file)">Change</span>' +
								'</span>' +
								'<a href="#" ng-click="remove()" class="input-group-addon btn btn-default fileinput-exists file-remove">Remove</a>' +
							'</div>' +
						'</div>' +
						'<div ng-if="myDisabled"><input type="text" class="form-control input-text" value="{{ngModel}}" disabled></div>' +
					'</div>',
		replace: true,
		controller: function($scope, $parse, Upload) {
			/*
			 * A condensed version if the uploadFile method for course level media.
			 * See the original function in the directives file for a full description.
			 *
			 * This condensed method also exists in playerCtrl, and could probably be
			 * condensed at some point in the future.
			 */
			$scope.uploadFile = function($file) {
				if (!$file) {
					return;
				}

				try {
					// prepare the payload data
					var uploadData = {
						file: $file,
						name: $file.name,
						isUnique: false,
						type: $scope.mediaType
					};

					// this function will be called if the upload succeeds
					var successFunction = function (resp) {
						// invoke the setter expression for communicating the filename
						// back to the location elected by the caller
						$scope.ngModel = $file.name;
					};

					// upload errored out
					var errorFunction = function(resp) {
						var errorMessage = '';
						try {
							errorMessage = resp.data.error_message;
						}
						catch (e) {
							errorMessage = 'Something went wrong when uploading the file.';
						}
						var popupBox = $mdDialog.show(
							$mdDialog.alert({
								title: 'Uploading Error',
								textContent: errorMessage,
								ok: 'Continue'
							})
						).finally(function() {});
					};

					// initiate the upload
					Upload.upload({
						url: current_url + '/upload',
						data: uploadData
					}).then(successFunction, errorFunction);
				}
				catch (e) {
					console.log(e);
				}
			}
		},
		link: function(scope, element, attrs) {

			scope.$watch("ngModel",function(newValue,oldValue) {
				//This gets called when data changes.
				scope.ngModel = newValue ? newValue : '';
			});

			scope.remove = function() {
				scope.ngModel = '';
			}

			angular.extend(scope, actions);

		}
	}
}]);

chameleonDir.directive('myText', function() {
	return {
		restrict: 'E',
		scope: {
			ngModel: '=',
			myLabel: '=',
			myDisabled: '='
		},
		template: '<div class="form-group">' +
			'<label ng-show="myLabel" class="control-label">{{myLabel}}</label>' +
			'<input ng-disabled="myDisabled" ng-model="ngModel" type="text" class="form-control input-text" placeholder="{{myLabel}}">' +
			'</div>',
		replace: true
	}
});

chameleonDir.directive('myTextArea', function() {
	return {
		restrict: 'E',
		scope: {
			ngModel: '=',
			myLabel: '=',
			myDisabled: '=',
			rtl: '=',
			language: '='
		},
		template: '<div class="form-group">' +
			'<label ng-if="myLabel" class="control-label">{{myLabel}}</label>' +
			'<textarea ck-editor ng-disabled="myDisabled" ng-model="ngModel" rtl="rtl" language="shortcode" class="form-control" rows="10"></textarea>' +
			'</div>',
		replace: true
	}
});

chameleonDir.directive('removeButtons', function() {
	return {
		restrict: 'E',
		replace: true,
		scope: {
			ngModel: '=',
			index: '=',
			enabled: '=?',
			removeFunction: '&?',
		},
		controller: function($scope) {
			if (angular.isUndefined($scope.enabled)) {
				$scope.enabled = true;
			}
			$scope.confirm_delete = false;
			$scope.delete_icon = 'clear';

			$scope.disabledClass = function() {
				return $scope.enabled ? 'enabled' : 'disabled';
			};

			$scope.removeElement = function() {
				if (!$scope.enabled) {
					return;
				}

				if ($scope.confirm_delete) {
					if(angular.isDefined($scope.removeFunction)) {
						var removeFunctionResult = $scope.removeFunction({'__index': $scope.index});
					}
					if (angular.isDefined(removeFunctionResult)) {
						return;
					}
					if (typeof $scope.index == 'object') {
						$scope.ngModel.splice($scope.ngModel.indexOf($scope.index), 1);
					}
					else {
						$scope.ngModel.splice($scope.index, 1);
					}
				}
				else {
					$scope.delete_icon = 'delete_forever';
					$scope.confirm_delete = true;
				}
			};

			$scope.cancelRemoveElement = function() {
				$scope.delete_icon = 'clear';
				$scope.confirm_delete = false;
			};
		},
		template: `<div class="btn-group pull-right" style="padding: 0;">
			<md-button ng-class="{{disabledClass()}}" ng-click="$event.stopPropagation();removeElement()" md-no-ink>
				<md-tooltip md-direction="top" md-delay="1000" md-autohide="true">Delete</md-tooltip>
				<md-icon class="material-icon">{{delete_icon}}</md-icon>
			</md-button>
			<md-button ng-show="confirm_delete" ng-click="$event.stopPropagation();cancelRemoveElement()" md-no-ink>
				<md-tooltip md-direction="top" md-delay="1000" md-autohide="true">Cancel</md-tooltip>
				<md-icon class="material-icon">undo</md-icon>
			</md-button>
		</div>`,
	};
});

chameleonDir.directive('topicNavigation', function($rootScope, mediaService, Media, $mdDialog) {
	var controller = function() {

		var interval;

		// in order to create a better sorting experience, when a user
		// attempts to drag an open topic, we need to automatically close
		// the pages before resorting. however, because the jquery sortable
		// automatically creates a sort helper using the height of the element
		// when dragging begins, we cannot use the drag methods to resize the
		// container, it has to be done before dragging is actived. because
		// sorting has a delay of 200, we will add a timer for 190 here, at which
		// point the expanded containers will close just before dragging is initialized.
		this.preSort = function(action) {
			if(action) {
				interval = setInterval(function() {
					$(".page-list").removeClass('active');
					$(".topic-toggle").addClass("closed");
				}, 190);
			} else {
				clearInterval(interval);
			}
		}

		var preSort = this.preSort;

		this.topicOptions = {
			handle: '.topic-item',
			cursorAt: {bottom: 10},
			start: function(event, ui) {
				$(".topic-item.add").hide();
				$(".topic-group").css('padding', '5px 0');
			},
			stop: function(event, ui) {
				$(".topic-item.add").show();
				$(".topic-group").css('padding', '0');
				preSort();
			},
			tolerance: 'pointer',
			delay: 200
		};

		this.pageOptions = {
			handle: '.page-item',
			start: function(event, ui) {
				$(".page-item.add").hide();
				$(".page-item").css('margin', '5px 0');
			},
			stop: function(event, ui) {
				$(".page-item.add").show();
				$(".page-item").css('margin', '0');

				// A page has just been dragged and dropped, potentially
				// reordering the list of pages. Since the active page is
				// tracked by the page's index, confusing things will start to
				// happen unless we update the variable that tracks the active
				// page to account for any changes that occurred in its
				// position in the list.
				if (ui.item.sortable.index == $rootScope.editor.course.page) {
					// (active page was dragged)
					$rootScope.editor.course.page = ui.item.sortable.dropindex;
				}
				else if (
					ui.item.sortable.index < $rootScope.editor.course.page &&
					ui.item.sortable.dropindex >= $rootScope.editor.course.page
				) {
					// (a page with an index lower than the active page was
					// dragged so that it now has an index higher than the
					// active page)
					$rootScope.editor.course.page--;
				}
				else if (
					// (a page with an index higher than the active page was
					// dragged so that it now has an index lower than the
					// active page)
					ui.item.sortable.index > $rootScope.editor.course.page &&
					ui.item.sortable.dropindex <= $rootScope.editor.course.page
				) {
					$rootScope.editor.course.page++;
				}
			},
			tolerance: 'pointer',
			connectWith: '.page-list',
			delay: 200
		};

		this.addPage = function(topic) {
			topic.page = (topic.page) ? topic.page : [];
			topic.page.push({'title':'New Page'});
		}

		this.addTopic = function() {
			var newTopic = {
				'title':'New Topic',
				'page':[],
				'media':[]
			};

			mediaService.initMediaListObject(newTopic.media, 0, 'image');

			this.topic.push(newTopic);
		}

		this.editPage = function(index, parent) {

			// if parent is set, a page was clicked
			if(parent > -1) {
				sessionStorage.topic = $rootScope.editor.course.topic = parent;
				sessionStorage.page = $rootScope.editor.course.page = index;
			} else {
				delete sessionStorage.topic;
				delete $rootScope.editor.course.topic;
				sessionStorage.page = $rootScope.editor.course.page = index;
			}
		}

		this.pageActive = function(index, parent) {
			if($rootScope.editor.course.topic > -1 && $rootScope.editor.course.topic == parent) {
				return index == $rootScope.editor.course.page;
			} else {
				return false;
			}
		}

		this.topicActive = function(index) {
			if($rootScope.editor.course.topic > -1) {
				return false;
			} else {
				return index == $rootScope.editor.course.page;
			}
		}

		this.copyPage = function(topic, page) {
			/**
			 * Takes a page object and analyzes it to find media file references
			 * and their associated media IDs. Whenever these are found, it
			 * replaces these media IDs and filenames with newly-generated media
			 * IDs and filenames. This process modifies the provided page object
			 * with the new values and returns a mapping from the original
			 * filenames to the newly-generated filenames.
			 *
			 * Since this is a recursive method, not all arguments are intended
			 * to be provided by the original caller. These arguments are the
			 * ones that are expected to be provided by the original caller:
			 *
			 * data: The page object to modify.
			 * all_media_assets: A complete list of all media filenames present
			 *                   in the page where the keys are the filenames.
			 *                   This list can include more media assets than
			 *                   actually occur in the page we're copying, but
			 *                   assets not listed here will not be duplicated.
			 */
			var createNewMediaIDs = function(data, all_media_assets, ancestry, filename_mapping, id_mapping) {
				// initialize filename mapping -- this will ultimately be the
				// return value of this function, and will contain a mapping
				// of original media filenames to newly generated media
				// filenames
				if (angular.isUndefined(filename_mapping)) {
					filename_mapping = {};
				}

				// used internally to keep track of how we have mapped
				// original media IDs to newly generated media IDs
				if (angular.isUndefined(id_mapping)) {
					id_mapping = {};
				}

				// used internally to keep track of object references that
				// have been traversed to reach the current level of
				// recursion; since media IDs are not placed consistently in
				// page data, we can walk this array from end to start to
				// search for the deepest media ID available at the current
				// level of recursion
				if (angular.isUndefined(ancestry)) {
					ancestry = [];
				}

				for (var k in data) {
					var type = typeof data[k];

					// we found a string that is contained in the list of media filenames
					if ('string' == type && data[k] in all_media_assets) {
						var original_filename = data[k];

						// non-legacy media filenames are always prefixed with
						// their associated media ID; we're walking the
						// ancestry array looking for a media ID that is the
						// prefix for this media filename
						for (var i = ancestry.length - 1; i >= 0; i--) {
							if ('object' !== typeof ancestry[i]) continue;
							if ('undefined' === typeof ancestry[i].id) continue;

							// This filename is prefixed by one of the media
							// IDs we encountered to reach this level of
							// recursion. If we haven't already generated a
							// new media ID, we do that now. We then use the
							// new media ID to generate a new filename for
							// this media asset, placing the new filename into
							// the data object.
							//
							// NOTE: We can't replace the media ID in the data
							// just yet because there might be more than one
							// media filename associated with this media ID --
							// we'll do this later when we step out of the
							// recursion level at which the media ID resides.
							if (0 === original_filename.indexOf(ancestry[i].id)) {
								var original_media_id = ancestry[i].id;
								if (original_media_id in id_mapping) {
									var new_media_id = id_mapping[original_media_id];
								} else {
									var new_media_id = mediaService.generateMediaId();
									id_mapping[original_media_id] = new_media_id;
								}

								var extension = (original_filename.indexOf('.') > -1) ?
									original_filename.replace(/^.*\.([^\.]*)$/, '$1') :
									undefined;
								var new_filename = mediaService.generateMediaFilename(new_media_id, extension);
								data[k] = new_filename;
								filename_mapping[original_filename] = {
									dst_filename: new_filename
								};

								// if this is a video that has an HLS
								// encoding, make a note of this so that when
								// the server is making duplicates of media
								// assets, it will know that it needs to
								// duplicate this video's HLS data (which
								// resides in a separate S3 bucket)
								if ('video' == k && data.hls) {
									filename_mapping[original_filename].hls = true;
								}
							}
						}
						continue;
					}

					// we found an object or array, so we need to recurse;
					// before doing so, we need to push this object or array
					// onto a stack ("ancestry") so we can search it for media
					// IDs as described in the block above; after we come out
					// of this level of recursion, if this is an object having
					// an ID property matching one of the newly generated
					// media IDs, we place the new media ID into the data
					if (-1 < ['array', 'object'].indexOf(type)) {
						ancestry.push(data[k]);
						createNewMediaIDs(data[k], all_media_assets, ancestry, filename_mapping, id_mapping);
						try {
							if (data[k].id in id_mapping) {
								data[k].id = id_mapping[data[k].id];
							}
						}
						catch (e) {}
						ancestry.pop();
						continue;
					}
				}

				return filename_mapping;
			};
			var dialogController = function($scope) {
				$scope.state = 'prompt';
				$scope.new_page_title = page.title + ' (copy)';
				$scope.disableCopy=false;
				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.done = function() {
					$mdDialog.hide();
				};
				$scope.newTitle=function(obj){
					$scope.new_page_title=obj.new_page_title;
				}
				$scope.copy = function() {
					$scope.disableCopy=true;
					// we cannot perform the copy operation if an empty page title was chosen
					if (!$scope.new_page_title) {
						console.log('No title!');
						return;
					}

					// get a list of all assets in this course's media directory
					Media.enumerate_media_assets({
						'course_id': $rootScope.editor.course.id
					}, function(all_media_assets) {
						// duplicate the object data for the page being copied
						var duplicatePage = angular.copy(page);

						// delete the UUID for the page so that a new one is generated
						delete duplicatePage.uuid;

						// set the new page title
						duplicatePage.title = $scope.new_page_title;

						// modify the duplicate page object to generate new
						// media IDs and new filenames for media assets
						var mapping = createNewMediaIDs(duplicatePage, all_media_assets);

						// we'll need to use XHR to call a server-side process
						// to make duplicates of all files mentioned in the
						// mapping; we do this inside of a promise so that we
						// can wait until copying work (if any) is done prior
						// to notifying the user that everything is complete
						var copyDonePromise = new Promise(function(resolve, reject) {

							// if there's nothing to copy, just resolve the promise
							if (Object.keys(mapping).length < 1) {
								resolve();
								return;
							}

							// prepare variables for indicating progress
							var progressCount = 0;
							$scope.progressText = 'Duplicating media assets';
							$scope.progressPct = 0;
							$scope.state = 'progress';

							// prepare an XHR instance for calling the server-side copy process
							var xhr = new XMLHttpRequest();
							xhr.open('POST', base_url + 'api/media?action=copy_media_assets&course_id=' + $rootScope.course.course_id, true);
							xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
							var statusRegex = {
								progressPct:  /progress_pct:\s*(\d+)/,
							};
							xhr.onreadystatechange = function() {
								if (2 < xhr.readyState) {
									// isolate new content that arrived since the last state change event
									var new_messages = '';
									if ('undefined' === typeof xhr.previous_text) {
										new_messages = xhr.responseText;
									} else {
										new_messages = xhr.responseText.substring(xhr.previous_text.length);
									}

									if (new_messages) {
										matches = /progress_pct:\s*(\d+)/.exec(new_messages);
										if (null !== matches) {
											$scope.progressPct = matches[1];
											$scope.$apply();
										}

										// print all response messages to the console
										if (new_messages) {
											console.log(`SERVER: ${new_messages}`);
										}
									}

									xhr.previous_text = xhr.responseText;
								}

								// when the server-side process is done, resolve the promise
								if (4 == xhr.readyState) {
									resolve();
								}
							}

							// kick off the server-side process, passing in
							// the mapping received from createNewMediaIDs()
							// to instruct it on which assets to copy and what
							// the new filenames should be
							xhr.send(JSON.stringify(mapping));
						}).then(function() {
							// now that the assets are copied (if it was
							// required) and the duplicate page contains new
							// media IDs and asset filenames, place the new
							// page into the course data and notify the user
							// that copying is done
							topic.page.push(duplicatePage);
							$scope.state = 'complete';
							$scope.$apply();
						});
					});
				};
			};
			$mdDialog.show({
				controller: dialogController,
				templateUrl: base_url + 'js/angular/templates/modals/copy_page.html'
			});
		}

		this.toggleTopics = function(idx) {
			$(".toggle-" + idx).toggleClass('closed');
			$(".topic-" + idx).toggleClass('active');
		}
	}
	return {
		restrict: 'E',
		scope: {
			topic: '='
		},
		controller: controller,
		controllerAs: 'tn',
		bindToController: true,
		templateUrl: base_url + "js/angular/templates/course/topic_navigation.html",
	}
});

chameleonDir.directive('pageEdit', function() {
	return {
		restrict: 'E',
		templateUrl: base_url + "angular/topicpage.php"
	}
});

chameleonDir.directive('topicEdit', function() {
	return {
		restrict: 'E',
		templateUrl: base_url + 'js/angular/templates/course/topic_edit.html'
	}
});

chameleonDir.directive('themeColorPicker', function($timeout) {
	return {
		restrict: 'E',
		scope: {
			pickerLabel: '@',
			pickerHex: '=',
			pickerControl: '@',
			secondaryHex: '=?',
			adjustment: '=?'
		},
		template: `
			<ng-form name="picker" class="color-picker" layout="row" ng-class="pickerControl">
				<md-content class="controls">
					<div class="display" layout="column" layout-align="start center">
						<h5>{{pickerLabel}}</h5>
						<div class="sticker swatch" style="background-color: {{hexDisplay}}"></div>
						<p><label>HEX</label> {{hexDisplay | uppercase}}</p>
						<p><label>RGB</label> {{pickerRGB.r}}, {{pickerRGB.g}}, {{pickerRGB.b}}</p>
					</div>
					<div class="buttons" layout="row">
						<md-button class="edit material-icons" ng-click="showControls($event)">mode_edit</md-button>
						<md-button class="clear gray" ng-click="clearValue(); picker.input.$setPristine()" ng-disabled="!picker.input.$dirty">clear adjustment</md-button>
					</div>
				</md-content>
				<md-content class="controls right">
					<input name="input" class="picker" type="text" ng-model="hexDisplay" value="{{hexDisplay}}"></input>
					<div class="fields" layout="row">
						<div flex>
							<span>HEX <input class="hex" type="text" ng-model="hexDisplay"></input></span>
							<span>RGB <input class="rgb" type="text" ng-model="pickerRGB.r"></input><input class="rgb" type="text" ng-model="pickerRGB.g"></input><input class="rgb" type="text" ng-model="pickerRGB.b"></input></span>
						</div>
						<md-button class="gray" ng-click="showControls($event)">OK</md-button>
					</div>
				</md-content>
			</ng-form>
		`,
		replace: true,
		controller: function($scope, themeService) {

			$scope.themeService = themeService;

			/*
			 * if secondary hex is set, we are dealing with shades of the
			 * secondary brand color. We will need to determine the lighter
			 * and darker shades automatically.
			 */
			if($scope.adjustment) {
				if($scope.pickerHex.indexOf('lighten')+1 || $scope.pickerHex.indexOf('darken')+1) {
					$scope.pickerHex = themeService.lightenDarken($scope.secondaryHex, $scope.adjustment);
				}
			}

			// store the original value for reset
			$scope.originalValue = $scope.pickerHex;

			// handle the values that are using less variables by default
			$scope.toHex = function(value) {
				if(value && value.indexOf('@') == 0) {
					return $scope.$parent.theme_data.variable[value.slice(1)];
				}
				return value;
			};

			$scope.hexDisplay = $scope.toHex($scope.pickerHex);

		},
		link: function(scope, elem, attr) {

			var input = $(elem).find('input.picker');
			var controls = $(input).closest('.controls.right');

			scope.clearValue = function() {
				scope.pickerHex = scope.originalValue;
				scope.hexDisplay = scope.toHex(scope.pickerHex);
				$(input).minicolors('value', scope.hexDisplay);
			}

			scope.showControls = function(ev) {
				// toggle button state
				$('.edit').not(ev.currentTarget).removeClass('active');
				$(ev.currentTarget).toggleClass('active');

				// toggle picker controls
				$('.controls.right').not(controls).removeClass('active');
				controls.toggleClass('active');
			}

			$timeout(function() {

	    	scope.$watch('secondaryHex', function(newValue, oldValue) {
	    		if(newValue && newValue !== oldValue) {
						scope.pickerHex = scope.themeService.lightenDarken(newValue, scope.adjustment);
						$(input).minicolors('value', scope.pickerHex);
					}
	    	});

	    	scope.$watch('hexDisplay', function(newValue, oldValue) {
	    		if(newValue) {
						scope.pickerHex = newValue;
						if(newValue.indexOf('#') == 0) {
	    				$(input).minicolors('value', newValue);
	    			}
	    		}
	    	});

				$(input).minicolors({
		  		inline: true,
		  		letterCase: 'uppercase',
		  		control: scope.pickerControl,
		  		change: function(value, opacity) {
		  			scope.pickerHex = value;
		  			scope.pickerRGB = $(this).minicolors('rgbObject', function(data) {
		  				return data;
		  			});
		  		}
				});

				scope.pickerRGB = $(input).minicolors('rgbObject', function(data) {
					return data;
				});
			}, 1);

		}
	}
});

chameleonDir.directive('themeFontSize', function() {
	return {
		restrict: 'E',
		scope: {
			themeData: "=",
			textElement: "=",
			textName: "=",
			masterSize: "="
		},
		template: `
			<ng-form name="textSizing">
				<md-content layout="row" class="sizing">
					<div flex class="example-container" ng-class="{'adjusted': textSizing.size.$dirty}">
						<p class="item">{{textName}}</p>
						<p layout="row" layout-align="start center" class="example" ng-style="elementCss(textElement)">The Quick Brown Fox Jumps Over The Lazy Dog.</p>
					</div>
					<theme-font-styles theme-data="themeData" text-element="textElement" text-item="'textSize'" master-size="masterSize"></theme-font-styles>
				</md-content>
			<ng-form>
		`,
		replace: true,
		controller: function($scope) {
			$scope.elementCss = $scope.$parent.getElementCss;
		}
	}
});

chameleonDir.directive('themeFontStyles', function() {
	return {
		restrict: 'E',
		scope: {
			themeData: "=",
			textElement: "=",
			textItem: "=",
			masterSize: "=?"
		},
		template: `
			<ng-form name="valueControls">
				<div class="controls" layout="row" ng-class="{'adjusted': valueControls.value.$dirty}">
					<md-button class="gray" ng-click="decreaseValue(textItem)">-</md-button>
					<div class="size-display">
						<input name="value" disabled ng-model="displayValue" />
						<span>{{displayUnit}}</span>
					</div>
					<md-button class="gray" ng-click="increaseValue(textItem)">+</md-button>
					<md-button class="gray clear" ng-click="resetValue(textItem);" ng-disabled="!valueControls.value.$dirty">clear adjustment</md-button>
				</div>
			<ng-form>
		`,
		replace: true,
		controller: function($scope, $filter) {

			$scope.tmp_value = null;

			/*
			 * If the masterSize variable is set, we are calling this directive from
			 * another directive, where the textElement is pre-defined and will not
			 * change, if the masterSize is not set however, the textElement could
			 * change and we need to watch for that.
			 */
			if($scope.masterSize) {

				$scope.originalValue = angular.copy($scope.themeData[$scope.textElement+'_'+$scope.textItem]);

				// watch the master font size, so we know if we need to globally increase sizes
				$scope.$watch('masterSize', function(newValue) {
					var value = ($scope.tmp_value) ? $scope.tmp_value : $scope.originalValue;
					$scope.themeData[$scope.textElement+'_'+$scope.textItem] = Math.ceil(parseInt(value, 10) * newValue)+"px";
				});
			} else {
				$scope.$watch('textElement', function(newValue) {
					$scope.originalValue = angular.copy($scope.themeData[newValue+'_'+$scope.textItem]);

				  $scope.displayValue = $filter('toNumber')($scope.themeData[newValue+'_'+$scope.textItem]);
				  $scope.displayUnit = $scope.themeData[newValue+'_'+$scope.textItem].replace($scope.displayValue, '');
				});
			}

			// watch the settings object for updates so we can display the correct data
		  $scope.$watch('themeData', function(newValue) {
			  $scope.displayValue = $filter('toNumber')(newValue[$scope.textElement+'_'+$scope.textItem]);
			  $scope.displayUnit = newValue[$scope.textElement+'_'+$scope.textItem].replace($scope.displayValue, '');
		  }, true);

		  // because we separated the integer from the measurement unit, we need to put
		  // those values back together here so it is saved correctly in the data
		  $scope.$watch('displayValue', function(newValue) {
	  		$scope.themeData[$scope.textElement + '_' + $scope.textItem] = newValue + $scope.displayUnit;
		  });

		  // function to increase the value
		  $scope.increaseValue = function() {
		  	$scope.valueControls.value.$setDirty();
		  	if ($scope.textItem!="letterSpacing" || ($scope.textItem=="letterSpacing" && $scope.displayValue<10)) {
		  		$scope.displayValue++;
		  	}
		  	$scope.tmp_value = $scope.displayValue;
		  }

		  // function to decrease the value
		  $scope.decreaseValue = function() {
		  	$scope.valueControls.value.$setDirty();
			if ($scope.displayValue) {
				if ($scope.displayValue>0 || ($scope.textItem=="letterSpacing" && $scope.displayValue>-1)) {
		  			$scope.displayValue--;
		  			$scope.tmp_value = $scope.displayValue;
		  		}
		  	}
		  }

		  // function to reset the value
		  $scope.resetValue = function() {
		  	$scope.valueControls.value.$setPristine();
		  	$scope.displayValue = Number($filter('toNumber')($scope.originalValue));
		  	$scope.tmp_value = null;
		  }
		}
	}
});

chameleonDir.directive('backgroundObject', function() {
	return {
		restrict: 'E',
		scope: {
			ngModel: 						 '=',
			editable:            '=?',
			hideNonTranslatable: '=?',
			internalFlex:        '=?',
			miscOptions:         '=?',
			translation:         '=?'
		},
		templateUrl: base_url + "js/angular/templates/objects/background.html",
		replace: true,
		controller: function($scope, $mdDialog, mediaService) {

			// define a temporary storage for settings that do not directly affect
			// the background object or need to be saved, and set defaults to the
			// visibility settings of the preview container and controls based on
			// whether the background gradient already exists or not
			$scope._temp = {};

			$scope.$watch('ngModel', function(newValue, oldValue) {

				// respond to changes in the type of background selected; whenever a new
				// background type is selected, we clear out any background image or video
				// that was already loaded
				//
				// create "backgroundMedia" object to hold media information in a format
				// the mediaSlot directive expects.
				switch(newValue.type) {
					case 'color':
						break;
					case 'image':
						$scope.backgroundMedia = {
							type: 'image',
							image: ['']
						};
						if(angular.isDefined(newValue.src) && !angular.isObject(newValue.src)) {
							$scope.backgroundMedia.image[0] = newValue.src;
						} else {
							newValue.src = null;
						}
						break;
					case 'video':
						$scope.backgroundMedia = {
							type: 'video',
							image: [''],
							video: '',
							audioTrack: false
						};
						$scope.ngModel.audioTrack=false;
						if(angular.isDefined(newValue.src) && angular.isObject(newValue.src)) {
							$scope.backgroundMedia.video = newValue.src.video || '';
							$scope.backgroundMedia.image[0] = newValue.src.image || '';
						}
						break;
					default:
						$scope.ngModel = {};
				}

				// stop initializing the background if there isn't one
				if(newValue.type == undefined) return;

				if(angular.isDefined(newValue.translated)) {
					$scope.backgroundMedia.translated = newValue.translated;
				}

				// if a background contains a 'src' but does not have an 'id', it is likely
				// it was created before the media updates. in this case, we should never
				// give the media element an id. for everything else, we need to add an
				// id to use the new media structure.
				if((angular.isDefined(newValue.src) && newValue.src !== '') && angular.isUndefined(newValue.id)) {
					// no id
				} else {
					$scope.backgroundMedia.id = newValue.id = (angular.isDefined(newValue.id) && newValue.id !== '') ? newValue.id : mediaService.generateMediaId();
				}

				// let's make sure the gradient toggle is updating on page changes, we don't
				// want gradients automatically applying to new/existing background media
				$scope._temp.show_gradient_controls = angular.isDefined(newValue.gradient);

				// some pages will have a gradient activated with no settings, this can cause
				// issues for the color picker, so we will set some default here just in case
				if(angular.isDefined(newValue.gradient)) {
					$scope.ngModel.gradient.color = newValue.gradient.color || "#000000";
					$scope.ngModel.gradient.startOpacity = newValue.gradient.startOpacity || 1;
					$scope.ngModel.gradient.endOpacity = newValue.gradient.endOpacity || 0;
					$scope.ngModel.gradient.direction = newValue.gradient.direction || 'horizontal';
				}

				// NOTE: the value of the background color will be in hex format by default,
				// but in order to apply the background color to the CSS gradient styles we
				// need to convert these values to RGB. Do that here:
				if(angular.isDefined(newValue.gradient) && angular.isDefined(newValue.gradient.color)) {
					$scope._temp.colorObject = hexToRgb(newValue.gradient.color);
					$scope._temp.color = $scope._temp.colorObject.r + ',' + $scope._temp.colorObject.g + ',' + $scope._temp.colorObject.b;
				}

				// watch the gradient direction value, so we can convert the result
				// (horizontal or vertical) into the css accepted values (top and left)
				// or alternatively (to right and to bottom)
				if(angular.isDefined(newValue.gradient) && angular.isDefined(newValue.gradient.direction)) {
					$scope._temp.alignment = (newValue.gradient.direction == 'horizontal') ? 'left' : 'top';
					$scope._temp.alignment2 = (newValue.gradient.direction == 'horizontal') ? 'to right' : 'to bottom';
				}
			}, true);

			// watch for changes in backgroundMedia that result from the user interacting
			// with the mediaSlot controls and pack the information into the course data
			$scope.$watch('backgroundMedia', function(newValue, oldValue) {

				if(angular.isUndefined(newValue)) { return }

				// image src is a string, video src is an object with video/image values
				switch(newValue.type) {
					case 'image':
						$scope.ngModel.src = newValue.image[0];
						break;
					case 'video':
						$scope.ngModel.src = {
							video: newValue.video,
							image: newValue.image[0]
						}
						break;
				}

				// watch to see when the backgroundMedia object has been translated, and pass
				// that translation value to the main data object
				$scope.ngModel.translated = newValue.translated;
			}, true);

			// we can not immediately show the gradient preview, because the containers
			// may not exist on the page yet, and we have no control of them here
			$scope._temp.show_gradient_overlay = false;
			$scope._temp.preview_icon = 'visibility';

			// toggle the view of the gradient preview without affecting the gradient
			// data. useful if a user simply wants to focus on the image without the
			// gradient covering it up
			$scope.togglePreview = function(ev) {
				var parent = $(ev.currentTarget).closest('.background-image');

				// because the gradient preview container is not a part of the media object
				// we have defined the container outside of the media container, and will
				// need to move it so it can overlay the preview image. Further, because the
				// media preview object does not always exist, we need to manually call the
				// function to prevent errors
				$scope.gradientPreviewElement = parent.find(".gradient-preview");
				if ($scope.gradientPreviewElement.length < 1) {
					throw 'Could not find gradient preview element!';
				}
				$scope.previewContentElement = parent.find(".preview-content");
				if ($scope.previewContentElement.length < 1) {
					throw 'Could not find preview content element!';
				}
				$scope.gradientPreviewElement.toggleClass('hidden').appendTo($scope.previewContentElement);

				$scope._temp.show_gradient_overlay = !$scope._temp.show_gradient_overlay;
				$scope._temp.preview_icon = ($scope._temp.show_gradient_overlay) ? 'visibility_off' : 'visibility';
			}

			function hexToRgb(hex) {
		  	var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		    return result ? {
		        r: parseInt(result[1], 16),
		        g: parseInt(result[2], 16),
		        b: parseInt(result[3], 16)
		    } : null;
			}

			$scope.showGradientWarning = function(ev) {
				if($scope._temp.show_gradient_controls) {
					// turning on gradient - no warning dialog necessary, define defaults
					$scope._temp.show_gradient_controls = true;
					$scope.ngModel.gradient = $scope.ngModel.gradient || {};
					$scope.ngModel.gradient.color = "#000000";
					$scope.ngModel.gradient.startOpacity = 1;
					$scope.ngModel.gradient.endOpacity = 0;
				} else {
					$scope._temp.show_gradient_controls = true;
			    // Appending dialog to document.body to cover sidenav in docs app
			    var confirm = $mdDialog.confirm()
			      .title('Warning')
			      .textContent('If you turn off the gradient, you will lose any existing gradient settings you have already entered.')
			      .ariaLabel('Warning')
			      .targetEvent(ev)
			      .ok('Continue')
			      .cancel('Cancel');
			    $mdDialog.show(confirm).then(function() {
			    	$scope._temp.show_gradient_controls = false;
			      delete $scope.ngModel.gradient;
			    }, function() {
			      // do nothing. action cancelled
			    });
			  }
		  }

			$scope.backgroundOptions = [
				{"key": "color", "name": "Color"},
				{"key": "image", "name": "Image"},
				{"key": "video", "name": "Video"},
				{"key": undefined, "name": "None"}
			];
		}
	}
});

chameleonDir.directive('summaryObject', function() {
	return {
		restrict: 'E',
		scope: {
			item:             "=ngModel",
			template_settings: "=templateSettings"
		},
		templateUrl: base_url + 'angular/add_summary.php',
		replace: true,
		controller: function($scope) {

			// if scored/unscored is set in the JSON, disable the toggle
			$scope.$watch('template_settings', function(newValue, oldValue) {
				if(angular.isDefined($scope.template_settings.summary.scored)) {
					$scope.disableScoreToggle = true;
					$scope.item.scored = $scope.template_settings.summary.scored;
				} else {
					$scope.disableScoreToggle = false;
					$scope.item.scored = (angular.isDefined($scope.item.scored)) ? $scope.item.scored : true;
				}

				// some pagetypes only support initial attempt summary's, so we'll set attempts to 1 and lock it
				if(angular.isDefined($scope.template_settings.summary.activityAttempts)) {
					$scope.disableActivityAttempts = true;
					$scope.item.activityAttempts = $scope.template_settings.summary.activityAttempts;
				} else {
					$scope.disableActivityAttempts = false;
				}

				//Should we show mastery score
				if(angular.isDefined($scope.template_settings.summary.masteryScore)) {
					$scope.showMasteryScore = $scope.template_settings.summary.masteryScore;
				} else {
					$scope.showMasteryScore = true;
				}

				//Should we show quiz timer?
				if(angular.isDefined($scope.template_settings.summary.quizTimer)) {
					$scope.showQuizTimer = $scope.template_settings.summary.quizTimer;
				} else {
					$scope.showQuizTimer = true;
				}

				// let's limit the number of media objects per summary to one, unless explicitly set otherwise in the JSON
				$scope.template_settings.summary = $scope.template_settings.summary || {};
				$scope.template_settings.summary.media = $scope.template_settings.summary.media || {};
				$scope.template_settings.summary.media.limit = $scope.template_settings.summary.media.limit || 1; 

			}, true);

			$scope.item.summary = (angular.isArray($scope.item.summary) || angular.isUndefined($scope.item.summary)) ? {} : $scope.item.summary;

			function typeCheck(item) {
				return (item && !angular.isArray(item)) ? item : {};
			}

			// let's waste some time converting arrays back to the objects we originally defined.
			$scope.item.summary.initial = typeCheck($scope.item.summary.initial);
			$scope.item.summary.initial.pass = typeCheck($scope.item.summary.initial.pass);
			$scope.item.summary.initial.fail = typeCheck($scope.item.summary.initial.fail);
			$scope.item.summary.initial.timeout = typeCheck($scope.item.summary.initial.timeout);
			$scope.item.summary.final = typeCheck($scope.item.summary.final);
			$scope.item.summary.final.pass = typeCheck($scope.item.summary.final.pass);
			$scope.item.summary.final.fail = typeCheck($scope.item.summary.final.fail);
			$scope.item.summary.final.timeout = typeCheck($scope.item.summary.final.timeout);
			$scope.item.summary.unscored = typeCheck($scope.item.summary.unscored);

			$scope.attempts = [
				{key: 1, name: '1'},
				{key: 2, name: '2'},
				{key: 3, name: '3'},
				{key: 4, name: '4'},
				{key: 'infinite', name: 'Infinite'}
			];

			$scope.summary_type = 'initial';

			$scope.changeType = type => $scope.summary_type = type;
		}
	}
});

chameleonDir.directive('summaryItem', function() {
	return {
		restrict: 'E',
		scope: {
			ngModel: 						 '=',
			editable:            '=?',
			hideNonTranslatable: '=?',
			internalFlex:        '=?',
			miscOptions:         '=?',
			translation:         '=?'
		},
		templateUrl: base_url + "js/angular/templates/objects/summary.html",
		replace: true,
		link: function(scope, elem, attr) {
			scope.label = attr.label;
			scope.ngModel = scope.ngModel || {};
			scope.ngModel.media = scope.ngModel.media || [];
			scope.template_settings = scope.$parent.template_settings.summary;

			scope.hideMedia = angular.isUndefined(scope.template_settings.media.enable) || scope.template_settings.media.enable;
		}
	}
});

chameleonDir.directive('commonTerms', function() {
	return {
		restrict: 'E',
		scope: {
			ngModel:      '=',
			pageTerms:    '=?',
			filter:       '=?',
			commonTermTrans:  '=?',
			shortcode:    '=?'
		},
		template: function(elem, attr) {
			if(attr.commonTermTrans) {
				return `
					<md-content layout="column" ng-repeat="item in sorted_term_list" class="md-whiteframe-1dp md-margin">
						<md-content layout="row" class="translation-row">
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<span>{{item.term}}</span>
							</div>
							<div flex="50" layout="row" layout-align="start center" class="md-padding">
								<input flex type="text" ng-model="page_data.translation[item.key]" class="form-control">
							</div>
						</md-content>
					</md-content>
				`
			} else {
				return `
					<md-content>
						<md-content class="md-padding" ng-if="filter">
							<span ng-if="!q" class="selected">SHOW ALL</span>
							<a ng-if="q" ng-click="selectLetter()">SHOW ALL</a>
							<span style="display: inline;" ng-repeat="letter in getLetters()"> |
								<span ng-if="letter == q || !hasTermsStartingWithLetter(letter)" ng-class="getClass(letter)">{{letter}}</span>
								<a ng-if="letter != q && hasTermsStartingWithLetter(letter)" ng-click="selectLetter(letter)">{{letter}}</a>
							</span>
						</md-content>
						<md-content layout="column" layout-fill layout-margin class="md-padding">
							<md-card ng-repeat="item in sorted_term_list | filter:q:startsWith">
								<md-card-content layout="row">
									<md-input-container flex>
										<label>{{item.term}}</label>
										<input ng-model="ngModel[item.key]" ng-disabled="!hasOverride(item.key)" placeholder="{{item.term}}">
									</md-input-container>
									<md-button ng-if="pageTerms && hasOverride(item.key)" class="md-primary" ng-click="setGlobal(item.key)">Set as Global</md-button>
									<md-button ng-if="hasOverride(item.key)" class="md-primary" ng-click="restoreDefault(item.key)">Restore Default</md-button>
									<md-button ng-if="!hasOverride(item.key)" ng-click="editTerm(item.key)">Edit</md-button>
								</md-card-content>
							</md-card>
						</md-content>
					</md-content>
				`
			}
		},
		controller: function($rootScope, $scope, $filter, CommonTerms) {
			CommonTerms.then((data) => {

				// master common terms list
				$scope.master_terms = data.data.common_terms;

				// scope for the letter a user will filter by
				$scope.q = undefined;

				// check to make sure common_terms exists and is an object
				if(angular.isUndefined($scope.ngModel) || (angular.isDefined($scope.ngModel) && angular.isArray($scope.ngModel))) {
					$scope.ngModel = {};
				}

				// filter common terms by a user selected letter
				$scope.startsWith = (data, input) => {
					if(angular.isUndefined(data.term)) { return }
					return data.term.toLowerCase().indexOf(input.toLowerCase()) === 0
				}

				// if we are on the common terms page we will display the full list of
				// common terms for editing. if we are on a specific page however, we will
				// only display those terms which have been defined for that page.
				const common_term_array = [];
				if($scope.commonTermTrans) {
					// if we are in the translation editor, we must show all the common
					// terms whether they were modified in english or not. if they were
					// modified in english, we will need to set the modified value on the
					// english side. for all other, we will use the default values.
					//
					// we must also compile a translated list, checking the translation
					// data to see if a term has been translated already and if not, using
					// the default translation values as a default.
					if($scope.pageTerms) {
						angular.forEach($scope.pageTerms, (value, key) => {
							let term = value || $scope.master_terms[key].term;
							common_term_array.push({key, term});
						});
					} else {
						angular.forEach($scope.master_terms, (value, key) => {
							let term = $scope.ngModel[key] || value.term;
							common_term_array.push({key, term});

							$scope.commonTermTrans[key] = angular.isDefined($scope.commonTermTrans[key]) ? $scope.commonTermTrans[key] : value.language[$scope.shortcode];
						});
					}
				}
				else if($scope.pageTerms) {
					angular.forEach($scope.pageTerms, (key) => {
						let term = $rootScope.data.common_terms[key] || $scope.master_terms[key].term;
						common_term_array.push({key, term});
					});
				}
				else {
					angular.forEach($scope.master_terms, v => { common_term_array.push({'key':v.key, 'term':v.term}) });
				}

				// make sure master common term list is alphabetical
				$scope.sorted_term_list = $filter('orderBy')(common_term_array, 'term');

				// allows a user to edit a custom term to override the default value
				$scope.editTerm = function(key) {
					let term = common_term_array.find(item => { return item.key == key });
					$scope.ngModel[key] = term.term;
				}

				// checks the course data to see if a user has overridden the default value
				$scope.hasOverride = function(key) {
					if(angular.isUndefined($scope.ngModel)) { return }
					return angular.isDefined($scope.ngModel[key]);
				}

				// returns a common term to its default value
				$scope.restoreDefault = function(key) {
					angular.forEach($scope.ngModel, function(v, k) {
						if(key === k) { delete $scope.ngModel[k] }
					});
				}

				$scope.setGlobal = function(key) {
					$rootScope.data.common_terms[key] = $scope.ngModel[key];
					delete $scope.ngModel[key];
				};

				// a function that returns a list of capital letters in order from A to Z
				$scope.getLetters = function() {
					var ret = [];
					for (var letterCode = 'A'.charCodeAt(0); letterCode <= 'Z'.charCodeAt(0); letterCode++) {
						ret.push(String.fromCharCode(letterCode));
					}
					return ret;
				};

				// serves as a helper function to the template; when the letter passed in is
				// the currently selected letter (if any), it returns 'selected'; otherwise,
				// it returns boolean false
				$scope.getClass = function(letter) {
					if (angular.isUndefined(letter)) {
						return false;
					}
					if (angular.isUndefined($scope.selected_letter)) {
						return false;
					}
					if (letter.toLowerCase() != $scope.selected_letter.toLowerCase()) {
						return false;
					}
					return 'selected';
				};

				// responds to a click event where the user changes which letter of common
				// terms we're showing
				$scope.selectLetter = function(letter) {
					$scope.selected_letter = letter;
					$scope.q = letter;
				};

				// tells us whether or not there are terms that begin with the specified letter
				$scope.hasTermsStartingWithLetter = function(letter) {
					return common_term_array.filter(item => { return item.term.charAt(0).toLowerCase() == letter.toLowerCase() }).length > 0;
				};

			});
		},
		replace: true
	}
});