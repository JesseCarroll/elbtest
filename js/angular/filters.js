var chameleonFilters = angular.module("chameleonFilters", []);

// included for development purposes
chameleonFilters.filter('prettyJSON', function () {
	function prettyPrintJson(json) {
		return JSON ?
			JSON.stringify(json, null, '  ') :
			'your browser doesnt support JSON so cant pretty print';
	}
	return prettyPrintJson;
});

chameleonFilters.filter("asDate", function () {
	return function (input) {
		return new Date(input);
	}
});

chameleonFilters.filter('capitalize', function() {
	return function(input) {
		if(input) {
			var isNumber = input.match(/[0-9]*/);
			if(isNumber[0]) {
				return input;
			}

			var result = '';

			if(input) {
				input = input.toString();
				input = input || '';

				// what if our input is camelcaps?
				var parts = input.match(/([a-zA-Z{1}][a-z]*[^A-Z]|[A-Z]*)/g);
				for(p in parts) {
					result += parts[p] + ' ';
				}

				input = result.trim().split(' ');
				for(x in input) {
					input[x] = input[x].substring(0,1).toUpperCase()+input[x].substring(1);
				}
				return input.join(' ');
			}
		}
	}
});

chameleonFilters.filter('countObject', function() {
	return function(items) {
		var count = {length: 0};
		for (var item in items) {
			count.length = count.length+1;
		}
		return count.length;
	}
});

chameleonFilters.filter('groupBy', ['$parse', function ($parse) {
	return function (list, group_by) {

		var filtered = [];
		var prev_item = null;
		var group_changed = false;
		// this is a new field which is added to each item where we append "_CHANGED"
		// to indicate a field change in the list
		//was var new_field = group_by + '_CHANGED'; - JB 12/17/2013
		var new_field = 'group_by_CHANGED';

		// loop through each item in the list
		angular.forEach(list, function (item) {

			group_changed = false;

			// if not the first item
			if (prev_item !== null) {

				// check if any of the group by field changed

				//force group_by into Array
				group_by = angular.isArray(group_by) ? group_by : [group_by];

				//check each group by parameter
				for (var i = 0, len = group_by.length; i < len; i++) {
					if ($parse(group_by[i])(prev_item) !== $parse(group_by[i])(item)) {
						group_changed = true;
					}
				}


			}// otherwise we have the first item in the list which is new
			else {
				group_changed = true;
			}

			// if the group changed, then add a new field to the item
			// to indicate this
			if (group_changed) {
				item[new_field] = true;
			} else {
				item[new_field] = false;
			}

			filtered.push(item);
			prev_item = item;

		});

		return filtered;
	};
}]);

chameleonFilters.filter("plainText", function() {
	return function(text) {
		return String(text).replace(/<[^>]+>/gm, '');
	}
});

chameleonFilters.filter("pageType", function() {
	return function (input, check) {
		var template = '';

		if(isNaN(input)) {
			template = 'quiz';
		}
		else if(input[0] == 2) {
			template = 'basic';
		}
		else {
			template = 'interaction';
		}

		if(template == check) {
			return true;
		}
	}
});

chameleonFilters.filter('removeObject', function() {
	return function(input) {
		if(input && input.indexOf('object:') > -1) {
			return input.replace('object:', '');
		}
	}
});

chameleonFilters.filter('removeTimestamp', function() {
	return function(input) {
		input = input.replace(/\?.+$/g, '');
		return input;
	}
});

chameleonFilters.filter("statusName", function() {
	return function(input) {
	  if(input == '1') {
		  return 'Active';
	  } else {
		  return 'Inactive';
	  }
	}
});

chameleonFilters.filter("title", function() {
	return function (input) {
		return input.replace('_',' ');
	}
});

/* trust URL for injection */
chameleonFilters.filter('trusted_url', ['$sce', function($sce) {
	return function(text) {
		return $sce.trustAsResourceUrl(text);
	};
}])

chameleonFilters.filter('toNumber', function() {
	return function(string) {
		return string.replace(/[^\d.-]/g,'');
	}
});

chameleonFilters.filter('toHex', function() {
	return function(string, scope) {
		var value = scope[string];
		if(value.indexOf('@') == 0) {
			return scope[value.slice(1)];
		} else {
			return value;
		}
	}
});

chameleonFilters.filter('dateFormat', function() {
	return function() {

		var date = new Date();

		dateFormat = function(n) {
			return (n < 10) ? '0'+n : n;
		}

		return date.getFullYear() + '-' + dateFormat(date.getMonth()+1) + '-' + dateFormat(date.getDate()) + ' ' + dateFormat(date.getHours()) + ':' + dateFormat(date.getMinutes()) + ':' + dateFormat(date.getSeconds());
	}
});

chameleonFilters.filter('analyticsTimeFormat', function() {
	return function(time) {
		if(!time) return;

		return (parseInt(time) / 60).toFixed(2).replace('.',':');
	}
});