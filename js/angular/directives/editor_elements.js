var editorElements = angular.module("editorElements", []);

//audio
editorElements.directive('addAudio', function() {
  return {
		restrict: 'E',
		scope: {
			ngModel: "=",
			posterImage: "="
		},
		templateUrl: base_url + 'js/angular/templates/media/add_audio.html',
		replace: true,
		link: function(scope, element, attrs) {

			scope.id = 'preview' + scope.$root.random;

			scope.initializeAudio = function() {
				var player = videojs(scope.id);
			};

			scope.$watch('posterImage', function(newValue, oldValue) {
				if (newValue && newValue !== oldValue) {
					scope.image = scope.$root.imagePath + scope.posterImage;
				} else {
					scope.image = scope.$root.placeholder_image;
				}
				scope.updatePoster();
			});

			scope.$watch('ngModel', function(newValue, oldValue) {
				if (newValue && newValue !== oldValue) {
					scope.audio = scope.$root.videoPath + scope.ngModel;
				} else {
					scope.audio = scope.$root.placeholder_audio;
				}
				scope.updateSrc();
			});

			scope.updatePoster = function() {
				var player = videojs(scope.id);
				player.poster(scope.image);
				player.load();
			};

			scope.updateSrc = function() {
				var player = videojs(scope.id);
				player.src(scope.audio);
			};

			scope.removeImage = function() {
				scope.posterImage = null;
			};

			scope.removeAudio = function() {
				scope.ngModel = null;
			};
		}
  };
});

//chart
editorElements.directive('addChart', function() {
  var controller = function($scope) {
		$scope.style_list = ['bar', 'donut', 'line', 'pie', 'polar', 'radar'];
		if (angular.isUndefined($scope.disableFields)) {
			$scope.disableFields = false;
		}
		if (angular.isUndefined($scope.noTranslate)) {
			$scope.noTranslate = false;
		}
  };

  return {
		restrict: "E",
		templateUrl: base_url + 'js/angular/templates/media/add_chart.html',
		controller: controller,
		scope: {
			disableFields: '=?',
			noTranslate:   '=?',
		}
  };
});

//checkbox
editorElements.directive('addCheckbox', function() {
});

//colorpicker
editorElements.directive('addColorpicker', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: "=",
			pickerSettings: "="
		},
		templateUrl: base_url + 'js/angular/templates/inputs/add_colorpicker.html',
		replace: true,
		link: function(scope, element, attrs) {
			scope.disabled = (attrs.disabled) ? true : false;
			scope.label = attrs.label;
			scope.editable = (attrs.editable=="false") ? false : true;
		}
	}
});

//hotspot
editorElements.directive('addHotspot', function() {
	var controller = function($scope, $rootScope, $mdDialog) {

		$scope.error = false;
		$scope.errorMsg = "Hotspots require an image. Please ensure you have an image uploaded in the Media tab before proceeding.";
		$scope.ngModel['x_position'] = $scope.ngModel['x_position'] || 50;
		$scope.ngModel['y_position'] = $scope.ngModel['y_position'] || 50;

		// check if a media element has been set at the main level
		// and set it to the current scope if it has
		$scope.$watch('mainMedia', function(newValue, oldValue) {
			if(newValue && newValue[0] && newValue[0].image && newValue[0].image[0]) {
				$scope.error = false;
				$scope.media = $rootScope.mediaRoot + newValue[0].image[0];
			} else {
				$scope.error = true;
			}
		}, true);

		$scope.getPosition = function(ev) {

			var targetHeight = ev.currentTarget.clientHeight;
			var targetWidth = ev.currentTarget.clientWidth;

			$scope.ngModel['x_position'] = Math.round((ev.offsetX) * 100 / targetWidth);
			$scope.ngModel['y_position'] = Math.round((ev.offsetY) * 100 / targetHeight);
		}

	  $scope.hotspotDialog = function(ev) {
	    $mdDialog.show({
	      controller: DialogController,
	      templateUrl: base_url + 'js/angular/templates/modals/hotspot.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      scope: $scope,
	      preserveScope: true,
	      clickOutsideToClose:true,
	      fullscreen: true
	    })
	    .then(function(answer) {
	      $scope.status = 'You said the information was "' + answer + '".';
	    }, function() {
	      $scope.status = 'You cancelled the dialog.';
	    });
	  };

		function DialogController($scope, $mdDialog) {
		  $scope.hide = function() {
		    $mdDialog.hide();
		  };
		  $scope.cancel = function() {
		    $mdDialog.cancel();
		  };
		  $scope.answer = function(answer) {
		    $mdDialog.hide(answer);
		  };
		}
	}
	return {
		restrict: 'E',
		scope: {
			ngModel: 		"=",
			mainMedia: 	"="
		},
		template: `
			<md-button class="md-raised" ng-disabled="disabled" ng-click="hotspotDialog($event)">Set Hotspot Position</md-button>
		`,
		controller: controller,
		link: function(scope, el, attr) {
			scope.disabled = attr.disabled=='true' ? true : false;
		}
	}
});

//image
editorElements.directive('addImage', function() {
  return {
		restrict: 'E',
		scope: {
			ngModel: "="
		},
		templateUrl: base_url + 'js/angular/templates/media/add_image.html',
		link: function(scope, element, attrs) {

		  scope.$watch('ngModel', function(newValue, oldValue) {
				if (newValue) {
					scope.image = scope.$root.imagePath + scope.ngModel;
				} else {
					scope.image = scope.$root.placeholder_image;
				}
		  });

		  scope.removeImage = function() {
			scope.ngModel = null;
		  }
		}
  }
});

//stockImage
editorElements.directive('addStockImage', function() {
	var controller = function($scope, $rootScope, $mdDialog, $http, Media) {
		$scope.images = [];
		$scope.searching = false;
		$scope.imageProvider = "unsplash";
		$scope.$watch('searchTerm', function (tmpStr){
	      if (!tmpStr || tmpStr.length == 0)
	        return 0;
	        // if searchTerm is still the same..
	        // go ahead and retrieve the data
	        if (tmpStr === $scope.searchTerm){
	        	var term = window.encodeURIComponent($scope.searchTerm);
	        	searchImages(term);
	        }
	    });
	    $scope.$watch('imageProvider', function (newValue, oldValue){
	    	if (newValue != oldValue){
	    		var term = window.encodeURIComponent($scope.searchTerm);
	        	searchImages(term);
	    	}
	    });
		$scope.stockImageDialog = function(ev) {
			$mdDialog.show({
				controller: DialogController,
				templateUrl: base_url + 'js/angular/templates/modals/stock_images.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				scope: $scope,
				preserveScope: true,
				clickOutsideToClose:true,
				fullscreen: true
			});
		};

		function searchImages(term){
			$scope.searching = true;
			if ($scope.imageProvider=="unsplash"){
        		var apibase = "https://api.unsplash.com/search/photos?client_id=cb05b944499659750e7076ef3f26f2b07f08bb112d6aa6d5ad56f31d94e0c8bf&per_page=50&query=";
        	} else if ($scope.imageProvider=="pixabay"){
        		var apibase = "https://pixabay.com/api/?key=5750974-ddd1e6b0138ce4c3556276ce8&response_group=high_resolution&per_page=200&q=";
        	}
			$http.get(apibase + term).success(function(data) {
	          	$scope.images = [];
	          	$scope.searching = false;
	            if ($scope.imageProvider=="unsplash"){
	            	angular.forEach(data.results, function(value, key) {
					  var item = {};
					  item.avatarImage = value.user.profile_image.medium;
					  item.avatarHeader = value.user.name;
					  item.profileLink = value.user.links.html+"?utm_source=Chameleon&utm_medium=referral&utm_campaign=api-credit";
					  item.providerURL = 'https://unsplash.com';
					  item.subHeader = "Unsplash";
					  item.thumb = value.urls.thumb;
					  item.full = value.urls.raw+"?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=1920&fit=max";
					  $scope.images.push(item);
					});
	            } else if ($scope.imageProvider=="pixabay"){
	            	angular.forEach(data.hits, function(value, key) {
					  var item = {};
					  item.avatarImage = value.userImageURL;
					  item.avatarHeader = value.user;
					  item.profileLink = "https://pixabay.com/en/users/"+value.user;
					  item.providerURL = 'https://pixabay.com';
					  item.subHeader = "Pixabay";
					  item.thumb = value.webformatURL;
					  item.full = value.fullHDURL;
					  $scope.images.push(item);
					});
	            }
	        });
		}

		function DialogController($scope, $mdDialog, Media) {
			$scope.hide = function() {
				$mdDialog.hide();
			};
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			$scope.select = function(image) {

				if (angular.isDefined($scope.media.id)) {
					var mediaURL = $rootScope.mediaRoot
				} else {
					var mediaURL = $rootScope.imagePath;
				}

				var saveURL = mediaURL + $scope.media.id +".jpg";

				// isolate the filename of the edited image
				var target_filepath = saveURL.replace(/^https?:\/\/[^\/]+\/[^\/]+\/(.*?)(?:\?.*)?$/, '$1');

				// call a web service that will download new version of the image to
				// the course's image directory; since we're targeting the same
				// filename, this will OVERWRITE the original image
				Media.download_to_course({}, {
					course: $rootScope.editor.course.id,
					url: image,
					target_filepath: target_filepath
				})

				// when the copy operation is done, invoke the callback function,
				// which should be responsible for updating the display
				.$promise.then(function() {
					$scope.media.image[0] = $scope.media.id +".jpg";
					$scope.clearUrlCache();
					$mdDialog.hide();
				});
			}
		}
	}
	return {
		restrict: 'E',
		template: `
			<md-button ng-hide="uploadStatus[0].state" class="md-button add" ng-click="stockImageDialog($event)">
				<md-icon>photo_library</md-icon>
				<div class="md-grid-text">Search Content Library</div>
			</md-button>
		`,
		controller: controller,
		link: function(scope, el, attr) {
			scope.disabled = attr.disabled=='true' ? true : false;
		}
	}
});

//number
editorElements.directive('addNumber', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: "="
		},
		templateUrl: base_url + 'js/angular/templates/inputs/add_number.html',
		replace: true,
		link: function(scope, element, attrs) {
			scope.disabled = (attrs.disabled) ? true : false;
			scope.label = attrs.label;
			scope.ngModel = scope.ngModel || attrs.placeholder;
		}
	}
});

//radio
editorElements.directive('addRadio', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: "=",
			radioList: "="
		},
		templateUrl: base_url + 'js/angular/templates/inputs/add_radio.html',
		replace: true,
		link: function(scope, element, attrs) {
			scope.label = attrs.label;
		}
	}
});

//select
editorElements.directive('addSelect', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: "=",
			selectList: "=",
			multiple: "=?"
		},
		templateUrl: function(elem, attr) {
			if(attr.multiple) {
				return base_url + 'js/angular/templates/inputs/add_multiselect.html'
			} else {
				return base_url + 'js/angular/templates/inputs/add_select.html'
			}
		},
		replace: true,
		link: function(scope, element, attrs) {
			scope.disabled = (attrs.disabled=='true') ? true : false;

			scope.label = attrs.label;
			scope.multiple = scope.multiple == true;

			const mapping = {};

			// make sure select list is in a key/value object
			scope.newSelect = [];

			angular.forEach(scope.selectList, function(value, key) {
				key = (angular.isUndefined(value.key)) ? value : value.key;
				name = (angular.isUndefined(value.key)) ? value : value.name || value.title || value.label;
				scope.newSelect.push({ key, name });
				mapping[key] = name;
			});

			scope.isMultiple = function() {

				var selected = (scope.ngModel) ? scope.ngModel.filter(item => { return item !== null }) : [];

				switch(selected.length) {
					case 0:
						return scope.label;
						break;
					case 1:
						return mapping[scope.ngModel];
						break;
					default:
						return '<i>Multiple Selected</i>';
				}
			};
		}
	}
});

//slider
editorElements.directive('addSlider', function() {

});


//switch
editorElements.directive('addSwitch', function() {
	return {
		retrict: "E",
		scope: {
			ngModel: "="
		},
		templateUrl: base_url + 'js/angular/templates/inputs/add_switch.html',
		replace: true,
		link: function(scope, element, attrs) {
			scope.label = attrs.label;
			scope.default = (attrs.default == 'on') ? true : false;
			scope.ngModel = angular.isDefined(scope.ngModel) ? scope.ngModel : scope.default;
			scope.switchTrue = (attrs.true) ? attrs.true : true;
			scope.switchFalse = (attrs.false) ? attrs.false : false;
			scope.editable = (attrs.editable=="false") ? false : true;
		}
	};
});

//text
editorElements.directive('addText', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: '=',
		},
		templateUrl: base_url + 'js/angular/templates/inputs/add_text.html',
		replace: true,
		link: function(scope, element, attrs) {
			switch (typeof attrs.disabled) {
				case 'string':
					scope.disabled = ('true' === attrs.disabled);
					break;
				case 'undefined':
				default:
					scope.disabled = false;
					break;
			}
			scope.ngModel = scope.ngModel || attrs.placeholder;
			scope.label = attrs.label;

			// When a single line text input field is disabled and contains so much
			// text that the width of the control does not allow the user to see all
			// of the text, it can be difficult and counterintuitive to horizontally
			// scroll the text in order to see all of it.
			//
			// To make it easier to see all of the text in this situation, we detect
			// when there is more text in the input than can be shown in the box and,
			// on mousing over it, offer the user a button that they can use to
			// "expand" the control from an input[type="text"] into a textarea.
			scope.expand = false;
			$(element[0]).mouseenter(function() {
				var result;

				// only offer the toggle button on disabled text inputs
				if (!scope.disabled) {
					return;
				}

				// find the input[type="text"] or textarea element
				result = $(this).find('input[type="text"],textarea');
				if (0 == result.length) {
					return;
				}
				var textInput = result[0];

				// do not offer the toggle button when there is too much text to show
				if (!scope.expand && textInput.scrollWidth <= textInput.offsetWidth) {
					return;
				}

				// create the toggle button
				var expandCollapseButton = document.createElement('i');
				expandCollapseButton.style.position = 'absolute';
				expandCollapseButton.style.top = 0;
				expandCollapseButton.style.right = 0;
				expandCollapseButton.style.cursor = 'pointer';
				expandCollapseButton.className = 'material-icons long-input';

				// keep toggle button updated
				var updateExpandCollapseIcon = function(element) {
					element.innerHTML = scope.expand ? 'expand_less' : 'expand_more';
					element.title = scope.expand ? 'Collapse' : 'Expand';
					scope.$apply();
				};
				updateExpandCollapseIcon(expandCollapseButton);
				$(expandCollapseButton).click(function() {
					scope.expand = !scope.expand;
					updateExpandCollapseIcon(this);
				});

				// add the toggle button to the DOM
				element[0].appendChild(expandCollapseButton);
			});

			$(element[0]).mouseleave(function() {
				// destroy the toggle button if present
				var result = $(this).find('i.long-input');
				if (0 == result.length) {
					return;
				}
				result.each(function() {
					$(this).remove();
				});
			});
		}
	};
});

//textarea
editorElements.directive('addTextarea', function() {
	return {
		restrict: "E",
		scope: {
			ngModel: "="
		},
		template: `
			<div>
				<label>{{label}}</label>
	      <md-input-container flex class="md-block">
	        <textarea ck-editor ng-model="ngModel" columns="1" md-maxlength="maxlength" ng-disabled="disabled" aria-label="label"></textarea>
	      </md-input-container>
	    </div>
    `,
		replace: true,
		link: function(scope, element, attrs) {
			scope.label = attrs.label;
			scope.disabled = (attrs.disabled) ? true : false;
			scope.maxlength = -1;
		}
	};
});

// CUSTOM FIELDS
// since ngModel can be the parent page, or an interaction, or quiz, we need
// another way to pass in the parent media object, if we are not already at
// that level. to do this we will optionally pass in mainMedia.
editorElements.directive('customFields', function() {
	return {
		restrict: "A",
		scope: {
			ngModel: 		"=",
			mainMedia: 	"=?"
		},
		replace: true,
		template:`
			<div layout="column">
				<add-text ng-if="string" ng-model="ngModel[key]" data-label="{{label}}"></add-text>
				<add-select disabled="{{disabled}}" ng-if="select" ng-model="ngModel[key]" data-label="{{label}}" select-list="list"></add-select>
				<add-colorpicker ng-if="color" ng-model="ngModel[key]" data-label="{{label}}"></add-colorpicker>
				<add-switch ng-if="boolean" ng-model="ngModel[key]" data-label="{{label}}"></add-switch>
				<md-datepicker ng-if="date" ng-model="ngModel[key]" data-label="{{label}}" md-placeholder="Enter date"></md-datepicker>
				<add-number ng-if="percentage || integer" ng-model="ngModel[key]" data-label="{{label}}"></add-number>
				<add-hotspot disabled="{{disabled}}" ng-if="hotspot" ng-model="ngModel" main-media="mainMedia" data-label="{{label}}"></add-hotspot>
				<add-select ng-if="multi" ng-model="ngModel" data-label="{{label}}" select-list="list" multiple="true"></add-select>
			</div>
		`,
		link: function(scope, el, attr) {
			var data = JSON.parse(attr.customFields);

			scope.key = data.key;
			scope.label = data.label;
			scope.list = data.values;
			scope[data.type] = true;
			scope.disabled = attr.disabled ? true : false;
		}
	}
});