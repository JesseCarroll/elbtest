var chameleonServices = angular.module("chameleonServices", []);

chameleonServices.service('randomStringService', function() {
	this.generate = function(string_length) {
		var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghiklmnopqrstuvwxyz';
		var alphanum = chars + '1234567890';
		string_length = string_length || 16;
		var randomstring = '';
		for (var i = 0; i < string_length; i++) {
			if (0 == i) {
				var rnum = Math.floor(Math.random() * chars.length);
				randomstring += chars.substring(rnum, rnum + 1);
			}
			else {
				var rnum = Math.floor(Math.random() * alphanum.length);
				randomstring += alphanum.substring(rnum, rnum + 1);
			}
		}
		return randomstring;
	}
});

chameleonServices.service('mediaService', function($filter, randomStringService) {
	this.allMediaTypes = {
		'audio'       : 'Audio',
		//'chart'       : 'Chart',
		'before-after': 'Before-after',
		'fpo'         : 'Fpo',
		'image'       : 'Image',
		'gallery'     : 'Gallery',
		'pullquote'   : 'Pullquote',
		'slideshow'   : 'Timed Carousel',
		'textBlock'   : 'Text Block',
		'video'       : 'Video',
		'zoom'        : 'Zoom',
		'adobe-edge'  : 'Adobe Animate',
		'embed'       : 'Embed'
	};

	this.getMediaTypeLabel = function(mediaTypeId) {
		if (angular.isUndefined(mediaTypeId)) {
			return '';
		}
		if (angular.isDefined(this.allMediaTypes[mediaTypeId])) {
			return this.allMediaTypes[mediaTypeId];
		}
		return $filter('capitalize')(mediaTypeId);
	};

	this.getAllMediaTypes = function() {
		return angular.copy(this.allMediaTypes);
	};

	this.generateMediaId = function() {
		return randomStringService.generate();
	};

	this.generateMediaFilename = function(id, extension) {
		var tag = randomStringService.generate(4);
		var ret = `${id}-${tag}`;
		if (angular.isDefined(extension)) {
			ret += `.${extension}`;
		}
		return ret;
	};

	this.initMediaListObject = function(mediaList, index, mediaTypeId) {
		// we're modifying an item already present in the media list
		if (angular.isDefined(mediaList[index])) {
			mediaList[index].type = mediaTypeId;
		}

		// we're adding a new item to the end of the media list
		else if (index == mediaList.length) {
			mediaList[index] = {
				'type': mediaTypeId,
				'image': []
			};
		}

		// shouldn't ever get here unless the provided index is bad,
		// so dump out a little debug in case it ever happens
		else {
			console.log(mediaList);
			console.log(index);
		}

		// the zoom media type requires a popup variable to be set to true
		if(mediaTypeId == 'zoom') {
			mediaList[index].popup = true;
		}

		// if the media item does not have a media ID already, assign one --
		// since assigning a media id will CHANGE the location where media
		// assets are expected to be found, we should reset all potential
		// references to media assets so that we don't try to load assets that
		// don't actually exist where we're looking for them
		if (!mediaList[index].id) {
			mediaList[index].id = this.generateMediaId();
			if (angular.isDefined(mediaList[index].image)) {
				mediaList[index].image = [];
			}
			delete mediaList[index].video;
			delete mediaList[index].audio;
			delete mediaList[index].folder;
		}
	};
});

chameleonServices.service('EditorState', function() {

	this.clearData = function(key) {
		delete sessionStorage[key];
	}

	// build ROOT object with stored editor states
	this.buildData = function() {
		this.editor = {};
		if (sessionStorage.client) {
			this.editor.client = {};
			this.editor.client.id = sessionStorage.client;
			if (sessionStorage.clientSection) {this.editor.client.section = sessionStorage.clientSection}
			if (sessionStorage.course) {
				this.editor.course = {};
				this.editor.course.id = sessionStorage.course;
				if (sessionStorage.courseSection) {
					this.editor.course.section = sessionStorage.courseSection
				}
				if (sessionStorage.topic) {
					this.editor.course.topic = sessionStorage.topic
				}
				if (sessionStorage.page) {
					this.editor.course.page = sessionStorage.page
				}
			}
		}

		return this.editor;
	};
});

chameleonServices.service('actions', function() {

	this.aws_path = aws_path;

	this.stage_list = [
		"Development",
		"Post-Production QA",
		"Content QA",
		"PM Review",
		"Client Review",
		"Functional QA",
		"Client Acceptance Review",
		"Live"
	];

	this.curriculum_status = {'0': 'Disabled', '1': 'Active'};

	this.delete_icon = 'X';

	this.addElement = function(scope, inline){
		//$scope.courseContent.$setDirty();
		if(inline) {
			scope.push('');
		}
		else {
			scope.push({});
		}
	};

	this.addMediaItem = function(scope, type) {
		scope.push({'type':type});
	}

	this.removeObjectItem = function(scope, item) {
		delete scope[item];
	}

	this.removeElement = function(scope, i) {
		if(this.confirm_delete){
			//$scope.courseContent.$setDirty();
			if(typeof i == 'object'){
				scope.splice(scope.indexOf(i), 1);
			}
			else {
				scope.splice(i, 1);
			}
		}
		else{
			this.delete_text = 'Delete?';
			this.delete_icon = 'Delete?';
			this.confirm_delete = true;
		}
	};

	this.cancelRemoveElement = function(){
		this.delete_text = 'Delete';
		this.delete_icon = 'X';
		this.confirm_delete = false;
	};
});

chameleonServices.service('TranslationService', function() {

	var total_keys = 0;
	var translated = 0;

	// exceptions are the sections of data that are either not translatable OR need
	// to be copied over to provide english placeholders during translation. there
	// are two groups, both of which need to be excluded from completion calculations.
	// the first group is called 'unlocked_exceptions'. these are the keys that need to
	// be updated in the translation if changes are made to the english data
	// (ex. config settings). the second group is called 'locked_exceptions'. these
	// are the keys that can not be automatically updated once the data exists in
	//  the translation file (ex. image, video).
	//
	// NOTE: we are setting the exceptions list outside of the handleExceptions function
	// so that we can access it outside of the method.
	var locked_exceptions = [
		'audio',
		'audioTrack',
		'gradient',
		'image',
		'src',
		'subtitle',
		'translated',
		'video'
	];

	var unlocked_exceptions = [
		'activityAttempts',
		'animate',
		'attempts',
		'config',
		'disableAnswerHighlight',
		'disableFeedback',
		'external',
		'folder',
		'forceComplete',
		'gate',
		'hls',
		'id',
		'masteryScore',
		'max',
		'mirror',
		'objectives',
		'popup',
		'pool',
		'poolSize',
		'quizTime',
		'randomize',
		'randomizeDistractors',
		'repeat',
		'reverseText',
		'roles',
		'sendToLMS',
		'scored',
		'template',
		'template_name',
		'type',
		'uuid',
		'value'
	];

	function handleExceptions(data, trans, key) {

		var all_exceptions = locked_exceptions.concat(unlocked_exceptions);

		if(all_exceptions.indexOf(key) == -1) return false;

		// if we are dealing with a locked key, check to see if the translation data
		// has been set and if it has not copy the english data.
		// if we are dealing with an unlocked key, we will always copy the english data
		// to make sure we have the latest values.
		if(locked_exceptions.indexOf(key)+1 && angular.isDefined(trans[key]) && trans[key] !== null) {
			switch(typeof trans[key]) {
				case 'string':
					trans[key] = (trans[key] !== undefined || trans[key] !== "") ? trans[key] : data;
					break;
				case 'object':
					trans[key] = (Object.keys(trans[key]).length > 0) ? trans[key] : data;
					break;
			}
		} else {
			trans[key] = data;
		}

		// because (currently) all course environments (dev, stage, prod) use the same
		// translation files, debug can never be true
		if(key == 'config') { trans[key].debug = false }

		return true;
	}

	// there may be cases where a key does not exist in the english data,
	// but is required or expected in the translation data. since, by default,
	// we hide fields in the translation editor if no english data exists for
	// that key, we need to force some fields to contain values so that these
	// fields can be set in the translation.
	function handleSpecialFields(data, trans, key) {

		if(key == 'media') {
			angular.forEach(data, function(value, idx) {

				// when a media type is translated, it will set the translated flag to
				// true. however, if a user goes back and changes the media type on that
				// page, the flag will still be set to true, even though the translated
				// data is no longer accurate or applicable. for this reason, we will check
				// here to make sure the media type matches, and if it does not, we will
				// set the translated flag to false. by setting the translated flag to
				// false, we redisplay the "Replace Media Item" button which copies the
				// english images into the appropriate translation media folder. without
				// going through that process the media does not display correctly in the
				// translation editor.
				if(value.type && trans.media && trans.media[idx] && trans.media[idx].type && (value.type !== trans.media[idx].type)) {
					trans.media[idx].translated = false;
				}

				// in this case, video media objects in english might not contain a
				// subtitle. however, subtitles are very important in translations,
				// so we will set them here to make them visible.
				if(value.type == 'video') {
					if(!angular.isDefined(value.subtitle)) {
						value.subtitle = '';
					} else {
						if(angular.isDefined(trans[key]) && angular.isDefined(trans[key][idx]) && angular.isDefined(trans[key][idx].subtitle) && trans[key][idx].subtitle == '') {
							trans[key][idx].subtitle = value.subtitle;
						}
					}
				}
			}, trans);
		}

		// in cases where a translated resource is stuck with a type when there shouldn't
		// be one, we will remove the type from the translation here
		if(key == 'resources' && angular.isObject(data)) {
			angular.forEach(data, function(value, idx) {
				if(angular.isDefined(trans[key]) && angular.isDefined(trans[key][idx]) && angular.isDefined(trans[key][idx].type) && angular.isUndefined(value.type)) {
					delete trans[key][idx].type;
				}
			});

		}
	}

	// since topics and pages can be added or removed after a translation
	// has started, we need to take special care when looping through the
	// data to make sure we are applying translated data to the correct
	// page, and discarding any translated data that is no longer relevant.
	function handlePages(data, trans, key) {
		if(key !== 'topic' || key !== 'page') return;

		angular.forEach(data, function(original, key) {
			angular.forEach(this, function(translated) {
				trans[key] = (translated && translated.uuid == original.uuid) ? translated : {};
			});
		}, trans);
	}

	function initTranslationData(data, trans, key) {

		// angular.isObject will return true if the item is an object or an array
		if(angular.isObject(data)) {

			// for objects that have already been converted into empty arrays, we need to convert
			// them back to prevent the editor from breaking - so do that here
			if(angular.isDefined(trans[key]) && !angular.isArray(data) && angular.isArray(trans[key])) {
				trans[key] = {};
			} else {
				trans[key] = (angular.isDefined(trans[key])) ? trans[key] : (angular.isArray(data)) ? [] : {};
			}

			// for some reason, empty objects are being converted to empty arrays.
			// not sure if this is being done by angular or php, but we need to
			// address possible incorrect data types here.
			if(key == 'common_terms' || key == 'certificate') {
				if(angular.isArray(trans[key])) { trans[key] = {} }
			}

			angular.forEach(data, function(value, key) {
				var exceptions = handleExceptions(value, this, key);

				// only continue if the key was not an exception, if it was the
				// data is already taken care of.
				if(!exceptions) {
					handlePages(value, this, key);
					handleSpecialFields(value, this, key);
					initTranslationData(value, this, key);
				}
			}, trans[key]);
		}
		else if(data) {
			total_keys++;
			if(trans[key]) {
				translated++;
			}
		}
	}

	this.getExceptionsList = function() {
		return unlocked_exceptions;
	};

	this.startTranslation = function(data, trans) {
		total_keys = 0;
		translated = 0;

		data = angular.copy(data);
		angular.forEach(data, function(value, key) {
			var exceptions = handleExceptions(value, this, key);

			// only continue if the key was not an exception, if it was the data is already taken care of.
			if(!exceptions) {
				handlePages(value, this, key);
				handleSpecialFields(value, this, key);
				initTranslationData(value, this, key);
			}
		}, trans);

		var response = {};
		response.data = trans;
		response.progress = Math.round(100 * translated / total_keys);
		return response;
	};
});

chameleonServices.service('chameleonData', ["$http", "$rootScope", "actions", "Versions", function($http, $rootScope, actions, Versions) {

	this.getData = function(data) {
		data = (data) ? '/' + data : '/get_data';
		var promise = $http({
			method : 'POST',
			url : current_url + data
		}).success(function(data, status, headers, config) {
			angular.extend($rootScope, data);
			angular.extend($rootScope, actions);

			// only for when we are in a course. check the versions table to retrieve the
			// latest course version available. if the course version exists in the course
			// json, we should be using the latest version of the chameleon.json, so make
			// sure we are doing that here.
			if($rootScope.data && $rootScope.course) {
				Versions.get({id: 'latest'}, function(ver) {
					var version = ($rootScope.data.config.version) ? ver.version_sub : $rootScope.course.course_version;
					$http.get(aws_path + 'core/versions/' + version + '/chameleon.json').then(function(data) {
						$rootScope.chameleon = data.data;
						$rootScope.loadData.resolve();
					});
				});
			} else {
				$rootScope.loadData.resolve();
			}
		});

		return promise;
	};

}]);

chameleonServices.service('userData', ["$http", "$rootScope", "actions", function($http, $rootScope, actions) {

	this.getData = function(data) {
		var promise = $http({
			method : 'POST',
			url : base_url + 'client/get_data'
		}).success(function(data, status, headers, config) {
			angular.extend($rootScope, data);
			angular.extend($rootScope, actions);
			$rootScope.loadUserData.resolve();
		});

		return promise;
	};
}]);

chameleonServices.service('CourseDataService', ['$http', function($http) {
	this.getChameleonJSON = function(ver) {
		return $http.get(aws_path + 'core/versions/' + ver + '/chameleon.json');
	};
	this.getCourseDB = function() {
		return 'course DB';
	};
	this.getCourseJSON = function() {
		return 'course JSON';
	};
}]);

chameleonServices.service('PreviewService', ['$http', function ($http) {
	this.loadPreview = function(url) {
		return $http.get(url).success(function(data) {
			if (data) {
				var url = cloudfront_path + data;
				window.open(url,'','width=1280,height=800,menubar=no,toolbar=no,resizable,status=no,titlebar=no,location=no,scrollbars=yes');
			}
		});
	};
}]);

chameleonServices.service('fileUpload', ['$http', '$q', function ($http, $q) {
	this.uploadFileToUrl = function(file, filename, additional_params) {
		var defer = $q.defer();

		var fd = new FormData();
		if (filename) {
			fd.append('0', file, filename);
		}
		else {
			fd.append('0', file);
		}
		for (var k in additional_params) {
			fd.append(k, additional_params[k]);
		}
		$http.post(current_url + '/upload', fd, {
			transformRequest: angular.identity,
			headers: {'Content-Type': undefined}
		})
		.success(function(data) {
			defer.resolve(data);
		})
		.error(function() {
		});

		return defer.promise;
	};
}]);

chameleonServices.service('themeService', function() {

	this.lightenDarken = function(col, amt) {
		var usePound = false;

		if (col[0] == "#") {
				col = col.slice(1);
				usePound = true;
		}

		var num = parseInt(col,16);

		var r = (num >> 16) + amt;

		if (r > 255) r = 255;
		else if  (r < 0) r = 0;

		var b = ((num >> 8) & 0x00FF) + amt;

		if (b > 255) b = 255;
		else if  (b < 0) b = 0;

		var g = (num & 0x0000FF) + amt;

		if (g > 255) g = 255;
		else if (g < 0) g = 0;

		return (usePound?"#":"") + (g | (b << 8) | (r << 16)).toString(16);
	}

});

chameleonServices.service('inputService', function() {
	this.formatSelect = function(arr, key, name) {
		let new_arr = [];

		angular.forEach(arr, function(value) {
			new_arr.push({key: value[key], name: value[name]});
		});

		return new_arr;
	}
});

chameleonServices.service('ChipService', ['$rootScope', '$mdDialog', function($rootScope, $mdDialog) {

	this.searchText = null;
	this.selectedItem = null;

	this.addChip = (newObj, objArr, type, applyToAll) => {
		// do not add a new chip if the title is missing
		if(!newObj.name) return;

		// generate a new key if one does not exist
		newObj.key = newObj.key || newObj.name.replace(' ','_');

		// check if the chip already exists before adding
		let duplicate = objArr.find((item) => { return item.name === newObj.name });
		if(!duplicate) { objArr.push(newObj) }

		// apply the newly created tag to all pages in the course
		if(applyToAll) {
			angular.forEach($rootScope.data.topic, function(topic) {
				angular.forEach(topic.page, function(page) {
					page.roles = page.roles || [];
					let duplicate = page.roles.find(role => { return role === newObj.key });
					if(!duplicate) { page.roles.push(newObj.key) }
				});
			});
		}
	};

	this.deleteFromAll = (topics, obj, type) => {
		angular.forEach(topics, function(topic) {
			angular.forEach(topic.page, function(page) {
				// nothing to remove if array is undefined
				if(angular.isUndefined(page[type])) { return }

				angular.forEach(page[type], function(value, key) {
					if(value == obj.key) {
						page[type].splice(key, 1);
					}
				}, page);
			});
		});
	};

	this.deleteModal = (ev, title) => {
		return $mdDialog.show({
			controller: function($scope, $mdDialog, title) {
				$scope.hide = function() {
					$mdDialog.hide();
				};
				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.answer = function(answer) {
					$mdDialog.hide(answer);
				};
				$scope.name = title;
			},
			template: `
				<md-content class="md-padding message">
					<p>You are about to remove the tag "{{name}}"</p>
					<div layout="row">
						<md-button ng-click="answer()">OK</md-button>
						<md-button ng-click="cancel()">Cancel</md-button>
					</div>
				</md-content>
			`,
			onShowing: function(scope,elem,attr) {
				$(elem).attr('id','alert-modal');
			},
			locals: { title },
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:false,
			fullscreen: true
		})
	}

	// since we are only storing the chip keys in the page arrays
	// we need a way to return the title for display
	this.getChipTitle = (key, arr) => {
		let result = arr.find( value => value.key === key );
		if(result) {
			return result.name;
		}
	}

	this.toTop = (elem) => {
		$(elem).animate({
			scrollTop: 0
		}, 600);
		return false;
	};

	// Search for chips
	this.querySearch = (query, data, key) => { return query ? data.filter(createFilterFor(query, key)) : [] };

	// Return the proper object when the append is called.
	this.transformChip = chip => { return (angular.isObject(chip)) ? chip.key : { name: chip, type: 'new' } };

	// Create filter function for a query string
	function createFilterFor(query, key) {
		const lowercaseQuery = angular.lowercase(query);
		return function filterFn(data) {
			var test = angular.lowercase(data[key]).indexOf(lowercaseQuery) > -1;
			return test;
		};
	}

}]);