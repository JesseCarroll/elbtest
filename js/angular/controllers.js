var app = angular.module("chamCtrl", []);

app.controller('chameleonCtrl', ['$rootScope', '$mdToast', function($rootScope, $mdToast) {
	var originatorEv;

	// global function for copying a course url to the clipboard
	// requires a textarea element with an ID of 'copy-content'
	// that contains the text to be copied.
	$rootScope.copy_text = "COPY LINK";
	$rootScope.copyLink = function() {
		var copy_content = document.getElementById("copy-content");
		copy_content.select();
		try {
			var copied = document.execCommand('copy');
			if(copied) {
				$rootScope.copy_text = "COPIED TO CLIPBOARD";
			}
		} catch(err) {
			console.log('Unable to copy.');
			console.log(err);
		}
	}

	// define the base_url at a global level for all sections of the editor
	$rootScope.base_url = base_url;

	$rootScope.addItem = function(scope, type) {
		if(!angular.isArray(scope)) return

		scope.push({});
		$rootScope[type] = scope[scope.length-1];
	}

	$rootScope.openMenu = function($mdOpenMenu, ev) {
		originatorEv = ev;
		$mdOpenMenu(ev);
	};

	$rootScope.showCourseSaveToast = function() {
		$mdToast.show(
			$mdToast.simple()
				.content('Course Saved Successfully!')
				.position('top right')
				.hideDelay(2000)
		);
	};
}]);

app.controller('migrationCtrl', ['$rootScope', '$scope', '$mdDialog', '$location', '$filter', function($rootScope, $scope, $mdDialog, $location, $filter) {

	$scope.state = $rootScope.course.state;
	$scope.state_message = {
		'author': 'You have the ability to fully develop and edit this course.',
		'translate': 'This course is in translation. Switching the course back to author will interrupt all translations in progress.',
		'locked': 'The course is now locked to prevent any developing, editing or translating.'
	};

	// watch when the locked state changes so we can override the course state
	// (if locked) and so we can adjust the text of the lock/unlock button
	// appropriately.
	$scope.$watch('course.locked', function(newValue, oldValue) {
		$scope.lock_button_text = (parseInt(newValue)) ? 'UNLOCK COURSE' : 'LOCK COURSE';
		$scope.state = (parseInt(newValue)) ? 'locked' : $rootScope.course.state;
	});

	// Toggle the lock state of a course. If the course is locked, the state
	// changes to 'locked'. If the course is unlocking, the state changes to
	// whichever state ('author' or 'translate') the course we in before being
	// locked.
	$scope.toggleLock = function(ev) {
		$mdDialog.show({
			controller: function($scope, $rootScope, $mdDialog) {

				// get temporary state so we can display the correct modal,
				// but don't change anything yet in case they cancel
				$scope.locked = ($rootScope.course.locked == 1) ? 0 : 1;
				var tempState=$rootScope.course.state=='author' ? 'translate':'author';
				$scope.state = (parseInt($scope.locked)) ? 'locked' : tempState;

				$scope.hide = function() {
					$mdDialog.hide();
				};
				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.answer = function(answer) {
					if (answer === false) {
						return;
					}

					$mdDialog.hide();
					$rootScope.course.locked = $scope.locked;
					//$rootScope.saveCourseDB();
				};
			},
			templateUrl: base_url + 'js/angular/templates/modals/change_state.html',
			targetEvent: ev
		});
	};

	// Controls the open/close states of the migration and state content areas
	$scope.toggleMigration = function(ev, section) {
		$scope.active = ($scope.active == section) ? null : section;
		$('.course-controls md-icon').html('add_circle');
		if($scope.active == section) {
			$(ev.currentTarget).find('md-icon').html('remove_circle');
		}
	}

	// Change the state of the course between 'author' and 'translate'
	$scope.changeState = function(ev, state) {
		if ($scope.state!=state) {
			$mdDialog.show({
				controller: function($scope, $rootScope, $mdDialog) {

					// get temporary state so we can display the correct modal,
					// but don't change anything yet in case they cancel

					$scope.hide = function() {
						$mdDialog.hide();
					};
					$scope.cancel = function() {
						$mdDialog.cancel();
					};
					$scope.answer = function(answer) {
						if (answer === false) {
							return;
						}

					$scope.state = state;
						$mdDialog.hide();
						$rootScope.course.state = state;
						$rootScope.editor.course.section = (state == 'translate') ? 'translate' : 'detail';

						// if we are moving back to the Author state from Translation, we need to
						// refresh back to the course root, since the URL has changed, we are unable
						// to simply change a view state.
						if(state == 'author') {
							function callback() {
								window.location.href = base_url + 'course/' + $rootScope.course.course_id;
							}
							$rootScope.saveCourseDB(callback);
						} else {
							$rootScope.saveCourseDB();
						}
					};
				},
				templateUrl: base_url + 'js/angular/templates/modals/change_state.html',
				targetEvent: ev,
				scope: $scope,
				preserveScope: true
			});
		}
	}

	// disable the publish button if the course has not been staged OR
	// if the course has been saved more recently than the last stage migration
	$scope.$watch('course', function(newValue, oldValue) {
		$scope.disablePublish = (angular.isDefined(newValue.date_staged) && newValue.date_staged !== null) ? Date.parse(newValue.date_staged) < Date.parse(newValue.date_updated) : true;
	}, true);

	$scope.migrateCourse = function(ev, env) {
		$mdDialog.show({
			controller: function($scope, $rootScope, $mdDialog, $location, JsonData) {

				$scope.env = env;

				// check for an existing updated date, and if it exist, we know we
				// are overwriting the migration for the environment, so we will
				// show the overwrite message in the modal
				$scope.updated = (env == 'stage') ? $rootScope.course.date_staged : $rootScope.course.date_published;
				$scope.overwrite = angular.isDefined($scope.updated) && $scope.updated !== null;

				// if the course has been previously published, we will check to see if
				// new topics or pages have been added, and if so, warn the user that
				// such changes will reset saved learning data
				if($scope.env == 'prod' && $scope.overwrite) {

					$scope.dataCheck = false;

					// get production data
					JsonData.get({'id':$rootScope.data.config.course_id, 'state':$scope.env}, function(prod_data) {

						JsonData.get({'id':$rootScope.data.config.course_id, 'state':'stage'}, function(stage_data) {
							$scope.resetWarning = false;

							if(angular.isUndefined(prod_data.topic)) {
								// if prod data doesn't exist, there is no conflict since the course
								// has not previously been published
								$scope.dataCheck = true;
							} else {
								// make sure the number of topics hasn't changed
								if(prod_data.topic.length !== stage_data.topic.length) {
									console.log("Topic lengths do not match");
									$scope.resetWarning = 'topic';
								} else {

									angular.forEach(prod_data.topic, function(topic, topic_key) {

										// make sure the topic keys match
										if(topic.uuid !== this.topic[topic_key].uuid) {
											console.log("Topic key does not match");
											$scope.resetWarning = 'topic';
										}

										// make sure the number of pages hasn't changed
										else if(topic.page.length !== this.topic[topic_key].page.length) {
											console.log("Page lengths do not match");
											$scope.resetWarning = 'page';
										} else {

											angular.forEach(topic.page, function(page, page_key) {
												
												// make sure the page keys match
												if(page.uuid !== this.topic[topic_key].page[page_key].uuid) {
													console.log("Page key does not match");
													$scope.resetWarning = 'page';
												}

											}, this);
										}
									}, stage_data);
								}
							}

							$scope.dataCheck = true;
						});
					});
				} else {
					$scope.dataCheck = true;
				}

				$scope.copy_text = 'COPY LINK';
				$scope.preview_url = $rootScope.courseRoot + '/' + env + '/index.html';
				$scope.download_url = $rootScope.courseRoot + '/cloud/downloads/' + $rootScope.course.local_download + '-' + env + '.zip';

				$scope.copyLink = function() {
					var copy_content = document.getElementById("copy-content");
					copy_content.select();
					try {
						var copied = document.execCommand('copy');
						if(copied) {
							$scope.copy_text = "COPIED TO CLIPBOARD";
						}
					} catch(err) {
						console.log('Unable to copy.');
					}
				}

				$scope.hide = function() {
					$mdDialog.hide();
				};
				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.answer = function(answer) {
					//$mdDialog.hide(answer);
					if (answer === false) {
						return;
					}

					// pass in the environment argument (stage or publish) to the data.
					var data = { 'env': env }

					// if this course has been marked as being available offline, add a
					// parameter that will create offline media packages at the
					// appropriate stage
					if (parseInt($rootScope.course.offline)) {
						// pass in offline status
						data.offline = $rootScope.course.offline;
					}

					// advance the course to the next stage
					//
					// instead of using high-level AngularJS XHR abstractions, we're
					// using XHR directly because this request can be a long-running
					// process and direct use of XHR allows us to respond to status and
					// progress messages that are received before the process is
					// finished running
					var xhr = new XMLHttpRequest();
					xhr.open('POST', $location.absUrl() + '/stage', true);
					xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
					var statusRegex = {
						progressPct:  /progress_pct:\s*(\d+)/,
						message:      /message:\s*(.*)/,
						progressMode: /progress_mode:\s*(.*)/
					};
					$scope['stageStatus'] = {};
					xhr.onreadystatechange = function() {
						if (2 < xhr.readyState) {
							// isolate new content that arrived since the last state change event
							var new_messages = '';
							if ('undefined' === typeof xhr.previous_text) {
								new_messages = xhr.responseText;
							} else {
								new_messages = xhr.responseText.substring(xhr.previous_text.length);
							}

							if (new_messages) {
								// respond to messages designed to be machine locatable
								var matches = null;
								for (var k in statusRegex) {
									matches = statusRegex[k].exec(new_messages);
									if (null !== matches) {
										$scope['stageStatus'][k] = matches[1];
										$scope.$apply();
									}
								}

								// print all response messages to the console
								if (new_messages) {
									console.log(new_messages);
								}
							}

							xhr.previous_text = xhr.responseText;
						}

						// after the course is successfully migrated/packaged, set
						// success to true, so the modal content shows the options
						// to open or download the course.
						//
						// also save the course data with the updated date for the
						// applicable environment (date_staged, date_published)
						if (4 == xhr.readyState) {
							//$mdDialog.hide();
							if(env == 'stage') {
								$rootScope.course.date_staged = $filter('dateFormat')();
							}
							else if(env == 'prod') {
								$rootScope.course.date_published =  $filter('dateFormat')();
							}
							//$rootScope.saveCourseDB();
							$scope.success = true;
						}
					}
					xhr.send(JSON.stringify(data));
				};
			},
			templateUrl: base_url + 'js/angular/templates/modals/migration.html',
			targetEvent: ev
		});
	};
}]);

app.controller('maintenance', function($scope, $mdToast, title, message) {
	$scope.title = title;
	$scope.message = message;

	$scope.cancel = function() {
		$mdToast.hide();
		sessionStorage.maintenance = 1;
	}
});

app.controller('navigationCtrl', function($scope, $rootScope, $http, $mdSidenav) {

	this.courseMenu = [
		{key: 'detail', name: 'Course Details'},
		{key: 'home', name: 'Homepage'},
		{key: 'topics', name: 'Topics'},
		{key: 'glossary', name: 'Glossary'},
		{key: 'resources', name: 'Resources'},
		{key: 'terms', name: 'Common Terms'},
		{key: 'pretest', name: 'Pre-Test'},
		{key: 'certificate', name: 'Certificate'},
		{key: 'objectives', name: 'Objectives'},
		{key: 'cms', name: 'CMS'},
		{key: 'assets', name: 'Assets'},
		{key: 'roles', name: 'Roles/Regions'},
		{key: 'settings', name: 'Settings'}
	]

	this.glossarySortingOptions = {
		start: function(event, ui) {
			$(".glossary-item.add").hide();
			$(".glossary-item").css('margin', '5px 0');
		},
		stop: function(event, ui) {
			$(".glossary-item.add").show();
			$(".glossary-item").css('margin', '0');
		},
		tolerance: 'pointer',
		delay: 200
	};

	this.changeClient = function() {
		window.location.href = base_url + 'client';
	};

	this.editSection = function(section, idx) {
		$rootScope[section] = $scope.data[section][idx];
	}

	this.editThemeSection = function(section) {
		$rootScope.theme_section = section;
	}

	this.selectedSection = function(section, key) {
		key = (key) ? 'client' : 'course';
		if (window.location.pathname.indexOf(key) > -1) {
			if ($rootScope.editor[key]) {
				return section == $rootScope.editor[key].section;
			}
		}
		else if (window.location.pathname.indexOf(section) > -1) {
			return true;
		}
	};

	this.selectPage = function(index, client) {
		//$rootScope.pageControls = [];
		if (client) {
			$rootScope.editor.client.section = index;
			sessionStorage.clientSection = index;

			if (window.location.pathname.indexOf('client') == -1 ||
				(window.location.pathname.indexOf('client') > -1 && window.location.pathname.indexOf('theme') > -1)
			) {
				window.location.href = base_url + 'client/' + $rootScope.editor.client.id;
			}
		} else {
			$rootScope.editor.course.section = index;
			sessionStorage.courseSection = index;
		}
	};

	$scope.showSearch = false;
	this.toggleSearch = function() {
		$scope.showSearch = !$scope.showSearch;
	}

	$rootScope.loadData.done(function() {

		$rootScope.toggle = function(id) {
			$mdSidenav(id).toggle();
		};

		$scope.clientLogo = {
			'background-image': 'url(' + aws_path + 'clients/' + $rootScope.client.client_id + '/images/' + $rootScope.client.client_logo + ')',
			'background-color': $rootScope.client.client_primary_color
		};
	});
});

app.controller('courseRepositoryCtrl', function($scope, $rootScope, CourseRepository, Upload, $mdDialog) {
	// for tracking and displaying the status of uploads
	$scope.uploadStatus = [];

	// requests a current list of assets in the repository and stores it in the local scope
	var refreshRepositoryAssetList = function () {
		$scope.courseRepositoryAssets = [];
		CourseRepository.query({course_id: $rootScope.course.course_id}, function(data) {
			for (k in data) {
				if ('string' !== typeof data[k]) continue;
				$scope.courseRepositoryAssets.push(createAssetObject(data[k]));
			}
		});
	}

	// helper function that takes a filename, determines type by examining the
	// extension, and returns an object representing the file
	var createAssetObject = function(filename) {
		if ('string' !== typeof filename) {
			//console.log(filename);
			throw 'Filename is not a string, but rather ' + typeof filename;
		}

		// determine file type by examining the filename extension
		var type;
		try {
			type = filename.split('.').pop();
		}
		catch (e) {
			type = '';
		}
		switch (type) {
			case 'docx':
				type = 'doc';
				break;
		}

		return {
			type: type,
			filename: filename
		};
	};

	// wait until course data is fully loaded to request initial list of assets
	$rootScope.loadData.done(function() {
		refreshRepositoryAssetList();
	});

	// handle a file upload
	$scope.uploadFile = function($file, index) {
		if (!$file) {
			return;
		}

		// initialize upload status object
		$scope.uploadStatus[index] = {};

		// this function is called before, during and at the end of the upload
		// process in order to update a scope variable where we advertise the
		// progress of the upload for any observers interested in acting on this
		// information
		var setPercent = function(pct) {
			if (0 == pct) {
				$scope.uploadStatus[index].state = '';
				$scope.uploadStatus[index].text = '';
			} else {
				$scope.uploadStatus[index].state = 'uploading';
				$scope.uploadStatus[index].text = `Uploading... (${pct}% complete)`;
			}
			$scope.uploadStatus[index].percent = pct;
		};

		// this function will be called if the upload succeeds
		var successFunction = function (resp) {
			// if we uploaded to a position in the asset list that already contained
			// an asset, check the name of the file being uploaded -- unless the
			// filename is identical, we need to delete the file that was already in
			// this position; commented console.log calls have intentionally been left
			// in the code to aid in code comprehension and debugging
			var p = new Promise(function(resolve, reject) {
				if (angular.isDefined($scope.courseRepositoryAssets[index])) {
					//console.log(`Uploaded replacement file '${$file.name}'`);
					if ($file.name == $scope.courseRepositoryAssets[index].filename) {
						//console.log('No need to delete old file because uploaded filename is the same');
						resolve();
					}
					CourseRepository.delete({course_id: $rootScope.course.course_id, filename: $scope.courseRepositoryAssets[index].filename}, function() {
						//console.log(`Deleted old file '${$scope.courseRepositoryAssets[index].filename}'`);
						resolve();
					});
				} else {
					//console.log(`New file '${$file.name}' successfully uploaded`);
					resolve();
				}
			});

			// once the old file was deleted (if necessary), refresh the asset list
			p.then(function() {
				refreshRepositoryAssetList();
				setPercent(0);
			});
		};

		// upload errored out
		var errorFunction = function(resp) {
			var errorMessage = 'Something went wrong when uploading the file.';
			try {
				var xmlDoc = $.parseXML(resp.data);
				var xml = $(xmlDoc);
				var code = xml.find('Code').text();
				switch (code) {
					case 'EntityTooLarge':
					errorMessage = 'Your proposed upload exceeds the maximum allowed size (15MB)';
					break;
				}
			}
			catch (e) {}
			var popupBox = $mdDialog.show(
				$mdDialog.alert({
					title: 'Uploading Error',
					textContent: errorMessage,
					ok: 'Continue'
				})
			).finally(function() {});
			setPercent(0);
		};

		// this is called to update the progress indicator
		var progressFunction = function (evt) {
			setPercent(parseInt(100.0 * evt.loaded / evt.total));
		};

		// We are uploading directly to S3 in order to avoid the encumbrance of
		// first uploading the asset to our own trusted host and then uploading it
		// from there to S3. To do this securely, we must first request permission
		// (from the trusted host) to upload this file. The trusted host responds
		// with some signature data that must be included when the user's browser
		// sends the file directly to S3. The browser then uploads the file to the
		// S3 bucket URL along with the signature data.
		CourseRepository.s3_directpost({course_id: $rootScope.course.course_id, filename: $file.name}, function(result) {
			try {
				// initialize progress variables in the scope
				setPercent(0);

				// transfer response object fields into a new object containing post data
				var postdata = {};
				for (k in result) {
					if ('string' !== typeof result[k]) continue;
					postdata[k] = result[k];
				}

				// place the file into the post data
				postdata.file = $file;

				// initiate the upload
				Upload.upload({
					url: `//${postdata.bucket}.s3.amazonaws.com/`,
					data: postdata
				}).then(successFunction, errorFunction, progressFunction);
			}
			catch (e) {
				console.log(e);
			}
		});
	};

	// remove the file at the specified index from the repository
	$scope.remove = function(index, doConfirm) {
		if (angular.isUndefined($scope.courseRepositoryAssets[index])) {
			throw `Invalid index ${index}`;
		}
		var worker = function() {
			CourseRepository.delete({course_id: $rootScope.course.course_id, filename: $scope.courseRepositoryAssets[index].filename}, function() {
				refreshRepositoryAssetList();
			});
		}

		// we always prompt for confirmation unless the doConfirm argument was
		// explicitly set to a falsy value
		if (angular.isUndefined(doConfirm)) {
			doConfirm = true;
		}

		// if we don't need to ask for confirmation, silently delete the file and return
		if (!doConfirm) {
			worker();
			return;
		}

		// otherwise, only delete the file if the user confirms their intent
		var confirmDialog = $mdDialog.confirm()
			.title(`Are you sure you want to permanently remove file "${$scope.courseRepositoryAssets[index].filename}"?`)
			.ariaLabel(`Are you sure you want to permanently remove file "${$scope.courseRepositoryAssets[index].filename}"?`)
			.ok('Yes')
			.cancel('No');
		$mdDialog.show(confirmDialog).then(function() {
			worker();
		});
	};

	$scope.download = function(index) {
		var url = $rootScope.courseRoot + '/cloud/repository/' + $scope.courseRepositoryAssets[index].filename;
		window.open(url, '_blank');
	};
});

app.controller('courseCtrl', function($scope, $rootScope, $http, $mdDialog, $mdToast, $timeout, $filter, PreviewService, Versions, mediaService, User, Themes, Courses, CourseRepository, $parse, Upload) {

	$rootScope.showSidenav = true;

	$rootScope.loadData.done(function() {

		// This code is intended to run whenever a course is loaded or when a new
		// theme is selected for a course. It modifies CKEditor's colorButton_colors
		// config key to contain both a standard set of colors as well as a set of
		// colors derived from the selected theme.
		$scope.$watch('data.config.theme_id', function(newValue, oldValue) {
			// get the ID of the selected theme or, if no theme is selected, the ID
			// of the default theme
			var getThemeIdPromise = new Promise(function(resolve, reject) {
				if (angular.isDefined($rootScope.data.config.theme_id)) {
					resolve($rootScope.data.config.theme_id);
				}
				else {
					Themes.get_default({client_id: $rootScope.client.client_id}, function(data) {
						resolve(data.result);
					});
				}
			});

			// once we have the theme ID, get the theme data and use it to set
			// CKEditor's colorButton_colors config key
			getThemeIdPromise.then(function(selectedThemeId) {
				Themes.get({client_id: $rootScope.client.client_id, theme_id: selectedThemeId}, function(themeData) {
					// each row contains 8 colors
					CKEDITOR.config.colorButton_colorsPerRow = 8;

					// this is the first row of colors, consisting of unchangeable defaults
					CKEDITOR.config.colorButton_colors = 'FFFFFF,000000,5F5E5E,C83200,F8F057,7CF77C,7EFCFC,2A55FF';

					// we'll add another row of colors using these variables from the theme
					var themeColorKeys = ['brandPrimary', 'brandSecondary',	'neutral80', 'neutral65', 'neutral55', 'neutral40', 'neutral15', 'neutral5'];
					for (var k in themeColorKeys) {
						var colorString = themeData.variable[themeColorKeys[k]].toUpperCase();

						// some theme colors are defined using HSLA notation, so use the
						// tinycolor library to convert them to HEX, which is what CKEditor
						// requires
						if (-1 < colorString.indexOf('HSLA')) {
							var color = tinycolor(colorString);
							colorString = color.toHexString();
						}

						// CKEditor's docs say not to include the pound sign before hex colors
						if (-1 < colorString.indexOf('#')) {
							colorString = colorString.replace('#', '');
						}

						CKEDITOR.config.colorButton_colors += `,${colorString}`;
					}
				});
			});
		});

		// occassionally we will need to update scopes when data changes for a
		// course, run a global watch to the course data and update necessary
		// items accordingly
		$scope.$watch('course', function(newValue, oldValue) {

			// when a course is migrated to stage or prod for the first time, a date
			// is added to the database replacing a null value. We will use
			// the presence of this date to determine if the user can download or
			// preview that particular environment (stage or prod).
			// check the course table for a date value in the date_staged and
			// date_published fields to determine which preview links to
			// display for the course.
			$scope.previewOptions = [];
			$scope.downloadOptions = [];

			// pre is always available for preview
			var pre = {};
			pre.name = 'Pre Link';
			pre.action = $scope.previewCourse;
			pre.argument = 'pre';
			$scope.previewOptions.push(pre);

			if(angular.isDefined($rootScope.course.date_staged) && $rootScope.course.date_staged !== null) {
				var stage_preview = {
					name: 'Stage Link',
					action: $scope.previewCourse,
					argument: 'stage'
				};

				var stage_download = {
					name: 'Stage Link',
					href: $rootScope.courseRoot + '/cloud/downloads/' + $rootScope.course.local_download + '-stage.zip',
					argument: 'stage'
				};

				$scope.previewOptions.push(stage_preview);
				$scope.downloadOptions.push(stage_download);
			}
			if(angular.isDefined($rootScope.course.date_published) && $rootScope.course.date_published !== null) {
				var prod_preview = {
					name: 'Prod Link',
					action: $scope.previewCourse,
					argument: 'prod'
				};

				var prod_download = {
					name: 'Prod Link',
					href: $rootScope.courseRoot + '/cloud/downloads/' + $rootScope.course.local_download + '-prod.zip',
					argument: 'prod'
				};

				$scope.previewOptions.push(prod_preview);
				$scope.downloadOptions.push(prod_download);
			}

			$rootScope.pageControls = [
				{
					type: "single",
					name: "Save",
					action: $scope.updateCourse,
					permission: { 'success': true }
				},
				{
					type: "multi",
					name: "Preview",
					options: $scope.previewOptions,
					permission: { 'success': true }
				},
				{
					type: "multi",
					name: "Download",
					options: $scope.downloadOptions,
					permission: User.can({
						id: $rootScope.user_id,
						task: 'download_course',
						client_id: $rootScope.client.client_id,
						course_id: $rootScope.course.course_id
					})
				},
			];
		}, true);

		// pass in the environment you want to preview (pre, stage, prod) and the
		// preview will open an a new tab/window.
		$scope.previewCourse = function(env) {
			// this function requires the course object to be available
			if(!$rootScope.course) { return }

			$mdDialog.show({
				controller: function DialogController($scope, $rootScope, $timeout, $mdDialog) {

					$scope.env = env;
					$scope.preview_url = $rootScope.courseRoot + '/' + env + '/index.html';
					$scope.uuid = $rootScope.uuid;

					$timeout(function() {
						$("#qrcode").qrcode($scope.preview_url);
					}, 10);

					$scope.hide = function() {
						$mdDialog.hide();
					};
					$scope.cancel = function() {
						$mdDialog.cancel();
					};
				},
				templateUrl: base_url + 'js/angular/templates/modals/preview_options.html',
				clickOutsideToClose: true,
				scope: $scope,
				preserveScope: true
			});
		};

		$rootScope.courseChangeConfirm = function(ev, course) {

		var being_edited = $mdDialog.alert()
			.title('')
			.textContent('The course you are attempting to edit is currently being edited by another user.')
			.ariaLabel('Course Locked')
			.targetEvent(ev)
			.ok('Cancel');

		var save_reminder = $mdDialog.confirm()
			.title('')
			.textContent('Would you like to save the current course before moving to another course?')
			.ariaLabel('Save Course')
			.targetEvent(ev)
			.ok('Yes')
			.cancel('No');

			// check to see if the course is already being edited
			Courses.get({'id': course.course_id}).$promise.then(function(data) {
				if(data.record.being_edited == '1') {
					$mdDialog.show(being_edited);
				} else {
					$mdDialog.show(save_reminder).then(
						function() {
							$scope.updateJSON($scope.data, function() {
								window.location.href = base_url + 'course/' + course.course_id;
							});
						},
						function() {
							window.location.href = base_url + 'course/' + course.course_id;
						});
				}
			});
		};

		$rootScope.coreRoot = aws_path + 'core/';
		$rootScope.courseRoot = aws_path + 'clients/' + $scope.client.client_id + '/courses/' + $scope.course.course_folder;
		$rootScope.assetRoot = $rootScope.courseRoot + '/pre/course/assets/';
		$rootScope.mediaRoot = $rootScope.courseRoot + '/pre/course/media/';
		$rootScope.imagePath = $rootScope.mediaRoot + 'images/';
		$rootScope.videoPath = $rootScope.mediaRoot + 'video/';
		$rootScope.audioPath = $rootScope.mediaRoot + 'audio/';
		$rootScope.placeholder_image = aws_path + 'core/images/placeholder.jpg';
		$rootScope.placeholder_video = aws_path + 'core/images/placeholder.mp4';
		$rootScope.random = Math.floor((Math.random() * 1000000) + 1);

		// define home template options
		$scope.home_template = [];
		angular.forEach($scope.chameleon.home, function(value, key) {
			var item = {};
			item.key = key;
			item.name = value.name;
			$scope.home_template.push(item);
		});

		$rootScope.certificate_data = angular.isObject($scope.data.certificate);

		// intialize certificate media object
		if($scope.data.certificate) {
			$scope.data.certificate.media = $scope.data.certificate.media || [{
				type: 'image',
				image: [],
				id: mediaService.generateMediaId()
			}];
		}

		$scope.showText = true;

		$scope.template_folder = ($rootScope.course_version == '2.0') ? 'legacy/' : '';

		$scope.sortableOptions = {
			handle: '.handle'
		};
		$scope.sortableModuleOptions = {
			handle: '.sortable',
			delay: 200,
			start: function() { $scope.showText = false },
			stop: function() { $scope.showText = true }
		};

		$scope.toastPosition = {
			bottom: false,
			top: true,
			left: false,
			right: true
		};

		$scope.glossary = null;
		$rootScope.$watch('glossary', function(newValue, oldValue) {
			$scope.glossary = newValue;
			if($scope.glossary && angular.isUndefined($scope.glossary.definition)) {
				$scope.glossary.definition = '';
			}
		});

		// watch for home template changes
		$scope.$watch('data.home.template', function(newValue) {
			$rootScope.template_settings = $scope.chameleon.home[newValue];

			// If we've switched to the popover home template and we don't already
			// have a media item in the first slot in the home media list, initialize
			// the first item in the list as an image.  This fulfills CHAM-1941's
			// requirement that both the image and video media items are conspiciously
			// evident on the page when the user has selected the popover home
			// template.
			if ('home-popover' == newValue && $scope.data.home.media.length == 0) {
				mediaService.initMediaListObject($scope.data.home.media, 0, 'image');
			}
		});

		// watch for section changes
		$scope.$watch('editor.course.section', function(newValue) {

			// keep template_settings updated when switching between course sections
			if (newValue == 'home') {
				// unless you changed home template, media was not showing without an empty timeout here
				$timeout(function() {
					$rootScope.template_settings = $scope.chameleon.home[$scope.data.home.template];
				})
			}
			else if (newValue == 'topics') {
				try {
					$scope.changeTemplate($scope.data.topic[sessionStorage.topic].page[sessionStorage.page].template);
				}
				catch (e) {}
			}
		});

		$scope.updateDB = function(data) {
			if (data) {
				// check if course is marked to be available offline, and if it's not
				// make sure the course order value (if any) is reset
				if(angular.isUndefined($scope.data.config.availableOffline)) {
					$http.put(base_url + 'client/' + $rootScope.client.client_id + '/player_order', {course_id: data.course_id}).then(function(data) {});
				}

				$http.post(current_url + '/update_db', {'data': data});
			}
		};

		$scope.changeTemplate = function(template) {
			if (angular.isUndefined($scope.item)) {
				return;
			}

			if (!angular.isObject(template)) {
				angular.forEach($scope.chameleon.templates, function(value) {
					if (value.key == template) {
						template = value;
					}
				});
			}

			$scope.item.template = template.key;
			$scope.item.template_name = template.name;
			$rootScope.template_settings = template;

			angular.forEach($rootScope.template_settings, function(value, key) {
				if (value.enable) {
					if (key == 'questions') {
						$scope.item['question'] = $scope.item['question'] || [];
					}
					if (key == 'background') {
						$scope.item[key] = $scope.item[key] || {};
					} else {
						$scope.item[key] = $scope.item[key] || [];
					}
					if (key == 'media') {
						if (value.limit == 0 || value.limit == undefined) { value.limit = 50 }
					}
				}
			});

			$scope.activateTabs();
		};

		$scope.activateTabs = function() {

			$rootScope.page_tabs = {
				'main': false,
				'background': false,
				'media': false,
				'interactions': false,
				'questions': false,
				'items': false,
				'categories': false,
				'summary': false,
				'dropzones': false,
				'extras': false
			};

			if ($rootScope.template_settings) {
				angular.forEach($rootScope.template_settings, function(value, key) {
					this[key] = value.enable;
				}, $rootScope.page_tabs);
			}

			$scope.tabSelect = 0;
		};

		$scope.$watch('editor', function(newValue, oldValue) {
			if(newValue == oldValue) return;

			var parent = $rootScope.editor.course.topic;
			var index = $rootScope.editor.course.page;

			// delete template settings
			delete $rootScope.template_settings;

			this.parent = $rootScope.editor.course.topic;
			this.index = $rootScope.editor.course.page;

			// if parent is set, a page was clicked
			if (this.parent > -1) {
				$scope.data_type = 'page';
				$scope.item = $scope.data.topic[this.parent].page[this.index];

				if($scope.item) {
					// page already has template
					if ($scope.item.template) {
						$scope.changeTemplate($scope.item.template);
					} else {
						$scope.activateTabs();
					}

					// fix to make sure backgrounds are objects instead of arrays
					if (angular.isArray($scope.item.background)) {
						$scope.item.background = {};
					}

					// TODO : need this?
					if ($scope.item.uuid) {
						$rootScope.uuid = $scope.item.uuid;
					}

					// ensure mastery score and attempts are returned as integers
					$scope.item.attempts = parseInt($scope.item.attempts, 10);
					$scope.item.masteryScore = parseInt($scope.item.masteryScore, 10);

					$scope.$watch('item.background', function(newValue, oldValue) {
						$scope._temp = {};
						if (newValue == undefined) {
							$scope._temp.imagePreview = null;
						}
					});
				}
			} else {
				//topic
				$scope.data_type = 'topic';
				$scope.item = $scope.data.topic[index];
				if (angular.isUndefined($scope.item.media)) {
					mediaService.initMediaListObject($scope.item.media, 0, 'image');
				}
			}
		}, true);
/*
				if ($rootScope.editor.course.topic > -1) {
						if ($rootScope.editor.course.page !== null) {
								var page = ($rootScope.editor.course.page > $scope.data.topic[$rootScope.editor.course.topic].page.length-1) ? 0 : $rootScope.editor.course.page;
								$scope.editPage(page, $rootScope.editor.course.topic);
						} else {
								$scope.editPage($rootScope.editor.course.topic);
						}
				}
*/
		$scope.getToastPosition = function() {
			return Object.keys($scope.toastPosition)
				.filter(function(pos) { return $scope.toastPosition[pos]; })
				.join(' ');
		};

		$scope.next = function() {
			this.selectedIndex = Math.min(this.selectedIndex + 1, 2) ;
		};

		$scope.previous = function() {
			this.selectedIndex = Math.max(this.selectedIndex - 1, 0);
		};

		$rootScope.showDownloads = function() {
			$mdDialog.show({
				controller: function($scope, $rootScope, $mdDialog) {

					var download_url = $rootScope.courseRoot + '/cloud/downloads/',
							filename = $rootScope.course.local_download,
							active_directories = $rootScope.active_directories;

					$scope.stage_disabled = true;
					$scope.prod_disabled = true;

					if (active_directories.stage) {
						$scope.stage_download = download_url + filename + '-stage.zip';
						$scope.stage_disabled = false;
					}
					if (active_directories.prod) {
						$scope.prod_download = download_url + filename + '-prod.zip';
						$scope.local_download = download_url + filename + '.zip';
						$scope.prod_disabled = false;
					}

					$scope.hide = function() {
						$mdDialog.hide();
					};
					$scope.cancel = function() {
						$mdDialog.cancel();
					};
					$scope.answer = function(answer) {
						$mdDialog.hide(answer);
					};
				},
				templateUrl: base_url + 'js/angular/templates/modals/download_options.html',
				clickOutsideToClose: true
			});
		};

		$scope.deleteCourse = function(ev, id, uuid) {
			$mdDialog.show({
				controller: DeleteController,
				templateUrl: base_url + 'js/angular/templates/confirm_delete.html',
				targetEvent: ev
			}).then(function(answer) {
				if (answer) {
					$http.get(current_url + '/delete');
					window.location.href = base_url + 'client/' + $rootScope.client.client_id;
				}
			});
		};

		$scope.showSaveToast = function() {
			$mdToast.show(
				$mdToast.simple()
					.content('Course Saved Successfully!')
					.position($scope.getToastPosition())
					.hideDelay(2000)
			);
		};

		$scope.toggleLanguages = function(element) {
			$scope.course.course_enabled_languages = $scope.course.course_enabled_languages || ['en-US'];
			$scope.data.config.languages = $scope.data.config.languages || [];

			if (angular.isArray($rootScope.course.course_enabled_languages)) {
				var idx = $rootScope.course.course_enabled_languages.indexOf(element);
			}

			// is currently selected
			if (idx > -1) {
				$scope.course.course_enabled_languages.splice(idx, 1);
				$scope.data.config.languages.splice((idx-1), 1);
			}

			// is newly selected
			else {
				$rootScope.course.course_enabled_languages.push(element);

				angular.forEach($rootScope.languages, function(value) {
					if (value.language_shortcode == element) {
						$scope.data.config.languages.push({'description':value.language_description,'shortcode': element,'datafile':'course/data/'+element+'.js'});
					}
				});

			}
		};

		// check if course version was changed, and if so, sync assets
		var versionChange = false;
		$rootScope.$watch('course_version', function(newValue, oldValue) {
			if (newValue !== oldValue) {
				versionChange = true;
			}
		});

		$rootScope.updateCourse = function() {

			// update database fields
			$scope.course.course_title = $scope.data.course;
			if ($scope.data.module) {
				$scope.course.course_title += ': ' + $scope.data.module;
			}
			$scope.course.offline = $scope.data.config.availableOffline;

			// date_updated is no longer automatically updated when the database
			// record is saved, so we will set the date here
			$scope.course.date_updated = $filter('dateFormat')();

			// debug is true when in development
			if ($scope.current_stage == 'Development') {
				$scope.data.config.debug = true;
			}

			// add course and client ID to data
			$scope.data.config.course_id = $rootScope.course.course_id;
			$scope.data.config.client_id = $rootScope.course.client_id;

			// if version has changed, sync assets and reset variables file
			if (versionChange) {
				versionChange = false;
				$scope.syncAssets($rootScope.course_version);
				$scope.resetVariables($rootScope.course_version);
			}

			$scope.updateJSON(angular.copy($scope.data));
			$scope.updateDB($scope.course);
		};

		$scope.updateJSON = function(data, callback) {
			if (data) {
				$http.post(current_url + '/update', {'data': data}).success(function() {
					if(callback) {
						callback();
					} else {
						$scope.showSaveToast();
					}
				});
			}
		};

		$scope.resetVariables = function(version) {
			$http.post(current_url + '/reset_version', {'data': version});
		};

		$scope.syncAssets = function(version) {
			$http.post(current_url + '/sync_assets', {'data': version}).then(function(response) {
				$scope.chameleon = response.data;
			});
		}
		//$scope.syncAssets($scope.course.course_version);

		$scope.toggleEditing = function(status) {
			$scope.course.being_edited = (status == 'lock') ? '1' : '0';
			$scope.updateDB({'being_edited': $scope.course.being_edited, 'updated_by': $rootScope.user_id});
		}

		window.onload = $scope.toggleEditing('lock');
		window.onbeforeunload = window.onunload = $scope.toggleEditing;
		$(window).on("navigate", function(event, data) {
			$scope.toggleEditing;
		});

		function DeleteController($scope, $mdDialog) {

			$scope.hide = function() {
				$mdDialog.hide();
			};
			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			$scope.answer = function(answer) {
				$mdDialog.hide(answer);
			};
		}

		$scope.$watch('resources.external', function(newValue, oldValue) {
			$scope.resource_type = (newValue) ? 'External URL' : 'File Upload';
		});

		$scope.removeResource = function() {
			delete $rootScope.resources.size;
			delete $rootScope.resources.type;
			delete $rootScope.resources.src;
		}

		// Set up the coursePoster variable to work with standard media controls.
		// This variable serves as glue between the course.poster varaible and the
		// media controls.
		//
		// NOTE: we want to save the filename of the course poster to the course's
		//   DB record, and if we fill in "course.poster" with a string value then
		//   it will automatically flow into the "poster" field of the DB record
		//
		// NOTE: it is intentional that coursePoster is within "data": we want the
		//   "glue" media object to be saved to the course data because unless we
		//   record the poster image filename somewhere within the course data, it
		//   will be deleted by the media garbage collection process
		if(angular.isUndefined($scope.data.coursePoster)) {
			$scope.data.coursePoster = [{
				type: 'image',
				image: [''],
				id: 'COURSEWARE_PLAYER_POSTER'
			}];
		};
		//if (angular.isDefined($scope.course.poster)) {
		//	$scope.data.coursePoster[0].image[0] = $scope.course.poster || '';
		//}
		$scope.$watch('data.coursePoster[0].image[0]', function(newValue, oldValue) {
			if (newValue === oldValue) {
				return;
			}
			$scope.course.poster = newValue;
		});

		/*
		 * A condensed version if the uploadFile method for course level media.
		 * See the original function in the directives file for a full description.
		 *
		 * This condensed method also exists in playerCtrl, and could probably be
		 * condensed at some point in the future.
		 */
		$scope.uploadFile = function($file, filenameDstExpression, options) {
			if (!$file) {
				return;
			}
			options = options || {};

			// ingest filenameDstExpression and turn it into a setter function if needed
			var filenameGetter = $parse(filenameDstExpression);
			var filenameSetter = filenameGetter.assign;

			// use a custom filename if we are passing one in
			if(options.filename) { filename = options.filename }

			try {
				// target filename:
				//
				// If this media slot has an ID, generate a new name for the file
				// based on the media slot's ID.
				//
				// Otherwise, use the filename provided by the user's browser.
				if (angular.isUndefined(filename)) {
					var filename = $file.name;
				}

				// prepare the payload data
				var uploadData = {
					file: $file,
					name: filename,
					isUnique: false
				};
				if (angular.isDefined(options.classification)) {
					uploadData.type = options.classification;
				}

				// this function will be called if the upload succeeds
				var successFunction = function (resp) {
					// invoke the setter expression for communicating the filename
					// back to the location elected by the caller
					filenameSetter($scope, filename);
					if(options.callback) {
						options.callback();
					}
					if(resp.data.type) $scope.resources.type = resp.data.type;
					if(resp.data.size) $scope.resources.size = resp.data.size;
				};

				// upload errored out
				var errorFunction = function(resp) {
					var errorMessage = '';
					try {
						errorMessage = resp.data.error_message;
					}
					catch (e) {
						errorMessage = 'Something went wrong when uploading the file.';
					}
					var popupBox = $mdDialog.show(
						$mdDialog.alert({
							title: 'Uploading Error',
							textContent: errorMessage,
							ok: 'Continue'
						})
					).finally(function() {});
				};

				// initiate the upload
				Upload.upload({
					url: current_url + '/upload',
					data: uploadData
				}).then(successFunction, errorFunction);
			}
			catch (e) {
				console.log(e);
			}
		};
	});
});

app.controller('courseSettings', function(Editor, $filter, $rootScope, $scope, $mdDialog, $http, $q, $timeout, Themes, inputService) {

	Editor.then(function(data) {
		$scope.helper = data.data.help;

		$scope.openHelp = function(ev, item) {
			if ($scope.helper[item]) {
				var confirm = $mdDialog.alert()
					.title($scope.helper[item].title)
					.textContent($scope.helper[item].description)
					.ariaLabel('Help')
					.targetEvent(ev)
					.ok('Got It');
				$mdDialog.show(confirm).then(function() {
				}, function() {
					// cancel disable event
				});
			}
		}
	});

	$rootScope.loadData.done(function() {

		$scope.options_list = [
			{'name':'Certificate', 'value':'certificate'},
			{'name':'CMS Code', 'value':'cmsCode'},
			{'name':'Glossary', 'value':'glossary'},
			{'name':'Objectives', 'value':'objectives'},
			{'name':'Pre-Test', 'value':'preTest'},
			{'name':'Resources', 'value':'resources'}
		];

		angular.forEach($scope.options_list, function(value) {
			if (angular.isDefined($scope.data[value.value])) {
				$scope.data.config[value.value] = true;
			}
		});

		$scope.api_list = [];
		angular.forEach($scope.chameleon.api_list, function(value, key) {
			var item = {};
			item.key = key;
			item.name = value;
			this.push(item);
		}, $scope.api_list);

		$scope.curriculum_select = [
			{"key":"", "name":"Select a Curriculum"}
		];
		angular.forEach($scope.curriculum_list, function(value) {
			var item = {};
			item.key = value.curriculum_id;
			item.name = value.curriculum_name;
			$scope.curriculum_select.push(item);
		});

		// use the Themes resource to fetch a list of themes that will populate the
		// theme selector dropdown
		$scope.theme_list = [];
		Themes.query({ client_id: $scope.course.client_id }, function(data) {
			angular.forEach(data, function(value) {
				$scope.theme_list.push({ key: value, name: $filter('capitalize')(value) });
			});
		});

		if ($scope.course.course_curriculum && $scope.course.course_curriculum > 0) {
			var item = {};
			item.key = 'curriculum';
			item.value = 'Curriculum';
		}

		$scope.addMenuItem = function(scope, ev) {
			if ($scope.data.config[scope]) {
				// make sure height is auto so menu items can be seamlessly added
				$scope.data[scope] = ($scope.data[scope]) ? $scope.data[scope] : (scope == 'certificate' || scope == 'cmsCode' || scope == 'preTest') ? {} : [];
			} else {
				if ($scope.data[scope]) {
					var confirm = $mdDialog.confirm()
						.title('Warning')
						.textContent('Disabling this section will also delete the data associated with it. Would you like to proceed?')
						.ariaLabel('Warning')
						.targetEvent(ev)
						.ok('Proceed')
						.cancel('Cancel');
					$mdDialog.show(confirm).then(function() {
						delete $scope.data[scope];
					}, function() {
						// cancel disable event
						$scope.data.config[scope] = true;
					});
				}
			}
		};

		$scope.data.config.gateType = 'click';
		$scope.data.config.disableGlossary = $scope.data.config.disableGlossary || false;

		// check if font is an array, and change it if it is
		if (angular.isArray($scope.data.config.font)) {
			$scope.data.config.font = {};
		}
		$scope.data.config.font = $scope.data.config.font || {};
	});
});

app.controller('preTestCtrl', function($scope, $rootScope) {

	$rootScope.loadData.done(function() {

		if(angular.isUndefined($rootScope.data.preTest)) return;

		$scope.preTest = $rootScope.data.preTest;
		$rootScope.data.preTest.media = $rootScope.data.preTest.media || [];
		$rootScope.data.preTest.question = $rootScope.data.preTest.question || [];

		angular.forEach($rootScope.chameleon.templates, function(value, key) {
			if(value.key == 'quiz') {
				$scope.template_settings = $rootScope.chameleon.templates[key];
			}
		});

		$scope.quiz_list = [];
		angular.forEach($rootScope.data, function(value, key) {
			if (key == 'topic') {
				angular.forEach(value, function(value, key) {
					angular.forEach(value.page, function(value) {
						if (value.template == 'quiz') { $scope.quiz_list.push(value); }
					});
				});
			}
		});

		$scope.quiz_selected = ($rootScope.data.preTest.type == 'copy' && $rootScope.data.preTest.question) ? true : false;

		$scope.resetQuestions = function() {
			$rootScope.data.preTest.type = '';
			$rootScope.data.preTest.question = [];
		}

		$scope.resetCopy = function() {
			$scope.quiz_selected = false;
			$rootScope.data.preTest.question = [];
		};

		$scope.deleteMirrorKey = function() {
			angular.forEach($scope.quiz_list, function(value, key) {
				if (value.mirror) {
					delete value.mirror;
				}
			});
		};

		$scope.copyQuiz = function(i) {
			$scope.quiz_selected = true;
			$rootScope.data.preTest.question = angular.copy($scope.quiz_list[i].question);
		};

		$scope.mirrorQuiz = function(i) {
			$scope.deleteMirrorKey();
			$scope.quiz_list[i].mirror = true;
		};
	});
});

app.controller('clientListCtrl', function(EditorState, chameleonData, actions, $scope, $rootScope) {

	// reset course/client data
	EditorState.clearData('client');
	EditorState.clearData('course');
	delete $rootScope.editor.client;
	delete $rootScope.editor.course;
	delete $rootScope.client;
	delete $rootScope.course;

	$scope.goToClient = function(id) {
		id = id || $rootScope.client.client_id;
		sessionStorage.client = id;
		window.location.href = base_url + 'client/' + id;
	};

});

app.controller('clientCtrl', function(actions, $scope, $rootScope, $http, $parse, $mdDialog, $mdToast, PreviewService, Upload) {

	$rootScope.showSidenav = true;

	$rootScope.loadData.done(function() {

		$scope.updateDB = function(data) {
			if (data) {
				$http.post(current_url + '/update_db', {'data': data});
			}
		}

		$rootScope.showPreview = function(ev, course) {
			$scope.loading = {};
			$scope.loading[course.course_id] = true;

			var url = base_url + 'course/' + course.course_id + '/preview/pre';
			PreviewService.loadPreview(url).then(function() {
				$scope.loading = {};
			});
		};

		$rootScope.exportCourse = function(ev, course) {
			$mdDialog.show({
				templateUrl: base_url + 'js/angular/templates/modals/course_pdf_export.html',
				targetEvent: ev,
				controller: function($scope, $rootScope, $mdDialog, $location) {
					$scope.cancel = function() {
						$mdDialog.cancel();
					};

					var xhr = new XMLHttpRequest();
					xhr.open('POST', base_url + 'api/course_pdf/' + course.course_id, true);
					xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
					var statusRegex = {
						progressPct:  /progress_pct:\s*(\d+)/,
						message:      /message:\s*(.*)/,
						downloadUrl:  /download_url:\s*(.*)/,
						error:        /error: \s*(.*)/
					};
					$scope['status'] = {
						progressPct: 0,
						message: 'Initializing Export',
						course: course
					};
					xhr.onreadystatechange = function() {
						if (2 < xhr.readyState) {
							// isolate new content that arrived since the last state change event
							var new_messages = '';
							if ('undefined' === typeof xhr.previous_text) {
								new_messages = xhr.responseText;
							} else {
								new_messages = xhr.responseText.substring(xhr.previous_text.length);
							}

							if (new_messages) {
								// respond to messages designed to be machine locatable
								var matches = null;
								for (var k in statusRegex) {
									matches = statusRegex[k].exec(new_messages);
									if (null !== matches) {
										$scope['status'][k] = matches[1];
										$scope.$apply();
									}
								}

								// print all response messages to the console
								if (new_messages) {
									console.log(new_messages);
								}
							}

							xhr.previous_text = xhr.responseText;
						}

						// after the course is successfully migrated/packaged, set
						// success to true, so the modal content shows the options
						// to open or download the course.
						//
						// also save the course data with the updated date for the
						// applicable environment (date_staged, date_published)
						if (4 == xhr.readyState) {
							if (angular.isUndefined($scope['status']['downloadUrl'])) {
								$scope['status']['error'] = 1;
								$scope['status']['message'] = 'No download link received. The export may have terminated prematurely.';
							}
							if (!$scope['status']['error']) {
								window.open($scope['status']['downloadUrl']);
							}
						}
					}
					xhr.send();
				}
			});
		};

		$scope.copyCourse = function(id) {
			window.location.href = base_url + 'course/' + id + '/copy';
		}

		$scope.saveTheme = function() {
			$scope.updateDB($scope.client);
		}

		$scope.goToClient = function(id) {
			sessionStorage.client = id;
			window.location.href = base_url + 'client/' + id;
		}

		$scope.goToCourse = function(course) {
			//if(course.being_edited == '1') return;

			sessionStorage.course = course.course_id;
			window.location.href = base_url + 'course/' + course.course_id;
		}

		$scope.openNotes = function(course) {
			$mdDialog.show({
				controller: NotesController,
				templateUrl: base_url + 'js/angular/templates/course_notes.html',
				clickOutsideToClose: true,
				locals: {course: course}
			}).then(function(answer){
				if (answer) {
					$rootScope.updateCourseDB(answer.course_id, answer);
				}
			});
		}

		function NotesController($scope, $mdDialog, course) {

			$scope.course = course;

			$scope.cancel = function() {
				$mdDialog.cancel();
			};
			$scope.answer = function(answer) {
				$mdDialog.hide(answer);
			};
		};

		/*
		 * A condensed version if the uploadFile method for course level media.
		 * See the original function in the directives file for a full description.
		 */
		$scope.uploadFile = function($file, filenameDstExpression) {
			if (!$file) {
				return;
			}

			// ingest filenameDstExpression and turn it into a setter function if needed
			var filenameGetter = $parse(filenameDstExpression);
			var filenameSetter = filenameGetter.assign;

			try {
				// target filename:
				//
				// If this media slot has an ID, generate a new name for the file
				// based on the media slot's ID.
				//
				// Otherwise, use the filename provided by the user's browser.
				if (angular.isUndefined(filename)) {
					var filename = $file.name;
				}

				// prepare the payload data
				var uploadData = {
					file: $file,
					name: filename,
					isUnique: false,
					type: 'client'
				};

				// this function will be called if the upload succeeds
				var successFunction = function (resp) {
					// invoke the setter expression for communicating the filename
					// back to the location elected by the caller
					filenameSetter($scope, filename);
				};

				// upload errored out
				var errorFunction = function(resp) {
					var errorMessage = '';
					try {
						errorMessage = resp.data.error_message;
					}
					catch (e) {
						errorMessage = 'Something went wrong when uploading the file.';
					}
					var popupBox = $mdDialog.show(
						$mdDialog.alert({
							title: 'Uploading Error',
							textContent: errorMessage,
							ok: 'Continue'
						})
					).finally(function() {});
				};

				// initiate the upload
				Upload.upload({
					url: current_url + '/upload',
					data: uploadData
				}).then(successFunction, errorFunction);
			}
			catch (e) {
				console.log(e);
			}
		};

		$rootScope.loadData.done(function() {
			$scope.getMediaUrl = function() {
				return $rootScope.aws_path + 'clients/' + $scope.client.client_id + '/images/' + $scope.player.logo;
			}
		});
	});
});

app.controller('courseListCtrl', function(User, EditorState, $rootScope, $scope, $mdDialog, $http, $window) {

	this.predicate = 'date_updated';

	this.reverse = !this.reverse;

	$rootScope.loadData.done(function() {
		// reset course/client data
		EditorState.clearData('course');
		delete $rootScope.editor.course;
		delete $rootScope.course;

		$scope.newCourse = function(ev, course) {
			$mdDialog.show({
				controller: newCourseController,
				templateUrl: base_url + 'js/angular/templates/modals/new_course.html',
				parent: angular.element(document.body),
				targetEvent: ev,
				clickOutsideToClose:true,
				locals: {course: course, clients: $scope.allowed_clients}
			})
			.then(function(answer) {
				$http.post(base_url + 'client/add_course', {'data': answer}).then(function(data) {
					$window.location.href = base_url + data.data;
				});
			}, function() {
				// cancelled
			});
		};

		$scope.state = function(course) {
			if(angular.isDefined(course.state) && course.state !== null) {
				return (course.state == 'translate') ? 'Translating' : 'Authoring';
				return course.state;
			} else {
				if(course.course_status < 4) {
					return 'Authoring';
				} else if(course.course_status < 7) {
					return 'Translating';
				} else {
					return 'Live';
				}
			}
		}

		$rootScope.pageControls = [
			{
				type: "single",
				name: "Add Course",
				action: $scope.newCourse,
				permission: User.can({
					id: $rootScope.user.user_id,
					task: 'add_course',
					client_id: $rootScope.client.client_id
				})
			}
		];
	});

	//Controller for creating a new course
	function newCourseController($scope, $mdDialog, course, clients) {

		$scope.title = 'New Course';
		$scope.data = {};
		$scope.data.version = (course && course.course_version[0] == '2') ? course.course_version : "modern";

		if (course) {
			$scope.title = 'Copy Course';
			$scope.data.course_id = course.course_id;
			if (course.course_title.indexOf(':') > -1) {
				var course_title = course.course_title.split(":");
				$scope.data.course_title = course_title[0];
				$scope.data.module = course_title[1].trim() + ' - (copy)';
			} else {
				$scope.data.course_title = course.course_title + ' - (copy)';
			}
			$scope.client_list = [];

			angular.forEach(clients, function(value) {
				var item = {};
				item.key = value.client_id;
				item.name = value.client_name;
				$scope.client_list.push(item);
			});
		}

		$scope.hide = function() {
			$mdDialog.hide();
		};
		$scope.cancel = function() {
			$mdDialog.cancel();
		};
		$scope.answer = function(answer) {
			$mdDialog.hide(answer);
		};
	}
});

app.controller('courseCMS', function($rootScope, $scope) {
	$rootScope.loadData.done(function() {
		$scope.cms = $rootScope.data.cmsCode;
	});
});

app.controller('courseTranslateIndex', function(PreviewService, TranslationService, $http, $scope) {

	$scope.active_languages = [];

	angular.forEach($scope.languages, function(item) {
		if(item.language_shortcode == 'en-US') return;

		if($scope.course.course_enabled_languages.indexOf(item.language_shortcode)+1) {
			$http.get(current_url + '/get_translation/' + item.language_shortcode).success(function(translation) {
				var transObject = TranslationService.startTranslation($scope.data, translation);

				item.progress = transObject.progress;
				item.deg = 360*item.progress/100;
			});

			$scope.active_languages.push(item);
		}
	});

	$scope.goToTranslation = function(shortcode) {
		window.location.href = current_url + '/translations/' + shortcode;
	};

	$scope.previewTranslation = function($event, shortcode) {
		$event.stopPropagation();

		$scope.loading = {};
		$scope.loading[shortcode] = true;

		var url = current_url + '/preview/?language=' + shortcode;
		PreviewService.loadPreview(url).then(function() {
			$scope.loading = {};
		});
	};
});

app.controller('courseTranslate', function(PreviewService, TranslationService, $scope, $rootScope, $timeout, $http, Upload, $parse, $mdDialog) {

	$rootScope.loadData.done(function() {

		$rootScope.showSidenav = true;

		$rootScope.courseRoot = aws_path + 'clients/' + $rootScope.course.client_id + '/courses/' + $rootScope.course.course_folder;
		$rootScope.assetRoot = $rootScope.courseRoot + '/pre/course/assets/';
		$rootScope.mediaRoot = $rootScope.courseRoot + '/pre/course/media/';
		$rootScope.imagePath = $rootScope.mediaRoot + 'images/';
		$rootScope.videoPath = $rootScope.mediaRoot + 'video/';
		$rootScope.audioPath = $rootScope.mediaRoot + 'audio/';
		$rootScope.placeholder_image = aws_path + 'core/images/placeholder.jpg';
		$rootScope.placeholder_video = aws_path + 'core/images/placeholder.mp4';

		$scope.transObject = TranslationService.startTranslation($scope.data, $scope.translation);

		// set some translation data to the rootScope so we can access it from the header/sidenav
		$rootScope.pages = [];
		angular.forEach($scope.languages, function(value){
			if (value.language_shortcode == $scope.shortcode) {
				$rootScope.language_description = value.language_description;
			}
		});

		// right to left languages should be added here
		this.language_list = ['ar', 'ur'];

		$scope.current_page = 0;
		$rootScope.rtl = this.language_list.indexOf($scope.shortcode) > -1;

		/*
		 * A condensed version if the uploadFile method for course level media.
		 * See the original function in the directives file for a full description.
		 */
		$scope.uploadFile = function($file, filenameDstExpression, options) {
			if (!$file) {
				return;
			}
			options = options || {};

			// ingest filenameDstExpression and turn it into a setter function if needed
			if (angular.isDefined(options.classification) && options.classification == 'resources') {
				var filenameGetter = $parse(filenameDstExpression + '.src');
			} else {
				var filenameGetter = $parse(filenameDstExpression);
			}
			var filenameSetter = filenameGetter.assign;

			// use a custom filename if we are passing one in
			if(options.filename) { filename = options.filename }

			try {
				// target filename:
				//
				// If this media slot has an ID, generate a new name for the file
				// based on the media slot's ID.
				//
				// Otherwise, use the filename provided by the user's browser.
				if (angular.isUndefined(filename)) {
					var filename = $file.name;
				}

				// prepare the payload data
				var uploadData = {
					file: $file,
					name: filename,
					isUnique: false
				};
				if (angular.isDefined(options.classification)) {
					uploadData.type = options.classification;
				}

				// this function will be called if the upload succeeds
				var successFunction = function (resp) {
					// invoke the setter expression for communicating the filename
					// back to the location elected by the caller
					filenameSetter($scope, filename);
					if(options.callback) {
						options.callback();
					}

					if(angular.isDefined(options.classification) && options.classification == 'resources') {
						if(angular.isDefined(resp.data.type)) {
							var typeGetter = $parse(filenameDstExpression + '.type');
							var typeSetter = typeGetter.assign;
							typeSetter($scope, resp.data.type);
						}
						if(angular.isDefined(resp.data.size)) {
							var sizeGetter = $parse(filenameDstExpression + '.size');
							var sizeSetter = sizeGetter.assign;
							sizeSetter($scope, resp.data.size);
						}
					}
				};

				// upload errored out
				var errorFunction = function(resp) {
					var errorMessage = '';
					try {
						errorMessage = resp.data.error_message;
					}
					catch (e) {
						errorMessage = 'Something went wrong when uploading the file.';
					}
					var popupBox = $mdDialog.show(
						$mdDialog.alert({
							title: 'Uploading Error',
							textContent: errorMessage,
							ok: 'Continue'
						})
					).finally(function() {});
				};

				// initiate the upload
				Upload.upload({
					url: current_url + '/upload',
					data: uploadData
				}).then(successFunction, errorFunction);
			}
			catch (e) {
				console.log(e);
			}
		};

		$scope.removeResource = function(resource) {
			delete resource.size;
			delete resource.type;
			delete resource.src;
		}

		this.previewTranslation = function() {
			PreviewService.loadPreview(base_url + 'course/' + $rootScope.course.course_id + '/preview/?language=' + $scope.shortcode);
		};

		$scope.buildPage = function(title, keys, original, translated) {

			// by default, the translation editor will use a simple text input
			// for entering the translation data. however, some items require
			// different inputs such as a text area, and some items are objects.
			// i imagine at some point we will use this to handle custom fields,
			// and/or start using advanced fields like a color picker.
			function itemType(item) {
				var text_area_items = ['summary', 'description', 'text'];

				if(text_area_items.indexOf(item) > -1) {
					return 'textarea';
				}
				else if(item == 'quiz summary') {
					return 'quizSummary'
				}
				else {
					return item;
				}
			};

			// if there is no english present in the key, we do not want to show it
			// in the translation editor for translating.
			function showItem(item, data) {

				if(item == 'quiz summary') {
					item = 'summary';
				}

				var data = (item == 'glossary' || item == 'resources' || item == 'common terms' || item == 'cmsCode' || item == 'roles') ? data : data[item];

				if (angular.isObject(data)) {
					var length = (angular.isArray(data)) ? data.length : Object.keys(data).length;

					// if the object or array only contains one item, and that item is the ID,
					// we still need to hide it in the translation editor.
					return (!length || (length == 1 && (data['id'] || data.id))) ? false : true;
				} else {
					return (data == undefined || data == '') ? false : true;
				}
			};

			var key_data = [];

			angular.forEach(keys, function(value){
				if(showItem(value, original)) {
					var item = {key: value, type: itemType(value)};
					key_data.push(item);
				}
			});

			$rootScope.pages.push({
				title: title,
				keys: key_data,
				data: original,
				translation: translated
			});
		};

		$rootScope.updateTranslation = function() {
			$http.post(current_url + '/update_translation', {'data': $scope.transObject.data});

			$scope.translation_prod = $.extend(true, {}, $scope.data, angular.copy($scope.transObject.data));
			$http.post(current_url + '/update_translation', {'data': $scope.translation_prod, 'prod': true});

			$scope.showCourseSaveToast();
		}

		$rootScope.translatePage = function(index) {
			$scope.page_data = $scope.pages[index];
			$scope.current_page = index;
		}

		$rootScope.activeTransPage = function(index) {
			return $scope.current_page == index;
		}

		$scope.firstPage = function() {
			return $scope.current_page == 0;
		}

		$scope.lastPage = function() {
			return $scope.current_page == $rootScope.pages.length - 1;
		}

		$scope.nextPage = function() {
			if ($scope.lastPage()) return;

			$timeout(function(){
				$scope.current_page = $scope.current_page + 1;
				$scope.translatePage($scope.current_page);
			});
			$scope.updateTranslation();
		}

		$scope.prevPage = function() {
			if ($scope.firstPage()) return;

			$timeout(function(){
				$scope.current_page = $scope.current_page - 1;
				$scope.translatePage($scope.current_page);
			});
			$scope.updateTranslation();
		}

		$rootScope.pageControls = [
			{
				type: "single",
				name: "Save",
				action: $scope.updateTranslation,
				permission: { 'success': true }
			},
			{
				type: "single",
				name: "Preview",
				action: this.previewTranslation,
				permission: { 'success': true }
			},
			{
				type: "single",
				name: "Back to Languages",
				action: function() {
					window.location.href = base_url + 'course/' + $scope.course.course_id;
				},
				permission: { 'success': true }
			},
		];

		var translation_pages = [
			{key: '', name: 'Course Details', data: ['course', 'module']},
			{key: 'home', name: 'Homepage', data: ['title', 'titleColor', 'summary', 'media', 'callToAction']},
			{key: 'preTest', name: 'Pre Test', data: ['title', 'text', 'media', 'question', 'quiz summary']},
			{key: 'topic'},
			{key: 'certificate', name: 'Certificate', data: ['text', 'title', 'media']},
			{key: 'cmsCode', name: 'CMS Code'},
			{key: 'common_terms', name: 'Common Terms', data: ['common terms']},
			{key: 'glossary', name: 'Glossary'},
			{key: 'resources', name: 'Resources'},
			{key: 'roles', name: 'Roles'}
		];

		angular.forEach(translation_pages, function(value) {

			// topics and pages are handled a bit differently, so here we will give them VIP treatment
			if(value.key == 'topic') {
				angular.forEach($scope.data.topic, function(topic_object, topic_idx) {
					var title = 'Topic '+(topic_idx+1)+': Title, text & image that display on home page';
					$scope.buildPage(title, ['title', 'description', 'media'], $scope.data.topic[topic_idx], this.topic[topic_idx]);

					angular.forEach(topic_object.page, function(page_object, page_idx) {
						var title = 'Topic ' + (topic_idx+1) + ': Page ' + (page_idx+1) + ': ' + page_object.title;
						var page_keys = [];

						angular.forEach(page_object, function(value, key) {
							// automatically add keys to pages, but not ones that are listed as exceptions
							if(TranslationService.getExceptionsList().indexOf(key) == -1) {
								// sometimes summary is a textarea, and sometimes it is an object - yay
								if (key == 'summary' && angular.isObject(value)) {
									page_keys.push('quiz summary');
								} else {
									page_keys.push(key);
								}
							}
						});

						$scope.buildPage(title, page_keys, $scope.data.topic[topic_idx].page[page_idx], this.topic[topic_idx].page[page_idx]);
					}, this);
				}, $scope.transObject.data);
			} else {
				var has_item = true;

				// if no key is passed in, we are working in the root of the data object.
				var data = (value.key) ? $scope.data[value.key] : $scope.data;

				// if data is not defined for the key, do not continue
				if(data == undefined) return;

				var translation = (value.key) ? $scope.transObject.data[value.key] : $scope.transObject.data;
				var has_items = (angular.isArray(data)) ? data.length > 0 : Object.keys(data).length > 0;

				// if we are not explicitly passing in an array of keys, we will use value.key as the default.
				var page_keys = value.data || [value.key];

				if(angular.isDefined(data) && has_items) {
					$scope.buildPage(value.name, page_keys, data, translation);
				}
			}
		});

		$('body').keydown(function(event){
			if (event.keyCode == '37') {
				if ($('input, textarea, .ta-editor > div').is(':focus')) { return }
				$scope.prevPage();
			}
			else if (event.keyCode == '39') {
				if ($('input, textarea, .ta-editor > div').is(':focus')) { return }
				$scope.nextPage();
			}
		});

		// START TRANSLATION
		$scope.translatePage($scope.current_page);
	});
});

app.controller('extraCtrl', function($scope, actions) {
	$scope.item.extra = $scope.item.extra || [];
});

app.controller('extraItemCtrl', function($scope) {
	this.changeExtra = function(type) {
		if (type == 'tip') {
			$scope.extra.media = $scope.extra.media || [{}];
		} else {
			// remove media
			delete $scope.extra['media'];
			delete $scope.extra['text'];
		}
	}

	$scope.extra.type = $scope.extra.type || '';

	this.list = [
		{"key":"", "name":"Select Type"}
	];

	for (x in $scope.chameleon.extra_list) {
		var item = {};
		item.key = x;
		item.name = $scope.chameleon.extra_list[x];
		this.list.push(item);
	}
});

app.controller('objectiveCtrl', function($rootScope, $scope, $timeout, ChipService) {

	$rootScope.loadData.done(function() {

		// define default data for chip use
		const type = 'objective';
		const pluralType = type + 's';

		// make sure objectives array is set and is type array
		$scope.data.objectives = ($scope.data.objectives && angular.isArray($scope.data.objectives)) ? $scope.data.objectives : [];

		// build objectives page using drag and drop chips
		angular.extend($scope, ChipService);

		$scope.newObject = {};

		// edit an existing chip
		$scope.editObjective = objective => $scope.newObject = objective;

		$scope.addNewObjective = () => {
			// add chip
			$scope.addChip($scope.newObject, $scope.data.objectives, type);

			// reset object once new chip is added
			$scope.resetFields();
		};

		$scope.createNewChip = function(elem) {
			$scope.resetFields();
			$scope.toTop(elem);
		}

		$scope.deleteObjective = function(ev, idx, obj) {
			// prevent chip content from being edited
			ev.stopPropagation();

			// show the delete modal
			$scope.deleteModal(ev, obj.name).then(function(answer) {
				// delete chip from master array
				$scope.data.objectives.splice(idx, 1);

				// remove deleted chip from any existing pages
				$scope.deleteFromAll($scope.data.topic, obj, pluralType);
			});
		}

		$scope.objectiveExists = function(topic, page) {
			$scope.data.topic[topic].page[page].objectives = $scope.data.topic[topic].page[page].objectives || [];
		}

		$scope.resetFields = () => $scope.newObject = {};

		// initialize draggable, and make sure newly added roles become draggable as well
		$scope.$watch('data.objectives', function(newValue, oldValue) {
			$timeout(function() {
				$('.draggable').draggable({
					revert: true,
					distance: 2,
					start: function(event, ui) {
						$(".role-list").css('overflow', 'visible');
					},
					stop: function(event, ui) {
						$(".role-list").css('overflow', 'auto');
					}
				});
			});
		}, true);

		$timeout(function() {
			$('.droppable').droppable({
				hoverClass: 'hovered',
				drop: function(event, ui) {

					// item object [draggable]
					const idx = $(ui.draggable).data('key');
					const obj = $scope.data.objectives[idx];

					// page object [droppable]
					const page = $(this).data('page');
					const topic = $(this).data('topic');
					let scope = $scope.data.topic[topic].page[page].objectives;

					// check for duplicates to prevent errors
					let duplicate = scope.some( value => { return value === obj.key } );

					if(!duplicate) {
						scope.push(obj.key);
						$scope.$digest();
					}
				}
			});
		});
	});
});

app.controller('interactionCtrl', function($scope, $rootScope) {

	$scope.item.interaction = $scope.item.interaction || [];

	this.editContent = ($scope.disableFields || $scope.noTranslate) ? true : false;

	$scope.$watch('template_settings', function(newValue, oldValue) {
		if(angular.isUndefined($scope.template_settings)) return;

		$scope.settings = $scope.template_settings.interactions;

		try {
			$scope.media_limit = $scope.settings.media.limit;
			if (0 == $scope.media_limit) {
				$scope.media_limit = 50;
			}
		}
		catch (e) {
			$scope.media_limit = 1;
		}
	});

	this.canAddInteraction = function() {
		return (
				angular.isUndefined($scope.settings) ||
				angular.isUndefined($scope.settings.limit) ||
				$scope.settings.limit < 1 ||
				$scope.settings.limit > $scope.item.interaction.length
		);
	};
});

app.controller('inlineAudio', function($scope) {

	this.temp_file = '';

	this.initAudio = function(audio) {

		if (!this.temp_file || this.temp_file !== audio) {
			// new audio - reinitialize
			myaudio = new Audio($scope.mediaRoot + 'audio/' + audio);
		}
		this.temp_file = audio;
		this.playPause();
	};

	this.playPause = function() {
		if (myaudio.paused && myaudio.currentTime >= 0 && !myaudio.ended) {
			myaudio.play();
			this.playing = true;
		} else {
			myaudio.pause();
			this.playing = false;
		}
	};
});

app.controller('interactionItemCtrl', function($rootScope, $scope, Upload, $parse, $mdDialog) {

	this.editContent = ($scope.disableFields || $scope.noTranslate) ? true : false;
	$scope.interaction.media = (angular.isArray($scope.interaction.media)) ? $scope.interaction.media : [];
	$scope.enable_media = true;
	$scope.media_limit = 50;
	$scope.interactionAudioPath = null;

	if ($rootScope.template_settings && $rootScope.template_settings.interactions) {

		if ($rootScope.template_settings.interactions.media && angular.isDefined($rootScope.template_settings.interactions.media.enable)) {
			$scope.enable_media = $rootScope.template_settings.interactions.media.enable;
		}

		if ($rootScope.template_settings.interactions.custom && $rootScope.template_settings.interactions.custom.length > 0) {
			$scope.custom_fields = $rootScope.template_settings.interactions.custom;
		}

		$scope.template_settings = $scope.template_settings.interactions;
		$scope.template_settings.media = ($scope.template_settings.media) ? $scope.template_settings.media : {};
		$scope.template_settings.media.limit = ($scope.template_settings.media.limit == 0) ? 50 : $scope.template_settings.media.limit;
	}

	//Allow for the hiding of fields within this controller
	$scope.hideField = function(name) {
		if(angular.isDefined($scope.template_settings[name])) {
			return $scope.template_settings[name];
		} else {
			return true;
		}
	}

	$scope.$watch('interaction.audio', function(newValue, oldValue) {
		$scope.interactionAudioPath = (newValue) ? $scope.audioPath + newValue : null;
	});

	/*
	 * A condensed version if the uploadFile method for course level media.
	 * See the original function in the directives file for a full description.
	 *
	 * This condensed method also exists in playerCtrl, and could probably be
	 * condensed at some point in the future.
	 */
	$scope.uploadFile = function($file, filenameDstExpression, options) {
		if (!$file) {
			return;
		}
		options = options || {};

		// ingest filenameDstExpression and turn it into a setter function if needed
		var filenameGetter = $parse(filenameDstExpression);
		var filenameSetter = filenameGetter.assign;

		// use a custom filename if we are passing one in
		if(options.filename) { filename = options.filename }

		try {
			// target filename:
			//
			// If this media slot has an ID, generate a new name for the file
			// based on the media slot's ID.
			//
			// Otherwise, use the filename provided by the user's browser.
			if (angular.isUndefined(filename)) {
				var filename = $file.name;
			}

			// prepare the payload data
			var uploadData = {
				file: $file,
				name: filename,
				isUnique: false
			};
			if (angular.isDefined(options.classification)) {
				uploadData.type = options.classification;
			}

			// this function will be called if the upload succeeds
			var successFunction = function (resp) {
				// invoke the setter expression for communicating the filename
				// back to the location elected by the caller
				filenameSetter($scope, filename);
				if(options.callback) {
					options.callback();
				}
				if(resp.data.type) $scope.resources.type = resp.data.type;
				if(resp.data.size) $scope.resources.size = resp.data.size;
			};

			// upload errored out
			var errorFunction = function(resp) {
				var errorMessage = '';
				try {
					errorMessage = resp.data.error_message;
				}
				catch (e) {
					errorMessage = 'Something went wrong when uploading the file.';
				}
				var popupBox = $mdDialog.show(
					$mdDialog.alert({
						title: 'Uploading Error',
						textContent: errorMessage,
						ok: 'Continue'
					})
				).finally(function() {});
			};

			// initiate the upload
			Upload.upload({
				url: current_url + '/upload',
				data: uploadData
			}).then(successFunction, errorFunction);
		}
		catch (e) {
			console.log(e);
		}
	};
});

app.controller('assetCtrl', function($scope, $rootScope, Assets, $timeout) {

	$rootScope.loadData.done(function() {

		$scope.asset_array = angular.copy($rootScope.chameleon.assets);

		Assets.get({id: $rootScope.course.course_id}, function(data) {
			angular.forEach($scope.asset_array, function(value) {
				value.map(function(value){
					value.path = data[value.filename] ? returnPath(value) : returnPath(value, true);
				});
			});
		});
	});

	function returnPath(item, master) {
		var pathRoot = (master) ? $rootScope.coreRoot + 'assets/' : $rootScope.assetRoot;
		var filename = item.filename + '?' + new Date().getTime();
		return pathRoot + filename;
	}

	$scope.changeAsset = function(item) {
		// not sure why, but the image will not refresh on the page without a
		// timeout here. i imagine this will fail in certain network conditions, but
		// in my testing, around 400 was the floor for success. Going to set this to
		// 700 for now, as that will provide some additional buffer without appearing
		// like it's taking too long to update. 
		$timeout(function() {
			item.path = returnPath(item);
		}, 700);
	}

	$scope.restoreAsset = function(item) {
		Assets.delete({id: $rootScope.course.course_id, filename: item.filename}, function() {
			item.path = $rootScope.coreRoot + 'assets/' + item.filename;
		})
	}
});

app.controller('questionCtrl', function($scope, $rootScope) {

	// "prompt" was an unfortunate choice for the name of this question property
	// because objects in AngularJS scopes inherit from the window object, which
	// contains a "prompt" propery, but the value assigned to this property is a
	// function (the function that can be used to display a prompt box to the
	// user), so if we haven't assigned a value to the "prompt" property of our
	// object to mask the one that belongs to the window object, we'll get a
	// function back when we ask for this property's value; we're checking for
	// this condition here and setting a prompt property in such a case
	if ('function' == typeof $scope.question.prompt) {
		$scope.question.prompt = '';
	}

	if ($scope.parentTemplate == 'pretest') {
		angular.forEach($rootScope.chameleon.templates, function(value) {
			if (value.key == 'quiz') {
				$scope.template_settings = value;
			}
		});
	}

	this.question_list;

	if(angular.isDefined($scope.template_settings)) {
		this.settings = $scope.template_settings;
		if(angular.isDefined($scope.template_settings.questions) &&
			angular.isDefined($scope.template_settings.questions.enable) &&
			$scope.template_settings.questions.enable == true &&
			$scope.template_settings.questions.type) {
			// check for question template limits if questions are enabled
			this.question_list = $scope.template_settings.questions.type.map(item => { return $scope.chameleon.question_list[item] });
		}
	}

	$scope.$watch('question.template', function(newValue, oldValue) {
		if(newValue) {
			$scope.question.settings = $scope.chameleon.question_list[newValue];
			if(angular.isDefined($scope.question.settings.answers)) {
				$scope.question.answer_settings = $scope.question.settings.answers;
			}
		}
	});

	this.showElement = function(key){
		if (this.settings && this.settings.questions && this.settings.questions.hide) {
			return this.settings.questions.hide.indexOf(key) > -1;
		}
	}

	$scope.question.template = $scope.question.template || '';
	$scope.question.distractor = $scope.question.distractor || [];
	$scope.question.media = $scope.question.media || [];

	//Allow for the hiding of fields within this controller
	$scope.hideField = function(name) {
		if(angular.isUndefined($scope.template_settings.questions)) { return }

		if(angular.isDefined($scope.template_settings.questions[name])) {
			return $scope.template_settings.questions[name];
		} else {
			return true;
		}
	}
});


app.controller('distractorCtrl', function($scope) {

	// show full containers in translator, otherwise content view can be toggled
	this.editContent = ($scope.disableFields || $scope.noTranslate) ? true : false;

	// sometimes booleans are saved as strings. convert them back here so elements like checkboxes and other conditionals work correctly
	$scope.distractor.value = ($scope.distractor.value == 'true' || $scope.distractor.value == true) ? true : false;

	// ensure distractors have media object
	$scope.distractor.media = $scope.distractor.media || [];

	//add hotspot selections to template 606: Hotspot Question
	if ($scope.question.template=="606") {
		this.custom_fields = [{"key":null,"label":"Hotspot Position","type":"hotspot"},{"key":"size","label":"Hotspot Size","type":"select","values":["regular","wide"]},{"key":"direction","label":"Hotspot Popup Location","type":"select","values":["top","bottom","left","right"]}];
	}
	if (angular.isDefined($scope.question.settings)) {
		$scope.$watch('question.settings', function(newValue, oldValue) {
			$scope.showMedia = angular.isDefined($scope.question.settings.answers) &&
				angular.isDefined($scope.question.settings.answers.media) &&
				(angular.isDefined($scope.question.settings.answers.media.enable) && $scope.question.settings.answers.media.enable == true) ||
				angular.isUndefined($scope.question.settings.answers.media.enable);
		});
	}
	

	//Allow for the hiding of fields within this controller
	$scope.hideField = function(name) {
		if(angular.isDefined($scope.question.answer_settings) && angular.isDefined($scope.question.answer_settings[name])) {
			return $scope.question.answer_settings[name];
		} else {
			return true;
		}
	}


});

app.controller('itemsCtrl', function($scope) {

	$scope.data.media = $scope.data.media || [];
	$scope.data.category = $scope.data.category || [];

	$scope.autocompleteDemoRequireMatch = true;
	$scope.selectedItem = null;
	$scope.searchText = null;

	$scope.toggle = function(item, list) {
		var idx = (angular.isString(list[0])) ? list.indexOf(item.title) : list.indexOf(item.key);
		if (idx > -1) {
			list.splice(idx, 1);
		}
		else {
			list.push(item.key);
		}
	}

	//Allow for the hiding of fields within this controller
	$scope.hideField = function(name) {
		if(angular.isDefined($scope.template_settings.items[name])) {
			return $scope.template_settings.items[name];
		} else {
			return true;
		}
	}

	$scope.exists = function(item, list) {
		if(angular.isString(list[0])) {
			return list.indexOf(item.title) > -1;
		}
		else {
			return list.indexOf(item.key) > -1;
		}
	}

	$scope.transformChip = function(chip) {
		// If it is an object, it's already a known chip
		if (angular.isObject(chip)) {
			return chip.key;
		}
		// Otherwise, create a new one
		var new_chip = {};
		new_chip.title = chip;
		$scope.item.categories.push(new_chip);
		return new_chip;
	};

	$scope.querySearch = function(query) {
		var results = query ? $scope.item.categories.filter($scope.createFilterFor(query)) : [];
		return results;
	};

	$scope.createFilterFor = function(query) {
		var lowercaseQuery = angular.lowercase(query);
		return function filterFn(categories) {
			return (angular.lowercase(categories.title).indexOf(lowercaseQuery) === 0);
		};
	};

});

app.controller('categoryCtrl', function($scope) {
		$scope.data.media = $scope.data.media || [];

		$scope.getKey = function(key, data) {
			if(angular.isDefined(key)) { return key }

			var index = -1;
			angular.forEach(data, function(value) {
				if(value.key > index) {
					index = value.key;
				}
			});
			return index+1;
		}

		//Allow for the hiding of fields within this controller
		$scope.hideField = function(name) {
			if(angular.isDefined($scope.template_settings.category[name])) {
				return $scope.template_settings.category[name];
			} else {
				return true;
			}
		}
});

app.controller('userCtrl', function(User, $scope, $http) {

	this.status_toggle = 'all';

	this.test_user = function(id) {
		$http.post(base_url + 'user/test',{id: id}).then(function() {
			window.location.href = base_url + 'client';
		});
	};

	this.goTo = function(id) {
		window.location.href = base_url + 'user/' + id + '/edit';
	};

	User.query().$promise.then(function(data) {
		$scope.user_list = data;
	});
});

app.controller('versionCtrl', function(Versions, $scope, $http) {

	this.goTo = function(id) {
		window.location.href = base_url + 'version/' + id + '/edit';
	};

	Versions.query().$promise.then(function(data) {
		$scope.versions = data;
	});
});

app.controller('clientTheme', function(Themes, $rootScope, $scope, $mdDialog, $filter) {

	let formatThemeName = name => $filter('lowercase')(name).replace(/\s/g,'_');

	let getThemeList = client_id => Themes.query({ client_id });

	let nameCheck = (name, theme_arr) => {
		if(angular.isUndefined(name) || name == '') { return false; }

		// theme_arr is a promise, so it will always return as a defined array
		// even if the results are empty so let's make sure we actually have an
		//array of themes to work with here
		if(angular.isUndefined(theme_arr) || theme_arr.length > 0) {
			return theme_arr.filter(value => formatThemeName(value) === formatThemeName(name)).length <= 0;
		} else {
			return true;
		}
	}

	$rootScope.loadData.done(function() {

		$scope.theme = {};

		// define the default theme
		Themes.get_default({client_id: $rootScope.client.client_id}, (data) => {
			$scope.theme.default_theme = data.result;
		});

		// define the full list of themes
		getThemeList($rootScope.client.client_id).$promise.then((theme_list) => {
			$scope.theme.list = theme_list;
		});

		$scope.addTheme = function(ev, srcThemeId) {
			$mdDialog.show({
				controller: function($scope, $rootScope, inputService) {

					// if srcThemeId is defined, we're copying an existing theme instead
					// of creating a brand new one
					//
					// themes can now be copied across clients, so compile the list of
					// clients the user has access to here (if we are copying) otherwise
					// set to false so the client select does not show when adding a theme
					if (angular.isDefined(srcThemeId)) {
						$scope.addOrCopy = 'Copy';
						$scope.theme.client_id = angular.copy($rootScope.client.client_id);
						$scope.client_list = inputService.formatSelect($rootScope.allowed_clients, 'client_id', 'client_name');
						$scope.theme_name = `Copy of ${srcThemeId}`;
					} else {
						$scope.addOrCopy = 'Add';
						$scope.theme.client_id = $rootScope.client.client_id;
						$scope.client_list = false;
						$scope.theme_name = '';
					}

					$scope.state = 'prompt';
					$scope.validThemeName = null;

					$scope.$watchGroup(['theme_name', 'theme.client_id'], function(newValue, oldValue) {
						$scope.theme_list = getThemeList(newValue[1]).$promise.then((theme_list) => {
							$scope.validThemeName = nameCheck(newValue[0], theme_list);
						});
					});

					$scope.hide = function() {
						$mdDialog.hide();
					};
					$scope.cancel = function() {
						$mdDialog.cancel();
					};
					$scope.answer = function(answer) {

						answer = formatThemeName(answer);

						$scope.mediaCopy = function(asset_arr) {
							var copyDonePromise = new Promise(function(resolve, reject) {
								// prepare variables for indicating progress
								var progressCount = 0;
								$scope.progressText = 'Logos are for sharing';
								$scope.progressPct = 0;
								$scope.state = 'progress';

								// prepare an XHR instance for calling the server-side copy process
								var xhr = new XMLHttpRequest();
								xhr.open('POST', base_url + 'api/media?action=copy_theme_assets&src_client_id=' + $rootScope.client.client_id + '&dst_client_id=' + $scope.theme.client_id, true);
								xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
								var statusRegex = {
									progressPct:  /progress_pct:\s*(\d+)/,
								};
								xhr.onreadystatechange = function() {
									if (2 < xhr.readyState) {
										// isolate new content that arrived since the last state change event
										var new_messages = '';
										if ('undefined' === typeof xhr.previous_text) {
											new_messages = xhr.responseText;
										} else {
											new_messages = xhr.responseText.substring(xhr.previous_text.length);
										}

										if (new_messages) {
											matches = /progress_pct:\s*(\d+)/.exec(new_messages);
											if (null !== matches) {
												$scope.progressPct = matches[1];
												$scope.$apply();
											}

											// print all response messages to the console
											if (new_messages) {
												console.log(`SERVER: ${new_messages}`);
											}
										}

										xhr.previous_text = xhr.responseText;
									}

									// when the server-side process is done, resolve the promise
									if (4 == xhr.readyState) {
										resolve();
									}
								}

								// kick off the server-side process, passing in
								// the mapping received from createNewMediaIDs()
								// to instruct it on which assets to copy and what
								// the new filenames should be
								xhr.send(JSON.stringify(asset_arr));
							});
							return copyDonePromise;
						}

						// if we're copying an existing theme, retrieve it and store it
						// under the new name
						if (angular.isDefined(srcThemeId)) {
							Themes.get({client_id: $rootScope.client.client_id, theme_id: srcThemeId}, function(themeData) {
								// if the the theme is being copied to a different client, we need
								// to also copy the brand assets to the new client
								if($rootScope.client.client_id !== $scope.theme.client_id) {
									$scope.mediaCopy(themeData.assets.logos).then(function() {
										Themes.save({client_id: $scope.theme.client_id, theme_id: answer}, themeData).$promise.then(function() {
											window.location.href = base_url + 'client/' + $scope.theme.client_id + '/theme/' + answer;
										});
									})
								} else {
									Themes.save({client_id: $scope.theme.client_id, theme_id: answer}, themeData).$promise.then(function() {
										window.location.href = base_url + 'client/' + $scope.theme.client_id + '/theme/' + answer;
									});
								}
							});
							return;
						}

						// otherwise, put a blank theme
						Themes.save({client_id: $rootScope.client.client_id, theme_id: answer}, '{}').$promise.then(function() {
							window.location.href = base_url + 'client/' + $rootScope.client.client_id + '/theme/' + answer;
						});
					};
				},
				templateUrl: base_url + 'js/angular/templates/modals/copy_theme.html',
				targetEvent: ev,
				clickOutsideToClose: true,
				scope: $scope,
				preserveScope: true
			});
		};

		$scope.editTheme = function(theme_id) {
			window.location.href = base_url + 'client/' + $rootScope.client.client_id + '/theme/' + theme_id;
		}

		$scope.makeDefault = function(theme_id) {
			Themes.set_default({client_id: $rootScope.client.client_id}, theme_id, function() {
				location.reload();
			});
		}
	});
});

app.controller('editTheme', function(Themes, $rootScope, $scope, $mdDialog, $filter, $timeout, $http, $q, $mdToast, $parse, Upload) {

	$rootScope.showSidenav = true;
	$rootScope.theme_loaded = false;

	$rootScope.loadData.done(function() {
		$rootScope.theme_id = document.location.pathname.replace(/^(\/\w+)+\//, '');

		// use the Themes resource to fetch a list of themes that will populate the
		// theme selector dropdown
		$rootScope.theme_list = Themes.query({
			client_id: $rootScope.client.client_id
		});

		$scope.theme_data = Themes.get_merged({client_id: $rootScope.client.client_id, theme_id: $scope.theme_id}, function(data) {

			$scope.theme_data.uiTheme = $scope.theme_data.uiTheme || 'dark';

			// define parent object for toggle models to live in
			$scope.toggle = {};

			// define the list of type elements that can be modified
			$scope.type_elements = {
				"body": "Body Text",
				"h1": "Header 1",
				"h2": "Header 2",
				"h3": "Header 3",
				"h4": "Header 4",
				"h5": "Header 5",
				"h6": "Header 6",
				"label": "Label Text",
				"caption": "Caption Text"
			};

			var originatorEv;
			$scope.openMenu = function($mdOpenMenu, ev) {
				originatorEv = ev;
				$mdOpenMenu(ev);
			};

			$scope.getElementObject = function(element, item=null) {

				/*
				 * typeface is handled differently, because there are only
				 * two typeface categories available:
				 *	- headingTypeface for header elements
				 *	- bodyTypeface for all other elements
				 */
				var typeface = (element.indexOf('h') > -1) ? $scope.theme_data.variable.headingTypeface : $scope.theme_data.variable.bodyTypeface;

				$scope.elementObject = {
					name: $scope.type_elements[element],
					typeface: typeface,
					textSize: $scope.theme_data.variable[element+'_textSize'],
					fontWeight: $scope.theme_data.variable[element+'_fontWeight'],
					textTransform: $scope.theme_data.variable[element+'_textTransform'],
					lineHeight: $scope.theme_data.variable[element+'_lineHeight'],
					letterSpacing: $scope.theme_data.variable[element+'_letterSpacing']
				}

				if(item) {
					return $scope.elementObject[item];
				}
				else {
					return $scope.elementObject;
				}
			};

			$scope.getElementCss = function(element) {
				var elementObject = $scope.getElementObject(element);

				return {
					'font-family': elementObject.typeface,
					'font-size': elementObject.textSize,
					'font-weight': elementObject.fontWeight,
					'text-transform': elementObject.textTransform,
					'line-height': elementObject.lineHeight,
					'letter-spacing': elementObject.letterSpacing
				};
			};

			$scope.typeElementSettings = {};

			$scope.theme_data.masterFontSizeValue = ($scope.theme_data.masterFontSizeValue) ? $scope.theme_data.masterFontSizeValue : 1;

			$scope.fonts = {
				"Arial, Helvetica, sans-serif": "Arial",
				"'Courier New', Courier, monospace": "Courier",
				"Georgia, serif": "Georgia",
				"'Lucida Sans Unicode', 'Lucida Grande', sans-serif": "Lucida",
				"'Palatino Linotype', 'Book Antiqua', Palatino, serif": "Palatino",
				"'PT Sans', sans-serif": "PT Sans",
				"Tahoma, Geneva, sans-serif": "Tahoma",
				"'Times New Roman', Times, serif": "Times New Roman",
				"'Trebuchet MS', Helvetica, sans-serif": "Trebuchet",
				"Verdana, Geneva, sans-serif": "Verdana"
			};

			$scope.$watch('fonts', function() {
				$scope.ordered_fonts = [];
				angular.forEach($scope.fonts, function(value, key) {
					$scope.ordered_fonts.push({key: key, name: value});
				});
			}, true);

			$scope.fontSelected = function(key) {
				return key == $scope.theme_data.variable.headingTypeface;
			};

			$http.get('https://www.googleapis.com/webfonts/v1/webfonts?key=AIzaSyBb_pLbXGeesG8wE32FMtywG4Vsfq6Uk_8').then(function(data) {
				$scope.google_fonts = data.data.items;
				var selectedText = '';
				//this.fonts = $scope.google_fonts;

				$scope.searchText    = '';
				$scope.selectedItem  = null;
				$scope.querySearch   = querySearch;
				$scope.onSelection = selectedItemChange;
				$scope.addFont = addFontFamily;

				function querySearch (query) {
					var results = query ? $scope.google_fonts.filter( createFilterFor(query) ) : $scope.google_fonts;
					var deferred = $q.defer();
					$timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
					return deferred.promise;
				}

				function createFilterFor(query) {
					var lowercaseQuery = angular.lowercase(query);
					return function filterFn(fonts) {
						var family = angular.lowercase(fonts.family);
						return (family.indexOf(lowercaseQuery) === 0);
					};
				}

				function selectedItemChange(item) {
					if (item){
						$scope.selectedItem = "'"+item.family+"', "+item.category;
						selectedText = item.family;
					}
				}

				function addFontFamily() {
					if ($scope.selectedItem != null){
						// google custom fonts needs to be an object, i think we are
						// experiencing some empty object to array conversions here
						if(angular.isArray($scope.theme_data.customFont.google)) {
							$scope.theme_data.customFont.google = {};
						}
						$scope.theme_data.customFont.google[$scope.selectedItem] = selectedText;
						$scope.fonts[$scope.selectedItem] = selectedText;
						WebFont.load({
							google: {
								families: [selectedText]
							}
						});
						$mdDialog.show(
								$mdDialog.alert()
									.clickOutsideToClose(true)
									.title('Google Font Successfully Added')
									.htmlContent('<p>'+selectedText+' has successfully been added to this theme.<br>You can now select it from the Heading and Body Typface dropdown menu at the top of this page.</p>')
									.ariaLabel('Invalid Typekit ID')
									.ok('OK')
							);
						$scope.selectedItem  = null;
						$scope.searchText  = '';
					}
				}

			});

			$scope.addTypekit = function(i) {
				var init = (i) ? i : false;
				$http.jsonp('//typekit.com/api/v1/json/kits/'+$scope.theme_data.customFont.typekit+'/published?callback=JSON_CALLBACK').success(function(data) {
					if (data.errors) {
						if (!init){
							$mdDialog.show(
									$mdDialog.alert()
										.clickOutsideToClose(true)
										.title('Invalid Typekit ID')
										.textContent('Make sure your Typekit is published and has the proper domains whitelisted.')
										.ariaLabel('Invalid Typekit ID')
										.ok('OK')
								);
						}
							$scope.theme_data.customFont.typekit = '';
					} else {
						WebFont.load({
							typekit: {
								id: $scope.theme_data.customFont.typekit
							}
						});
						angular.forEach(data.kit.families, function(value,key){
							$scope.fonts[value.css_stack] = value.name;
						});
						if (!init){
							$mdDialog.show(
									$mdDialog.alert()
										.clickOutsideToClose(true)
										.title('Typekit Successfully Added')
										.htmlContent('<p>The fonts from your typekit have successfully been added to this theme.<br>You can now select them from the Heading and Body Typface dropdown menu at the top of this page.</p>')
										.ariaLabel('Invalid Typekit ID')
										.ok('OK')
								);
						}
					}
				})
			}

			$scope.addCustomFont = function() {
				$scope.theme_data.customFont.custom.push({'type':'', 'fonts':[{}]})
			};

			$scope.deleteCustomFont = function(idx) {
				 $scope.theme_data.customFont.custom.splice(idx, 1);
			};

			$scope.addCustomFontFamily = function(fonts) {
				fonts.push({});
			};

			$scope.deleteCustomFontFamily = function(fonts, idx) {
				if(fonts.length == 1) {
					fonts.name = '';
					fonts.css = '';
				} else {
					fonts.splice(idx, 1);
				}
			};

			$scope.initFontArray = function(item) {
				item.fonts = item.fonts || [{}];
				angular.forEach(item.fonts, function(value, key) {
					if(angular.isArray(value)) { item.fonts[key] = {} }
				});
			};

			$scope.add3rdParty = function(font) {

				let file = font.src;
				const pattern = new RegExp("[^.]+$");
				let filetype = pattern.exec(file);

				if(filetype == 'css') {
					var link = document.createElement("link");
					link.type = "text/css";
					link.rel = "stylesheet";
					link.href = font.src;
					document.getElementsByTagName("head")[0].appendChild(link);
				}

				if(filetype == 'js') {
					const script = document.createElement('script');
					script.src = font.src;
					document.getElementsByTagName("body")[0].appendChild(script);
				}

				angular.forEach(font.fonts, function(value) {
					$scope.fonts[value.css] = value.name;
				});
				
			}

			if ($scope.theme_data.customFont){
				if ($scope.theme_data.customFont.google && Object.keys($scope.theme_data.customFont.google).length > 0){
					var googleFamilies = [];
					angular.forEach($scope.theme_data.customFont.google, function(value,key){
						$scope.fonts[key] = value;
						googleFamilies.push(value);
					});
					WebFont.load({
						google: {
							families: googleFamilies
						}
					});
				}
				if ($scope.theme_data.customFont.typekit && $scope.theme_data.customFont.typekit!='') {
					$scope.addTypekit(true);
				}
				if ($scope.theme_data.customFont.custom && $scope.theme_data.customFont.custom.length > 0){
					angular.forEach($scope.theme_data.customFont.custom, function(value) {
						$scope.add3rdParty(value);
					});
				}
			} else {
				$scope.theme_data.customFont = {"google":{},"tyepkit":"","custom":[]};
			}

			// construct a path to a file that lives in the client's image directory on S3
			$scope.getClientImagePath = function(filename, doQuote) {
				if (angular.isUndefined(doQuote)) {
					doQuote = true;
				}
				var quote = doQuote ? "'" : '';
				return quote + aws_path + 'clients/' + $rootScope.client.client_id + '/images/' + filename + quote;
			};

			// clears out the logo slot referenced by the given index
			$scope.removeLogoImage = function(index) {
				var imagePath = $scope.getClientImagePath($scope.theme_data.assets.logos[index]);
				$scope.theme_data.assets.logos[index] = undefined;
				$scope.logo_previews[index] = aws_path + 'core/images/placeholder.jpg';

				// if any of the logo variables currently point to the image being
				// removed, we should either (A) select the default logo from the first
				// logo slot; or (B) if no logo exists in the first slot, select the
				// "empty/none" option
				for (var variable_name in $scope.logo_variables) {
					if (imagePath == $scope.theme_data.variable[variable_name]) {
						$scope.theme_data.variable[variable_name] = $scope.theme_data.assets.logos[0] ?
							$scope.getClientImagePath($scope.theme_data.assets.logos[0]) : undefined;
					}
				}
			};

			// define the slots that the user can fill with logo images
			$scope.logo_slots = ['Logo 1','Optional Logo 2','Optional Reversed Logo 1','Optional Reversed Logo 2'];

			// define the variables in theme_data.variable that can be set to logo URLs
			$scope.logo_variables = {
				logoLoading: 'Loading Page',
				logoMobile: 'Mobile Home',
				logoHome: 'Home Page',
				logoMenu: 'Topic Menu'
			};

			// initialize data structures for storing data about logo slots
			$scope.theme_data.assets = $scope.theme_data.assets || {};
			$scope.theme_data.assets.logos = $scope.theme_data.assets.logos || [];

			// initialize a data structure for showing which image is in which logo slot
			$scope.logo_previews = [];
			for (var i = 0; i < $scope.logo_slots.length; i++) {
				$scope.logo_previews[i] = (angular.isDefined($scope.theme_data.assets.logos[i]) && null !== $scope.theme_data.assets.logos[i])?
					$scope.getClientImagePath($scope.theme_data.assets.logos[i], false) :
					aws_path + 'core/images/placeholder.jpg';
			}

			// respond to changes in the list of images that are uploaded to logo slots
			$scope.$watch('theme_data.assets.logos', function(newValue, oldValue) {
				if (oldValue === newValue) {
					return;
				}

				// comparing logos slot by slot
				for (var i = 0; i < newValue.length; i++) {
					if (newValue[i] == oldValue[i]) {
						continue;
					}

					// when a logo is changed/updated, let's make sure we are updating
					// the preview call
					$scope.logo_previews[i] = (angular.isDefined($scope.theme_data.assets.logos[i]) && null !== $scope.theme_data.assets.logos[i])?
						$scope.getClientImagePath($scope.theme_data.assets.logos[i], false) :
						aws_path + 'core/images/placeholder.jpg';

					// if a logo was uploaded to the FIRST logo slot where there did not
					// used to be any logo present in that slot, any unset logo location
					// selections should be changed to point to this newly uploaded logo,
					// because the first logo slot is designated as the default
					if (0 === i && !oldValue[i] && newValue[i]) {
						for (var variable_name in $scope.logo_variables) {
							if (
								null === $scope.theme_data.variable[variable_name] ||
								angular.isUndefined($scope.theme_data.variable[variable_name]) ||
								-1 < $scope.theme_data.variable[variable_name].indexOf('core/images')
							) {
								$scope.theme_data.variable[variable_name] = $scope.getClientImagePath(newValue[i]);
							}
						}
						continue;
					}

					// when we upload a new image into a slot that already contains an
					// image AND one of the logo variables refers to the image being
					// replaced, we'll update the logo variable to refer to the new image
					if (null !== oldValue[i] && null !== newValue[i]) {
						var newImagePath = $scope.getClientImagePath(newValue[i]);
						var oldImagePath = $scope.getClientImagePath(oldValue[i]);
						for (var variable_name in $scope.logo_variables) {
							if (oldImagePath == $scope.theme_data.variable[variable_name]) {
								$scope.theme_data.variable[variable_name] = newImagePath;
							}
						}
						continue;
					}
				}
			}, true);

			$scope.font_weight = ['100','200','300','400','500','600','700','800','900','bold','bolder','lighter','normal'];
			$scope.text_transform = ['capitalize','lowercase','none','uppercase'];

			$scope.typeElement = 'body';

			$scope.avatar_options = {
				'animalFaces':'Animal Faces',
				'carnivalFaces':'Carnival Faces',
				'gamePieces':'Game Pieces',
				'mac': 'Mac',
				'simpleFaces': 'Simple Faces'
			};

			$scope.saveTheme = function() {
				Themes.save({client_id: $rootScope.client.client_id, theme_id: $scope.theme_id}, $scope.theme_data, function() {
					$scope.showSaveToast();
				});
			};

			$scope.showConfirm = function(key, ev) {
				if(!$scope.toggle[key]) {
					$mdDialog.show({
						controller: function($scope, $mdDialog) {
							$scope.hide = function() {
								$mdDialog.hide();
							};
							$scope.cancel = function() {
								$mdDialog.cancel();
							};
							$scope.answer = function(answer) {
								$mdDialog.hide(answer);
							};
						},
						template: `
							<md-content class="md-padding message">
								<p>It is suggested you have a full understanding of the editor before adjusting these settings. Altering advanced settings can potentially affect legibility, interactions, responsive capability and core functionality.</p>
								<p>Please check your entire course after adjusting.<p>
								<div layout="row">
									<md-button ng-click="answer()">Continue</md-button>
									<md-button ng-click="cancel()">Cancel</md-button>
								</div>
							</md-content>
						`,
						onShowing: function(scope,elem,attr) {
							$(elem).attr('id','alert-modal');
						},
						parent: angular.element(document.body),
						targetEvent: ev,
						clickOutsideToClose:false,
						fullscreen: true
					})
					.then(function(answer) {
						// continue
					}, function() {
						$scope.toggle[key] = false;
					});
				}
			};

			$rootScope.$watch('theme_section', function(newValue) {
				if(newValue == 'ui') {
					//$timeout(function() {
						var darkSlideshow = $('.preview-dark.ui-slideshow');
						darkSlideshow.owlCarousel({
							items: 1,
							loop: true,
							rewind: false,
							dots: false,
							autoplay: true,
							center: true
						});
					//}, 50);

					//$timeout(function() {
						$('.preview-light.ui-slideshow').owlCarousel({
							items: 1,
							loop: true,
							rewind: false,
							dots: false,
							autoplay: true,
							center: true
						});
					//}, 50);
				}
				if (newValue == 'assets') {
					$('.avatar-slideshow img').css({'height':'90px','width':'90px'});
				}
			});

			$scope.resetToLastSave = function(ev) {
				$mdDialog.show({
					controller: function($scope, $mdDialog) {
						$scope.hide = function() {
							$mdDialog.hide();
						};
						$scope.cancel = function() {
							$mdDialog.cancel();
						};
						$scope.answer = function(answer) {
							$mdDialog.hide(answer);
						};
					},
					template: `
						<md-content class="md-padding message">
							<p>This will return the editor to your last saved theme. Any settings or uploads will be deleted</p>
							<div layout="row">
								<md-button ng-click="answer()">Continue</md-button>
								<md-button ng-click="cancel()">Cancel</md-button>
							</div>
						</md-content>
					`,
					onShowing: function(scope,elem,attr) {
						$(elem).attr('id','alert-modal');
					},
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:false,
					fullscreen: true
				})
				.then(function(answer) {
					// continue
					location.reload();
				}, function() {
					// do nothing
				});
			};

			$scope.resetToDefault = function(ev) {
				$mdDialog.show({
					controller: function($scope, $mdDialog) {
						$scope.hide = function() {
							$mdDialog.hide();
						};
						$scope.cancel = function() {
							$mdDialog.cancel();
						};
						$scope.answer = function(answer) {
							$mdDialog.hide(answer);
						};
					},
					template: `
						<md-content class="md-padding message">
							<p>This will return the editor to the default theme. Any settings or uploads will be deleted</p>
							<div layout="row">
								<md-button ng-click="answer()">Continue</md-button>
								<md-button ng-click="cancel()">Cancel</md-button>
							</div>
						</md-content>
					`,
					onShowing: function(scope,elem,attr) {
						$(elem).attr('id','alert-modal');
					},
					parent: angular.element(document.body),
					targetEvent: ev,
					clickOutsideToClose:false,
					fullscreen: true
				})
				.then(function(answer) {
					// continue
					Themes.save({client_id: $rootScope.client.client_id, theme_id: $scope.theme_id}, '{}', function() {
						location.reload();
					});
				}, function() {
					// do nothing
				});
			};

			$scope.toastPosition = {
				bottom: false,
				top: true,
				left: false,
				right: true
			};

			$scope.getToastPosition = function() {
				return Object.keys($scope.toastPosition)
					.filter(function(pos) { return $scope.toastPosition[pos]; })
					.join(' ');
			};

			$scope.showSaveToast = function() {
				$mdToast.show(
					$mdToast.simple()
						.content('Theme Saved Successfully!')
						.position($scope.getToastPosition())
						.hideDelay(2000)
				);
			};

			/*
			 * A condensed version if the uploadFile method for client theme level media.
			 * See the original function in the directives file for a full description.
			 *
			 * This condensed method also exists in playerCtrl, and could probably be
			 * condensed at some point in the future.
			 */
			$scope.uploadFile = function($file, filenameDstExpression) {
				if (!$file) {
					return;
				}

				// ingest filenameDstExpression and turn it into a setter function if needed
				var filenameGetter = $parse(filenameDstExpression);
				var filenameSetter = filenameGetter.assign;

				try {
					// target filename:
					//
					// If this media slot has an ID, generate a new name for the file
					// based on the media slot's ID.
					//
					// Otherwise, use the filename provided by the user's browser.
					if (angular.isUndefined(filename)) {
						var filename = $file.name;
					}

					// prepare the payload data
					var uploadData = {
						file: $file,
						name: filename,
						isUnique: false,
						type: 'theme'
					};

					// this function will be called if the upload succeeds
					var successFunction = function (resp) {
						// invoke the setter expression for communicating the filename
						// back to the location elected by the caller
						filenameSetter($scope, filename);
					};

					// upload errored out
					var errorFunction = function(resp) {
						var errorMessage = '';
						try {
							errorMessage = resp.data.error_message;
						}
						catch (e) {
							errorMessage = 'Something went wrong when uploading the file.';
						}
						var popupBox = $mdDialog.show(
							$mdDialog.alert({
								title: 'Uploading Error',
								textContent: errorMessage,
								ok: 'Continue'
							})
						).finally(function() {});
					};

					// initiate the upload
					Upload.upload({
						url: current_url + '/upload',
						data: uploadData
					}).then(successFunction, errorFunction);
				}
				catch (e) {
					console.log(e);
				}
			};

			$rootScope.theme_section = 'ui';
			$rootScope.theme_loaded = true;

			$rootScope.pageControls = [
				{
					type: "single",
					name: "Save",
					action: $scope.saveTheme,
					permission: {'success': true}
				},
				{
					type: "multi",
					name: "Reset",
					options: [
						{ name: "Last Save", action: $scope.resetToLastSave },
						{ name: "Default", action: $scope.resetToDefault }
					],
					permission: {'success': true}
				}
			];
		});
	});
});

app.controller('curriculumCtrl', function($scope, $rootScope, $mdDialog, $http) {
	this.predicate = 'date_created';
	this.reverse = !this.reverse;

	this.curriculumDetail = function(ev, data) {
		$mdDialog.show({
			controller: function($scope, $mdDialog, client_id, data) {
				$scope.title = (data) ? 'Edit Curriculum' : 'Add New Curriculum';
				$scope.data = angular.copy(data) || {};

				if(!$scope.data.curriculum_status) {
					$scope.data.curriculum_status = 1;
				}

				$scope.hide = function() {
					$mdDialog.hide();
				};
				$scope.cancel = function() {
					$mdDialog.cancel();
				};
				$scope.answer = function(answer) {
					$mdDialog.hide(answer);
				};
			},
			templateUrl: base_url + 'js/angular/templates/modals/new_curriculum.html',
			parent: angular.element(document.body),
			targetEvent: ev,
			clickOutsideToClose:true,
			locals: {client_id: $rootScope.client.client_id, data: data}
		})
		.then(function(answer) {
			$http.post(base_url + 'client/add_curriculum', {'data': answer}).then(function(data) {
				window.location.href = base_url + 'client/' + $rootScope.client.client_id;
			});
		}, function() {
			// cancelled
		});
	};
});

app.controller('PlayerCtrl', function(Curriculum, $scope, $rootScope, $http, $mdDialog, $parse, Upload) {

	// set defaults
	var color = "#DADADA";
	var expiration = 14;

	$scope.player = {};
	$scope.users = {};
	$scope.player.expiration = $scope.player.expiration || expiration;
	$scope.player.color = $scope.player.color || color;
	$scope.page = 'config';

	// check the database to pull in saved data for the application config tab
	$http.get(current_url + '/player_config').then(function(data) {
		if(data.data) {
			$scope.player = data.data;
			// ensure expiration value is an integer
			$scope.player.expiration = parseInt($scope.player.expiration);
		}
	});

	// check the database to pull in saved data for course ordering list
	$http.get(current_url + '/player_order').then(function(data) {
		$scope.player_order = data.data;
	});

	$scope.curriculum_list = null;
	Curriculum.query(function(data){
		if(data.length > 0) {
			$scope.curriculum_list = [
				{"key":"", "name":"Select a Curriculum"}
			];
			angular.forEach(data, function(value) {
				var item = {};
				item.key = value.curriculum_id;
				item.name = value.curriculum_name;
				$scope.curriculum_list.push(item);
			});
		}
	});

	$scope.importUsers = function(ev, data) {
		$http.post('/player/save_users', {
			'client_id' : $scope.client.client_id,
			'curriculum': data.curriculum,
			'users'     : data.users
		}).then(function(response){
			// success. empty fields and show alert
			var title = "Success";
			var message = "Users were successfully saved.";

			// import will return a warning if the user attempts to add an email
			// address that already exists in the database.
			if(response.data) {
				title = 'Email already exists:';
				message = response.data;
			}

			// show alert
			$scope.showAlert(ev, message, title);

			// reset fields on save
			$scope.users = {};

		});
	}

	/*
	 * A condensed version if the uploadFile method for course level media.
	 * See the original function in the directives file for a full description.
	 */
	$scope.uploadFile = function($file, filenameDstExpression) {
		if (!$file) {
			return;
		}

		// ingest filenameDstExpression and turn it into a setter function if needed
		var filenameGetter = $parse(filenameDstExpression);
		var filenameSetter = filenameGetter.assign;

		try {
			// target filename:
			//
			// If this media slot has an ID, generate a new name for the file
			// based on the media slot's ID.
			//
			// Otherwise, use the filename provided by the user's browser.
			if (angular.isUndefined(filename)) {
				var filename = $file.name;
			}

			// prepare the payload data
			var uploadData = {
				file: $file,
				name: filename,
				isUnique: false,
				type: 'client'
			};

			// this function will be called if the upload succeeds
			var successFunction = function (resp) {
				// invoke the setter expression for communicating the filename
				// back to the location elected by the caller
				filenameSetter($scope, filename);
			};

			// upload errored out
			var errorFunction = function(resp) {
				var errorMessage = '';
				try {
					errorMessage = resp.data.error_message;
				}
				catch (e) {
					errorMessage = 'Something went wrong when uploading the file.';
				}
				var popupBox = $mdDialog.show(
					$mdDialog.alert({
						title: 'Uploading Error',
						textContent: errorMessage,
						ok: 'Continue'
					})
				).finally(function() {});
			};

			// initiate the upload
			Upload.upload({
				url: current_url + '/upload',
				data: uploadData
			}).then(successFunction, errorFunction);
		}
		catch (e) {
			console.log(e);
		}
	};

	$rootScope.loadData.done(function() {
		$scope.getMediaUrl = function() {
			return $rootScope.aws_path + 'clients/' + $scope.client.client_id + '/images/' + $scope.player.logo;
		}
	});

	$scope.removeImage = function() {
		$scope.player.logo = '';
	}

	$scope.saveConfig = function(ev, data) {
		$http.post(current_url + '/player_config', data).then(
			function(response) {
				// courseware player config settings were successfully saved
				// so now save the invite code to the client table
				$http.post(current_url + '/update_db', {'data': $scope.client}).then(
					function(response) {
						// client invite code was successfully saved
						$scope.showAlert(ev, 'Configuration settings were successfully saved.');
					},
					function(response) {
						console.log(response);
					}
				);
			},
			function(response) {
				console.log(response);
			}
		);

	}

	$scope.saveOrder = function(ev) {
		$http.post(current_url + '/player_order', $scope.player_order).then(
			function(response) {
				$scope.showAlert(ev, 'Course order was successfully saved.');
			},
			function(response) {
				console.log(response);
			}
		);
	}

	$scope.showAlert = function(ev, message, title) {
		title = (title) ? title : 'Success';

		$mdDialog.show(
			$mdDialog.alert()
				.clickOutsideToClose(true)
				.title(title)
				.textContent(message)
				.ariaLabel('User Upload Status')
				.ok('Continue')
				.targetEvent(ev)
		);
	}

});

app.controller('avatarSlideshow', function($scope, $timeout) {


	$scope.avatarCount = 20;

	$scope.getAvatars = function(num) {
	return new Array(num);
	}

	$scope.getAvatarIndex = function(x) {
	x = x+1;
	return (x.toString().length > 1) ? x : '0' + x;
	}

	$timeout(function() {
		$('.avatar-slideshow').owlCarousel({
			items: 7,
			loop: false,
			dots: false,
			margin: 20,
			navContainer: '.navigation',
			navText: ['<md-button class="md-icon material-icons">chevron_left</div>','<md-button class="md-icon material-icons">chevron_right</div>']
		});
	}, 100);
});

app.controller('roleCtrl', function($rootScope, $scope, $timeout, ChipService) {

	$rootScope.loadData.done(function() {

		// define default data for chip use
		const type = 'role';
		const pluralType = type + 's';

		// make sure roles array is set and is type array
		$scope.data.roles = ($scope.data.roles && angular.isArray($scope.data.roles)) ? $scope.data.roles : [];

		// build roles page using drag and drop chips
		angular.extend($scope, ChipService);

		$scope.newObject = {};

		$scope.addNewRole = () => {
			// add chip
			$scope.addChip($scope.newObject, $scope.data.roles, type, $scope.applyToAll);

			// reset object once new chip is added
			$scope.resetFields();
		};

		$scope.createNewChip = function(elem) {
			$scope.resetFields();
			$scope.toTop(elem);
		}

		$scope.deleteRole = function(ev, idx, obj) {
			// prevent chip content from being edited
			ev.stopPropagation();

			// show the delete modal
			$scope.deleteModal(ev, obj.name).then(function(answer) {
				// delete chip from master array
				$scope.data.roles.splice(idx, 1);

				// remove deleted chip from any existing pages
				$scope.deleteFromAll($scope.data.topic, obj, pluralType);
			});
		}

		$scope.roleExists = function(topic, page) {
			$scope.data.topic[topic].page[page].roles = $scope.data.topic[topic].page[page].roles || [];
		}

		$scope.resetFields = () => $scope.newObject = {};

		// initialize draggable, and make sure newly added roles become draggable as well
		$scope.$watch('data.roles', function(newValue, oldValue) {
			$timeout(function() {
				$('.draggable').draggable({
					revert: true,
					distance: 2,
					start: function(event, ui) {
						$(".role-list").css('overflow', 'visible');
					},
					stop: function(event, ui) {
						$(".role-list").css('overflow', 'auto');
					}
				});
			});
		}, true);

		$timeout(function() {
			$('.droppable').droppable({
				hoverClass: 'hovered',
				drop: function(event, ui) {

					// item object [draggable]
					const idx = $(ui.draggable).data('key');
					const obj = $scope.data.roles[idx];

					// page object [droppable]
					const page = $(this).data('page');
					const topic = $(this).data('topic');
					let scope = $scope.data.topic[topic].page[page].roles;

					// check for duplicates to prevent errors
					let duplicate = scope.some( value => { return value === obj.key } );

					if(!duplicate) {
						scope.push(obj.key);
						$scope.$digest();
					}
				}
			});
		});
	});
});

app.controller('analyticsCtrl', function($scope, $rootScope, Analytics) {

	// set the default date range to one week, beginning today
	$scope.endDate = new Date();
	$scope.startDate = new Date(
		$scope.endDate.getFullYear(),
		$scope.endDate.getMonth(),
		$scope.endDate.getDate()-7
	);
	$scope.courseAnalytics = '0';

	$rootScope.loadData.done(function() {

		$scope.$watchGroup(['startDate', 'endDate', 'courseAnalytics'], function(newValue, oldValue) {

			// clear charts if they have already been rendered
			if($scope.topCountriesChart) $scope.topCountriesChart.destroy();
			if($scope.courseActivityChart) $scope.courseActivityChart.destroy();

			// hide analytics content until the data has completed loading
			$scope.loaded = false;

			Analytics.get({id: $scope.client.client_id, startDate: newValue[0], endDate: newValue[1], course: newValue[2]}, function(data) {

				$scope.loaded = true;
				$scope.countriesDisplay = 'map';
				$scope.tableDisplay = 'browser';

				angular.extend($scope, data);

				// TOP COUNTRIES REPORTING
				var countriesMapData = [
					['Country', 'Sessions']
				];
				var countriesChartData = [];
				var countriesChartLabels = [];
				angular.forEach(data.countries, function(value) {
					countriesMapData.push([value.country, parseInt(value.sessions)]);

					countriesChartData.push(value.sessions);
					countriesChartLabels.push(value.country);
				});

				// TOP COUNTRIES PIE CHART
				var tc_ctx = document.getElementById("topCountries").getContext("2d");
				$scope.topCountriesChart = new Chart(tc_ctx, {
						type: 'doughnut',
						data: {
							datasets: [{
								data: countriesChartData,
								backgroundColor: ['#4C5260','#939DB0','#D4CBC4','#E1E9E8','#F94345'],
							}],
							labels: countriesChartLabels
						},
						options: {
							legend: {
								position: 'left'
							}
						}
				});

				// TOP COUNTRIES MAP
				google.charts.load('current', {
					'packages':['geochart'],
					// Note: you will need to get a mapsApiKey for your project.
					// See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
					'mapsApiKey': 'AIzaSyCE9QLGymUFOmKeDmdb1OmXiinpDpmuak8'
				});
				google.charts.setOnLoadCallback(drawRegionsMap);

				function drawRegionsMap() {
					var data = google.visualization.arrayToDataTable(countriesMapData);
					var options = {};
					var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));
					chart.draw(data, options);
				}

				// COURSE ACTIVITY REPORTING
				dateChartLabels = [];
				dateChartData = [];
				angular.forEach(data.date, function(value) {
					// extract the year
					var year = value.date.slice(0,4);
					// extract month
					var month = value.date.slice(4,6);
					// extract day
					var day = value.date.slice(6,8);

					var date = month + '/' + day + '/' + year;

					dateChartLabels.push(date);
					dateChartData.push(value.sessions);
				});

				// COURSE ACTIVITY LINE CHART
				var ca_ctx = document.getElementById("courseActivity").getContext("2d");
				$scope.courseActivityChart = new Chart(ca_ctx, {
					type: 'line',
					data: {
						labels: dateChartLabels,
						datasets: [
							{
								label: 'Sessions',
								data: dateChartData,
								borderColor: '#008ac6',
								backgroundColor: '#e6f5fa'
							}
						],
					},
					options: {
						scales: {
							yAxes: [{
								ticks: {
									min: 0,
									stepSize: 1
								}
							}]
						}
					}
				});

				$scope.percent = function(int) {
					return Math.round((int / $scope.summary.sessions) * 100);
				}
			});
		});


	});
});

app.controller('templateListCtrl', function($scope) {

	/*
	 * function to check if the template group is changing and split
	 * the template view accordingly
	 */
	$scope.templateGroup = 0;
	$scope.groupChange = function(group) {
		if($scope.templateGroup !== group) {
			$scope.templateGroup = group;
			return true;
		}
		return false;
	}

	$scope.mobile=false;
	$scope.$watch('mobile',function(newValue,oldValue) {
		if(newValue.mobile) {
			newValue.mobile=false;
		} else {
			newValue.mobile=true;
		}
	},true);

	$scope.q = {'text': true, 'image': true, 'both': true};

	$scope.$watch('q', function(newValue, oldValue) {
		if(newValue.text || newValue.image) {
			newValue.both = true;
		} else {
			newValue.both = false;
		}
	}, true);

	/*
	 * adding informative descriptions to the template group numbers
	 */
	$scope.groupTitle = function(group) {
		switch(group) {
			case '200':
				return group + '. Static Page Layouts';
				break;
			case '300':
				return group + '. Click-Tell Layouts';
				break;
			case '400':
				return group + '. Sortable Checkpoints & Multi-Layer Activities';
				break;
			case '500':
				return group + '. Multi-Level';
				break;
			case '700':
				return group + '. Checkpoints';
				break;
			case 'Quiz':
				return 'Standard Quiz and Games';
				break;
			default:
				return group;
		}
	}
});