var app = angular.module('chameleonEditor', ['ngRoute', 'ngSanitize', 'ui.sortable', 'chamCtrl', 'chameleonDir', 'editorElements', 'chameleonFilters', 'chameleonServices', 'ngMaterial', 'ngResource', 'ngMessages', 'ui.codemirror', 'minicolors', 'ui.router', 'ngFileUpload']);

app.factory('Assets', function($resource) {
	return $resource(base_url + 'api/assets/:id/:filename', null);
});

app.factory('Analytics', function($resource) {
	return $resource(base_url + 'api/analytics/:id', {id: '@_id', startDate: ':startDate'});
});

app.factory('Clients', function($resource) {
	return $resource(base_url + 'api/clients/:id', {id: '@_id'});
});

app.factory('Courses', function($resource) {
	return $resource(base_url + 'api/courses/:id', {id: '@_id'});
});

app.factory('Curriculum', function($resource) {
	return $resource(base_url + 'curriculum/:id', {id: '@_id'});
});

app.factory('User', function($resource) {
	return $resource(base_url + 'api/user/:id', {id: '@_id'}, {
		'can': {
			method: 'GET',
			params: {
				action: 'can'
			}
		}
	});
});

app.factory('UserRoles', function($resource) {
	return $resource(base_url + 'api/userroles/:id', {id: '@_id'});
});

app.factory('Versions', function($resource) {
	return $resource(base_url + 'api/versions/:id', {id: '@_id'});
});

// resource to call actions in the 'media' method of the 'api' CI controller
app.factory('Media', function($resource) {
	return $resource(base_url + 'api/media', null, {
		'enumerate_media_assets': {
			method: 'GET',
			params: {
				action: 'enumerate_media_assets'
			}
		},
		'get_adobe_image_editor_api_key': {
			method: 'GET',
			params: {
				action: 'get_adobe_image_editor_api_key'
			},
			cache: true
		},
		'download_to_course': {
			method: 'POST',
			params: {
				action: 'download_to_course'
			}
		},
		'initialize_translation': {
			method: 'POST',
			params: {
				action: 'initialize_translation'
			}
		}
	});
});

// factory for creating an Adobe image editor object
app.factory('aviaryEditor', function(Media, $rootScope) {
	var editor = {};

	// stores a reference to an image editor object for this slot
	editor.instance = null;

	// initialize the editor object to prepare it for use
	editor.initialize = function() {

		// kick off fetching the Adobe Creative SDK API key
		editor.apiKey = Media.get_adobe_image_editor_api_key();

		// we need to delay insantiation of the image editor until we have the API key
		editor.apiKey.$promise.then(function() {
			editor.instance = new Aviary.Feather({
				apiKey: editor.apiKey,

				// Defines what happens when the user clicks the "Save" button in the
				// image editor.
				//
				// When the editor launches, we feed it the ID of the image element
				// containing the image that we want to edit along with the URL of the
				// image that is loaded into that element.  The user then does work on
				// the image to produce a new image which is then saved to a location
				// on Adobe's Amazon S3 storage space. When the user presses the "Save"
				// button, the method below is called with the same image ID and the
				// new URL to the modified image. It is our responsibility to fetch the
				// image and update the display of the element containing the edited
				// image.
				onSave: function(dummy, newURL) {
					//console.log('Edited copy of image is available at ' + newURL);

					// isolate the filename of the edited image
					var image_url = editor.image_element.attr('src');
					var target_filepath = image_url.replace(/^https?:\/\/[^\/]+\/[^\/]+\/(.*?)(?:\?.*)?$/, '$1')

					// call a web service that will download new version of the image to
					// the course's image directory; since we're targeting the same
					// filename, this will OVERWRITE the original image
					Media.download_to_course({}, {
						course: $rootScope.editor.course.id,
						url: newURL,
						target_filepath: target_filepath
					})

					// when the copy operation is done, invoke the callback function,
					// which should be responsible for updating the display
					.$promise.then(function() {
						if (angular.isDefined(editor.saveCallback)) {
							editor.saveCallback();
						}
						editor.instance.close();
					});
				}
			});
		});

		return editor.apiKey.$promise;
	};

	// this is the method that is wired up to an image's click event to launch
	// the image editor
	editor.editImage = function(img) {
		editor.image_element = img;
		var params = {
			image: img,
			url: img.attr('src')
			// NOTE: It seems kind of dumb to have to supply the URL of the image
			// when we're already passing a reference to the image element in the
			// 'image' property.  However, this is intentional, and the image editor
			// will not function correctly if it is missing.  From Adobe's
			// documentation, here's why: "Providing it is a clue to our API that
			// your image is on another domain and you need to request origin-clean
			// data from our server which adds to image load time."
		};
		editor.instance.launch(params);
		return false;
	};

	return editor;
});

// resource supporting the course repository on the course dashboard
app.factory('CourseRepository', function($resource) {
	return $resource(base_url + 'api/courses/:course_id/repository/:filename', null, {
		s3_directpost: {
			method: 'GET',
			url: base_url + 'api/courses/:course_id/repository/s3_directpost/:filename'
		}
	});
});

// resource to call methods within the 'theme' CI controller
app.factory('Themes', function($resource) {
  return $resource(base_url + 'api/clients/:client_id/themes/:theme_id', null, {
		'get':         {method:'GET'},
		'get_merged':  {
			method: 'GET',
			params: {merge: true}
		},
		'query':       {method:'GET', isArray:true},
		'save':        {method:'PUT'},
		'remove':      {method:'DELETE'},
		'delete':      {method:'DELETE'},
		'get_default': {
			method: 'GET',
			url: base_url + 'api/clients/:client_id/settings/default_theme'
		},
		'set_default': {
			method: 'PUT',
			url: base_url + 'api/clients/:client_id/settings/default_theme'
		}
	});
});

app.factory('Editor', function($http) {
	return $http.get(aws_path + 'core/editor.json');
});

app.factory('JsonData', function($resource) {
	return $resource(base_url + 'api/data/:id/:state');
});

app.factory('CommonTerms', function($http) {
	return $http.get(aws_path + 'core/terms.json');
});

app.config(function($mdThemingProvider, $stateProvider, $urlRouterProvider, $sceDelegateProvider) {

	$mdThemingProvider.alwaysWatchTheme(true);

	// Editor Color Theme
	$mdThemingProvider.theme('default')
		.primaryPalette('blue')
		.accentPalette('purple');

	$mdThemingProvider.theme('grey')
		.primaryPalette('grey');


	// URL Origin Policy Settings
	$sceDelegateProvider.resourceUrlWhitelist([
		// Allow same origin resource loads.
		'self',
		// Allow loading from S3
		'https://s3.amazonaws.com/**'
	]);
});

app.run(function(EditorState, chameleonData, userData, $rootScope, $mdDialog, $mdToast, $mdSidenav, $http, Versions, $q, CommonTerms) {

	/* * * * * * * * * * * * * * * * * * * *
	 * USER SESSION FUNCTIONALITY
	 * * * * * * * * * * * * * * * * * * * */

	var sessionInterval;

	// Check the validity of the login session.
	//
	// Validity is a function both of the amount of time left in the session (a
	// session with no time in it is expired) and of the login session ID (it
	// should match what was recorded in the login record at the time of last
	// login).
	//
	// If the session is valid, but only has a small amount of time left before
	// it expires, show an alert to let the user know that their session is about
	// to expire due to inactivity.
	(function () {
		var warning_seconds = 60 * 5;
		var timer = 15 * 1000;

		// a utility function for showing a modal alert box
		var popupBox = null;
		$rootScope.alertBox = function(title, message, onClose) {
			if (popupBox !== null) {
				$mdDialog.hide(popupBox);
			}
			popupBox = $mdDialog
			.show($mdDialog.alert({
				title: title,
				textContent: message,
				ok: 'Continue'
			}))
			.finally(function() {
				popupBox = null;
				onClose();
			});
		};

		// a utility function for getting a cookie value
		function getCookie(cname) {
			var name = cname + "=";
			var ca = document.cookie.split(';');
			for(var i = 0; i <ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0)==' ') {
					c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
					return c.substring(name.length,c.length);
				}
			}
			return null;
		}

		// If a user is clicking, they are active and we want to make sure their
		// session reflects this activity, so we call a method on the CI session
		// hook that resets the session expiration.
		var lastPing = null;
		var updateActiveSession = function() {
			var time = Math.floor(new Date().getTime() / 1000);
			if (null === lastPing || (time - 60) > lastPing) {
				$http.get(base_url + '?chameleoneditor_session_hook=1&action=update_active');
				lastPing = time;
			}
		};
		$(document).click(updateActiveSession);

		// show an alert box notifying the user that their login session has
		// expired, ensure that certain cookies pertaining to login are removed,
		// and redirect to the logout page
		var session_expired_alert_shown = false;
		var logout_url = window.location + '?chameleoneditor_session_hook=1&action=logout';
		function showSessionExpiredAlert(message) {
			clearInterval(sessionInterval);
			$(document).unbind('click', updateActiveSession);
			$http.get(logout_url).then(function() {
				if (angular.isUndefined(message)) {
					message = 'Your session has expired. Please log in again.';
				}
				if (session_expired_alert_shown) {
					return;
				}
				$rootScope.alertBox('Session Expired', message, function() {
					window.location.replace(base_url, 'ajax/logout');
				});
				session_expired_alert_shown = true;
			});
		}

		// show an alert box notifying the user that their login session will
		// expire soon
		var session_warning_alert_shown = false;
		function showSessionWarningAlert() {
			if (session_warning_alert_shown) {
				return;
			}
			$rootScope.alertBox('Session Alert', 'Please click "Continue" to remain logged in and continue with this session.');
			session_warning_alert_shown = true;
		}

		// periodic check to test the validity of the login session
		var check_validity_url = window.location + '?chameleoneditor_session_hook=1&action=check_validity';
		var previous_seconds_left = null;
		sessionInterval = setInterval(function() {

			// check the validity of the login session and take action if the session
			// is no longer valid, or if it will expire soon
			$http.get(check_validity_url)
			.then(function(response) {

				// an error result indicates that the session isn't good anymore
				if (response.data.error) {
					//console.log(response.data.error);
					var message = '';
					if (response.data.error.match(' does not match provided login session ID ')) {
						message = 'This session has ended because you have logged in from another location.';
					}
					showSessionExpiredAlert(message);
					return;
				}

				// non-error result should contain seconds left in session
				if (response.data.result) {
					//console.log(response.data.result);
					var seconds_left = response.data.result.time_until_expiration;

					// if the number of seconds left in the session went *up* instead of
					// down, that means that there must have been some session activity;
					// if we already showed an alert box telling the user that their
					// session was about to expire, then they probably reacted to it in
					// order to keep their session alive; now we have to reset the flag
					// saying that we already showed them the warning so that we can show
					// it again if we need to
					if (session_warning_alert_shown && null !== previous_seconds_left && previous_seconds_left < seconds_left) {
						//console.log('resetting alert flag');
						session_warning_alert_shown = false;
					}
					previous_seconds_left = seconds_left;

					// warn the user if there is not much time left in their session
					if (seconds_left <= warning_seconds) {
						// set lastPing to null to ensure that any click that happens from
						// now on will result in us pinging CI to update session expiration
						// time
						lastPing = null;
						showSessionWarningAlert();
						return;
					}
				}
			});
		}, timer);
	})();

	$rootScope.editor = EditorState.buildData();

	$rootScope.loadData = $.Deferred();
	$rootScope.loadUserData = $.Deferred();

	// editor configuration
	$http.get(aws_path + 'core/editor.json').then(function(data) {
		var maintenance = data.data.maintenance;
		if(maintenance.enabled && !sessionStorage.maintenance) {
			$mdToast.show({
				controller: 'maintenance',
				templateUrl: base_url + 'js/angular/templates/modals/maintenance.html',
				parent: document.body,
				hideDelay: false,
				position: 'top right',
				locals: {title: maintenance.title, message: maintenance.message}
			})
		}
	});

	userData.getData().finally();

	var loadCourseDataPromise = chameleonData.getData().finally(function(response){

		if(sessionStorage && sessionStorage.client) {
				if(sessionStorage.client == $rootScope.client.client_id) {
						$rootScope.editor.client.section = (sessionStorage.clientSection) ? sessionStorage.clientSection : 'courses';

						if(sessionStorage.course && $rootScope.editor.course && (sessionStorage.course == $rootScope.editor.course.id)) {
							$rootScope.editor.course.section = ($rootScope.course.state == 'translate') ? 'translate' : 'detail';
						} else {
							$rootScope.editor.course.section = ($rootScope.course.state == 'translate') ? 'translate' : 'detail';
							sessionStorage.course = $rootScope.editor.course.id;
						}
				} else {
					sessionStorage.client = $rootScope.editor.client.id;
				}
		}
		else {
			// reset course session
			sessionStorage.client = $rootScope.client.client_id;
			sessionStorage.page = 0;
			sessionStorage.topic = -1;
		}
	});

	loadCourseDataPromise.then(function() {
		if(!angular.isDefined($rootScope.data)) return;

		// convert 'image' properties of topics into legacy media objects
		for (topicKey in $rootScope.data.topic) {
			if ('undefined' === typeof $rootScope.data.topic[topicKey].media) {
				var mediaObject = {type: 'image'};
				if ('undefined' === typeof $rootScope.data.topic[topicKey].image) {
					mediaObject.image = [];
				} else {
					mediaObject.image = [$rootScope.data.topic[topicKey].image];
					delete $rootScope.data.topic[topicKey].image;
				}
				$rootScope.data.topic[topicKey].media = [mediaObject];
			}
		}

		// intialize certificate media object
		if ('undefined' !== typeof $rootScope.data.certificate && 'undefined' === typeof $rootScope.data.certificate.media) {
			var mediaObject = {type: 'image'};
			if ('undefined' === typeof $rootScope.data.certificate.image) {
				mediaObject.image = [];
			} else {
				mediaObject.image = [$rootScope.data.certificate.image];
				delete $rootScope.data.certificate.image;
			}
			$rootScope.data.certificate.media = [mediaObject];
		}

		// determine the course version by checking the json data and the database
		$rootScope.course_version = (angular.isDefined($rootScope.data.config.version)) ? $rootScope.data.config.version : $rootScope.course.course_version;

		if($rootScope.course_version && $rootScope.course_version[0] !== '2') {
			Versions.get({id: 'latest'}, function(ver) {
				if($rootScope.course_version !== ver.version_number) {
					$rootScope.dataMaintenance(ver);
				}
			});
		} else {
			$rootScope.dataMaintenance_legacy();
		}

		// since the maintenance function will only run once, and never runs again
		// after saving itself, I'm going to enable a manual way to force the
		// maintenance function to run, just in case. This way, if for whatever reason
		// we need to force run maintenance, we can do it in the console instead of
		// modifying version numbers in the data.
		window.chameleon_editor = {};
		window.chameleon_editor.runMaintenance = $rootScope.dataMaintenance;
	});

	// GLOBAL COURSE DB SAVE FUNCTION
	$rootScope.saveCourseDB = function(callback) {
		if($rootScope.course) {
			$http.post(base_url + 'course/' + $rootScope.course.course_id + '/update_db', {'data': $rootScope.course}).success(function(data) {
				console.log('Course DB saved');
				if(callback) { callback(); }
			});
		}
	};

	/*
	 * DATA MAINTENANCE FUNCTION
	 * contains code that needs to run when a course version changes to
	 * cleanup or otherwise modify existing course data to work with
	 * the latest version
	 */
	$rootScope.dataMaintenance = function(ver) {

		// this section expects a user to be inside course, and thusly the course
		// json data to exist, do not proceed if it does not
		if(angular.isUndefined($rootScope.data)) { return }

		$rootScope.saveCourseData = function(data) {
			$http.post(base_url + 'course/' + $rootScope.course.course_id + '/update', {'data': data}).success(function() {
				console.log('Course data saved');
			});
		};

		$rootScope.saveTranslationData = function(shortcode, data) {
			$http.post(base_url + 'course/' + $rootScope.course.course_id + '/update_translation/' + shortcode, {'data': data}).success(function() {
				console.log('Translation data saved (' + shortcode + ')');
			});
		};

		// return the translation data for the specified language
		$rootScope.translationData = function(shortcode) {
			return $http.get(current_url + '/get_translation/' + shortcode);
		};

		// check course version, and update to the latest version automatically
		// automatic version update should only apply to courses that are higher
		// than version 2.
		//
		// NOTE: we are not updating the version of the course in the database,
		// because this could have adverse effects on courses that already exist
		// in production, but have been modified on dev. To address this, we are
		// instead adding the course version to the main.js data file.
		if(ver) { $rootScope.data.config.version = ver.version_number }

		/* * * * * * * * * * * * * * * * * *
		 * COMMON TERMS UPDATES
		 * * * * * * * * * * * * * * * * * */

		// make sure old common term keys match the new format. specifically, remove
		// punctuation and spaces and use camelCase. A few exceptions have different
		// keys entirely, and one exception is all caps (ABCDEFGHIJK) - so we must
		// account for those separately.
		$rootScope.commonTermsMaintenance = function(terms, promise, isTranslation) {

			if(terms && Object.keys(terms).length > 0) {
				CommonTerms.then(function(data) {

					let new_common_terms = {};
					angular.forEach(terms, (v, k) => {

						// update keys
						switch(k) {
							case "ABCDEFGHIJK":
								break;
							case "A fun way to learn from BI Worldwide":
								k = "escapeSlogan";
								break;
							case "Answer 50% correct to reach full velocity":
								k = "escapeThreshold";
								break;
							case "Are you sure you want to skip the pre-test?":
								k = "skipPretestConfirm";
								break;
							case "Click menu in the lower left to see the pages you’ve missed within the course.":
								k = "reviewMissedPages";
								break;
							case "Drag items to correct the order":
								k = "dragItemsOrder";
								break;
							case "Escape the planet's atmosphere by gaining speed":
								k = "escapeDirections";
								break;
							case "For the best experience, please view in landscape orientation":
								k = "landscapeWarning";
								break;
							case "For the best experience, please view in portrait orientation":
								k = "portraitWarning";
								break;
							case "Please check your internet and network connection or see your IT Administrator":
								k = "checkInternet";
								break;
							case "Please complete items on this page to move forward in the course":
								k = "completeItemsWarning";
								break;
							case "Please complete items on this page within the alloted time":
								k = "completeItemsTimeout";
								break;
							case "Select an item to match it with its correct pairing":
								k = "selectToMatch";
								break;
							case "select the ''play audio'' button to hear the first clip":
								k = "selectToHear";
								break;
							case "Some or all of your progress has been reset.":
								k = "progressReset";
								break;
							case "Some pages/topics in this course have changed.":
								k = "topicsChanged";
								break;
							case "Test your knowledge of Over The Top Technology":
								k = "testYourKnowledge";
								break;
							case "There is more content further down on this page. Scroll down within your browser.":
								k = "scrollForMore";
								break;
							case "Think you should cover this info in more detail?":
								k = "shouldYouCoverThisAgain";
								break;
							case "This will not be able to be changed. If you are unsure of your role, please stop the session and check with your course administrator.":
								k = "roleSelectionWarning";
								break;
							case "To see your results, be sure to place the minimum number of cards in":
								k = "minimumCardsWarning";
								break;
							case "View your certificate by selecting the certificate icon in the control bar":
								k = "howToViewCertificate";
								break;
							case "You may need to relaunch this course to continue tracking":
								k = "relaunchToContinueTracking";
								break;
							case "You missed one or more of the correct answers":
								k = "missedOneOrMoreCorrect";
								break;
							case "You missed one or more of the correct answers and you chose one or more of the incorrect answers":
								k = "missedOneOrMoreCorrectAndIncorrect";
								break;
							case "You must complete every element on this page before moving forward":
								k = "completeElementWarning";
								break;
							case "You need to complete all pages":
								k = "completeAllPages";
								break;
							case "You ran out of fuel and got a little toasty on reentry. Let's stay positive and say you survived.":
								k = "escapeFailMessage";
								break;
							case "You selected all the correct answers but also one or more of the incorrect answers":
								k = "allCorrectSomeIncorrect";
								break;
							case "Your device has reconnected to the internet":
								k = "reconnectedMessage";
								break;
							case "You've reached escape velocity and have broken free of the planet's gravity field.":
								k = "escapePassMessage";
								break;
							default:
								if(k.indexOf(' ') > 0) {
									k.split(' ').map((v, idx) => {
										if(idx === 0) {
											k = v.toLowerCase();
										} else {
											v = v.replace(/\W/g, '');
											v = v[0].toUpperCase() + v.slice(1).toLowerCase();
											k += v;
										}
									});
								}
								k = k[0].toLowerCase() + k.slice(1);
						}

						// while we are going through and updating common terms, we also need
						// to make sure the terms are in a case that matches legacy styles
						// that were removed from the framework.
						function transformText(key, value) {

							// LOWERCASE
							const lowercase_arr = [ "startCourse", "viewMenu", "startTopic", "restart", "resume" ];

							// UPPERCASE
							const uppercase_arr = [
								"help",
								"seeFinalFeedback",
								"retry",
								"seeFullCourseMenu",
								"view",
								"previous",
								"next",
								"nextStep",
								"back",
								"backToTop",
								"matchToTheItem",
								"cardsLeft",
								"languages",
								"listenToPageNarration",
								"seeCorrectOrder",
								"audioOn",
								"audioOff",
								"getStarted",
								"glossary",
								"objectives",
								"resources",
								"download",
								"menu",
								"completeAllPages",
								"topicsChanged",
								"youHaveLostNetworkConnection",
								"reconnectedMessage",
								"assignPagesToRole",
								"thisPageIsLocked",
								"thisPageIsTimed",
								"youHaveCompletedThisCourse"
							]

							lowercase_arr.map(item => {
								if(key == item) {
									value = value.toLowerCase();
								}
							});
							
							uppercase_arr.map(item => {
								if(key == item) {
									value = value.toUpperCase();
								}
							});

							return value;
						}

						if(!isTranslation) {
							// remove unmodified terms (in English) and update remaining ones
							// the their correct styles.
							const master_terms_list = data.data.common_terms;
							if(master_terms_list[k] && master_terms_list[k].term !== v) {
								new_common_terms[k] = transformText(k, v);
							}
						} else {
							new_common_terms[k] = transformText(k, v);
						}

					});

					promise.resolve(new_common_terms);

				});
			} else {
				promise.resolve(terms);
			}
		}

		$rootScope.updateData = function(data, promise, isTranslation) {
			var deferTerms = $.Deferred();

			$rootScope.commonTermsMaintenance(data.common_terms, deferTerms, isTranslation);
			deferTerms.done(function(terms) {

				data.common_terms = terms;

				angular.forEach(data.topic, function(topic, topic_key) {
					angular.forEach(topic.page, function(page, page_key) {

						/* * * * * * * * * * * * * * * * * *
						 * SUMMARY UPDATES
						 * * * * * * * * * * * * * * * * * */

						// loop through summaries and sort by threshold score so we can pull out
						// the first (pass) and last (fail) in the group
						function summaryThresholdFunction(summary_data) {
							var hasThreshold = summary_data.filter(item => { return item.threshold });

							if(hasThreshold && hasThreshold.length > 0) {
								var sorted_summaries = [];
								var key_map = hasThreshold.map( item => { return item.threshold }).sort((a, b) => (a - b)).reverse();

								angular.forEach(hasThreshold, function(value) {
									var key = key_map.indexOf(value.threshold);
									sorted_summaries[key] = value;
								});
							}

							return sorted_summaries;
						}

						// copy summary data and convert summaries from an array to an object
						// automatically set activityAttempts to 'infinite'
						if(angular.isDefined(page.summary) && angular.isArray(page.summary)) {
							// temporarily store data
							var summary_data = page.summary;
							page.summary = {};
							page.activityAttempts = 'infinite';

							if(angular.isUndefined(page.scored) || page.scored == false) {
								// unscored
								if(summary_data.length === 1) {
									page.summary.unscored = summary_data[0];
								}
								else if(summary_data.length > 1) {
									page.summary.initial = {};
									var hasType = summary_data.filter(item => { return item.summaryType });
									
									if(hasType && hasType.length > 0) {
										angular.forEach(hasType, function(value) {
											page.summary.initial[value.summaryType] = value;
										});
									}
									else {
										var sorted_summaries = summaryThresholdFunction(summary_data);
										page.summary.initial.pass = sorted_summaries[0];
										page.summary.initial.fail = sorted_summaries[sorted_summaries.length-1];
									}
								}
							} else {
								// scored
								page.summary.initial = {};
								var hasType = summary_data.filter(item => { return item.summaryType });

								if(summary_data.length === 1) {
									if(hasType && hasType.length > 0) {
										page.summary.initial[summary_data[0].summaryType] = summary_data[0];
									} else {
										page.summary.initial.pass = summary_data[0];
									}
								}
								else if(summary_data.length > 1) {
									if(hasType && hasType.length > 0) {
										angular.forEach(hasType, function(value) {
											page.summary.initial[value.summaryType] = value;
										});
									}
									else {
										var sorted_summaries = summaryThresholdFunction(summary_data);
										if(sorted_summaries) {
											page.summary.initial.pass = sorted_summaries[0];
											page.summary.initial.fail = sorted_summaries[sorted_summaries.length-1];
										} else {
											page.summary.initial.pass = summary_data[0];
											page.summary.initial.fail = summary_data[summary_data.length-1];
										}
									}
								}
							}
						}

						/* * * * * * * * * * * * * * * * * *
						 * CATEGORY UPDATES
						 * * * * * * * * * * * * * * * * * */

						// because we added the ability to reorder categories, we can no
						// longer use the index as the category key, for that reason we must
						// now use the category title to reference a particular category in
						// an item
						//
						// alright, so to add more chaos to this whole process, it turns out
						// the category name in items was never translated, so we have a bunch
						// of translation files with an item category set to null. As is the
						// default behavior of the translation editor, if a field is set to null,
						// it will populate with the English data by default. Which means we have
						// English item category keys trying to match translated categories.
						// To fix this, we have to pass in the English data, match the item key
						// with its English counterpart to get the key (or possibly updated string)
						// of the category. Then appropriately loop through the categories to get
						// the translated title of the category so we can apply it to the item
						// category.
						if(page.items && page.items.length > 0) {
							// loop through items
							angular.forEach(page.items, function(items, item_key) {
								// do the items in the page have a category?
								if(items.category && items.category.length > 0) {
									// loop through item category
									angular.forEach(items.category, function(category, category_key) {
										// if the category is a string, it was updated in a previous maintenance
										// to something really stupid that I thought would fix the category sorting
										// issues, but really just created more work and issues.
										if(angular.isString(category)) {
											// loop through page.categories to find the matching title and return the
											// category value back to the key
											angular.forEach(this, function(data) {
												if(data.title == category) {
													items.category[category_key] = data.key;
												}
											});
										}
										if(isTranslation) {
											// if we are here, we are in a translation, and the category key is most likely null
											// pull out the english category title and categories array
											var item_category = data.topic[topic_key].page[page_key].items[item_key].category[category_key];
											items.category[category_key] = item_category;
										}
									}, page.categories);
								}
							});
						}

						/* * * * * * * * * * * * * * * * * *
						 * ROLE UPDATES
						 * * * * * * * * * * * * * * * * * */

						// roles set to a page were previously stored as full objects, but really
						// just need to be keys pointing to an existing object - so we will convert
						// the existing duplicate objects to keys here
						if(angular.isDefined(page.roles) && page.roles.length > 0) {
							angular.forEach(page.roles, function(roles, role_key) {
								if(angular.isDefined(roles) && roles !== null && angular.isObject(roles)) {
									data.topic[topic_key].page[page_key].roles[role_key] = roles.key;
								}
							});
						}

						/* * * * * * * * * * * * * * * * * *
						 * OBJECTIVE UPDATES
						 * * * * * * * * * * * * * * * * * */

						// with the objective update, a page can now have an array of
						// objectives rather than just a single value string. the previous
						// objective field was 'objective', but we are now adding a new
						// one 'objectives' that better identifies the value options. for
						// a page that has an existing objective, let's copy that into the
						// new objectives array
						if(page.objective && page.objective !== '') {
							data.topic[topic_key].page[page_key].objectives = data.topic[topic_key].page[page_key].objectives || [];
							data.topic[topic_key].page[page_key].objectives.push(page.objective);
						}

						/* * * * * * * * * * * * * * * * * *
						 * QUIZ UPDATES
						 * * * * * * * * * * * * * * * * * */

						// the untimed audio quiz type now is limited to a different question
						// type, so update that here
						if(page.template == 'quiz-listen-untimed') {
							angular.forEach(page.question, function(value) {
								value.template = '605';
							});
						}

						promise.resolve(data);
					})
				});
			});
		}

		// update categories for English data
		const englishPromise = $.Deferred();
		$rootScope.updateData($rootScope.data, englishPromise);
		englishPromise.done(function(data) {
			$rootScope.saveCourseData(data);
		});

		// check the english data to see if any translations have been added
		// if so make sure each of those data files is updated as well
		if($rootScope.data.config.languages && $rootScope.data.config.languages.length > 0) {
			angular.forEach($rootScope.data.config.languages, function(value) {
				// attempt to pull in translation data
				$rootScope.translationData(value.shortcode).success(function(data) {
					// update categories for each language data file
					const languagePromise = $.Deferred(); 
					$rootScope.updateData(data, languagePromise, true);
					languagePromise.done(function(data) {
						$rootScope.saveTranslationData(value.shortcode, data);
					});
				});
			});
		}

		$rootScope.dataMaintenance_legacy();
	}

	$rootScope.dataMaintenance_legacy = function() {
		// we are changing the way a course progresses through states. There is no longer
		// a course_status defining the current stage in a series of progressive states.
		// Now we have three states ("author", "translate" and "locked"). The 'course_status'
		// field remains in the database, but a new field was added called 'state'. Let's
		// populate the correct state using the existing course_status integer.
		//
		// OLD STAGES
		// Development								0
		// Post-Production QA					1
		// Content QA									2
		// PM Review									3
		// Client Review							4
		// Functional QA							5
		// Client Acceptance Review		6
		// Live												7
		//
		// If a course is 3 or less, it is in the 'author' state.
		// If a course is 4 or greater, it is in the 'translate' state.
		// If a course is 7, it is live, so let's lock it down.
		if($rootScope.course && $rootScope.course.state == null) {
			if(parseInt($rootScope.course.course_status) <= 3) {
				console.log('Updating course state to "author"');
				$rootScope.course.state = 'author';
			}
			else if(parseInt($rootScope.course.course_status) >= 4) {
				console.log('Updating course state to "translate"');
				$rootScope.course.state = 'translate';
			}
			else if(parseInt($rootScope.course_course_status) == 7) {
				console.log('Locking live course.');
				$rootScope.course.locked = 1;
			}
			$rootScope.saveCourseDB();
		}
	}
});
