CKEDITOR.plugins.add( 'tip', {
  requires: 'widget',

  icons: 'tip',

  init: function( editor ) {
  	editor.widgets.add( 'tip', {
  		button: 'Tip',

			template:
			  '<div class="ac-tip">' +
					'<input name="accordiontip-1" type="checkbox" />' +
					'<label class="tip-title">Tip Title</label>' +
					'<article>' +
						'<div class="tip-content">Tip Text</div>' +
					'</article>' +
				'</div>',

			editables: {
				title: {
					selector: '.tip-title'
				},
				content: {
					selector: '.tip-content'
				}
			},

			requiredContent: 'label(tip-title); div(tip-content)',

			dialog: 'tip',

			data: function() {
				if(this.data.title) {
					$(this.element.$).find('.tip-title').html(this.data.title);
				}
			}
  	});

  	CKEDITOR.dialog.add( 'tip', this.path + 'dialogs/tip.js' );
  }

});