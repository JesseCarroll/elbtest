CKEDITOR.dialog.add( 'tip', function( editor ) {
    return {
        title: 'Add New Tip',
        minWidth: 200,
        minHeight: 100,
        contents: [
        	{
        		id: 'info',
        		elements: [
        			{
        				id: 'title',
        				type: 'text',
        				label: 'Tip Title',
				        setup: function( widget ) {
				            this.setValue( widget.data.title );
				        },
				        commit: function( widget ) {
				            widget.setData( 'title', this.getValue() );
				        }
        			}
        		]
        	}
        ]
    };
} );