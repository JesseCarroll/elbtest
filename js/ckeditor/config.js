/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	config.toolbar = 'Full';

	config.toolbar_Full = [
		{ name: 'clipboard', items : [ 'Undo','Redo','-','Cut','Copy','Paste','PasteText' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
		{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','Blockquote'] },
		{ name: 'styles', items : [ 'Styles','Format', 'FontSize', 'lineheight' ] },
		{ name: 'colors', items : [ 'TextColor','BGColor' ] },
		{ name: 'links', items : [ 'Link','Unlink' ] },
		{ name: 'insert', items : [ 'Tip','CodeSnippet','Table','HorizontalRule','SpecialChar' ] },
		{ name: 'document', items : [ 'Preview' ] }
	];

	config.allowedContent = true;
	CKEDITOR.dtd.$removeEmpty['label'] = false;

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Anchor,Image,NewPage,Flash,Smiley,PageBreak,Iframe,FontSize,Format,Font,CreateDiv,PasteFromWord,Save,Print';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;h4;h5;h6;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

	config.forcePasteAsPlainText = true;

	config.extraPlugins = 'lineutils,widget';
	config.plugins+=',lineheight';
	config.line_height=".5em;.6em;.7em;.8em;.9em;1em;1.1em;1.2em;1.3em;1.4em;1.5em;1.6em;1.7em;1.8em;1.9em;2em;2.1em;2.2em;2.3em;2.4em;2.5em;2.6em;2.7em;2.8em;2.9em;3em";

	config.stylesSet = 'my_styles';

	config.disableNativeSpellChecker = false;
	
	// CHAM-2723 - remove formatting from content that is pasted in from Word
	config.pasteFromWord = true;
	config.pasteFromWordRemoveFontStyles = true;
	config.pasteFromWordNumberedHeadingToList = true;
};

CKEDITOR.stylesSet.add('my_styles', [
	{name: 'No Wrap', element: 'span', styles: {'white-space': 'nowrap'}}
]);