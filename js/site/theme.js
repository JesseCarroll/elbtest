$(document).ready(function () {
    var i = $(window).width();
    if( i > 750 ){
        var url = window.location.href;
        var page = url.substr(url.lastIndexOf('/')+1);
        // console.log('URL '+url);
        // console.log('PAGE '+page);
        $('.nav .main-nav li a').removeClass();
        if( page == "" ){
            // console.log('Home');
            $('.nav .main-nav li:first-child a').addClass('activated');
        } else if( page == 'adaptive-design' || page == 'user-experience' || page == 'leaderboards-engagement' || page == 'course-editor' || page == 'translation-editor' ){
            // console.log('Passion');
            $('.nav .main-nav li:nth-child(3) a').addClass('activated');
        } else if( page == 'built-in-cloud' || page == 'scorm-data' || page == 'technical-specs' ){
            // console.log('Technology');
            $('.nav .main-nav li:nth-child(4) a').addClass('activated');
        } else if( page == 'why-global' || page == 'global-challenges' || page == 'global-adaptive-design' || page == 'multiple-languages' || page == 'global-course-editor' || page == 'global-translation-editor' ){
            // console.log('Global');
            $('.nav .main-nav li:nth-child(5) a').addClass('activated');
        } else{
            // console.log('Else');
            $('.nav .main-nav li a[href*="'+url+'"]').addClass('activated');
        }
    }

    //$('.denial-text.ie').remove();
    if( $('html').hasClass('ie9') || $('html').hasClass('ie8') ){
        $('html.ie .main-content').empty();
        $('html.ie footer').remove();
        $('.main-content').prepend("<div class='denial-text ie'><h3>This course is available on modern browsers, including Chrome, Safari, Firefox and Internet Explorer 11. Please reopen the course using one of these approved browsers.</h3></div>");
        $('body').css('background','#000');
    }

    $("#myModal").on("shown.bs.modal", function() {
        $("#myInput").focus()
    });
    
    /*var a = $(window).height();
    480 > a || 480 == i || 500 > i ? ($(".page-container.page").css("height", "inherit"), $(".page-container.page").css("min-height", "inherit")) : ($(".page-container.page").css("height", a + "px"), $(window).resize(function() {
        var i = $(window).height();
        $(".page-container.page").css("height", i + "px")
    })),*/ $('a[href^="#"]').on("click", function(i) {
        i.preventDefault();
        var n = this.hash,
            e = $(n);
        $("html, body").stop().animate({
            scrollTop: e.offset().top
        }, 900, "swing")
    })

	$('.internal-sample').on('click', function() {
		var resource = $(this).data('resource');
		$('.modal input[name=resource]').val(resource);
	});

	$('.ie9 input').each(function() {
		if($(this).attr('placeholder')) {
			$(this).val($(this).attr('placeholder'));
		}
	}).click(function(){
		if(this.value == $(this).attr('placeholder')) {
			this.value = '';
		}
	}).blur(function() {
		if(this.value == '') {
			this.value = $(this).attr('placeholder');
		}
	});
});