{
   "course": "BIW Medical Demo",
   "module": "Anatomy of the Brain",
   "config": {
      "debug":false,
      "narration":false,
      "api":"none"
   },
   "home":
      {
         "title":"Welcome to the <strong>Anatomy of the Brain</strong> eLearning module.",
         "summary":"<p>It is important to have an understanding of the anatomy and physiology of the brain in a healthy state so you can have customer conversations about Disengaged™ therapies and the conditions they treat.</p>",
         "media":[
            {
               "type":"image",
               "image":["homescreen1.jpg","homescreen2.jpg","homescreen3.jpg"],
               "alt":"Alt tag"
            }
         ]
      },
   "topic":[
      {
         "title":"Brain Structure",
         "image":"topic-1.jpg",
         "description":"The brain is the most complex organ in the human body. It has the same general structure as the brains of other mammals, but a more developed cortex than any other. Learn about the general structure in this topic.",
         "page":[
		 {
               "template":"201",
               "title":"Cerebral Hemispheres",
               "text":"<p>The cerebral hemispheres (the cerebrum) form the largest part of the human brain and are situated above other brain structures. They are covered with a cortical layer (the cerebral cortex) which has a convoluted topography.</p><p>Underneath the cerebrum lies the brainstem, resembling a stalk on which the cerebrum is attached. At the rear of the brain, beneath the cerebrum and behind the brainstem, is the cerebellum, a structure with a horizontally furrowed surface, the cerebellar cortex that makes it look different from any other brain area.</p>",
			   "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s1.jpg"]
                  }
               ]
            },
			{
               "template":"401",
               "title":"Important Structures",
               "text":"<p>Every structure of the brain serves a vital purpose. </p>",
               "interaction-prompt":"",
               "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s2.jpg"]
                  }
               ],
               "interaction":[
                  {
                     "label":"Corpus Callosum",
                     "text":"Wide, flat bundle of neural fibers beneath the cortex in the brain at the longitudinal fissure. It connects the left and right cerebral hemispheres and <strong>facilitates interhemispheric communication</strong>. It is the largest white matter structure in the brain.",
                     "x-pos":"25",
                     "y-pos":"8"
                  },
                  {
                     "label":"Cerebral Cortex",
                     "text":"Outermost layered structure of neural tissue of the cerebrum. Often referred to as grey matter, it consists of cell bodies and capillaries. <strong>The cerebral cortex plays a key role in memory, attention, perceptual awareness, thought, language, and consciousness</strong>.",
                     "x-pos":"77",
                     "y-pos":"8"
                  },
                  {
                     "label":"Thalamus",
                     "text":"Midline symmetrical structure of two halves, situated between the cerebral cortex and midbrain. <strong>Relays sensory and motor signals to the cerebral cortex and regulates consciousness, sleep and alertness</strong>.",
                     "x-pos":"80",
                     "y-pos":"28"
                  },
                  {
                     "label":"Cerebellum",
                     "text":"Located at the bottom of the brain, with the large mass of the cerebral cortex above it and the portion of the brainstem called the pons in front of it. <strong>The region of the brain that plays an important role in motor control</strong>.",
                     "x-pos":"79",
                     "y-pos":"48"
                  },
                  {
                     "label":"Brainstem",
                     "text":"Posterior part of the brain, adjoining and structurally continuous with the spinal cord. <strong>It provides the main motor and sensory innervation to the face and neck via the cranial nerves</strong>.",
                     "x-pos":"68",
                     "y-pos":"69"
                  },
                  {
                     "label":"Hypothalamus",
                     "text":"Portion of the brain containing a number of small nuclei with a variety of functions; one of the most important of which is to link the nervous system to the endocrine system via the pituitary gland. <strong>Responsible for certain metabolic processes and other activities of the autonomic nervous system</strong>.",
                     "x-pos":"38",
                     "y-pos":"60"
                  }
               ]
            },
			{
               "template":"303",
               "title":"Brain Cells",
               "text":"<p>Brains of all species are comprised primarily of two broad classes of cells: neurons and glial cells. </p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Glial Cells",
                     "text":"Glial cells (also known as glia or neuroglia) come in several types, and perform a number of critical functions, including structural support, metabolic support, insulation, and guidance of development.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3a.jpg"]
                        }
                     ]

                  },
                  {
                     "label":"Neurons",
                     "text":"Neurons, however, are usually considered the most important cells in the brain. The property that makes neurons unique is their ability to send signals to specific target cells over long distances. They send these signals by means of an axon, which is a thin protoplasmic fiber that extends from the cell body and projects, usually with numerous branches, to other areas, sometimes nearby, sometimes in distant parts of the brain or body.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3b.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Neurotransmitters",
                     "text":"Axons transmit signals to other neurons by means of specialized junctions called synapses. A single axon may make as many as several thousand synaptic connections with other cells. When an action potential, traveling along an axon, arrives at a synapse, it causes a chemical called a neurotransmitter to be released. The neurotransmitter binds to receptor molecules in the membrane of the target cell.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3c.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Synapses",
                     "text":"<p>Synapses are the key functional elements of the brain. The essential function of the brain is cell-to-cell communication, and synapses are the points at which communication occurs. The human brain has been estimated to contain approximately 100 trillion synapses.</p><p>The functions of these synapses are very diverse: some or excitatory, others are inhibitory; others work by activating second messenger systems that change the internal chemistry of their target cells in complex ways.</p><p><em>Play the video to see an animation.</em></p>",
                     "media":[
                        {
                            "type":"video",
                     		"caption":"",
                     		"image":["t1s3d.jpg"],
                     		"video":"t1s3d.mp4"
                        }
                     ]
                  }
               ]
            },
			{
               "template":"206",
               "title":"Disengaged Conditions Overview",
               "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus. </p>",
               "media":[
                  {
                     "type":"slideshow",
                     "caption":"",
                     "image":["t1s4b.jpg","t1s4a.jpg"],
                     "popup":false
                  },
                  {
                     "type":"pullquote",
                     "caption":"Help your customers imagine the possibilities with EngageMe™"
                  }
               ]
            },
			{
               "template":"307",
               "title":"Case Studies",
               "text":"<p>What is it like to live with disengage syndrome? Explore these true stories to find out. You’ll meet Nora, Liang, John and Soma – discover how their conditions affected their lives, and how BIW therapies helped them engage again.</p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Nora, Philadelphia",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Nora’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5a.jpg"],
						   "video":"t1s5a.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Liang, Beijing",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Liang’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5b.jpg"],
						   "video":"t1s5b.mp4"
                        }
                     ]
                  },
                  {
                     "label":"John, London",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play John’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5c.jpg"],
						   "video":"t1s5c.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Soma, Chennai",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Soma’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5d.jpg"],
						   "video":"t1s5d.mp4"
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "title":"Physiology",
         "image":"topic-2.jpg",
         "description":"The brain serves as the center of the nervous system. This topic will explain how it works.",
         "page":[
            {
               "template":"399",
               "title":"Medical Device Demo",
               "interaction-prompt":"Apply what you've learned below in the medical device simulation",
               "text":"Outermost layered structure of neural tissue of the cerebrum. Often referred to as grey matter, it consists of cell bodies and capillaries. <strong>The cerebral cortex plays a key role in memory, attention, perceptual awareness, thought, language, and consciousness</strong>.",
			   "media":[
                  {
                     "type":"custom",
                     "caption":"",
                     "folder":"simulation",
                     "height":"800",
                     "width":"1280"
                  }
               ]
            },
			{
               "template":"401",
               "title":"Important Structures",
               "text":"<p>Every structure of the brain serves a vital purpose. </p>",
               "interaction-prompt":"",
               "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s2.jpg"]
                  }
               ],
               "interaction":[
                  {
                     "label":"Corpus Callosum",
                     "text":"Wide, flat bundle of neural fibers beneath the cortex in the brain at the longitudinal fissure. It connects the left and right cerebral hemispheres and <strong>facilitates interhemispheric communication</strong>. It is the largest white matter structure in the brain.",
                     "x-pos":"25",
                     "y-pos":"8"
                  },
                  {
                     "label":"Cerebral Cortex",
                     "text":"Outermost layered structure of neural tissue of the cerebrum. Often referred to as grey matter, it consists of cell bodies and capillaries. <strong>The cerebral cortex plays a key role in memory, attention, perceptual awareness, thought, language, and consciousness</strong>.",
                     "x-pos":"77",
                     "y-pos":"8"
                  },
                  {
                     "label":"Thalamus",
                     "text":"Midline symmetrical structure of two halves, situated between the cerebral cortex and midbrain. <strong>Relays sensory and motor signals to the cerebral cortex and regulates consciousness, sleep and alertness</strong>.",
                     "x-pos":"80",
                     "y-pos":"28"
                  },
                  {
                     "label":"Cerebellum",
                     "text":"Located at the bottom of the brain, with the large mass of the cerebral cortex above it and the portion of the brainstem called the pons in front of it. <strong>The region of the brain that plays an important role in motor control</strong>.",
                     "x-pos":"79",
                     "y-pos":"48"
                  },
                  {
                     "label":"Brainstem",
                     "text":"Posterior part of the brain, adjoining and structurally continuous with the spinal cord. <strong>It provides the main motor and sensory innervation to the face and neck via the cranial nerves</strong>.",
                     "x-pos":"68",
                     "y-pos":"69"
                  },
                  {
                     "label":"Hypothalamus",
                     "text":"Portion of the brain containing a number of small nuclei with a variety of functions; one of the most important of which is to link the nervous system to the endocrine system via the pituitary gland. <strong>Responsible for certain metabolic processes and other activities of the autonomic nervous system</strong>.",
                     "x-pos":"38",
                     "y-pos":"60"
                  }
               ]
            },
			{
               "template":"303",
               "title":"Brain Cells",
               "text":"<p>Brains of all species are comprised primarily of two broad classes of cells: neurons and glial cells. </p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Glial Cells",
                     "text":"Glial cells (also known as glia or neuroglia) come in several types, and perform a number of critical functions, including structural support, metabolic support, insulation, and guidance of development.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3a.jpg"]
                        }
                     ]

                  },
                  {
                     "label":"Neurons",
                     "text":"Neurons, however, are usually considered the most important cells in the brain. The property that makes neurons unique is their ability to send signals to specific target cells over long distances. They send these signals by means of an axon, which is a thin protoplasmic fiber that extends from the cell body and projects, usually with numerous branches, to other areas, sometimes nearby, sometimes in distant parts of the brain or body.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3b.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Neurotransmitters",
                     "text":"Axons transmit signals to other neurons by means of specialized junctions called synapses. A single axon may make as many as several thousand synaptic connections with other cells. When an action potential, traveling along an axon, arrives at a synapse, it causes a chemical called a neurotransmitter to be released. The neurotransmitter binds to receptor molecules in the membrane of the target cell.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3c.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Synapses",
                     "text":"<p>Synapses are the key functional elements of the brain. The essential function of the brain is cell-to-cell communication, and synapses are the points at which communication occurs. The human brain has been estimated to contain approximately 100 trillion synapses.</p><p>The functions of these synapses are very diverse: some or excitatory, others are inhibitory; others work by activating second messenger systems that change the internal chemistry of their target cells in complex ways.</p><p><em>Play the video to see an animation.</em></p>",
                     "media":[
                        {
                            "type":"video",
                     		"caption":"",
                     		"image":["t1s3d.jpg"],
                     		"video":"t1s3d.mp4"
                        }
                     ]
                  }
               ]
            },
			{
               "template":"206",
               "title":"Disengaged Conditions Overview",
               "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus. </p>",
               "media":[
                  {
                     "type":"slideshow",
                     "caption":"",
                     "image":["t1s4b.jpg","t1s4a.jpg"],
                     "popup":false
                  },
                  {
                     "type":"pullquote",
                     "caption":"Help your customers imagine the possibilities with EngageMe™"
                  }
               ]
            },
			{
               "template":"307",
               "title":"Case Studies",
               "text":"<p>What is it like to live with disengage syndrome? Explore these true stories to find out. You’ll meet Nora, Liang, John and Soma – discover how their conditions affected their lives, and how BIW therapies helped them engage again.</p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Nora, Philadelphia",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Nora’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5a.jpg"],
						   "video":"t1s5a.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Liang, Beijing",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Liang’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5b.jpg"],
						   "video":"t1s5b.mp4"
                        }
                     ]
                  },
                  {
                     "label":"John, London",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play John’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5c.jpg"],
						   "video":"t1s5c.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Soma, Chennai",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Soma’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5d.jpg"],
						   "video":"t1s5d.mp4"
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "title":"Disengaged Conditions",
         "image":"topic-3.jpg",
         "description":"Despite being protected by the thick bones of the skull and cerebrospinal fluid, the human brain is susceptible to damage and disease. Learn how Disengaged Conditions affect brain function in this topic.",
         "page":[
            {
               "template":"201",
               "title":"Cerebral Hemispheres",
               "text":"<p>The cerebral hemispheres (the cerebrum) form the largest part of the human brain and are situated above other brain structures. They are covered with a cortical layer (the cerebral cortex) which has a convoluted topography.</p><p>Underneath the cerebrum lies the brainstem, resembling a stalk on which the cerebrum is attached. At the rear of the brain, beneath the cerebrum and behind the brainstem, is the cerebellum, a structure with a horizontally furrowed surface, the cerebellar cortex that makes it look different from any other brain area.</p>",
			   "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s1.jpg"]
                  }
               ]
            },
			{
               "template":"401",
               "title":"Important Structures",
               "text":"<p>Every structure of the brain serves a vital purpose. </p>",
               "interaction-prompt":"",
               "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s2.jpg"]
                  }
               ],
               "interaction":[
                  {
                     "label":"Corpus Callosum",
                     "text":"Wide, flat bundle of neural fibers beneath the cortex in the brain at the longitudinal fissure. It connects the left and right cerebral hemispheres and <strong>facilitates interhemispheric communication</strong>. It is the largest white matter structure in the brain.",
                     "x-pos":"25",
                     "y-pos":"8"
                  },
                  {
                     "label":"Cerebral Cortex",
                     "text":"Outermost layered structure of neural tissue of the cerebrum. Often referred to as grey matter, it consists of cell bodies and capillaries. <strong>The cerebral cortex plays a key role in memory, attention, perceptual awareness, thought, language, and consciousness</strong>.",
                     "x-pos":"77",
                     "y-pos":"8"
                  },
                  {
                     "label":"Thalamus",
                     "text":"Midline symmetrical structure of two halves, situated between the cerebral cortex and midbrain. <strong>Relays sensory and motor signals to the cerebral cortex and regulates consciousness, sleep and alertness</strong>.",
                     "x-pos":"80",
                     "y-pos":"28"
                  },
                  {
                     "label":"Cerebellum",
                     "text":"Located at the bottom of the brain, with the large mass of the cerebral cortex above it and the portion of the brainstem called the pons in front of it. <strong>The region of the brain that plays an important role in motor control</strong>.",
                     "x-pos":"79",
                     "y-pos":"48"
                  },
                  {
                     "label":"Brainstem",
                     "text":"Posterior part of the brain, adjoining and structurally continuous with the spinal cord. <strong>It provides the main motor and sensory innervation to the face and neck via the cranial nerves</strong>.",
                     "x-pos":"68",
                     "y-pos":"69"
                  },
                  {
                     "label":"Hypothalamus",
                     "text":"Portion of the brain containing a number of small nuclei with a variety of functions; one of the most important of which is to link the nervous system to the endocrine system via the pituitary gland. <strong>Responsible for certain metabolic processes and other activities of the autonomic nervous system</strong>.",
                     "x-pos":"38",
                     "y-pos":"60"
                  }
               ]
            },
			{
               "template":"303",
               "title":"Brain Cells",
               "text":"<p>Brains of all species are comprised primarily of two broad classes of cells: neurons and glial cells. </p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Glial Cells",
                     "text":"Glial cells (also known as glia or neuroglia) come in several types, and perform a number of critical functions, including structural support, metabolic support, insulation, and guidance of development.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3a.jpg"]
                        }
                     ]

                  },
                  {
                     "label":"Neurons",
                     "text":"Neurons, however, are usually considered the most important cells in the brain. The property that makes neurons unique is their ability to send signals to specific target cells over long distances. They send these signals by means of an axon, which is a thin protoplasmic fiber that extends from the cell body and projects, usually with numerous branches, to other areas, sometimes nearby, sometimes in distant parts of the brain or body.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3b.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Neurotransmitters",
                     "text":"Axons transmit signals to other neurons by means of specialized junctions called synapses. A single axon may make as many as several thousand synaptic connections with other cells. When an action potential, traveling along an axon, arrives at a synapse, it causes a chemical called a neurotransmitter to be released. The neurotransmitter binds to receptor molecules in the membrane of the target cell.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3c.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Synapses",
                     "text":"<p>Synapses are the key functional elements of the brain. The essential function of the brain is cell-to-cell communication, and synapses are the points at which communication occurs. The human brain has been estimated to contain approximately 100 trillion synapses.</p><p>The functions of these synapses are very diverse: some or excitatory, others are inhibitory; others work by activating second messenger systems that change the internal chemistry of their target cells in complex ways.</p><p><em>Play the video to see an animation.</em></p>",
                     "media":[
                        {
                            "type":"video",
                     		"caption":"",
                     		"image":["t1s3d.jpg"],
                     		"video":"t1s3d.mp4"
                        }
                     ]
                  }
               ]
            },
			{
               "template":"206",
               "title":"Disengaged Conditions Overview",
               "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus. </p>",
               "media":[
                  {
                     "type":"slideshow",
                     "caption":"",
                     "image":["t1s4b.jpg","t1s4a.jpg"],
                     "popup":false
                  },
                  {
                     "type":"pullquote",
                     "caption":"Help your customers imagine the possibilities with EngageMe™"
                  }
               ]
            },
			{
               "template":"307",
               "title":"Case Studies",
               "text":"<p>What is it like to live with disengage syndrome? Explore these true stories to find out. You’ll meet Nora, Liang, John and Soma – discover how their conditions affected their lives, and how BIW therapies helped them engage again.</p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Nora, Philadelphia",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Nora’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5a.jpg"],
						   "video":"t1s5a.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Liang, Beijing",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Liang’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5b.jpg"],
						   "video":"t1s5b.mp4"
                        }
                     ]
                  },
                  {
                     "label":"John, London",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play John’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5c.jpg"],
						   "video":"t1s5c.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Soma, Chennai",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Soma’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5d.jpg"],
						   "video":"t1s5d.mp4"
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "title":"EngageMe™ Therapies",
         "image":"topic-4.jpg",
         "description":"Disengaged brains... Learn how BIW can help alleviate the symptoms and bring engagement back to full function.",
         "page":[
            {
               "template":"201",
               "title":"Cerebral Hemispheres",
               "text":"<p>The cerebral hemispheres (the cerebrum) form the largest part of the human brain and are situated above other brain structures. They are covered with a cortical layer (the cerebral cortex) which has a convoluted topography.</p><p>Underneath the cerebrum lies the brainstem, resembling a stalk on which the cerebrum is attached. At the rear of the brain, beneath the cerebrum and behind the brainstem, is the cerebellum, a structure with a horizontally furrowed surface, the cerebellar cortex that makes it look different from any other brain area.</p>",
			   "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s1.jpg"]
                  }
               ]
            },
			{
               "template":"401",
               "title":"Important Structures",
               "text":"<p>Every structure of the brain serves a vital purpose. </p>",
               "interaction-prompt":"",
               "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s2.jpg"]
                  }
               ],
               "interaction":[
                  {
                     "label":"Corpus Callosum",
                     "text":"Wide, flat bundle of neural fibers beneath the cortex in the brain at the longitudinal fissure. It connects the left and right cerebral hemispheres and <strong>facilitates interhemispheric communication</strong>. It is the largest white matter structure in the brain.",
                     "x-pos":"25",
                     "y-pos":"8"
                  },
                  {
                     "label":"Cerebral Cortex",
                     "text":"Outermost layered structure of neural tissue of the cerebrum. Often referred to as grey matter, it consists of cell bodies and capillaries. <strong>The cerebral cortex plays a key role in memory, attention, perceptual awareness, thought, language, and consciousness</strong>.",
                     "x-pos":"77",
                     "y-pos":"8"
                  },
                  {
                     "label":"Thalamus",
                     "text":"Midline symmetrical structure of two halves, situated between the cerebral cortex and midbrain. <strong>Relays sensory and motor signals to the cerebral cortex and regulates consciousness, sleep and alertness</strong>.",
                     "x-pos":"80",
                     "y-pos":"28"
                  },
                  {
                     "label":"Cerebellum",
                     "text":"Located at the bottom of the brain, with the large mass of the cerebral cortex above it and the portion of the brainstem called the pons in front of it. <strong>The region of the brain that plays an important role in motor control</strong>.",
                     "x-pos":"79",
                     "y-pos":"48"
                  },
                  {
                     "label":"Brainstem",
                     "text":"Posterior part of the brain, adjoining and structurally continuous with the spinal cord. <strong>It provides the main motor and sensory innervation to the face and neck via the cranial nerves</strong>.",
                     "x-pos":"68",
                     "y-pos":"69"
                  },
                  {
                     "label":"Hypothalamus",
                     "text":"Portion of the brain containing a number of small nuclei with a variety of functions; one of the most important of which is to link the nervous system to the endocrine system via the pituitary gland. <strong>Responsible for certain metabolic processes and other activities of the autonomic nervous system</strong>.",
                     "x-pos":"38",
                     "y-pos":"60"
                  }
               ]
            },
			{
               "template":"303",
               "title":"Brain Cells",
               "text":"<p>Brains of all species are comprised primarily of two broad classes of cells: neurons and glial cells. </p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Glial Cells",
                     "text":"Glial cells (also known as glia or neuroglia) come in several types, and perform a number of critical functions, including structural support, metabolic support, insulation, and guidance of development.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3a.jpg"]
                        }
                     ]

                  },
                  {
                     "label":"Neurons",
                     "text":"Neurons, however, are usually considered the most important cells in the brain. The property that makes neurons unique is their ability to send signals to specific target cells over long distances. They send these signals by means of an axon, which is a thin protoplasmic fiber that extends from the cell body and projects, usually with numerous branches, to other areas, sometimes nearby, sometimes in distant parts of the brain or body.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3b.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Neurotransmitters",
                     "text":"Axons transmit signals to other neurons by means of specialized junctions called synapses. A single axon may make as many as several thousand synaptic connections with other cells. When an action potential, traveling along an axon, arrives at a synapse, it causes a chemical called a neurotransmitter to be released. The neurotransmitter binds to receptor molecules in the membrane of the target cell.",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t1s3c.jpg"]
                        }
                     ]
                  },
                  {
                     "label":"Synapses",
                     "text":"<p>Synapses are the key functional elements of the brain. The essential function of the brain is cell-to-cell communication, and synapses are the points at which communication occurs. The human brain has been estimated to contain approximately 100 trillion synapses.</p><p>The functions of these synapses are very diverse: some or excitatory, others are inhibitory; others work by activating second messenger systems that change the internal chemistry of their target cells in complex ways.</p><p><em>Play the video to see an animation.</em></p>",
                     "media":[
                        {
                            "type":"video",
                     		"caption":"",
                     		"image":["t1s3d.jpg"],
                     		"video":"t1s3d.mp4"
                        }
                     ]
                  }
               ]
            },
			{
               "template":"206",
               "title":"Disengaged Conditions Overview",
               "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus. </p>",
               "media":[
                  {
                     "type":"slideshow",
                     "caption":"",
                     "image":["t1s4b.jpg","t1s4a.jpg"],
                     "popup":false
                  },
                  {
                     "type":"pullquote",
                     "caption":"Help your customers imagine the possibilities with EngageMe™"
                  }
               ]
            },
			{
               "template":"307",
               "title":"Case Studies",
               "text":"<p>What is it like to live with disengage syndrome? Explore these true stories to find out. You’ll meet Nora, Liang, John and Soma – discover how their conditions affected their lives, and how BIW therapies helped them engage again.</p>",
               "interaction-prompt":"",
               "interaction":[
                  {
                     "label":"Nora, Philadelphia",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Nora’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5a.jpg"],
						   "video":"t1s5a.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Liang, Beijing",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Liang’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5b.jpg"],
						   "video":"t1s5b.mp4"
                        }
                     ]
                  },
                  {
                     "label":"John, London",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play John’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5c.jpg"],
						   "video":"t1s5c.mp4"
                        }
                     ]
                  },
                  {
                     "label":"Soma, Chennai",
                     "text":"<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu. Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus. Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.</p><p><em>Play Soma’s video to learn more.</em></p>",
                     "media":[
                        {
                           "type":"video",
                           "caption":"",
                           "image":["t1s5d.jpg"],
						   "video":"t1s5d.mp4"
                        }
                     ]
                  }
               ]
            }
         ]
      },
      {
         "title":"Case Studies",
         "image":"topic-5.jpg",
         "description":"Explore the lives of people from around the globe who have been affected by Disengaged Conditions.",
         "page":[
            {
               "template":"201",
               "title":"Cerebral Hemispheres",
               "text":"<p>The cerebral hemispheres (the cerebrum) form the largest part of the human brain and are situated above other brain structures. They are covered with a cortical layer (the cerebral cortex) which has a convoluted topography.</p><p>Underneath the cerebrum lies the brainstem, resembling a stalk on which the cerebrum is attached. At the rear of the brain, beneath the cerebrum and behind the brainstem, is the cerebellum, a structure with a horizontally furrowed surface, the cerebellar cortex that makes it look different from any other brain area.</p>",
			   "media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t1s1.jpg"]
                  }
               ]
            }
         ]
      },
      {
         "title":"Assessment",
         "image":"topic-6.jpg",
         "description":"After completing the learning topics, you must score 90% or better on the final assessment to pass the module.",
         "page":[
            {
               "template":"quiz",
               "title":"Test Your Knowledge",
			   "text":"<p>Welcome to the final assessment of the Anatomy of the Brain module. In order to obtain certification for BIW therapies, you must pass the assessment in each of the modules.</p><p>Assessment stats:<ul><li>There are 20 questions</li><li>You have one try to answer each question</li><li>You must score 90% to pass the assessment</li></ul></p>",
 				"media":[
                  {
                     "type":"image",
                     "caption":"",
                     "image":["t6s1.jpg"],
                     "alt":"Alt tag"
                  }
               ],
               "attempts":"2",
               "summary":[
                {
                  "title":"Thank you for taking this short quiz",
                  "text":"Congratulations! You've successfully completed this module with a passing score of at least 80%.  You have demonstrated that you understand the key 3D TRASAR™ CIP concepts in delivering value through prioritized actions."
                },
                {
                  "title":"Thank you for taking this short quiz",
                  "text":"You have completed this module with a score below 80%. Please review any or all sections or topics to ensure that you understand the key 3D TRASAR™ CIP concepts in delivering value through prioritized actions."
                }
               ],
               "question":[
                  {
                     "template":"601",
                     "prompt":"The region of the cerebral cortex affected by disengaged syndrome is:</br><em>Select the best choice.</em> ",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t6s1a.jpg"],
                           "alt":"Alt tag"
                        }
                     ],
                     "distractor":[
                        {
                           "text":"Primary motor cortex",
                           "value":false,
                           "feedback":"Sorry that is incorrect."
                        },
                        {
                           "text":"Primary sensory cortex",
                           "value":true
                        },
                        {
                           "text":"Somatic motor association area",
                           "value":false,
                           "feedback":"Sorry that is incorrect."
                        },
                        {
                           "text":"Visual appreciation area",
                           "value":false,
                           "feedback":"Sorry that is incorrect."
                        },
                        {
                           "text":"Visual cortex",
                           "value":false,
                           "feedback":"Sorry that is incorrect."
                        }
                     ],
                     "incorrect":"Sorry that is incorrect.",
                     "correct":"That is correct."
                  },
				  {
                     "template":"603",
                     "prompt":"You’ve just explained the advantages and benefits of BIW therapies. Your customer objects to both price and length of treatment necessary. How do you respond?</br><em>Select the best choice.</em> ",
                     "media":[
                        {
                           "type":"image",
                           "caption":"",
                           "image":["t6s1b.jpg"],
                           "alt":"Alt tag"
                        }
                     ],
                     "distractor":[
                        {
                           "text":"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam non posuere arcu.",
                           "value":false,
                           "feedback":"Sorry, I just don’t have the time right now.",
                           "media":[
                              {
                                 "type":"image",
                                 "caption":"",
                                 "image":["t6s1b1.jpg"],
                                 "alt":"Alt tag"
                              }
                           ]
                        },
                        {
                           "text":"Nullam eu aliquet nisi. Praesent vel arcu auctor, mollis eros vitae, convallis risus. Mauris scelerisque convallis rhoncus.",
                           "value":false,
						   "feedback":"I’d just like the facts, please.",
                           "media":[
                              {
                                 "type":"image",
                                 "caption":"",
                                 "image":["t6s1b2.jpg"],
                                 "alt":"Alt tag"
                              }
                           ]
                        },
                        {
                           "text":"Aenean leo nisi, tristique eget ante sed, varius mattis arcu. Nunc malesuada eu mi sed dapibus.",
                           "value":true,
                           "feedback":"That sounds great, tell me more!",
                           "media":[
                              {
                                 "type":"image",
                                 "caption":"",
                                 "image":["t6s1b3.jpg"],
                                 "alt":"Alt tag"
                              }
                           ]
                        }
                     ],
                     "incorrect":"Sorry, that is not the best response.",
                     "correct":"That’s correct."
                  },
				  {
                     "template":"605",
                     "prompt":"Identify the Thalamus on the image below. <em>Click the correct location on the image.</em>",
                     "media":[
                        {
                           "type":"image-map",
                           "caption":"",
                           "image":["t6s1c.jpg"],
                           "alt":""
                        }
                     ],
                     "distractor":[
                        {
                           "value":true,
                           "feedback":"Correct.",
                           "shape":"polygon",
                           "alt":"",
                           "coords": "569,246,595,246,611,243,621,238,627,230,631,222,630,212,625,205,617,199,608,197,597,194,587,193,574,195,563,199,553,205,547,216,547,229,551,243"
                        },
                        {
                           "value":false,
                           "feedback":"Sorry, that is incorrect.",
                           "shape":"rect",
                           "alt":"",
                           "coords": "0,0,1050,699"
                        }
                     ],
                     "incorrect":"Sorry, that is incorrect.",
                     "correct":"Correct."
                  }
               ]
            }
         ]
      }
   ],
   "glossary":[
      {
         "term":"Longitudinal fissure",
         "definition":"Deep groove that separates the two hemispheres of the brain."
      },
      {
         "term":"Endocrine system",
         "definition":"Collection of glands of an organism that secretes hormones directly into the circulatory system to be carried toward a distant target organ."
      },
      {
         "term":"Pituitary gland",
         "definition":"Endocrine gland, located off the bottom of the hypothalamus, which synthesizes and secretes important endocrine hormones."
      },
	  {
         "term":"Axon",
         "definition":"Nerve fibre; long slender projection of a nerve cell, or neuron, that typically conducts electrical impulses away from the neuron’s cell body."
      },
	  {
         "term":"Synapses",
         "definition":"Structures that permit neurons to pass an electrical or chemical signal to another cell."
      }

   ],
   "resources":[
   ]
}