/***********************
* Adobe Edge Animate Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge Animate
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      

      

      

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight01}", "click", function(sym, e) {
         sym.play("start_02");
         // Hide an Element.
         sym.$("hilight01").hide();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight02}", "click", function(sym, e) {
         sym.play("start_03");
         // Hide an Element.
         sym.$("hilight02").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight03}", "click", function(sym, e) {
         sym.play("start_04");
         // Hide an Element.
         sym.$("hilight03").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight04}", "click", function(sym, e) {
         sym.play("start_05");
         // Hide an Element.
         sym.$("hilight04").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight05}", "click", function(sym, e) {
         sym.play("start_06");
         // Hide an Element.
         sym.$("hilight05").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight06}", "click", function(sym, e) {
         sym.play("start_07");
         // Hide an Element.
         sym.$("hilight06").hide();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 8000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 7000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 4000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2250, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2000, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1750, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight07}", "click", function(sym, e) {
         sym.play("start_08");
         // Hide an Element.
         sym.$("hilight07").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight08}", "click", function(sym, e) {
         sym.play("start_09");
         // Hide an Element.
         sym.$("hilight08").hide();
         // Hide an Element.

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight09}", "click", function(sym, e) {
         sym.play("start_10");
         // Hide an Element.
         sym.$("hilight09").hide();
         // Hide an Element.

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight10}", "click", function(sym, e) {
         sym.play("start_11");
         // Hide an Element.
         sym.$("hilight10").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight11}", "click", function(sym, e) {
         sym.play("start_12");
         // Hide an Element.
         sym.$("hilight11").hide();
         // Hide an Element.

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight12}", "click", function(sym, e) {
         sym.play("start_13");
         // Hide an Element.
         sym.$("hilight12").hide();
         // Hide an Element.

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight13}", "click", function(sym, e) {
         sym.play("start_14");
         // Hide an Element.
         sym.$("hilight13").hide();
         // Hide an Element.

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight14}", "click", function(sym, e) {
         sym.play("start_15");
         // Hide an Element.
         sym.$("hilight14").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight15}", "click", function(sym, e) {
         sym.play("start_16");
         // Hide an Element.
         sym.$("hilight15").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight16}", "click", function(sym, e) {
         sym.play("start_17");
         // Hide an Element.
         sym.$("hilight16").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight17}", "click", function(sym, e) {
         sym.play("start_18");
         // Hide an Element.
         sym.$("hilight17").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight18}", "click", function(sym, e) {
         sym.play("start_19");
         // Hide an Element.
         sym.$("hilight18").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight19}", "click", function(sym, e) {
         sym.play("start_20");
         // Hide an Element.
         sym.$("hilight19").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight20}", "click", function(sym, e) {
         sym.play("start_21");
         // Hide an Element.
         sym.$("hilight20").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight21}", "click", function(sym, e) {
         sym.play("start_22");
         // Hide an Element.
         sym.$("hilight21").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight22}", "click", function(sym, e) {
         sym.play("start_23");
         // Hide an Element.
         sym.$("hilight22").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight23}", "click", function(sym, e) {
         sym.play("start_24");
         // Hide an Element.
         sym.$("hilight23").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight24}", "click", function(sym, e) {
         sym.play("start_25");
         // Hide an Element.
         sym.$("hilight24").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight25}", "click", function(sym, e) {
         sym.play("start_26");
         // Hide an Element.
         sym.$("hilight25").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight26}", "click", function(sym, e) {
         sym.play("start_27");
         // Hide an Element.
         sym.$("hilight26").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight27}", "click", function(sym, e) {
         sym.play("start_28");
         // Hide an Element.
         sym.$("hilight27").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight28}", "click", function(sym, e) {
         sym.play("start_29");
         // Hide an Element.
         sym.$("hilight28").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight29}", "click", function(sym, e) {
         sym.play("start_30");
         // Hide an Element.
         sym.$("hilight29").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight30}", "click", function(sym, e) {
         sym.play("start_31");
         // Hide an Element.
         sym.$("hilight30").hide();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_hilight31}", "click", function(sym, e) {
         sym.play("start_32");
         // Hide an Element.
         sym.$("hilight31").hide();

      });
      //Edge binding end

      

   })("stage");
   //Edge symbol end:'stage'

   //=========================================================
   
   //Edge symbol: 'hilight'
   (function(symbolName) {   
   
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 500, function(sym, e) {
         sym.play("h_start");

      });
      //Edge binding end

   })("hilight");
   //Edge symbol end:'hilight'

   //=========================================================
   
   //Edge symbol: 'screen_01'
   (function(symbolName) {   
   
   })("screen_01");
   //Edge symbol end:'screen_01'

   //=========================================================
   
   //Edge symbol: 'screen_02'
   (function(symbolName) {   
   
   })("screen_02");
   //Edge symbol end:'screen_02'

   //=========================================================
   
   //Edge symbol: 'screen_03'
   (function(symbolName) {   
   
   })("screen_03");
   //Edge symbol end:'screen_03'

   //=========================================================
   
   //Edge symbol: 'screen_04'
   (function(symbolName) {   
   
   })("screen_04");
   //Edge symbol end:'screen_04'

   //=========================================================
   
   //Edge symbol: 'screen_05'
   (function(symbolName) {   
   
   })("screen_05");
   //Edge symbol end:'screen_05'

   //=========================================================
   
   //Edge symbol: 'screen_06'
   (function(symbolName) {   
   
   })("screen_06");
   //Edge symbol end:'screen_06'

})(jQuery, AdobeEdge, "EDGE-416691931");