/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};
var opts = {
    'preloadAudio': false
};
var resources = [
];
var symbols = {
"stage": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "both",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
            {
                id: 'screen_01',
                type: 'rect',
                rect: ['0', '0','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight01',
                type: 'rect',
                rect: ['116px', '305px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_02',
                display: 'none',
                type: 'rect',
                rect: ['0', '0','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight02',
                display: 'none',
                type: 'rect',
                rect: ['115px', '303','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_03',
                display: 'none',
                type: 'rect',
                rect: ['0', '0','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight03',
                display: 'none',
                type: 'rect',
                rect: ['115px', '230px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_04',
                display: 'none',
                type: 'rect',
                rect: ['0px', '0px','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight04',
                display: 'none',
                type: 'rect',
                rect: ['58px', '197px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_05',
                display: 'block',
                type: 'rect',
                rect: ['0px', '0px','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight05',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_06',
                display: 'none',
                type: 'rect',
                rect: ['0px', '0px','auto','auto','auto', 'auto']
            },
            {
                id: 'hilight06',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_07',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_07.png",'0px','0px']
            },
            {
                id: 'hilight07',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_08',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_08.png",'0px','0px']
            },
            {
                id: 'hilight08',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_09',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_09.png",'0px','0px']
            },
            {
                id: 'hilight09',
                display: 'none',
                type: 'rect',
                rect: ['115px', '197px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_10',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_10.png",'0px','0px']
            },
            {
                id: 'hilight10',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_11',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_11.png",'0px','0px']
            },
            {
                id: 'hilight11',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_12',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_12.png",'0px','0px']
            },
            {
                id: 'hilight12',
                display: 'none',
                type: 'rect',
                rect: ['170px', '195px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_13',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_13.png",'0px','0px']
            },
            {
                id: 'hilight13',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_14',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_14.png",'0px','0px']
            },
            {
                id: 'hilight14',
                display: 'none',
                type: 'rect',
                rect: ['115px', '303px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_15',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_15.png",'0px','0px']
            },
            {
                id: 'hilight15',
                display: 'none',
                type: 'rect',
                rect: ['60px', '195px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_16',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_16.png",'0px','0px']
            },
            {
                id: 'hilight16',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_17',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_17.png",'0px','0px']
            },
            {
                id: 'hilight17',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_18',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_18.png",'0px','0px']
            },
            {
                id: 'hilight18',
                display: 'none',
                type: 'rect',
                rect: ['117px', '197px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_19',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_19.png",'0px','0px']
            },
            {
                id: 'hilight19',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_20',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_20.png",'0px','0px']
            },
            {
                id: 'hilight20',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_21',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_21.png",'0px','0px']
            },
            {
                id: 'hilight21',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_22',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_22.png",'0px','0px']
            },
            {
                id: 'hilight22',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_23',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_23.png",'0px','0px']
            },
            {
                id: 'hilight23',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_24',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_24.png",'0px','0px']
            },
            {
                id: 'hilight24',
                display: 'none',
                type: 'rect',
                rect: ['172px', '197px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_25',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_25.png",'0px','0px']
            },
            {
                id: 'hilight25',
                display: 'none',
                type: 'rect',
                rect: ['118px', '388px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_26',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_26.png",'0px','0px']
            },
            {
                id: 'hilight26',
                display: 'none',
                type: 'rect',
                rect: ['116px', '302px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_27',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_27.png",'0px','0px']
            },
            {
                id: 'hilight27',
                display: 'none',
                type: 'rect',
                rect: ['116px', '304px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_28',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_28.png",'0px','0px']
            },
            {
                id: 'hilight28',
                display: 'none',
                type: 'rect',
                rect: ['316px', '358px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_29',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_29.png",'0px','0px']
            },
            {
                id: 'hilight29',
                display: 'none',
                type: 'rect',
                rect: ['114px', '304px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_30',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_30.png",'0px','0px']
            },
            {
                id: 'hilight30',
                display: 'none',
                type: 'rect',
                rect: ['58px', '184px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_31',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_31.png",'0px','0px']
            },
            {
                id: 'hilight31',
                display: 'none',
                type: 'rect',
                rect: ['117px', '304px','auto','auto','auto', 'auto'],
                opacity: 1
            },
            {
                id: 'screen_32',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_32.png",'0px','0px']
            },
            {
                id: 'screen_33',
                display: 'none',
                type: 'image',
                rect: ['0px', '0px','407px','513px','auto', 'auto'],
                fill: ["rgba(0,0,0,0)",im+"screen_33.png",'0px','0px']
            },
            {
                id: 'RoundRect',
                type: 'rect',
                rect: ['264px', '15px','174px','141px','auto', 'auto'],
                borderRadius: ["10px", "10px", "10px", "10px"],
                fill: ["rgba(89,150,229,1.00)"],
                stroke: [2,"rgba(107,137,226,1.00)","solid"],
                filter: [0, 0, 1, 1, 0, 0, 0, 0, "rgba(0,0,0,0)", 0, 0, 0]
            },
            {
                id: 'Step_heading',
                type: 'text',
                rect: ['276px', '29px','auto','auto','auto', 'auto'],
                text: "Step Text:",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 15, "rgba(255,255,255,1.00)", "700", "none", "normal"]
            },
            {
                id: 'Text1',
                display: 'block',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select English as your desired language.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text2',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select OK.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text3',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select the Trial button.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text4',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Enter 5 Apr 2012 to set the date.<br><br>Tap 1 to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text5',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button until you reach 5.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text6',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Tap Jan to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text7',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button until you reach Apr.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text8',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Tap 2011 to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text9',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button until you reach 2012.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text10',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select OK.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text11',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Enter 2:05 pm to set the time. <br><br>Tap 12 to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text12',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button until you reach 2.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text13',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Tap 00 to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text14',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button until you reach 05.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text15',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Tap am to highlight field.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text16',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Press the Increase button to change to pm.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text17',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select OK.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text18',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "1,111.1 is selected by default. Use this as your desired number format.<br><br>Select OK.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text19',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "On the ENS: <br>Press the ENS button to initiate discovery mode (the green light will blink).",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text20',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "On the Controller: Select Continue. <br>Note: If no ENS is found, you will receive a screen indicating \"No device found\".",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text21',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select the correct ENS serial number (located on the back of the ENS).",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text22',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Select OK. Note: The Controller is now paired with the ENS.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            },
            {
                id: 'Text23',
                display: 'none',
                type: 'text',
                rect: ['276px', '52px','158px','auto','auto', 'auto'],
                text: "Note: This screen appears because no cables are connected.",
                align: "left",
                font: ['Arial, Helvetica, sans-serif', 13, "rgba(255,255,255,1.00)", "400", "none", "normal"]
            }],
            symbolInstances: [
            {
                id: 'hilight01',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight25',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight15',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight20',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight30',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight21',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight17',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight22',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'screen_03',
                symbolName: 'screen_03',
                autoPlay: {

                }
            },
            {
                id: 'hilight12',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight14',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight13',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'screen_06',
                symbolName: 'screen_06',
                autoPlay: {

                }
            },
            {
                id: 'hilight09',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight03',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'screen_01',
                symbolName: 'screen_01',
                autoPlay: {

                }
            },
            {
                id: 'hilight26',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight28',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight10',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight02',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight08',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight16',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight23',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight11',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight31',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight06',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight29',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight07',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'screen_02',
                symbolName: 'screen_02',
                autoPlay: {

                }
            },
            {
                id: 'hilight05',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight24',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight18',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight27',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight04',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'hilight19',
                symbolName: 'hilight',
                autoPlay: {

                }
            },
            {
                id: 'screen_04',
                symbolName: 'screen_04',
                autoPlay: {

                }
            },
            {
                id: 'screen_05',
                symbolName: 'screen_05',
                autoPlay: {

                }
            }
            ]
        },
    states: {
        "Base State": {
            "${_hilight05}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_28}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight21}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_Text22}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight26}": [
                ["style", "top", '302px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '116px']
            ],
            "${_hilight11}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_26}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight09}": [
                ["style", "top", '197px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '115px']
            ],
            "${_screen_06}": [
                ["style", "top", '0px'],
                ["style", "left", '0px'],
                ["style", "display", 'none']
            ],
            "${_hilight16}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_Text21}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_hilight19}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_hilight12}": [
                ["style", "top", '195px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '170px']
            ],
            "${_screen_31}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_19}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text10}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight31}": [
                ["style", "top", '304px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '117px']
            ],
            "${_hilight28}": [
                ["style", "top", '358px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '316px']
            ],
            "${_hilight10}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_hilight27}": [
                ["style", "top", '304px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '116px']
            ],
            "${_screen_03}": [
                ["style", "display", 'none']
            ],
            "${_hilight13}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_04}": [
                ["style", "top", '0px'],
                ["style", "left", '0px'],
                ["style", "display", 'none']
            ],
            "${_Text3}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_screen_23}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight01}": [
                ["style", "-webkit-transform-origin", [50,50], {valueTemplate:'@@0@@% @@1@@%'} ],
                ["style", "-moz-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-ms-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "msTransformOrigin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "-o-transform-origin", [50,50],{valueTemplate:'@@0@@% @@1@@%'}],
                ["style", "overflow", 'visible'],
                ["style", "opacity", '1'],
                ["style", "left", '104px'],
                ["style", "top", '154px']
            ],
            "${_screen_17}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight25}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_Text13}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_Text6}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight30}": [
                ["style", "top", '184px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '58px']
            ],
            "${_screen_08}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Stage}": [
                ["color", "background-color", 'rgba(255,255,255,1.00)'],
                ["style", "overflow", 'visible'],
                ["style", "height", '513px'],
                ["style", "width", '450px']
            ],
            "${_Text15}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_hilight15}": [
                ["style", "top", '195px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '60px']
            ],
            "${_Text8}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight20}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_07}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text23}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["color", "color", 'rgba(255,255,255,1)'],
                ["style", "display", 'none'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_Text14}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_Text4}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight22}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_22}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_13}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight29}": [
                ["style", "top", '304px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '114px']
            ],
            "${_hilight04}": [
                ["style", "top", '197px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '58px']
            ],
            "${_hilight06}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_hilight08}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_10}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_25}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text20}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_screen_16}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight02}": [
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '115px'],
                ["style", "overflow", 'visible']
            ],
            "${_hilight23}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_Text9}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_screen_29}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text19}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_screen_20}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight14}": [
                ["style", "top", '303px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '115px']
            ],
            "${_Text7}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_Text11}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_screen_30}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_09}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text5}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_hilight17}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_screen_15}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text2}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_hilight24}": [
                ["style", "top", '197px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '172px']
            ],
            "${_screen_18}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_RoundRect}": [
                ["style", "border-style", 'solid'],
                ["style", "left", '264px'],
                ["style", "width", '174px'],
                ["style", "top", '15px'],
                ["style", "border-width", '2px'],
                ["color", "background-color", 'rgba(89,150,229,1.00)'],
                ["style", "height", '141px'],
                ["color", "border-color", 'rgba(107,137,226,1.00)']
            ],
            "${_screen_14}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_21}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_screen_02}": [
                ["style", "display", 'none']
            ],
            "${_screen_24}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text1}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'block'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_screen_33}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight07}": [
                ["style", "top", '388px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '118px']
            ],
            "${_hilight03}": [
                ["style", "top", '230px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '115px']
            ],
            "${_Text17}": [
                ["style", "top", '52px'],
                ["style", "font-size", '13px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "width", '158px']
            ],
            "${_Text16}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_Step_heading}": [
                ["style", "top", '29px'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-weight", '700'],
                ["style", "left", '276px'],
                ["style", "font-size", '15px']
            ],
            "${_screen_27}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text18}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_screen_11}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_Text12}": [
                ["style", "top", '52px'],
                ["style", "width", '158px'],
                ["style", "display", 'none'],
                ["color", "color", 'rgba(255,255,255,1.00)'],
                ["style", "font-family", 'Arial, Helvetica, sans-serif'],
                ["style", "left", '276px'],
                ["style", "font-size", '13px']
            ],
            "${_screen_05}": [
                ["style", "top", '0px'],
                ["style", "left", '0px'],
                ["style", "display", 'block']
            ],
            "${_screen_32}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${_hilight18}": [
                ["style", "top", '197px'],
                ["style", "overflow", 'visible'],
                ["style", "display", 'none'],
                ["style", "opacity", '1'],
                ["style", "left", '117px']
            ],
            "${_screen_12}": [
                ["style", "display", 'none'],
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 8000,
            autoPlay: true,
            labels: {
                "start_01": 0,
                "start_02": 250,
                "start_03": 500,
                "start_04": 750,
                "start_05": 1000,
                "start_06": 1250,
                "start_07": 1500,
                "start_08": 1750,
                "start_09": 2000,
                "start_10": 2250,
                "start_11": 2500,
                "start_12": 2750,
                "start_13": 3000,
                "start_14": 3250,
                "start_15": 3500,
                "start_16": 3750,
                "start_17": 4000,
                "start_18": 4250,
                "start_19": 4500,
                "start_20": 4750,
                "start_21": 5000,
                "start_22": 5250,
                "start_23": 5500,
                "start_24": 5750,
                "start_25": 6000,
                "start_26": 6250,
                "start_27": 6500,
                "start_28": 6750,
                "start_29": 7000,
                "start_30": 7250,
                "start_31": 7500,
                "start_32": 7750
            },
            timeline: [
                { id: "eid293", tween: [ "style", "${_hilight24}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid329", tween: [ "style", "${_hilight24}", "display", 'block', { fromValue: 'none'}], position: 5750, duration: 0 },
                { id: "eid147", tween: [ "style", "${_screen_05}", "display", 'none', { fromValue: 'block'}], position: 0, duration: 0 },
                { id: "eid82", tween: [ "style", "${_screen_05}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 },
                { id: "eid156", tween: [ "style", "${_hilight04}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid155", tween: [ "style", "${_hilight04}", "display", 'block', { fromValue: 'none'}], position: 750, duration: 0 },
                { id: "eid309", tween: [ "style", "${_hilight08}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid312", tween: [ "style", "${_hilight08}", "display", 'block', { fromValue: 'none'}], position: 1750, duration: 0 },
                { id: "eid302", tween: [ "style", "${_hilight15}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid320", tween: [ "style", "${_hilight15}", "display", 'block', { fromValue: 'none'}], position: 3500, duration: 0 },
                { id: "eid289", tween: [ "style", "${_hilight28}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid333", tween: [ "style", "${_hilight28}", "display", 'block', { fromValue: 'none'}], position: 6750, duration: 0 },
                { id: "eid197", tween: [ "style", "${_screen_29}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid241", tween: [ "style", "${_screen_29}", "display", 'block', { fromValue: 'none'}], position: 7000, duration: 0 },
                { id: "eid353", tween: [ "style", "${_Text10}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid380", tween: [ "style", "${_Text10}", "display", 'block', { fromValue: 'none'}], position: 3250, duration: 0 },
                { id: "eid381", tween: [ "style", "${_Text10}", "display", 'none', { fromValue: 'block'}], position: 3500, duration: 0 },
                { id: "eid359", tween: [ "style", "${_Text4}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid364", tween: [ "style", "${_Text4}", "display", 'block', { fromValue: 'none'}], position: 750, duration: 0 },
                { id: "eid374", tween: [ "style", "${_Text4}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
                { id: "eid207", tween: [ "style", "${_screen_19}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid231", tween: [ "style", "${_screen_19}", "display", 'block', { fromValue: 'none'}], position: 4500, duration: 0 },
                { id: "eid298", tween: [ "style", "${_hilight19}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid324", tween: [ "style", "${_hilight19}", "display", 'block', { fromValue: 'none'}], position: 4500, duration: 0 },
                { id: "eid301", tween: [ "style", "${_hilight16}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid321", tween: [ "style", "${_hilight16}", "display", 'block', { fromValue: 'none'}], position: 3750, duration: 0 },
                { id: "eid299", tween: [ "style", "${_hilight18}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid323", tween: [ "style", "${_hilight18}", "display", 'block', { fromValue: 'none'}], position: 4250, duration: 0 },
                { id: "eid208", tween: [ "style", "${_screen_18}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid230", tween: [ "style", "${_screen_18}", "display", 'block', { fromValue: 'none'}], position: 4250, duration: 0 },
                { id: "eid348", tween: [ "style", "${_Text15}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid390", tween: [ "style", "${_Text15}", "display", 'block', { fromValue: 'none'}], position: 5750, duration: 0 },
                { id: "eid391", tween: [ "style", "${_Text15}", "display", 'none', { fromValue: 'block'}], position: 6000, duration: 0 },
                { id: "eid206", tween: [ "style", "${_screen_20}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid232", tween: [ "style", "${_screen_20}", "display", 'block', { fromValue: 'none'}], position: 4750, duration: 0 },
                { id: "eid308", tween: [ "style", "${_hilight09}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid313", tween: [ "style", "${_hilight09}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
                { id: "eid292", tween: [ "style", "${_hilight25}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid330", tween: [ "style", "${_hilight25}", "display", 'block', { fromValue: 'none'}], position: 6000, duration: 0 },
                { id: "eid287", tween: [ "style", "${_hilight30}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid335", tween: [ "style", "${_hilight30}", "display", 'block', { fromValue: 'none'}], position: 7250, duration: 0 },
                { id: "eid339", tween: [ "style", "${_Text2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid362", tween: [ "style", "${_Text2}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
                { id: "eid372", tween: [ "style", "${_Text2}", "display", 'none', { fromValue: 'block'}], position: 500, duration: 0 },
                { id: "eid151", tween: [ "style", "${_hilight03}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid152", tween: [ "style", "${_hilight03}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
                { id: "eid80", tween: [ "style", "${_screen_04}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid79", tween: [ "style", "${_screen_04}", "display", 'block', { fromValue: 'none'}], position: 750, duration: 0 },
                { id: "eid355", tween: [ "style", "${_Text8}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid368", tween: [ "style", "${_Text8}", "display", 'block', { fromValue: 'none'}], position: 2750, duration: 0 },
                { id: "eid378", tween: [ "style", "${_Text8}", "display", 'none', { fromValue: 'block'}], position: 3000, duration: 0 },
                { id: "eid354", tween: [ "style", "${_Text9}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid369", tween: [ "style", "${_Text9}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0 },
                { id: "eid379", tween: [ "style", "${_Text9}", "display", 'none', { fromValue: 'block'}], position: 3250, duration: 0 },
                { id: "eid288", tween: [ "style", "${_hilight29}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid334", tween: [ "style", "${_hilight29}", "display", 'block', { fromValue: 'none'}], position: 7000, duration: 0 },
                { id: "eid341", tween: [ "style", "${_Text22}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid404", tween: [ "style", "${_Text22}", "display", 'block', { fromValue: 'none'}], position: 7500, duration: 0 },
                { id: "eid405", tween: [ "style", "${_Text22}", "display", 'none', { fromValue: 'block'}], position: 7750, duration: 0 },
                { id: "eid212", tween: [ "style", "${_screen_14}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid226", tween: [ "style", "${_screen_14}", "display", 'block', { fromValue: 'none'}], position: 3250, duration: 0 },
                { id: "eid356", tween: [ "style", "${_Text7}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid367", tween: [ "style", "${_Text7}", "display", 'block', { fromValue: 'none'}], position: 2250, duration: 0 },
                { id: "eid377", tween: [ "style", "${_Text7}", "display", 'none', { fromValue: 'block'}], position: 2750, duration: 0 },
                { id: "eid78", tween: [ "style", "${_screen_03}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid77", tween: [ "style", "${_screen_03}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
                { id: "eid217", tween: [ "style", "${_screen_09}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid221", tween: [ "style", "${_screen_09}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
                { id: "eid48", tween: [ "style", "${_screen_02}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid49", tween: [ "style", "${_screen_02}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
                { id: "eid352", tween: [ "style", "${_Text11}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid382", tween: [ "style", "${_Text11}", "display", 'block', { fromValue: 'none'}], position: 3500, duration: 0 },
                { id: "eid383", tween: [ "style", "${_Text11}", "display", 'none', { fromValue: 'block'}], position: 3750, duration: 0 },
                { id: "eid296", tween: [ "style", "${_hilight21}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid326", tween: [ "style", "${_hilight21}", "display", 'block', { fromValue: 'none'}], position: 5000, duration: 0 },
                { id: "eid343", tween: [ "style", "${_Text20}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid400", tween: [ "style", "${_Text20}", "display", 'block', { fromValue: 'none'}], position: 7000, duration: 0 },
                { id: "eid401", tween: [ "style", "${_Text20}", "display", 'none', { fromValue: 'block'}], position: 7250, duration: 0 },
                { id: "eid305", tween: [ "style", "${_hilight12}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid317", tween: [ "style", "${_hilight12}", "display", 'block', { fromValue: 'none'}], position: 2750, duration: 0 },
                { id: "eid342", tween: [ "style", "${_Text21}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid402", tween: [ "style", "${_Text21}", "display", 'block', { fromValue: 'none'}], position: 7250, duration: 0 },
                { id: "eid403", tween: [ "style", "${_Text21}", "display", 'none', { fromValue: 'block'}], position: 7500, duration: 0 },
                { id: "eid213", tween: [ "style", "${_screen_13}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid225", tween: [ "style", "${_screen_13}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0 },
                { id: "eid350", tween: [ "style", "${_Text13}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid386", tween: [ "style", "${_Text13}", "display", 'block', { fromValue: 'none'}], position: 4250, duration: 0 },
                { id: "eid387", tween: [ "style", "${_Text13}", "display", 'none', { fromValue: 'block'}], position: 4500, duration: 0 },
                { id: "eid307", tween: [ "style", "${_hilight10}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid315", tween: [ "style", "${_hilight10}", "display", 'block', { fromValue: 'none'}], position: 2250, duration: 0 },
                { id: "eid351", tween: [ "style", "${_Text12}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid384", tween: [ "style", "${_Text12}", "display", 'block', { fromValue: 'none'}], position: 3750, duration: 0 },
                { id: "eid385", tween: [ "style", "${_Text12}", "display", 'none', { fromValue: 'block'}], position: 4250, duration: 0 },
                { id: "eid310", tween: [ "style", "${_hilight07}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid311", tween: [ "style", "${_hilight07}", "display", 'block', { fromValue: 'none'}], position: 1500, duration: 0 },
                { id: "eid300", tween: [ "style", "${_hilight17}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid322", tween: [ "style", "${_hilight17}", "display", 'block', { fromValue: 'none'}], position: 4000, duration: 0 },
                { id: "eid346", tween: [ "style", "${_Text17}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid394", tween: [ "style", "${_Text17}", "display", 'block', { fromValue: 'none'}], position: 6250, duration: 0 },
                { id: "eid395", tween: [ "style", "${_Text17}", "display", 'none', { fromValue: 'block'}], position: 6500, duration: 0 },
                { id: "eid358", tween: [ "style", "${_Text5}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid365", tween: [ "style", "${_Text5}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 },
                { id: "eid375", tween: [ "style", "${_Text5}", "display", 'none', { fromValue: 'block'}], position: 2000, duration: 0 },
                { id: "eid193", tween: [ "style", "${_screen_33}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid245", tween: [ "style", "${_screen_33}", "display", 'block', { fromValue: 'none'}], position: 8000, duration: 0 },
                { id: "eid194", tween: [ "style", "${_screen_32}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid244", tween: [ "style", "${_screen_32}", "display", 'block', { fromValue: 'none'}], position: 7750, duration: 0 },
                { id: "eid408", tween: [ "style", "${_Text23}", "display", 'block', { fromValue: 'none'}], position: 7750, duration: 0 },
                { id: "eid218", tween: [ "style", "${_screen_08}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid220", tween: [ "style", "${_screen_08}", "display", 'block', { fromValue: 'none'}], position: 1750, duration: 0 },
                { id: "eid211", tween: [ "style", "${_screen_15}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid227", tween: [ "style", "${_screen_15}", "display", 'block', { fromValue: 'none'}], position: 3500, duration: 0 },
                { id: "eid192", tween: [ "style", "${_screen_07}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid219", tween: [ "style", "${_screen_07}", "display", 'block', { fromValue: 'none'}], position: 1500, duration: 0 },
                { id: "eid148", tween: [ "style", "${_hilight02}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid149", tween: [ "style", "${_hilight02}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
                { id: "eid216", tween: [ "style", "${_screen_10}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid222", tween: [ "style", "${_screen_10}", "display", 'block', { fromValue: 'none'}], position: 2250, duration: 0 },
                { id: "eid345", tween: [ "style", "${_Text18}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid396", tween: [ "style", "${_Text18}", "display", 'block', { fromValue: 'none'}], position: 6500, duration: 0 },
                { id: "eid397", tween: [ "style", "${_Text18}", "display", 'none', { fromValue: 'block'}], position: 6750, duration: 0 },
                { id: "eid164", tween: [ "style", "${_hilight06}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid161", tween: [ "style", "${_hilight06}", "display", 'block', { fromValue: 'none'}], position: 1250, duration: 0 },
                { id: "eid198", tween: [ "style", "${_screen_28}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid240", tween: [ "style", "${_screen_28}", "display", 'block', { fromValue: 'none'}], position: 6750, duration: 0 },
                { id: "eid160", tween: [ "style", "${_hilight05}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid157", tween: [ "style", "${_hilight05}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 },
                { id: "eid205", tween: [ "style", "${_screen_21}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid233", tween: [ "style", "${_screen_21}", "display", 'block', { fromValue: 'none'}], position: 5000, duration: 0 },
                { id: "eid304", tween: [ "style", "${_hilight13}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid318", tween: [ "style", "${_hilight13}", "display", 'block', { fromValue: 'none'}], position: 3000, duration: 0 },
                { id: "eid85", tween: [ "style", "${_screen_06}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid84", tween: [ "style", "${_screen_06}", "display", 'block', { fromValue: 'none'}], position: 1250, duration: 0 },
                { id: "eid295", tween: [ "style", "${_hilight22}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid327", tween: [ "style", "${_hilight22}", "display", 'block', { fromValue: 'none'}], position: 5250, duration: 0 },
                { id: "eid214", tween: [ "style", "${_screen_12}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid224", tween: [ "style", "${_screen_12}", "display", 'block', { fromValue: 'none'}], position: 2750, duration: 0 },
                { id: "eid204", tween: [ "style", "${_screen_22}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid234", tween: [ "style", "${_screen_22}", "display", 'block', { fromValue: 'none'}], position: 5250, duration: 0 },
                { id: "eid306", tween: [ "style", "${_hilight11}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid316", tween: [ "style", "${_hilight11}", "display", 'block', { fromValue: 'none'}], position: 2500, duration: 0 },
                { id: "eid291", tween: [ "style", "${_hilight26}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid331", tween: [ "style", "${_hilight26}", "display", 'block', { fromValue: 'none'}], position: 6250, duration: 0 },
                { id: "eid195", tween: [ "style", "${_screen_31}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid243", tween: [ "style", "${_screen_31}", "display", 'block', { fromValue: 'none'}], position: 7500, duration: 0 },
                { id: "eid357", tween: [ "style", "${_Text6}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid366", tween: [ "style", "${_Text6}", "display", 'block', { fromValue: 'none'}], position: 2000, duration: 0 },
                { id: "eid376", tween: [ "style", "${_Text6}", "display", 'none', { fromValue: 'block'}], position: 2250, duration: 0 },
                { id: "eid203", tween: [ "style", "${_screen_23}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid235", tween: [ "style", "${_screen_23}", "display", 'block', { fromValue: 'none'}], position: 5500, duration: 0 },
                { id: "eid200", tween: [ "style", "${_screen_26}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid238", tween: [ "style", "${_screen_26}", "display", 'block', { fromValue: 'none'}], position: 6250, duration: 0 },
                { id: "eid209", tween: [ "style", "${_screen_17}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid229", tween: [ "style", "${_screen_17}", "display", 'block', { fromValue: 'none'}], position: 4000, duration: 0 },
                { id: "eid210", tween: [ "style", "${_screen_16}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid228", tween: [ "style", "${_screen_16}", "display", 'block', { fromValue: 'none'}], position: 3750, duration: 0 },
                { id: "eid196", tween: [ "style", "${_screen_30}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid242", tween: [ "style", "${_screen_30}", "display", 'block', { fromValue: 'none'}], position: 7250, duration: 0 },
                { id: "eid347", tween: [ "style", "${_Text16}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid392", tween: [ "style", "${_Text16}", "display", 'block', { fromValue: 'none'}], position: 6000, duration: 0 },
                { id: "eid393", tween: [ "style", "${_Text16}", "display", 'none', { fromValue: 'block'}], position: 6250, duration: 0 },
                { id: "eid290", tween: [ "style", "${_hilight27}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid332", tween: [ "style", "${_hilight27}", "display", 'block', { fromValue: 'none'}], position: 6500, duration: 0 },
                { id: "eid10", tween: [ "style", "${_Stage}", "height", '513px', { fromValue: '513px'}], position: 0, duration: 0 },
                { id: "eid360", tween: [ "style", "${_Text3}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid363", tween: [ "style", "${_Text3}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
                { id: "eid373", tween: [ "style", "${_Text3}", "display", 'none', { fromValue: 'block'}], position: 750, duration: 0 },
                { id: "eid297", tween: [ "style", "${_hilight20}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid325", tween: [ "style", "${_hilight20}", "display", 'block', { fromValue: 'none'}], position: 4750, duration: 0 },
                { id: "eid9", tween: [ "style", "${_Stage}", "width", '450px', { fromValue: '450px'}], position: 0, duration: 0 },
                { id: "eid199", tween: [ "style", "${_screen_27}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid239", tween: [ "style", "${_screen_27}", "display", 'block', { fromValue: 'none'}], position: 6500, duration: 0 },
                { id: "eid286", tween: [ "style", "${_hilight31}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid336", tween: [ "style", "${_hilight31}", "display", 'block', { fromValue: 'none'}], position: 7500, duration: 0 },
                { id: "eid202", tween: [ "style", "${_screen_24}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid236", tween: [ "style", "${_screen_24}", "display", 'block', { fromValue: 'none'}], position: 5750, duration: 0 },
                { id: "eid344", tween: [ "style", "${_Text19}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid398", tween: [ "style", "${_Text19}", "display", 'block', { fromValue: 'none'}], position: 6750, duration: 0 },
                { id: "eid399", tween: [ "style", "${_Text19}", "display", 'none', { fromValue: 'block'}], position: 7000, duration: 0 },
                { id: "eid303", tween: [ "style", "${_hilight14}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid319", tween: [ "style", "${_hilight14}", "display", 'block', { fromValue: 'none'}], position: 3250, duration: 0 },
                { id: "eid349", tween: [ "style", "${_Text14}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid388", tween: [ "style", "${_Text14}", "display", 'block', { fromValue: 'none'}], position: 4500, duration: 0 },
                { id: "eid389", tween: [ "style", "${_Text14}", "display", 'none', { fromValue: 'block'}], position: 5750, duration: 0 },
                { id: "eid215", tween: [ "style", "${_screen_11}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid223", tween: [ "style", "${_screen_11}", "display", 'block', { fromValue: 'none'}], position: 2500, duration: 0 },
                { id: "eid201", tween: [ "style", "${_screen_25}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid237", tween: [ "style", "${_screen_25}", "display", 'block', { fromValue: 'none'}], position: 6000, duration: 0 },
                { id: "eid294", tween: [ "style", "${_hilight23}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
                { id: "eid328", tween: [ "style", "${_hilight23}", "display", 'block', { fromValue: 'none'}], position: 5500, duration: 0 },
                { id: "eid371", tween: [ "style", "${_Text1}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
                { id: "eid370", tween: [ "style", "${_Text1}", "display", 'none', { fromValue: 'block'}], position: 250, duration: 0 },
                { id: "eid338", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_screen_01}', [0] ], ""], position: 7680 }            ]
        }
    }
},
"hilight": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    rect: ['0px', '0px', '35px', '35px', 'auto', 'auto'],
                    borderRadius: ['50%', '50%', '50%', '50%'],
                    id: 'Ellipse_1',
                    stroke: [0, 'rgba(0,0,0,1)', 'none'],
                    type: 'ellipse',
                    fill: ['rgba(21,151,240,1.00)']
                },
                {
                    rect: ['0px', '0px', '35px', '35px', 'auto', 'auto'],
                    borderRadius: ['50%', '50%', '50%', '50%'],
                    opacity: 0.17073170731707,
                    id: 'Ellipse',
                    stroke: [0, 'rgba(0,0,0,1)', 'none'],
                    type: 'ellipse',
                    fill: ['rgba(21,151,240,1.00)']
                },
                {
                    type: 'rect',
                    id: 'Rectangle',
                    stroke: [0, 'rgba(0,0,0,0.00)', 'none'],
                    rect: ['-19px', '0px', '74px', '35px', 'auto', 'auto'],
                    fill: ['rgba(21,151,240,0.00)']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_Rectangle}": [
                ["color", "background-color", 'rgba(21,151,240,0.00)'],
                ["color", "border-color", 'rgba(0,0,0,0.00)'],
                ["style", "left", '-19px'],
                ["style", "width", '74px']
            ],
            "${_Ellipse_1}": [
                ["color", "background-color", 'rgba(21,151,240,1.00)'],
                ["style", "top", '0px'],
                ["transform", "scaleY", '1'],
                ["transform", "scaleX", '1'],
                ["style", "height", '35px'],
                ["style", "opacity", '0.5'],
                ["style", "left", '0px'],
                ["style", "width", '35px']
            ],
            "${_Ellipse}": [
                ["style", "top", '0px'],
                ["color", "background-color", 'rgba(21,151,240,1.00)'],
                ["style", "height", '35px'],
                ["style", "opacity", '0.17073170731707'],
                ["style", "left", '0px'],
                ["style", "width", '35px']
            ],
            "${symbolSelector}": [
                ["style", "height", '35px'],
                ["style", "width", '35px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 500,
            autoPlay: true,
            labels: {
                "h_start": 0
            },
            timeline: [
                { id: "eid30", tween: [ "style", "${_Ellipse_1}", "opacity", '0', { fromValue: '0.5'}], position: 0, duration: 500 },
                { id: "eid25", tween: [ "transform", "${_Ellipse_1}", "scaleY", '2.16449', { fromValue: '1'}], position: 0, duration: 500 },
                { id: "eid24", tween: [ "transform", "${_Ellipse_1}", "scaleX", '2.16448', { fromValue: '1'}], position: 0, duration: 500 }            ]
        }
    }
},
"screen_01": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_01',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_01.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ],
            "${_screen_01}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"screen_02": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_02',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_02.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ],
            "${_screen_02}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"screen_03": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_03',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_03.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ],
            "${_screen_03}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"screen_04": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_04',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_04.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_screen_04}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"screen_05": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_05',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_05.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_screen_05}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
},
"screen_06": {
    version: "3.0.0",
    minimumCompatibleVersion: "3.0.0",
    build: "3.0.0.322",
    baseState: "Base State",
    scaleToFit: "none",
    centerStage: "none",
    initialState: "Base State",
    gpuAccelerate: false,
    resizeInstances: false,
    content: {
            dom: [
                {
                    id: 'screen_06',
                    type: 'image',
                    rect: ['0px', '0px', '407px', '513px', 'auto', 'auto'],
                    fill: ['rgba(0,0,0,0)', 'images/screen_06.png', '0px', '0px']
                }
            ],
            symbolInstances: [
            ]
        },
    states: {
        "Base State": {
            "${_screen_06}": [
                ["style", "left", '0px'],
                ["style", "top", '0px']
            ],
            "${symbolSelector}": [
                ["style", "height", '513px'],
                ["style", "width", '407px']
            ]
        }
    },
    timelines: {
        "Default Timeline": {
            fromState: "Base State",
            toState: "",
            duration: 0,
            autoPlay: true,
            timeline: [
            ]
        }
    }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources, opts);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-416691931");
