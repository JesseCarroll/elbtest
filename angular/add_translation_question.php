<md-content layout="column" class="md-whiteframe-1dp" ng-controller="questionCtrl as q">
	<div layout="row" layout-align="start center" layout-padding class="module_header">
		<span flex><b>Question {{$index + 1}}</b></span>
	</div>

	<md-content layout="column" layout-padding>
		<div ng-hide="disableFields || noTranslate" class="form-group">
			<label>Question Type</label>
			<select class="form-control" ng-model="question.template" ng-options="key as value for (key,value) in q.list"></select>
		</div>
		<add-text ng-model="question.prompt"    label="Prompt"    disabled="{{disableFields}}"></add-text>
		<add-text ng-if="question.hint" ng-model="question.hint"      label="Hint"      disabled="{{disableFields}}"></add-text>
		<add-text ng-model="question.category"  label="Category"  disabled="{{disableFields}}"></add-text>
		<add-text ng-model="question.correct"   label="Correct"   disabled="{{disableFields}}"></add-text>
		<add-text ng-model="question.incorrect" label="Incorrect" disabled="{{disableFields}}"></add-text>
		<add-media-controls
			existing-media-item-list = "question.media"
			disable-add              = "true"
			disable-decorations      = "true"
			expand-all               = "true"
			hide-non-translatable    = "true"
			editable                 = "!disableFields"
			translation              = "mediaControlsTranslationShortcode"
			internal-flex            = "100"
		></add-media-controls>
		<div
			ng-if="question.distractor.length > 0"
			ng-repeat="distractor in question.distractor"
			ng-init="parent = question.distractor; noTranslate = noTranslate; translation = translation;"
			ng-include="base_url + 'angular/add_distractor.php'"
		></div>
	</md-content>
</md-content>