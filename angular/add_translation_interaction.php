<md-whiteframe class="md-whiteframe-z1" layout layout-align="center center" ng-controller="interactionCtrl as i">
	<div layout="column" layout-fill>
		<div layout="row" class="topic_header">
			<span class="heading" flex><b>Interaction {{$index + 1}}:</b> {{interaction.label}}</span>
		</div>
		<md-content>
			<div layout="row" layout-padding>
				<div flex>
					<my-text ng-model="interaction.label" my-label="'Label'" my-disabled="disableFields"></my-text>
					<my-text-area ng-model="interaction.text" rtl="rtl" my-label="'Text'" my-disabled="disableFields"></my-text-area>
					<add-media-controls
						existing-media-item-list = "interaction.media"
						disable-add              = "true"
						disable-decorations      = "true"
						expand-all               = "true"
						hide-non-translatable    = "true"
						editable                 = "!disableFields"
						translation              = "mediaControlsTranslationShortcode"
					></add-media-controls>
					<my-single-upload ng-model="interaction.audio" my-label="'Interaction Audio'" my-disabled="disableFields" media-type="'audio'"></my-single-upload>
					<div class="custom_fields" ng-show="template_settings.interactions.custom.length > 0">
						<my-text ng-repeat="custom in template_settings.interactions.custom" ng-model="interaction[custom.key]" my-label="custom.label"></my-text>
					</div>
				</div>
			</div>
		</md-content>
	</div>
</md-whiteframe>
