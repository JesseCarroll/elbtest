<md-whiteframe class="md-whiteframe-z1" layout="column" ng-controller="objectiveCtrl as o">
  <div layout="row" layout-padding layout-align="start center" class="sortable module_header objective">
    <span flex class="md-primary" ng-click="o.editContent = !o.editContent">{{objective.title | capitalize}}</span>
    <i class="material-icons toggle-arrow" ng-class="{'active': o.editContent}" ng-click="o.editContent = !o.editContent">arrow_drop_up</i>
    <md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
    <md-button md-no-ink class="md-mini delete" ng-click="removeElement(parent, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
  </div>

  <md-content layout="column" layout-padding ng-show="o.editContent" class="md-padding" ng-init="objective.key = 'objective'+($index+1)">
    <md-input-container flex>
        <label>Objective Title</label>
        <input ng-model="objective.title">
    </md-input-container>
    <md-input-container flex>
      <label>Objective Description</label>
      <textarea ng-model="objective.description" columns="1" md-maxlength="250" maxlength="250"></textarea>
    </md-input-container>
    <md-input-container>
      <label>Mastery Score (%)</label>
      <input type="number" min="0" max="100" ng-model="objective.masteryScore">
    </md-input-container>
  </md-content>

</md-whiteframe>