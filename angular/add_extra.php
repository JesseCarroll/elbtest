<md-whiteframe class="md-whiteframe-z1" layout="column" ng-controller="extraItemCtrl as ex">
  <div ng-hide="disableFields || noTranslate" layout="row" layout-padding layout-align="start center" class="sortable module_header">
    <span flex class="md-primary" ng-click="m.editContent = !m.editContent"><b>Extra {{$index + 1}}: </b>{{extra.type | capitalize}}</span>
    <i class="material-icons toggle-arrow" ng-class= "{'active': m.editContent}" ng-click="m.editContent = !m.editContent">arrow_drop_up</i>
    <md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
    <md-button md-no-ink class="md-mini delete" ng-click="removeElement(parent, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
  </div>

  <md-content ng-switch on="media.type" layout="column" layout-padding ng-show="m.editContent || disableFields || noTranslate" class="md-padding">
      <add-select ng-model="extra.type" data-label="Extra Type" select-list="ex.list" disabled="{{disableFields}}"></add-select>
      <add-text ng-if="extra.type" ng-model="extra.title" data-label="Title" disabled="{{disableFields}}"></add-text>
      <add-number ng-if="extra.type" ng-model="extra.position" data-min="0" data-label="Position (Enter the number of the paragraph you want the tip button to follow.)" disabled="{{disableFields}}"></add-number>
      <div ng-if="extra.type && extra.type !== 'tip-response'">
        <add-textarea ng-model="extra.text" data-label="Text"></add-textarea>
      </div>
  </md-content>
</md-whiteframe>