<md-whiteframe class="md-whiteframe-z1" layout layout-align="center center" ng-controller="categoryCtrl">
	<div layout="column" layout-fill>
		<div ng-hide="disableFields || noTranslate" layout="row" layout-padding layout-align="start center" class="sortable module_header">
	  	<span ng-class="{'handle':parent.length > 1}" class="tab-title" ng-click="m.editContent = !m.editContent" flex><b>Category {{$index + 1}}:</b> {{data.title}}</span>
	    <i class="material-icons toggle-arrow" ng-class= "{'active': m.editContent}" ng-click="m.editContent = !m.editContent">arrow_drop_up</i>
	    <md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
	    <md-button md-no-ink class="md-mini delete" ng-click="removeElement(parent, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
		</div>
    <md-content id="category_{{data.$$hashKey | removeObject}}"  ng-show="m.editContent || disableFields || noTranslate" ng-init="data.key = getKey(data.key, parent)">
      <div flex layout-padding>
				<add-text ng-model="data.title" data-label="Title" disabled="{{disableFields}}"></add-text>
				<add-textarea ng-model="data.text" data-label="Text" disabled="{{disableFields}}"></add-textarea>

				<add-media-controls
					ng-if="template_settings.categories.media.enable"
					existing-media-item-list="data.media"
					template-settings="template_settings.categories"
				></add-media-controls>
			</div>
		</md-content>
	</div>
</md-whiteframe>
