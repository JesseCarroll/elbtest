<md-whiteframe class="md-whiteframe-z1" layout="column" ng-controller="glossaryCtrl as g">
	<div ng-hide="disableFields || noTranslate" layout="row" layout-padding layout-align="start center" class="sortable module_header">
		<span flex class="md-primary" ng-click="g.editContent = !g.editContent">
			{{glossary.term}}
			<i ng-if="!glossary.term">click to add glossary term</i>
		</span>
		<i class="material-icons toggle-arrow" ng-class="{'active': g.editContent}" ng-click="g.editContent = !g.editContent">arrow_drop_up</i>
		<md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
		<md-button md-no-ink class="md-mini delete" ng-click="removeElement(data.glossary, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
	</div>
	<md-content layout="column" layout-padding ng-show="g.editContent || disableFields || noTranslate" class="md-padding">
		<add-text ng-model="glossary.term" data-label="Glossary Term"></add-text>
		<add-textarea ng-model="glossary.definition" data-label="Glossary Definition"></add-textarea>
	</div>
</div>
