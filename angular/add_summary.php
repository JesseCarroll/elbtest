<div>
	<h4>Scoring Settings</h4>
	<md-content style="overflow:visible;">
		<md-grid-list md-cols="4" md-row-height="4:1" md-gutter="10px">
			<md-grid-tile class="gray" md-rowspan="1" md-colspan="1">
				<div flex layout="row" layout-align="space-around center">
					Unscored
					<md-switch ng-model="item.scored" aria-label="Is Scored" ng-disabled="disableScoreToggle"></md-switch>
					Scored
				</div>
			</md-grid-tile>
			<md-grid-tile ng-if="item.scored" class="gray" md-rowspan="1" md-colspan="3">
				<div flex layout="row" layout-align="space-around center">
					<md-input-container ng-if="showMasteryScore">
						<label>Passing Score (%)</label>
						<input type="number" min="0" max="100" ng-model="item.masteryScore">
					</md-input-container>
					<div layout="row" layout-align="start center" class="settings-item">
						<md-switch class="pull-right" md-no-ink ng-model="item.sendToLMS" aria-label="Send to LMS">Send to LMS</md-switch>
					</div>
					<div layout="row" layout-align="start center" class="settings-item">
						<div layout-align="center center" aria-label="If this is on, the user doesn't have to visit any/all pages, just has to pass the quiz to complete the course.">
							<md-tooltip md-direction="bottom left">
								If this is on, the user doesn't have to visit any/all pages, just has to pass the quiz to complete the course.
							</md-tooltip>
							<md-icon class="material-icons">info</md-icon>
						</div>
						<md-switch class="pull-right" md-no-ink ng-model="item.forceComplete" aria-label="Passing of Quiz = Course is Complete">Mark All Pages Complete</md-switch>
					</div>
				</div>
			</md-grid-tile>
		</md-grid-list>
	</md-content>
	<h4>Attempts and Summaries</h4>
	<div layout="row" ng-if="item.scored" class="summary-header">
		<div flex layout="row" layout-align="center center">
			<add-select flex="70" ng-model="item.activityAttempts" ng-hide="disableActivityAttempts" label="Number of Activity Attempts" select-list="attempts"></add-select>
		</div>
		<div flex class="button-container" ng-class="{'active': summary_type == 'initial'}" ng-click="changeType('initial')">
			<div layout="row" layout-align="center center" layout-fill class="button">Summary: Initial Attempt</div>
		</div>
		<div flex class="button-container" ng-class="{'active': summary_type == 'final'}">
			<div ng-if="item.activityAttempts > 1" layout="row" layout-align="center center" layout-fill class="button" ng-click="changeType('final')">Summary: Final Attempt</div>
		</div>
		<div flex layout="row" layout-align="center center">
			<md-input-container flex="70" ng-if="showQuizTimer">
				<label>Total Quiz Time Allowed (secs)</label>
				<input type="number" min="0" ng-model="item.quizTime">
			</md-input-container>
		</div>
	</div>
	<div ng-if="item.scored" layout="column" class="summary-content md-padding" ng-switch="summary_type">
		<div ng-switch-when="initial">
			<summary-item ng-model="item.summary.initial.pass" template_settings="template_settings" label="Pass Summary"></summary-item>
			<summary-item ng-model="item.summary.initial.fail" template_settings="template_settings" label="Fail Summary"></summary-item>
			<summary-item ng-if="item.quizTime" ng-model="item.summary.initial.timeout" template_settings="template_settings" label="Time Expired Summary"></summary-item>
		</div>
		<div ng-switch-when="final">
			<summary-item ng-model="item.summary.final.pass" template_settings="template_settings" label="Pass Summary"></summary-item>
			<summary-item ng-model="item.summary.final.fail" template_settings="template_settings" label="Fail Summary"></summary-item>
			<summary-item ng-if="item.quizTime" ng-model="item.summary.final.timeout" template_settings="template_settings" label="Time Expired Summary"></summary-item>
		</div>
	</div>
	<div ng-if="!item.scored" layout="column" class="md-padding">
		<summary-item ng-model="item.summary.unscored" label="Summary"></summary-item>
	</div>
</div>