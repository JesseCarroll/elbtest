<md-content class="md-whiteframe-1dp quest-sort" layout="column" ng-controller="questionCtrl as q">
	<div ng-hide="disableFields || noTranslate" layout="row" layout-align="start center" class="md-padding module_header q-subtab" ng-click="q.editContent = !q.editContent">
		<span flex class="md-primary">{{$index + 1}}: {{question.prompt}}</span>
		<i class="material-icons toggle-arrow" ng-class="{'active': q.editContent}">arrow_drop_up</i>
		<remove-buttons ng-model="parent" index="$index"></remove-buttons>
	</div>

	<md-content layout="column" ng-show="q.editContent" class="md-padding">
		<add-select ng-if="hideField('questionType')" ng-model="question.template" data-label="Question Type" select-list="q.question_list"></add-select>
		<add-text ng-model="question.prompt" data-label="Question:" placeholder="Type question text here"></add-text>
		<add-text ng-if="question.hint" ng-show="parentTemplate == 'millionaire'" ng-model="question.hint" data-label="Hint"></add-text>
		<add-text ng-show="parentTemplate == 'peril' || parentTemplate == 'escape' || parentTemplate == 'cardsort'" ng-model="question.category" data-label="Category"></add-text>
		<add-select flex ng-model="question.objective" label="Objectives" select-list="data.objectives" multiple="true"></add-select>
		<add-text ng-if="hideField('correct')" ng-hide="q.showElement('correct')" ng-model="question.correct" data-label="Correct Feedback" placeholder="{{data.common_terms['Correct']}}"></add-text>
		<add-text ng-if="hideField('incorrect')" ng-hide="q.showElement('incorrect')" ng-model="question.incorrect" data-label="Final Incorrect Feedback" placeholder="{{data.common_terms['Incorrect']}}"></add-text>
		<div layout="row" layout-align="start" class="settings-item">
			<md-switch md-no-ink ng-model="question.randomizeDistractors" aria-label="Randomize Distractors">Randomize Distractors</md-switch>
		</div>

		<add-media-controls
			ng-if="template_settings.media.enable != false"
			existing-media-item-list="question.media"
			template-settings="question.settings"
		></add-media-controls>
		<div ng-hide="q.showElement('distractors')" ui-sortable="q.sortableOptions" ng-model="question.distractor">
			<div ng-repeat="distractor in question.distractor" ng-init="parent = question.distractor" ng-include="'../angular/add_distractor.php'"></div>
		</div>
		<button ng-if="!q.settings.answers || question.distractor.length < q.settings.answers.limit" ng-hide="q.showElement('distractors')" type="button" class="btn btn-link" ng-click="addElement(question.distractor)">Add Answer</button>
	</md-content>

</md-content>
