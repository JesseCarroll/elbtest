<md-whiteframe class="md-whiteframe-z1" layout layout-align="center center">
    <div layout="column" layout-fill>
		<div layout="row" class="topic_header">
		  	<span class="heading" flex><b>Summary {{$index + 1}}:</b> {{summary.title}}</span>
		</div>
        <md-content>
            <div layout="row" layout-padding>
                <div flex>
					<my-text ng-model="summary.title" my-label="'Title'" my-disabled="disableFields"></my-text>
					<my-text-area ng-model="summary.text" rtl="rtl" my-label="'Text'" my-disabled="disableFields"></my-text-area>

					<div layout="row">
				        <md-input-container ng-if="summary.category || summary.category == ''" flex="20">
				            <label>Category</label>
				            <input ng-disabled="disableFields" type="text" ng-model="summary.category">
				        </md-input-container>

						<div class="form-group" ng-if="summary.threshold">
							<label class="control-label">Score Threshold</label>
							<input ng-disabled="disableFields" style="width: 50px;" ng-model="summary.threshold" type="text" class="form-control" maxlength="3">
						</div>
					</div>
				</div>
			</div>
		</md-content>
	</div>
</md-whiteframe>