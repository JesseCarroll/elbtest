<md-whiteframe class="md-whiteframe-z1" layout="column" ng-controller="interactionItemCtrl as m">
	<div ng-hide="disableFields || noTranslate" layout="row" layout-padding layout-align="start center" class="sortable module_header">
		<span flex class="md-primary" ng-click="m.editContent = !m.editContent">
			{{interaction.label}}
			<i ng-if="!interaction.label">click to add interaction detail</i>
		</span>
		<i class="material-icons toggle-arrow" ng-class="{'active': m.editContent}" ng-click="m.editContent = !m.editContent">arrow_drop_up</i>
		<md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
		<md-button md-no-ink class="md-mini delete" ng-click="removeElement(parent, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
	</div>

	<md-content ng-if="m.editContent" class="md-padding">

		<add-text ng-model="interaction.label" data-label="Title"></add-text>
		<label>Interaction Audio: {{interaction.audio}}</label>
		<div layout="row">
			<audio flex controls ng-if="hideField('interactionAudio') && interactionAudioPath">
				<source ng-src="{{interactionAudioPath}}" type="audio/mp3" />
			</audio>
			<button class="md-button" trigger-upload="uploadFile($file, 'interaction.audio', {classification: 'audio'})">Upload</button>
			<md-button ng-if="interaction.audio" ng-click="interaction.audio = ''">Delete</md-button>
		</div>

		<add-textarea ng-if="showText" ng-model="interaction.text" data-label="Text"></add-textarea>

		<add-media-controls
			ng-if="enable_media"
			existing-media-item-list="interaction.media"
			template-settings="settings"
		></add-media-controls>

		<div custom-fields="{{custom}}" ng-model="interaction" main-media="item.media" ng-repeat="custom in custom_fields"></div>

	</md-content>
</md-whiteframe>
