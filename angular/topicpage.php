<div>
	<md-tabs md-dynamic-height md-selected="tabSelect">
		<!-- MAIN -->
		<md-tab id="tab1" ng-if="page_tabs.main">
			<md-tab-label>Main</md-tab-label>
			<md-tab-body>
				<md-content class="fileupload fileinput topic-marginT">
					<md-grid-list md-cols="12" md-row-height="4:3" md-gutter="6px">
						<md-grid-tile md-colspan="2" md-rowspan="2" class="green">
							<div layout-fill class="background" ng-style="{'background-image':'url(' + coreRoot + 'templates/' + item.template + '.png)'}"></div>
							<md-grid-tile-footer layout-align="center center">{{item.template_name}}</md-grid-tile-footer>
						</md-grid-tile>

						<md-grid-tile ng-if="course_version.indexOf('3.') == -1" md-colspan="2" md-rowspan="2" class="blue" data-provides="fileinput">
							<div layout="row" layout-fill trigger-upload="uploadFile($file, 'item.background', {'classification': 'images'})">
								<md-icon ng-hide="item.background" style="cursor:pointer" class="material-icons" trigger-upload="uploadFile($file, 'item.background', {'classification': 'images'})">add</md-icon>
								<div ng-show="item.background" layout-fill class="background" ng-style="{'background-image':'url(' + imagePath + item.background + ')'}"></div>
							</div>
							<span class="delete" data-dismiss="fileinput" ng-show="item.background" ng-click="removeObjectItem(item, 'background')"><i class="fa fa-trash"></i></span>
							<md-grid-tile-footer trigger-upload="uploadFile($file, 'item.background', {'classification': 'images'})" layout-align="center center">Background Image</md-grid-tile-footer>
						</md-grid-tile>

						<md-grid-tile md-colspan="2" md-rowspan="2" class="violet">
							<div class="audio-player" layout="row" layout-fill layout-align="center center" ng-controller="inlineAudio as audio">
								<md-icon ng-hide="item.audio" style="cursor:pointer" class="material-icons" trigger-upload="uploadFile($file, 'item.audio', {'classification': 'audio'})">add</md-icon>
								<div ng-if="item.audio" layout="column" layout-fill layout-padding layout-align="center center">
									<a ng-class="{'playing':audio.playing}" class="audio-btn" ng-click="$event.stopPropagation(); audio.initAudio(item.audio)">play / pause</a>
									<span flex>{{item.audio}}</span>
								</div>
								<span class="delete" ng-show="item.audio" ng-click="removeObjectItem(item, 'audio')"><i class="fa fa-trash"></i></span>
								<md-grid-tile-footer style="cursor:pointer" layout-align="center center">
									<span trigger-upload="uploadFile($file, 'item.audio', {'classification': 'audio'})">Narration Audio</span>
								</md-grid-tile-footer>
							</div>
						</md-grid-tile>
						<md-grid-tile md-colspan="2" md-rowspan="1" class="gray">
							<md-switch flex md-no-ink ng-model="item.gate" aria-label="Gate Page">Gate Page</md-switch>
						</md-grid-tile>
						<md-grid-tile md-colspan="2" md-rowspan="1" class="gray">
							<div flex layout="row" class="md-padding" style="max-width: 100%">
								<add-select flex ng-model="item.objectives" label="Objectives" select-list="data.objectives" multiple="true"></add-select>
							</div>
						</md-grid-tile>
						<md-grid-tile ng-if="course_version.indexOf('3.') > -1" md-colspan="2" md-rowspan="2" class="gray"></md-grid-tile>
						<md-grid-tile md-colspan="2" md-rowspan="2" class="gray"></md-grid-tile>
						<md-grid-tile md-colspan="2" md-rowspan="1" class="gray">
							<md-input-container>
								<label>Page Timer</label>
								<input type="number" min="0" ng-model="item.timer">
							</md-input-container>
						</md-grid-tile>
						<md-grid-tile md-colspan="2" md-rowspan="1" class="gray">
							<div flex layout="row" class="md-padding" style="max-width: 100%">
								<add-select flex ng-model="item.roles" label="Roles" select-list="data.roles" multiple="true"></add-select>
							</div>
						</md-grid-tile>
					</md-grid-list>

					<md-content layout="column" class="md-padding">
						<add-text ng-model="item.title" data-label="Page Title"></add-text>
						<add-textarea ng-model="item.text" data-label="Intro"></add-textarea>
						<add-text ng-hide="template_settings.main.hide['prompt']" ng-model="item.prompt" data-label="Page Prompt"></add-text>

						<common-terms ng-if="template_settings.main.common_terms" ng-model="item.common_terms" page-terms="template_settings.main.common_terms"></common-terms>

						<div class="custom_fields" ng-if="template_settings.main.custom.length > 0">
							<h3>Customize this template</h3>
							<div custom-fields="{{custom}}" ng-model="item" ng-repeat="custom in template_settings.main.custom track by $id(custom)"></div>
						</div>

					</md-content>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Background -->
		<md-tab id="tab2" ng-if="page_tabs.background">
			<md-tab-label>Background</md-tab-label>
			<md-tab-body>
				<md-content layout-fill class="md-padding" md-swipe-left="next()" md-swipe-right="previous()">
					<background-object ng-model="item.background"></background-object>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Media -->
		<md-tab id="tab3" ng-if="page_tabs.media">
			<md-tab-label>Media</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()">
					<add-media-controls
						existing-media-item-list="item.media"
						template-settings="template_settings"
					></add-media-controls>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Interactions -->
		<md-tab id="tab4" ng-if="page_tabs.interactions">
			<md-tab-label>Interactions</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()" ng-controller="interactionCtrl as module">
					<add-text ng-if="course_version.indexOf('2.') > -1" ng-model="item['interaction-prompt']" data-label="Interaction Prompt"></add-text>
					<div ui-sortable="sortableModuleOptions" ng-model="item.interaction">
						<div ng-repeat="interaction in item.interaction" ng-init="parent = item.interaction" ng-include="base_url + 'angular/add_interaction.php'"></div>
					</div>
					<button ng-if="module.canAddInteraction()" type="button" class="btn btn-link" ng-click="addElement(item.interaction)">Add Interaction</button>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Categories -->
		<md-tab id="tab6" ng-if="page_tabs.categories">
			<md-tab-label>Categories</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()">
					<div ui-sortable="sortableOptions" ng-model="item.categories">
						<div ng-repeat="data in item.categories" ng-init="parent = item.categories; parentTemplate = item.template" ng-include="'../angular/add_category.php'"></div>
					</div>
					<button ng-hide="!(angular.isUndefined(template_settings.categories.limit)) && template_settings.categories.limit > 0 && item.categories.length >= template_settings.categories.limit" type="button" class="btn btn-link" ng-click="addElement(item.categories)">Add Category</button>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Items -->
		<md-tab id="tab5" ng-if="page_tabs.items">
			<md-tab-label>Items</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()">
					<div ui-sortable="sortableOptions" ng-model="item.items">
						<div ng-repeat="data in item.items" ng-init="parent = item.items; parentTemplate = item.template" ng-include="'../angular/add_items.php'"></div>
					</div>
					<button ng-hide="!(angular.isUndefined(template_settings.items.limit)) && template_settings.items.limit > 0 && item.items.length >= template_settings.items.limit" type="button" class="btn btn-link" ng-click="addElement(item.items)">Add Items</button>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Drop Zones -->
		<md-tab id="tab8" ng-if="page_tabs.dropzones">
			<md-tab-label>Drop Zones</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()" >
					<div ui-sortable="sortableOptions" ng-model="item.dropzones">
						<div ng-repeat="dropzone in item.dropzones | orderBy:'threshold':!reverse" ng-init="parent = item.dropzones" ng-include="'../angular/add_dropzone.php'"></div>
					</div>
					<button type="button" class="btn btn-link" ng-click="addElement(item.dropzones)">Add Drop Zone</button>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Questions -->
		<md-tab id="tab9" ng-if="page_tabs.questions">
			<md-tab-label>Questions</md-tab-label>
			<md-tab-body>
				<div role="tabpanel" md-swipe-left="next()" md-swipe-right="previous()">
					<md-content layout="row" layout-xs="column" layout-margin class="md-padding section-settings" ng-hide="template_settings.questions.settingsPanel!==undefined && template_settings.questions.settingsPanel!==true">
						<md-content layout="column" style="min-width:150px;">
							<md-input-container ng-hide="template_settings.questions.hide.indexOf('attempts') > -1">
								<label>Attempts</label>
								<input type="number" min="0" ng-model="item.attempts">
							</md-input-container>
						</md-content>
						<md-content layout="column">
							<div layout="row" layout-align="start center" class="settings-item">
								<label flex>Disable Feedback</label>
								<md-switch md-no-ink ng-model="item.disableFeedback" aria-label="Disable Feedback"></md-switch>
							</div>
							<div layout="row" layout-align="start center" class="settings-item">
								<label flex>Disable Correct<br>Answer Highlight</label>
								<md-switch md-no-ink ng-model="item.disableAnswerHighlight" style="line-height:130%;" aria-label="Disable Correct Answer Highlight"></md-switch>
							</div>
						</md-content>
						<md-content layout="column">
							<div layout="row" layout-align="start center" class="settings-item">
								<label flex>Randomize Questions</label>
								<md-switch class="pull-right" md-no-ink ng-model="item.randomize" aria-label="Scored"></md-switch>
							</div>
							<div layout="row" layout-align="start center" class="settings-item">
								<label flex>Randomize Distractors</label>
								<md-switch md-no-ink ng-model="item.randomizeDistractors" aria-label="Randomize Distractors"></md-switch>
							</div>
						</md-content>
						<md-content layout="column">
							<div layout="row" layout-align="start center" class="settings-item">
								<label flex>Use Question Pool</label>
								<md-switch class="pull-right" md-no-ink ng-model="item.pool" aria-label="Send to LMS"></md-switch>
							</div>
							<div layout="row" layout-align="start center" class="settings-item">
								<md-input-container ng-if="item.pool">
									<label>Question Pool Size</label>
									<input type="number" min="0" max="{{(item.question && item.question.length>0) ? item.question.length : 0}}" ng-model="item.poolSize">
								</md-input-container>
							</div>
						</md-content>
					</md-content>
					<md-divider></md-divider>
					<md-content class="md-padding">
						<div ui-sortable="sortableModuleOptions" ng-model="item.question">
							<div ng-repeat="question in item.question" ng-init="parent = item.question; parentTemplate = item.template" ng-include="'../angular/add_question.php'"></div>
						</div>
						<button ng-hide="!(angular.isUndefined(template_settings.questions.limit)) && template_settings.questions.limit > 0 && item.question.length >= template_settings.questions.limit" type="button" class="btn btn-link" ng-click="addElement(item.question)">Add Question</button>
					</md-content>
				</div>
			</md-tab-body>
		</md-tab>

		<!-- Summaries -->
		<md-tab id="tab7" ng-if="page_tabs.summary">
			<md-tab-label>Summary</md-tab-label>
			<md-tab-body>
				<summary-object ng-model="item" template-settings="template_settings"></summary-object>
			</md-tab-body>
		</md-tab>

		<!-- Extras -->
		<md-tab id="tab10" ng-if="page_tabs.extras">
			<md-tab-label>Extras</md-tab-label>
			<md-tab-body>
				<md-content class="md-padding" md-swipe-left="next()" md-swipe-right="previous()" ng-controller="extraCtrl as ex">
					<div ui-sortable="sortableOptions" ng-model="item.extra">
			<div ng-repeat="extra in item.extra" ng-init="parent = item.extra" ng-include="'../angular/add_extra.php'"></div>
			</div>
			<md-button ng-hide="item.extra.length >= template_settings.extra.limit" class="md-raised md-primary" ng-click="addElement(item.extra)">Add Extra</md-button>
				</md-content>
			</md-tab-body>
		</md-tab>

		<!-- Templates -->
		<md-tab id="tab11">
			<md-tab-label>Templates</md-tab-label>
			<md-tab-body>
				<div ng-controller="templateListCtrl">
					<md-content layout="row" layout-xs="column" class="template-setting-overflow topic-marginT">
						<md-content layout="column" class="ng-scope flex-50 template-setting-overflow" style="margin-right:10px;">
							<div class="filter template-setting-cont" layout="row">
								<div class="template-setting-header">PAGE FOCUS:</div>
					          	<md-checkbox class="text" ng-model="q['text']" ng-aria-label="text">Text</md-checkbox>
					          	<md-checkbox class="image" ng-model="q['image']"  aria-label="image">Image</md-checkbox>
					        </div>
			          	</md-content>
						<md-content layout="column" class="ng-scope flex-50 template-setting-overflow">
							<div class="filter template-setting-cont" layout="row">
								<div class="template-setting-header">SHOW MOBILE EQUIVALENT:</div>
				          		<md-switch ng-model="mobile" aria-label="Show Mobile Equivalent" style="margin:5px 0 4px 16px;"></md-switch>
					        </div>
			          	</md-content>
			        </md-content>
					<div style="padding:16px;">Select the type of page you want below.</div>
					<md-content layout="row" layout-wrap class="md-padding" md-swipe-left="next()" md-swipe-right="previous()" >
						<label flex="100" ng-repeat-start="template in chameleon.templates" ng-if="groupChange(template.group)">
							{{groupTitle(template.group)}}
						</label>

						<div ng-repeat-end flex="{{mobile ? '25' : '20'}}">
							<div class="template-box {{mobile ? 'mobile':''}}" ng-class="{'disable': template.type && !q[template.type], 'active': item.template == template.key}" ng-click="changeTemplate(template)">
								<div ng-if="mobile" style="display:flex">
									<div class="template-desktop"><img class="thumb" src="{{coreRoot}}templates/{{template_folder}}{{template.key}}.png"></div>
									<div class="template-mobile"><img class="thumb" src="{{coreRoot}}templates/{{template_folder}}{{template.key}}m.png"></div>
								</div>
								<img ng-if="!mobile" style="width:100%;" class="thumb" src="{{coreRoot}}templates/{{template_folder}}{{template.key}}.png">
								<p class="key">{{template.key | capitalize}}</p>
								<p class="name">{{template.name}}</p>
								<div class="type" ng-class="template.type" ng-if="template.type"></div>
							</div>
						</div>
					</md-content>
				</div>
			</md-tab-body>
		</md-tab>
	</md-tabs>
