<md-whiteframe class="md-whiteframe-z1" layout layout-align="center center">
  <div layout="column" layout-fill>
		<div layout="row" class="topic_header">
	  	<span ng-class="{'handle':parent.length > 1}" class="heading" flex><b>Drop Zone {{$index + 1}}:</b> {{dropzone.title}}</span>
      <div class="btn-group pull-right" style="padding: 0;">
        <button type="button" class="btn inline btn-info pull-right" ng-bind="delete_icon" ng-click="removeElement(parent, $index)"></button>
        <button ng-hide="confirm_delete" class="btn btn-info inline toggle collapsed" type="button" data-toggle="collapse" data-target="#dropzone_{{dropzone.$$hashKey | removeObject}}"></button>
        <button ng-show="confirm_delete" type="button" class="btn inline btn-info pull-right" ng-click="cancelRemoveElement()">Cancel</button>
      </div>
		</div>
    <md-content id="dropzone_{{dropzone.$$hashKey | removeObject}}" class="collapse">
      <div layout="column" layout-padding>
        <div flex>
	        <md-input-container flex>
            <label>Drop Zone Title</label>
            <input ng-model="dropzone.title">
	        </md-input-container>

					<div layout="row">
						<md-input-container flex="20">
							<label>Drop Zone Value</label>
							<input type="number" min="0" ng-model="dropzone.value">
						</md-input-container>

						<md-input-container flex="20">
							<label>Drop Zone Min</label>
							<input type="number" min="0" ng-model="dropzone.min">
						</md-input-container>

						<md-input-container flex="20">
							<label>Drop Zone Max</label>
							<input type="number" min="0" ng-model="dropzone.max">
						</md-input-container>

						<md-switch ng-model="dropzone.summary" aria-label="Show on Summary Page">
							Show on Summary Page
						</md-switch>
			    </div>
				</div>
			</div>
		</md-content>
	</div>
</md-whiteframe>