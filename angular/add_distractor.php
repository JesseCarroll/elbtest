<md-content layout="column" ng-class="{'correct': distractor.value}" class="md-whiteframe-z1 distractor" ng-controller="distractorCtrl as d">
	<div ng-hide="disableFields || noTranslate" layout="row" layout-padding layout-align="start center" class="sortable">
		<md-checkbox ng-if="hideField('boolean')" ng-disabled="disableFields || noTranslate" ng-model="distractor.value" class="md-primary" aria-label="Distractor Value"></md-checkbox>
		<span flex class="md-primary" ng-click="d.editContent = !d.editContent">{{$index + 1}}: {{distractor.text}}</span>
		<md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
		<md-button md-no-ink class="md-mini delete" ng-click="removeElement(question.distractor, $index)" ng-bind="delete_icon" aria-label="Delete Answer"></md-button>
	</div>

	<md-content layout="column" ng-show="d.editContent" class="md-padding" style="background:transparent;">
		<add-text ng-model="distractor.text" label="Answer" disabled="{{disableFields}}"></add-text>
		<add-text ng-hide="distractor.value" disabled="{{disableFields}}" ng-model="distractor.feedback" data-label="Initial Incorrect Feedback" placeholder="{{data.common_terms['Incorrect']}}. {{data.common_terms['Please try again']}}."></add-text>
		<add-media-controls
			ng-if="showMedia"
			existing-media-item-list = "distractor.media"
			template-settings        = "question.answer_settings"
			disable-add              = "noTranslate"
			disable-decorations      = "noTranslate"
			expand-all               = "noTranslate"
			hide-non-translatable    = "noTranslate"
			editable                 = "!disableFields"
			translation              = "translation"
			internal-flex            = "100"
		></add-media-controls>
        <div ng-if="question.template=='606'" disabled="{{disableFields}}" custom-fields="{{custom}}" ng-model="distractor" main-media="question.media" ng-repeat="custom in d.custom_fields"></div>
	</md-content>
</md-content>
