<div ng-switch on="media_type" style="padding: 10px;">

	<div ng-switch-when="image">
		<img ng-if="media.image[0]" style="width: 100%" ng-src="{{image_path}}{{media.image[0]}}">
		<img ng-if="ngModel" style="width: 100%" ng-src="{{image_path}}{{ngModel}}">
	</div>
	<div ng-switch-when="video">
		<img ng-if="media.image[0]" style="width: 100%; margin-bottom: 15px;" ng-src="{{image_path}}{{media.image[0]}}">

		<video width="100%" controls ng-init="this.video = video_path + media.video">
			<source ng-src="{{this.video}}" type="video/mp4">
			Your browser does not support video preview.
		</video>
	</div>
</div>