<md-content class="md-whiteframe-1dp" layout="column" layout-align="center center" ng-controller="itemsCtrl" style="margin-bottom:20px;">
	<div ng-hide="disableFields || noTranslate || hideControls" layout="row" layout-padding layout-align="start center" class="sortable module_header items">
  	<span ng-class="{'handle':parent.length > 1 && !hideControls}" class="tab-title" ng-click="m.editContent = !m.editContent" flex><b>Item {{$index + 1}}:</b> {{data.title}}</span>
    <i class="material-icons toggle-arrow" ng-class= "{'active': m.editContent}" ng-click="m.editContent = !m.editContent">arrow_drop_up</i>
    <md-button md-no-ink class="md-mini" ng-click="cancelRemoveElement()" ng-show="confirm_delete" aria-label="Cancel Delete">Cancel</md-button>
    <md-button md-no-ink class="md-mini delete" ng-click="removeElement(parent, $index)" ng-bind="delete_icon" aria-label="Delete Element"></md-button>
	</div>
  <md-content id="item_{{data.$$hashKey | removeObject}}" ng-show="m.editContent || disableFields || noTranslate || hideControls" layout="column" class="module_content items" layout-padding>
		<add-text ng-if="hideField('text')" ng-model="data.title" data-label="Title" disabled="{{disableFields}}"></add-text>
		<add-textarea ng-model="data.text" data-label="Text" disabled="{{disableFields}}"></add-textarea>

		<add-media-controls
			ng-if="template_settings.items.media.enable"
			existing-media-item-list="data.media"
			template-settings="template_settings.items"
		></add-media-controls>

		<add-text ng-if="hideField('correct')" ng-model="data.correct" data-label="Correct" disabled="{{disableFields}}"></add-text>
		<add-text ng-if="hideField('incorrect')" ng-model="data.incorrect" data-label="Incorrect" disabled="{{disableFields}}"></add-text>
		<add-text ng-if="hideField('finalIncorrect')" ng-model="data.final" data-label="Final Incorrect" disabled="{{disableFields}}"></add-text>

		<div ng-if="hideField('categories')">
			<div ng-if="item.categories.length == 0">No categories exist. Please create some categories in the Categories tab.</div>

			<!-- category -->
			<label flex>Category</label>
			<div flex layout="row" layout-padding>
	  		<md-checkbox ng-repeat="category in item.categories" ng-checked="exists(category, data.category)" ng-click="toggle(category, data.category)" aria-label="{{category.title || 'No Category Title'">{{category.title}}</md-checkbox>
	    </div>
	  </div>
		<div class="custom_fields" ng-if="template_settings.items.custom.length > 0">
			<h3>Customize this template</h3>
			<div custom-fields="{{custom}}" ng-model="data" main-media="item.media" ng-repeat="custom in template_settings.items.custom"></div>
		</div>
	</md-content>
</md-content>