<md-whiteframe class="md-whiteframe-z1" layout layout-align="center center">
    <div layout="column" layout-fill>
		<div layout="row" class="topic_header">
		  	<span class="heading" flex><b>Drop Zone {{$index + 1}}:</b> {{dropzone.title}}</span>
		</div>
        <md-content id="dropzone_{{dropzone.$$hashKey | removeObject}}">
            <div layout="column" layout-padding>
                <div flex>
			        <md-input-container flex>
			            <label>Drop Zone Title</label>
			            <input ng-disabled="disableFields" ng-model="dropzone.title">
			        </md-input-container>
<!--
					<div layout="row">
				        <md-input-container flex="20">
				            <label>Drop Zone Value</label>
				            <input type="number" min="0" ng-model="dropzone.value">
				        </md-input-container>

				        <md-input-container flex="20">
				            <label>Drop Zone Max</label>
				            <input type="number" min="0" ng-model="dropzone.max">
				        </md-input-container>
				    </div>
-->
				</div>
			</div>
		</md-content>
	</div>
</md-whiteframe>